﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.Collections.IEnumerable
struct IEnumerable_t2201844525;
// System.Text.StringBuilder
struct StringBuilder_t2947296310;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher
struct Dispatcher_t55733337;
// System.Object[]
struct ObjectU5BU5D_t3384890222;
// System.IAsyncResult
struct IAsyncResult_t1614106113;
// System.AsyncCallback
struct AsyncCallback_t606388952;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache
struct DispatcherCache_t37386862;
// System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>
struct Dictionary_2_t4130252518;
// System.Collections.Generic.IEqualityComparer`1<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey>
struct IEqualityComparer_1_t301474111;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1946233076;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1098411375;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey
struct DispatcherKey_t3620541735;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory
struct DispatcherFactory_t2726905719;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1484232934;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer
struct _EqualityComparer_t4027696990;
// Boo.Lang.Runtime.ExtensionRegistry
struct ExtensionRegistry_t4000900622;
// Boo.Lang.List`1<System.Reflection.MemberInfo>
struct List_1_t3801035620;
// Boo.Lang.List`1<System.Object>
struct List_1_t2975039247;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>
struct IEnumerable_1_t2375221188;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D
struct U3CCoerceU3Ec__AnonStorey1D_t2894686503;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Delegate
struct Delegate_t69892740;
// Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E
struct U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927;
// System.ApplicationException
struct ApplicationException_t3096800207;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t1692149247;
// System.IO.TextReader
struct TextReader_t2214828068;
// System.ArgumentException
struct ArgumentException_t3493207885;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t3445233625;
// Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC
struct U3CGetExtensionMethodsU3Ec__IteratorC_t147372645;
// System.Reflection.ParameterInfo
struct ParameterInfo_t3577984089;
// System.Reflection.MethodBase
struct MethodBase_t1640104494;
// System.Collections.IEnumerator
struct IEnumerator_t2441520391;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>
struct IEnumerator_1_t3960042352;
// System.NotSupportedException
struct NotSupportedException_t3527988452;
// Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD
struct U3ClinesU3Ec__IteratorD_t4243100130;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t2206957974;
// System.IntPtr[]
struct IntPtrU5BU5D_t1854953685;
// System.Collections.IDictionary
struct IDictionary_t1267335557;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>
struct IEnumerator_1_t2890029915;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t385114661;
// System.Int32[]
struct Int32U5BU5D_t595981822;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3878825437;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey[]
struct DispatcherKeyU5BU5D_t1340720734;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher[]
struct DispatcherU5BU5D_t2331836964;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1371384188;
// System.Collections.Generic.Dictionary`2/Transform`1<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher,System.Collections.DictionaryEntry>
struct Transform_1_t2809642575;
// System.Char[]
struct CharU5BU5D_t674980486;
// System.Void
struct Void_t2725935594;
// System.DelegateData
struct DelegateData_t421390435;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t3800382035;
// System.Reflection.MemberFilter
struct MemberFilter_t139494810;

extern RuntimeClass* StringBuilder_t2947296310_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_t2201844525_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t1054406163_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t2441520391_il2cpp_TypeInfo_var;
extern const uint32_t Builtins_join_m2871937356_MetadataUsageId;
extern RuntimeClass* DispatcherKey_t3620541735_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t4130252518_il2cpp_TypeInfo_var;
extern RuntimeClass* DispatcherCache_t37386862_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2520180731_RuntimeMethod_var;
extern const uint32_t DispatcherCache__cctor_m85627468_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m3549283624_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m2522272636_RuntimeMethod_var;
extern const uint32_t DispatcherCache_Get_m1017620631_MetadataUsageId;
extern RuntimeClass* Dispatcher_t55733337_il2cpp_TypeInfo_var;
extern const uint32_t DelegatePInvokeWrapper_DispatcherFactory_t2726905719_MetadataUsageId;
extern RuntimeClass* _EqualityComparer_t4027696990_il2cpp_TypeInfo_var;
extern const uint32_t DispatcherKey__cctor_m1717898894_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t _EqualityComparer_Equals_m1474669999_MetadataUsageId;
extern RuntimeClass* List_1_t3801035620_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m70727746_RuntimeMethod_var;
extern const uint32_t ExtensionRegistry__ctor_m2256012551_MetadataUsageId;
extern const RuntimeType* ExtensionAttribute_t2158284563_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Contains_m737308532_RuntimeMethod_var;
extern const uint32_t ExtensionRegistry_AddExtensionMembers_m3338094051_MetadataUsageId;
extern const RuntimeMethod* List_1__ctor_m2248684867_RuntimeMethod_var;
extern const uint32_t ExtensionRegistry_CopyExtensions_m3871346462_MetadataUsageId;
extern const RuntimeType* RuntimeServices_t4253430258_0_0_0_var;
extern RuntimeClass* ObjectU5BU5D_t3384890222_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeServices_t4253430258_il2cpp_TypeInfo_var;
extern RuntimeClass* ExtensionRegistry_t4000900622_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t362855854_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeServices__cctor_m1557287055_MetadataUsageId;
extern const uint32_t RuntimeServices_RegisterExtensions_m2899472418_MetadataUsageId;
extern const uint32_t RuntimeServices_GetDispatcher_m393801607_MetadataUsageId;
extern RuntimeClass* U3CCoerceU3Ec__AnonStorey1D_t2894686503_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t1484232934_il2cpp_TypeInfo_var;
extern RuntimeClass* DispatcherFactory_t2726905719_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m1840774687_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral790103786;
extern const uint32_t RuntimeServices_Coerce_m2787646698_MetadataUsageId;
extern RuntimeClass* ICoercible_t1994556652_il2cpp_TypeInfo_var;
extern const RuntimeMethod* RuntimeServices_IdentityDispatcher_m2167579733_RuntimeMethod_var;
extern const RuntimeMethod* RuntimeServices_CoercibleDispatcher_m286273110_RuntimeMethod_var;
extern const uint32_t RuntimeServices_CreateCoerceDispatcher_m4246784321_MetadataUsageId;
extern const RuntimeType* Dispatcher_t55733337_0_0_0_var;
extern const RuntimeType* NumericPromotions_t1087937753_0_0_0_var;
extern RuntimeClass* TypeCode_t100547427_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2546639394;
extern Il2CppCodeGenString* _stringLiteral1198481194;
extern const uint32_t RuntimeServices_EmitPromotionDispatcher_m363477410_MetadataUsageId;
extern const uint32_t RuntimeServices_IsPromotableNumeric_m2143124046_MetadataUsageId;
extern RuntimeClass* U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m681769745_RuntimeMethod_var;
extern const uint32_t RuntimeServices_EmitImplicitConversionDispatcher_m932860437_MetadataUsageId;
extern const uint32_t RuntimeServices_CoercibleDispatcher_m286273110_MetadataUsageId;
extern RuntimeClass* ApplicationException_t3096800207_il2cpp_TypeInfo_var;
extern RuntimeClass* TextReader_t2214828068_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3277810487;
extern Il2CppCodeGenString* _stringLiteral3540775313;
extern const uint32_t RuntimeServices_GetEnumerable_m3919393360_MetadataUsageId;
extern const uint32_t RuntimeServices_op_Addition_m1883231782_MetadataUsageId;
extern RuntimeClass* RuntimeArray_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeServices_EqualityOperator_m210483301_MetadataUsageId;
extern RuntimeClass* ArgumentException_t3493207885_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3151788631;
extern const uint32_t RuntimeServices_ArrayEqualityImpl_m3534544265_MetadataUsageId;
extern RuntimeClass* IConvertible_t2722588824_il2cpp_TypeInfo_var;
extern RuntimeClass* Decimal_t2663171440_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeServices_EqualityOperator_m2285347940_MetadataUsageId;
extern const uint32_t RuntimeServices_FindImplicitConversionOperator_m4110355163_MetadataUsageId;
extern RuntimeClass* U3CGetExtensionMethodsU3Ec__IteratorC_t147372645_il2cpp_TypeInfo_var;
extern const uint32_t RuntimeServices_GetExtensionMethods_m4000040295_MetadataUsageId;
extern RuntimeClass* IEnumerable_1_t3445233625_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t3960042352_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral474303686;
extern const uint32_t RuntimeServices_FindImplicitConversionMethod_m3016211815_MetadataUsageId;
extern const uint32_t U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m1840774687_MetadataUsageId;
extern const uint32_t U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m681769745_MetadataUsageId;
extern const uint32_t U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m4098321379_MetadataUsageId;
extern RuntimeClass* IEnumerable_1_t2375221188_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t2890029915_il2cpp_TypeInfo_var;
extern RuntimeClass* MethodInfo_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetExtensionMethodsU3Ec__IteratorC_MoveNext_m1298909905_MetadataUsageId;
extern const uint32_t U3CGetExtensionMethodsU3Ec__IteratorC_Dispose_m2812033218_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t3527988452_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetExtensionMethodsU3Ec__IteratorC_Reset_m3260254275_MetadataUsageId;
extern RuntimeClass* U3ClinesU3Ec__IteratorD_t4243100130_il2cpp_TypeInfo_var;
extern const uint32_t TextReaderEnumerator_lines_m3632397483_MetadataUsageId;
extern const uint32_t U3ClinesU3Ec__IteratorD_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3174056700_MetadataUsageId;
extern const uint32_t U3ClinesU3Ec__IteratorD_MoveNext_m1366279500_MetadataUsageId;
extern const uint32_t U3ClinesU3Ec__IteratorD_Dispose_m1809672304_MetadataUsageId;
extern const uint32_t U3ClinesU3Ec__IteratorD_Reset_m2544135774_MetadataUsageId;

struct ObjectU5BU5D_t3384890222;
struct TypeU5BU5D_t1484232934;
struct MemberInfoU5BU5D_t385114661;
struct MethodInfoU5BU5D_t1946502684;
struct ParameterInfoU5BU5D_t603298340;


#ifndef U3CMODULEU3E_T3896811612_H
#define U3CMODULEU3E_T3896811612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3896811612 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3896811612_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T82373287_H
#define EXCEPTION_T82373287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t82373287  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1854953685* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t82373287 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1854953685* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1854953685** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1854953685* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___inner_exception_1)); }
	inline Exception_t82373287 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t82373287 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t82373287 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t82373287, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t82373287, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T82373287_H
#ifndef VALUETYPE_T3433162460_H
#define VALUETYPE_T3433162460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3433162460  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_com
{
};
#endif // VALUETYPE_T3433162460_H
#ifndef U3CLINESU3EC__ITERATORD_T4243100130_H
#define U3CLINESU3EC__ITERATORD_T4243100130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD
struct  U3ClinesU3Ec__IteratorD_t4243100130  : public RuntimeObject
{
public:
	// System.IO.TextReader Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::reader
	TextReader_t2214828068 * ___reader_0;
	// System.IO.TextReader Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::<$s_51>__0
	TextReader_t2214828068 * ___U3CU24s_51U3E__0_1;
	// System.String Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::<line>__1
	String_t* ___U3ClineU3E__1_2;
	// System.Int32 Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::$PC
	int32_t ___U24PC_3;
	// System.String Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::$current
	String_t* ___U24current_4;
	// System.IO.TextReader Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::<$>reader
	TextReader_t2214828068 * ___U3CU24U3Ereader_5;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(U3ClinesU3Ec__IteratorD_t4243100130, ___reader_0)); }
	inline TextReader_t2214828068 * get_reader_0() const { return ___reader_0; }
	inline TextReader_t2214828068 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(TextReader_t2214828068 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_U3CU24s_51U3E__0_1() { return static_cast<int32_t>(offsetof(U3ClinesU3Ec__IteratorD_t4243100130, ___U3CU24s_51U3E__0_1)); }
	inline TextReader_t2214828068 * get_U3CU24s_51U3E__0_1() const { return ___U3CU24s_51U3E__0_1; }
	inline TextReader_t2214828068 ** get_address_of_U3CU24s_51U3E__0_1() { return &___U3CU24s_51U3E__0_1; }
	inline void set_U3CU24s_51U3E__0_1(TextReader_t2214828068 * value)
	{
		___U3CU24s_51U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24s_51U3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3ClineU3E__1_2() { return static_cast<int32_t>(offsetof(U3ClinesU3Ec__IteratorD_t4243100130, ___U3ClineU3E__1_2)); }
	inline String_t* get_U3ClineU3E__1_2() const { return ___U3ClineU3E__1_2; }
	inline String_t** get_address_of_U3ClineU3E__1_2() { return &___U3ClineU3E__1_2; }
	inline void set_U3ClineU3E__1_2(String_t* value)
	{
		___U3ClineU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClineU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3ClinesU3Ec__IteratorD_t4243100130, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3ClinesU3Ec__IteratorD_t4243100130, ___U24current_4)); }
	inline String_t* get_U24current_4() const { return ___U24current_4; }
	inline String_t** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(String_t* value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ereader_5() { return static_cast<int32_t>(offsetof(U3ClinesU3Ec__IteratorD_t4243100130, ___U3CU24U3Ereader_5)); }
	inline TextReader_t2214828068 * get_U3CU24U3Ereader_5() const { return ___U3CU24U3Ereader_5; }
	inline TextReader_t2214828068 ** get_address_of_U3CU24U3Ereader_5() { return &___U3CU24U3Ereader_5; }
	inline void set_U3CU24U3Ereader_5(TextReader_t2214828068 * value)
	{
		___U3CU24U3Ereader_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3Ereader_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLINESU3EC__ITERATORD_T4243100130_H
#ifndef TEXTREADERENUMERATOR_T1211441672_H
#define TEXTREADERENUMERATOR_T1211441672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.TextReaderEnumerator
struct  TextReaderEnumerator_t1211441672  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADERENUMERATOR_T1211441672_H
#ifndef U3CGETEXTENSIONMETHODSU3EC__ITERATORC_T147372645_H
#define U3CGETEXTENSIONMETHODSU3EC__ITERATORC_T147372645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC
struct  U3CGetExtensionMethodsU3Ec__IteratorC_t147372645  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::<$s_49>__0
	RuntimeObject* ___U3CU24s_49U3E__0_0;
	// System.Reflection.MemberInfo Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::<member>__1
	MemberInfo_t * ___U3CmemberU3E__1_1;
	// System.Int32 Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::$PC
	int32_t ___U24PC_2;
	// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::$current
	MethodInfo_t * ___U24current_3;

public:
	inline static int32_t get_offset_of_U3CU24s_49U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetExtensionMethodsU3Ec__IteratorC_t147372645, ___U3CU24s_49U3E__0_0)); }
	inline RuntimeObject* get_U3CU24s_49U3E__0_0() const { return ___U3CU24s_49U3E__0_0; }
	inline RuntimeObject** get_address_of_U3CU24s_49U3E__0_0() { return &___U3CU24s_49U3E__0_0; }
	inline void set_U3CU24s_49U3E__0_0(RuntimeObject* value)
	{
		___U3CU24s_49U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24s_49U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CmemberU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetExtensionMethodsU3Ec__IteratorC_t147372645, ___U3CmemberU3E__1_1)); }
	inline MemberInfo_t * get_U3CmemberU3E__1_1() const { return ___U3CmemberU3E__1_1; }
	inline MemberInfo_t ** get_address_of_U3CmemberU3E__1_1() { return &___U3CmemberU3E__1_1; }
	inline void set_U3CmemberU3E__1_1(MemberInfo_t * value)
	{
		___U3CmemberU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmemberU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetExtensionMethodsU3Ec__IteratorC_t147372645, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetExtensionMethodsU3Ec__IteratorC_t147372645, ___U24current_3)); }
	inline MethodInfo_t * get_U24current_3() const { return ___U24current_3; }
	inline MethodInfo_t ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(MethodInfo_t * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETEXTENSIONMETHODSU3EC__ITERATORC_T147372645_H
#ifndef TEXTREADER_T2214828068_H
#define TEXTREADER_T2214828068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t2214828068  : public RuntimeObject
{
public:

public:
};

struct TextReader_t2214828068_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t2214828068 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(TextReader_t2214828068_StaticFields, ___Null_0)); }
	inline TextReader_t2214828068 * get_Null_0() const { return ___Null_0; }
	inline TextReader_t2214828068 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(TextReader_t2214828068 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_T2214828068_H
#ifndef U3CEMITIMPLICITCONVERSIONDISPATCHERU3EC__ANONSTOREY1E_T3634380927_H
#define U3CEMITIMPLICITCONVERSIONDISPATCHERU3EC__ANONSTOREY1E_T3634380927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E
struct  U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E::method
	MethodInfo_t * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEMITIMPLICITCONVERSIONDISPATCHERU3EC__ANONSTOREY1E_T3634380927_H
#ifndef RUNTIMESERVICES_T4253430258_H
#define RUNTIMESERVICES_T4253430258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.RuntimeServices
struct  RuntimeServices_t4253430258  : public RuntimeObject
{
public:

public:
};

struct RuntimeServices_t4253430258_StaticFields
{
public:
	// System.Object[] Boo.Lang.Runtime.RuntimeServices::NoArguments
	ObjectU5BU5D_t3384890222* ___NoArguments_0;
	// System.Type Boo.Lang.Runtime.RuntimeServices::RuntimeServicesType
	Type_t * ___RuntimeServicesType_1;
	// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache Boo.Lang.Runtime.RuntimeServices::_cache
	DispatcherCache_t37386862 * ____cache_2;
	// Boo.Lang.Runtime.ExtensionRegistry Boo.Lang.Runtime.RuntimeServices::_extensions
	ExtensionRegistry_t4000900622 * ____extensions_3;
	// System.Object Boo.Lang.Runtime.RuntimeServices::True
	RuntimeObject * ___True_4;

public:
	inline static int32_t get_offset_of_NoArguments_0() { return static_cast<int32_t>(offsetof(RuntimeServices_t4253430258_StaticFields, ___NoArguments_0)); }
	inline ObjectU5BU5D_t3384890222* get_NoArguments_0() const { return ___NoArguments_0; }
	inline ObjectU5BU5D_t3384890222** get_address_of_NoArguments_0() { return &___NoArguments_0; }
	inline void set_NoArguments_0(ObjectU5BU5D_t3384890222* value)
	{
		___NoArguments_0 = value;
		Il2CppCodeGenWriteBarrier((&___NoArguments_0), value);
	}

	inline static int32_t get_offset_of_RuntimeServicesType_1() { return static_cast<int32_t>(offsetof(RuntimeServices_t4253430258_StaticFields, ___RuntimeServicesType_1)); }
	inline Type_t * get_RuntimeServicesType_1() const { return ___RuntimeServicesType_1; }
	inline Type_t ** get_address_of_RuntimeServicesType_1() { return &___RuntimeServicesType_1; }
	inline void set_RuntimeServicesType_1(Type_t * value)
	{
		___RuntimeServicesType_1 = value;
		Il2CppCodeGenWriteBarrier((&___RuntimeServicesType_1), value);
	}

	inline static int32_t get_offset_of__cache_2() { return static_cast<int32_t>(offsetof(RuntimeServices_t4253430258_StaticFields, ____cache_2)); }
	inline DispatcherCache_t37386862 * get__cache_2() const { return ____cache_2; }
	inline DispatcherCache_t37386862 ** get_address_of__cache_2() { return &____cache_2; }
	inline void set__cache_2(DispatcherCache_t37386862 * value)
	{
		____cache_2 = value;
		Il2CppCodeGenWriteBarrier((&____cache_2), value);
	}

	inline static int32_t get_offset_of__extensions_3() { return static_cast<int32_t>(offsetof(RuntimeServices_t4253430258_StaticFields, ____extensions_3)); }
	inline ExtensionRegistry_t4000900622 * get__extensions_3() const { return ____extensions_3; }
	inline ExtensionRegistry_t4000900622 ** get_address_of__extensions_3() { return &____extensions_3; }
	inline void set__extensions_3(ExtensionRegistry_t4000900622 * value)
	{
		____extensions_3 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_3), value);
	}

	inline static int32_t get_offset_of_True_4() { return static_cast<int32_t>(offsetof(RuntimeServices_t4253430258_StaticFields, ___True_4)); }
	inline RuntimeObject * get_True_4() const { return ___True_4; }
	inline RuntimeObject ** get_address_of_True_4() { return &___True_4; }
	inline void set_True_4(RuntimeObject * value)
	{
		___True_4 = value;
		Il2CppCodeGenWriteBarrier((&___True_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMESERVICES_T4253430258_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef LIST_1_T3801035620_H
#define LIST_1_T3801035620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.List`1<System.Reflection.MemberInfo>
struct  List_1_t3801035620  : public RuntimeObject
{
public:
	// T[] Boo.Lang.List`1::_items
	MemberInfoU5BU5D_t385114661* ____items_1;
	// System.Int32 Boo.Lang.List`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3801035620, ____items_1)); }
	inline MemberInfoU5BU5D_t385114661* get__items_1() const { return ____items_1; }
	inline MemberInfoU5BU5D_t385114661** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MemberInfoU5BU5D_t385114661* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(List_1_t3801035620, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};

struct List_1_t3801035620_StaticFields
{
public:
	// T[] Boo.Lang.List`1::EmptyArray
	MemberInfoU5BU5D_t385114661* ___EmptyArray_0;

public:
	inline static int32_t get_offset_of_EmptyArray_0() { return static_cast<int32_t>(offsetof(List_1_t3801035620_StaticFields, ___EmptyArray_0)); }
	inline MemberInfoU5BU5D_t385114661* get_EmptyArray_0() const { return ___EmptyArray_0; }
	inline MemberInfoU5BU5D_t385114661** get_address_of_EmptyArray_0() { return &___EmptyArray_0; }
	inline void set_EmptyArray_0(MemberInfoU5BU5D_t385114661* value)
	{
		___EmptyArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3801035620_H
#ifndef EXTENSIONREGISTRY_T4000900622_H
#define EXTENSIONREGISTRY_T4000900622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.ExtensionRegistry
struct  ExtensionRegistry_t4000900622  : public RuntimeObject
{
public:
	// Boo.Lang.List`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::_extensions
	List_1_t3801035620 * ____extensions_0;
	// System.Object Boo.Lang.Runtime.ExtensionRegistry::_classLock
	RuntimeObject * ____classLock_1;

public:
	inline static int32_t get_offset_of__extensions_0() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4000900622, ____extensions_0)); }
	inline List_1_t3801035620 * get__extensions_0() const { return ____extensions_0; }
	inline List_1_t3801035620 ** get_address_of__extensions_0() { return &____extensions_0; }
	inline void set__extensions_0(List_1_t3801035620 * value)
	{
		____extensions_0 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_0), value);
	}

	inline static int32_t get_offset_of__classLock_1() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4000900622, ____classLock_1)); }
	inline RuntimeObject * get__classLock_1() const { return ____classLock_1; }
	inline RuntimeObject ** get_address_of__classLock_1() { return &____classLock_1; }
	inline void set__classLock_1(RuntimeObject * value)
	{
		____classLock_1 = value;
		Il2CppCodeGenWriteBarrier((&____classLock_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONREGISTRY_T4000900622_H
#ifndef NUMERICPROMOTIONS_T1087937753_H
#define NUMERICPROMOTIONS_T1087937753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.NumericPromotions
struct  NumericPromotions_t1087937753  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERICPROMOTIONS_T1087937753_H
#ifndef _EQUALITYCOMPARER_T4027696990_H
#define _EQUALITYCOMPARER_T4027696990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer
struct  _EqualityComparer_t4027696990  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _EQUALITYCOMPARER_T4027696990_H
#ifndef U3CCOERCEU3EC__ANONSTOREY1D_T2894686503_H
#define U3CCOERCEU3EC__ANONSTOREY1D_T2894686503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D
struct  U3CCoerceU3Ec__AnonStorey1D_t2894686503  : public RuntimeObject
{
public:
	// System.Object Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::value
	RuntimeObject * ___value_0;
	// System.Type Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::toType
	Type_t * ___toType_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CCoerceU3Ec__AnonStorey1D_t2894686503, ___value_0)); }
	inline RuntimeObject * get_value_0() const { return ___value_0; }
	inline RuntimeObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RuntimeObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_toType_1() { return static_cast<int32_t>(offsetof(U3CCoerceU3Ec__AnonStorey1D_t2894686503, ___toType_1)); }
	inline Type_t * get_toType_1() const { return ___toType_1; }
	inline Type_t ** get_address_of_toType_1() { return &___toType_1; }
	inline void set_toType_1(Type_t * value)
	{
		___toType_1 = value;
		Il2CppCodeGenWriteBarrier((&___toType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOERCEU3EC__ANONSTOREY1D_T2894686503_H
#ifndef BUILTINS_T3619821444_H
#define BUILTINS_T3619821444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Builtins
struct  Builtins_t3619821444  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINS_T3619821444_H
#ifndef DICTIONARY_2_T4130252518_H
#define DICTIONARY_2_T4130252518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>
struct  Dictionary_2_t4130252518  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t595981822* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t3878825437* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	DispatcherKeyU5BU5D_t1340720734* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	DispatcherU5BU5D_t2331836964* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t1371384188 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___table_4)); }
	inline Int32U5BU5D_t595981822* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t595981822** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t595981822* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___linkSlots_5)); }
	inline LinkU5BU5D_t3878825437* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t3878825437** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t3878825437* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___keySlots_6)); }
	inline DispatcherKeyU5BU5D_t1340720734* get_keySlots_6() const { return ___keySlots_6; }
	inline DispatcherKeyU5BU5D_t1340720734** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(DispatcherKeyU5BU5D_t1340720734* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___valueSlots_7)); }
	inline DispatcherU5BU5D_t2331836964* get_valueSlots_7() const { return ___valueSlots_7; }
	inline DispatcherU5BU5D_t2331836964** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(DispatcherU5BU5D_t2331836964* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___serialization_info_13)); }
	inline SerializationInfo_t1371384188 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t1371384188 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t1371384188 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t4130252518_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2809642575 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t4130252518_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2809642575 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2809642575 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2809642575 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T4130252518_H
#ifndef DISPATCHERKEY_T3620541735_H
#define DISPATCHERKEY_T3620541735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey
struct  DispatcherKey_t3620541735  : public RuntimeObject
{
public:
	// System.Type Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::_type
	Type_t * ____type_1;
	// System.String Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::_name
	String_t* ____name_2;
	// System.Type[] Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::_arguments
	TypeU5BU5D_t1484232934* ____arguments_3;

public:
	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(DispatcherKey_t3620541735, ____type_1)); }
	inline Type_t * get__type_1() const { return ____type_1; }
	inline Type_t ** get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(Type_t * value)
	{
		____type_1 = value;
		Il2CppCodeGenWriteBarrier((&____type_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(DispatcherKey_t3620541735, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__arguments_3() { return static_cast<int32_t>(offsetof(DispatcherKey_t3620541735, ____arguments_3)); }
	inline TypeU5BU5D_t1484232934* get__arguments_3() const { return ____arguments_3; }
	inline TypeU5BU5D_t1484232934** get_address_of__arguments_3() { return &____arguments_3; }
	inline void set__arguments_3(TypeU5BU5D_t1484232934* value)
	{
		____arguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____arguments_3), value);
	}
};

struct DispatcherKey_t3620541735_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey> Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::EqualityComparer
	RuntimeObject* ___EqualityComparer_0;

public:
	inline static int32_t get_offset_of_EqualityComparer_0() { return static_cast<int32_t>(offsetof(DispatcherKey_t3620541735_StaticFields, ___EqualityComparer_0)); }
	inline RuntimeObject* get_EqualityComparer_0() const { return ___EqualityComparer_0; }
	inline RuntimeObject** get_address_of_EqualityComparer_0() { return &___EqualityComparer_0; }
	inline void set_EqualityComparer_0(RuntimeObject* value)
	{
		___EqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___EqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHERKEY_T3620541735_H
#ifndef DISPATCHERCACHE_T37386862_H
#define DISPATCHERCACHE_T37386862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache
struct  DispatcherCache_t37386862  : public RuntimeObject
{
public:

public:
};

struct DispatcherCache_t37386862_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher> Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::_cache
	Dictionary_2_t4130252518 * ____cache_0;

public:
	inline static int32_t get_offset_of__cache_0() { return static_cast<int32_t>(offsetof(DispatcherCache_t37386862_StaticFields, ____cache_0)); }
	inline Dictionary_2_t4130252518 * get__cache_0() const { return ____cache_0; }
	inline Dictionary_2_t4130252518 ** get_address_of__cache_0() { return &____cache_0; }
	inline void set__cache_0(Dictionary_2_t4130252518 * value)
	{
		____cache_0 = value;
		Il2CppCodeGenWriteBarrier((&____cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHERCACHE_T37386862_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t674980486* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t674980486* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t674980486** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t674980486* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STRINGBUILDER_T2947296310_H
#define STRINGBUILDER_T2947296310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t2947296310  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t2947296310, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t2947296310, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t2947296310, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t2947296310, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T2947296310_H
#ifndef INT32_T3425510919_H
#define INT32_T3425510919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t3425510919 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t3425510919, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T3425510919_H
#ifndef ENUM_T473240710_H
#define ENUM_T473240710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t473240710  : public ValueType_t3433162460
{
public:

public:
};

struct Enum_t473240710_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t674980486* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t473240710_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t674980486* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t674980486** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t674980486* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t473240710_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t473240710_marshaled_com
{
};
#endif // ENUM_T473240710_H
#ifndef VOID_T2725935594_H
#define VOID_T2725935594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2725935594 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2725935594_H
#ifndef METHODBASE_T1640104494_H
#define METHODBASE_T1640104494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t1640104494  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T1640104494_H
#ifndef BOOLEAN_T362855854_H
#define BOOLEAN_T362855854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t362855854 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t362855854, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t362855854_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t362855854_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t362855854_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T362855854_H
#ifndef UINT32_T3933237433_H
#define UINT32_T3933237433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t3933237433 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t3933237433, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T3933237433_H
#ifndef INT64_T2252457107_H
#define INT64_T2252457107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t2252457107 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t2252457107, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T2252457107_H
#ifndef UINT64_T1498391637_H
#define UINT64_T1498391637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t1498391637 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t1498391637, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T1498391637_H
#ifndef SINGLE_T2847614712_H
#define SINGLE_T2847614712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2847614712 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2847614712, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2847614712_H
#ifndef DECIMAL_T2663171440_H
#define DECIMAL_T2663171440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t2663171440 
{
public:
	// System.UInt32 System.Decimal::flags
	uint32_t ___flags_5;
	// System.UInt32 System.Decimal::hi
	uint32_t ___hi_6;
	// System.UInt32 System.Decimal::lo
	uint32_t ___lo_7;
	// System.UInt32 System.Decimal::mid
	uint32_t ___mid_8;

public:
	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(Decimal_t2663171440, ___flags_5)); }
	inline uint32_t get_flags_5() const { return ___flags_5; }
	inline uint32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(uint32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_hi_6() { return static_cast<int32_t>(offsetof(Decimal_t2663171440, ___hi_6)); }
	inline uint32_t get_hi_6() const { return ___hi_6; }
	inline uint32_t* get_address_of_hi_6() { return &___hi_6; }
	inline void set_hi_6(uint32_t value)
	{
		___hi_6 = value;
	}

	inline static int32_t get_offset_of_lo_7() { return static_cast<int32_t>(offsetof(Decimal_t2663171440, ___lo_7)); }
	inline uint32_t get_lo_7() const { return ___lo_7; }
	inline uint32_t* get_address_of_lo_7() { return &___lo_7; }
	inline void set_lo_7(uint32_t value)
	{
		___lo_7 = value;
	}

	inline static int32_t get_offset_of_mid_8() { return static_cast<int32_t>(offsetof(Decimal_t2663171440, ___mid_8)); }
	inline uint32_t get_mid_8() const { return ___mid_8; }
	inline uint32_t* get_address_of_mid_8() { return &___mid_8; }
	inline void set_mid_8(uint32_t value)
	{
		___mid_8 = value;
	}
};

struct Decimal_t2663171440_StaticFields
{
public:
	// System.Decimal System.Decimal::MinValue
	Decimal_t2663171440  ___MinValue_0;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t2663171440  ___MaxValue_1;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t2663171440  ___MinusOne_2;
	// System.Decimal System.Decimal::One
	Decimal_t2663171440  ___One_3;
	// System.Decimal System.Decimal::MaxValueDiv10
	Decimal_t2663171440  ___MaxValueDiv10_4;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(Decimal_t2663171440_StaticFields, ___MinValue_0)); }
	inline Decimal_t2663171440  get_MinValue_0() const { return ___MinValue_0; }
	inline Decimal_t2663171440 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(Decimal_t2663171440  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(Decimal_t2663171440_StaticFields, ___MaxValue_1)); }
	inline Decimal_t2663171440  get_MaxValue_1() const { return ___MaxValue_1; }
	inline Decimal_t2663171440 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(Decimal_t2663171440  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinusOne_2() { return static_cast<int32_t>(offsetof(Decimal_t2663171440_StaticFields, ___MinusOne_2)); }
	inline Decimal_t2663171440  get_MinusOne_2() const { return ___MinusOne_2; }
	inline Decimal_t2663171440 * get_address_of_MinusOne_2() { return &___MinusOne_2; }
	inline void set_MinusOne_2(Decimal_t2663171440  value)
	{
		___MinusOne_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(Decimal_t2663171440_StaticFields, ___One_3)); }
	inline Decimal_t2663171440  get_One_3() const { return ___One_3; }
	inline Decimal_t2663171440 * get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(Decimal_t2663171440  value)
	{
		___One_3 = value;
	}

	inline static int32_t get_offset_of_MaxValueDiv10_4() { return static_cast<int32_t>(offsetof(Decimal_t2663171440_StaticFields, ___MaxValueDiv10_4)); }
	inline Decimal_t2663171440  get_MaxValueDiv10_4() const { return ___MaxValueDiv10_4; }
	inline Decimal_t2663171440 * get_address_of_MaxValueDiv10_4() { return &___MaxValueDiv10_4; }
	inline void set_MaxValueDiv10_4(Decimal_t2663171440  value)
	{
		___MaxValueDiv10_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T2663171440_H
#ifndef DOUBLE_T1029397067_H
#define DOUBLE_T1029397067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t1029397067 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t1029397067, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T1029397067_H
#ifndef APPLICATIONEXCEPTION_T3096800207_H
#define APPLICATIONEXCEPTION_T3096800207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ApplicationException
struct  ApplicationException_t3096800207  : public Exception_t82373287
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEXCEPTION_T3096800207_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T500802787_H
#define SYSTEMEXCEPTION_T500802787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t500802787  : public Exception_t82373287
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T500802787_H
#ifndef MEMBERTYPES_T2151977880_H
#define MEMBERTYPES_T2151977880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberTypes
struct  MemberTypes_t2151977880 
{
public:
	// System.Int32 System.Reflection.MemberTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MemberTypes_t2151977880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERTYPES_T2151977880_H
#ifndef RUNTIMETYPEHANDLE_T3928229644_H
#define RUNTIMETYPEHANDLE_T3928229644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3928229644 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3928229644, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3928229644_H
#ifndef BINDINGFLAGS_T3095937196_H
#define BINDINGFLAGS_T3095937196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t3095937196 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t3095937196, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T3095937196_H
#ifndef NOTSUPPORTEDEXCEPTION_T3527988452_H
#define NOTSUPPORTEDEXCEPTION_T3527988452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t3527988452  : public SystemException_t500802787
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T3527988452_H
#ifndef ARGUMENTEXCEPTION_T3493207885_H
#define ARGUMENTEXCEPTION_T3493207885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t3493207885  : public SystemException_t500802787
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t3493207885, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T3493207885_H
#ifndef PARAMETERATTRIBUTES_T1362082053_H
#define PARAMETERATTRIBUTES_T1362082053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t1362082053 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t1362082053, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T1362082053_H
#ifndef TYPECODE_T100547427_H
#define TYPECODE_T100547427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TypeCode
struct  TypeCode_t100547427 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeCode_t100547427, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECODE_T100547427_H
#ifndef DELEGATE_T69892740_H
#define DELEGATE_T69892740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t69892740  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t421390435 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___data_8)); }
	inline DelegateData_t421390435 * get_data_8() const { return ___data_8; }
	inline DelegateData_t421390435 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t421390435 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T69892740_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t1640104494
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef PARAMETERINFO_T3577984089_H
#define PARAMETERINFO_T3577984089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t3577984089  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.ParameterInfo::marshalAs
	UnmanagedMarshal_t3800382035 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t3577984089, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t3577984089, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t3577984089, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t3577984089, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t3577984089, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t3577984089, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t3577984089, ___marshalAs_6)); }
	inline UnmanagedMarshal_t3800382035 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline UnmanagedMarshal_t3800382035 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(UnmanagedMarshal_t3800382035 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERINFO_T3577984089_H
#ifndef MULTICASTDELEGATE_T1138444986_H
#define MULTICASTDELEGATE_T1138444986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1138444986  : public Delegate_t69892740
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1138444986 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1138444986 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1138444986, ___prev_9)); }
	inline MulticastDelegate_t1138444986 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1138444986 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1138444986 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1138444986, ___kpm_next_10)); }
	inline MulticastDelegate_t1138444986 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1138444986 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1138444986 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1138444986_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3928229644  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3928229644  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3928229644 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3928229644  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1484232934* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t139494810 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t139494810 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t139494810 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1484232934* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1484232934** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1484232934* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t139494810 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t139494810 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t139494810 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t139494810 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t139494810 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t139494810 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t139494810 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t139494810 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t139494810 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef ASYNCCALLBACK_T606388952_H
#define ASYNCCALLBACK_T606388952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t606388952  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T606388952_H
#ifndef DISPATCHER_T55733337_H
#define DISPATCHER_T55733337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.Dispatcher
struct  Dispatcher_t55733337  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHER_T55733337_H
#ifndef DISPATCHERFACTORY_T2726905719_H
#define DISPATCHERFACTORY_T2726905719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory
struct  DispatcherFactory_t2726905719  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHERFACTORY_T2726905719_H
// System.Object[]
struct ObjectU5BU5D_t3384890222  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t1484232934  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t385114661  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MemberInfo_t * m_Items[1];

public:
	inline MemberInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MemberInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MemberInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MemberInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MemberInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MemberInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1946502684  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MethodInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t603298340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t3577984089 * m_Items[1];

public:
	inline ParameterInfo_t3577984089 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t3577984089 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t3577984089 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t3577984089 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t3577984089 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t3577984089 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
extern "C"  void Dictionary_2__ctor_m229984624_gshared (Dictionary_2_t1946233076 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m2098810262_gshared (Dictionary_2_t1946233076 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1598188702_gshared (Dictionary_2_t1946233076 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void Boo.Lang.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2096670811_gshared (List_1_t2975039247 * __this, const RuntimeMethod* method);
// System.Boolean Boo.Lang.List`1<System.Object>::Contains(T)
extern "C"  bool List_1_Contains_m1078115507_gshared (List_1_t2975039247 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void Boo.Lang.List`1<System.Object>::.ctor(System.Collections.IEnumerable)
extern "C"  void List_1__ctor_m3036154082_gshared (List_1_t2975039247 * __this, RuntimeObject* p0, const RuntimeMethod* method);

// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m1478954267 (StringBuilder_t2947296310 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Object)
extern "C"  StringBuilder_t2947296310 * StringBuilder_Append_m2113509404 (StringBuilder_t2947296310 * __this, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t2947296310 * StringBuilder_Append_m4003059777 (StringBuilder_t2947296310 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString()
extern "C"  String_t* StringBuilder_ToString_m3471081727 (StringBuilder_t2947296310 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object Boo.Lang.Runtime.DynamicDispatching.Dispatcher::Invoke(System.Object,System.Object[])
extern "C"  RuntimeObject * Dispatcher_Invoke_m1905279945 (Dispatcher_t55733337 * __this, RuntimeObject * ___target0, ObjectU5BU5D_t3384890222* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m990968439 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
#define Dictionary_2__ctor_m2520180731(__this, p0, method) ((  void (*) (Dictionary_2_t4130252518 *, RuntimeObject*, const RuntimeMethod*))Dictionary_2__ctor_m229984624_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3549283624(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t4130252518 *, DispatcherKey_t3620541735 *, Dispatcher_t55733337 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m2098810262_gshared)(__this, p0, p1, method)
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m1645019632 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::Invoke()
extern "C"  Dispatcher_t55733337 * DispatcherFactory_Invoke_m2201777297 (DispatcherFactory_t2726905719 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>::Add(!0,!1)
#define Dictionary_2_Add_m2522272636(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4130252518 *, DispatcherKey_t3620541735 *, Dispatcher_t55733337 *, const RuntimeMethod*))Dictionary_2_Add_m1598188702_gshared)(__this, p0, p1, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m4123057206 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer::.ctor()
extern "C"  void _EqualityComparer__ctor_m198146823 (_EqualityComparer_t4027696990 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::GetHashCode()
extern "C"  int32_t String_GetHashCode_m2872400450 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m2569768297 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.List`1<System.Reflection.MemberInfo>::.ctor()
#define List_1__ctor_m70727746(__this, method) ((  void (*) (List_1_t3801035620 *, const RuntimeMethod*))List_1__ctor_m2096670811_gshared)(__this, method)
// Boo.Lang.List`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::CopyExtensions()
extern "C"  List_1_t3801035620 * ExtensionRegistry_CopyExtensions_m3871346462 (ExtensionRegistry_t4000900622 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.List`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::AddExtensionMembers(Boo.Lang.List`1<System.Reflection.MemberInfo>,System.Type)
extern "C"  List_1_t3801035620 * ExtensionRegistry_AddExtensionMembers_m3338094051 (RuntimeObject * __this /* static, unused */, List_1_t3801035620 * ___extensions0, Type_t * ___type1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1145571015 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3928229644  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.MemberInfo,System.Type)
extern "C"  bool Attribute_IsDefined_m2961780120 (RuntimeObject * __this /* static, unused */, MemberInfo_t * p0, Type_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.List`1<System.Reflection.MemberInfo>::Contains(T)
#define List_1_Contains_m737308532(__this, p0, method) ((  bool (*) (List_1_t3801035620 *, MemberInfo_t *, const RuntimeMethod*))List_1_Contains_m1078115507_gshared)(__this, p0, method)
// System.Void Boo.Lang.List`1<System.Reflection.MemberInfo>::.ctor(System.Collections.IEnumerable)
#define List_1__ctor_m2248684867(__this, p0, method) ((  void (*) (List_1_t3801035620 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m3036154082_gshared)(__this, p0, method)
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::.ctor()
extern "C"  void DispatcherCache__ctor_m4089538556 (DispatcherCache_t37386862 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.ExtensionRegistry::.ctor()
extern "C"  void ExtensionRegistry__ctor_m2256012551 (ExtensionRegistry_t4000900622 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.ExtensionRegistry::Register(System.Type)
extern "C"  void ExtensionRegistry_Register_m3445952763 (ExtensionRegistry_t4000900622 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m4220726354 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::.ctor(System.Type,System.String,System.Type[])
extern "C"  void DispatcherKey__ctor_m3788284358 (DispatcherKey_t3620541735 * __this, Type_t * ___type0, String_t* ___name1, TypeU5BU5D_t1484232934* ___arguments2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::Get(Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory)
extern "C"  Dispatcher_t55733337 * DispatcherCache_Get_m1017620631 (DispatcherCache_t37386862 * __this, DispatcherKey_t3620541735 * ___key0, DispatcherFactory_t2726905719 * ___factory1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::.ctor()
extern "C"  void U3CCoerceU3Ec__AnonStorey1D__ctor_m2923968775 (U3CCoerceU3Ec__AnonStorey1D_t2894686503 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void DispatcherFactory__ctor_m1281786830 (DispatcherFactory_t2726905719 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::GetDispatcher(System.Object,System.String,System.Type[],Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory)
extern "C"  Dispatcher_t55733337 * RuntimeServices_GetDispatcher_m393801607 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___target0, String_t* ___cacheKeyName1, TypeU5BU5D_t1484232934* ___cacheKeyTypes2, DispatcherFactory_t2726905719 * ___factory3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.DynamicDispatching.Dispatcher::.ctor(System.Object,System.IntPtr)
extern "C"  void Dispatcher__ctor_m2618689193 (Dispatcher_t55733337 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsPromotableNumeric(System.Type)
extern "C"  bool RuntimeServices_IsPromotableNumeric_m2143124046 (RuntimeObject * __this /* static, unused */, Type_t * ___fromType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::EmitPromotionDispatcher(System.Type,System.Type)
extern "C"  Dispatcher_t55733337 * RuntimeServices_EmitPromotionDispatcher_m363477410 (RuntimeObject * __this /* static, unused */, Type_t * ___fromType0, Type_t * ___toType1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices::FindImplicitConversionOperator(System.Type,System.Type)
extern "C"  MethodInfo_t * RuntimeServices_FindImplicitConversionOperator_m4110355163 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::EmitImplicitConversionDispatcher(System.Reflection.MethodInfo)
extern "C"  Dispatcher_t55733337 * RuntimeServices_EmitImplicitConversionDispatcher_m932860437 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___method0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.TypeCode System.Type::GetTypeCode(System.Type)
extern "C"  int32_t Type_GetTypeCode_m1876906287 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3729018088 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t3384890222* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Type::GetMethod(System.String)
extern "C"  MethodInfo_t * Type_GetMethod_m3054148569 (Type_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Reflection.MethodInfo)
extern "C"  Delegate_t69892740 * Delegate_CreateDelegate_m2493700642 (RuntimeObject * __this /* static, unused */, Type_t * p0, MethodInfo_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsPromotableNumeric(System.TypeCode)
extern "C"  bool RuntimeServices_IsPromotableNumeric_m1958702208 (RuntimeObject * __this /* static, unused */, int32_t ___code0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E::.ctor()
extern "C"  void U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E__ctor_m1755829606 (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ApplicationException::.ctor(System.String)
extern "C"  void ApplicationException__ctor_m1526084798 (ApplicationException_t3096800207 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> Boo.Lang.Runtime.TextReaderEnumerator::lines(System.IO.TextReader)
extern "C"  RuntimeObject* TextReaderEnumerator_lines_m3632397483 (RuntimeObject * __this /* static, unused */, TextReader_t2214828068 * ___reader0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m3706473663 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsNumeric(System.TypeCode)
extern "C"  bool RuntimeServices_IsNumeric_m1190125561 (RuntimeObject * __this /* static, unused */, int32_t ___code0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.TypeCode,System.Object,System.TypeCode)
extern "C"  bool RuntimeServices_EqualityOperator_m2285347940 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___lhs0, int32_t ___lhsTypeCode1, RuntimeObject * ___rhs2, int32_t ___rhsTypeCode3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::ArrayEqualityImpl(System.Array,System.Array)
extern "C"  bool RuntimeServices_ArrayEqualityImpl_m3534544265 (RuntimeObject * __this /* static, unused */, RuntimeArray * ___lhs0, RuntimeArray * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Rank()
extern "C"  int32_t Array_get_Rank_m254332292 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1720950498 (ArgumentException_t3493207885 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m3017877312 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array::GetValue(System.Int32)
extern "C"  RuntimeObject * Array_GetValue_m3784019948 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.Object)
extern "C"  bool RuntimeServices_EqualityOperator_m210483301 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___lhs0, RuntimeObject * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.TypeCode Boo.Lang.Runtime.RuntimeServices::GetConvertTypeCode(System.TypeCode,System.TypeCode)
extern "C"  int32_t RuntimeServices_GetConvertTypeCode_m1652112802 (RuntimeObject * __this /* static, unused */, int32_t ___lhsTypeCode0, int32_t ___rhsTypeCode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_Equality(System.Decimal,System.Decimal)
extern "C"  bool Decimal_op_Equality_m3698431823 (RuntimeObject * __this /* static, unused */, Decimal_t2663171440  p0, Decimal_t2663171440  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices::FindImplicitConversionMethod(System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>,System.Type,System.Type)
extern "C"  MethodInfo_t * RuntimeServices_FindImplicitConversionMethod_m3016211815 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___candidates0, Type_t * ___from1, Type_t * ___to2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo> Boo.Lang.Runtime.RuntimeServices::GetExtensionMethods()
extern "C"  RuntimeObject* RuntimeServices_GetExtensionMethods_m4000040295 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::.ctor()
extern "C"  void U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m1649374608 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::CreateCoerceDispatcher(System.Object,System.Type)
extern "C"  Dispatcher_t55733337 * RuntimeServices_CreateCoerceDispatcher_m4246784321 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, Type_t * ___toType1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern "C"  RuntimeObject * MethodBase_Invoke_m3234672952 (MethodBase_t1640104494 * __this, RuntimeObject * p0, ObjectU5BU5D_t3384890222* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo> Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.Generic.IEnumerable<System.Reflection.MethodInfo>.GetEnumerator()
extern "C"  RuntimeObject* U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m4098321379 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::CompareExchange(System.Int32&,System.Int32,System.Int32)
extern "C"  int32_t Interlocked_CompareExchange_m540286148 (RuntimeObject * __this /* static, unused */, int32_t* p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::get_Extensions()
extern "C"  RuntimeObject* ExtensionRegistry_get_Extensions_m4200656435 (ExtensionRegistry_t4000900622 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m4139731146 (NotSupportedException_t3527988452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::.ctor()
extern "C"  void U3ClinesU3Ec__IteratorD__ctor_m213053513 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  RuntimeObject* U3ClinesU3Ec__IteratorD_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3174056700 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Boo.Lang.Builtins::join(System.Collections.IEnumerable,System.String)
extern "C"  String_t* Builtins_join_m2871937356 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___enumerable0, String_t* ___separator1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builtins_join_m2871937356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t2947296310 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t2947296310 * L_0 = (StringBuilder_t2947296310 *)il2cpp_codegen_object_new(StringBuilder_t2947296310_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1478954267(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		RuntimeObject* L_1 = ___enumerable0;
		NullCheck(L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2201844525_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
		RuntimeObject* L_3 = V_1;
		V_2 = ((RuntimeObject*)IsInst((RuntimeObject*)L_3, IDisposable_t1054406163_il2cpp_TypeInfo_var));
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject* L_4 = V_1;
			NullCheck(L_4);
			bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_4);
			if (!L_5)
			{
				goto IL_0051;
			}
		}

IL_001f:
		{
			StringBuilder_t2947296310 * L_6 = V_0;
			RuntimeObject* L_7 = V_1;
			NullCheck(L_7);
			RuntimeObject * L_8 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_7);
			NullCheck(L_6);
			StringBuilder_Append_m2113509404(L_6, L_8, /*hidden argument*/NULL);
			goto IL_0046;
		}

IL_0031:
		{
			StringBuilder_t2947296310 * L_9 = V_0;
			String_t* L_10 = ___separator1;
			NullCheck(L_9);
			StringBuilder_Append_m4003059777(L_9, L_10, /*hidden argument*/NULL);
			StringBuilder_t2947296310 * L_11 = V_0;
			RuntimeObject* L_12 = V_1;
			NullCheck(L_12);
			RuntimeObject * L_13 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_12);
			NullCheck(L_11);
			StringBuilder_Append_m2113509404(L_11, L_13, /*hidden argument*/NULL);
		}

IL_0046:
		{
			RuntimeObject* L_14 = V_1;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0031;
			}
		}

IL_0051:
		{
			IL2CPP_LEAVE(0x63, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_16 = V_2;
			if (!L_16)
			{
				goto IL_0062;
			}
		}

IL_005c:
		{
			RuntimeObject* L_17 = V_2;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1054406163_il2cpp_TypeInfo_var, L_17);
		}

IL_0062:
		{
			IL2CPP_END_FINALLY(86)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_0063:
	{
		StringBuilder_t2947296310 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = StringBuilder_ToString_m3471081727(L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.Dispatcher::.ctor(System.Object,System.IntPtr)
extern "C"  void Dispatcher__ctor_m2618689193 (Dispatcher_t55733337 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object Boo.Lang.Runtime.DynamicDispatching.Dispatcher::Invoke(System.Object,System.Object[])
extern "C"  RuntimeObject * Dispatcher_Invoke_m1905279945 (Dispatcher_t55733337 * __this, RuntimeObject * ___target0, ObjectU5BU5D_t3384890222* ___args1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Dispatcher_Invoke_m1905279945((Dispatcher_t55733337 *)__this->get_prev_9(),___target0, ___args1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___target0, ObjectU5BU5D_t3384890222* ___args1, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___target0, ___args1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, RuntimeObject * ___target0, ObjectU5BU5D_t3384890222* ___args1, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___target0, ___args1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, ObjectU5BU5D_t3384890222* ___args1, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___target0, ___args1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Boo.Lang.Runtime.DynamicDispatching.Dispatcher::BeginInvoke(System.Object,System.Object[],System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Dispatcher_BeginInvoke_m1516098481 (Dispatcher_t55733337 * __this, RuntimeObject * ___target0, ObjectU5BU5D_t3384890222* ___args1, AsyncCallback_t606388952 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___target0;
	__d_args[1] = ___args1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Object Boo.Lang.Runtime.DynamicDispatching.Dispatcher::EndInvoke(System.IAsyncResult)
extern "C"  RuntimeObject * Dispatcher_EndInvoke_m3275239858 (Dispatcher_t55733337 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::.ctor()
extern "C"  void DispatcherCache__ctor_m4089538556 (DispatcherCache_t37386862 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::.cctor()
extern "C"  void DispatcherCache__cctor_m85627468 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DispatcherCache__cctor_m85627468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DispatcherKey_t3620541735_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((DispatcherKey_t3620541735_StaticFields*)il2cpp_codegen_static_fields_for(DispatcherKey_t3620541735_il2cpp_TypeInfo_var))->get_EqualityComparer_0();
		Dictionary_2_t4130252518 * L_1 = (Dictionary_2_t4130252518 *)il2cpp_codegen_object_new(Dictionary_2_t4130252518_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2520180731(L_1, L_0, /*hidden argument*/Dictionary_2__ctor_m2520180731_RuntimeMethod_var);
		((DispatcherCache_t37386862_StaticFields*)il2cpp_codegen_static_fields_for(DispatcherCache_t37386862_il2cpp_TypeInfo_var))->set__cache_0(L_1);
		return;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::Get(Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory)
extern "C"  Dispatcher_t55733337 * DispatcherCache_Get_m1017620631 (DispatcherCache_t37386862 * __this, DispatcherKey_t3620541735 * ___key0, DispatcherFactory_t2726905719 * ___factory1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DispatcherCache_Get_m1017620631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dispatcher_t55733337 * V_0 = NULL;
	Dictionary_2_t4130252518 * V_1 = NULL;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(DispatcherCache_t37386862_il2cpp_TypeInfo_var);
		Dictionary_2_t4130252518 * L_0 = ((DispatcherCache_t37386862_StaticFields*)il2cpp_codegen_static_fields_for(DispatcherCache_t37386862_il2cpp_TypeInfo_var))->get__cache_0();
		DispatcherKey_t3620541735 * L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m3549283624(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3549283624_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DispatcherCache_t37386862_il2cpp_TypeInfo_var);
		Dictionary_2_t4130252518 * L_3 = ((DispatcherCache_t37386862_StaticFields*)il2cpp_codegen_static_fields_for(DispatcherCache_t37386862_il2cpp_TypeInfo_var))->get__cache_0();
		V_1 = L_3;
		Dictionary_2_t4130252518 * L_4 = V_1;
		Monitor_Enter_m1645019632(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(DispatcherCache_t37386862_il2cpp_TypeInfo_var);
			Dictionary_2_t4130252518 * L_5 = ((DispatcherCache_t37386862_StaticFields*)il2cpp_codegen_static_fields_for(DispatcherCache_t37386862_il2cpp_TypeInfo_var))->get__cache_0();
			DispatcherKey_t3620541735 * L_6 = ___key0;
			NullCheck(L_5);
			bool L_7 = Dictionary_2_TryGetValue_m3549283624(L_5, L_6, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3549283624_RuntimeMethod_var);
			if (L_7)
			{
				goto IL_0043;
			}
		}

IL_0030:
		{
			DispatcherFactory_t2726905719 * L_8 = ___factory1;
			NullCheck(L_8);
			Dispatcher_t55733337 * L_9 = DispatcherFactory_Invoke_m2201777297(L_8, /*hidden argument*/NULL);
			V_0 = L_9;
			IL2CPP_RUNTIME_CLASS_INIT(DispatcherCache_t37386862_il2cpp_TypeInfo_var);
			Dictionary_2_t4130252518 * L_10 = ((DispatcherCache_t37386862_StaticFields*)il2cpp_codegen_static_fields_for(DispatcherCache_t37386862_il2cpp_TypeInfo_var))->get__cache_0();
			DispatcherKey_t3620541735 * L_11 = ___key0;
			Dispatcher_t55733337 * L_12 = V_0;
			NullCheck(L_10);
			Dictionary_2_Add_m2522272636(L_10, L_11, L_12, /*hidden argument*/Dictionary_2_Add_m2522272636_RuntimeMethod_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		Dictionary_2_t4130252518 * L_13 = V_1;
		Monitor_Exit_m4123057206(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_004f:
	{
		Dispatcher_t55733337 * L_14 = V_0;
		return L_14;
	}
}
extern "C"  Dispatcher_t55733337 * DelegatePInvokeWrapper_DispatcherFactory_t2726905719 (DispatcherFactory_t2726905719 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DelegatePInvokeWrapper_DispatcherFactory_t2726905719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	typedef Il2CppMethodPointer (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	Il2CppMethodPointer returnValue = il2cppPInvokeFunc();

	// Marshaling of return value back from native representation
	Dispatcher_t55733337 * _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_function_ptr_to_delegate<Dispatcher_t55733337>(returnValue, Dispatcher_t55733337_il2cpp_TypeInfo_var);

	return _returnValue_unmarshaled;
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void DispatcherFactory__ctor_m1281786830 (DispatcherFactory_t2726905719 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::Invoke()
extern "C"  Dispatcher_t55733337 * DispatcherFactory_Invoke_m2201777297 (DispatcherFactory_t2726905719 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DispatcherFactory_Invoke_m2201777297((DispatcherFactory_t2726905719 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Dispatcher_t55733337 * (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Dispatcher_t55733337 * (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* DispatcherFactory_BeginInvoke_m2759691272 (DispatcherFactory_t2726905719 * __this, AsyncCallback_t606388952 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (RuntimeObject*)___object1);
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory::EndInvoke(System.IAsyncResult)
extern "C"  Dispatcher_t55733337 * DispatcherFactory_EndInvoke_m220187180 (DispatcherFactory_t2726905719 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Dispatcher_t55733337 *)__result;
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::.ctor(System.Type,System.String,System.Type[])
extern "C"  void DispatcherKey__ctor_m3788284358 (DispatcherKey_t3620541735 * __this, Type_t * ___type0, String_t* ___name1, TypeU5BU5D_t1484232934* ___arguments2, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___type0;
		__this->set__type_1(L_0);
		String_t* L_1 = ___name1;
		__this->set__name_2(L_1);
		TypeU5BU5D_t1484232934* L_2 = ___arguments2;
		__this->set__arguments_3(L_2);
		return;
	}
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::.cctor()
extern "C"  void DispatcherKey__cctor_m1717898894 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DispatcherKey__cctor_m1717898894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		_EqualityComparer_t4027696990 * L_0 = (_EqualityComparer_t4027696990 *)il2cpp_codegen_object_new(_EqualityComparer_t4027696990_il2cpp_TypeInfo_var);
		_EqualityComparer__ctor_m198146823(L_0, /*hidden argument*/NULL);
		((DispatcherKey_t3620541735_StaticFields*)il2cpp_codegen_static_fields_for(DispatcherKey_t3620541735_il2cpp_TypeInfo_var))->set_EqualityComparer_0(L_0);
		return;
	}
}
// System.Void Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer::.ctor()
extern "C"  void _EqualityComparer__ctor_m198146823 (_EqualityComparer_t4027696990 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer::GetHashCode(Boo.Lang.Runtime.DynamicDispatching.DispatcherKey)
extern "C"  int32_t _EqualityComparer_GetHashCode_m437555421 (_EqualityComparer_t4027696990 * __this, DispatcherKey_t3620541735 * ___key0, const RuntimeMethod* method)
{
	{
		DispatcherKey_t3620541735 * L_0 = ___key0;
		NullCheck(L_0);
		Type_t * L_1 = L_0->get__type_1();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Type::GetHashCode() */, L_1);
		DispatcherKey_t3620541735 * L_3 = ___key0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get__name_2();
		NullCheck(L_4);
		int32_t L_5 = String_GetHashCode_m2872400450(L_4, /*hidden argument*/NULL);
		DispatcherKey_t3620541735 * L_6 = ___key0;
		NullCheck(L_6);
		TypeU5BU5D_t1484232934* L_7 = L_6->get__arguments_3();
		NullCheck(L_7);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_2^(int32_t)L_5))^(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))));
	}
}
// System.Boolean Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer::Equals(Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.DispatcherKey)
extern "C"  bool _EqualityComparer_Equals_m1474669999 (_EqualityComparer_t4027696990 * __this, DispatcherKey_t3620541735 * ___x0, DispatcherKey_t3620541735 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_EqualityComparer_Equals_m1474669999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		DispatcherKey_t3620541735 * L_0 = ___x0;
		NullCheck(L_0);
		Type_t * L_1 = L_0->get__type_1();
		DispatcherKey_t3620541735 * L_2 = ___y1;
		NullCheck(L_2);
		Type_t * L_3 = L_2->get__type_1();
		if ((((RuntimeObject*)(Type_t *)L_1) == ((RuntimeObject*)(Type_t *)L_3)))
		{
			goto IL_0013;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		DispatcherKey_t3620541735 * L_4 = ___x0;
		NullCheck(L_4);
		TypeU5BU5D_t1484232934* L_5 = L_4->get__arguments_3();
		NullCheck(L_5);
		DispatcherKey_t3620541735 * L_6 = ___y1;
		NullCheck(L_6);
		TypeU5BU5D_t1484232934* L_7 = L_6->get__arguments_3();
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))))))
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		DispatcherKey_t3620541735 * L_8 = ___x0;
		NullCheck(L_8);
		String_t* L_9 = L_8->get__name_2();
		DispatcherKey_t3620541735 * L_10 = ___y1;
		NullCheck(L_10);
		String_t* L_11 = L_10->get__name_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Inequality_m2569768297(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0042;
		}
	}
	{
		return (bool)0;
	}

IL_0042:
	{
		V_0 = 0;
		goto IL_0064;
	}

IL_0049:
	{
		DispatcherKey_t3620541735 * L_13 = ___x0;
		NullCheck(L_13);
		TypeU5BU5D_t1484232934* L_14 = L_13->get__arguments_3();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		DispatcherKey_t3620541735 * L_18 = ___y1;
		NullCheck(L_18);
		TypeU5BU5D_t1484232934* L_19 = L_18->get__arguments_3();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Type_t * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		if ((((RuntimeObject*)(Type_t *)L_17) == ((RuntimeObject*)(Type_t *)L_22)))
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_24 = V_0;
		DispatcherKey_t3620541735 * L_25 = ___x0;
		NullCheck(L_25);
		TypeU5BU5D_t1484232934* L_26 = L_25->get__arguments_3();
		NullCheck(L_26);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_26)->max_length)))))))
		{
			goto IL_0049;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void Boo.Lang.Runtime.ExtensionRegistry::.ctor()
extern "C"  void ExtensionRegistry__ctor_m2256012551 (ExtensionRegistry_t4000900622 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtensionRegistry__ctor_m2256012551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3801035620 * L_0 = (List_1_t3801035620 *)il2cpp_codegen_object_new(List_1_t3801035620_il2cpp_TypeInfo_var);
		List_1__ctor_m70727746(L_0, /*hidden argument*/List_1__ctor_m70727746_RuntimeMethod_var);
		__this->set__extensions_0(L_0);
		RuntimeObject * L_1 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m990968439(L_1, /*hidden argument*/NULL);
		__this->set__classLock_1(L_1);
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boo.Lang.Runtime.ExtensionRegistry::Register(System.Type)
extern "C"  void ExtensionRegistry_Register_m3445952763 (ExtensionRegistry_t4000900622 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject * L_0 = __this->get__classLock_1();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m1645019632(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		List_1_t3801035620 * L_2 = ExtensionRegistry_CopyExtensions_m3871346462(__this, /*hidden argument*/NULL);
		Type_t * L_3 = ___type0;
		List_1_t3801035620 * L_4 = ExtensionRegistry_AddExtensionMembers_m3338094051(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set__extensions_0(L_4);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		RuntimeObject * L_5 = V_0;
		Monitor_Exit_m4123057206(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::get_Extensions()
extern "C"  RuntimeObject* ExtensionRegistry_get_Extensions_m4200656435 (ExtensionRegistry_t4000900622 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3801035620 * L_0 = __this->get__extensions_0();
		return L_0;
	}
}
// Boo.Lang.List`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::AddExtensionMembers(Boo.Lang.List`1<System.Reflection.MemberInfo>,System.Type)
extern "C"  List_1_t3801035620 * ExtensionRegistry_AddExtensionMembers_m3338094051 (RuntimeObject * __this /* static, unused */, List_1_t3801035620 * ___extensions0, Type_t * ___type1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtensionRegistry_AddExtensionMembers_m3338094051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MemberInfo_t * V_0 = NULL;
	MemberInfoU5BU5D_t385114661* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Type_t * L_0 = ___type1;
		NullCheck(L_0);
		MemberInfoU5BU5D_t385114661* L_1 = VirtFuncInvoker1< MemberInfoU5BU5D_t385114661*, int32_t >::Invoke(47 /* System.Reflection.MemberInfo[] System.Type::GetMembers(System.Reflection.BindingFlags) */, L_0, ((int32_t)24));
		V_1 = L_1;
		V_2 = 0;
		goto IL_004b;
	}

IL_0010:
	{
		MemberInfoU5BU5D_t385114661* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		MemberInfo_t * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		MemberInfo_t * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1145571015(NULL /*static, unused*/, LoadTypeToken(ExtensionAttribute_t2158284563_0_0_0_var), /*hidden argument*/NULL);
		bool L_8 = Attribute_IsDefined_m2961780120(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_002e;
		}
	}
	{
		goto IL_0047;
	}

IL_002e:
	{
		List_1_t3801035620 * L_9 = ___extensions0;
		MemberInfo_t * L_10 = V_0;
		NullCheck(L_9);
		bool L_11 = List_1_Contains_m737308532(L_9, L_10, /*hidden argument*/List_1_Contains_m737308532_RuntimeMethod_var);
		if (!L_11)
		{
			goto IL_003f;
		}
	}
	{
		goto IL_0047;
	}

IL_003f:
	{
		List_1_t3801035620 * L_12 = ___extensions0;
		MemberInfo_t * L_13 = V_0;
		NullCheck(L_12);
		VirtFuncInvoker1< List_1_t3801035620 *, MemberInfo_t * >::Invoke(34 /* Boo.Lang.List`1<T> Boo.Lang.List`1<System.Reflection.MemberInfo>::Add(T) */, L_12, L_13);
	}

IL_0047:
	{
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_15 = V_2;
		MemberInfoU5BU5D_t385114661* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		List_1_t3801035620 * L_17 = ___extensions0;
		return L_17;
	}
}
// Boo.Lang.List`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::CopyExtensions()
extern "C"  List_1_t3801035620 * ExtensionRegistry_CopyExtensions_m3871346462 (ExtensionRegistry_t4000900622 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtensionRegistry_CopyExtensions_m3871346462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3801035620 * L_0 = __this->get__extensions_0();
		List_1_t3801035620 * L_1 = (List_1_t3801035620 *)il2cpp_codegen_object_new(List_1_t3801035620_il2cpp_TypeInfo_var);
		List_1__ctor_m2248684867(L_1, L_0, /*hidden argument*/List_1__ctor_m2248684867_RuntimeMethod_var);
		return L_1;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices::.cctor()
extern "C"  void RuntimeServices__cctor_m1557287055 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices__cctor_m1557287055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((RuntimeServices_t4253430258_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeServices_t4253430258_il2cpp_TypeInfo_var))->set_NoArguments_0(((ObjectU5BU5D_t3384890222*)SZArrayNew(ObjectU5BU5D_t3384890222_il2cpp_TypeInfo_var, (uint32_t)0)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1145571015(NULL /*static, unused*/, LoadTypeToken(RuntimeServices_t4253430258_0_0_0_var), /*hidden argument*/NULL);
		((RuntimeServices_t4253430258_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeServices_t4253430258_il2cpp_TypeInfo_var))->set_RuntimeServicesType_1(L_0);
		DispatcherCache_t37386862 * L_1 = (DispatcherCache_t37386862 *)il2cpp_codegen_object_new(DispatcherCache_t37386862_il2cpp_TypeInfo_var);
		DispatcherCache__ctor_m4089538556(L_1, /*hidden argument*/NULL);
		((RuntimeServices_t4253430258_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeServices_t4253430258_il2cpp_TypeInfo_var))->set__cache_2(L_1);
		ExtensionRegistry_t4000900622 * L_2 = (ExtensionRegistry_t4000900622 *)il2cpp_codegen_object_new(ExtensionRegistry_t4000900622_il2cpp_TypeInfo_var);
		ExtensionRegistry__ctor_m2256012551(L_2, /*hidden argument*/NULL);
		((RuntimeServices_t4253430258_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeServices_t4253430258_il2cpp_TypeInfo_var))->set__extensions_3(L_2);
		bool L_3 = ((bool)1);
		RuntimeObject * L_4 = Box(Boolean_t362855854_il2cpp_TypeInfo_var, &L_3);
		((RuntimeServices_t4253430258_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeServices_t4253430258_il2cpp_TypeInfo_var))->set_True_4(L_4);
		return;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices::RegisterExtensions(System.Type)
extern "C"  void RuntimeServices_RegisterExtensions_m2899472418 (RuntimeObject * __this /* static, unused */, Type_t * ___extensions0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_RegisterExtensions_m2899472418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		ExtensionRegistry_t4000900622 * L_0 = ((RuntimeServices_t4253430258_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeServices_t4253430258_il2cpp_TypeInfo_var))->get__extensions_3();
		Type_t * L_1 = ___extensions0;
		NullCheck(L_0);
		ExtensionRegistry_Register_m3445952763(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::GetDispatcher(System.Object,System.String,System.Type[],Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory)
extern "C"  Dispatcher_t55733337 * RuntimeServices_GetDispatcher_m393801607 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___target0, String_t* ___cacheKeyName1, TypeU5BU5D_t1484232934* ___cacheKeyTypes2, DispatcherFactory_t2726905719 * ___factory3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_GetDispatcher_m393801607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	DispatcherKey_t3620541735 * V_1 = NULL;
	Type_t * G_B2_0 = NULL;
	Type_t * G_B1_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		Type_t * L_1 = ((Type_t *)IsInstClass((RuntimeObject*)L_0, Type_t_il2cpp_TypeInfo_var));
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0013;
		}
	}
	{
		RuntimeObject * L_2 = ___target0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m4220726354(L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
	}

IL_0013:
	{
		V_0 = G_B2_0;
		Type_t * L_4 = V_0;
		String_t* L_5 = ___cacheKeyName1;
		TypeU5BU5D_t1484232934* L_6 = ___cacheKeyTypes2;
		DispatcherKey_t3620541735 * L_7 = (DispatcherKey_t3620541735 *)il2cpp_codegen_object_new(DispatcherKey_t3620541735_il2cpp_TypeInfo_var);
		DispatcherKey__ctor_m3788284358(L_7, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		DispatcherCache_t37386862 * L_8 = ((RuntimeServices_t4253430258_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeServices_t4253430258_il2cpp_TypeInfo_var))->get__cache_2();
		DispatcherKey_t3620541735 * L_9 = V_1;
		DispatcherFactory_t2726905719 * L_10 = ___factory3;
		NullCheck(L_8);
		Dispatcher_t55733337 * L_11 = DispatcherCache_Get_m1017620631(L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices::Coerce(System.Object,System.Type)
extern "C"  RuntimeObject * RuntimeServices_Coerce_m2787646698 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, Type_t * ___toType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_Coerce_m2787646698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3384890222* V_0 = NULL;
	Dispatcher_t55733337 * V_1 = NULL;
	U3CCoerceU3Ec__AnonStorey1D_t2894686503 * V_2 = NULL;
	{
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_0 = (U3CCoerceU3Ec__AnonStorey1D_t2894686503 *)il2cpp_codegen_object_new(U3CCoerceU3Ec__AnonStorey1D_t2894686503_il2cpp_TypeInfo_var);
		U3CCoerceU3Ec__AnonStorey1D__ctor_m2923968775(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_1 = V_2;
		RuntimeObject * L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_3 = V_2;
		Type_t * L_4 = ___toType1;
		NullCheck(L_3);
		L_3->set_toType_1(L_4);
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_5 = V_2;
		NullCheck(L_5);
		RuntimeObject * L_6 = L_5->get_value_0();
		if (L_6)
		{
			goto IL_0021;
		}
	}
	{
		return NULL;
	}

IL_0021:
	{
		ObjectU5BU5D_t3384890222* L_7 = ((ObjectU5BU5D_t3384890222*)SZArrayNew(ObjectU5BU5D_t3384890222_il2cpp_TypeInfo_var, (uint32_t)1));
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_8 = V_2;
		NullCheck(L_8);
		Type_t * L_9 = L_8->get_toType_1();
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_9);
		V_0 = L_7;
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_10 = V_2;
		NullCheck(L_10);
		RuntimeObject * L_11 = L_10->get_value_0();
		TypeU5BU5D_t1484232934* L_12 = ((TypeU5BU5D_t1484232934*)SZArrayNew(TypeU5BU5D_t1484232934_il2cpp_TypeInfo_var, (uint32_t)1));
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_13 = V_2;
		NullCheck(L_13);
		Type_t * L_14 = L_13->get_toType_1();
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_14);
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_15 = V_2;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m1840774687_RuntimeMethod_var);
		DispatcherFactory_t2726905719 * L_17 = (DispatcherFactory_t2726905719 *)il2cpp_codegen_object_new(DispatcherFactory_t2726905719_il2cpp_TypeInfo_var);
		DispatcherFactory__ctor_m1281786830(L_17, L_15, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		Dispatcher_t55733337 * L_18 = RuntimeServices_GetDispatcher_m393801607(NULL /*static, unused*/, L_11, _stringLiteral790103786, L_12, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		Dispatcher_t55733337 * L_19 = V_1;
		U3CCoerceU3Ec__AnonStorey1D_t2894686503 * L_20 = V_2;
		NullCheck(L_20);
		RuntimeObject * L_21 = L_20->get_value_0();
		ObjectU5BU5D_t3384890222* L_22 = V_0;
		NullCheck(L_19);
		RuntimeObject * L_23 = Dispatcher_Invoke_m1905279945(L_19, L_21, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::CreateCoerceDispatcher(System.Object,System.Type)
extern "C"  Dispatcher_t55733337 * RuntimeServices_CreateCoerceDispatcher_m4246784321 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, Type_t * ___toType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_CreateCoerceDispatcher_m4246784321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	{
		Type_t * L_0 = ___toType1;
		RuntimeObject * L_1 = ___value0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(41 /* System.Boolean System.Type::IsInstanceOfType(System.Object) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)RuntimeServices_IdentityDispatcher_m2167579733_RuntimeMethod_var);
		Dispatcher_t55733337 * L_4 = (Dispatcher_t55733337 *)il2cpp_codegen_object_new(Dispatcher_t55733337_il2cpp_TypeInfo_var);
		Dispatcher__ctor_m2618689193(L_4, NULL, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0019:
	{
		RuntimeObject * L_5 = ___value0;
		if (!((RuntimeObject*)IsInst((RuntimeObject*)L_5, ICoercible_t1994556652_il2cpp_TypeInfo_var)))
		{
			goto IL_0031;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)RuntimeServices_CoercibleDispatcher_m286273110_RuntimeMethod_var);
		Dispatcher_t55733337 * L_7 = (Dispatcher_t55733337 *)il2cpp_codegen_object_new(Dispatcher_t55733337_il2cpp_TypeInfo_var);
		Dispatcher__ctor_m2618689193(L_7, NULL, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0031:
	{
		RuntimeObject * L_8 = ___value0;
		NullCheck(L_8);
		Type_t * L_9 = Object_GetType_m4220726354(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Type_t * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		bool L_11 = RuntimeServices_IsPromotableNumeric_m2143124046(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0056;
		}
	}
	{
		Type_t * L_12 = ___toType1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		bool L_13 = RuntimeServices_IsPromotableNumeric_m2143124046(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0056;
		}
	}
	{
		Type_t * L_14 = V_0;
		Type_t * L_15 = ___toType1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		Dispatcher_t55733337 * L_16 = RuntimeServices_EmitPromotionDispatcher_m363477410(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}

IL_0056:
	{
		Type_t * L_17 = V_0;
		Type_t * L_18 = ___toType1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		MethodInfo_t * L_19 = RuntimeServices_FindImplicitConversionOperator_m4110355163(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		MethodInfo_t * L_20 = V_1;
		if (L_20)
		{
			goto IL_0071;
		}
	}
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)RuntimeServices_IdentityDispatcher_m2167579733_RuntimeMethod_var);
		Dispatcher_t55733337 * L_22 = (Dispatcher_t55733337 *)il2cpp_codegen_object_new(Dispatcher_t55733337_il2cpp_TypeInfo_var);
		Dispatcher__ctor_m2618689193(L_22, NULL, L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_0071:
	{
		MethodInfo_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		Dispatcher_t55733337 * L_24 = RuntimeServices_EmitImplicitConversionDispatcher_m932860437(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::EmitPromotionDispatcher(System.Type,System.Type)
extern "C"  Dispatcher_t55733337 * RuntimeServices_EmitPromotionDispatcher_m363477410 (RuntimeObject * __this /* static, unused */, Type_t * ___fromType0, Type_t * ___toType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_EmitPromotionDispatcher_m363477410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1145571015(NULL /*static, unused*/, LoadTypeToken(Dispatcher_t55733337_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1145571015(NULL /*static, unused*/, LoadTypeToken(NumericPromotions_t1087937753_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t3384890222* L_2 = ((ObjectU5BU5D_t3384890222*)SZArrayNew(ObjectU5BU5D_t3384890222_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral2546639394);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2546639394);
		ObjectU5BU5D_t3384890222* L_3 = L_2;
		Type_t * L_4 = ___fromType0;
		int32_t L_5 = Type_GetTypeCode_m1876906287(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(TypeCode_t100547427_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3384890222* L_8 = L_3;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral1198481194);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1198481194);
		ObjectU5BU5D_t3384890222* L_9 = L_8;
		Type_t * L_10 = ___toType1;
		int32_t L_11 = Type_GetTypeCode_m1876906287(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(TypeCode_t100547427_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3729018088(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_1);
		MethodInfo_t * L_15 = Type_GetMethod_m3054148569(L_1, L_14, /*hidden argument*/NULL);
		Delegate_t69892740 * L_16 = Delegate_CreateDelegate_m2493700642(NULL /*static, unused*/, L_0, L_15, /*hidden argument*/NULL);
		return ((Dispatcher_t55733337 *)CastclassSealed((RuntimeObject*)L_16, Dispatcher_t55733337_il2cpp_TypeInfo_var));
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsPromotableNumeric(System.Type)
extern "C"  bool RuntimeServices_IsPromotableNumeric_m2143124046 (RuntimeObject * __this /* static, unused */, Type_t * ___fromType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_IsPromotableNumeric_m2143124046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___fromType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_1 = Type_GetTypeCode_m1876906287(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		bool L_2 = RuntimeServices_IsPromotableNumeric_m1958702208(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices::EmitImplicitConversionDispatcher(System.Reflection.MethodInfo)
extern "C"  Dispatcher_t55733337 * RuntimeServices_EmitImplicitConversionDispatcher_m932860437 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___method0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_EmitImplicitConversionDispatcher_m932860437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927 * V_0 = NULL;
	{
		U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927 * L_0 = (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927 *)il2cpp_codegen_object_new(U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927_il2cpp_TypeInfo_var);
		U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E__ctor_m1755829606(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927 * L_1 = V_0;
		MethodInfo_t * L_2 = ___method0;
		NullCheck(L_1);
		L_1->set_method_0(L_2);
		U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m681769745_RuntimeMethod_var);
		Dispatcher_t55733337 * L_5 = (Dispatcher_t55733337 *)il2cpp_codegen_object_new(Dispatcher_t55733337_il2cpp_TypeInfo_var);
		Dispatcher__ctor_m2618689193(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices::CoercibleDispatcher(System.Object,System.Object[])
extern "C"  RuntimeObject * RuntimeServices_CoercibleDispatcher_m286273110 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___o0, ObjectU5BU5D_t3384890222* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_CoercibleDispatcher_m286273110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___o0;
		ObjectU5BU5D_t3384890222* L_1 = ___args1;
		NullCheck(L_1);
		int32_t L_2 = 0;
		RuntimeObject * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICoercible_t1994556652_il2cpp_TypeInfo_var)));
		RuntimeObject * L_4 = InterfaceFuncInvoker1< RuntimeObject *, Type_t * >::Invoke(0 /* System.Object Boo.Lang.Runtime.ICoercible::Coerce(System.Type) */, ICoercible_t1994556652_il2cpp_TypeInfo_var, ((RuntimeObject*)Castclass((RuntimeObject*)L_0, ICoercible_t1994556652_il2cpp_TypeInfo_var)), ((Type_t *)CastclassClass((RuntimeObject*)L_3, Type_t_il2cpp_TypeInfo_var)));
		return L_4;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices::IdentityDispatcher(System.Object,System.Object[])
extern "C"  RuntimeObject * RuntimeServices_IdentityDispatcher_m2167579733 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___o0, ObjectU5BU5D_t3384890222* ___args1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___o0;
		return L_0;
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsNumeric(System.TypeCode)
extern "C"  bool RuntimeServices_IsNumeric_m1190125561 (RuntimeObject * __this /* static, unused */, int32_t ___code0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___code0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)5)))
		{
			case 0:
			{
				goto IL_003d;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_003f;
			}
			case 3:
			{
				goto IL_0045;
			}
			case 4:
			{
				goto IL_0041;
			}
			case 5:
			{
				goto IL_0047;
			}
			case 6:
			{
				goto IL_0043;
			}
			case 7:
			{
				goto IL_0049;
			}
			case 8:
			{
				goto IL_004b;
			}
			case 9:
			{
				goto IL_004d;
			}
			case 10:
			{
				goto IL_004f;
			}
		}
	}
	{
		goto IL_0051;
	}

IL_003b:
	{
		return (bool)1;
	}

IL_003d:
	{
		return (bool)1;
	}

IL_003f:
	{
		return (bool)1;
	}

IL_0041:
	{
		return (bool)1;
	}

IL_0043:
	{
		return (bool)1;
	}

IL_0045:
	{
		return (bool)1;
	}

IL_0047:
	{
		return (bool)1;
	}

IL_0049:
	{
		return (bool)1;
	}

IL_004b:
	{
		return (bool)1;
	}

IL_004d:
	{
		return (bool)1;
	}

IL_004f:
	{
		return (bool)1;
	}

IL_0051:
	{
		return (bool)0;
	}
}
// System.Collections.IEnumerable Boo.Lang.Runtime.RuntimeServices::GetEnumerable(System.Object)
extern "C"  RuntimeObject* RuntimeServices_GetEnumerable_m3919393360 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___enumerable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_GetEnumerable_m3919393360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	TextReader_t2214828068 * V_1 = NULL;
	{
		RuntimeObject * L_0 = ___enumerable0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ApplicationException_t3096800207 * L_1 = (ApplicationException_t3096800207 *)il2cpp_codegen_object_new(ApplicationException_t3096800207_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m1526084798(L_1, _stringLiteral3277810487, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		RuntimeObject * L_2 = ___enumerable0;
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_2, IEnumerable_t2201844525_il2cpp_TypeInfo_var));
		RuntimeObject* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0020;
		}
	}
	{
		RuntimeObject* L_4 = V_0;
		return L_4;
	}

IL_0020:
	{
		RuntimeObject * L_5 = ___enumerable0;
		V_1 = ((TextReader_t2214828068 *)IsInstClass((RuntimeObject*)L_5, TextReader_t2214828068_il2cpp_TypeInfo_var));
		TextReader_t2214828068 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		TextReader_t2214828068 * L_7 = V_1;
		RuntimeObject* L_8 = TextReaderEnumerator_lines_m3632397483(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0034:
	{
		ApplicationException_t3096800207 * L_9 = (ApplicationException_t3096800207 *)il2cpp_codegen_object_new(ApplicationException_t3096800207_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m1526084798(L_9, _stringLiteral3540775313, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}
}
// System.String Boo.Lang.Runtime.RuntimeServices::op_Addition(System.String,System.Object)
extern "C"  String_t* RuntimeServices_op_Addition_m1883231782 (RuntimeObject * __this /* static, unused */, String_t* ___lhs0, RuntimeObject * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_op_Addition_m1883231782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___lhs0;
		RuntimeObject * L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3706473663(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.Object)
extern "C"  bool RuntimeServices_EqualityOperator_m210483301 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___lhs0, RuntimeObject * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_EqualityOperator_m210483301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeArray * V_2 = NULL;
	RuntimeArray * V_3 = NULL;
	int32_t G_B15_0 = 0;
	{
		RuntimeObject * L_0 = ___lhs0;
		RuntimeObject * L_1 = ___rhs1;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_0) == ((RuntimeObject*)(RuntimeObject *)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		RuntimeObject * L_2 = ___lhs0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject * L_3 = ___rhs1;
		RuntimeObject * L_4 = ___lhs0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_3, L_4);
		return L_5;
	}

IL_0017:
	{
		RuntimeObject * L_6 = ___rhs1;
		if (L_6)
		{
			goto IL_0025;
		}
	}
	{
		RuntimeObject * L_7 = ___lhs0;
		RuntimeObject * L_8 = ___rhs1;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_7, L_8);
		return L_9;
	}

IL_0025:
	{
		RuntimeObject * L_10 = ___lhs0;
		NullCheck(L_10);
		Type_t * L_11 = Object_GetType_m4220726354(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_12 = Type_GetTypeCode_m1876906287(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		RuntimeObject * L_13 = ___rhs1;
		NullCheck(L_13);
		Type_t * L_14 = Object_GetType_m4220726354(L_13, /*hidden argument*/NULL);
		int32_t L_15 = Type_GetTypeCode_m1876906287(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		bool L_17 = RuntimeServices_IsNumeric_m1190125561(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		bool L_19 = RuntimeServices_IsNumeric_m1190125561(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_005d;
		}
	}
	{
		RuntimeObject * L_20 = ___lhs0;
		int32_t L_21 = V_0;
		RuntimeObject * L_22 = ___rhs1;
		int32_t L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		bool L_24 = RuntimeServices_EqualityOperator_m2285347940(NULL /*static, unused*/, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}

IL_005d:
	{
		RuntimeObject * L_25 = ___lhs0;
		V_2 = ((RuntimeArray *)IsInstClass((RuntimeObject*)L_25, RuntimeArray_il2cpp_TypeInfo_var));
		RuntimeArray * L_26 = V_2;
		if (!L_26)
		{
			goto IL_007f;
		}
	}
	{
		RuntimeObject * L_27 = ___rhs1;
		V_3 = ((RuntimeArray *)IsInstClass((RuntimeObject*)L_27, RuntimeArray_il2cpp_TypeInfo_var));
		RuntimeArray * L_28 = V_3;
		if (!L_28)
		{
			goto IL_007f;
		}
	}
	{
		RuntimeArray * L_29 = V_2;
		RuntimeArray * L_30 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		bool L_31 = RuntimeServices_ArrayEqualityImpl_m3534544265(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		return L_31;
	}

IL_007f:
	{
		RuntimeObject * L_32 = ___lhs0;
		RuntimeObject * L_33 = ___rhs1;
		NullCheck(L_32);
		bool L_34 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_32, L_33);
		if (L_34)
		{
			goto IL_0094;
		}
	}
	{
		RuntimeObject * L_35 = ___rhs1;
		RuntimeObject * L_36 = ___lhs0;
		NullCheck(L_35);
		bool L_37 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_35, L_36);
		G_B15_0 = ((int32_t)(L_37));
		goto IL_0095;
	}

IL_0094:
	{
		G_B15_0 = 1;
	}

IL_0095:
	{
		return (bool)G_B15_0;
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::ArrayEqualityImpl(System.Array,System.Array)
extern "C"  bool RuntimeServices_ArrayEqualityImpl_m3534544265 (RuntimeObject * __this /* static, unused */, RuntimeArray * ___lhs0, RuntimeArray * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_ArrayEqualityImpl_m3534544265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		RuntimeArray * L_0 = ___lhs0;
		NullCheck(L_0);
		int32_t L_1 = Array_get_Rank_m254332292(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		RuntimeArray * L_2 = ___rhs1;
		NullCheck(L_2);
		int32_t L_3 = Array_get_Rank_m254332292(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		ArgumentException_t3493207885 * L_4 = (ArgumentException_t3493207885 *)il2cpp_codegen_object_new(ArgumentException_t3493207885_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1720950498(L_4, _stringLiteral3151788631, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		RuntimeArray * L_5 = ___lhs0;
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m3017877312(L_5, /*hidden argument*/NULL);
		RuntimeArray * L_7 = ___rhs1;
		NullCheck(L_7);
		int32_t L_8 = Array_get_Length_m3017877312(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0036;
		}
	}
	{
		return (bool)0;
	}

IL_0036:
	{
		V_0 = 0;
		goto IL_005b;
	}

IL_003d:
	{
		RuntimeArray * L_9 = ___lhs0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		RuntimeObject * L_11 = Array_GetValue_m3784019948(L_9, L_10, /*hidden argument*/NULL);
		RuntimeArray * L_12 = ___rhs1;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		RuntimeObject * L_14 = Array_GetValue_m3784019948(L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		bool L_15 = RuntimeServices_EqualityOperator_m210483301(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0057;
		}
	}
	{
		return (bool)0;
	}

IL_0057:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		RuntimeArray * L_18 = ___lhs0;
		NullCheck(L_18);
		int32_t L_19 = Array_get_Length_m3017877312(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_003d;
		}
	}
	{
		return (bool)1;
	}
}
// System.TypeCode Boo.Lang.Runtime.RuntimeServices::GetConvertTypeCode(System.TypeCode,System.TypeCode)
extern "C"  int32_t RuntimeServices_GetConvertTypeCode_m1652112802 (RuntimeObject * __this /* static, unused */, int32_t ___lhsTypeCode0, int32_t ___rhsTypeCode1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___lhsTypeCode0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)15))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0013;
		}
	}

IL_0010:
	{
		return (int32_t)(((int32_t)15));
	}

IL_0013:
	{
		int32_t L_2 = ___lhsTypeCode0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)14))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0026;
		}
	}

IL_0023:
	{
		return (int32_t)(((int32_t)14));
	}

IL_0026:
	{
		int32_t L_4 = ___lhsTypeCode0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)13))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_5 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0039;
		}
	}

IL_0036:
	{
		return (int32_t)(((int32_t)13));
	}

IL_0039:
	{
		int32_t L_6 = ___lhsTypeCode0;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_7 = ___rhsTypeCode1;
		if ((((int32_t)L_7) == ((int32_t)5)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = ___rhsTypeCode1;
		if ((((int32_t)L_8) == ((int32_t)7)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_9 = ___rhsTypeCode1;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)9))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_10 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0062;
		}
	}

IL_005f:
	{
		return (int32_t)(((int32_t)11));
	}

IL_0062:
	{
		return (int32_t)(((int32_t)12));
	}

IL_0065:
	{
		int32_t L_11 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_12 = ___lhsTypeCode0;
		if ((((int32_t)L_12) == ((int32_t)5)))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_13 = ___lhsTypeCode0;
		if ((((int32_t)L_13) == ((int32_t)7)))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_14 = ___lhsTypeCode0;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)9))))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_15 = ___lhsTypeCode0;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_008e;
		}
	}

IL_008b:
	{
		return (int32_t)(((int32_t)11));
	}

IL_008e:
	{
		return (int32_t)(((int32_t)12));
	}

IL_0091:
	{
		int32_t L_16 = ___lhsTypeCode0;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)11))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_17 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00a4;
		}
	}

IL_00a1:
	{
		return (int32_t)(((int32_t)11));
	}

IL_00a4:
	{
		int32_t L_18 = ___lhsTypeCode0;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00c8;
		}
	}
	{
		int32_t L_19 = ___rhsTypeCode1;
		if ((((int32_t)L_19) == ((int32_t)5)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_20 = ___rhsTypeCode1;
		if ((((int32_t)L_20) == ((int32_t)7)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_21 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00c5;
		}
	}

IL_00c2:
	{
		return (int32_t)(((int32_t)11));
	}

IL_00c5:
	{
		return (int32_t)(((int32_t)10));
	}

IL_00c8:
	{
		int32_t L_22 = ___rhsTypeCode1;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_23 = ___lhsTypeCode0;
		if ((((int32_t)L_23) == ((int32_t)5)))
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_24 = ___lhsTypeCode0;
		if ((((int32_t)L_24) == ((int32_t)7)))
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_25 = ___lhsTypeCode0;
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00e9;
		}
	}

IL_00e6:
	{
		return (int32_t)(((int32_t)11));
	}

IL_00e9:
	{
		return (int32_t)(((int32_t)10));
	}

IL_00ec:
	{
		return (int32_t)(((int32_t)9));
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.TypeCode,System.Object,System.TypeCode)
extern "C"  bool RuntimeServices_EqualityOperator_m2285347940 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___lhs0, int32_t ___lhsTypeCode1, RuntimeObject * ___rhs2, int32_t ___rhsTypeCode3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_EqualityOperator_m2285347940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	int32_t V_2 = 0;
	{
		RuntimeObject * L_0 = ___lhs0;
		V_0 = ((RuntimeObject*)Castclass((RuntimeObject*)L_0, IConvertible_t2722588824_il2cpp_TypeInfo_var));
		RuntimeObject * L_1 = ___rhs2;
		V_1 = ((RuntimeObject*)Castclass((RuntimeObject*)L_1, IConvertible_t2722588824_il2cpp_TypeInfo_var));
		int32_t L_2 = ___lhsTypeCode1;
		int32_t L_3 = ___rhsTypeCode3;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		int32_t L_4 = RuntimeServices_GetConvertTypeCode_m1652112802(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_2;
		switch (((int32_t)((int32_t)L_5-(int32_t)((int32_t)10))))
		{
			case 0:
			{
				goto IL_0094;
			}
			case 1:
			{
				goto IL_0083;
			}
			case 2:
			{
				goto IL_0072;
			}
			case 3:
			{
				goto IL_0061;
			}
			case 4:
			{
				goto IL_0050;
			}
			case 5:
			{
				goto IL_003c;
			}
		}
	}
	{
		goto IL_00a5;
	}

IL_003c:
	{
		RuntimeObject* L_6 = V_0;
		NullCheck(L_6);
		Decimal_t2663171440  L_7 = InterfaceFuncInvoker1< Decimal_t2663171440 , RuntimeObject* >::Invoke(4 /* System.Decimal System.IConvertible::ToDecimal(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_6, (RuntimeObject*)NULL);
		RuntimeObject* L_8 = V_1;
		NullCheck(L_8);
		Decimal_t2663171440  L_9 = InterfaceFuncInvoker1< Decimal_t2663171440 , RuntimeObject* >::Invoke(4 /* System.Decimal System.IConvertible::ToDecimal(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_8, (RuntimeObject*)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t2663171440_il2cpp_TypeInfo_var);
		bool L_10 = Decimal_op_Equality_m3698431823(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0050:
	{
		RuntimeObject* L_11 = V_0;
		NullCheck(L_11);
		double L_12 = InterfaceFuncInvoker1< double, RuntimeObject* >::Invoke(5 /* System.Double System.IConvertible::ToDouble(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_11, (RuntimeObject*)NULL);
		RuntimeObject* L_13 = V_1;
		NullCheck(L_13);
		double L_14 = InterfaceFuncInvoker1< double, RuntimeObject* >::Invoke(5 /* System.Double System.IConvertible::ToDouble(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_13, (RuntimeObject*)NULL);
		return (bool)((((double)L_12) == ((double)L_14))? 1 : 0);
	}

IL_0061:
	{
		RuntimeObject* L_15 = V_0;
		NullCheck(L_15);
		float L_16 = InterfaceFuncInvoker1< float, RuntimeObject* >::Invoke(10 /* System.Single System.IConvertible::ToSingle(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_15, (RuntimeObject*)NULL);
		RuntimeObject* L_17 = V_1;
		NullCheck(L_17);
		float L_18 = InterfaceFuncInvoker1< float, RuntimeObject* >::Invoke(10 /* System.Single System.IConvertible::ToSingle(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_17, (RuntimeObject*)NULL);
		return (bool)((((float)L_16) == ((float)L_18))? 1 : 0);
	}

IL_0072:
	{
		RuntimeObject* L_19 = V_0;
		NullCheck(L_19);
		uint64_t L_20 = InterfaceFuncInvoker1< uint64_t, RuntimeObject* >::Invoke(15 /* System.UInt64 System.IConvertible::ToUInt64(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_19, (RuntimeObject*)NULL);
		RuntimeObject* L_21 = V_1;
		NullCheck(L_21);
		uint64_t L_22 = InterfaceFuncInvoker1< uint64_t, RuntimeObject* >::Invoke(15 /* System.UInt64 System.IConvertible::ToUInt64(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_21, (RuntimeObject*)NULL);
		return (bool)((((int64_t)L_20) == ((int64_t)L_22))? 1 : 0);
	}

IL_0083:
	{
		RuntimeObject* L_23 = V_0;
		NullCheck(L_23);
		int64_t L_24 = InterfaceFuncInvoker1< int64_t, RuntimeObject* >::Invoke(8 /* System.Int64 System.IConvertible::ToInt64(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_23, (RuntimeObject*)NULL);
		RuntimeObject* L_25 = V_1;
		NullCheck(L_25);
		int64_t L_26 = InterfaceFuncInvoker1< int64_t, RuntimeObject* >::Invoke(8 /* System.Int64 System.IConvertible::ToInt64(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_25, (RuntimeObject*)NULL);
		return (bool)((((int64_t)L_24) == ((int64_t)L_26))? 1 : 0);
	}

IL_0094:
	{
		RuntimeObject* L_27 = V_0;
		NullCheck(L_27);
		uint32_t L_28 = InterfaceFuncInvoker1< uint32_t, RuntimeObject* >::Invoke(14 /* System.UInt32 System.IConvertible::ToUInt32(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_27, (RuntimeObject*)NULL);
		RuntimeObject* L_29 = V_1;
		NullCheck(L_29);
		uint32_t L_30 = InterfaceFuncInvoker1< uint32_t, RuntimeObject* >::Invoke(14 /* System.UInt32 System.IConvertible::ToUInt32(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_29, (RuntimeObject*)NULL);
		return (bool)((((int32_t)L_28) == ((int32_t)L_30))? 1 : 0);
	}

IL_00a5:
	{
		RuntimeObject* L_31 = V_0;
		NullCheck(L_31);
		int32_t L_32 = InterfaceFuncInvoker1< int32_t, RuntimeObject* >::Invoke(7 /* System.Int32 System.IConvertible::ToInt32(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_31, (RuntimeObject*)NULL);
		RuntimeObject* L_33 = V_1;
		NullCheck(L_33);
		int32_t L_34 = InterfaceFuncInvoker1< int32_t, RuntimeObject* >::Invoke(7 /* System.Int32 System.IConvertible::ToInt32(System.IFormatProvider) */, IConvertible_t2722588824_il2cpp_TypeInfo_var, L_33, (RuntimeObject*)NULL);
		return (bool)((((int32_t)L_32) == ((int32_t)L_34))? 1 : 0);
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices::IsPromotableNumeric(System.TypeCode)
extern "C"  bool RuntimeServices_IsPromotableNumeric_m1958702208 (RuntimeObject * __this /* static, unused */, int32_t ___code0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___code0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)3)))
		{
			case 0:
			{
				goto IL_0057;
			}
			case 1:
			{
				goto IL_005b;
			}
			case 2:
			{
				goto IL_0045;
			}
			case 3:
			{
				goto IL_0043;
			}
			case 4:
			{
				goto IL_0047;
			}
			case 5:
			{
				goto IL_004d;
			}
			case 6:
			{
				goto IL_0049;
			}
			case 7:
			{
				goto IL_004f;
			}
			case 8:
			{
				goto IL_004b;
			}
			case 9:
			{
				goto IL_0051;
			}
			case 10:
			{
				goto IL_0053;
			}
			case 11:
			{
				goto IL_0055;
			}
			case 12:
			{
				goto IL_0059;
			}
		}
	}
	{
		goto IL_005d;
	}

IL_0043:
	{
		return (bool)1;
	}

IL_0045:
	{
		return (bool)1;
	}

IL_0047:
	{
		return (bool)1;
	}

IL_0049:
	{
		return (bool)1;
	}

IL_004b:
	{
		return (bool)1;
	}

IL_004d:
	{
		return (bool)1;
	}

IL_004f:
	{
		return (bool)1;
	}

IL_0051:
	{
		return (bool)1;
	}

IL_0053:
	{
		return (bool)1;
	}

IL_0055:
	{
		return (bool)1;
	}

IL_0057:
	{
		return (bool)1;
	}

IL_0059:
	{
		return (bool)1;
	}

IL_005b:
	{
		return (bool)1;
	}

IL_005d:
	{
		return (bool)0;
	}
}
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices::FindImplicitConversionOperator(System.Type,System.Type)
extern "C"  MethodInfo_t * RuntimeServices_FindImplicitConversionOperator_m4110355163 (RuntimeObject * __this /* static, unused */, Type_t * ___from0, Type_t * ___to1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_FindImplicitConversionOperator_m4110355163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MethodInfo_t * G_B3_0 = NULL;
	MethodInfo_t * G_B1_0 = NULL;
	MethodInfo_t * G_B2_0 = NULL;
	{
		Type_t * L_0 = ___from0;
		NullCheck(L_0);
		MethodInfoU5BU5D_t1946502684* L_1 = VirtFuncInvoker1< MethodInfoU5BU5D_t1946502684*, int32_t >::Invoke(54 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)88));
		Type_t * L_2 = ___from0;
		Type_t * L_3 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		MethodInfo_t * L_4 = RuntimeServices_FindImplicitConversionMethod_m3016211815(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_1, L_2, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = L_4;
		G_B1_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_0038;
		}
	}
	{
		Type_t * L_6 = ___to1;
		NullCheck(L_6);
		MethodInfoU5BU5D_t1946502684* L_7 = VirtFuncInvoker1< MethodInfoU5BU5D_t1946502684*, int32_t >::Invoke(54 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_6, ((int32_t)88));
		Type_t * L_8 = ___from0;
		Type_t * L_9 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		MethodInfo_t * L_10 = RuntimeServices_FindImplicitConversionMethod_m3016211815(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_7, L_8, L_9, /*hidden argument*/NULL);
		MethodInfo_t * L_11 = L_10;
		G_B2_0 = L_11;
		if (L_11)
		{
			G_B3_0 = L_11;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		RuntimeObject* L_12 = RuntimeServices_GetExtensionMethods_m4000040295(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_13 = ___from0;
		Type_t * L_14 = ___to1;
		MethodInfo_t * L_15 = RuntimeServices_FindImplicitConversionMethod_m3016211815(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		G_B3_0 = L_15;
	}

IL_0038:
	{
		return G_B3_0;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo> Boo.Lang.Runtime.RuntimeServices::GetExtensionMethods()
extern "C"  RuntimeObject* RuntimeServices_GetExtensionMethods_m4000040295 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_GetExtensionMethods_m4000040295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * V_0 = NULL;
	{
		U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * L_0 = (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 *)il2cpp_codegen_object_new(U3CGetExtensionMethodsU3Ec__IteratorC_t147372645_il2cpp_TypeInfo_var);
		U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m1649374608(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * L_1 = V_0;
		U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * L_2 = L_1;
		NullCheck(L_2);
		L_2->set_U24PC_2(((int32_t)-2));
		return L_2;
	}
}
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices::FindImplicitConversionMethod(System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>,System.Type,System.Type)
extern "C"  MethodInfo_t * RuntimeServices_FindImplicitConversionMethod_m3016211815 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___candidates0, Type_t * ___from1, Type_t * ___to2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RuntimeServices_FindImplicitConversionMethod_m3016211815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	ParameterInfoU5BU5D_t603298340* V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = ___candidates0;
		NullCheck(L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>::GetEnumerator() */, IEnumerable_1_t3445233625_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0072;
		}

IL_000c:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck(L_2);
			MethodInfo_t * L_3 = InterfaceFuncInvoker0< MethodInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>::get_Current() */, IEnumerator_1_t3960042352_il2cpp_TypeInfo_var, L_2);
			V_0 = L_3;
			MethodInfo_t * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_4);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_6 = String_op_Inequality_m2569768297(NULL /*static, unused*/, L_5, _stringLiteral474303686, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_002d;
			}
		}

IL_0028:
		{
			goto IL_0072;
		}

IL_002d:
		{
			MethodInfo_t * L_7 = V_0;
			NullCheck(L_7);
			Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(31 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_7);
			Type_t * L_9 = ___to2;
			if ((((RuntimeObject*)(Type_t *)L_8) == ((RuntimeObject*)(Type_t *)L_9)))
			{
				goto IL_003e;
			}
		}

IL_0039:
		{
			goto IL_0072;
		}

IL_003e:
		{
			MethodInfo_t * L_10 = V_0;
			NullCheck(L_10);
			ParameterInfoU5BU5D_t603298340* L_11 = VirtFuncInvoker0< ParameterInfoU5BU5D_t603298340* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_10);
			V_2 = L_11;
			ParameterInfoU5BU5D_t603298340* L_12 = V_2;
			NullCheck(L_12);
			if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length))))) == ((int32_t)1)))
			{
				goto IL_0053;
			}
		}

IL_004e:
		{
			goto IL_0072;
		}

IL_0053:
		{
			ParameterInfoU5BU5D_t603298340* L_13 = V_2;
			NullCheck(L_13);
			int32_t L_14 = 0;
			ParameterInfo_t3577984089 * L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
			NullCheck(L_15);
			Type_t * L_16 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_15);
			Type_t * L_17 = ___from1;
			NullCheck(L_16);
			bool L_18 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_16, L_17);
			if (L_18)
			{
				goto IL_006b;
			}
		}

IL_0066:
		{
			goto IL_0072;
		}

IL_006b:
		{
			MethodInfo_t * L_19 = V_0;
			V_3 = L_19;
			IL2CPP_LEAVE(0x8F, FINALLY_0082);
		}

IL_0072:
		{
			RuntimeObject* L_20 = V_1;
			NullCheck(L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_000c;
			}
		}

IL_007d:
		{
			IL2CPP_LEAVE(0x8D, FINALLY_0082);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_0082;
	}

FINALLY_0082:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_22 = V_1;
			if (L_22)
			{
				goto IL_0086;
			}
		}

IL_0085:
		{
			IL2CPP_END_FINALLY(130)
		}

IL_0086:
		{
			RuntimeObject* L_23 = V_1;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1054406163_il2cpp_TypeInfo_var, L_23);
			IL2CPP_END_FINALLY(130)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(130)
	{
		IL2CPP_JUMP_TBL(0x8F, IL_008f)
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_008d:
	{
		return (MethodInfo_t *)NULL;
	}

IL_008f:
	{
		MethodInfo_t * L_24 = V_3;
		return L_24;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::.ctor()
extern "C"  void U3CCoerceU3Ec__AnonStorey1D__ctor_m2923968775 (U3CCoerceU3Ec__AnonStorey1D_t2894686503 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::<>m__15()
extern "C"  Dispatcher_t55733337 * U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m1840774687 (U3CCoerceU3Ec__AnonStorey1D_t2894686503 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCoerceU3Ec__AnonStorey1D_U3CU3Em__15_m1840774687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = __this->get_value_0();
		Type_t * L_1 = __this->get_toType_1();
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		Dispatcher_t55733337 * L_2 = RuntimeServices_CreateCoerceDispatcher_m4246784321(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E::.ctor()
extern "C"  void U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E__ctor_m1755829606 (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E::<>m__16(System.Object,System.Object[])
extern "C"  RuntimeObject * U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m681769745 (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t3634380927 * __this, RuntimeObject * ___target0, ObjectU5BU5D_t3384890222* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_U3CU3Em__16_m681769745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MethodInfo_t * L_0 = __this->get_method_0();
		ObjectU5BU5D_t3384890222* L_1 = ((ObjectU5BU5D_t3384890222*)SZArrayNew(ObjectU5BU5D_t3384890222_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeObject * L_2 = ___target0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		NullCheck(L_0);
		RuntimeObject * L_3 = MethodBase_Invoke_m3234672952(L_0, NULL, L_1, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::.ctor()
extern "C"  void U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m1649374608 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.Generic.IEnumerator<System.Reflection.MethodInfo>.get_Current()
extern "C"  MethodInfo_t * U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MethodInfoU3E_get_Current_m3686655581 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1606385554 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method)
{
	{
		MethodInfo_t * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Collections.IEnumerator Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_IEnumerable_GetEnumerator_m1148137917 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m4098321379(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo> Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::System.Collections.Generic.IEnumerable<System.Reflection.MethodInfo>.GetEnumerator()
extern "C"  RuntimeObject* U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m4098321379 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtensionMethodsU3Ec__IteratorC_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MethodInfoU3E_GetEnumerator_m4098321379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t* L_0 = __this->get_address_of_U24PC_2();
		int32_t L_1 = Interlocked_CompareExchange_m540286148(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * L_2 = (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 *)il2cpp_codegen_object_new(U3CGetExtensionMethodsU3Ec__IteratorC_t147372645_il2cpp_TypeInfo_var);
		U3CGetExtensionMethodsU3Ec__IteratorC__ctor_m1649374608(L_2, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::MoveNext()
extern "C"  bool U3CGetExtensionMethodsU3Ec__IteratorC_MoveNext_m1298909905 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtensionMethodsU3Ec__IteratorC_MoveNext_m1298909905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_003b;
			}
		}
	}
	{
		goto IL_00c2;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		ExtensionRegistry_t4000900622 * L_2 = ((RuntimeServices_t4253430258_StaticFields*)il2cpp_codegen_static_fields_for(RuntimeServices_t4253430258_il2cpp_TypeInfo_var))->get__extensions_3();
		NullCheck(L_2);
		RuntimeObject* L_3 = ExtensionRegistry_get_Extensions_m4200656435(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>::GetEnumerator() */, IEnumerable_1_t2375221188_il2cpp_TypeInfo_var, L_3);
		__this->set_U3CU24s_49U3E__0_0(L_4);
		V_0 = ((int32_t)-3);
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			switch (((int32_t)((int32_t)L_5-(int32_t)1)))
			{
				case 0:
				{
					goto IL_008d;
				}
			}
		}

IL_0047:
		{
			goto IL_008d;
		}

IL_004c:
		{
			RuntimeObject* L_6 = __this->get_U3CU24s_49U3E__0_0();
			NullCheck(L_6);
			MemberInfo_t * L_7 = InterfaceFuncInvoker0< MemberInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>::get_Current() */, IEnumerator_1_t2890029915_il2cpp_TypeInfo_var, L_6);
			__this->set_U3CmemberU3E__1_1(L_7);
			MemberInfo_t * L_8 = __this->get_U3CmemberU3E__1_1();
			NullCheck(L_8);
			int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.MemberTypes System.Reflection.MemberInfo::get_MemberType() */, L_8);
			if ((!(((uint32_t)L_9) == ((uint32_t)8))))
			{
				goto IL_008d;
			}
		}

IL_006e:
		{
			MemberInfo_t * L_10 = __this->get_U3CmemberU3E__1_1();
			__this->set_U24current_3(((MethodInfo_t *)CastclassClass((RuntimeObject*)L_10, MethodInfo_t_il2cpp_TypeInfo_var)));
			__this->set_U24PC_2(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC4, FINALLY_00a2);
		}

IL_008d:
		{
			RuntimeObject* L_11 = __this->get_U3CU24s_49U3E__0_0();
			NullCheck(L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_004c;
			}
		}

IL_009d:
		{
			IL2CPP_LEAVE(0xBB, FINALLY_00a2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_00a2;
	}

FINALLY_00a2:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a6;
			}
		}

IL_00a5:
		{
			IL2CPP_END_FINALLY(162)
		}

IL_00a6:
		{
			RuntimeObject* L_14 = __this->get_U3CU24s_49U3E__0_0();
			if (L_14)
			{
				goto IL_00af;
			}
		}

IL_00ae:
		{
			IL2CPP_END_FINALLY(162)
		}

IL_00af:
		{
			RuntimeObject* L_15 = __this->get_U3CU24s_49U3E__0_0();
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1054406163_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(162)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(162)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_JUMP_TBL(0xBB, IL_00bb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_00bb:
	{
		__this->set_U24PC_2((-1));
	}

IL_00c2:
	{
		return (bool)0;
	}

IL_00c4:
	{
		return (bool)1;
	}
	// Dead block : IL_00c6: ldloc.2
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::Dispose()
extern "C"  void U3CGetExtensionMethodsU3Ec__IteratorC_Dispose_m2812033218 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtensionMethodsU3Ec__IteratorC_Dispose_m2812033218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_2 = __this->get_U3CU24s_49U3E__0_0();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			RuntimeObject* L_3 = __this->get_U3CU24s_49U3E__0_0();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1054406163_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::Reset()
extern "C"  void U3CGetExtensionMethodsU3Ec__IteratorC_Reset_m3260254275 (U3CGetExtensionMethodsU3Ec__IteratorC_t147372645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetExtensionMethodsU3Ec__IteratorC_Reset_m3260254275_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t3527988452 * L_0 = (NotSupportedException_t3527988452 *)il2cpp_codegen_object_new(NotSupportedException_t3527988452_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m4139731146(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerable`1<System.String> Boo.Lang.Runtime.TextReaderEnumerator::lines(System.IO.TextReader)
extern "C"  RuntimeObject* TextReaderEnumerator_lines_m3632397483 (RuntimeObject * __this /* static, unused */, TextReader_t2214828068 * ___reader0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextReaderEnumerator_lines_m3632397483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3ClinesU3Ec__IteratorD_t4243100130 * V_0 = NULL;
	{
		U3ClinesU3Ec__IteratorD_t4243100130 * L_0 = (U3ClinesU3Ec__IteratorD_t4243100130 *)il2cpp_codegen_object_new(U3ClinesU3Ec__IteratorD_t4243100130_il2cpp_TypeInfo_var);
		U3ClinesU3Ec__IteratorD__ctor_m213053513(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3ClinesU3Ec__IteratorD_t4243100130 * L_1 = V_0;
		TextReader_t2214828068 * L_2 = ___reader0;
		NullCheck(L_1);
		L_1->set_reader_0(L_2);
		U3ClinesU3Ec__IteratorD_t4243100130 * L_3 = V_0;
		TextReader_t2214828068 * L_4 = ___reader0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ereader_5(L_4);
		U3ClinesU3Ec__IteratorD_t4243100130 * L_5 = V_0;
		U3ClinesU3Ec__IteratorD_t4243100130 * L_6 = L_5;
		NullCheck(L_6);
		L_6->set_U24PC_3(((int32_t)-2));
		return L_6;
	}
}
// System.Void Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::.ctor()
extern "C"  void U3ClinesU3Ec__IteratorD__ctor_m213053513 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3ClinesU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m2995840804 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3ClinesU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1973143609 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* U3ClinesU3Ec__IteratorD_System_Collections_IEnumerable_GetEnumerator_m1723112731 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = U3ClinesU3Ec__IteratorD_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3174056700(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.String> Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  RuntimeObject* U3ClinesU3Ec__IteratorD_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3174056700 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3ClinesU3Ec__IteratorD_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3174056700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3ClinesU3Ec__IteratorD_t4243100130 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m540286148(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3ClinesU3Ec__IteratorD_t4243100130 * L_2 = (U3ClinesU3Ec__IteratorD_t4243100130 *)il2cpp_codegen_object_new(U3ClinesU3Ec__IteratorD_t4243100130_il2cpp_TypeInfo_var);
		U3ClinesU3Ec__IteratorD__ctor_m213053513(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3ClinesU3Ec__IteratorD_t4243100130 * L_3 = V_0;
		TextReader_t2214828068 * L_4 = __this->get_U3CU24U3Ereader_5();
		NullCheck(L_3);
		L_3->set_reader_0(L_4);
		U3ClinesU3Ec__IteratorD_t4243100130 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::MoveNext()
extern "C"  bool U3ClinesU3Ec__IteratorD_MoveNext_m1366279500 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3ClinesU3Ec__IteratorD_MoveNext_m1366279500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	String_t* V_2 = NULL;
	bool V_3 = false;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0032;
			}
		}
	}
	{
		goto IL_009d;
	}

IL_0023:
	{
		TextReader_t2214828068 * L_2 = __this->get_reader_0();
		__this->set_U3CU24s_51U3E__0_1(L_2);
		V_0 = ((int32_t)-3);
	}

IL_0032:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_3 = V_0;
			switch (((int32_t)((int32_t)L_3-(int32_t)1)))
			{
				case 0:
				{
					goto IL_005d;
				}
			}
		}

IL_003e:
		{
			goto IL_005d;
		}

IL_0043:
		{
			String_t* L_4 = __this->get_U3ClineU3E__1_2();
			__this->set_U24current_4(L_4);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x9F, FINALLY_007b);
		}

IL_005d:
		{
			TextReader_t2214828068 * L_5 = __this->get_reader_0();
			NullCheck(L_5);
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.IO.TextReader::ReadLine() */, L_5);
			String_t* L_7 = L_6;
			V_2 = L_7;
			__this->set_U3ClineU3E__1_2(L_7);
			String_t* L_8 = V_2;
			if (L_8)
			{
				goto IL_0043;
			}
		}

IL_0076:
		{
			IL2CPP_LEAVE(0x96, FINALLY_007b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_007b;
	}

FINALLY_007b:
	{ // begin finally (depth: 1)
		{
			bool L_9 = V_1;
			if (!L_9)
			{
				goto IL_007f;
			}
		}

IL_007e:
		{
			IL2CPP_END_FINALLY(123)
		}

IL_007f:
		{
			TextReader_t2214828068 * L_10 = __this->get_U3CU24s_51U3E__0_1();
			if (!L_10)
			{
				goto IL_0095;
			}
		}

IL_008a:
		{
			TextReader_t2214828068 * L_11 = __this->get_U3CU24s_51U3E__0_1();
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1054406163_il2cpp_TypeInfo_var, L_11);
		}

IL_0095:
		{
			IL2CPP_END_FINALLY(123)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(123)
	{
		IL2CPP_JUMP_TBL(0x9F, IL_009f)
		IL2CPP_JUMP_TBL(0x96, IL_0096)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_0096:
	{
		__this->set_U24PC_3((-1));
	}

IL_009d:
	{
		return (bool)0;
	}

IL_009f:
	{
		return (bool)1;
	}
	// Dead block : IL_00a1: ldloc.3
}
// System.Void Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::Dispose()
extern "C"  void U3ClinesU3Ec__IteratorD_Dispose_m1809672304 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3ClinesU3Ec__IteratorD_Dispose_m1809672304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003d;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			TextReader_t2214828068 * L_2 = __this->get_U3CU24s_51U3E__0_1();
			if (!L_2)
			{
				goto IL_003c;
			}
		}

IL_0031:
		{
			TextReader_t2214828068 * L_3 = __this->get_U3CU24s_51U3E__0_1();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1054406163_il2cpp_TypeInfo_var, L_3);
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void Boo.Lang.Runtime.TextReaderEnumerator/<lines>c__IteratorD::Reset()
extern "C"  void U3ClinesU3Ec__IteratorD_Reset_m2544135774 (U3ClinesU3Ec__IteratorD_t4243100130 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3ClinesU3Ec__IteratorD_Reset_m2544135774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t3527988452 * L_0 = (NotSupportedException_t3527988452 *)il2cpp_codegen_object_new(NotSupportedException_t3527988452_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m4139731146(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
