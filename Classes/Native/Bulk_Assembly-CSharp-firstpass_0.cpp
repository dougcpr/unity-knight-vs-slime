﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// AR3DOFCameraManager
struct AR3DOFCameraManager_t3216700943;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1096588306;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t3869411053;
// UnityEngine.Object
struct Object_t1008057425;
// UnityEngine.Camera
struct Camera_t226495598;
// UnityEngine.Component
struct Component_t531478471;
// UnityEngine.GameObject
struct GameObject_t3649338848;
// UnityEngine.XR.iOS.UnityARVideo
struct UnityARVideo_t2470488057;
// UnityEngine.Transform
struct Transform_t3933397867;
// DontDestroyOnLoad
struct DontDestroyOnLoad_t2575831040;
// PointCloudParticleExample
struct PointCloudParticleExample_t3205217707;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct ARFrameUpdate_t3333962149;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1777616458;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t419044168;
// UnityARCameraManager
struct UnityARCameraManager_t1803962782;
// UnityARCameraNearFar
struct UnityARCameraNearFar_t4152080291;
// UnityEngine.XR.iOS.ARPlaneAnchorGameObject
struct ARPlaneAnchorGameObject_t3146329710;
// UnityEngine.XR.iOS.UnityARAnchorManager
struct UnityARAnchorManager_t3624459219;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct Dictionary_2_t3033730875;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1946233076;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct ARAnchorAdded_t1580091102;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct ARAnchorUpdated_t1492128022;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct ARAnchorRemoved_t3663543047;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct List_1_t1889681993;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3160831282;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct ValueCollection_t70314757;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t3277784254;
// System.Collections.Generic.IEnumerable`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct IEnumerable_1_t278075526;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t1549224815;
// UnityEngine.XR.iOS.UnityARGeneratePlane
struct UnityARGeneratePlane_t3109641619;
// UnityEngine.XR.iOS.UnityARHitTestExample
struct UnityARHitTestExample_t2411672648;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>
struct List_1_t547075962;
// UnityEngine.XR.iOS.UnityARKitControl
struct UnityARKitControl_t4114739031;
// UnityEngine.XR.iOS.UnityARMatrixOps
struct UnityARMatrixOps_t180517933;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct internal_ARFrameUpdate_t2726389622;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct internal_ARAnchorAdded_t3791587037;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct internal_ARAnchorUpdated_t2928809250;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct internal_ARAnchorRemoved_t409959511;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct ARSessionFailed_t3599980200;
// System.Delegate
struct Delegate_t69892740;
// UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct ARKitWorldTackingSessionConfiguration_t2186350340;
// UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t3142208763;
// UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t1002809393;
// System.Single[]
struct SingleU5BU5D_t3488826921;
// UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1843157284;
// System.IAsyncResult
struct IAsyncResult_t1614106113;
// System.AsyncCallback
struct AsyncCallback_t606388952;
// UnityEngine.XR.iOS.UnityARUtility
struct UnityARUtility_t2651983316;
// UnityEngine.MeshFilter
struct MeshFilter_t2678471223;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1027034615;
// UnityEngine.Texture
struct Texture_t1132728222;
// UnityEngine.Material
struct Material_t4055262778;
// UnityEngine.Texture2D
struct Texture2D_t2870930912;
// UnityPointCloudExample
struct UnityPointCloudExample_t595762290;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2392691131;
// System.Int32[]
struct Int32U5BU5D_t595981822;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3878825437;
// System.String[]
struct StringU5BU5D_t1448570014;
// UnityEngine.XR.iOS.ARPlaneAnchorGameObject[]
struct ARPlaneAnchorGameObjectU5BU5D_t2636665883;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1241335807;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1371384188;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject,System.Collections.DictionaryEntry>
struct Transform_1_t4146383110;
// System.Char[]
struct CharU5BU5D_t674980486;
// UnityEngine.XR.iOS.ARHitTestResult[]
struct ARHitTestResultU5BU5D_t3516045062;
// UnityEngine.MeshCollider
struct MeshCollider_t3960673270;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t1488940705;
// System.Void
struct Void_t2725935594;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t421390435;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t4153953548;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t1646417403;
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t3872385379;
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_t2020264667;
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t4098669274;

extern RuntimeClass* UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var;
extern RuntimeClass* ARKitSessionConfiguration_t3142208763_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t1008057425_il2cpp_TypeInfo_var;
extern const uint32_t AR3DOFCameraManager_Start_m724138112_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688_RuntimeMethod_var;
extern const uint32_t AR3DOFCameraManager_SetCamera_m3892213082_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisUnityARVideo_t2470488057_m3425289194_RuntimeMethod_var;
extern const uint32_t AR3DOFCameraManager_SetupNewCamera_m2725600353_MetadataUsageId;
extern const uint32_t AR3DOFCameraManager_Update_m4197268577_MetadataUsageId;
extern const uint32_t DontDestroyOnLoad_Start_m2304192681_MetadataUsageId;
extern RuntimeClass* ARFrameUpdate_t3333962149_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PointCloudParticleExample_ARFrameUpdated_m1335679990_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisParticleSystem_t1777616458_m895722352_RuntimeMethod_var;
extern const uint32_t PointCloudParticleExample_Start_m1170738568_MetadataUsageId;
extern RuntimeClass* Mathf_t413863475_il2cpp_TypeInfo_var;
extern RuntimeClass* ParticleU5BU5D_t419044168_il2cpp_TypeInfo_var;
extern const uint32_t PointCloudParticleExample_Update_m3373663811_MetadataUsageId;
extern RuntimeClass* ARKitWorldTackingSessionConfiguration_t2186350340_il2cpp_TypeInfo_var;
extern const uint32_t UnityARCameraManager_Start_m443402790_MetadataUsageId;
extern const uint32_t UnityARCameraManager_SetCamera_m3794718110_MetadataUsageId;
extern const uint32_t UnityARCameraManager_SetupNewCamera_m3593985194_MetadataUsageId;
extern const uint32_t UnityARCameraManager_Update_m3305433653_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t226495598_m3685067992_RuntimeMethod_var;
extern const uint32_t UnityARCameraNearFar_Start_m2942652737_MetadataUsageId;
extern const uint32_t UnityARCameraNearFar_UpdateCameraClipPlanes_m123287997_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t3033730875_il2cpp_TypeInfo_var;
extern RuntimeClass* ARAnchorAdded_t1580091102_il2cpp_TypeInfo_var;
extern RuntimeClass* ARAnchorUpdated_t1492128022_il2cpp_TypeInfo_var;
extern RuntimeClass* ARAnchorRemoved_t3663543047_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2541633073_RuntimeMethod_var;
extern const RuntimeMethod* UnityARAnchorManager_AddAnchor_m3732879992_RuntimeMethod_var;
extern const RuntimeMethod* UnityARAnchorManager_UpdateAnchor_m1796371617_RuntimeMethod_var;
extern const RuntimeMethod* UnityARAnchorManager_RemoveAnchor_m1348574666_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager__ctor_m1537079611_MetadataUsageId;
extern RuntimeClass* UnityARUtility_t2651983316_il2cpp_TypeInfo_var;
extern RuntimeClass* ARPlaneAnchorGameObject_t3146329710_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisDontDestroyOnLoad_t2575831040_m3797858659_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m113011829_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_AddAnchor_m3732879992_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m4199403017_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m1097037868_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m4064418252_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_RemoveAnchor_m1348574666_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_set_Item_m4260355931_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_UpdateAnchor_m1796371617_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m2396092950_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3648846247_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2283436456_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m4042749809_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m1758728446_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_Destroy_m3970721311_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_get_Values_m3019150958_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToList_TisARPlaneAnchorGameObject_t3146329710_m589330593_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_GetCurrentPlaneAnchors_m3710250286_MetadataUsageId;
extern RuntimeClass* UnityARAnchorManager_t3624459219_il2cpp_TypeInfo_var;
extern const uint32_t UnityARGeneratePlane_Start_m1171670317_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m1057070554_RuntimeMethod_var;
extern const uint32_t UnityARGeneratePlane_OnGUI_m2684108629_MetadataUsageId;
extern RuntimeClass* Debug_t3314485529_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t2847614712_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m655066853_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2437281809_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3632740371_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1007619642_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3562983614_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1670108847;
extern Il2CppCodeGenString* _stringLiteral297872476;
extern const uint32_t UnityARHitTestExample_HitTestWithResultType_m2836964868_MetadataUsageId;
extern RuntimeClass* Input_t1121709983_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t59524482_il2cpp_TypeInfo_var;
extern RuntimeClass* ARPoint_t1649490841_il2cpp_TypeInfo_var;
extern RuntimeClass* ARHitTestResultTypeU5BU5D_t1137434915_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t1316871563____U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0_FieldInfo_var;
extern const uint32_t UnityARHitTestExample_Update_m2631636100_MetadataUsageId;
extern RuntimeClass* UnityARSessionRunOptionU5BU5D_t3872385379_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityARAlignmentU5BU5D_t2020264667_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityARPlaneDetectionU5BU5D_t4098669274_il2cpp_TypeInfo_var;
extern const uint32_t UnityARKitControl__ctor_m4265604983_MetadataUsageId;
extern RuntimeClass* GUI_t3786480996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1616253138;
extern Il2CppCodeGenString* _stringLiteral2126568017;
extern Il2CppCodeGenString* _stringLiteral1876860631;
extern Il2CppCodeGenString* _stringLiteral263420843;
extern Il2CppCodeGenString* _stringLiteral3336902858;
extern Il2CppCodeGenString* _stringLiteral386416851;
extern Il2CppCodeGenString* _stringLiteral3645053941;
extern Il2CppCodeGenString* _stringLiteral78005878;
extern Il2CppCodeGenString* _stringLiteral3550138209;
extern Il2CppCodeGenString* _stringLiteral414407049;
extern Il2CppCodeGenString* _stringLiteral3907610049;
extern Il2CppCodeGenString* _stringLiteral4085911591;
extern Il2CppCodeGenString* _stringLiteral1573470155;
extern Il2CppCodeGenString* _stringLiteral1272959178;
extern Il2CppCodeGenString* _stringLiteral893077012;
extern Il2CppCodeGenString* _stringLiteral3186302199;
extern const uint32_t UnityARKitControl_OnGUI_m2842321300_MetadataUsageId;
extern RuntimeClass* Vector4_t1376926224_il2cpp_TypeInfo_var;
extern const uint32_t UnityARMatrixOps_GetPosition_m3553979848_MetadataUsageId;
extern RuntimeClass* Quaternion_t3165733013_il2cpp_TypeInfo_var;
extern const uint32_t UnityARMatrixOps_QuaternionFromMatrix_m2802180225_MetadataUsageId;
extern RuntimeClass* internal_ARFrameUpdate_t2726389622_il2cpp_TypeInfo_var;
extern RuntimeClass* internal_ARAnchorAdded_t3791587037_il2cpp_TypeInfo_var;
extern RuntimeClass* internal_ARAnchorUpdated_t2928809250_il2cpp_TypeInfo_var;
extern RuntimeClass* internal_ARAnchorRemoved_t409959511_il2cpp_TypeInfo_var;
extern RuntimeClass* ARSessionFailed_t3599980200_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__frame_update_m3249087823_RuntimeMethod_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__anchor_added_m3990782978_RuntimeMethod_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__anchor_updated_m4275459728_RuntimeMethod_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__anchor_removed_m4149770974_RuntimeMethod_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__ar_session_failed_m1149369129_RuntimeMethod_var;
extern const uint32_t UnityARSessionNativeInterface__ctor_m2217614958_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1467721625_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m3076006849_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m3219973195_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARAnchorAddedEvent_m1017817494_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m994455513_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARAnchorUpdatedEvent_m2689967175_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m1279350122_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARAnchorRemovedEvent_m2859602900_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARSessionFailedEvent_m1051638243_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARSessionFailedEvent_m3748117041_MetadataUsageId;
struct ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke;
struct ARKitWorldTackingSessionConfiguration_t2186350340;;
struct ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke;;
struct ARKitSessionConfiguration_t3142208763_marshaled_pinvoke;
struct ARKitSessionConfiguration_t3142208763;;
struct ARKitSessionConfiguration_t3142208763_marshaled_pinvoke;;
struct UnityARHitTestResult_t1002809393_marshaled_pinvoke;
struct UnityARHitTestResult_t1002809393;;
struct UnityARHitTestResult_t1002809393_marshaled_pinvoke;;
extern const uint32_t UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t1288378485_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface_GetCameraPose_m3014150810_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetCameraProjection_m248827655_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_SetCameraClipPlanes_m540333021_MetadataUsageId;
extern RuntimeClass* UnityARCamera_t2376600594_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface__frame_update_m3249087823_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* SingleU5BU5D_t3488826921_il2cpp_TypeInfo_var;
extern RuntimeClass* Marshal_t485009956_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3U5BU5D_t4153953548_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface_UpdatePointCloudData_m3039135696_MetadataUsageId;
extern RuntimeClass* ARPlaneAnchor_t1843157284_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m1229793965_MetadataUsageId;
extern RuntimeClass* ARHitTestResult_t1803723679_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface_GetHitTestResultFromResultData_m2693418722_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface__anchor_added_m3990782978_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface__anchor_updated_m4275459728_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface__anchor_removed_m4149770974_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral707593833;
extern const uint32_t UnityARSessionNativeInterface__ar_session_failed_m1149369129_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_RunWithConfigAndOptions_m3750156203_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_RunWithConfig_m3364444856_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_RunWithConfigAndOptions_m2810560210_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_RunWithConfig_m152596073_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_Pause_m224144355_MetadataUsageId;
extern RuntimeClass* Int32_t3425510919_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t547075962_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2682833015_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1601798359_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1984200360;
extern const uint32_t UnityARSessionNativeInterface_HitTest_m1126351971_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetARVideoTextureHandles_m3043872758_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetARAmbientIntensity_m2324758640_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetARTrackingQuality_m2526139771_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1899687960_MetadataUsageId;
struct ARPlaneAnchor_t1843157284_marshaled_pinvoke;
struct ARPlaneAnchor_t1843157284;;
struct ARPlaneAnchor_t1843157284_marshaled_pinvoke;;
extern const uint32_t ARAnchorAdded_BeginInvoke_m3780076230_MetadataUsageId;
extern const uint32_t ARAnchorRemoved_BeginInvoke_m2521998422_MetadataUsageId;
extern const uint32_t ARAnchorUpdated_BeginInvoke_m1428606326_MetadataUsageId;
extern const uint32_t ARFrameUpdate_BeginInvoke_m2642825502_MetadataUsageId;
extern RuntimeClass* UnityARAnchorData_t1511263172_il2cpp_TypeInfo_var;
extern const uint32_t internal_ARAnchorAdded_BeginInvoke_m1551770526_MetadataUsageId;
extern const uint32_t internal_ARAnchorRemoved_BeginInvoke_m3275603482_MetadataUsageId;
extern const uint32_t internal_ARAnchorUpdated_BeginInvoke_m2660600822_MetadataUsageId;
extern RuntimeClass* internal_UnityARCamera_t1506237410_il2cpp_TypeInfo_var;
extern const uint32_t internal_ARFrameUpdate_BeginInvoke_m4150912559_MetadataUsageId;
extern const uint32_t UnityARUtility_InitializePlanePrefab_m2229940298_MetadataUsageId;
extern RuntimeClass* GameObject_t3649338848_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t3649338848_m4208004768_RuntimeMethod_var;
extern const uint32_t UnityARUtility_CreatePlaneInScene_m2413657456_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponentInChildren_TisMeshFilter_t2678471223_m4019234351_RuntimeMethod_var;
extern const uint32_t UnityARUtility_UpdatePlaneWithAnchorTransform_m923713608_MetadataUsageId;
extern const uint32_t UnityARVideo_Start_m2313235990_MetadataUsageId;
extern RuntimeClass* CommandBuffer_t1027034615_il2cpp_TypeInfo_var;
extern const uint32_t UnityARVideo_InitializeCommandBuffer_m3792744751_MetadataUsageId;
extern const uint32_t UnityARVideo_OnDestroy_m757367868_MetadataUsageId;
extern RuntimeClass* Vector3_t596762001_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral813233302;
extern Il2CppCodeGenString* _stringLiteral2373698739;
extern Il2CppCodeGenString* _stringLiteral3027136166;
extern Il2CppCodeGenString* _stringLiteral3578222424;
extern Il2CppCodeGenString* _stringLiteral556172373;
extern const uint32_t UnityARVideo_OnPreRender_m1630060808_MetadataUsageId;
extern RuntimeClass* List_1_t2392691131_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityPointCloudExample_ARFrameUpdated_m2014687293_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m267030108_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m423025013_RuntimeMethod_var;
extern const uint32_t UnityPointCloudExample_Start_m761710379_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m1883443879_RuntimeMethod_var;
extern const uint32_t UnityPointCloudExample_Update_m617780277_MetadataUsageId;
struct Vector3_t596762001 ;

struct Vector3U5BU5D_t4153953548;
struct ParticleU5BU5D_t419044168;
struct ARHitTestResultTypeU5BU5D_t1137434915;
struct UnityARSessionRunOptionU5BU5D_t3872385379;
struct UnityARAlignmentU5BU5D_t2020264667;
struct UnityARPlaneDetectionU5BU5D_t4098669274;
struct SingleU5BU5D_t3488826921;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3896811616_H
#define U3CMODULEU3E_T3896811616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3896811616 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3896811616_H
#ifndef UNITYARANCHORMANAGER_T3624459219_H
#define UNITYARANCHORMANAGER_T3624459219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorManager
struct  UnityARAnchorManager_t3624459219  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::planeAnchorMap
	Dictionary_2_t3033730875 * ___planeAnchorMap_0;

public:
	inline static int32_t get_offset_of_planeAnchorMap_0() { return static_cast<int32_t>(offsetof(UnityARAnchorManager_t3624459219, ___planeAnchorMap_0)); }
	inline Dictionary_2_t3033730875 * get_planeAnchorMap_0() const { return ___planeAnchorMap_0; }
	inline Dictionary_2_t3033730875 ** get_address_of_planeAnchorMap_0() { return &___planeAnchorMap_0; }
	inline void set_planeAnchorMap_0(Dictionary_2_t3033730875 * value)
	{
		___planeAnchorMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___planeAnchorMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORMANAGER_T3624459219_H
#ifndef DICTIONARY_2_T3033730875_H
#define DICTIONARY_2_T3033730875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct  Dictionary_2_t3033730875  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t595981822* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t3878825437* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1448570014* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ARPlaneAnchorGameObjectU5BU5D_t2636665883* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t1371384188 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___table_4)); }
	inline Int32U5BU5D_t595981822* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t595981822** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t595981822* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___linkSlots_5)); }
	inline LinkU5BU5D_t3878825437* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t3878825437** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t3878825437* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___keySlots_6)); }
	inline StringU5BU5D_t1448570014* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1448570014** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1448570014* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___valueSlots_7)); }
	inline ARPlaneAnchorGameObjectU5BU5D_t2636665883* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ARPlaneAnchorGameObjectU5BU5D_t2636665883** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ARPlaneAnchorGameObjectU5BU5D_t2636665883* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___serialization_info_13)); }
	inline SerializationInfo_t1371384188 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t1371384188 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t1371384188 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3033730875_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4146383110 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3033730875_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4146383110 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4146383110 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4146383110 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3033730875_H
#ifndef LIST_1_T1889681993_H
#define LIST_1_T1889681993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct  List_1_t1889681993  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ARPlaneAnchorGameObjectU5BU5D_t2636665883* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1889681993, ____items_1)); }
	inline ARPlaneAnchorGameObjectU5BU5D_t2636665883* get__items_1() const { return ____items_1; }
	inline ARPlaneAnchorGameObjectU5BU5D_t2636665883** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ARPlaneAnchorGameObjectU5BU5D_t2636665883* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1889681993, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1889681993, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1889681993_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ARPlaneAnchorGameObjectU5BU5D_t2636665883* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1889681993_StaticFields, ___EmptyArray_4)); }
	inline ARPlaneAnchorGameObjectU5BU5D_t2636665883* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ARPlaneAnchorGameObjectU5BU5D_t2636665883** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ARPlaneAnchorGameObjectU5BU5D_t2636665883* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1889681993_H
#ifndef VALUECOLLECTION_T70314757_H
#define VALUECOLLECTION_T70314757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct  ValueCollection_t70314757  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t3033730875 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t70314757, ___dictionary_0)); }
	inline Dictionary_2_t3033730875 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3033730875 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3033730875 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T70314757_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t674980486* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t674980486* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t674980486** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t674980486* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T547075962_H
#define LIST_1_T547075962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>
struct  List_1_t547075962  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ARHitTestResultU5BU5D_t3516045062* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t547075962, ____items_1)); }
	inline ARHitTestResultU5BU5D_t3516045062* get__items_1() const { return ____items_1; }
	inline ARHitTestResultU5BU5D_t3516045062** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ARHitTestResultU5BU5D_t3516045062* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t547075962, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t547075962, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t547075962_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ARHitTestResultU5BU5D_t3516045062* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t547075962_StaticFields, ___EmptyArray_4)); }
	inline ARHitTestResultU5BU5D_t3516045062* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ARHitTestResultU5BU5D_t3516045062** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ARHitTestResultU5BU5D_t3516045062* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T547075962_H
#ifndef UNITYARMATRIXOPS_T180517933_H
#define UNITYARMATRIXOPS_T180517933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrixOps
struct  UnityARMatrixOps_t180517933  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIXOPS_T180517933_H
#ifndef UNITYARUTILITY_T2651983316_H
#define UNITYARUTILITY_T2651983316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUtility
struct  UnityARUtility_t2651983316  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider UnityEngine.XR.iOS.UnityARUtility::meshCollider
	MeshCollider_t3960673270 * ___meshCollider_0;
	// UnityEngine.MeshFilter UnityEngine.XR.iOS.UnityARUtility::meshFilter
	MeshFilter_t2678471223 * ___meshFilter_1;

public:
	inline static int32_t get_offset_of_meshCollider_0() { return static_cast<int32_t>(offsetof(UnityARUtility_t2651983316, ___meshCollider_0)); }
	inline MeshCollider_t3960673270 * get_meshCollider_0() const { return ___meshCollider_0; }
	inline MeshCollider_t3960673270 ** get_address_of_meshCollider_0() { return &___meshCollider_0; }
	inline void set_meshCollider_0(MeshCollider_t3960673270 * value)
	{
		___meshCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshCollider_0), value);
	}

	inline static int32_t get_offset_of_meshFilter_1() { return static_cast<int32_t>(offsetof(UnityARUtility_t2651983316, ___meshFilter_1)); }
	inline MeshFilter_t2678471223 * get_meshFilter_1() const { return ___meshFilter_1; }
	inline MeshFilter_t2678471223 ** get_address_of_meshFilter_1() { return &___meshFilter_1; }
	inline void set_meshFilter_1(MeshFilter_t2678471223 * value)
	{
		___meshFilter_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_1), value);
	}
};

struct UnityARUtility_t2651983316_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::planePrefab
	GameObject_t3649338848 * ___planePrefab_2;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARUtility_t2651983316_StaticFields, ___planePrefab_2)); }
	inline GameObject_t3649338848 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t3649338848 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t3649338848 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUTILITY_T2651983316_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3433162460_H
#define VALUETYPE_T3433162460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3433162460  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_com
{
};
#endif // VALUETYPE_T3433162460_H
#ifndef LIST_1_T2392691131_H
#define LIST_1_T2392691131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t2392691131  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t1488940705* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2392691131, ____items_1)); }
	inline GameObjectU5BU5D_t1488940705* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t1488940705** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t1488940705* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2392691131, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2392691131, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2392691131_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t1488940705* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2392691131_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t1488940705* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t1488940705** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t1488940705* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2392691131_H
#ifndef COLOR_T320819310_H
#define COLOR_T320819310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t320819310 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t320819310, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t320819310, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t320819310, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t320819310, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T320819310_H
#ifndef VOID_T2725935594_H
#define VOID_T2725935594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2725935594 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2725935594_H
#ifndef VECTOR2_T59524482_H
#define VECTOR2_T59524482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t59524482 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t59524482, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t59524482, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t59524482_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t59524482  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t59524482  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t59524482  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t59524482  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t59524482  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t59524482  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t59524482  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t59524482  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___zeroVector_2)); }
	inline Vector2_t59524482  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t59524482 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t59524482  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___oneVector_3)); }
	inline Vector2_t59524482  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t59524482 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t59524482  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___upVector_4)); }
	inline Vector2_t59524482  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t59524482 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t59524482  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___downVector_5)); }
	inline Vector2_t59524482  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t59524482 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t59524482  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___leftVector_6)); }
	inline Vector2_t59524482  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t59524482 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t59524482  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___rightVector_7)); }
	inline Vector2_t59524482  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t59524482 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t59524482  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t59524482  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t59524482 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t59524482  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t59524482  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t59524482 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t59524482  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T59524482_H
#ifndef DOUBLE_T1029397067_H
#define DOUBLE_T1029397067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t1029397067 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t1029397067, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T1029397067_H
#ifndef COLOR32_T2499566028_H
#define COLOR32_T2499566028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t2499566028 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t2499566028, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t2499566028, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t2499566028, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t2499566028, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2499566028_H
#ifndef ENUMERATOR_T214722923_H
#define ENUMERATOR_T214722923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t214722923 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3160831282 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t214722923, ___l_0)); }
	inline List_1_t3160831282 * get_l_0() const { return ___l_0; }
	inline List_1_t3160831282 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3160831282 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t214722923, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t214722923, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t214722923, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T214722923_H
#ifndef ENUMERATOR_T3238540930_H
#define ENUMERATOR_T3238540930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct  Enumerator_t3238540930 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1889681993 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ARPlaneAnchorGameObject_t3146329710 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3238540930, ___l_0)); }
	inline List_1_t1889681993 * get_l_0() const { return ___l_0; }
	inline List_1_t1889681993 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1889681993 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3238540930, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3238540930, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3238540930, ___current_3)); }
	inline ARPlaneAnchorGameObject_t3146329710 * get_current_3() const { return ___current_3; }
	inline ARPlaneAnchorGameObject_t3146329710 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ARPlaneAnchorGameObject_t3146329710 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3238540930_H
#ifndef RECT_T1992046353_H
#define RECT_T1992046353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t1992046353 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T1992046353_H
#ifndef ARLIGHTESTIMATE_T1280269830_H
#define ARLIGHTESTIMATE_T1280269830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARLightEstimate
struct  ARLightEstimate_t1280269830 
{
public:
	// System.Double UnityEngine.XR.iOS.ARLightEstimate::ambientIntensity
	double ___ambientIntensity_0;

public:
	inline static int32_t get_offset_of_ambientIntensity_0() { return static_cast<int32_t>(offsetof(ARLightEstimate_t1280269830, ___ambientIntensity_0)); }
	inline double get_ambientIntensity_0() const { return ___ambientIntensity_0; }
	inline double* get_address_of_ambientIntensity_0() { return &___ambientIntensity_0; }
	inline void set_ambientIntensity_0(double value)
	{
		___ambientIntensity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARLIGHTESTIMATE_T1280269830_H
#ifndef VECTOR4_T1376926224_H
#define VECTOR4_T1376926224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t1376926224 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t1376926224_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t1376926224  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t1376926224  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t1376926224  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t1376926224  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___zeroVector_5)); }
	inline Vector4_t1376926224  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t1376926224 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t1376926224  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___oneVector_6)); }
	inline Vector4_t1376926224  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t1376926224 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t1376926224  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t1376926224  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t1376926224 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t1376926224  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t1376926224  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t1376926224 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t1376926224  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T1376926224_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ARSIZE_T2759123995_H
#define ARSIZE_T2759123995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARSize
struct  ARSize_t2759123995 
{
public:
	// System.Double UnityEngine.XR.iOS.ARSize::width
	double ___width_0;
	// System.Double UnityEngine.XR.iOS.ARSize::height
	double ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ARSize_t2759123995, ___width_0)); }
	inline double get_width_0() const { return ___width_0; }
	inline double* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(double value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ARSize_t2759123995, ___height_1)); }
	inline double get_height_1() const { return ___height_1; }
	inline double* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(double value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSIZE_T2759123995_H
#ifndef SINGLE_T2847614712_H
#define SINGLE_T2847614712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2847614712 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2847614712, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2847614712_H
#ifndef UINT32_T3933237433_H
#define UINT32_T3933237433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t3933237433 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t3933237433, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T3933237433_H
#ifndef INT32_T3425510919_H
#define INT32_T3425510919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t3425510919 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t3425510919, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T3425510919_H
#ifndef QUATERNION_T3165733013_H
#define QUATERNION_T3165733013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t3165733013 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t3165733013_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t3165733013  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t3165733013  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t3165733013 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t3165733013  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T3165733013_H
#ifndef VECTOR3_T596762001_H
#define VECTOR3_T596762001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t596762001 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t596762001_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t596762001  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t596762001  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t596762001  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t596762001  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t596762001  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t596762001  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t596762001  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t596762001  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t596762001  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t596762001  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___zeroVector_4)); }
	inline Vector3_t596762001  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t596762001 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t596762001  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___oneVector_5)); }
	inline Vector3_t596762001  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t596762001 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t596762001  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___upVector_6)); }
	inline Vector3_t596762001  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t596762001 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t596762001  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___downVector_7)); }
	inline Vector3_t596762001  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t596762001 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t596762001  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___leftVector_8)); }
	inline Vector3_t596762001  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t596762001 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t596762001  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___rightVector_9)); }
	inline Vector3_t596762001  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t596762001 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t596762001  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___forwardVector_10)); }
	inline Vector3_t596762001  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t596762001 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t596762001  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___backVector_11)); }
	inline Vector3_t596762001  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t596762001 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t596762001  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t596762001  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t596762001 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t596762001  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t596762001  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t596762001 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t596762001  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T596762001_H
#ifndef MATRIX4X4_T1288378485_H
#define MATRIX4X4_T1288378485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1288378485 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1288378485_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1288378485  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1288378485  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1288378485  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1288378485 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1288378485  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1288378485  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1288378485 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1288378485  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1288378485_H
#ifndef ENUM_T473240710_H
#define ENUM_T473240710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t473240710  : public ValueType_t3433162460
{
public:

public:
};

struct Enum_t473240710_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t674980486* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t473240710_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t674980486* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t674980486** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t674980486* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t473240710_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t473240710_marshaled_com
{
};
#endif // ENUM_T473240710_H
#ifndef ARPOINT_T1649490841_H
#define ARPOINT_T1649490841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPoint
struct  ARPoint_t1649490841 
{
public:
	// System.Double UnityEngine.XR.iOS.ARPoint::x
	double ___x_0;
	// System.Double UnityEngine.XR.iOS.ARPoint::y
	double ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ARPoint_t1649490841, ___x_0)); }
	inline double get_x_0() const { return ___x_0; }
	inline double* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(double value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ARPoint_t1649490841, ___y_1)); }
	inline double get_y_1() const { return ___y_1; }
	inline double* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(double value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPOINT_T1649490841_H
#ifndef RESOLUTION_T3724314788_H
#define RESOLUTION_T3724314788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Resolution
struct  Resolution_t3724314788 
{
public:
	// System.Int32 UnityEngine.Resolution::m_Width
	int32_t ___m_Width_0;
	// System.Int32 UnityEngine.Resolution::m_Height
	int32_t ___m_Height_1;
	// System.Int32 UnityEngine.Resolution::m_RefreshRate
	int32_t ___m_RefreshRate_2;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(Resolution_t3724314788, ___m_Width_0)); }
	inline int32_t get_m_Width_0() const { return ___m_Width_0; }
	inline int32_t* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(int32_t value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(Resolution_t3724314788, ___m_Height_1)); }
	inline int32_t get_m_Height_1() const { return ___m_Height_1; }
	inline int32_t* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(int32_t value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_RefreshRate_2() { return static_cast<int32_t>(offsetof(Resolution_t3724314788, ___m_RefreshRate_2)); }
	inline int32_t get_m_RefreshRate_2() const { return ___m_RefreshRate_2; }
	inline int32_t* get_address_of_m_RefreshRate_2() { return &___m_RefreshRate_2; }
	inline void set_m_RefreshRate_2(int32_t value)
	{
		___m_RefreshRate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T3724314788_H
#ifndef BOOLEAN_T362855854_H
#define BOOLEAN_T362855854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t362855854 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t362855854, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t362855854_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t362855854_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t362855854_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T362855854_H
#ifndef U24ARRAYTYPEU3D24_T4048413920_H
#define U24ARRAYTYPEU3D24_T4048413920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t4048413920 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t4048413920__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T4048413920_H
#ifndef INT64_T2252457107_H
#define INT64_T2252457107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t2252457107 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t2252457107, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T2252457107_H
#ifndef ARTRACKINGSTATEREASON_T2365446872_H
#define ARTRACKINGSTATEREASON_T2365446872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t2365446872 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t2365446872, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T2365446872_H
#ifndef ARTRACKINGSTATE_T3679923289_H
#define ARTRACKINGSTATE_T3679923289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t3679923289 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t3679923289, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T3679923289_H
#ifndef FILTERMODE_T1528873982_H
#define FILTERMODE_T1528873982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t1528873982 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t1528873982, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T1528873982_H
#ifndef TOUCHPHASE_T2982654895_H
#define TOUCHPHASE_T2982654895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t2982654895 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t2982654895, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T2982654895_H
#ifndef RUNTIMEFIELDHANDLE_T1604667936_H
#define RUNTIMEFIELDHANDLE_T1604667936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1604667936 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1604667936, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1604667936_H
#ifndef TEXTUREWRAPMODE_T1836080092_H
#define TEXTUREWRAPMODE_T1836080092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t1836080092 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t1836080092, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T1836080092_H
#ifndef UNITYARSESSIONRUNOPTION_T2337414342_H
#define UNITYARSESSIONRUNOPTION_T2337414342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionRunOption
struct  UnityARSessionRunOption_t2337414342 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARSessionRunOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARSessionRunOption_t2337414342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONRUNOPTION_T2337414342_H
#ifndef SCREENORIENTATION_T527742245_H
#define SCREENORIENTATION_T527742245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t527742245 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t527742245, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T527742245_H
#ifndef DELEGATE_T69892740_H
#define DELEGATE_T69892740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t69892740  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t421390435 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___data_8)); }
	inline DelegateData_t421390435 * get_data_8() const { return ___data_8; }
	inline DelegateData_t421390435 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t421390435 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T69892740_H
#ifndef BUILTINRENDERTEXTURETYPE_T1209109390_H
#define BUILTINRENDERTEXTURETYPE_T1209109390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t1209109390 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t1209109390, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T1209109390_H
#ifndef CAMERAEVENT_T4234829282_H
#define CAMERAEVENT_T4234829282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t4234829282 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t4234829282, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T4234829282_H
#ifndef COMMANDBUFFER_T1027034615_H
#define COMMANDBUFFER_T1027034615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t1027034615  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t1027034615, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T1027034615_H
#ifndef TEXTUREFORMAT_T2347173699_H
#define TEXTUREFORMAT_T2347173699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2347173699 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2347173699, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2347173699_H
#ifndef UNITYARMATRIX4X4_T1132406930_H
#define UNITYARMATRIX4X4_T1132406930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_t1132406930 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_t1376926224  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_t1376926224  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_t1376926224  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_t1376926224  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column0_0)); }
	inline Vector4_t1376926224  get_column0_0() const { return ___column0_0; }
	inline Vector4_t1376926224 * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_t1376926224  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column1_1)); }
	inline Vector4_t1376926224  get_column1_1() const { return ___column1_1; }
	inline Vector4_t1376926224 * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_t1376926224  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column2_2)); }
	inline Vector4_t1376926224  get_column2_2() const { return ___column2_2; }
	inline Vector4_t1376926224 * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_t1376926224  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column3_3)); }
	inline Vector4_t1376926224  get_column3_3() const { return ___column3_3; }
	inline Vector4_t1376926224 * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_t1376926224  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_T1132406930_H
#ifndef ARTRACKINGQUALITY_T1141893651_H
#define ARTRACKINGQUALITY_T1141893651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingQuality
struct  ARTrackingQuality_t1141893651 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARTrackingQuality::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingQuality_t1141893651, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGQUALITY_T1141893651_H
#ifndef TOUCHTYPE_T706641163_H
#define TOUCHTYPE_T706641163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t706641163 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t706641163, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T706641163_H
#ifndef ARRECT_T3161395965_H
#define ARRECT_T3161395965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARRect
struct  ARRect_t3161395965 
{
public:
	// UnityEngine.XR.iOS.ARPoint UnityEngine.XR.iOS.ARRect::origin
	ARPoint_t1649490841  ___origin_0;
	// UnityEngine.XR.iOS.ARSize UnityEngine.XR.iOS.ARRect::size
	ARSize_t2759123995  ___size_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(ARRect_t3161395965, ___origin_0)); }
	inline ARPoint_t1649490841  get_origin_0() const { return ___origin_0; }
	inline ARPoint_t1649490841 * get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(ARPoint_t1649490841  value)
	{
		___origin_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ARRect_t3161395965, ___size_1)); }
	inline ARSize_t2759123995  get_size_1() const { return ___size_1; }
	inline ARSize_t2759123995 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(ARSize_t2759123995  value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRECT_T3161395965_H
#ifndef ARANCHOR_T4044008708_H
#define ARANCHOR_T4044008708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARAnchor
struct  ARAnchor_t4044008708 
{
public:
	// System.String UnityEngine.XR.iOS.ARAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARAnchor::transform
	Matrix4x4_t1288378485  ___transform_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARAnchor_t4044008708, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARAnchor_t4044008708, ___transform_1)); }
	inline Matrix4x4_t1288378485  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t1288378485 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t1288378485  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARAnchor
struct ARAnchor_t4044008708_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t1288378485  ___transform_1;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARAnchor
struct ARAnchor_t4044008708_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t1288378485  ___transform_1;
};
#endif // ARANCHOR_T4044008708_H
#ifndef PARTICLE_T1268635397_H
#define PARTICLE_T1268635397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/Particle
struct  Particle_t1268635397 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t596762001  ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t596762001  ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t596762001  ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t596762001  ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t596762001  ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t596762001  ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t596762001  ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t596762001  ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t2499566028  ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_12;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_13;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_Position_0)); }
	inline Vector3_t596762001  get_m_Position_0() const { return ___m_Position_0; }
	inline Vector3_t596762001 * get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(Vector3_t596762001  value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_1() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_Velocity_1)); }
	inline Vector3_t596762001  get_m_Velocity_1() const { return ___m_Velocity_1; }
	inline Vector3_t596762001 * get_address_of_m_Velocity_1() { return &___m_Velocity_1; }
	inline void set_m_Velocity_1(Vector3_t596762001  value)
	{
		___m_Velocity_1 = value;
	}

	inline static int32_t get_offset_of_m_AnimatedVelocity_2() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_AnimatedVelocity_2)); }
	inline Vector3_t596762001  get_m_AnimatedVelocity_2() const { return ___m_AnimatedVelocity_2; }
	inline Vector3_t596762001 * get_address_of_m_AnimatedVelocity_2() { return &___m_AnimatedVelocity_2; }
	inline void set_m_AnimatedVelocity_2(Vector3_t596762001  value)
	{
		___m_AnimatedVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_InitialVelocity_3() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_InitialVelocity_3)); }
	inline Vector3_t596762001  get_m_InitialVelocity_3() const { return ___m_InitialVelocity_3; }
	inline Vector3_t596762001 * get_address_of_m_InitialVelocity_3() { return &___m_InitialVelocity_3; }
	inline void set_m_InitialVelocity_3(Vector3_t596762001  value)
	{
		___m_InitialVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_AxisOfRotation_4() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_AxisOfRotation_4)); }
	inline Vector3_t596762001  get_m_AxisOfRotation_4() const { return ___m_AxisOfRotation_4; }
	inline Vector3_t596762001 * get_address_of_m_AxisOfRotation_4() { return &___m_AxisOfRotation_4; }
	inline void set_m_AxisOfRotation_4(Vector3_t596762001  value)
	{
		___m_AxisOfRotation_4 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_Rotation_5)); }
	inline Vector3_t596762001  get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline Vector3_t596762001 * get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(Vector3_t596762001  value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocity_6() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_AngularVelocity_6)); }
	inline Vector3_t596762001  get_m_AngularVelocity_6() const { return ___m_AngularVelocity_6; }
	inline Vector3_t596762001 * get_address_of_m_AngularVelocity_6() { return &___m_AngularVelocity_6; }
	inline void set_m_AngularVelocity_6(Vector3_t596762001  value)
	{
		___m_AngularVelocity_6 = value;
	}

	inline static int32_t get_offset_of_m_StartSize_7() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_StartSize_7)); }
	inline Vector3_t596762001  get_m_StartSize_7() const { return ___m_StartSize_7; }
	inline Vector3_t596762001 * get_address_of_m_StartSize_7() { return &___m_StartSize_7; }
	inline void set_m_StartSize_7(Vector3_t596762001  value)
	{
		___m_StartSize_7 = value;
	}

	inline static int32_t get_offset_of_m_StartColor_8() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_StartColor_8)); }
	inline Color32_t2499566028  get_m_StartColor_8() const { return ___m_StartColor_8; }
	inline Color32_t2499566028 * get_address_of_m_StartColor_8() { return &___m_StartColor_8; }
	inline void set_m_StartColor_8(Color32_t2499566028  value)
	{
		___m_StartColor_8 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_9() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_RandomSeed_9)); }
	inline uint32_t get_m_RandomSeed_9() const { return ___m_RandomSeed_9; }
	inline uint32_t* get_address_of_m_RandomSeed_9() { return &___m_RandomSeed_9; }
	inline void set_m_RandomSeed_9(uint32_t value)
	{
		___m_RandomSeed_9 = value;
	}

	inline static int32_t get_offset_of_m_Lifetime_10() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_Lifetime_10)); }
	inline float get_m_Lifetime_10() const { return ___m_Lifetime_10; }
	inline float* get_address_of_m_Lifetime_10() { return &___m_Lifetime_10; }
	inline void set_m_Lifetime_10(float value)
	{
		___m_Lifetime_10 = value;
	}

	inline static int32_t get_offset_of_m_StartLifetime_11() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_StartLifetime_11)); }
	inline float get_m_StartLifetime_11() const { return ___m_StartLifetime_11; }
	inline float* get_address_of_m_StartLifetime_11() { return &___m_StartLifetime_11; }
	inline void set_m_StartLifetime_11(float value)
	{
		___m_StartLifetime_11 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator0_12() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_EmitAccumulator0_12)); }
	inline float get_m_EmitAccumulator0_12() const { return ___m_EmitAccumulator0_12; }
	inline float* get_address_of_m_EmitAccumulator0_12() { return &___m_EmitAccumulator0_12; }
	inline void set_m_EmitAccumulator0_12(float value)
	{
		___m_EmitAccumulator0_12 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator1_13() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_EmitAccumulator1_13)); }
	inline float get_m_EmitAccumulator1_13() const { return ___m_EmitAccumulator1_13; }
	inline float* get_address_of_m_EmitAccumulator1_13() { return &___m_EmitAccumulator1_13; }
	inline void set_m_EmitAccumulator1_13(float value)
	{
		___m_EmitAccumulator1_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLE_T1268635397_H
#ifndef ARTEXTUREHANDLES_T1375402930_H
#define ARTEXTUREHANDLES_T1375402930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTextureHandles
struct  ARTextureHandles_t1375402930 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureY
	IntPtr_t ___textureY_0;
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureCbCr
	IntPtr_t ___textureCbCr_1;

public:
	inline static int32_t get_offset_of_textureY_0() { return static_cast<int32_t>(offsetof(ARTextureHandles_t1375402930, ___textureY_0)); }
	inline IntPtr_t get_textureY_0() const { return ___textureY_0; }
	inline IntPtr_t* get_address_of_textureY_0() { return &___textureY_0; }
	inline void set_textureY_0(IntPtr_t value)
	{
		___textureY_0 = value;
	}

	inline static int32_t get_offset_of_textureCbCr_1() { return static_cast<int32_t>(offsetof(ARTextureHandles_t1375402930, ___textureCbCr_1)); }
	inline IntPtr_t get_textureCbCr_1() const { return ___textureCbCr_1; }
	inline IntPtr_t* get_address_of_textureCbCr_1() { return &___textureCbCr_1; }
	inline void set_textureCbCr_1(IntPtr_t value)
	{
		___textureCbCr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTEXTUREHANDLES_T1375402930_H
#ifndef ARERRORCODE_T907243840_H
#define ARERRORCODE_T907243840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARErrorCode
struct  ARErrorCode_t907243840 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARErrorCode::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARErrorCode_t907243840, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARERRORCODE_T907243840_H
#ifndef ARHITTESTRESULTTYPE_T270086150_H
#define ARHITTESTRESULTTYPE_T270086150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResultType
struct  ARHitTestResultType_t270086150 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARHitTestResultType::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARHitTestResultType_t270086150, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARHITTESTRESULTTYPE_T270086150_H
#ifndef ARPLANEANCHORALIGNMENT_T185787390_H
#define ARPLANEANCHORALIGNMENT_T185787390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t185787390 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t185787390, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T185787390_H
#ifndef OBJECT_T1008057425_H
#define OBJECT_T1008057425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1008057425  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1008057425, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1008057425_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1008057425_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1008057425_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1008057425_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1008057425_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1316871563_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1316871563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1316871563  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1316871563_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-8E7629AD5AF686202B8CB7C014505C432FFE31E6
	U24ArrayTypeU3D24_t4048413920  ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1316871563_StaticFields, ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0)); }
	inline U24ArrayTypeU3D24_t4048413920  get_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() const { return ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline U24ArrayTypeU3D24_t4048413920 * get_address_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return &___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline void set_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(U24ArrayTypeU3D24_t4048413920  value)
	{
		___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1316871563_H
#ifndef UNITYARALIGNMENT_T1226993326_H
#define UNITYARALIGNMENT_T1226993326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAlignment
struct  UnityARAlignment_t1226993326 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARAlignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARAlignment_t1226993326, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARALIGNMENT_T1226993326_H
#ifndef UNITYARPLANEDETECTION_T2335225179_H
#define UNITYARPLANEDETECTION_T2335225179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARPlaneDetection
struct  UnityARPlaneDetection_t2335225179 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARPlaneDetection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARPlaneDetection_t2335225179, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARPLANEDETECTION_T2335225179_H
#ifndef GAMEOBJECT_T3649338848_H
#define GAMEOBJECT_T3649338848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t3649338848  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T3649338848_H
#ifndef COMPONENT_T531478471_H
#define COMPONENT_T531478471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t531478471  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T531478471_H
#ifndef TEXTURE_T1132728222_H
#define TEXTURE_T1132728222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t1132728222  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T1132728222_H
#ifndef ARKITWORLDTACKINGSESSIONCONFIGURATION_T2186350340_H
#define ARKITWORLDTACKINGSESSIONCONFIGURATION_T2186350340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct  ARKitWorldTackingSessionConfiguration_t2186350340 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t2186350340, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t2186350340, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t2186350340, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t2186350340, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
#endif // ARKITWORLDTACKINGSESSIONCONFIGURATION_T2186350340_H
#ifndef MATERIAL_T4055262778_H
#define MATERIAL_T4055262778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t4055262778  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T4055262778_H
#ifndef RENDERTARGETIDENTIFIER_T2478716510_H
#define RENDERTARGETIDENTIFIER_T2478716510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_t2478716510 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2478716510, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2478716510, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2478716510, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_T2478716510_H
#ifndef UNITYARCAMERA_T2376600594_H
#define UNITYARCAMERA_T2376600594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARCamera
struct  UnityARCamera_t2376600594 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::worldTransform
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.Vector3[] UnityEngine.XR.iOS.UnityARCamera::pointCloudData
	Vector3U5BU5D_t4153953548* ___pointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t1132406930  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t1132406930  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t1132406930  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t1132406930  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_pointCloudData_4() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___pointCloudData_4)); }
	inline Vector3U5BU5D_t4153953548* get_pointCloudData_4() const { return ___pointCloudData_4; }
	inline Vector3U5BU5D_t4153953548** get_address_of_pointCloudData_4() { return &___pointCloudData_4; }
	inline void set_pointCloudData_4(Vector3U5BU5D_t4153953548* value)
	{
		___pointCloudData_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t2376600594_marshaled_pinvoke
{
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t596762001 * ___pointCloudData_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t2376600594_marshaled_com
{
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t596762001 * ___pointCloudData_4;
};
#endif // UNITYARCAMERA_T2376600594_H
#ifndef ARKITSESSIONCONFIGURATION_T3142208763_H
#define ARKITSESSIONCONFIGURATION_T3142208763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitSessionConfiguration
struct  ARKitSessionConfiguration_t3142208763 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitSessionConfiguration::alignment
	int32_t ___alignment_0;
	// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_2;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t3142208763, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_1() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t3142208763, ___getPointCloudData_1)); }
	inline bool get_getPointCloudData_1() const { return ___getPointCloudData_1; }
	inline bool* get_address_of_getPointCloudData_1() { return &___getPointCloudData_1; }
	inline void set_getPointCloudData_1(bool value)
	{
		___getPointCloudData_1 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_2() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t3142208763, ___enableLightEstimation_2)); }
	inline bool get_enableLightEstimation_2() const { return ___enableLightEstimation_2; }
	inline bool* get_address_of_enableLightEstimation_2() { return &___enableLightEstimation_2; }
	inline void set_enableLightEstimation_2(bool value)
	{
		___enableLightEstimation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t3142208763_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___getPointCloudData_1;
	int32_t ___enableLightEstimation_2;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t3142208763_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___getPointCloudData_1;
	int32_t ___enableLightEstimation_2;
};
#endif // ARKITSESSIONCONFIGURATION_T3142208763_H
#ifndef UNITYARHITTESTRESULT_T1002809393_H
#define UNITYARHITTESTRESULT_T1002809393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestResult
struct  UnityARHitTestResult_t1002809393 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.UnityARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.UnityARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARHitTestResult::localTransform
	Matrix4x4_t1288378485  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARHitTestResult::worldTransform
	Matrix4x4_t1288378485  ___worldTransform_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityARHitTestResult::anchor
	IntPtr_t ___anchor_4;
	// System.Boolean UnityEngine.XR.iOS.UnityARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___localTransform_2)); }
	inline Matrix4x4_t1288378485  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t1288378485 * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t1288378485  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___worldTransform_3)); }
	inline Matrix4x4_t1288378485  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t1288378485 * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t1288378485  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchor_4() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___anchor_4)); }
	inline IntPtr_t get_anchor_4() const { return ___anchor_4; }
	inline IntPtr_t* get_address_of_anchor_4() { return &___anchor_4; }
	inline void set_anchor_4(IntPtr_t value)
	{
		___anchor_4 = value;
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t1002809393_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1288378485  ___localTransform_2;
	Matrix4x4_t1288378485  ___worldTransform_3;
	intptr_t ___anchor_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t1002809393_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1288378485  ___localTransform_2;
	Matrix4x4_t1288378485  ___worldTransform_3;
	intptr_t ___anchor_4;
	int32_t ___isValid_5;
};
#endif // UNITYARHITTESTRESULT_T1002809393_H
#ifndef INTERNAL_UNITYARCAMERA_T1506237410_H
#define INTERNAL_UNITYARCAMERA_T1506237410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.internal_UnityARCamera
struct  internal_UnityARCamera_t1506237410 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::worldTransform
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.internal_UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.internal_UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// System.UInt32 UnityEngine.XR.iOS.internal_UnityARCamera::getPointCloudData
	uint32_t ___getPointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t1132406930  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t1132406930  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t1132406930  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t1132406930  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_4() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___getPointCloudData_4)); }
	inline uint32_t get_getPointCloudData_4() const { return ___getPointCloudData_4; }
	inline uint32_t* get_address_of_getPointCloudData_4() { return &___getPointCloudData_4; }
	inline void set_getPointCloudData_4(uint32_t value)
	{
		___getPointCloudData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_UNITYARCAMERA_T1506237410_H
#ifndef TOUCH_T3132414106_H
#define TOUCH_T3132414106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t3132414106 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t59524482  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t59524482  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t59524482  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Position_1)); }
	inline Vector2_t59524482  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t59524482 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t59524482  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_RawPosition_2)); }
	inline Vector2_t59524482  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t59524482 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t59524482  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_PositionDelta_3)); }
	inline Vector2_t59524482  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t59524482 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t59524482  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T3132414106_H
#ifndef MULTICASTDELEGATE_T1138444986_H
#define MULTICASTDELEGATE_T1138444986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1138444986  : public Delegate_t69892740
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1138444986 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1138444986 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1138444986, ___prev_9)); }
	inline MulticastDelegate_t1138444986 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1138444986 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1138444986 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1138444986, ___kpm_next_10)); }
	inline MulticastDelegate_t1138444986 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1138444986 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1138444986 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1138444986_H
#ifndef UNITYARANCHORDATA_T1511263172_H
#define UNITYARANCHORDATA_T1511263172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorData
struct  UnityARAnchorData_t1511263172 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARAnchorData::ptrIdentifier
	IntPtr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARAnchorData::transform
	UnityARMatrix4x4_t1132406930  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.UnityARAnchorData::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::center
	Vector4_t1376926224  ___center_3;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::extent
	Vector4_t1376926224  ___extent_4;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___ptrIdentifier_0)); }
	inline IntPtr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline IntPtr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(IntPtr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___transform_1)); }
	inline UnityARMatrix4x4_t1132406930  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t1132406930  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___center_3)); }
	inline Vector4_t1376926224  get_center_3() const { return ___center_3; }
	inline Vector4_t1376926224 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector4_t1376926224  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___extent_4)); }
	inline Vector4_t1376926224  get_extent_4() const { return ___extent_4; }
	inline Vector4_t1376926224 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector4_t1376926224  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORDATA_T1511263172_H
#ifndef ARCAMERA_T3388895497_H
#define ARCAMERA_T3388895497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARCamera
struct  ARCamera_t3388895497 
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARCamera::worldTransform
	Matrix4x4_t1288378485  ___worldTransform_0;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::eulerAngles
	Vector3_t596762001  ___eulerAngles_1;
	// UnityEngine.XR.iOS.ARTrackingQuality UnityEngine.XR.iOS.ARCamera::trackingQuality
	int64_t ___trackingQuality_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row1
	Vector3_t596762001  ___intrinsics_row1_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row2
	Vector3_t596762001  ___intrinsics_row2_4;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row3
	Vector3_t596762001  ___intrinsics_row3_5;
	// UnityEngine.XR.iOS.ARSize UnityEngine.XR.iOS.ARCamera::imageResolution
	ARSize_t2759123995  ___imageResolution_6;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(ARCamera_t3388895497, ___worldTransform_0)); }
	inline Matrix4x4_t1288378485  get_worldTransform_0() const { return ___worldTransform_0; }
	inline Matrix4x4_t1288378485 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(Matrix4x4_t1288378485  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_eulerAngles_1() { return static_cast<int32_t>(offsetof(ARCamera_t3388895497, ___eulerAngles_1)); }
	inline Vector3_t596762001  get_eulerAngles_1() const { return ___eulerAngles_1; }
	inline Vector3_t596762001 * get_address_of_eulerAngles_1() { return &___eulerAngles_1; }
	inline void set_eulerAngles_1(Vector3_t596762001  value)
	{
		___eulerAngles_1 = value;
	}

	inline static int32_t get_offset_of_trackingQuality_2() { return static_cast<int32_t>(offsetof(ARCamera_t3388895497, ___trackingQuality_2)); }
	inline int64_t get_trackingQuality_2() const { return ___trackingQuality_2; }
	inline int64_t* get_address_of_trackingQuality_2() { return &___trackingQuality_2; }
	inline void set_trackingQuality_2(int64_t value)
	{
		___trackingQuality_2 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row1_3() { return static_cast<int32_t>(offsetof(ARCamera_t3388895497, ___intrinsics_row1_3)); }
	inline Vector3_t596762001  get_intrinsics_row1_3() const { return ___intrinsics_row1_3; }
	inline Vector3_t596762001 * get_address_of_intrinsics_row1_3() { return &___intrinsics_row1_3; }
	inline void set_intrinsics_row1_3(Vector3_t596762001  value)
	{
		___intrinsics_row1_3 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row2_4() { return static_cast<int32_t>(offsetof(ARCamera_t3388895497, ___intrinsics_row2_4)); }
	inline Vector3_t596762001  get_intrinsics_row2_4() const { return ___intrinsics_row2_4; }
	inline Vector3_t596762001 * get_address_of_intrinsics_row2_4() { return &___intrinsics_row2_4; }
	inline void set_intrinsics_row2_4(Vector3_t596762001  value)
	{
		___intrinsics_row2_4 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row3_5() { return static_cast<int32_t>(offsetof(ARCamera_t3388895497, ___intrinsics_row3_5)); }
	inline Vector3_t596762001  get_intrinsics_row3_5() const { return ___intrinsics_row3_5; }
	inline Vector3_t596762001 * get_address_of_intrinsics_row3_5() { return &___intrinsics_row3_5; }
	inline void set_intrinsics_row3_5(Vector3_t596762001  value)
	{
		___intrinsics_row3_5 = value;
	}

	inline static int32_t get_offset_of_imageResolution_6() { return static_cast<int32_t>(offsetof(ARCamera_t3388895497, ___imageResolution_6)); }
	inline ARSize_t2759123995  get_imageResolution_6() const { return ___imageResolution_6; }
	inline ARSize_t2759123995 * get_address_of_imageResolution_6() { return &___imageResolution_6; }
	inline void set_imageResolution_6(ARSize_t2759123995  value)
	{
		___imageResolution_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCAMERA_T3388895497_H
#ifndef ARPLANEANCHOR_T1843157284_H
#define ARPLANEANCHOR_T1843157284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchor
struct  ARPlaneAnchor_t1843157284 
{
public:
	// System.String UnityEngine.XR.iOS.ARPlaneAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARPlaneAnchor::transform
	Matrix4x4_t1288378485  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.ARPlaneAnchor::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::center
	Vector3_t596762001  ___center_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::extent
	Vector3_t596762001  ___extent_4;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___transform_1)); }
	inline Matrix4x4_t1288378485  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t1288378485 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t1288378485  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___center_3)); }
	inline Vector3_t596762001  get_center_3() const { return ___center_3; }
	inline Vector3_t596762001 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector3_t596762001  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___extent_4)); }
	inline Vector3_t596762001  get_extent_4() const { return ___extent_4; }
	inline Vector3_t596762001 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector3_t596762001  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1843157284_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t1288378485  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t596762001  ___center_3;
	Vector3_t596762001  ___extent_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1843157284_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t1288378485  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t596762001  ___center_3;
	Vector3_t596762001  ___extent_4;
};
#endif // ARPLANEANCHOR_T1843157284_H
#ifndef ARHITTESTRESULT_T1803723679_H
#define ARHITTESTRESULT_T1803723679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResult
struct  ARHitTestResult_t1803723679 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.ARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.ARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::localTransform
	Matrix4x4_t1288378485  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::worldTransform
	Matrix4x4_t1288378485  ___worldTransform_3;
	// System.String UnityEngine.XR.iOS.ARHitTestResult::anchorIdentifier
	String_t* ___anchorIdentifier_4;
	// System.Boolean UnityEngine.XR.iOS.ARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___localTransform_2)); }
	inline Matrix4x4_t1288378485  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t1288378485 * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t1288378485  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___worldTransform_3)); }
	inline Matrix4x4_t1288378485  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t1288378485 * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t1288378485  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchorIdentifier_4() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___anchorIdentifier_4)); }
	inline String_t* get_anchorIdentifier_4() const { return ___anchorIdentifier_4; }
	inline String_t** get_address_of_anchorIdentifier_4() { return &___anchorIdentifier_4; }
	inline void set_anchorIdentifier_4(String_t* value)
	{
		___anchorIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___anchorIdentifier_4), value);
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t1803723679_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1288378485  ___localTransform_2;
	Matrix4x4_t1288378485  ___worldTransform_3;
	char* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t1803723679_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1288378485  ___localTransform_2;
	Matrix4x4_t1288378485  ___worldTransform_3;
	Il2CppChar* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
#endif // ARHITTESTRESULT_T1803723679_H
#ifndef ARPLANEANCHORGAMEOBJECT_T3146329710_H
#define ARPLANEANCHORGAMEOBJECT_T3146329710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorGameObject
struct  ARPlaneAnchorGameObject_t3146329710  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.ARPlaneAnchorGameObject::gameObject
	GameObject_t3649338848 * ___gameObject_0;
	// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.ARPlaneAnchorGameObject::planeAnchor
	ARPlaneAnchor_t1843157284  ___planeAnchor_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_t3146329710, ___gameObject_0)); }
	inline GameObject_t3649338848 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t3649338848 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t3649338848 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_planeAnchor_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_t3146329710, ___planeAnchor_1)); }
	inline ARPlaneAnchor_t1843157284  get_planeAnchor_1() const { return ___planeAnchor_1; }
	inline ARPlaneAnchor_t1843157284 * get_address_of_planeAnchor_1() { return &___planeAnchor_1; }
	inline void set_planeAnchor_1(ARPlaneAnchor_t1843157284  value)
	{
		___planeAnchor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORGAMEOBJECT_T3146329710_H
#ifndef BEHAVIOUR_T2200997390_H
#define BEHAVIOUR_T2200997390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2200997390  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2200997390_H
#ifndef TEXTURE2D_T2870930912_H
#define TEXTURE2D_T2870930912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t2870930912  : public Texture_t1132728222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T2870930912_H
#ifndef UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#define UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct  UnityARSessionNativeInterface_t3869411053  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::m_NativeARSession
	IntPtr_t ___m_NativeARSession_5;

public:
	inline static int32_t get_offset_of_m_NativeARSession_5() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053, ___m_NativeARSession_5)); }
	inline IntPtr_t get_m_NativeARSession_5() const { return ___m_NativeARSession_5; }
	inline IntPtr_t* get_address_of_m_NativeARSession_5() { return &___m_NativeARSession_5; }
	inline void set_m_NativeARSession_5(IntPtr_t value)
	{
		___m_NativeARSession_5 = value;
	}
};

struct UnityARSessionNativeInterface_t3869411053_StaticFields
{
public:
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFrameUpdatedEvent
	ARFrameUpdate_t3333962149 * ___ARFrameUpdatedEvent_0;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorAddedEvent
	ARAnchorAdded_t1580091102 * ___ARAnchorAddedEvent_1;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorUpdatedEvent
	ARAnchorUpdated_t1492128022 * ___ARAnchorUpdatedEvent_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorRemovedEvent
	ARAnchorRemoved_t3663543047 * ___ARAnchorRemovedEvent_3;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionFailedEvent
	ARSessionFailed_t3599980200 * ___ARSessionFailedEvent_4;
	// UnityEngine.XR.iOS.UnityARCamera UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_Camera
	UnityARCamera_t2376600594  ___s_Camera_6;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_UnityARSessionNativeInterface
	UnityARSessionNativeInterface_t3869411053 * ___s_UnityARSessionNativeInterface_7;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache0
	internal_ARFrameUpdate_t2726389622 * ___U3CU3Ef__mgU24cache0_8;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache1
	internal_ARAnchorAdded_t3791587037 * ___U3CU3Ef__mgU24cache1_9;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache2
	internal_ARAnchorUpdated_t2928809250 * ___U3CU3Ef__mgU24cache2_10;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache3
	internal_ARAnchorRemoved_t409959511 * ___U3CU3Ef__mgU24cache3_11;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache4
	ARSessionFailed_t3599980200 * ___U3CU3Ef__mgU24cache4_12;

public:
	inline static int32_t get_offset_of_ARFrameUpdatedEvent_0() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARFrameUpdatedEvent_0)); }
	inline ARFrameUpdate_t3333962149 * get_ARFrameUpdatedEvent_0() const { return ___ARFrameUpdatedEvent_0; }
	inline ARFrameUpdate_t3333962149 ** get_address_of_ARFrameUpdatedEvent_0() { return &___ARFrameUpdatedEvent_0; }
	inline void set_ARFrameUpdatedEvent_0(ARFrameUpdate_t3333962149 * value)
	{
		___ARFrameUpdatedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ARFrameUpdatedEvent_0), value);
	}

	inline static int32_t get_offset_of_ARAnchorAddedEvent_1() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorAddedEvent_1)); }
	inline ARAnchorAdded_t1580091102 * get_ARAnchorAddedEvent_1() const { return ___ARAnchorAddedEvent_1; }
	inline ARAnchorAdded_t1580091102 ** get_address_of_ARAnchorAddedEvent_1() { return &___ARAnchorAddedEvent_1; }
	inline void set_ARAnchorAddedEvent_1(ARAnchorAdded_t1580091102 * value)
	{
		___ARAnchorAddedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorAddedEvent_1), value);
	}

	inline static int32_t get_offset_of_ARAnchorUpdatedEvent_2() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorUpdatedEvent_2)); }
	inline ARAnchorUpdated_t1492128022 * get_ARAnchorUpdatedEvent_2() const { return ___ARAnchorUpdatedEvent_2; }
	inline ARAnchorUpdated_t1492128022 ** get_address_of_ARAnchorUpdatedEvent_2() { return &___ARAnchorUpdatedEvent_2; }
	inline void set_ARAnchorUpdatedEvent_2(ARAnchorUpdated_t1492128022 * value)
	{
		___ARAnchorUpdatedEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorUpdatedEvent_2), value);
	}

	inline static int32_t get_offset_of_ARAnchorRemovedEvent_3() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorRemovedEvent_3)); }
	inline ARAnchorRemoved_t3663543047 * get_ARAnchorRemovedEvent_3() const { return ___ARAnchorRemovedEvent_3; }
	inline ARAnchorRemoved_t3663543047 ** get_address_of_ARAnchorRemovedEvent_3() { return &___ARAnchorRemovedEvent_3; }
	inline void set_ARAnchorRemovedEvent_3(ARAnchorRemoved_t3663543047 * value)
	{
		___ARAnchorRemovedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorRemovedEvent_3), value);
	}

	inline static int32_t get_offset_of_ARSessionFailedEvent_4() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARSessionFailedEvent_4)); }
	inline ARSessionFailed_t3599980200 * get_ARSessionFailedEvent_4() const { return ___ARSessionFailedEvent_4; }
	inline ARSessionFailed_t3599980200 ** get_address_of_ARSessionFailedEvent_4() { return &___ARSessionFailedEvent_4; }
	inline void set_ARSessionFailedEvent_4(ARSessionFailed_t3599980200 * value)
	{
		___ARSessionFailedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionFailedEvent_4), value);
	}

	inline static int32_t get_offset_of_s_Camera_6() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___s_Camera_6)); }
	inline UnityARCamera_t2376600594  get_s_Camera_6() const { return ___s_Camera_6; }
	inline UnityARCamera_t2376600594 * get_address_of_s_Camera_6() { return &___s_Camera_6; }
	inline void set_s_Camera_6(UnityARCamera_t2376600594  value)
	{
		___s_Camera_6 = value;
	}

	inline static int32_t get_offset_of_s_UnityARSessionNativeInterface_7() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___s_UnityARSessionNativeInterface_7)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_s_UnityARSessionNativeInterface_7() const { return ___s_UnityARSessionNativeInterface_7; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_s_UnityARSessionNativeInterface_7() { return &___s_UnityARSessionNativeInterface_7; }
	inline void set_s_UnityARSessionNativeInterface_7(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___s_UnityARSessionNativeInterface_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnityARSessionNativeInterface_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline internal_ARFrameUpdate_t2726389622 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline internal_ARFrameUpdate_t2726389622 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(internal_ARFrameUpdate_t2726389622 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_9() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache1_9)); }
	inline internal_ARAnchorAdded_t3791587037 * get_U3CU3Ef__mgU24cache1_9() const { return ___U3CU3Ef__mgU24cache1_9; }
	inline internal_ARAnchorAdded_t3791587037 ** get_address_of_U3CU3Ef__mgU24cache1_9() { return &___U3CU3Ef__mgU24cache1_9; }
	inline void set_U3CU3Ef__mgU24cache1_9(internal_ARAnchorAdded_t3791587037 * value)
	{
		___U3CU3Ef__mgU24cache1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_10() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache2_10)); }
	inline internal_ARAnchorUpdated_t2928809250 * get_U3CU3Ef__mgU24cache2_10() const { return ___U3CU3Ef__mgU24cache2_10; }
	inline internal_ARAnchorUpdated_t2928809250 ** get_address_of_U3CU3Ef__mgU24cache2_10() { return &___U3CU3Ef__mgU24cache2_10; }
	inline void set_U3CU3Ef__mgU24cache2_10(internal_ARAnchorUpdated_t2928809250 * value)
	{
		___U3CU3Ef__mgU24cache2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_11() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache3_11)); }
	inline internal_ARAnchorRemoved_t409959511 * get_U3CU3Ef__mgU24cache3_11() const { return ___U3CU3Ef__mgU24cache3_11; }
	inline internal_ARAnchorRemoved_t409959511 ** get_address_of_U3CU3Ef__mgU24cache3_11() { return &___U3CU3Ef__mgU24cache3_11; }
	inline void set_U3CU3Ef__mgU24cache3_11(internal_ARAnchorRemoved_t409959511 * value)
	{
		___U3CU3Ef__mgU24cache3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_12() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache4_12)); }
	inline ARSessionFailed_t3599980200 * get_U3CU3Ef__mgU24cache4_12() const { return ___U3CU3Ef__mgU24cache4_12; }
	inline ARSessionFailed_t3599980200 ** get_address_of_U3CU3Ef__mgU24cache4_12() { return &___U3CU3Ef__mgU24cache4_12; }
	inline void set_U3CU3Ef__mgU24cache4_12(ARSessionFailed_t3599980200 * value)
	{
		___U3CU3Ef__mgU24cache4_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#ifndef ARANCHORADDED_T1580091102_H
#define ARANCHORADDED_T1580091102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct  ARAnchorAdded_t1580091102  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORADDED_T1580091102_H
#ifndef ARANCHORUPDATED_T1492128022_H
#define ARANCHORUPDATED_T1492128022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct  ARAnchorUpdated_t1492128022  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORUPDATED_T1492128022_H
#ifndef ARANCHORREMOVED_T3663543047_H
#define ARANCHORREMOVED_T3663543047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct  ARAnchorRemoved_t3663543047  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORREMOVED_T3663543047_H
#ifndef MESHFILTER_T2678471223_H
#define MESHFILTER_T2678471223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t2678471223  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T2678471223_H
#ifndef TRANSFORM_T3933397867_H
#define TRANSFORM_T3933397867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3933397867  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3933397867_H
#ifndef ARFRAME_T3142156576_H
#define ARFRAME_T3142156576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARFrame
struct  ARFrame_t3142156576 
{
public:
	// System.Double UnityEngine.XR.iOS.ARFrame::timestamp
	double ___timestamp_0;
	// System.IntPtr UnityEngine.XR.iOS.ARFrame::capturedImage
	IntPtr_t ___capturedImage_1;
	// UnityEngine.XR.iOS.ARCamera UnityEngine.XR.iOS.ARFrame::camera
	ARCamera_t3388895497  ___camera_2;
	// UnityEngine.XR.iOS.ARLightEstimate UnityEngine.XR.iOS.ARFrame::lightEstimate
	ARLightEstimate_t1280269830  ___lightEstimate_3;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(ARFrame_t3142156576, ___timestamp_0)); }
	inline double get_timestamp_0() const { return ___timestamp_0; }
	inline double* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(double value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_capturedImage_1() { return static_cast<int32_t>(offsetof(ARFrame_t3142156576, ___capturedImage_1)); }
	inline IntPtr_t get_capturedImage_1() const { return ___capturedImage_1; }
	inline IntPtr_t* get_address_of_capturedImage_1() { return &___capturedImage_1; }
	inline void set_capturedImage_1(IntPtr_t value)
	{
		___capturedImage_1 = value;
	}

	inline static int32_t get_offset_of_camera_2() { return static_cast<int32_t>(offsetof(ARFrame_t3142156576, ___camera_2)); }
	inline ARCamera_t3388895497  get_camera_2() const { return ___camera_2; }
	inline ARCamera_t3388895497 * get_address_of_camera_2() { return &___camera_2; }
	inline void set_camera_2(ARCamera_t3388895497  value)
	{
		___camera_2 = value;
	}

	inline static int32_t get_offset_of_lightEstimate_3() { return static_cast<int32_t>(offsetof(ARFrame_t3142156576, ___lightEstimate_3)); }
	inline ARLightEstimate_t1280269830  get_lightEstimate_3() const { return ___lightEstimate_3; }
	inline ARLightEstimate_t1280269830 * get_address_of_lightEstimate_3() { return &___lightEstimate_3; }
	inline void set_lightEstimate_3(ARLightEstimate_t1280269830  value)
	{
		___lightEstimate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAME_T3142156576_H
#ifndef PARTICLESYSTEM_T1777616458_H
#define PARTICLESYSTEM_T1777616458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t1777616458  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T1777616458_H
#ifndef ENUMERATOR_T1895934899_H
#define ENUMERATOR_T1895934899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>
struct  Enumerator_t1895934899 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t547075962 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ARHitTestResult_t1803723679  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1895934899, ___l_0)); }
	inline List_1_t547075962 * get_l_0() const { return ___l_0; }
	inline List_1_t547075962 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t547075962 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1895934899, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1895934899, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1895934899, ___current_3)); }
	inline ARHitTestResult_t1803723679  get_current_3() const { return ___current_3; }
	inline ARHitTestResult_t1803723679 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ARHitTestResult_t1803723679  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1895934899_H
#ifndef ASYNCCALLBACK_T606388952_H
#define ASYNCCALLBACK_T606388952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t606388952  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T606388952_H
#ifndef INTERNAL_ARFRAMEUPDATE_T2726389622_H
#define INTERNAL_ARFRAMEUPDATE_T2726389622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct  internal_ARFrameUpdate_t2726389622  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFRAMEUPDATE_T2726389622_H
#ifndef INTERNAL_ARANCHORADDED_T3791587037_H
#define INTERNAL_ARANCHORADDED_T3791587037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct  internal_ARAnchorAdded_t3791587037  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORADDED_T3791587037_H
#ifndef ARFRAMEUPDATE_T3333962149_H
#define ARFRAMEUPDATE_T3333962149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct  ARFrameUpdate_t3333962149  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAMEUPDATE_T3333962149_H
#ifndef INTERNAL_ARANCHORREMOVED_T409959511_H
#define INTERNAL_ARANCHORREMOVED_T409959511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct  internal_ARAnchorRemoved_t409959511  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORREMOVED_T409959511_H
#ifndef ARSESSIONFAILED_T3599980200_H
#define ARSESSIONFAILED_T3599980200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct  ARSessionFailed_t3599980200  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONFAILED_T3599980200_H
#ifndef INTERNAL_ARANCHORUPDATED_T2928809250_H
#define INTERNAL_ARANCHORUPDATED_T2928809250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct  internal_ARAnchorUpdated_t2928809250  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORUPDATED_T2928809250_H
#ifndef CAMERA_T226495598_H
#define CAMERA_T226495598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t226495598  : public Behaviour_t2200997390
{
public:

public:
};

struct Camera_t226495598_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t1646417403 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t1646417403 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t1646417403 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t226495598_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t1646417403 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t1646417403 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t1646417403 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t226495598_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t1646417403 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t1646417403 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t1646417403 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t226495598_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t1646417403 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t1646417403 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t1646417403 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T226495598_H
#ifndef MONOBEHAVIOUR_T1096588306_H
#define MONOBEHAVIOUR_T1096588306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1096588306  : public Behaviour_t2200997390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1096588306_H
#ifndef UNITYARVIDEO_T2470488057_H
#define UNITYARVIDEO_T2470488057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideo
struct  UnityARVideo_t2470488057  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Material UnityEngine.XR.iOS.UnityARVideo::m_ClearMaterial
	Material_t4055262778 * ___m_ClearMaterial_2;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.XR.iOS.UnityARVideo::m_VideoCommandBuffer
	CommandBuffer_t1027034615 * ___m_VideoCommandBuffer_3;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureY
	Texture2D_t2870930912 * ____videoTextureY_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureCbCr
	Texture2D_t2870930912 * ____videoTextureCbCr_5;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARVideo::m_Session
	UnityARSessionNativeInterface_t3869411053 * ___m_Session_6;
	// System.Boolean UnityEngine.XR.iOS.UnityARVideo::bCommandBufferInitialized
	bool ___bCommandBufferInitialized_7;

public:
	inline static int32_t get_offset_of_m_ClearMaterial_2() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ___m_ClearMaterial_2)); }
	inline Material_t4055262778 * get_m_ClearMaterial_2() const { return ___m_ClearMaterial_2; }
	inline Material_t4055262778 ** get_address_of_m_ClearMaterial_2() { return &___m_ClearMaterial_2; }
	inline void set_m_ClearMaterial_2(Material_t4055262778 * value)
	{
		___m_ClearMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClearMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_VideoCommandBuffer_3() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ___m_VideoCommandBuffer_3)); }
	inline CommandBuffer_t1027034615 * get_m_VideoCommandBuffer_3() const { return ___m_VideoCommandBuffer_3; }
	inline CommandBuffer_t1027034615 ** get_address_of_m_VideoCommandBuffer_3() { return &___m_VideoCommandBuffer_3; }
	inline void set_m_VideoCommandBuffer_3(CommandBuffer_t1027034615 * value)
	{
		___m_VideoCommandBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VideoCommandBuffer_3), value);
	}

	inline static int32_t get_offset_of__videoTextureY_4() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ____videoTextureY_4)); }
	inline Texture2D_t2870930912 * get__videoTextureY_4() const { return ____videoTextureY_4; }
	inline Texture2D_t2870930912 ** get_address_of__videoTextureY_4() { return &____videoTextureY_4; }
	inline void set__videoTextureY_4(Texture2D_t2870930912 * value)
	{
		____videoTextureY_4 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureY_4), value);
	}

	inline static int32_t get_offset_of__videoTextureCbCr_5() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ____videoTextureCbCr_5)); }
	inline Texture2D_t2870930912 * get__videoTextureCbCr_5() const { return ____videoTextureCbCr_5; }
	inline Texture2D_t2870930912 ** get_address_of__videoTextureCbCr_5() { return &____videoTextureCbCr_5; }
	inline void set__videoTextureCbCr_5(Texture2D_t2870930912 * value)
	{
		____videoTextureCbCr_5 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureCbCr_5), value);
	}

	inline static int32_t get_offset_of_m_Session_6() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ___m_Session_6)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_m_Session_6() const { return ___m_Session_6; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_m_Session_6() { return &___m_Session_6; }
	inline void set_m_Session_6(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___m_Session_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_6), value);
	}

	inline static int32_t get_offset_of_bCommandBufferInitialized_7() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ___bCommandBufferInitialized_7)); }
	inline bool get_bCommandBufferInitialized_7() const { return ___bCommandBufferInitialized_7; }
	inline bool* get_address_of_bCommandBufferInitialized_7() { return &___bCommandBufferInitialized_7; }
	inline void set_bCommandBufferInitialized_7(bool value)
	{
		___bCommandBufferInitialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEO_T2470488057_H
#ifndef UNITYARCAMERANEARFAR_T4152080291_H
#define UNITYARCAMERANEARFAR_T4152080291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraNearFar
struct  UnityARCameraNearFar_t4152080291  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Camera UnityARCameraNearFar::attachedCamera
	Camera_t226495598 * ___attachedCamera_2;
	// System.Single UnityARCameraNearFar::currentNearZ
	float ___currentNearZ_3;
	// System.Single UnityARCameraNearFar::currentFarZ
	float ___currentFarZ_4;

public:
	inline static int32_t get_offset_of_attachedCamera_2() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t4152080291, ___attachedCamera_2)); }
	inline Camera_t226495598 * get_attachedCamera_2() const { return ___attachedCamera_2; }
	inline Camera_t226495598 ** get_address_of_attachedCamera_2() { return &___attachedCamera_2; }
	inline void set_attachedCamera_2(Camera_t226495598 * value)
	{
		___attachedCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachedCamera_2), value);
	}

	inline static int32_t get_offset_of_currentNearZ_3() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t4152080291, ___currentNearZ_3)); }
	inline float get_currentNearZ_3() const { return ___currentNearZ_3; }
	inline float* get_address_of_currentNearZ_3() { return &___currentNearZ_3; }
	inline void set_currentNearZ_3(float value)
	{
		___currentNearZ_3 = value;
	}

	inline static int32_t get_offset_of_currentFarZ_4() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t4152080291, ___currentFarZ_4)); }
	inline float get_currentFarZ_4() const { return ___currentFarZ_4; }
	inline float* get_address_of_currentFarZ_4() { return &___currentFarZ_4; }
	inline void set_currentFarZ_4(float value)
	{
		___currentFarZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERANEARFAR_T4152080291_H
#ifndef UNITYARGENERATEPLANE_T3109641619_H
#define UNITYARGENERATEPLANE_T3109641619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARGeneratePlane
struct  UnityARGeneratePlane_t3109641619  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARGeneratePlane::planePrefab
	GameObject_t3649338848 * ___planePrefab_2;
	// UnityEngine.XR.iOS.UnityARAnchorManager UnityEngine.XR.iOS.UnityARGeneratePlane::unityARAnchorManager
	UnityARAnchorManager_t3624459219 * ___unityARAnchorManager_3;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3109641619, ___planePrefab_2)); }
	inline GameObject_t3649338848 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t3649338848 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t3649338848 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}

	inline static int32_t get_offset_of_unityARAnchorManager_3() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3109641619, ___unityARAnchorManager_3)); }
	inline UnityARAnchorManager_t3624459219 * get_unityARAnchorManager_3() const { return ___unityARAnchorManager_3; }
	inline UnityARAnchorManager_t3624459219 ** get_address_of_unityARAnchorManager_3() { return &___unityARAnchorManager_3; }
	inline void set_unityARAnchorManager_3(UnityARAnchorManager_t3624459219 * value)
	{
		___unityARAnchorManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___unityARAnchorManager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARGENERATEPLANE_T3109641619_H
#ifndef UNITYARHITTESTEXAMPLE_T2411672648_H
#define UNITYARHITTESTEXAMPLE_T2411672648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestExample
struct  UnityARHitTestExample_t2411672648  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.UnityARHitTestExample::m_HitTransform
	Transform_t3933397867 * ___m_HitTransform_2;

public:
	inline static int32_t get_offset_of_m_HitTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t2411672648, ___m_HitTransform_2)); }
	inline Transform_t3933397867 * get_m_HitTransform_2() const { return ___m_HitTransform_2; }
	inline Transform_t3933397867 ** get_address_of_m_HitTransform_2() { return &___m_HitTransform_2; }
	inline void set_m_HitTransform_2(Transform_t3933397867 * value)
	{
		___m_HitTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARHITTESTEXAMPLE_T2411672648_H
#ifndef UNITYPOINTCLOUDEXAMPLE_T595762290_H
#define UNITYPOINTCLOUDEXAMPLE_T595762290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityPointCloudExample
struct  UnityPointCloudExample_t595762290  : public MonoBehaviour_t1096588306
{
public:
	// System.UInt32 UnityPointCloudExample::numPointsToShow
	uint32_t ___numPointsToShow_2;
	// UnityEngine.GameObject UnityPointCloudExample::PointCloudPrefab
	GameObject_t3649338848 * ___PointCloudPrefab_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityPointCloudExample::pointCloudObjects
	List_1_t2392691131 * ___pointCloudObjects_4;
	// UnityEngine.Vector3[] UnityPointCloudExample::m_PointCloudData
	Vector3U5BU5D_t4153953548* ___m_PointCloudData_5;

public:
	inline static int32_t get_offset_of_numPointsToShow_2() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t595762290, ___numPointsToShow_2)); }
	inline uint32_t get_numPointsToShow_2() const { return ___numPointsToShow_2; }
	inline uint32_t* get_address_of_numPointsToShow_2() { return &___numPointsToShow_2; }
	inline void set_numPointsToShow_2(uint32_t value)
	{
		___numPointsToShow_2 = value;
	}

	inline static int32_t get_offset_of_PointCloudPrefab_3() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t595762290, ___PointCloudPrefab_3)); }
	inline GameObject_t3649338848 * get_PointCloudPrefab_3() const { return ___PointCloudPrefab_3; }
	inline GameObject_t3649338848 ** get_address_of_PointCloudPrefab_3() { return &___PointCloudPrefab_3; }
	inline void set_PointCloudPrefab_3(GameObject_t3649338848 * value)
	{
		___PointCloudPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudPrefab_3), value);
	}

	inline static int32_t get_offset_of_pointCloudObjects_4() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t595762290, ___pointCloudObjects_4)); }
	inline List_1_t2392691131 * get_pointCloudObjects_4() const { return ___pointCloudObjects_4; }
	inline List_1_t2392691131 ** get_address_of_pointCloudObjects_4() { return &___pointCloudObjects_4; }
	inline void set_pointCloudObjects_4(List_1_t2392691131 * value)
	{
		___pointCloudObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudObjects_4), value);
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t595762290, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t4153953548* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t4153953548** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t4153953548* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPOINTCLOUDEXAMPLE_T595762290_H
#ifndef UNITYARKITCONTROL_T4114739031_H
#define UNITYARKITCONTROL_T4114739031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARKitControl
struct  UnityARKitControl_t4114739031  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.XR.iOS.UnityARSessionRunOption[] UnityEngine.XR.iOS.UnityARKitControl::runOptions
	UnityARSessionRunOptionU5BU5D_t3872385379* ___runOptions_2;
	// UnityEngine.XR.iOS.UnityARAlignment[] UnityEngine.XR.iOS.UnityARKitControl::alignmentOptions
	UnityARAlignmentU5BU5D_t2020264667* ___alignmentOptions_3;
	// UnityEngine.XR.iOS.UnityARPlaneDetection[] UnityEngine.XR.iOS.UnityARKitControl::planeOptions
	UnityARPlaneDetectionU5BU5D_t4098669274* ___planeOptions_4;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentOptionIndex
	int32_t ___currentOptionIndex_5;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentAlignmentIndex
	int32_t ___currentAlignmentIndex_6;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentPlaneIndex
	int32_t ___currentPlaneIndex_7;

public:
	inline static int32_t get_offset_of_runOptions_2() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___runOptions_2)); }
	inline UnityARSessionRunOptionU5BU5D_t3872385379* get_runOptions_2() const { return ___runOptions_2; }
	inline UnityARSessionRunOptionU5BU5D_t3872385379** get_address_of_runOptions_2() { return &___runOptions_2; }
	inline void set_runOptions_2(UnityARSessionRunOptionU5BU5D_t3872385379* value)
	{
		___runOptions_2 = value;
		Il2CppCodeGenWriteBarrier((&___runOptions_2), value);
	}

	inline static int32_t get_offset_of_alignmentOptions_3() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___alignmentOptions_3)); }
	inline UnityARAlignmentU5BU5D_t2020264667* get_alignmentOptions_3() const { return ___alignmentOptions_3; }
	inline UnityARAlignmentU5BU5D_t2020264667** get_address_of_alignmentOptions_3() { return &___alignmentOptions_3; }
	inline void set_alignmentOptions_3(UnityARAlignmentU5BU5D_t2020264667* value)
	{
		___alignmentOptions_3 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentOptions_3), value);
	}

	inline static int32_t get_offset_of_planeOptions_4() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___planeOptions_4)); }
	inline UnityARPlaneDetectionU5BU5D_t4098669274* get_planeOptions_4() const { return ___planeOptions_4; }
	inline UnityARPlaneDetectionU5BU5D_t4098669274** get_address_of_planeOptions_4() { return &___planeOptions_4; }
	inline void set_planeOptions_4(UnityARPlaneDetectionU5BU5D_t4098669274* value)
	{
		___planeOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeOptions_4), value);
	}

	inline static int32_t get_offset_of_currentOptionIndex_5() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___currentOptionIndex_5)); }
	inline int32_t get_currentOptionIndex_5() const { return ___currentOptionIndex_5; }
	inline int32_t* get_address_of_currentOptionIndex_5() { return &___currentOptionIndex_5; }
	inline void set_currentOptionIndex_5(int32_t value)
	{
		___currentOptionIndex_5 = value;
	}

	inline static int32_t get_offset_of_currentAlignmentIndex_6() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___currentAlignmentIndex_6)); }
	inline int32_t get_currentAlignmentIndex_6() const { return ___currentAlignmentIndex_6; }
	inline int32_t* get_address_of_currentAlignmentIndex_6() { return &___currentAlignmentIndex_6; }
	inline void set_currentAlignmentIndex_6(int32_t value)
	{
		___currentAlignmentIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentPlaneIndex_7() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___currentPlaneIndex_7)); }
	inline int32_t get_currentPlaneIndex_7() const { return ___currentPlaneIndex_7; }
	inline int32_t* get_address_of_currentPlaneIndex_7() { return &___currentPlaneIndex_7; }
	inline void set_currentPlaneIndex_7(int32_t value)
	{
		___currentPlaneIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITCONTROL_T4114739031_H
#ifndef AR3DOFCAMERAMANAGER_T3216700943_H
#define AR3DOFCAMERAMANAGER_T3216700943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AR3DOFCameraManager
struct  AR3DOFCameraManager_t3216700943  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Camera AR3DOFCameraManager::m_camera
	Camera_t226495598 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface AR3DOFCameraManager::m_session
	UnityARSessionNativeInterface_t3869411053 * ___m_session_3;
	// UnityEngine.Material AR3DOFCameraManager::savedClearMaterial
	Material_t4055262778 * ___savedClearMaterial_4;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t3216700943, ___m_camera_2)); }
	inline Camera_t226495598 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t226495598 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t226495598 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t3216700943, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t3216700943, ___savedClearMaterial_4)); }
	inline Material_t4055262778 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t4055262778 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t4055262778 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AR3DOFCAMERAMANAGER_T3216700943_H
#ifndef POINTCLOUDPARTICLEEXAMPLE_T3205217707_H
#define POINTCLOUDPARTICLEEXAMPLE_T3205217707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudParticleExample
struct  PointCloudParticleExample_t3205217707  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.ParticleSystem PointCloudParticleExample::pointCloudParticlePrefab
	ParticleSystem_t1777616458 * ___pointCloudParticlePrefab_2;
	// System.Int32 PointCloudParticleExample::maxPointsToShow
	int32_t ___maxPointsToShow_3;
	// System.Single PointCloudParticleExample::particleSize
	float ___particleSize_4;
	// UnityEngine.Vector3[] PointCloudParticleExample::m_PointCloudData
	Vector3U5BU5D_t4153953548* ___m_PointCloudData_5;
	// System.Boolean PointCloudParticleExample::frameUpdated
	bool ___frameUpdated_6;
	// UnityEngine.ParticleSystem PointCloudParticleExample::currentPS
	ParticleSystem_t1777616458 * ___currentPS_7;
	// UnityEngine.ParticleSystem/Particle[] PointCloudParticleExample::particles
	ParticleU5BU5D_t419044168* ___particles_8;

public:
	inline static int32_t get_offset_of_pointCloudParticlePrefab_2() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___pointCloudParticlePrefab_2)); }
	inline ParticleSystem_t1777616458 * get_pointCloudParticlePrefab_2() const { return ___pointCloudParticlePrefab_2; }
	inline ParticleSystem_t1777616458 ** get_address_of_pointCloudParticlePrefab_2() { return &___pointCloudParticlePrefab_2; }
	inline void set_pointCloudParticlePrefab_2(ParticleSystem_t1777616458 * value)
	{
		___pointCloudParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_maxPointsToShow_3() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___maxPointsToShow_3)); }
	inline int32_t get_maxPointsToShow_3() const { return ___maxPointsToShow_3; }
	inline int32_t* get_address_of_maxPointsToShow_3() { return &___maxPointsToShow_3; }
	inline void set_maxPointsToShow_3(int32_t value)
	{
		___maxPointsToShow_3 = value;
	}

	inline static int32_t get_offset_of_particleSize_4() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___particleSize_4)); }
	inline float get_particleSize_4() const { return ___particleSize_4; }
	inline float* get_address_of_particleSize_4() { return &___particleSize_4; }
	inline void set_particleSize_4(float value)
	{
		___particleSize_4 = value;
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t4153953548* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t4153953548** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t4153953548* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}

	inline static int32_t get_offset_of_frameUpdated_6() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___frameUpdated_6)); }
	inline bool get_frameUpdated_6() const { return ___frameUpdated_6; }
	inline bool* get_address_of_frameUpdated_6() { return &___frameUpdated_6; }
	inline void set_frameUpdated_6(bool value)
	{
		___frameUpdated_6 = value;
	}

	inline static int32_t get_offset_of_currentPS_7() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___currentPS_7)); }
	inline ParticleSystem_t1777616458 * get_currentPS_7() const { return ___currentPS_7; }
	inline ParticleSystem_t1777616458 ** get_address_of_currentPS_7() { return &___currentPS_7; }
	inline void set_currentPS_7(ParticleSystem_t1777616458 * value)
	{
		___currentPS_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_7), value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___particles_8)); }
	inline ParticleU5BU5D_t419044168* get_particles_8() const { return ___particles_8; }
	inline ParticleU5BU5D_t419044168** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ParticleU5BU5D_t419044168* value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier((&___particles_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDPARTICLEEXAMPLE_T3205217707_H
#ifndef UNITYARCAMERAMANAGER_T1803962782_H
#define UNITYARCAMERAMANAGER_T1803962782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraManager
struct  UnityARCameraManager_t1803962782  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Camera UnityARCameraManager::m_camera
	Camera_t226495598 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARCameraManager::m_session
	UnityARSessionNativeInterface_t3869411053 * ___m_session_3;
	// UnityEngine.Material UnityARCameraManager::savedClearMaterial
	Material_t4055262778 * ___savedClearMaterial_4;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t1803962782, ___m_camera_2)); }
	inline Camera_t226495598 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t226495598 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t226495598 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t1803962782, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t1803962782, ___savedClearMaterial_4)); }
	inline Material_t4055262778 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t4055262778 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t4055262778 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERAMANAGER_T1803962782_H
#ifndef DONTDESTROYONLOAD_T2575831040_H
#define DONTDESTROYONLOAD_T2575831040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DontDestroyOnLoad
struct  DontDestroyOnLoad_t2575831040  : public MonoBehaviour_t1096588306
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROYONLOAD_T2575831040_H
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t4153953548  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t596762001  m_Items[1];

public:
	inline Vector3_t596762001  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t596762001 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t596762001  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t596762001  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t596762001 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t596762001  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t419044168  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Particle_t1268635397  m_Items[1];

public:
	inline Particle_t1268635397  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Particle_t1268635397 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Particle_t1268635397  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Particle_t1268635397  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Particle_t1268635397 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Particle_t1268635397  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.XR.iOS.ARHitTestResultType[]
struct ARHitTestResultTypeU5BU5D_t1137434915  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t3872385379  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_t2020264667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t4098669274  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t3488826921  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};

extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke(const ARKitWorldTackingSessionConfiguration_t2186350340& unmarshaled, ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke& marshaled);
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_back(const ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke& marshaled, ARKitWorldTackingSessionConfiguration_t2186350340& unmarshaled);
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_cleanup(ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke& marshaled);
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke(const ARKitSessionConfiguration_t3142208763& unmarshaled, ARKitSessionConfiguration_t3142208763_marshaled_pinvoke& marshaled);
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke_back(const ARKitSessionConfiguration_t3142208763_marshaled_pinvoke& marshaled, ARKitSessionConfiguration_t3142208763& unmarshaled);
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke_cleanup(ARKitSessionConfiguration_t3142208763_marshaled_pinvoke& marshaled);
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke(const UnityARHitTestResult_t1002809393& unmarshaled, UnityARHitTestResult_t1002809393_marshaled_pinvoke& marshaled);
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke_back(const UnityARHitTestResult_t1002809393_marshaled_pinvoke& marshaled, UnityARHitTestResult_t1002809393& unmarshaled);
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke_cleanup(UnityARHitTestResult_t1002809393_marshaled_pinvoke& marshaled);
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke(const ARPlaneAnchor_t1843157284& unmarshaled, ARPlaneAnchor_t1843157284_marshaled_pinvoke& marshaled);
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke_back(const ARPlaneAnchor_t1843157284_marshaled_pinvoke& marshaled, ARPlaneAnchor_t1843157284& unmarshaled);
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke_cleanup(ARPlaneAnchor_t1843157284_marshaled_pinvoke& marshaled);

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m3090413657_gshared (GameObject_t3649338848 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m2970963764_gshared (GameObject_t3649338848 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m3859482884_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m3649492292_gshared (Component_t531478471 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m304234479_gshared (Dictionary_2_t1946233076 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1598188702_gshared (Dictionary_2_t1946233076 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m2197744408_gshared (Dictionary_2_t1946233076 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m3662815801_gshared (Dictionary_2_t1946233076 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m322316142_gshared (Dictionary_2_t1946233076 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m2227163262_gshared (Dictionary_2_t1946233076 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t214722923  List_1_GetEnumerator_m2646801933_gshared (List_1_t3160831282 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m2382863564_gshared (Enumerator_t214722923 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1172281808_gshared (Enumerator_t214722923 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1931444793_gshared (Enumerator_t214722923 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3424159269_gshared (Dictionary_2_t1946233076 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C"  ValueCollection_t3277784254 * Dictionary_2_get_Values_m3669209363_gshared (Dictionary_2_t1946233076 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t3160831282 * Enumerable_ToList_TisRuntimeObject_m3075656733_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m1948580076_gshared (List_1_t3160831282 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::get_Count()
extern "C"  int32_t List_1_get_Count_m655066853_gshared (List_1_t547075962 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::GetEnumerator()
extern "C"  Enumerator_t1895934899  List_1_GetEnumerator_m2437281809_gshared (List_1_t547075962 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::get_Current()
extern "C"  ARHitTestResult_t1803723679  Enumerator_get_Current_m3632740371_gshared (Enumerator_t1895934899 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1007619642_gshared (Enumerator_t1895934899 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3562983614_gshared (Enumerator_t1895934899 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::.ctor()
extern "C"  void List_1__ctor_m2682833015_gshared (List_1_t547075962 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::Add(!0)
extern "C"  void List_1_Add_m1601798359_gshared (List_1_t547075962 * __this, ARHitTestResult_t1803723679  p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_m3730568893_gshared (GameObject_t3649338848 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m112108964_gshared (List_1_t3160831282 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m2991374128_gshared (List_1_t3160831282 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m3635692520_gshared (List_1_t3160831282 * __this, int32_t p0, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m4062598822 (MonoBehaviour_t1096588306 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m714144203 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARSessionNativeInterface()
extern "C"  UnityARSessionNativeInterface_t3869411053 * UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfig(UnityEngine.XR.iOS.ARKitSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_RunWithConfig_m152596073 (UnityARSessionNativeInterface_t3869411053 * __this, ARKitSessionConfiguration_t3142208763  ___config0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m528327324 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, Object_t1008057425 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t226495598 * Camera_get_main_m2671983186 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1545791448 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, Object_t1008057425 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t3649338848 * Component_get_gameObject_m3065601689 (Component_t531478471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.XR.iOS.UnityARVideo>()
#define GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688(__this, method) ((  UnityARVideo_t2470488057 * (*) (GameObject_t3649338848 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m3090413657_gshared)(__this, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m1839175560 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void AR3DOFCameraManager::SetupNewCamera(UnityEngine.Camera)
extern "C"  void AR3DOFCameraManager_SetupNewCamera_m2725600353 (AR3DOFCameraManager_t3216700943 * __this, Camera_t226495598 * ___newCamera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.XR.iOS.UnityARVideo>()
#define GameObject_AddComponent_TisUnityARVideo_t2470488057_m3425289194(__this, method) ((  UnityARVideo_t2470488057 * (*) (GameObject_t3649338848 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2970963764_gshared)(__this, method)
// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetCameraPose()
extern "C"  Matrix4x4_t1288378485  UnityARSessionNativeInterface_GetCameraPose_m3014150810 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3933397867 * Component_get_transform_m2033240428 (Component_t531478471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.XR.iOS.UnityARMatrixOps::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t596762001  UnityARMatrixOps_GetPosition_m3553979848 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1288378485  ___matrix0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m3583812093 (Transform_t3933397867 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::GetRotation(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t3165733013  UnityARMatrixOps_GetRotation_m3890376831 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1288378485  ___matrix0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m1950704683 (Transform_t3933397867 * __this, Quaternion_t3165733013  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetCameraProjection()
extern "C"  Matrix4x4_t1288378485  UnityARSessionNativeInterface_GetCameraProjection_m248827655 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m1066889169 (Camera_t226495598 * __this, Matrix4x4_t1288378485  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m4110163480 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void ARFrameUpdate__ctor_m722081207 (ARFrameUpdate_t3333962149 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1467721625 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t3333962149 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.ParticleSystem>(!!0)
#define Object_Instantiate_TisParticleSystem_t1777616458_m895722352(__this /* static, unused */, p0, method) ((  ParticleSystem_t1777616458 * (*) (RuntimeObject * /* static, unused */, ParticleSystem_t1777616458 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m3859482884_gshared)(__this /* static, unused */, p0, method)
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m3947409433 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m2752520204 (Particle_t1268635397 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2728568087 (Color_t320819310 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t2499566028  Color32_op_Implicit_m4104578668 (RuntimeObject * __this /* static, unused */, Color_t320819310  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C"  void Particle_set_startColor_m577033657 (Particle_t1268635397 * __this, Color32_t2499566028  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C"  void Particle_set_startSize_m2538937397 (Particle_t1268635397 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern "C"  void ParticleSystem_SetParticles_m1829004744 (ParticleSystem_t1777616458 * __this, ParticleU5BU5D_t419044168* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfig(UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_RunWithConfig_m3364444856 (UnityARSessionNativeInterface_t3869411053 * __this, ARKitWorldTackingSessionConfiguration_t2186350340  ___config0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityARCameraManager::SetupNewCamera(UnityEngine.Camera)
extern "C"  void UnityARCameraManager_SetupNewCamera_m3593985194 (UnityARCameraManager_t1803962782 * __this, Camera_t226495598 * ___newCamera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t226495598_m3685067992(__this, method) ((  Camera_t226495598 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// System.Void UnityARCameraNearFar::UpdateCameraClipPlanes()
extern "C"  void UnityARCameraNearFar_UpdateCameraClipPlanes_m123287997 (UnityARCameraNearFar_t4152080291 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m418873954 (Camera_t226495598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m2103707533 (Camera_t226495598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCameraClipPlanes(System.Single,System.Single)
extern "C"  void UnityARSessionNativeInterface_SetCameraClipPlanes_m540333021 (UnityARSessionNativeInterface_t3869411053 * __this, float ___nearZ0, float ___farZ1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,System.Boolean,System.Boolean)
extern "C"  void ARKitSessionConfiguration__ctor_m373048997 (ARKitSessionConfiguration_t3142208763 * __this, int32_t ___alignment0, bool ___getPointCloudData1, bool ___enableLightEstimation2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::IsARKitSessionConfigurationSupported()
extern "C"  bool ARKitSessionConfiguration_IsARKitSessionConfigurationSupported_m3090600542 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::get_IsSupported()
extern "C"  bool ARKitSessionConfiguration_get_IsSupported_m3459182394 (ARKitSessionConfiguration_t3142208763 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitSessionConfiguration::set_IsSupported(System.Boolean)
extern "C"  void ARKitSessionConfiguration_set_IsSupported_m10952259 (ARKitSessionConfiguration_t3142208763 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,UnityEngine.XR.iOS.UnityARPlaneDetection,System.Boolean,System.Boolean)
extern "C"  void ARKitWorldTackingSessionConfiguration__ctor_m2859336466 (ARKitWorldTackingSessionConfiguration_t2186350340 * __this, int32_t ___alignment0, int32_t ___planeDetection1, bool ___getPointCloudData2, bool ___enableLightEstimation3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::IsARKitWorldTrackingSessionConfigurationSupported()
extern "C"  bool ARKitWorldTackingSessionConfiguration_IsARKitWorldTrackingSessionConfigurationSupported_m4262940539 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::get_IsSupported()
extern "C"  bool ARKitWorldTackingSessionConfiguration_get_IsSupported_m1346839597 (ARKitWorldTackingSessionConfiguration_t2186350340 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::set_IsSupported(System.Boolean)
extern "C"  void ARKitWorldTackingSessionConfiguration_set_IsSupported_m2691427501 (ARKitWorldTackingSessionConfiguration_t2186350340 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m990968439 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::.ctor()
#define Dictionary_2__ctor_m2541633073(__this, method) ((  void (*) (Dictionary_2_t3033730875 *, const RuntimeMethod*))Dictionary_2__ctor_m304234479_gshared)(__this, method)
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorAdded__ctor_m2351544190 (ARAnchorAdded_t1580091102 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorAddedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m3219973195 (RuntimeObject * __this /* static, unused */, ARAnchorAdded_t1580091102 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorUpdated__ctor_m401717748 (ARAnchorUpdated_t1492128022 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m994455513 (RuntimeObject * __this /* static, unused */, ARAnchorUpdated_t1492128022 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorRemoved__ctor_m1468226077 (ARAnchorRemoved_t3663543047 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorRemovedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m1279350122 (RuntimeObject * __this /* static, unused */, ARAnchorRemoved_t3663543047 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::CreatePlaneInScene(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t3649338848 * UnityARUtility_CreatePlaneInScene_m2413657456 (RuntimeObject * __this /* static, unused */, ARPlaneAnchor_t1843157284  ___arPlaneAnchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<DontDestroyOnLoad>()
#define GameObject_AddComponent_TisDontDestroyOnLoad_t2575831040_m3797858659(__this, method) ((  DontDestroyOnLoad_t2575831040 * (*) (GameObject_t3649338848 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2970963764_gshared)(__this, method)
// System.Void UnityEngine.XR.iOS.ARPlaneAnchorGameObject::.ctor()
extern "C"  void ARPlaneAnchorGameObject__ctor_m2225510302 (ARPlaneAnchorGameObject_t3146329710 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::Add(!0,!1)
#define Dictionary_2_Add_m113011829(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3033730875 *, String_t*, ARPlaneAnchorGameObject_t3146329710 *, const RuntimeMethod*))Dictionary_2_Add_m1598188702_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m4199403017(__this, p0, method) ((  bool (*) (Dictionary_2_t3033730875 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m2197744408_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::get_Item(!0)
#define Dictionary_2_get_Item_m1097037868(__this, p0, method) ((  ARPlaneAnchorGameObject_t3146329710 * (*) (Dictionary_2_t3033730875 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m3662815801_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::Remove(!0)
#define Dictionary_2_Remove_m4064418252(__this, p0, method) ((  bool (*) (Dictionary_2_t3033730875 *, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m322316142_gshared)(__this, p0, method)
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::UpdatePlaneWithAnchorTransform(UnityEngine.GameObject,UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t3649338848 * UnityARUtility_UpdatePlaneWithAnchorTransform_m923713608 (RuntimeObject * __this /* static, unused */, GameObject_t3649338848 * ___plane0, ARPlaneAnchor_t1843157284  ___arPlaneAnchor1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m4260355931(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3033730875 *, String_t*, ARPlaneAnchorGameObject_t3146329710 *, const RuntimeMethod*))Dictionary_2_set_Item_m2227163262_gshared)(__this, p0, p1, method)
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::GetCurrentPlaneAnchors()
extern "C"  List_1_t1889681993 * UnityARAnchorManager_GetCurrentPlaneAnchors_m3710250286 (UnityARAnchorManager_t3624459219 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::GetEnumerator()
#define List_1_GetEnumerator_m2396092950(__this, method) ((  Enumerator_t3238540930  (*) (List_1_t1889681993 *, const RuntimeMethod*))List_1_GetEnumerator_m2646801933_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::get_Current()
#define Enumerator_get_Current_m3648846247(__this, method) ((  ARPlaneAnchorGameObject_t3146329710 * (*) (Enumerator_t3238540930 *, const RuntimeMethod*))Enumerator_get_Current_m2382863564_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::MoveNext()
#define Enumerator_MoveNext_m2283436456(__this, method) ((  bool (*) (Enumerator_t3238540930 *, const RuntimeMethod*))Enumerator_MoveNext_m1172281808_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::Dispose()
#define Enumerator_Dispose_m4042749809(__this, method) ((  void (*) (Enumerator_t3238540930 *, const RuntimeMethod*))Enumerator_Dispose_m1931444793_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::Clear()
#define Dictionary_2_Clear_m1758728446(__this, method) ((  void (*) (Dictionary_2_t3033730875 *, const RuntimeMethod*))Dictionary_2_Clear_m3424159269_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::get_Values()
#define Dictionary_2_get_Values_m3019150958(__this, method) ((  ValueCollection_t70314757 * (*) (Dictionary_2_t3033730875 *, const RuntimeMethod*))Dictionary_2_get_Values_m3669209363_gshared)(__this, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisARPlaneAnchorGameObject_t3146329710_m589330593(__this /* static, unused */, p0, method) ((  List_1_t1889681993 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_m3075656733_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::.ctor()
extern "C"  void UnityARAnchorManager__ctor_m1537079611 (UnityARAnchorManager_t3624459219 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARUtility::InitializePlanePrefab(UnityEngine.GameObject)
extern "C"  void UnityARUtility_InitializePlanePrefab_m2229940298 (RuntimeObject * __this /* static, unused */, GameObject_t3649338848 * ___go0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::Destroy()
extern "C"  void UnityARAnchorManager_Destroy_m3970721311 (UnityARAnchorManager_t3624459219 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::get_Count()
#define List_1_get_Count_m1057070554(__this, method) ((  int32_t (*) (List_1_t1889681993 *, const RuntimeMethod*))List_1_get_Count_m1948580076_gshared)(__this, method)
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult> UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  List_1_t547075962 * UnityARSessionNativeInterface_HitTest_m1126351971 (UnityARSessionNativeInterface_t3869411053 * __this, ARPoint_t1649490841  ___point0, int64_t ___types1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::get_Count()
#define List_1_get_Count_m655066853(__this, method) ((  int32_t (*) (List_1_t547075962 *, const RuntimeMethod*))List_1_get_Count_m655066853_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::GetEnumerator()
#define List_1_GetEnumerator_m2437281809(__this, method) ((  Enumerator_t1895934899  (*) (List_1_t547075962 *, const RuntimeMethod*))List_1_GetEnumerator_m2437281809_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::get_Current()
#define Enumerator_get_Current_m3632740371(__this, method) ((  ARHitTestResult_t1803723679  (*) (Enumerator_t1895934899 *, const RuntimeMethod*))Enumerator_get_Current_m3632740371_gshared)(__this, method)
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m2782172005 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2789033506 (Transform_t3933397867 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m1288069348 (Transform_t3933397867 * __this, Quaternion_t3165733013  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t596762001  Transform_get_position_m3654347341 (Transform_t3933397867 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* String_Format_m223808606 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::MoveNext()
#define Enumerator_MoveNext_m1007619642(__this, method) ((  bool (*) (Enumerator_t1895934899 *, const RuntimeMethod*))Enumerator_MoveNext_m1007619642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::Dispose()
#define Enumerator_Dispose_m3562983614(__this, method) ((  void (*) (Enumerator_t1895934899 *, const RuntimeMethod*))Enumerator_Dispose_m3562983614_gshared)(__this, method)
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m4114401638 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t3132414106  Input_GetTouch_m2909242247 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m993859056 (Touch_t3132414106 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t59524482  Touch_get_position_m2291334870 (Touch_t3132414106 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t596762001  Vector2_op_Implicit_m2201252505 (RuntimeObject * __this /* static, unused */, Vector2_t59524482  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t596762001  Camera_ScreenToViewportPoint_m474730427 (Camera_t226495598 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m552509826 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t1604667936  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.UnityARHitTestExample::HitTestWithResultType(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  bool UnityARHitTestExample_HitTestWithResultType_m2836964868 (UnityARHitTestExample_t2411672648 * __this, ARPoint_t1649490841  ___point0, int64_t ___resultTypes1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1383055445 (Rect_t1992046353 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern "C"  bool GUI_Button_m1880561377 (RuntimeObject * __this /* static, unused */, Rect_t1992046353  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::Pause()
extern "C"  void UnityARSessionNativeInterface_Pause_m224144355 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfigAndOptions(UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_RunWithConfigAndOptions_m3750156203 (UnityARSessionNativeInterface_t3869411053 * __this, ARKitWorldTackingSessionConfiguration_t2186350340  ___config0, int32_t ___runOptions1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2023871361 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t1376926224  Matrix4x4_GetColumn_m2106332924 (Matrix4x4_t1288378485 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C"  Vector3_t596762001  Vector4_op_Implicit_m4265673868 (RuntimeObject * __this /* static, unused */, Vector4_t1376926224  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t3165733013  UnityARMatrixOps_QuaternionFromMatrix_m2802180225 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1288378485  ___m0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m3766957970 (Matrix4x4_t1288378485 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m1799835110 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m1995337595 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_frame_update(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void UnityARSessionNativeInterface__frame_update_m3249087823 (RuntimeObject * __this /* static, unused */, internal_UnityARCamera_t1506237410  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_added(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_added_m3990782978 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t1511263172  ___anchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_updated(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_updated_m4275459728 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t1511263172  ___anchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_removed(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_removed_m4149770974 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t1511263172  ___anchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_ar_session_failed(System.String)
extern "C"  void UnityARSessionNativeInterface__ar_session_failed_m1149369129 (RuntimeObject * __this /* static, unused */, String_t* ___error0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARFrameUpdate__ctor_m2168096248 (internal_ARFrameUpdate_t2726389622 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorAdded__ctor_m3202793875 (internal_ARAnchorAdded_t3791587037 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorUpdated__ctor_m1845092063 (internal_ARAnchorUpdated_t2928809250 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorRemoved__ctor_m2196994448 (internal_ARAnchorRemoved_t409959511 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::.ctor(System.Object,System.IntPtr)
extern "C"  void ARSessionFailed__ctor_m2477073150 (ARSessionFailed_t3599980200 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::unity_CreateNativeARSession(UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved,UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed)
extern "C"  IntPtr_t UnityARSessionNativeInterface_unity_CreateNativeARSession_m2427596710 (RuntimeObject * __this /* static, unused */, internal_ARFrameUpdate_t2726389622 * ___frameUpdate0, internal_ARAnchorAdded_t3791587037 * ___anchorAdded1, internal_ARAnchorUpdated_t2928809250 * ___anchorUpdated2, internal_ARAnchorRemoved_t409959511 * ___anchorRemoved3, ARSessionFailed_t3599980200 * ___sessionFailed4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t69892740 * Delegate_Combine_m2396730294 (RuntimeObject * __this /* static, unused */, Delegate_t69892740 * p0, Delegate_t69892740 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t69892740 * Delegate_Remove_m1307738938 (RuntimeObject * __this /* static, unused */, Delegate_t69892740 * p0, Delegate_t69892740 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::.ctor()
extern "C"  void UnityARSessionNativeInterface__ctor_m2217614958 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetColumn_m567234837 (Matrix4x4_t1288378485 * __this, int32_t p0, Vector4_t1376926224  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCameraNearFar(System.Single,System.Single)
extern "C"  void UnityARSessionNativeInterface_SetCameraNearFar_m2805008808 (RuntimeObject * __this /* static, unused */, float ___nearZ0, float ___farZ1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::UpdatePointCloudData(UnityEngine.XR.iOS.UnityARCamera&)
extern "C"  void UnityARSessionNativeInterface_UpdatePointCloudData_m3039135696 (RuntimeObject * __this /* static, unused */, UnityARCamera_t2376600594 * ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::Invoke(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void ARFrameUpdate_Invoke_m1537461562 (ARFrameUpdate_t3333962149 * __this, UnityARCamera_t2376600594  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARPointCloud(System.IntPtr&,System.UInt32&)
extern "C"  bool UnityARSessionNativeInterface_GetARPointCloud_m228888586 (RuntimeObject * __this /* static, unused */, IntPtr_t* ___verts0, uint32_t* ___vertLength1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern "C"  void Marshal_Copy_m4018830953 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, SingleU5BU5D_t3488826921* p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAuto(System.IntPtr)
extern "C"  String_t* Marshal_PtrToStringAuto_m4261366353 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1043686083 (Vector3_t596762001 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m69146234 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetPlaneAnchorFromAnchorData(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  ARPlaneAnchor_t1843157284  UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m1229793965 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t1511263172  ___anchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorAdded_Invoke_m1710052577 (ARAnchorAdded_t1580091102 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorUpdated_Invoke_m2598267505 (ARAnchorUpdated_t1492128022 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorRemoved_Invoke_m1905220566 (ARAnchorRemoved_t3663543047 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::Invoke(System.String)
extern "C"  void ARSessionFailed_Invoke_m2486705370 (ARSessionFailed_t3599980200 * __this, String_t* ___error0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartWorldTrackingSessionWithOptions(System.IntPtr,UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_StartWorldTrackingSessionWithOptions_m2736903977 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitWorldTackingSessionConfiguration_t2186350340  ___configuration1, int32_t ___runOptions2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartWorldTrackingSession(System.IntPtr,UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_StartWorldTrackingSession_m2658584269 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitWorldTackingSessionConfiguration_t2186350340  ___configuration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartSessionWithOptions(System.IntPtr,UnityEngine.XR.iOS.ARKitSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_StartSessionWithOptions_m984328055 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitSessionConfiguration_t3142208763  ___configuration1, int32_t ___runOptions2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartSession(System.IntPtr,UnityEngine.XR.iOS.ARKitSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_StartSession_m283474087 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitSessionConfiguration_t3142208763  ___configuration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::PauseSession(System.IntPtr)
extern "C"  void UnityARSessionNativeInterface_PauseSession_m3540433210 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(System.IntPtr,UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  int32_t UnityARSessionNativeInterface_HitTest_m4214093291 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARPoint_t1649490841  ___point1, int64_t ___types2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m1813664034 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::.ctor()
#define List_1__ctor_m2682833015(__this, method) ((  void (*) (List_1_t547075962 *, const RuntimeMethod*))List_1__ctor_m2682833015_gshared)(__this, method)
// UnityEngine.XR.iOS.UnityARHitTestResult UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetLastHitTestResult(System.Int32)
extern "C"  UnityARHitTestResult_t1002809393  UnityARSessionNativeInterface_GetLastHitTestResult_m2103824391 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.ARHitTestResult UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetHitTestResultFromResultData(UnityEngine.XR.iOS.UnityARHitTestResult)
extern "C"  ARHitTestResult_t1803723679  UnityARSessionNativeInterface_GetHitTestResultFromResultData_m2693418722 (RuntimeObject * __this /* static, unused */, UnityARHitTestResult_t1002809393  ___resultData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::Add(!0)
#define List_1_Add_m1601798359(__this, p0, method) ((  void (*) (List_1_t547075962 *, ARHitTestResult_t1803723679 , const RuntimeMethod*))List_1_Add_m1601798359_gshared)(__this, p0, method)
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetVideoTextureHandles()
extern "C"  ARTextureHandles_t1375402930  UnityARSessionNativeInterface_GetVideoTextureHandles_m1659577931 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetAmbientIntensity()
extern "C"  float UnityARSessionNativeInterface_GetAmbientIntensity_m2586499763 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetTrackingQuality()
extern "C"  int32_t UnityARSessionNativeInterface_GetTrackingQuality_m3764224120 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetYUVTexCoordScale()
extern "C"  float UnityARSessionNativeInterface_GetYUVTexCoordScale_m2704052041 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorAdded_Invoke_m992918404 (internal_ARAnchorAdded_t3791587037 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorRemoved_Invoke_m4011290538 (internal_ARAnchorRemoved_t409959511 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorUpdated_Invoke_m2200307882 (internal_ARAnchorUpdated_t2928809250 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::Invoke(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void internal_ARFrameUpdate_Invoke_m585242343 (internal_ARFrameUpdate_t2726389622 * __this, internal_UnityARCamera_t1506237410  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t3649338848_m4208004768(__this /* static, unused */, p0, method) ((  GameObject_t3649338848 * (*) (RuntimeObject * /* static, unused */, GameObject_t3649338848 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m3859482884_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m433497810 (GameObject_t3649338848 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m2740154093 (Object_t1008057425 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3933397867 * GameObject_get_transform_m890220094 (GameObject_t3649338848 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.MeshFilter>()
#define GameObject_GetComponentInChildren_TisMeshFilter_t2678471223_m4019234351(__this, method) ((  MeshFilter_t2678471223 * (*) (GameObject_t3649338848 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_m3730568893_gshared)(__this, method)
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m3940090091 (Transform_t3933397867 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
extern "C"  void CommandBuffer__ctor_m762038758 (CommandBuffer_t1027034615 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  RenderTargetIdentifier_t2478716510  RenderTargetIdentifier_op_Implicit_m407650415 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern "C"  void CommandBuffer_Blit_m382886313 (CommandBuffer_t1027034615 * __this, Texture_t1132728222 * p0, RenderTargetIdentifier_t2478716510  p1, Material_t4055262778 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_AddCommandBuffer_m1530474523 (Camera_t226495598 * __this, int32_t p0, CommandBuffer_t1027034615 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_RemoveCommandBuffer_m4279420586 (Camera_t226495598 * __this, int32_t p0, CommandBuffer_t1027034615 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARVideoTextureHandles()
extern "C"  ARTextureHandles_t1375402930  UnityARSessionNativeInterface_GetARVideoTextureHandles_m3043872758 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m2427008840 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARVideo::InitializeCommandBuffer()
extern "C"  void UnityARVideo_InitializeCommandBuffer_m3792744751 (UnityARVideo_t2470488057 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t3724314788  Screen_get_currentResolution_m3445211963 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m494188502 (Resolution_t3724314788 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m1988903362 (Resolution_t3724314788 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  Texture2D_t2870930912 * Texture2D_CreateExternalTexture_m1925818354 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, bool p3, bool p4, IntPtr_t p5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m3057534198 (Texture_t1132728222 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m2518460338 (Texture_t1132728222 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)
extern "C"  void Texture2D_UpdateExternalTexture_m703548091 (Texture2D_t2870930912 * __this, IntPtr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1483606842 (Material_t4055262778 * __this, String_t* p0, Texture_t1132728222 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m4014868403 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t596762001  Vector3_get_zero_m325886990 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t3165733013  Quaternion_Euler_m437352610 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t596762001  Vector3_get_one_m2020581060 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t1288378485  Matrix4x4_TRS_m1848776295 (RuntimeObject * __this /* static, unused */, Vector3_t596762001  p0, Quaternion_t3165733013  p1, Vector3_t596762001  p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m1397732260 (Material_t4055262778 * __this, String_t* p0, Matrix4x4_t1288378485  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARYUVTexCoordScale()
extern "C"  float UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1899687960 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C"  void Material_SetFloat_m272696061 (Material_t4055262778 * __this, String_t* p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C"  void Material_SetInt_m130172955 (Material_t4055262778 * __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
#define List_1__ctor_m267030108(__this, method) ((  void (*) (List_1_t2392691131 *, const RuntimeMethod*))List_1__ctor_m112108964_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
#define List_1_Add_m423025013(__this, p0, method) ((  void (*) (List_1_t2392691131 *, GameObject_t3649338848 *, const RuntimeMethod*))List_1_Add_m2991374128_gshared)(__this, p0, method)
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t1376926224  Vector4_op_Implicit_m878911398 (RuntimeObject * __this /* static, unused */, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m1883443879(__this, p0, method) ((  GameObject_t3649338848 * (*) (List_1_t2392691131 *, int32_t, const RuntimeMethod*))List_1_get_Item_m3635692520_gshared)(__this, p0, method)
// System.Int64 System.Math::Min(System.Int64,System.Int64)
extern "C"  int64_t Math_Min_m3904695542 (RuntimeObject * __this /* static, unused */, int64_t p0, int64_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AR3DOFCameraManager::.ctor()
extern "C"  void AR3DOFCameraManager__ctor_m4125940046 (AR3DOFCameraManager_t3216700943 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AR3DOFCameraManager::Start()
extern "C"  void AR3DOFCameraManager_Start_m724138112 (AR3DOFCameraManager_t3216700943 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AR3DOFCameraManager_Start_m724138112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARKitSessionConfiguration_t3142208763  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Application_set_targetFrameRate_m714144203(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_session_3(L_0);
		Initobj (ARKitSessionConfiguration_t3142208763_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_alignment_0(0);
		(&V_0)->set_getPointCloudData_1((bool)1);
		(&V_0)->set_enableLightEstimation_2((bool)1);
		UnityARSessionNativeInterface_t3869411053 * L_1 = __this->get_m_session_3();
		ARKitSessionConfiguration_t3142208763  L_2 = V_0;
		NullCheck(L_1);
		UnityARSessionNativeInterface_RunWithConfig_m152596073(L_1, L_2, /*hidden argument*/NULL);
		Camera_t226495598 * L_3 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m528327324(NULL /*static, unused*/, L_3, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		Camera_t226495598 * L_5 = Camera_get_main_m2671983186(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_camera_2(L_5);
	}

IL_005a:
	{
		return;
	}
}
// System.Void AR3DOFCameraManager::SetCamera(UnityEngine.Camera)
extern "C"  void AR3DOFCameraManager_SetCamera_m3892213082 (AR3DOFCameraManager_t3216700943 * __this, Camera_t226495598 * ___newCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AR3DOFCameraManager_SetCamera_m3892213082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARVideo_t2470488057 * V_0 = NULL;
	{
		Camera_t226495598 * L_0 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Camera_t226495598 * L_2 = __this->get_m_camera_2();
		NullCheck(L_2);
		GameObject_t3649338848 * L_3 = Component_get_gameObject_m3065601689(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		UnityARVideo_t2470488057 * L_4 = GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688(L_3, /*hidden argument*/GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688_RuntimeMethod_var);
		V_0 = L_4;
		UnityARVideo_t2470488057 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_5, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityARVideo_t2470488057 * L_7 = V_0;
		NullCheck(L_7);
		Material_t4055262778 * L_8 = L_7->get_m_ClearMaterial_2();
		__this->set_savedClearMaterial_4(L_8);
		UnityARVideo_t2470488057 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_Destroy_m1839175560(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0040:
	{
		Camera_t226495598 * L_10 = ___newCamera0;
		AR3DOFCameraManager_SetupNewCamera_m2725600353(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AR3DOFCameraManager::SetupNewCamera(UnityEngine.Camera)
extern "C"  void AR3DOFCameraManager_SetupNewCamera_m2725600353 (AR3DOFCameraManager_t3216700943 * __this, Camera_t226495598 * ___newCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AR3DOFCameraManager_SetupNewCamera_m2725600353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARVideo_t2470488057 * V_0 = NULL;
	{
		Camera_t226495598 * L_0 = ___newCamera0;
		__this->set_m_camera_2(L_0);
		Camera_t226495598 * L_1 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0064;
		}
	}
	{
		Camera_t226495598 * L_3 = __this->get_m_camera_2();
		NullCheck(L_3);
		GameObject_t3649338848 * L_4 = Component_get_gameObject_m3065601689(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityARVideo_t2470488057 * L_5 = GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688(L_4, /*hidden argument*/GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688_RuntimeMethod_var);
		V_0 = L_5;
		UnityARVideo_t2470488057 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_6, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		UnityARVideo_t2470488057 * L_8 = V_0;
		NullCheck(L_8);
		Material_t4055262778 * L_9 = L_8->get_m_ClearMaterial_2();
		__this->set_savedClearMaterial_4(L_9);
		UnityARVideo_t2470488057 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_Destroy_m1839175560(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0047:
	{
		Camera_t226495598 * L_11 = __this->get_m_camera_2();
		NullCheck(L_11);
		GameObject_t3649338848 * L_12 = Component_get_gameObject_m3065601689(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityARVideo_t2470488057 * L_13 = GameObject_AddComponent_TisUnityARVideo_t2470488057_m3425289194(L_12, /*hidden argument*/GameObject_AddComponent_TisUnityARVideo_t2470488057_m3425289194_RuntimeMethod_var);
		V_0 = L_13;
		UnityARVideo_t2470488057 * L_14 = V_0;
		Material_t4055262778 * L_15 = __this->get_savedClearMaterial_4();
		NullCheck(L_14);
		L_14->set_m_ClearMaterial_2(L_15);
	}

IL_0064:
	{
		return;
	}
}
// System.Void AR3DOFCameraManager::Update()
extern "C"  void AR3DOFCameraManager_Update_m4197268577 (AR3DOFCameraManager_t3216700943 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AR3DOFCameraManager_Update_m4197268577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t1288378485  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t226495598 * L_0 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		UnityARSessionNativeInterface_t3869411053 * L_2 = __this->get_m_session_3();
		NullCheck(L_2);
		Matrix4x4_t1288378485  L_3 = UnityARSessionNativeInterface_GetCameraPose_m3014150810(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Camera_t226495598 * L_4 = __this->get_m_camera_2();
		NullCheck(L_4);
		Transform_t3933397867 * L_5 = Component_get_transform_m2033240428(L_4, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_6 = V_0;
		Vector3_t596762001  L_7 = UnityARMatrixOps_GetPosition_m3553979848(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localPosition_m3583812093(L_5, L_7, /*hidden argument*/NULL);
		Camera_t226495598 * L_8 = __this->get_m_camera_2();
		NullCheck(L_8);
		Transform_t3933397867 * L_9 = Component_get_transform_m2033240428(L_8, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_10 = V_0;
		Quaternion_t3165733013  L_11 = UnityARMatrixOps_GetRotation_m3890376831(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localRotation_m1950704683(L_9, L_11, /*hidden argument*/NULL);
		Camera_t226495598 * L_12 = __this->get_m_camera_2();
		UnityARSessionNativeInterface_t3869411053 * L_13 = __this->get_m_session_3();
		NullCheck(L_13);
		Matrix4x4_t1288378485  L_14 = UnityARSessionNativeInterface_GetCameraProjection_m248827655(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Camera_set_projectionMatrix_m1066889169(L_12, L_14, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void DontDestroyOnLoad::.ctor()
extern "C"  void DontDestroyOnLoad__ctor_m663724303 (DontDestroyOnLoad_t2575831040 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DontDestroyOnLoad::Start()
extern "C"  void DontDestroyOnLoad_Start_m2304192681 (DontDestroyOnLoad_t2575831040 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DontDestroyOnLoad_Start_m2304192681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t3649338848 * L_0 = Component_get_gameObject_m3065601689(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4110163480(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DontDestroyOnLoad::Update()
extern "C"  void DontDestroyOnLoad_Update_m1314657096 (DontDestroyOnLoad_t2575831040 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PointCloudParticleExample::.ctor()
extern "C"  void PointCloudParticleExample__ctor_m343527625 (PointCloudParticleExample_t3205217707 * __this, const RuntimeMethod* method)
{
	{
		__this->set_particleSize_4((1.0f));
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PointCloudParticleExample::Start()
extern "C"  void PointCloudParticleExample_Start_m1170738568 (PointCloudParticleExample_t3205217707 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointCloudParticleExample_Start_m1170738568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)PointCloudParticleExample_ARFrameUpdated_m1335679990_RuntimeMethod_var);
		ARFrameUpdate_t3333962149 * L_1 = (ARFrameUpdate_t3333962149 *)il2cpp_codegen_object_new(ARFrameUpdate_t3333962149_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m722081207(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1467721625(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ParticleSystem_t1777616458 * L_2 = __this->get_pointCloudParticlePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		ParticleSystem_t1777616458 * L_3 = Object_Instantiate_TisParticleSystem_t1777616458_m895722352(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisParticleSystem_t1777616458_m895722352_RuntimeMethod_var);
		__this->set_currentPS_7(L_3);
		__this->set_frameUpdated_6((bool)0);
		return;
	}
}
// System.Void PointCloudParticleExample::ARFrameUpdated(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void PointCloudParticleExample_ARFrameUpdated_m1335679990 (PointCloudParticleExample_t3205217707 * __this, UnityARCamera_t2376600594  ___camera0, const RuntimeMethod* method)
{
	{
		Vector3U5BU5D_t4153953548* L_0 = (&___camera0)->get_pointCloudData_4();
		__this->set_m_PointCloudData_5(L_0);
		__this->set_frameUpdated_6((bool)1);
		return;
	}
}
// System.Void PointCloudParticleExample::Update()
extern "C"  void PointCloudParticleExample_Update_m3373663811 (PointCloudParticleExample_t3205217707 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointCloudParticleExample_Update_m3373663811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ParticleU5BU5D_t419044168* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t596762001  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3U5BU5D_t4153953548* V_4 = NULL;
	int32_t V_5 = 0;
	ParticleU5BU5D_t419044168* V_6 = NULL;
	{
		bool L_0 = __this->get_frameUpdated_6();
		if (!L_0)
		{
			goto IL_00fa;
		}
	}
	{
		Vector3U5BU5D_t4153953548* L_1 = __this->get_m_PointCloudData_5();
		if (!L_1)
		{
			goto IL_00cb;
		}
	}
	{
		Vector3U5BU5D_t4153953548* L_2 = __this->get_m_PointCloudData_5();
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_00cb;
		}
	}
	{
		Vector3U5BU5D_t4153953548* L_3 = __this->get_m_PointCloudData_5();
		NullCheck(L_3);
		int32_t L_4 = __this->get_maxPointsToShow_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Min_m3947409433(NULL /*static, unused*/, (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))), L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		V_1 = ((ParticleU5BU5D_t419044168*)SZArrayNew(ParticleU5BU5D_t419044168_il2cpp_TypeInfo_var, (uint32_t)L_6));
		V_2 = 0;
		Vector3U5BU5D_t4153953548* L_7 = __this->get_m_PointCloudData_5();
		V_4 = L_7;
		V_5 = 0;
		goto IL_00ae;
	}

IL_0051:
	{
		Vector3U5BU5D_t4153953548* L_8 = V_4;
		int32_t L_9 = V_5;
		NullCheck(L_8);
		V_3 = (*(Vector3_t596762001 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))));
		ParticleU5BU5D_t419044168* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		Vector3_t596762001  L_12 = V_3;
		Particle_set_position_m2752520204(((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11))), L_12, /*hidden argument*/NULL);
		ParticleU5BU5D_t419044168* L_13 = V_1;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		Color_t320819310  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m2728568087((&L_15), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Color32_t2499566028  L_16 = Color32_op_Implicit_m4104578668(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Particle_set_startColor_m577033657(((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14))), L_16, /*hidden argument*/NULL);
		ParticleU5BU5D_t419044168* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		float L_19 = __this->get_particleSize_4();
		Particle_set_startSize_m2538937397(((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
		int32_t L_21 = V_5;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00ae:
	{
		int32_t L_22 = V_5;
		Vector3U5BU5D_t4153953548* L_23 = V_4;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
		{
			goto IL_0051;
		}
	}
	{
		ParticleSystem_t1777616458 * L_24 = __this->get_currentPS_7();
		ParticleU5BU5D_t419044168* L_25 = V_1;
		int32_t L_26 = V_0;
		NullCheck(L_24);
		ParticleSystem_SetParticles_m1829004744(L_24, L_25, L_26, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_00cb:
	{
		V_6 = ((ParticleU5BU5D_t419044168*)SZArrayNew(ParticleU5BU5D_t419044168_il2cpp_TypeInfo_var, (uint32_t)1));
		ParticleU5BU5D_t419044168* L_27 = V_6;
		NullCheck(L_27);
		Particle_set_startSize_m2538937397(((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), (0.0f), /*hidden argument*/NULL);
		ParticleSystem_t1777616458 * L_28 = __this->get_currentPS_7();
		ParticleU5BU5D_t419044168* L_29 = V_6;
		NullCheck(L_28);
		ParticleSystem_SetParticles_m1829004744(L_28, L_29, 1, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		__this->set_frameUpdated_6((bool)0);
	}

IL_00fa:
	{
		return;
	}
}
// System.Void UnityARCameraManager::.ctor()
extern "C"  void UnityARCameraManager__ctor_m1353594769 (UnityARCameraManager_t1803962782 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraManager::Start()
extern "C"  void UnityARCameraManager_Start_m443402790 (UnityARCameraManager_t1803962782 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraManager_Start_m443402790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARKitWorldTackingSessionConfiguration_t2186350340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Application_set_targetFrameRate_m714144203(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_session_3(L_0);
		Initobj (ARKitWorldTackingSessionConfiguration_t2186350340_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_planeDetection_1(1);
		(&V_0)->set_alignment_0(0);
		(&V_0)->set_getPointCloudData_2((bool)1);
		(&V_0)->set_enableLightEstimation_3((bool)1);
		UnityARSessionNativeInterface_t3869411053 * L_1 = __this->get_m_session_3();
		ARKitWorldTackingSessionConfiguration_t2186350340  L_2 = V_0;
		NullCheck(L_1);
		UnityARSessionNativeInterface_RunWithConfig_m3364444856(L_1, L_2, /*hidden argument*/NULL);
		Camera_t226495598 * L_3 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m528327324(NULL /*static, unused*/, L_3, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0062;
		}
	}
	{
		Camera_t226495598 * L_5 = Camera_get_main_m2671983186(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_camera_2(L_5);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UnityARCameraManager::SetCamera(UnityEngine.Camera)
extern "C"  void UnityARCameraManager_SetCamera_m3794718110 (UnityARCameraManager_t1803962782 * __this, Camera_t226495598 * ___newCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraManager_SetCamera_m3794718110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARVideo_t2470488057 * V_0 = NULL;
	{
		Camera_t226495598 * L_0 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Camera_t226495598 * L_2 = __this->get_m_camera_2();
		NullCheck(L_2);
		GameObject_t3649338848 * L_3 = Component_get_gameObject_m3065601689(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		UnityARVideo_t2470488057 * L_4 = GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688(L_3, /*hidden argument*/GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688_RuntimeMethod_var);
		V_0 = L_4;
		UnityARVideo_t2470488057 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_5, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityARVideo_t2470488057 * L_7 = V_0;
		NullCheck(L_7);
		Material_t4055262778 * L_8 = L_7->get_m_ClearMaterial_2();
		__this->set_savedClearMaterial_4(L_8);
		UnityARVideo_t2470488057 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_Destroy_m1839175560(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0040:
	{
		Camera_t226495598 * L_10 = ___newCamera0;
		UnityARCameraManager_SetupNewCamera_m3593985194(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraManager::SetupNewCamera(UnityEngine.Camera)
extern "C"  void UnityARCameraManager_SetupNewCamera_m3593985194 (UnityARCameraManager_t1803962782 * __this, Camera_t226495598 * ___newCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraManager_SetupNewCamera_m3593985194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARVideo_t2470488057 * V_0 = NULL;
	{
		Camera_t226495598 * L_0 = ___newCamera0;
		__this->set_m_camera_2(L_0);
		Camera_t226495598 * L_1 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0064;
		}
	}
	{
		Camera_t226495598 * L_3 = __this->get_m_camera_2();
		NullCheck(L_3);
		GameObject_t3649338848 * L_4 = Component_get_gameObject_m3065601689(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityARVideo_t2470488057 * L_5 = GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688(L_4, /*hidden argument*/GameObject_GetComponent_TisUnityARVideo_t2470488057_m4038439688_RuntimeMethod_var);
		V_0 = L_5;
		UnityARVideo_t2470488057 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_6, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		UnityARVideo_t2470488057 * L_8 = V_0;
		NullCheck(L_8);
		Material_t4055262778 * L_9 = L_8->get_m_ClearMaterial_2();
		__this->set_savedClearMaterial_4(L_9);
		UnityARVideo_t2470488057 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_Destroy_m1839175560(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0047:
	{
		Camera_t226495598 * L_11 = __this->get_m_camera_2();
		NullCheck(L_11);
		GameObject_t3649338848 * L_12 = Component_get_gameObject_m3065601689(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityARVideo_t2470488057 * L_13 = GameObject_AddComponent_TisUnityARVideo_t2470488057_m3425289194(L_12, /*hidden argument*/GameObject_AddComponent_TisUnityARVideo_t2470488057_m3425289194_RuntimeMethod_var);
		V_0 = L_13;
		UnityARVideo_t2470488057 * L_14 = V_0;
		Material_t4055262778 * L_15 = __this->get_savedClearMaterial_4();
		NullCheck(L_14);
		L_14->set_m_ClearMaterial_2(L_15);
	}

IL_0064:
	{
		return;
	}
}
// System.Void UnityARCameraManager::Update()
extern "C"  void UnityARCameraManager_Update_m3305433653 (UnityARCameraManager_t1803962782 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraManager_Update_m3305433653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t1288378485  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t226495598 * L_0 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		UnityARSessionNativeInterface_t3869411053 * L_2 = __this->get_m_session_3();
		NullCheck(L_2);
		Matrix4x4_t1288378485  L_3 = UnityARSessionNativeInterface_GetCameraPose_m3014150810(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Camera_t226495598 * L_4 = __this->get_m_camera_2();
		NullCheck(L_4);
		Transform_t3933397867 * L_5 = Component_get_transform_m2033240428(L_4, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_6 = V_0;
		Vector3_t596762001  L_7 = UnityARMatrixOps_GetPosition_m3553979848(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localPosition_m3583812093(L_5, L_7, /*hidden argument*/NULL);
		Camera_t226495598 * L_8 = __this->get_m_camera_2();
		NullCheck(L_8);
		Transform_t3933397867 * L_9 = Component_get_transform_m2033240428(L_8, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_10 = V_0;
		Quaternion_t3165733013  L_11 = UnityARMatrixOps_GetRotation_m3890376831(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localRotation_m1950704683(L_9, L_11, /*hidden argument*/NULL);
		Camera_t226495598 * L_12 = __this->get_m_camera_2();
		UnityARSessionNativeInterface_t3869411053 * L_13 = __this->get_m_session_3();
		NullCheck(L_13);
		Matrix4x4_t1288378485  L_14 = UnityARSessionNativeInterface_GetCameraProjection_m248827655(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Camera_set_projectionMatrix_m1066889169(L_12, L_14, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void UnityARCameraNearFar::.ctor()
extern "C"  void UnityARCameraNearFar__ctor_m153127307 (UnityARCameraNearFar_t4152080291 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraNearFar::Start()
extern "C"  void UnityARCameraNearFar_Start_m2942652737 (UnityARCameraNearFar_t4152080291 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraNearFar_Start_m2942652737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t226495598 * L_0 = Component_GetComponent_TisCamera_t226495598_m3685067992(__this, /*hidden argument*/Component_GetComponent_TisCamera_t226495598_m3685067992_RuntimeMethod_var);
		__this->set_attachedCamera_2(L_0);
		UnityARCameraNearFar_UpdateCameraClipPlanes_m123287997(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraNearFar::UpdateCameraClipPlanes()
extern "C"  void UnityARCameraNearFar_UpdateCameraClipPlanes_m123287997 (UnityARCameraNearFar_t4152080291 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraNearFar_UpdateCameraClipPlanes_m123287997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t226495598 * L_0 = __this->get_attachedCamera_2();
		NullCheck(L_0);
		float L_1 = Camera_get_nearClipPlane_m418873954(L_0, /*hidden argument*/NULL);
		__this->set_currentNearZ_3(L_1);
		Camera_t226495598 * L_2 = __this->get_attachedCamera_2();
		NullCheck(L_2);
		float L_3 = Camera_get_farClipPlane_m2103707533(L_2, /*hidden argument*/NULL);
		__this->set_currentFarZ_4(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_4 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_currentNearZ_3();
		float L_6 = __this->get_currentFarZ_4();
		NullCheck(L_4);
		UnityARSessionNativeInterface_SetCameraClipPlanes_m540333021(L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraNearFar::Update()
extern "C"  void UnityARCameraNearFar_Update_m381574187 (UnityARCameraNearFar_t4152080291 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_currentNearZ_3();
		Camera_t226495598 * L_1 = __this->get_attachedCamera_2();
		NullCheck(L_1);
		float L_2 = Camera_get_nearClipPlane_m418873954(L_1, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_002c;
		}
	}
	{
		float L_3 = __this->get_currentFarZ_4();
		Camera_t226495598 * L_4 = __this->get_attachedCamera_2();
		NullCheck(L_4);
		float L_5 = Camera_get_farClipPlane_m2103707533(L_4, /*hidden argument*/NULL);
		if ((((float)L_3) == ((float)L_5)))
		{
			goto IL_0032;
		}
	}

IL_002c:
	{
		UnityARCameraNearFar_UpdateCameraClipPlanes_m123287997(__this, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARAnchor
extern "C" void ARAnchor_t4044008708_marshal_pinvoke(const ARAnchor_t4044008708& unmarshaled, ARAnchor_t4044008708_marshaled_pinvoke& marshaled)
{
	marshaled.___identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_identifier_0());
	marshaled.___transform_1 = unmarshaled.get_transform_1();
}
extern "C" void ARAnchor_t4044008708_marshal_pinvoke_back(const ARAnchor_t4044008708_marshaled_pinvoke& marshaled, ARAnchor_t4044008708& unmarshaled)
{
	unmarshaled.set_identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___identifier_0));
	Matrix4x4_t1288378485  unmarshaled_transform_temp_1;
	memset(&unmarshaled_transform_temp_1, 0, sizeof(unmarshaled_transform_temp_1));
	unmarshaled_transform_temp_1 = marshaled.___transform_1;
	unmarshaled.set_transform_1(unmarshaled_transform_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARAnchor
extern "C" void ARAnchor_t4044008708_marshal_pinvoke_cleanup(ARAnchor_t4044008708_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___identifier_0);
	marshaled.___identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARAnchor
extern "C" void ARAnchor_t4044008708_marshal_com(const ARAnchor_t4044008708& unmarshaled, ARAnchor_t4044008708_marshaled_com& marshaled)
{
	marshaled.___identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_identifier_0());
	marshaled.___transform_1 = unmarshaled.get_transform_1();
}
extern "C" void ARAnchor_t4044008708_marshal_com_back(const ARAnchor_t4044008708_marshaled_com& marshaled, ARAnchor_t4044008708& unmarshaled)
{
	unmarshaled.set_identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___identifier_0));
	Matrix4x4_t1288378485  unmarshaled_transform_temp_1;
	memset(&unmarshaled_transform_temp_1, 0, sizeof(unmarshaled_transform_temp_1));
	unmarshaled_transform_temp_1 = marshaled.___transform_1;
	unmarshaled.set_transform_1(unmarshaled_transform_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARAnchor
extern "C" void ARAnchor_t4044008708_marshal_com_cleanup(ARAnchor_t4044008708_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___identifier_0);
	marshaled.___identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARHitTestResult
extern "C" void ARHitTestResult_t1803723679_marshal_pinvoke(const ARHitTestResult_t1803723679& unmarshaled, ARHitTestResult_t1803723679_marshaled_pinvoke& marshaled)
{
	marshaled.___type_0 = unmarshaled.get_type_0();
	marshaled.___distance_1 = unmarshaled.get_distance_1();
	marshaled.___localTransform_2 = unmarshaled.get_localTransform_2();
	marshaled.___worldTransform_3 = unmarshaled.get_worldTransform_3();
	marshaled.___anchorIdentifier_4 = il2cpp_codegen_marshal_string(unmarshaled.get_anchorIdentifier_4());
	marshaled.___isValid_5 = static_cast<int32_t>(unmarshaled.get_isValid_5());
}
extern "C" void ARHitTestResult_t1803723679_marshal_pinvoke_back(const ARHitTestResult_t1803723679_marshaled_pinvoke& marshaled, ARHitTestResult_t1803723679& unmarshaled)
{
	int64_t unmarshaled_type_temp_0 = 0;
	unmarshaled_type_temp_0 = marshaled.___type_0;
	unmarshaled.set_type_0(unmarshaled_type_temp_0);
	double unmarshaled_distance_temp_1 = 0.0;
	unmarshaled_distance_temp_1 = marshaled.___distance_1;
	unmarshaled.set_distance_1(unmarshaled_distance_temp_1);
	Matrix4x4_t1288378485  unmarshaled_localTransform_temp_2;
	memset(&unmarshaled_localTransform_temp_2, 0, sizeof(unmarshaled_localTransform_temp_2));
	unmarshaled_localTransform_temp_2 = marshaled.___localTransform_2;
	unmarshaled.set_localTransform_2(unmarshaled_localTransform_temp_2);
	Matrix4x4_t1288378485  unmarshaled_worldTransform_temp_3;
	memset(&unmarshaled_worldTransform_temp_3, 0, sizeof(unmarshaled_worldTransform_temp_3));
	unmarshaled_worldTransform_temp_3 = marshaled.___worldTransform_3;
	unmarshaled.set_worldTransform_3(unmarshaled_worldTransform_temp_3);
	unmarshaled.set_anchorIdentifier_4(il2cpp_codegen_marshal_string_result(marshaled.___anchorIdentifier_4));
	bool unmarshaled_isValid_temp_5 = false;
	unmarshaled_isValid_temp_5 = static_cast<bool>(marshaled.___isValid_5);
	unmarshaled.set_isValid_5(unmarshaled_isValid_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARHitTestResult
extern "C" void ARHitTestResult_t1803723679_marshal_pinvoke_cleanup(ARHitTestResult_t1803723679_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___anchorIdentifier_4);
	marshaled.___anchorIdentifier_4 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARHitTestResult
extern "C" void ARHitTestResult_t1803723679_marshal_com(const ARHitTestResult_t1803723679& unmarshaled, ARHitTestResult_t1803723679_marshaled_com& marshaled)
{
	marshaled.___type_0 = unmarshaled.get_type_0();
	marshaled.___distance_1 = unmarshaled.get_distance_1();
	marshaled.___localTransform_2 = unmarshaled.get_localTransform_2();
	marshaled.___worldTransform_3 = unmarshaled.get_worldTransform_3();
	marshaled.___anchorIdentifier_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_anchorIdentifier_4());
	marshaled.___isValid_5 = static_cast<int32_t>(unmarshaled.get_isValid_5());
}
extern "C" void ARHitTestResult_t1803723679_marshal_com_back(const ARHitTestResult_t1803723679_marshaled_com& marshaled, ARHitTestResult_t1803723679& unmarshaled)
{
	int64_t unmarshaled_type_temp_0 = 0;
	unmarshaled_type_temp_0 = marshaled.___type_0;
	unmarshaled.set_type_0(unmarshaled_type_temp_0);
	double unmarshaled_distance_temp_1 = 0.0;
	unmarshaled_distance_temp_1 = marshaled.___distance_1;
	unmarshaled.set_distance_1(unmarshaled_distance_temp_1);
	Matrix4x4_t1288378485  unmarshaled_localTransform_temp_2;
	memset(&unmarshaled_localTransform_temp_2, 0, sizeof(unmarshaled_localTransform_temp_2));
	unmarshaled_localTransform_temp_2 = marshaled.___localTransform_2;
	unmarshaled.set_localTransform_2(unmarshaled_localTransform_temp_2);
	Matrix4x4_t1288378485  unmarshaled_worldTransform_temp_3;
	memset(&unmarshaled_worldTransform_temp_3, 0, sizeof(unmarshaled_worldTransform_temp_3));
	unmarshaled_worldTransform_temp_3 = marshaled.___worldTransform_3;
	unmarshaled.set_worldTransform_3(unmarshaled_worldTransform_temp_3);
	unmarshaled.set_anchorIdentifier_4(il2cpp_codegen_marshal_bstring_result(marshaled.___anchorIdentifier_4));
	bool unmarshaled_isValid_temp_5 = false;
	unmarshaled_isValid_temp_5 = static_cast<bool>(marshaled.___isValid_5);
	unmarshaled.set_isValid_5(unmarshaled_isValid_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARHitTestResult
extern "C" void ARHitTestResult_t1803723679_marshal_com_cleanup(ARHitTestResult_t1803723679_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___anchorIdentifier_4);
	marshaled.___anchorIdentifier_4 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARKitSessionConfiguration
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke(const ARKitSessionConfiguration_t3142208763& unmarshaled, ARKitSessionConfiguration_t3142208763_marshaled_pinvoke& marshaled)
{
	marshaled.___alignment_0 = unmarshaled.get_alignment_0();
	marshaled.___getPointCloudData_1 = static_cast<int32_t>(unmarshaled.get_getPointCloudData_1());
	marshaled.___enableLightEstimation_2 = static_cast<int32_t>(unmarshaled.get_enableLightEstimation_2());
}
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke_back(const ARKitSessionConfiguration_t3142208763_marshaled_pinvoke& marshaled, ARKitSessionConfiguration_t3142208763& unmarshaled)
{
	int32_t unmarshaled_alignment_temp_0 = 0;
	unmarshaled_alignment_temp_0 = marshaled.___alignment_0;
	unmarshaled.set_alignment_0(unmarshaled_alignment_temp_0);
	bool unmarshaled_getPointCloudData_temp_1 = false;
	unmarshaled_getPointCloudData_temp_1 = static_cast<bool>(marshaled.___getPointCloudData_1);
	unmarshaled.set_getPointCloudData_1(unmarshaled_getPointCloudData_temp_1);
	bool unmarshaled_enableLightEstimation_temp_2 = false;
	unmarshaled_enableLightEstimation_temp_2 = static_cast<bool>(marshaled.___enableLightEstimation_2);
	unmarshaled.set_enableLightEstimation_2(unmarshaled_enableLightEstimation_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARKitSessionConfiguration
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke_cleanup(ARKitSessionConfiguration_t3142208763_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARKitSessionConfiguration
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_com(const ARKitSessionConfiguration_t3142208763& unmarshaled, ARKitSessionConfiguration_t3142208763_marshaled_com& marshaled)
{
	marshaled.___alignment_0 = unmarshaled.get_alignment_0();
	marshaled.___getPointCloudData_1 = static_cast<int32_t>(unmarshaled.get_getPointCloudData_1());
	marshaled.___enableLightEstimation_2 = static_cast<int32_t>(unmarshaled.get_enableLightEstimation_2());
}
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_com_back(const ARKitSessionConfiguration_t3142208763_marshaled_com& marshaled, ARKitSessionConfiguration_t3142208763& unmarshaled)
{
	int32_t unmarshaled_alignment_temp_0 = 0;
	unmarshaled_alignment_temp_0 = marshaled.___alignment_0;
	unmarshaled.set_alignment_0(unmarshaled_alignment_temp_0);
	bool unmarshaled_getPointCloudData_temp_1 = false;
	unmarshaled_getPointCloudData_temp_1 = static_cast<bool>(marshaled.___getPointCloudData_1);
	unmarshaled.set_getPointCloudData_1(unmarshaled_getPointCloudData_temp_1);
	bool unmarshaled_enableLightEstimation_temp_2 = false;
	unmarshaled_enableLightEstimation_temp_2 = static_cast<bool>(marshaled.___enableLightEstimation_2);
	unmarshaled.set_enableLightEstimation_2(unmarshaled_enableLightEstimation_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARKitSessionConfiguration
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_com_cleanup(ARKitSessionConfiguration_t3142208763_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.iOS.ARKitSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,System.Boolean,System.Boolean)
extern "C"  void ARKitSessionConfiguration__ctor_m373048997 (ARKitSessionConfiguration_t3142208763 * __this, int32_t ___alignment0, bool ___getPointCloudData1, bool ___enableLightEstimation2, const RuntimeMethod* method)
{
	{
		bool L_0 = ___getPointCloudData1;
		__this->set_getPointCloudData_1(L_0);
		int32_t L_1 = ___alignment0;
		__this->set_alignment_0(L_1);
		bool L_2 = ___enableLightEstimation2;
		__this->set_enableLightEstimation_2(L_2);
		return;
	}
}
extern "C"  void ARKitSessionConfiguration__ctor_m373048997_AdjustorThunk (RuntimeObject * __this, int32_t ___alignment0, bool ___getPointCloudData1, bool ___enableLightEstimation2, const RuntimeMethod* method)
{
	ARKitSessionConfiguration_t3142208763 * _thisAdjusted = reinterpret_cast<ARKitSessionConfiguration_t3142208763 *>(__this + 1);
	ARKitSessionConfiguration__ctor_m373048997(_thisAdjusted, ___alignment0, ___getPointCloudData1, ___enableLightEstimation2, method);
}
// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::get_IsSupported()
extern "C"  bool ARKitSessionConfiguration_get_IsSupported_m3459182394 (ARKitSessionConfiguration_t3142208763 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = ARKitSessionConfiguration_IsARKitSessionConfigurationSupported_m3090600542(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  bool ARKitSessionConfiguration_get_IsSupported_m3459182394_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ARKitSessionConfiguration_t3142208763 * _thisAdjusted = reinterpret_cast<ARKitSessionConfiguration_t3142208763 *>(__this + 1);
	return ARKitSessionConfiguration_get_IsSupported_m3459182394(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.iOS.ARKitSessionConfiguration::set_IsSupported(System.Boolean)
extern "C"  void ARKitSessionConfiguration_set_IsSupported_m10952259 (ARKitSessionConfiguration_t3142208763 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void ARKitSessionConfiguration_set_IsSupported_m10952259_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	ARKitSessionConfiguration_t3142208763 * _thisAdjusted = reinterpret_cast<ARKitSessionConfiguration_t3142208763 *>(__this + 1);
	ARKitSessionConfiguration_set_IsSupported_m10952259(_thisAdjusted, ___value0, method);
}
extern "C" int32_t DEFAULT_CALL IsARKitSessionConfigurationSupported();
// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::IsARKitSessionConfigurationSupported()
extern "C"  bool ARKitSessionConfiguration_IsARKitSessionConfigurationSupported_m3090600542 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(IsARKitSessionConfigurationSupported)();

	return static_cast<bool>(returnValue);
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke(const ARKitWorldTackingSessionConfiguration_t2186350340& unmarshaled, ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke& marshaled)
{
	marshaled.___alignment_0 = unmarshaled.get_alignment_0();
	marshaled.___planeDetection_1 = unmarshaled.get_planeDetection_1();
	marshaled.___getPointCloudData_2 = static_cast<int32_t>(unmarshaled.get_getPointCloudData_2());
	marshaled.___enableLightEstimation_3 = static_cast<int32_t>(unmarshaled.get_enableLightEstimation_3());
}
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_back(const ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke& marshaled, ARKitWorldTackingSessionConfiguration_t2186350340& unmarshaled)
{
	int32_t unmarshaled_alignment_temp_0 = 0;
	unmarshaled_alignment_temp_0 = marshaled.___alignment_0;
	unmarshaled.set_alignment_0(unmarshaled_alignment_temp_0);
	int32_t unmarshaled_planeDetection_temp_1 = 0;
	unmarshaled_planeDetection_temp_1 = marshaled.___planeDetection_1;
	unmarshaled.set_planeDetection_1(unmarshaled_planeDetection_temp_1);
	bool unmarshaled_getPointCloudData_temp_2 = false;
	unmarshaled_getPointCloudData_temp_2 = static_cast<bool>(marshaled.___getPointCloudData_2);
	unmarshaled.set_getPointCloudData_2(unmarshaled_getPointCloudData_temp_2);
	bool unmarshaled_enableLightEstimation_temp_3 = false;
	unmarshaled_enableLightEstimation_temp_3 = static_cast<bool>(marshaled.___enableLightEstimation_3);
	unmarshaled.set_enableLightEstimation_3(unmarshaled_enableLightEstimation_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_cleanup(ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_com(const ARKitWorldTackingSessionConfiguration_t2186350340& unmarshaled, ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_com& marshaled)
{
	marshaled.___alignment_0 = unmarshaled.get_alignment_0();
	marshaled.___planeDetection_1 = unmarshaled.get_planeDetection_1();
	marshaled.___getPointCloudData_2 = static_cast<int32_t>(unmarshaled.get_getPointCloudData_2());
	marshaled.___enableLightEstimation_3 = static_cast<int32_t>(unmarshaled.get_enableLightEstimation_3());
}
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_com_back(const ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_com& marshaled, ARKitWorldTackingSessionConfiguration_t2186350340& unmarshaled)
{
	int32_t unmarshaled_alignment_temp_0 = 0;
	unmarshaled_alignment_temp_0 = marshaled.___alignment_0;
	unmarshaled.set_alignment_0(unmarshaled_alignment_temp_0);
	int32_t unmarshaled_planeDetection_temp_1 = 0;
	unmarshaled_planeDetection_temp_1 = marshaled.___planeDetection_1;
	unmarshaled.set_planeDetection_1(unmarshaled_planeDetection_temp_1);
	bool unmarshaled_getPointCloudData_temp_2 = false;
	unmarshaled_getPointCloudData_temp_2 = static_cast<bool>(marshaled.___getPointCloudData_2);
	unmarshaled.set_getPointCloudData_2(unmarshaled_getPointCloudData_temp_2);
	bool unmarshaled_enableLightEstimation_temp_3 = false;
	unmarshaled_enableLightEstimation_temp_3 = static_cast<bool>(marshaled.___enableLightEstimation_3);
	unmarshaled.set_enableLightEstimation_3(unmarshaled_enableLightEstimation_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_com_cleanup(ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,UnityEngine.XR.iOS.UnityARPlaneDetection,System.Boolean,System.Boolean)
extern "C"  void ARKitWorldTackingSessionConfiguration__ctor_m2859336466 (ARKitWorldTackingSessionConfiguration_t2186350340 * __this, int32_t ___alignment0, int32_t ___planeDetection1, bool ___getPointCloudData2, bool ___enableLightEstimation3, const RuntimeMethod* method)
{
	{
		bool L_0 = ___getPointCloudData2;
		__this->set_getPointCloudData_2(L_0);
		int32_t L_1 = ___alignment0;
		__this->set_alignment_0(L_1);
		int32_t L_2 = ___planeDetection1;
		__this->set_planeDetection_1(L_2);
		bool L_3 = ___enableLightEstimation3;
		__this->set_enableLightEstimation_3(L_3);
		return;
	}
}
extern "C"  void ARKitWorldTackingSessionConfiguration__ctor_m2859336466_AdjustorThunk (RuntimeObject * __this, int32_t ___alignment0, int32_t ___planeDetection1, bool ___getPointCloudData2, bool ___enableLightEstimation3, const RuntimeMethod* method)
{
	ARKitWorldTackingSessionConfiguration_t2186350340 * _thisAdjusted = reinterpret_cast<ARKitWorldTackingSessionConfiguration_t2186350340 *>(__this + 1);
	ARKitWorldTackingSessionConfiguration__ctor_m2859336466(_thisAdjusted, ___alignment0, ___planeDetection1, ___getPointCloudData2, ___enableLightEstimation3, method);
}
// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::get_IsSupported()
extern "C"  bool ARKitWorldTackingSessionConfiguration_get_IsSupported_m1346839597 (ARKitWorldTackingSessionConfiguration_t2186350340 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = ARKitWorldTackingSessionConfiguration_IsARKitWorldTrackingSessionConfigurationSupported_m4262940539(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  bool ARKitWorldTackingSessionConfiguration_get_IsSupported_m1346839597_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ARKitWorldTackingSessionConfiguration_t2186350340 * _thisAdjusted = reinterpret_cast<ARKitWorldTackingSessionConfiguration_t2186350340 *>(__this + 1);
	return ARKitWorldTackingSessionConfiguration_get_IsSupported_m1346839597(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::set_IsSupported(System.Boolean)
extern "C"  void ARKitWorldTackingSessionConfiguration_set_IsSupported_m2691427501 (ARKitWorldTackingSessionConfiguration_t2186350340 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void ARKitWorldTackingSessionConfiguration_set_IsSupported_m2691427501_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	ARKitWorldTackingSessionConfiguration_t2186350340 * _thisAdjusted = reinterpret_cast<ARKitWorldTackingSessionConfiguration_t2186350340 *>(__this + 1);
	ARKitWorldTackingSessionConfiguration_set_IsSupported_m2691427501(_thisAdjusted, ___value0, method);
}
extern "C" int32_t DEFAULT_CALL IsARKitWorldTrackingSessionConfigurationSupported();
// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::IsARKitWorldTrackingSessionConfigurationSupported()
extern "C"  bool ARKitWorldTackingSessionConfiguration_IsARKitWorldTrackingSessionConfigurationSupported_m4262940539 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(IsARKitWorldTrackingSessionConfigurationSupported)();

	return static_cast<bool>(returnValue);
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARPlaneAnchor
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke(const ARPlaneAnchor_t1843157284& unmarshaled, ARPlaneAnchor_t1843157284_marshaled_pinvoke& marshaled)
{
	marshaled.___identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_identifier_0());
	marshaled.___transform_1 = unmarshaled.get_transform_1();
	marshaled.___alignment_2 = unmarshaled.get_alignment_2();
	marshaled.___center_3 = unmarshaled.get_center_3();
	marshaled.___extent_4 = unmarshaled.get_extent_4();
}
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke_back(const ARPlaneAnchor_t1843157284_marshaled_pinvoke& marshaled, ARPlaneAnchor_t1843157284& unmarshaled)
{
	unmarshaled.set_identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___identifier_0));
	Matrix4x4_t1288378485  unmarshaled_transform_temp_1;
	memset(&unmarshaled_transform_temp_1, 0, sizeof(unmarshaled_transform_temp_1));
	unmarshaled_transform_temp_1 = marshaled.___transform_1;
	unmarshaled.set_transform_1(unmarshaled_transform_temp_1);
	int64_t unmarshaled_alignment_temp_2 = 0;
	unmarshaled_alignment_temp_2 = marshaled.___alignment_2;
	unmarshaled.set_alignment_2(unmarshaled_alignment_temp_2);
	Vector3_t596762001  unmarshaled_center_temp_3;
	memset(&unmarshaled_center_temp_3, 0, sizeof(unmarshaled_center_temp_3));
	unmarshaled_center_temp_3 = marshaled.___center_3;
	unmarshaled.set_center_3(unmarshaled_center_temp_3);
	Vector3_t596762001  unmarshaled_extent_temp_4;
	memset(&unmarshaled_extent_temp_4, 0, sizeof(unmarshaled_extent_temp_4));
	unmarshaled_extent_temp_4 = marshaled.___extent_4;
	unmarshaled.set_extent_4(unmarshaled_extent_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARPlaneAnchor
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke_cleanup(ARPlaneAnchor_t1843157284_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___identifier_0);
	marshaled.___identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARPlaneAnchor
extern "C" void ARPlaneAnchor_t1843157284_marshal_com(const ARPlaneAnchor_t1843157284& unmarshaled, ARPlaneAnchor_t1843157284_marshaled_com& marshaled)
{
	marshaled.___identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_identifier_0());
	marshaled.___transform_1 = unmarshaled.get_transform_1();
	marshaled.___alignment_2 = unmarshaled.get_alignment_2();
	marshaled.___center_3 = unmarshaled.get_center_3();
	marshaled.___extent_4 = unmarshaled.get_extent_4();
}
extern "C" void ARPlaneAnchor_t1843157284_marshal_com_back(const ARPlaneAnchor_t1843157284_marshaled_com& marshaled, ARPlaneAnchor_t1843157284& unmarshaled)
{
	unmarshaled.set_identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___identifier_0));
	Matrix4x4_t1288378485  unmarshaled_transform_temp_1;
	memset(&unmarshaled_transform_temp_1, 0, sizeof(unmarshaled_transform_temp_1));
	unmarshaled_transform_temp_1 = marshaled.___transform_1;
	unmarshaled.set_transform_1(unmarshaled_transform_temp_1);
	int64_t unmarshaled_alignment_temp_2 = 0;
	unmarshaled_alignment_temp_2 = marshaled.___alignment_2;
	unmarshaled.set_alignment_2(unmarshaled_alignment_temp_2);
	Vector3_t596762001  unmarshaled_center_temp_3;
	memset(&unmarshaled_center_temp_3, 0, sizeof(unmarshaled_center_temp_3));
	unmarshaled_center_temp_3 = marshaled.___center_3;
	unmarshaled.set_center_3(unmarshaled_center_temp_3);
	Vector3_t596762001  unmarshaled_extent_temp_4;
	memset(&unmarshaled_extent_temp_4, 0, sizeof(unmarshaled_extent_temp_4));
	unmarshaled_extent_temp_4 = marshaled.___extent_4;
	unmarshaled.set_extent_4(unmarshaled_extent_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARPlaneAnchor
extern "C" void ARPlaneAnchor_t1843157284_marshal_com_cleanup(ARPlaneAnchor_t1843157284_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___identifier_0);
	marshaled.___identifier_0 = NULL;
}
// System.Void UnityEngine.XR.iOS.ARPlaneAnchorGameObject::.ctor()
extern "C"  void ARPlaneAnchorGameObject__ctor_m2225510302 (ARPlaneAnchorGameObject_t3146329710 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::.ctor()
extern "C"  void UnityARAnchorManager__ctor_m1537079611 (UnityARAnchorManager_t3624459219 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager__ctor_m1537079611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		Dictionary_2_t3033730875 * L_0 = (Dictionary_2_t3033730875 *)il2cpp_codegen_object_new(Dictionary_2_t3033730875_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2541633073(L_0, /*hidden argument*/Dictionary_2__ctor_m2541633073_RuntimeMethod_var);
		__this->set_planeAnchorMap_0(L_0);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnityARAnchorManager_AddAnchor_m3732879992_RuntimeMethod_var);
		ARAnchorAdded_t1580091102 * L_2 = (ARAnchorAdded_t1580091102 *)il2cpp_codegen_object_new(ARAnchorAdded_t1580091102_il2cpp_TypeInfo_var);
		ARAnchorAdded__ctor_m2351544190(L_2, __this, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m3219973195(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)UnityARAnchorManager_UpdateAnchor_m1796371617_RuntimeMethod_var);
		ARAnchorUpdated_t1492128022 * L_4 = (ARAnchorUpdated_t1492128022 *)il2cpp_codegen_object_new(ARAnchorUpdated_t1492128022_il2cpp_TypeInfo_var);
		ARAnchorUpdated__ctor_m401717748(L_4, __this, L_3, /*hidden argument*/NULL);
		UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m994455513(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)UnityARAnchorManager_RemoveAnchor_m1348574666_RuntimeMethod_var);
		ARAnchorRemoved_t3663543047 * L_6 = (ARAnchorRemoved_t3663543047 *)il2cpp_codegen_object_new(ARAnchorRemoved_t3663543047_il2cpp_TypeInfo_var);
		ARAnchorRemoved__ctor_m1468226077(L_6, __this, L_5, /*hidden argument*/NULL);
		UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m1279350122(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::AddAnchor(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void UnityARAnchorManager_AddAnchor_m3732879992 (UnityARAnchorManager_t3624459219 * __this, ARPlaneAnchor_t1843157284  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_AddAnchor_m3732879992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t3649338848 * V_0 = NULL;
	ARPlaneAnchorGameObject_t3146329710 * V_1 = NULL;
	{
		ARPlaneAnchor_t1843157284  L_0 = ___arPlaneAnchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t2651983316_il2cpp_TypeInfo_var);
		GameObject_t3649338848 * L_1 = UnityARUtility_CreatePlaneInScene_m2413657456(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3649338848 * L_2 = V_0;
		NullCheck(L_2);
		GameObject_AddComponent_TisDontDestroyOnLoad_t2575831040_m3797858659(L_2, /*hidden argument*/GameObject_AddComponent_TisDontDestroyOnLoad_t2575831040_m3797858659_RuntimeMethod_var);
		ARPlaneAnchorGameObject_t3146329710 * L_3 = (ARPlaneAnchorGameObject_t3146329710 *)il2cpp_codegen_object_new(ARPlaneAnchorGameObject_t3146329710_il2cpp_TypeInfo_var);
		ARPlaneAnchorGameObject__ctor_m2225510302(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		ARPlaneAnchorGameObject_t3146329710 * L_4 = V_1;
		ARPlaneAnchor_t1843157284  L_5 = ___arPlaneAnchor0;
		NullCheck(L_4);
		L_4->set_planeAnchor_1(L_5);
		ARPlaneAnchorGameObject_t3146329710 * L_6 = V_1;
		GameObject_t3649338848 * L_7 = V_0;
		NullCheck(L_6);
		L_6->set_gameObject_0(L_7);
		Dictionary_2_t3033730875 * L_8 = __this->get_planeAnchorMap_0();
		String_t* L_9 = (&___arPlaneAnchor0)->get_identifier_0();
		ARPlaneAnchorGameObject_t3146329710 * L_10 = V_1;
		NullCheck(L_8);
		Dictionary_2_Add_m113011829(L_8, L_9, L_10, /*hidden argument*/Dictionary_2_Add_m113011829_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::RemoveAnchor(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void UnityARAnchorManager_RemoveAnchor_m1348574666 (UnityARAnchorManager_t3624459219 * __this, ARPlaneAnchor_t1843157284  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_RemoveAnchor_m1348574666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchorGameObject_t3146329710 * V_0 = NULL;
	{
		Dictionary_2_t3033730875 * L_0 = __this->get_planeAnchorMap_0();
		String_t* L_1 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m4199403017(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m4199403017_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		Dictionary_2_t3033730875 * L_3 = __this->get_planeAnchorMap_0();
		String_t* L_4 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_3);
		ARPlaneAnchorGameObject_t3146329710 * L_5 = Dictionary_2_get_Item_m1097037868(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m1097037868_RuntimeMethod_var);
		V_0 = L_5;
		ARPlaneAnchorGameObject_t3146329710 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_t3649338848 * L_7 = L_6->get_gameObject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_Destroy_m1839175560(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Dictionary_2_t3033730875 * L_8 = __this->get_planeAnchorMap_0();
		String_t* L_9 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_8);
		Dictionary_2_Remove_m4064418252(L_8, L_9, /*hidden argument*/Dictionary_2_Remove_m4064418252_RuntimeMethod_var);
	}

IL_0048:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::UpdateAnchor(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void UnityARAnchorManager_UpdateAnchor_m1796371617 (UnityARAnchorManager_t3624459219 * __this, ARPlaneAnchor_t1843157284  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_UpdateAnchor_m1796371617_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchorGameObject_t3146329710 * V_0 = NULL;
	{
		Dictionary_2_t3033730875 * L_0 = __this->get_planeAnchorMap_0();
		String_t* L_1 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m4199403017(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m4199403017_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		Dictionary_2_t3033730875 * L_3 = __this->get_planeAnchorMap_0();
		String_t* L_4 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_3);
		ARPlaneAnchorGameObject_t3146329710 * L_5 = Dictionary_2_get_Item_m1097037868(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m1097037868_RuntimeMethod_var);
		V_0 = L_5;
		ARPlaneAnchorGameObject_t3146329710 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_t3649338848 * L_7 = L_6->get_gameObject_0();
		ARPlaneAnchor_t1843157284  L_8 = ___arPlaneAnchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t2651983316_il2cpp_TypeInfo_var);
		UnityARUtility_UpdatePlaneWithAnchorTransform_m923713608(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		ARPlaneAnchorGameObject_t3146329710 * L_9 = V_0;
		ARPlaneAnchor_t1843157284  L_10 = ___arPlaneAnchor0;
		NullCheck(L_9);
		L_9->set_planeAnchor_1(L_10);
		Dictionary_2_t3033730875 * L_11 = __this->get_planeAnchorMap_0();
		String_t* L_12 = (&___arPlaneAnchor0)->get_identifier_0();
		ARPlaneAnchorGameObject_t3146329710 * L_13 = V_0;
		NullCheck(L_11);
		Dictionary_2_set_Item_m4260355931(L_11, L_12, L_13, /*hidden argument*/Dictionary_2_set_Item_m4260355931_RuntimeMethod_var);
	}

IL_0051:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::Destroy()
extern "C"  void UnityARAnchorManager_Destroy_m3970721311 (UnityARAnchorManager_t3624459219 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_Destroy_m3970721311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchorGameObject_t3146329710 * V_0 = NULL;
	Enumerator_t3238540930  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1889681993 * L_0 = UnityARAnchorManager_GetCurrentPlaneAnchors_m3710250286(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t3238540930  L_1 = List_1_GetEnumerator_m2396092950(L_0, /*hidden argument*/List_1_GetEnumerator_m2396092950_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			ARPlaneAnchorGameObject_t3146329710 * L_2 = Enumerator_get_Current_m3648846247((&V_1), /*hidden argument*/Enumerator_get_Current_m3648846247_RuntimeMethod_var);
			V_0 = L_2;
			ARPlaneAnchorGameObject_t3146329710 * L_3 = V_0;
			NullCheck(L_3);
			GameObject_t3649338848 * L_4 = L_3->get_gameObject_0();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
			Object_Destroy_m1839175560(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m2283436456((&V_1), /*hidden argument*/Enumerator_MoveNext_m2283436456_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4042749809((&V_1), /*hidden argument*/Enumerator_Dispose_m4042749809_RuntimeMethod_var);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_0043:
	{
		Dictionary_2_t3033730875 * L_6 = __this->get_planeAnchorMap_0();
		NullCheck(L_6);
		Dictionary_2_Clear_m1758728446(L_6, /*hidden argument*/Dictionary_2_Clear_m1758728446_RuntimeMethod_var);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::GetCurrentPlaneAnchors()
extern "C"  List_1_t1889681993 * UnityARAnchorManager_GetCurrentPlaneAnchors_m3710250286 (UnityARAnchorManager_t3624459219 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_GetCurrentPlaneAnchors_m3710250286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3033730875 * L_0 = __this->get_planeAnchorMap_0();
		NullCheck(L_0);
		ValueCollection_t70314757 * L_1 = Dictionary_2_get_Values_m3019150958(L_0, /*hidden argument*/Dictionary_2_get_Values_m3019150958_RuntimeMethod_var);
		List_1_t1889681993 * L_2 = Enumerable_ToList_TisARPlaneAnchorGameObject_t3146329710_m589330593(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisARPlaneAnchorGameObject_t3146329710_m589330593_RuntimeMethod_var);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.UnityARCamera
extern "C" void UnityARCamera_t2376600594_marshal_pinvoke(const UnityARCamera_t2376600594& unmarshaled, UnityARCamera_t2376600594_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___pointCloudData_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'pointCloudData' of type 'UnityARCamera'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___pointCloudData_4Exception);
}
extern "C" void UnityARCamera_t2376600594_marshal_pinvoke_back(const UnityARCamera_t2376600594_marshaled_pinvoke& marshaled, UnityARCamera_t2376600594& unmarshaled)
{
	Il2CppCodeGenException* ___pointCloudData_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'pointCloudData' of type 'UnityARCamera'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___pointCloudData_4Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.UnityARCamera
extern "C" void UnityARCamera_t2376600594_marshal_pinvoke_cleanup(UnityARCamera_t2376600594_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.UnityARCamera
extern "C" void UnityARCamera_t2376600594_marshal_com(const UnityARCamera_t2376600594& unmarshaled, UnityARCamera_t2376600594_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___pointCloudData_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'pointCloudData' of type 'UnityARCamera'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___pointCloudData_4Exception);
}
extern "C" void UnityARCamera_t2376600594_marshal_com_back(const UnityARCamera_t2376600594_marshaled_com& marshaled, UnityARCamera_t2376600594& unmarshaled)
{
	Il2CppCodeGenException* ___pointCloudData_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'pointCloudData' of type 'UnityARCamera'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___pointCloudData_4Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.UnityARCamera
extern "C" void UnityARCamera_t2376600594_marshal_com_cleanup(UnityARCamera_t2376600594_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.iOS.UnityARGeneratePlane::.ctor()
extern "C"  void UnityARGeneratePlane__ctor_m399314513 (UnityARGeneratePlane_t3109641619 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARGeneratePlane::Start()
extern "C"  void UnityARGeneratePlane_Start_m1171670317 (UnityARGeneratePlane_t3109641619 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARGeneratePlane_Start_m1171670317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityARAnchorManager_t3624459219 * L_0 = (UnityARAnchorManager_t3624459219 *)il2cpp_codegen_object_new(UnityARAnchorManager_t3624459219_il2cpp_TypeInfo_var);
		UnityARAnchorManager__ctor_m1537079611(L_0, /*hidden argument*/NULL);
		__this->set_unityARAnchorManager_3(L_0);
		GameObject_t3649338848 * L_1 = __this->get_planePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t2651983316_il2cpp_TypeInfo_var);
		UnityARUtility_InitializePlanePrefab_m2229940298(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARGeneratePlane::OnDestroy()
extern "C"  void UnityARGeneratePlane_OnDestroy_m3825353551 (UnityARGeneratePlane_t3109641619 * __this, const RuntimeMethod* method)
{
	{
		UnityARAnchorManager_t3624459219 * L_0 = __this->get_unityARAnchorManager_3();
		NullCheck(L_0);
		UnityARAnchorManager_Destroy_m3970721311(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARGeneratePlane::OnGUI()
extern "C"  void UnityARGeneratePlane_OnGUI_m2684108629 (UnityARGeneratePlane_t3109641619 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARGeneratePlane_OnGUI_m2684108629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1889681993 * V_0 = NULL;
	{
		UnityARAnchorManager_t3624459219 * L_0 = __this->get_unityARAnchorManager_3();
		NullCheck(L_0);
		List_1_t1889681993 * L_1 = UnityARAnchorManager_GetCurrentPlaneAnchors_m3710250286(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t1889681993 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m1057070554(L_2, /*hidden argument*/List_1_get_Count_m1057070554_RuntimeMethod_var);
		if ((((int32_t)L_3) < ((int32_t)1)))
		{
			goto IL_0018;
		}
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARHitTestExample::.ctor()
extern "C"  void UnityARHitTestExample__ctor_m3597268695 (UnityARHitTestExample_t2411672648 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.XR.iOS.UnityARHitTestExample::HitTestWithResultType(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  bool UnityARHitTestExample_HitTestWithResultType_m2836964868 (UnityARHitTestExample_t2411672648 * __this, ARPoint_t1649490841  ___point0, int64_t ___resultTypes1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARHitTestExample_HitTestWithResultType_m2836964868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t547075962 * V_0 = NULL;
	ARHitTestResult_t1803723679  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t1895934899  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t596762001  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t596762001  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t596762001  V_5;
	memset(&V_5, 0, sizeof(V_5));
	bool V_6 = false;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARPoint_t1649490841  L_1 = ___point0;
		int64_t L_2 = ___resultTypes1;
		NullCheck(L_0);
		List_1_t547075962 * L_3 = UnityARSessionNativeInterface_HitTest_m1126351971(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		List_1_t547075962 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m655066853(L_4, /*hidden argument*/List_1_get_Count_m655066853_RuntimeMethod_var);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_00e5;
		}
	}
	{
		List_1_t547075962 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t1895934899  L_7 = List_1_GetEnumerator_m2437281809(L_6, /*hidden argument*/List_1_GetEnumerator_m2437281809_RuntimeMethod_var);
		V_2 = L_7;
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c6;
		}

IL_0025:
		{
			ARHitTestResult_t1803723679  L_8 = Enumerator_get_Current_m3632740371((&V_2), /*hidden argument*/Enumerator_get_Current_m3632740371_RuntimeMethod_var);
			V_1 = L_8;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
			Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral1670108847, /*hidden argument*/NULL);
			Transform_t3933397867 * L_9 = __this->get_m_HitTransform_2();
			Matrix4x4_t1288378485  L_10 = (&V_1)->get_worldTransform_3();
			Vector3_t596762001  L_11 = UnityARMatrixOps_GetPosition_m3553979848(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			NullCheck(L_9);
			Transform_set_position_m2789033506(L_9, L_11, /*hidden argument*/NULL);
			Transform_t3933397867 * L_12 = __this->get_m_HitTransform_2();
			Matrix4x4_t1288378485  L_13 = (&V_1)->get_worldTransform_3();
			Quaternion_t3165733013  L_14 = UnityARMatrixOps_GetRotation_m3890376831(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			NullCheck(L_12);
			Transform_set_rotation_m1288069348(L_12, L_14, /*hidden argument*/NULL);
			Transform_t3933397867 * L_15 = __this->get_m_HitTransform_2();
			NullCheck(L_15);
			Vector3_t596762001  L_16 = Transform_get_position_m3654347341(L_15, /*hidden argument*/NULL);
			V_3 = L_16;
			float L_17 = (&V_3)->get_x_1();
			float L_18 = L_17;
			RuntimeObject * L_19 = Box(Single_t2847614712_il2cpp_TypeInfo_var, &L_18);
			Transform_t3933397867 * L_20 = __this->get_m_HitTransform_2();
			NullCheck(L_20);
			Vector3_t596762001  L_21 = Transform_get_position_m3654347341(L_20, /*hidden argument*/NULL);
			V_4 = L_21;
			float L_22 = (&V_4)->get_y_2();
			float L_23 = L_22;
			RuntimeObject * L_24 = Box(Single_t2847614712_il2cpp_TypeInfo_var, &L_23);
			Transform_t3933397867 * L_25 = __this->get_m_HitTransform_2();
			NullCheck(L_25);
			Vector3_t596762001  L_26 = Transform_get_position_m3654347341(L_25, /*hidden argument*/NULL);
			V_5 = L_26;
			float L_27 = (&V_5)->get_z_3();
			float L_28 = L_27;
			RuntimeObject * L_29 = Box(Single_t2847614712_il2cpp_TypeInfo_var, &L_28);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_30 = String_Format_m223808606(NULL /*static, unused*/, _stringLiteral297872476, L_19, L_24, L_29, /*hidden argument*/NULL);
			Debug_Log_m2782172005(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			V_6 = (bool)1;
			IL2CPP_LEAVE(0xE7, FINALLY_00d7);
		}

IL_00c6:
		{
			bool L_31 = Enumerator_MoveNext_m1007619642((&V_2), /*hidden argument*/Enumerator_MoveNext_m1007619642_RuntimeMethod_var);
			if (L_31)
			{
				goto IL_0025;
			}
		}

IL_00d2:
		{
			IL2CPP_LEAVE(0xE5, FINALLY_00d7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_00d7;
	}

FINALLY_00d7:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3562983614((&V_2), /*hidden argument*/Enumerator_Dispose_m3562983614_RuntimeMethod_var);
		IL2CPP_END_FINALLY(215)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(215)
	{
		IL2CPP_JUMP_TBL(0xE7, IL_00e7)
		IL2CPP_JUMP_TBL(0xE5, IL_00e5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_00e5:
	{
		return (bool)0;
	}

IL_00e7:
	{
		bool L_32 = V_6;
		return L_32;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARHitTestExample::Update()
extern "C"  void UnityARHitTestExample_Update_m2631636100 (UnityARHitTestExample_t2411672648 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARHitTestExample_Update_m2631636100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t3132414106  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t596762001  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ARPoint_t1649490841  V_2;
	memset(&V_2, 0, sizeof(V_2));
	ARPoint_t1649490841  V_3;
	memset(&V_3, 0, sizeof(V_3));
	ARHitTestResultTypeU5BU5D_t1137434915* V_4 = NULL;
	int64_t V_5 = 0;
	ARHitTestResultTypeU5BU5D_t1137434915* V_6 = NULL;
	int32_t V_7 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1121709983_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m4114401638(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_00b4;
		}
	}
	{
		Transform_t3933397867 * L_1 = __this->get_m_HitTransform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1121709983_il2cpp_TypeInfo_var);
		Touch_t3132414106  L_3 = Input_GetTouch_m2909242247(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = Touch_get_phase_m993859056((&V_0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_00b4;
		}
	}
	{
		Camera_t226495598 * L_5 = Camera_get_main_m2671983186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t59524482  L_6 = Touch_get_position_m2291334870((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_7 = Vector2_op_Implicit_m2201252505(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t596762001  L_8 = Camera_ScreenToViewportPoint_m474730427(L_5, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Initobj (ARPoint_t1649490841_il2cpp_TypeInfo_var, (&V_3));
		float L_9 = (&V_1)->get_x_1();
		(&V_3)->set_x_0((((double)((double)L_9))));
		float L_10 = (&V_1)->get_y_2();
		(&V_3)->set_y_1((((double)((double)L_10))));
		ARPoint_t1649490841  L_11 = V_3;
		V_2 = L_11;
		ARHitTestResultTypeU5BU5D_t1137434915* L_12 = ((ARHitTestResultTypeU5BU5D_t1137434915*)SZArrayNew(ARHitTestResultTypeU5BU5D_t1137434915_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m552509826(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_12, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1316871563____U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0_FieldInfo_var), /*hidden argument*/NULL);
		V_4 = L_12;
		ARHitTestResultTypeU5BU5D_t1137434915* L_13 = V_4;
		V_6 = L_13;
		V_7 = 0;
		goto IL_00a9;
	}

IL_008d:
	{
		ARHitTestResultTypeU5BU5D_t1137434915* L_14 = V_6;
		int32_t L_15 = V_7;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int64_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_5 = L_17;
		ARPoint_t1649490841  L_18 = V_2;
		int64_t L_19 = V_5;
		bool L_20 = UnityARHitTestExample_HitTestWithResultType_m2836964868(__this, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00a3;
		}
	}
	{
		return;
	}

IL_00a3:
	{
		int32_t L_21 = V_7;
		V_7 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00a9:
	{
		int32_t L_22 = V_7;
		ARHitTestResultTypeU5BU5D_t1137434915* L_23 = V_6;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
		{
			goto IL_008d;
		}
	}

IL_00b4:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.UnityARHitTestResult
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke(const UnityARHitTestResult_t1002809393& unmarshaled, UnityARHitTestResult_t1002809393_marshaled_pinvoke& marshaled)
{
	marshaled.___type_0 = unmarshaled.get_type_0();
	marshaled.___distance_1 = unmarshaled.get_distance_1();
	marshaled.___localTransform_2 = unmarshaled.get_localTransform_2();
	marshaled.___worldTransform_3 = unmarshaled.get_worldTransform_3();
	marshaled.___anchor_4 = reinterpret_cast<intptr_t>((unmarshaled.get_anchor_4()).get_m_value_0());
	marshaled.___isValid_5 = static_cast<int32_t>(unmarshaled.get_isValid_5());
}
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke_back(const UnityARHitTestResult_t1002809393_marshaled_pinvoke& marshaled, UnityARHitTestResult_t1002809393& unmarshaled)
{
	int64_t unmarshaled_type_temp_0 = 0;
	unmarshaled_type_temp_0 = marshaled.___type_0;
	unmarshaled.set_type_0(unmarshaled_type_temp_0);
	double unmarshaled_distance_temp_1 = 0.0;
	unmarshaled_distance_temp_1 = marshaled.___distance_1;
	unmarshaled.set_distance_1(unmarshaled_distance_temp_1);
	Matrix4x4_t1288378485  unmarshaled_localTransform_temp_2;
	memset(&unmarshaled_localTransform_temp_2, 0, sizeof(unmarshaled_localTransform_temp_2));
	unmarshaled_localTransform_temp_2 = marshaled.___localTransform_2;
	unmarshaled.set_localTransform_2(unmarshaled_localTransform_temp_2);
	Matrix4x4_t1288378485  unmarshaled_worldTransform_temp_3;
	memset(&unmarshaled_worldTransform_temp_3, 0, sizeof(unmarshaled_worldTransform_temp_3));
	unmarshaled_worldTransform_temp_3 = marshaled.___worldTransform_3;
	unmarshaled.set_worldTransform_3(unmarshaled_worldTransform_temp_3);
	IntPtr_t unmarshaled_anchor_temp_4;
	memset(&unmarshaled_anchor_temp_4, 0, sizeof(unmarshaled_anchor_temp_4));
	IntPtr_t unmarshaled_anchor_temp_4_temp;
	unmarshaled_anchor_temp_4_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___anchor_4)));
	unmarshaled_anchor_temp_4 = unmarshaled_anchor_temp_4_temp;
	unmarshaled.set_anchor_4(unmarshaled_anchor_temp_4);
	bool unmarshaled_isValid_temp_5 = false;
	unmarshaled_isValid_temp_5 = static_cast<bool>(marshaled.___isValid_5);
	unmarshaled.set_isValid_5(unmarshaled_isValid_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.UnityARHitTestResult
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke_cleanup(UnityARHitTestResult_t1002809393_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.UnityARHitTestResult
extern "C" void UnityARHitTestResult_t1002809393_marshal_com(const UnityARHitTestResult_t1002809393& unmarshaled, UnityARHitTestResult_t1002809393_marshaled_com& marshaled)
{
	marshaled.___type_0 = unmarshaled.get_type_0();
	marshaled.___distance_1 = unmarshaled.get_distance_1();
	marshaled.___localTransform_2 = unmarshaled.get_localTransform_2();
	marshaled.___worldTransform_3 = unmarshaled.get_worldTransform_3();
	marshaled.___anchor_4 = reinterpret_cast<intptr_t>((unmarshaled.get_anchor_4()).get_m_value_0());
	marshaled.___isValid_5 = static_cast<int32_t>(unmarshaled.get_isValid_5());
}
extern "C" void UnityARHitTestResult_t1002809393_marshal_com_back(const UnityARHitTestResult_t1002809393_marshaled_com& marshaled, UnityARHitTestResult_t1002809393& unmarshaled)
{
	int64_t unmarshaled_type_temp_0 = 0;
	unmarshaled_type_temp_0 = marshaled.___type_0;
	unmarshaled.set_type_0(unmarshaled_type_temp_0);
	double unmarshaled_distance_temp_1 = 0.0;
	unmarshaled_distance_temp_1 = marshaled.___distance_1;
	unmarshaled.set_distance_1(unmarshaled_distance_temp_1);
	Matrix4x4_t1288378485  unmarshaled_localTransform_temp_2;
	memset(&unmarshaled_localTransform_temp_2, 0, sizeof(unmarshaled_localTransform_temp_2));
	unmarshaled_localTransform_temp_2 = marshaled.___localTransform_2;
	unmarshaled.set_localTransform_2(unmarshaled_localTransform_temp_2);
	Matrix4x4_t1288378485  unmarshaled_worldTransform_temp_3;
	memset(&unmarshaled_worldTransform_temp_3, 0, sizeof(unmarshaled_worldTransform_temp_3));
	unmarshaled_worldTransform_temp_3 = marshaled.___worldTransform_3;
	unmarshaled.set_worldTransform_3(unmarshaled_worldTransform_temp_3);
	IntPtr_t unmarshaled_anchor_temp_4;
	memset(&unmarshaled_anchor_temp_4, 0, sizeof(unmarshaled_anchor_temp_4));
	IntPtr_t unmarshaled_anchor_temp_4_temp;
	unmarshaled_anchor_temp_4_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___anchor_4)));
	unmarshaled_anchor_temp_4 = unmarshaled_anchor_temp_4_temp;
	unmarshaled.set_anchor_4(unmarshaled_anchor_temp_4);
	bool unmarshaled_isValid_temp_5 = false;
	unmarshaled_isValid_temp_5 = static_cast<bool>(marshaled.___isValid_5);
	unmarshaled.set_isValid_5(unmarshaled_isValid_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.UnityARHitTestResult
extern "C" void UnityARHitTestResult_t1002809393_marshal_com_cleanup(UnityARHitTestResult_t1002809393_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.iOS.UnityARKitControl::.ctor()
extern "C"  void UnityARKitControl__ctor_m4265604983 (UnityARKitControl_t4114739031 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARKitControl__ctor_m4265604983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_runOptions_2(((UnityARSessionRunOptionU5BU5D_t3872385379*)SZArrayNew(UnityARSessionRunOptionU5BU5D_t3872385379_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_alignmentOptions_3(((UnityARAlignmentU5BU5D_t2020264667*)SZArrayNew(UnityARAlignmentU5BU5D_t2020264667_il2cpp_TypeInfo_var, (uint32_t)3)));
		__this->set_planeOptions_4(((UnityARPlaneDetectionU5BU5D_t4098669274*)SZArrayNew(UnityARPlaneDetectionU5BU5D_t4098669274_il2cpp_TypeInfo_var, (uint32_t)4)));
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARKitControl::Start()
extern "C"  void UnityARKitControl_Start_m741677045 (UnityARKitControl_t4114739031 * __this, const RuntimeMethod* method)
{
	{
		UnityARSessionRunOptionU5BU5D_t3872385379* L_0 = __this->get_runOptions_2();
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)3);
		UnityARSessionRunOptionU5BU5D_t3872385379* L_1 = __this->get_runOptions_2();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)1);
		UnityARSessionRunOptionU5BU5D_t3872385379* L_2 = __this->get_runOptions_2();
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)2);
		UnityARSessionRunOptionU5BU5D_t3872385379* L_3 = __this->get_runOptions_2();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)0);
		UnityARAlignmentU5BU5D_t2020264667* L_4 = __this->get_alignmentOptions_3();
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)2);
		UnityARAlignmentU5BU5D_t2020264667* L_5 = __this->get_alignmentOptions_3();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)0);
		UnityARAlignmentU5BU5D_t2020264667* L_6 = __this->get_alignmentOptions_3();
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)1);
		UnityARPlaneDetectionU5BU5D_t4098669274* L_7 = __this->get_planeOptions_4();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)1);
		UnityARPlaneDetectionU5BU5D_t4098669274* L_8 = __this->get_planeOptions_4();
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)0);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARKitControl::Update()
extern "C"  void UnityARKitControl_Update_m867453362 (UnityARKitControl_t4114739031 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARKitControl::OnGUI()
extern "C"  void UnityARKitControl_OnGUI_m2842321300 (UnityARKitControl_t4114739031 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARKitControl_OnGUI_m2842321300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARKitWorldTackingSessionConfiguration_t2186350340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ARKitSessionConfiguration_t3142208763  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* G_B13_0 = NULL;
	String_t* G_B20_0 = NULL;
	String_t* G_B29_0 = NULL;
	{
		Rect_t1992046353  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m1383055445((&L_0), (100.0f), (100.0f), (200.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m1880561377(NULL /*static, unused*/, L_0, _stringLiteral1616253138, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_2 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UnityARSessionNativeInterface_Pause_m224144355(L_2, /*hidden argument*/NULL);
	}

IL_0032:
	{
		Rect_t1992046353  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1383055445((&L_3), (300.0f), (100.0f), (200.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_4 = GUI_Button_m1880561377(NULL /*static, unused*/, L_3, _stringLiteral2126568017, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0095;
		}
	}
	{
		UnityARAlignmentU5BU5D_t2020264667* L_5 = __this->get_alignmentOptions_3();
		int32_t L_6 = __this->get_currentAlignmentIndex_6();
		NullCheck(L_5);
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		UnityARPlaneDetectionU5BU5D_t4098669274* L_9 = __this->get_planeOptions_4();
		int32_t L_10 = __this->get_currentPlaneIndex_7();
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		ARKitWorldTackingSessionConfiguration__ctor_m2859336466((&V_0), L_8, L_12, (bool)0, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_13 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARKitWorldTackingSessionConfiguration_t2186350340  L_14 = V_0;
		UnityARSessionRunOptionU5BU5D_t3872385379* L_15 = __this->get_runOptions_2();
		int32_t L_16 = __this->get_currentOptionIndex_5();
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_13);
		UnityARSessionNativeInterface_RunWithConfigAndOptions_m3750156203(L_13, L_14, L_18, /*hidden argument*/NULL);
	}

IL_0095:
	{
		Rect_t1992046353  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Rect__ctor_m1383055445((&L_19), (100.0f), (300.0f), (200.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_20 = GUI_Button_m1880561377(NULL /*static, unused*/, L_19, _stringLiteral1876860631, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00de;
		}
	}
	{
		UnityARAlignmentU5BU5D_t2020264667* L_21 = __this->get_alignmentOptions_3();
		int32_t L_22 = __this->get_currentAlignmentIndex_6();
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		ARKitSessionConfiguration__ctor_m373048997((&V_1), L_24, (bool)1, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_25 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARKitSessionConfiguration_t3142208763  L_26 = V_1;
		NullCheck(L_25);
		UnityARSessionNativeInterface_RunWithConfig_m152596073(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00de:
	{
		int32_t L_27 = __this->get_currentOptionIndex_5();
		if (L_27)
		{
			goto IL_00f3;
		}
	}
	{
		G_B13_0 = _stringLiteral263420843;
		goto IL_0124;
	}

IL_00f3:
	{
		int32_t L_28 = __this->get_currentOptionIndex_5();
		if ((!(((uint32_t)L_28) == ((uint32_t)1))))
		{
			goto IL_0109;
		}
	}
	{
		G_B13_0 = _stringLiteral3336902858;
		goto IL_0124;
	}

IL_0109:
	{
		int32_t L_29 = __this->get_currentOptionIndex_5();
		if ((!(((uint32_t)L_29) == ((uint32_t)2))))
		{
			goto IL_011f;
		}
	}
	{
		G_B13_0 = _stringLiteral386416851;
		goto IL_0124;
	}

IL_011f:
	{
		G_B13_0 = _stringLiteral3645053941;
	}

IL_0124:
	{
		V_2 = G_B13_0;
		Rect_t1992046353  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Rect__ctor_m1383055445((&L_30), (100.0f), (200.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		String_t* L_31 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2023871361(NULL /*static, unused*/, _stringLiteral78005878, L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_33 = GUI_Button_m1880561377(NULL /*static, unused*/, L_30, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0163;
		}
	}
	{
		int32_t L_34 = __this->get_currentOptionIndex_5();
		__this->set_currentOptionIndex_5(((int32_t)((int32_t)((int32_t)((int32_t)L_34+(int32_t)1))%(int32_t)4)));
	}

IL_0163:
	{
		int32_t L_35 = __this->get_currentAlignmentIndex_6();
		if (L_35)
		{
			goto IL_0178;
		}
	}
	{
		G_B20_0 = _stringLiteral3550138209;
		goto IL_0193;
	}

IL_0178:
	{
		int32_t L_36 = __this->get_currentAlignmentIndex_6();
		if ((!(((uint32_t)L_36) == ((uint32_t)1))))
		{
			goto IL_018e;
		}
	}
	{
		G_B20_0 = _stringLiteral414407049;
		goto IL_0193;
	}

IL_018e:
	{
		G_B20_0 = _stringLiteral3907610049;
	}

IL_0193:
	{
		V_3 = G_B20_0;
		Rect_t1992046353  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Rect__ctor_m1383055445((&L_37), (300.0f), (200.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		String_t* L_38 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2023871361(NULL /*static, unused*/, _stringLiteral4085911591, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_40 = GUI_Button_m1880561377(NULL /*static, unused*/, L_37, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_01d2;
		}
	}
	{
		int32_t L_41 = __this->get_currentAlignmentIndex_6();
		__this->set_currentAlignmentIndex_6(((int32_t)((int32_t)((int32_t)((int32_t)L_41+(int32_t)1))%(int32_t)3)));
	}

IL_01d2:
	{
		int32_t L_42 = __this->get_currentPlaneIndex_7();
		if (L_42)
		{
			goto IL_01e7;
		}
	}
	{
		G_B29_0 = _stringLiteral1573470155;
		goto IL_0218;
	}

IL_01e7:
	{
		int32_t L_43 = __this->get_currentPlaneIndex_7();
		if ((!(((uint32_t)L_43) == ((uint32_t)1))))
		{
			goto IL_01fd;
		}
	}
	{
		G_B29_0 = _stringLiteral1272959178;
		goto IL_0218;
	}

IL_01fd:
	{
		int32_t L_44 = __this->get_currentPlaneIndex_7();
		if ((!(((uint32_t)L_44) == ((uint32_t)2))))
		{
			goto IL_0213;
		}
	}
	{
		G_B29_0 = _stringLiteral893077012;
		goto IL_0218;
	}

IL_0213:
	{
		G_B29_0 = _stringLiteral3645053941;
	}

IL_0218:
	{
		V_4 = G_B29_0;
		Rect_t1992046353  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Rect__ctor_m1383055445((&L_45), (500.0f), (200.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		String_t* L_46 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m2023871361(NULL /*static, unused*/, _stringLiteral3186302199, L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_48 = GUI_Button_m1880561377(NULL /*static, unused*/, L_45, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0259;
		}
	}
	{
		int32_t L_49 = __this->get_currentPlaneIndex_7();
		__this->set_currentPlaneIndex_7(((int32_t)((int32_t)((int32_t)((int32_t)L_49+(int32_t)1))%(int32_t)4)));
	}

IL_0259:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARMatrixOps::.ctor()
extern "C"  void UnityARMatrixOps__ctor_m3227036820 (UnityARMatrixOps_t180517933 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.XR.iOS.UnityARMatrixOps::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t596762001  UnityARMatrixOps_GetPosition_m3553979848 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1288378485  ___matrix0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARMatrixOps_GetPosition_m3553979848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t596762001  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector4_t1376926224  L_0 = Matrix4x4_GetColumn_m2106332924((&___matrix0), 3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t1376926224_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_1 = Vector4_op_Implicit_m4265673868(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_3();
		(&V_0)->set_z_3(((-L_2)));
		Vector3_t596762001  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::GetRotation(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t3165733013  UnityARMatrixOps_GetRotation_m3890376831 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1288378485  ___matrix0, const RuntimeMethod* method)
{
	Quaternion_t3165733013  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Matrix4x4_t1288378485  L_0 = ___matrix0;
		Quaternion_t3165733013  L_1 = UnityARMatrixOps_QuaternionFromMatrix_m2802180225(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_2();
		(&V_0)->set_z_2(((-L_2)));
		float L_3 = (&V_0)->get_w_3();
		(&V_0)->set_w_3(((-L_3)));
		Quaternion_t3165733013  L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t3165733013  UnityARMatrixOps_QuaternionFromMatrix_m2802180225 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1288378485  ___m0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARMatrixOps_QuaternionFromMatrix_m2802180225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t3165733013  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Quaternion_t3165733013_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = Matrix4x4_get_Item_m3766957970((&___m0), 0, 0, /*hidden argument*/NULL);
		float L_1 = Matrix4x4_get_Item_m3766957970((&___m0), 1, 1, /*hidden argument*/NULL);
		float L_2 = Matrix4x4_get_Item_m3766957970((&___m0), 2, 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Max_m1799835110(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_0))+(float)L_1))+(float)L_2)), /*hidden argument*/NULL);
		float L_4 = sqrtf(L_3);
		(&V_0)->set_w_3(((float)((float)L_4/(float)(2.0f))));
		float L_5 = Matrix4x4_get_Item_m3766957970((&___m0), 0, 0, /*hidden argument*/NULL);
		float L_6 = Matrix4x4_get_Item_m3766957970((&___m0), 1, 1, /*hidden argument*/NULL);
		float L_7 = Matrix4x4_get_Item_m3766957970((&___m0), 2, 2, /*hidden argument*/NULL);
		float L_8 = Mathf_Max_m1799835110(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))-(float)L_6))-(float)L_7)), /*hidden argument*/NULL);
		float L_9 = sqrtf(L_8);
		(&V_0)->set_x_0(((float)((float)L_9/(float)(2.0f))));
		float L_10 = Matrix4x4_get_Item_m3766957970((&___m0), 0, 0, /*hidden argument*/NULL);
		float L_11 = Matrix4x4_get_Item_m3766957970((&___m0), 1, 1, /*hidden argument*/NULL);
		float L_12 = Matrix4x4_get_Item_m3766957970((&___m0), 2, 2, /*hidden argument*/NULL);
		float L_13 = Mathf_Max_m1799835110(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_10))+(float)L_11))-(float)L_12)), /*hidden argument*/NULL);
		float L_14 = sqrtf(L_13);
		(&V_0)->set_y_1(((float)((float)L_14/(float)(2.0f))));
		float L_15 = Matrix4x4_get_Item_m3766957970((&___m0), 0, 0, /*hidden argument*/NULL);
		float L_16 = Matrix4x4_get_Item_m3766957970((&___m0), 1, 1, /*hidden argument*/NULL);
		float L_17 = Matrix4x4_get_Item_m3766957970((&___m0), 2, 2, /*hidden argument*/NULL);
		float L_18 = Mathf_Max_m1799835110(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_15))-(float)L_16))+(float)L_17)), /*hidden argument*/NULL);
		float L_19 = sqrtf(L_18);
		(&V_0)->set_z_2(((float)((float)L_19/(float)(2.0f))));
		Quaternion_t3165733013 * L_20 = (&V_0);
		float L_21 = L_20->get_x_0();
		float L_22 = (&V_0)->get_x_0();
		float L_23 = Matrix4x4_get_Item_m3766957970((&___m0), 2, 1, /*hidden argument*/NULL);
		float L_24 = Matrix4x4_get_Item_m3766957970((&___m0), 1, 2, /*hidden argument*/NULL);
		float L_25 = Mathf_Sign_m1995337595(NULL /*static, unused*/, ((float)((float)L_22*(float)((float)((float)L_23-(float)L_24)))), /*hidden argument*/NULL);
		L_20->set_x_0(((float)((float)L_21*(float)L_25)));
		Quaternion_t3165733013 * L_26 = (&V_0);
		float L_27 = L_26->get_y_1();
		float L_28 = (&V_0)->get_y_1();
		float L_29 = Matrix4x4_get_Item_m3766957970((&___m0), 0, 2, /*hidden argument*/NULL);
		float L_30 = Matrix4x4_get_Item_m3766957970((&___m0), 2, 0, /*hidden argument*/NULL);
		float L_31 = Mathf_Sign_m1995337595(NULL /*static, unused*/, ((float)((float)L_28*(float)((float)((float)L_29-(float)L_30)))), /*hidden argument*/NULL);
		L_26->set_y_1(((float)((float)L_27*(float)L_31)));
		Quaternion_t3165733013 * L_32 = (&V_0);
		float L_33 = L_32->get_z_2();
		float L_34 = (&V_0)->get_z_2();
		float L_35 = Matrix4x4_get_Item_m3766957970((&___m0), 1, 0, /*hidden argument*/NULL);
		float L_36 = Matrix4x4_get_Item_m3766957970((&___m0), 0, 1, /*hidden argument*/NULL);
		float L_37 = Mathf_Sign_m1995337595(NULL /*static, unused*/, ((float)((float)L_34*(float)((float)((float)L_35-(float)L_36)))), /*hidden argument*/NULL);
		L_32->set_z_2(((float)((float)L_33*(float)L_37)));
		Quaternion_t3165733013  L_38 = V_0;
		return L_38;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__frame_update_m3249087823(internal_UnityARCamera_t1506237410  ___camera0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	UnityARSessionNativeInterface__frame_update_m3249087823(NULL, ___camera0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_added_m3990782978(UnityARAnchorData_t1511263172  ___anchor0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	UnityARSessionNativeInterface__anchor_added_m3990782978(NULL, ___anchor0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_updated_m4275459728(UnityARAnchorData_t1511263172  ___anchor0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	UnityARSessionNativeInterface__anchor_updated_m4275459728(NULL, ___anchor0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_removed_m4149770974(UnityARAnchorData_t1511263172  ___anchor0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	UnityARSessionNativeInterface__anchor_removed_m4149770974(NULL, ___anchor0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_session_failed_m1149369129(char* ___error0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___error0' to managed representation
	String_t* ____error0_unmarshaled = NULL;
	____error0_unmarshaled = il2cpp_codegen_marshal_string_result(___error0);

	// Managed method invocation
	UnityARSessionNativeInterface__ar_session_failed_m1149369129(NULL, ____error0_unmarshaled, NULL);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::.ctor()
extern "C"  void UnityARSessionNativeInterface__ctor_m2217614958 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__ctor_m2217614958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARSessionNativeInterface_t3869411053 * G_B2_0 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B1_0 = NULL;
	internal_ARFrameUpdate_t2726389622 * G_B4_0 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B4_1 = NULL;
	internal_ARFrameUpdate_t2726389622 * G_B3_0 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B3_1 = NULL;
	internal_ARAnchorAdded_t3791587037 * G_B6_0 = NULL;
	internal_ARFrameUpdate_t2726389622 * G_B6_1 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B6_2 = NULL;
	internal_ARAnchorAdded_t3791587037 * G_B5_0 = NULL;
	internal_ARFrameUpdate_t2726389622 * G_B5_1 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B5_2 = NULL;
	internal_ARAnchorUpdated_t2928809250 * G_B8_0 = NULL;
	internal_ARAnchorAdded_t3791587037 * G_B8_1 = NULL;
	internal_ARFrameUpdate_t2726389622 * G_B8_2 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B8_3 = NULL;
	internal_ARAnchorUpdated_t2928809250 * G_B7_0 = NULL;
	internal_ARAnchorAdded_t3791587037 * G_B7_1 = NULL;
	internal_ARFrameUpdate_t2726389622 * G_B7_2 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B7_3 = NULL;
	internal_ARAnchorRemoved_t409959511 * G_B10_0 = NULL;
	internal_ARAnchorUpdated_t2928809250 * G_B10_1 = NULL;
	internal_ARAnchorAdded_t3791587037 * G_B10_2 = NULL;
	internal_ARFrameUpdate_t2726389622 * G_B10_3 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B10_4 = NULL;
	internal_ARAnchorRemoved_t409959511 * G_B9_0 = NULL;
	internal_ARAnchorUpdated_t2928809250 * G_B9_1 = NULL;
	internal_ARAnchorAdded_t3791587037 * G_B9_2 = NULL;
	internal_ARFrameUpdate_t2726389622 * G_B9_3 = NULL;
	UnityARSessionNativeInterface_t3869411053 * G_B9_4 = NULL;
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		internal_ARFrameUpdate_t2726389622 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_8();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_001f;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__frame_update_m3249087823_RuntimeMethod_var);
		internal_ARFrameUpdate_t2726389622 * L_2 = (internal_ARFrameUpdate_t2726389622 *)il2cpp_codegen_object_new(internal_ARFrameUpdate_t2726389622_il2cpp_TypeInfo_var);
		internal_ARFrameUpdate__ctor_m2168096248(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_8(L_2);
		G_B2_0 = G_B1_0;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		internal_ARFrameUpdate_t2726389622 * L_3 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_8();
		internal_ARAnchorAdded_t3791587037 * L_4 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_9();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		if (L_4)
		{
			G_B4_0 = L_3;
			G_B4_1 = G_B2_0;
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__anchor_added_m3990782978_RuntimeMethod_var);
		internal_ARAnchorAdded_t3791587037 * L_6 = (internal_ARAnchorAdded_t3791587037 *)il2cpp_codegen_object_new(internal_ARAnchorAdded_t3791587037_il2cpp_TypeInfo_var);
		internal_ARAnchorAdded__ctor_m3202793875(L_6, NULL, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_9(L_6);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		internal_ARAnchorAdded_t3791587037 * L_7 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_9();
		internal_ARAnchorUpdated_t2928809250 * L_8 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_10();
		G_B5_0 = L_7;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		if (L_8)
		{
			G_B6_0 = L_7;
			G_B6_1 = G_B4_0;
			G_B6_2 = G_B4_1;
			goto IL_0059;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__anchor_updated_m4275459728_RuntimeMethod_var);
		internal_ARAnchorUpdated_t2928809250 * L_10 = (internal_ARAnchorUpdated_t2928809250 *)il2cpp_codegen_object_new(internal_ARAnchorUpdated_t2928809250_il2cpp_TypeInfo_var);
		internal_ARAnchorUpdated__ctor_m1845092063(L_10, NULL, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache2_10(L_10);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		internal_ARAnchorUpdated_t2928809250 * L_11 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_10();
		internal_ARAnchorRemoved_t409959511 * L_12 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache3_11();
		G_B7_0 = L_11;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
		if (L_12)
		{
			G_B8_0 = L_11;
			G_B8_1 = G_B6_0;
			G_B8_2 = G_B6_1;
			G_B8_3 = G_B6_2;
			goto IL_0076;
		}
	}
	{
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__anchor_removed_m4149770974_RuntimeMethod_var);
		internal_ARAnchorRemoved_t409959511 * L_14 = (internal_ARAnchorRemoved_t409959511 *)il2cpp_codegen_object_new(internal_ARAnchorRemoved_t409959511_il2cpp_TypeInfo_var);
		internal_ARAnchorRemoved__ctor_m2196994448(L_14, NULL, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache3_11(L_14);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
		G_B8_2 = G_B7_2;
		G_B8_3 = G_B7_3;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		internal_ARAnchorRemoved_t409959511 * L_15 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache3_11();
		ARSessionFailed_t3599980200 * L_16 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache4_12();
		G_B9_0 = L_15;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
		G_B9_4 = G_B8_3;
		if (L_16)
		{
			G_B10_0 = L_15;
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			G_B10_3 = G_B8_2;
			G_B10_4 = G_B8_3;
			goto IL_0093;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__ar_session_failed_m1149369129_RuntimeMethod_var);
		ARSessionFailed_t3599980200 * L_18 = (ARSessionFailed_t3599980200 *)il2cpp_codegen_object_new(ARSessionFailed_t3599980200_il2cpp_TypeInfo_var);
		ARSessionFailed__ctor_m2477073150(L_18, NULL, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache4_12(L_18);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
	}

IL_0093:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARSessionFailed_t3599980200 * L_19 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache4_12();
		IntPtr_t L_20 = UnityARSessionNativeInterface_unity_CreateNativeARSession_m2427596710(NULL /*static, unused*/, G_B10_3, G_B10_2, G_B10_1, G_B10_0, L_19, /*hidden argument*/NULL);
		NullCheck(G_B10_4);
		G_B10_4->set_m_NativeARSession_5(L_20);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1467721625 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t3333962149 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1467721625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARFrameUpdate_t3333962149 * V_0 = NULL;
	ARFrameUpdate_t3333962149 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARFrameUpdate_t3333962149 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARFrameUpdatedEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		ARFrameUpdate_t3333962149 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARFrameUpdate_t3333962149 * L_2 = V_1;
		ARFrameUpdate_t3333962149 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Combine_m2396730294(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARFrameUpdate_t3333962149 * L_5 = V_0;
		ARFrameUpdate_t3333962149 * L_6 = InterlockedCompareExchangeImpl<ARFrameUpdate_t3333962149 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARFrameUpdatedEvent_0()), ((ARFrameUpdate_t3333962149 *)CastclassSealed((RuntimeObject*)L_4, ARFrameUpdate_t3333962149_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARFrameUpdate_t3333962149 * L_7 = V_0;
		ARFrameUpdate_t3333962149 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARFrameUpdate_t3333962149 *)L_7) == ((RuntimeObject*)(ARFrameUpdate_t3333962149 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m3076006849 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t3333962149 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m3076006849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARFrameUpdate_t3333962149 * V_0 = NULL;
	ARFrameUpdate_t3333962149 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARFrameUpdate_t3333962149 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARFrameUpdatedEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		ARFrameUpdate_t3333962149 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARFrameUpdate_t3333962149 * L_2 = V_1;
		ARFrameUpdate_t3333962149 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Remove_m1307738938(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARFrameUpdate_t3333962149 * L_5 = V_0;
		ARFrameUpdate_t3333962149 * L_6 = InterlockedCompareExchangeImpl<ARFrameUpdate_t3333962149 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARFrameUpdatedEvent_0()), ((ARFrameUpdate_t3333962149 *)CastclassSealed((RuntimeObject*)L_4, ARFrameUpdate_t3333962149_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARFrameUpdate_t3333962149 * L_7 = V_0;
		ARFrameUpdate_t3333962149 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARFrameUpdate_t3333962149 *)L_7) == ((RuntimeObject*)(ARFrameUpdate_t3333962149 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorAddedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m3219973195 (RuntimeObject * __this /* static, unused */, ARAnchorAdded_t1580091102 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m3219973195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorAdded_t1580091102 * V_0 = NULL;
	ARAnchorAdded_t1580091102 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorAdded_t1580091102 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorAddedEvent_1();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorAdded_t1580091102 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorAdded_t1580091102 * L_2 = V_1;
		ARAnchorAdded_t1580091102 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Combine_m2396730294(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorAdded_t1580091102 * L_5 = V_0;
		ARAnchorAdded_t1580091102 * L_6 = InterlockedCompareExchangeImpl<ARAnchorAdded_t1580091102 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARAnchorAddedEvent_1()), ((ARAnchorAdded_t1580091102 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorAdded_t1580091102_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorAdded_t1580091102 * L_7 = V_0;
		ARAnchorAdded_t1580091102 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorAdded_t1580091102 *)L_7) == ((RuntimeObject*)(ARAnchorAdded_t1580091102 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARAnchorAddedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded)
extern "C"  void UnityARSessionNativeInterface_remove_ARAnchorAddedEvent_m1017817494 (RuntimeObject * __this /* static, unused */, ARAnchorAdded_t1580091102 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARAnchorAddedEvent_m1017817494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorAdded_t1580091102 * V_0 = NULL;
	ARAnchorAdded_t1580091102 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorAdded_t1580091102 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorAddedEvent_1();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorAdded_t1580091102 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorAdded_t1580091102 * L_2 = V_1;
		ARAnchorAdded_t1580091102 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Remove_m1307738938(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorAdded_t1580091102 * L_5 = V_0;
		ARAnchorAdded_t1580091102 * L_6 = InterlockedCompareExchangeImpl<ARAnchorAdded_t1580091102 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARAnchorAddedEvent_1()), ((ARAnchorAdded_t1580091102 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorAdded_t1580091102_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorAdded_t1580091102 * L_7 = V_0;
		ARAnchorAdded_t1580091102 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorAdded_t1580091102 *)L_7) == ((RuntimeObject*)(ARAnchorAdded_t1580091102 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m994455513 (RuntimeObject * __this /* static, unused */, ARAnchorUpdated_t1492128022 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m994455513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorUpdated_t1492128022 * V_0 = NULL;
	ARAnchorUpdated_t1492128022 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t1492128022 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorUpdatedEvent_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorUpdated_t1492128022 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t1492128022 * L_2 = V_1;
		ARAnchorUpdated_t1492128022 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Combine_m2396730294(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorUpdated_t1492128022 * L_5 = V_0;
		ARAnchorUpdated_t1492128022 * L_6 = InterlockedCompareExchangeImpl<ARAnchorUpdated_t1492128022 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARAnchorUpdatedEvent_2()), ((ARAnchorUpdated_t1492128022 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorUpdated_t1492128022_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorUpdated_t1492128022 * L_7 = V_0;
		ARAnchorUpdated_t1492128022 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorUpdated_t1492128022 *)L_7) == ((RuntimeObject*)(ARAnchorUpdated_t1492128022 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARAnchorUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated)
extern "C"  void UnityARSessionNativeInterface_remove_ARAnchorUpdatedEvent_m2689967175 (RuntimeObject * __this /* static, unused */, ARAnchorUpdated_t1492128022 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARAnchorUpdatedEvent_m2689967175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorUpdated_t1492128022 * V_0 = NULL;
	ARAnchorUpdated_t1492128022 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t1492128022 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorUpdatedEvent_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorUpdated_t1492128022 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t1492128022 * L_2 = V_1;
		ARAnchorUpdated_t1492128022 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Remove_m1307738938(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorUpdated_t1492128022 * L_5 = V_0;
		ARAnchorUpdated_t1492128022 * L_6 = InterlockedCompareExchangeImpl<ARAnchorUpdated_t1492128022 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARAnchorUpdatedEvent_2()), ((ARAnchorUpdated_t1492128022 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorUpdated_t1492128022_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorUpdated_t1492128022 * L_7 = V_0;
		ARAnchorUpdated_t1492128022 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorUpdated_t1492128022 *)L_7) == ((RuntimeObject*)(ARAnchorUpdated_t1492128022 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorRemovedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m1279350122 (RuntimeObject * __this /* static, unused */, ARAnchorRemoved_t3663543047 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m1279350122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorRemoved_t3663543047 * V_0 = NULL;
	ARAnchorRemoved_t3663543047 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t3663543047 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorRemovedEvent_3();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorRemoved_t3663543047 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t3663543047 * L_2 = V_1;
		ARAnchorRemoved_t3663543047 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Combine_m2396730294(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorRemoved_t3663543047 * L_5 = V_0;
		ARAnchorRemoved_t3663543047 * L_6 = InterlockedCompareExchangeImpl<ARAnchorRemoved_t3663543047 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARAnchorRemovedEvent_3()), ((ARAnchorRemoved_t3663543047 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorRemoved_t3663543047_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorRemoved_t3663543047 * L_7 = V_0;
		ARAnchorRemoved_t3663543047 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorRemoved_t3663543047 *)L_7) == ((RuntimeObject*)(ARAnchorRemoved_t3663543047 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARAnchorRemovedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved)
extern "C"  void UnityARSessionNativeInterface_remove_ARAnchorRemovedEvent_m2859602900 (RuntimeObject * __this /* static, unused */, ARAnchorRemoved_t3663543047 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARAnchorRemovedEvent_m2859602900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorRemoved_t3663543047 * V_0 = NULL;
	ARAnchorRemoved_t3663543047 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t3663543047 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorRemovedEvent_3();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorRemoved_t3663543047 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t3663543047 * L_2 = V_1;
		ARAnchorRemoved_t3663543047 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Remove_m1307738938(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorRemoved_t3663543047 * L_5 = V_0;
		ARAnchorRemoved_t3663543047 * L_6 = InterlockedCompareExchangeImpl<ARAnchorRemoved_t3663543047 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARAnchorRemovedEvent_3()), ((ARAnchorRemoved_t3663543047 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorRemoved_t3663543047_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorRemoved_t3663543047 * L_7 = V_0;
		ARAnchorRemoved_t3663543047 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorRemoved_t3663543047 *)L_7) == ((RuntimeObject*)(ARAnchorRemoved_t3663543047 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARSessionFailedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed)
extern "C"  void UnityARSessionNativeInterface_add_ARSessionFailedEvent_m1051638243 (RuntimeObject * __this /* static, unused */, ARSessionFailed_t3599980200 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARSessionFailedEvent_m1051638243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARSessionFailed_t3599980200 * V_0 = NULL;
	ARSessionFailed_t3599980200 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARSessionFailed_t3599980200 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARSessionFailedEvent_4();
		V_0 = L_0;
	}

IL_0006:
	{
		ARSessionFailed_t3599980200 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARSessionFailed_t3599980200 * L_2 = V_1;
		ARSessionFailed_t3599980200 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Combine_m2396730294(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARSessionFailed_t3599980200 * L_5 = V_0;
		ARSessionFailed_t3599980200 * L_6 = InterlockedCompareExchangeImpl<ARSessionFailed_t3599980200 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARSessionFailedEvent_4()), ((ARSessionFailed_t3599980200 *)CastclassSealed((RuntimeObject*)L_4, ARSessionFailed_t3599980200_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARSessionFailed_t3599980200 * L_7 = V_0;
		ARSessionFailed_t3599980200 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARSessionFailed_t3599980200 *)L_7) == ((RuntimeObject*)(ARSessionFailed_t3599980200 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARSessionFailedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed)
extern "C"  void UnityARSessionNativeInterface_remove_ARSessionFailedEvent_m3748117041 (RuntimeObject * __this /* static, unused */, ARSessionFailed_t3599980200 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARSessionFailedEvent_m3748117041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARSessionFailed_t3599980200 * V_0 = NULL;
	ARSessionFailed_t3599980200 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARSessionFailed_t3599980200 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARSessionFailedEvent_4();
		V_0 = L_0;
	}

IL_0006:
	{
		ARSessionFailed_t3599980200 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARSessionFailed_t3599980200 * L_2 = V_1;
		ARSessionFailed_t3599980200 * L_3 = ___value0;
		Delegate_t69892740 * L_4 = Delegate_Remove_m1307738938(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARSessionFailed_t3599980200 * L_5 = V_0;
		ARSessionFailed_t3599980200 * L_6 = InterlockedCompareExchangeImpl<ARSessionFailed_t3599980200 *>((((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_ARSessionFailedEvent_4()), ((ARSessionFailed_t3599980200 *)CastclassSealed((RuntimeObject*)L_4, ARSessionFailed_t3599980200_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARSessionFailed_t3599980200 * L_7 = V_0;
		ARSessionFailed_t3599980200 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARSessionFailed_t3599980200 *)L_7) == ((RuntimeObject*)(ARSessionFailed_t3599980200 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
extern "C" intptr_t DEFAULT_CALL unity_CreateNativeARSession(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::unity_CreateNativeARSession(UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved,UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed)
extern "C"  IntPtr_t UnityARSessionNativeInterface_unity_CreateNativeARSession_m2427596710 (RuntimeObject * __this /* static, unused */, internal_ARFrameUpdate_t2726389622 * ___frameUpdate0, internal_ARAnchorAdded_t3791587037 * ___anchorAdded1, internal_ARAnchorUpdated_t2928809250 * ___anchorUpdated2, internal_ARAnchorRemoved_t409959511 * ___anchorRemoved3, ARSessionFailed_t3599980200 * ___sessionFailed4, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___frameUpdate0' to native representation
	Il2CppMethodPointer ____frameUpdate0_marshaled = NULL;
	____frameUpdate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___frameUpdate0));

	// Marshaling of parameter '___anchorAdded1' to native representation
	Il2CppMethodPointer ____anchorAdded1_marshaled = NULL;
	____anchorAdded1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___anchorAdded1));

	// Marshaling of parameter '___anchorUpdated2' to native representation
	Il2CppMethodPointer ____anchorUpdated2_marshaled = NULL;
	____anchorUpdated2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___anchorUpdated2));

	// Marshaling of parameter '___anchorRemoved3' to native representation
	Il2CppMethodPointer ____anchorRemoved3_marshaled = NULL;
	____anchorRemoved3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___anchorRemoved3));

	// Marshaling of parameter '___sessionFailed4' to native representation
	Il2CppMethodPointer ____sessionFailed4_marshaled = NULL;
	____sessionFailed4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___sessionFailed4));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(unity_CreateNativeARSession)(____frameUpdate0_marshaled, ____anchorAdded1_marshaled, ____anchorUpdated2_marshaled, ____anchorRemoved3_marshaled, ____sessionFailed4_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL StartWorldTrackingSession(intptr_t, ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartWorldTrackingSession(System.IntPtr,UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_StartWorldTrackingSession_m2658584269 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitWorldTackingSessionConfiguration_t2186350340  ___configuration1, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke);

	// Marshaling of parameter '___configuration1' to native representation
	ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke ____configuration1_marshaled = {};
	ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke(___configuration1, ____configuration1_marshaled);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StartWorldTrackingSession)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ____configuration1_marshaled);

	// Marshaling cleanup of parameter '___configuration1' native representation
	ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_cleanup(____configuration1_marshaled);

}
extern "C" void DEFAULT_CALL StartWorldTrackingSessionWithOptions(intptr_t, ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke, int32_t);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartWorldTrackingSessionWithOptions(System.IntPtr,UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_StartWorldTrackingSessionWithOptions_m2736903977 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitWorldTackingSessionConfiguration_t2186350340  ___configuration1, int32_t ___runOptions2, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke, int32_t);

	// Marshaling of parameter '___configuration1' to native representation
	ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke ____configuration1_marshaled = {};
	ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke(___configuration1, ____configuration1_marshaled);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StartWorldTrackingSessionWithOptions)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ____configuration1_marshaled, ___runOptions2);

	// Marshaling cleanup of parameter '___configuration1' native representation
	ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_cleanup(____configuration1_marshaled);

}
extern "C" void DEFAULT_CALL StartSession(intptr_t, ARKitSessionConfiguration_t3142208763_marshaled_pinvoke);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartSession(System.IntPtr,UnityEngine.XR.iOS.ARKitSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_StartSession_m283474087 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitSessionConfiguration_t3142208763  ___configuration1, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARKitSessionConfiguration_t3142208763_marshaled_pinvoke);

	// Marshaling of parameter '___configuration1' to native representation
	ARKitSessionConfiguration_t3142208763_marshaled_pinvoke ____configuration1_marshaled = {};
	ARKitSessionConfiguration_t3142208763_marshal_pinvoke(___configuration1, ____configuration1_marshaled);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StartSession)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ____configuration1_marshaled);

	// Marshaling cleanup of parameter '___configuration1' native representation
	ARKitSessionConfiguration_t3142208763_marshal_pinvoke_cleanup(____configuration1_marshaled);

}
extern "C" void DEFAULT_CALL StartSessionWithOptions(intptr_t, ARKitSessionConfiguration_t3142208763_marshaled_pinvoke, int32_t);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartSessionWithOptions(System.IntPtr,UnityEngine.XR.iOS.ARKitSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_StartSessionWithOptions_m984328055 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitSessionConfiguration_t3142208763  ___configuration1, int32_t ___runOptions2, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARKitSessionConfiguration_t3142208763_marshaled_pinvoke, int32_t);

	// Marshaling of parameter '___configuration1' to native representation
	ARKitSessionConfiguration_t3142208763_marshaled_pinvoke ____configuration1_marshaled = {};
	ARKitSessionConfiguration_t3142208763_marshal_pinvoke(___configuration1, ____configuration1_marshaled);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StartSessionWithOptions)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ____configuration1_marshaled, ___runOptions2);

	// Marshaling cleanup of parameter '___configuration1' native representation
	ARKitSessionConfiguration_t3142208763_marshal_pinvoke_cleanup(____configuration1_marshaled);

}
extern "C" void DEFAULT_CALL PauseSession(intptr_t);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::PauseSession(System.IntPtr)
extern "C"  void UnityARSessionNativeInterface_PauseSession_m3540433210 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PauseSession)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()));

}
extern "C" int32_t DEFAULT_CALL HitTest(intptr_t, ARPoint_t1649490841 , int64_t);
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(System.IntPtr,UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  int32_t UnityARSessionNativeInterface_HitTest_m4214093291 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARPoint_t1649490841  ___point1, int64_t ___types2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARPoint_t1649490841 , int64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(HitTest)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ___point1, ___types2);

	return returnValue;
}
extern "C" UnityARHitTestResult_t1002809393_marshaled_pinvoke DEFAULT_CALL GetLastHitTestResult(int32_t);
// UnityEngine.XR.iOS.UnityARHitTestResult UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetLastHitTestResult(System.Int32)
extern "C"  UnityARHitTestResult_t1002809393  UnityARSessionNativeInterface_GetLastHitTestResult_m2103824391 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method)
{


	typedef UnityARHitTestResult_t1002809393_marshaled_pinvoke (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	UnityARHitTestResult_t1002809393_marshaled_pinvoke returnValue = reinterpret_cast<PInvokeFunc>(GetLastHitTestResult)(___index0);

	// Marshaling of return value back from native representation
	UnityARHitTestResult_t1002809393  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	UnityARHitTestResult_t1002809393_marshal_pinvoke_back(returnValue, _returnValue_unmarshaled);

	// Marshaling cleanup of return value native representation
	UnityARHitTestResult_t1002809393_marshal_pinvoke_cleanup(returnValue);

	return _returnValue_unmarshaled;
}
extern "C" ARTextureHandles_t1375402930  DEFAULT_CALL GetVideoTextureHandles();
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetVideoTextureHandles()
extern "C"  ARTextureHandles_t1375402930  UnityARSessionNativeInterface_GetVideoTextureHandles_m1659577931 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef ARTextureHandles_t1375402930  (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	ARTextureHandles_t1375402930  returnValue = reinterpret_cast<PInvokeFunc>(GetVideoTextureHandles)();

	return returnValue;
}
extern "C" float DEFAULT_CALL GetAmbientIntensity();
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetAmbientIntensity()
extern "C"  float UnityARSessionNativeInterface_GetAmbientIntensity_m2586499763 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(GetAmbientIntensity)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL GetTrackingQuality();
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetTrackingQuality()
extern "C"  int32_t UnityARSessionNativeInterface_GetTrackingQuality_m3764224120 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetTrackingQuality)();

	return returnValue;
}
extern "C" float DEFAULT_CALL GetYUVTexCoordScale();
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetYUVTexCoordScale()
extern "C"  float UnityARSessionNativeInterface_GetYUVTexCoordScale_m2704052041 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(GetYUVTexCoordScale)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL GetARPointCloud(intptr_t*, uint32_t*);
// System.Boolean UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARPointCloud(System.IntPtr&,System.UInt32&)
extern "C"  bool UnityARSessionNativeInterface_GetARPointCloud_m228888586 (RuntimeObject * __this /* static, unused */, IntPtr_t* ___verts0, uint32_t* ___vertLength1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t*, uint32_t*);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetARPointCloud)(reinterpret_cast<intptr_t*>(___verts0), ___vertLength1);

	// Marshaling of parameter '___verts0' back from native representation
	___verts0 = reinterpret_cast<IntPtr_t*>(reinterpret_cast<intptr_t*>(___verts0));

	return static_cast<bool>(returnValue);
}
extern "C" void DEFAULT_CALL SetCameraNearFar(float, float);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCameraNearFar(System.Single,System.Single)
extern "C"  void UnityARSessionNativeInterface_SetCameraNearFar_m2805008808 (RuntimeObject * __this /* static, unused */, float ___nearZ0, float ___farZ1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SetCameraNearFar)(___nearZ0, ___farZ1);

}
// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARSessionNativeInterface()
extern "C"  UnityARSessionNativeInterface_t3869411053 * UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_s_UnityARSessionNativeInterface_7();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		UnityARSessionNativeInterface_t3869411053 * L_1 = (UnityARSessionNativeInterface_t3869411053 *)il2cpp_codegen_object_new(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface__ctor_m2217614958(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->set_s_UnityARSessionNativeInterface_7(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_2 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_s_UnityARSessionNativeInterface_7();
		return L_2;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetCameraPose()
extern "C"  Matrix4x4_t1288378485  UnityARSessionNativeInterface_GetCameraPose_m3014150810 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetCameraPose_m3014150810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t1288378485  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1288378485_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARMatrix4x4_t1132406930 * L_0 = (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_worldTransform_0();
		Vector4_t1376926224  L_1 = L_0->get_column0_0();
		Matrix4x4_SetColumn_m567234837((&V_0), 0, L_1, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_2 = (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_worldTransform_0();
		Vector4_t1376926224  L_3 = L_2->get_column1_1();
		Matrix4x4_SetColumn_m567234837((&V_0), 1, L_3, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_4 = (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_worldTransform_0();
		Vector4_t1376926224  L_5 = L_4->get_column2_2();
		Matrix4x4_SetColumn_m567234837((&V_0), 2, L_5, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_6 = (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_worldTransform_0();
		Vector4_t1376926224  L_7 = L_6->get_column3_3();
		Matrix4x4_SetColumn_m567234837((&V_0), 3, L_7, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_8 = V_0;
		return L_8;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetCameraProjection()
extern "C"  Matrix4x4_t1288378485  UnityARSessionNativeInterface_GetCameraProjection_m248827655 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetCameraProjection_m248827655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t1288378485  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1288378485_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARMatrix4x4_t1132406930 * L_0 = (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_projectionMatrix_1();
		Vector4_t1376926224  L_1 = L_0->get_column0_0();
		Matrix4x4_SetColumn_m567234837((&V_0), 0, L_1, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_2 = (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_projectionMatrix_1();
		Vector4_t1376926224  L_3 = L_2->get_column1_1();
		Matrix4x4_SetColumn_m567234837((&V_0), 1, L_3, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_4 = (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_projectionMatrix_1();
		Vector4_t1376926224  L_5 = L_4->get_column2_2();
		Matrix4x4_SetColumn_m567234837((&V_0), 2, L_5, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_6 = (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_projectionMatrix_1();
		Vector4_t1376926224  L_7 = L_6->get_column3_3();
		Matrix4x4_SetColumn_m567234837((&V_0), 3, L_7, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_8 = V_0;
		return L_8;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCameraClipPlanes(System.Single,System.Single)
extern "C"  void UnityARSessionNativeInterface_SetCameraClipPlanes_m540333021 (UnityARSessionNativeInterface_t3869411053 * __this, float ___nearZ0, float ___farZ1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_SetCameraClipPlanes_m540333021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___nearZ0;
		float L_1 = ___farZ1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_SetCameraNearFar_m2805008808(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_frame_update(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void UnityARSessionNativeInterface__frame_update_m3249087823 (RuntimeObject * __this /* static, unused */, internal_UnityARCamera_t1506237410  ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__frame_update_m3249087823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARCamera_t2376600594  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (UnityARCamera_t2376600594_il2cpp_TypeInfo_var, (&V_0));
		UnityARMatrix4x4_t1132406930  L_0 = (&___camera0)->get_projectionMatrix_1();
		(&V_0)->set_projectionMatrix_1(L_0);
		UnityARMatrix4x4_t1132406930  L_1 = (&___camera0)->get_worldTransform_0();
		(&V_0)->set_worldTransform_0(L_1);
		int32_t L_2 = (&___camera0)->get_trackingState_2();
		(&V_0)->set_trackingState_2(L_2);
		int32_t L_3 = (&___camera0)->get_trackingReason_3();
		(&V_0)->set_trackingReason_3(L_3);
		UnityARCamera_t2376600594  L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->set_s_Camera_6(L_4);
		uint32_t L_5 = (&___camera0)->get_getPointCloudData_4();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_UpdatePointCloudData_m3039135696(NULL /*static, unused*/, (((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6()), /*hidden argument*/NULL);
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARFrameUpdate_t3333962149 * L_6 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARFrameUpdatedEvent_0();
		if (!L_6)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARFrameUpdate_t3333962149 * L_7 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARFrameUpdatedEvent_0();
		UnityARCamera_t2376600594  L_8 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_s_Camera_6();
		NullCheck(L_7);
		ARFrameUpdate_Invoke_m1537461562(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::UpdatePointCloudData(UnityEngine.XR.iOS.UnityARCamera&)
extern "C"  void UnityARSessionNativeInterface_UpdatePointCloudData_m3039135696 (RuntimeObject * __this /* static, unused */, UnityARCamera_t2376600594 * ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_UpdatePointCloudData_m3039135696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	uint32_t V_1 = 0;
	bool V_2 = false;
	SingleU5BU5D_t3488826921* V_3 = NULL;
	Vector3U5BU5D_t4153953548* V_4 = NULL;
	int32_t V_5 = 0;
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		V_0 = L_0;
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		bool L_1 = UnityARSessionNativeInterface_GetARPointCloud_m228888586(NULL /*static, unused*/, (&V_0), (&V_1), /*hidden argument*/NULL);
		V_2 = L_1;
		V_3 = (SingleU5BU5D_t3488826921*)NULL;
		bool L_2 = V_2;
		if (!L_2)
		{
			goto IL_00a2;
		}
	}
	{
		uint32_t L_3 = V_1;
		V_3 = ((SingleU5BU5D_t3488826921*)SZArrayNew(SingleU5BU5D_t3488826921_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)L_3))));
		IntPtr_t L_4 = V_0;
		SingleU5BU5D_t3488826921* L_5 = V_3;
		uint32_t L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t485009956_il2cpp_TypeInfo_var);
		Marshal_Copy_m4018830953(NULL /*static, unused*/, L_4, L_5, 0, L_6, /*hidden argument*/NULL);
		uint32_t L_7 = V_1;
		V_4 = ((Vector3U5BU5D_t4153953548*)SZArrayNew(Vector3U5BU5D_t4153953548_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)((int32_t)((uint32_t)(int32_t)L_7/(uint32_t)(int32_t)4))))));
		V_5 = 0;
		goto IL_0090;
	}

IL_003e:
	{
		Vector3U5BU5D_t4153953548* L_8 = V_4;
		int32_t L_9 = V_5;
		NullCheck(L_8);
		SingleU5BU5D_t3488826921* L_10 = V_3;
		int32_t L_11 = V_5;
		int32_t L_12 = L_11;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_10);
		int32_t L_13 = L_12;
		float L_14 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_9/(int32_t)4)))))->set_x_1(L_14);
		Vector3U5BU5D_t4153953548* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		SingleU5BU5D_t3488826921* L_17 = V_3;
		int32_t L_18 = V_5;
		int32_t L_19 = L_18;
		V_5 = ((int32_t)((int32_t)L_19+(int32_t)1));
		NullCheck(L_17);
		int32_t L_20 = L_19;
		float L_21 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_16/(int32_t)4)))))->set_y_2(L_21);
		Vector3U5BU5D_t4153953548* L_22 = V_4;
		int32_t L_23 = V_5;
		NullCheck(L_22);
		SingleU5BU5D_t3488826921* L_24 = V_3;
		int32_t L_25 = V_5;
		int32_t L_26 = L_25;
		V_5 = ((int32_t)((int32_t)L_26+(int32_t)1));
		NullCheck(L_24);
		int32_t L_27 = L_26;
		float L_28 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23/(int32_t)4)))))->set_z_3(((-L_28)));
		int32_t L_29 = V_5;
		V_5 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_0090:
	{
		int32_t L_30 = V_5;
		uint32_t L_31 = V_1;
		if ((((int64_t)(((int64_t)((int64_t)L_30)))) < ((int64_t)(((int64_t)((uint64_t)L_31))))))
		{
			goto IL_003e;
		}
	}
	{
		UnityARCamera_t2376600594 * L_32 = ___camera0;
		Vector3U5BU5D_t4153953548* L_33 = V_4;
		L_32->set_pointCloudData_4(L_33);
	}

IL_00a2:
	{
		return;
	}
}
// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetPlaneAnchorFromAnchorData(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  ARPlaneAnchor_t1843157284  UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m1229793965 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t1511263172  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m1229793965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t1843157284  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1288378485  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (ARPlaneAnchor_t1843157284_il2cpp_TypeInfo_var, (&V_0));
		IntPtr_t L_0 = (&___anchor0)->get_ptrIdentifier_0();
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t485009956_il2cpp_TypeInfo_var);
		String_t* L_1 = Marshal_PtrToStringAuto_m4261366353(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		(&V_0)->set_identifier_0(L_1);
		Initobj (Matrix4x4_t1288378485_il2cpp_TypeInfo_var, (&V_1));
		UnityARMatrix4x4_t1132406930 * L_2 = (&___anchor0)->get_address_of_transform_1();
		Vector4_t1376926224  L_3 = L_2->get_column0_0();
		Matrix4x4_SetColumn_m567234837((&V_1), 0, L_3, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_4 = (&___anchor0)->get_address_of_transform_1();
		Vector4_t1376926224  L_5 = L_4->get_column1_1();
		Matrix4x4_SetColumn_m567234837((&V_1), 1, L_5, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_6 = (&___anchor0)->get_address_of_transform_1();
		Vector4_t1376926224  L_7 = L_6->get_column2_2();
		Matrix4x4_SetColumn_m567234837((&V_1), 2, L_7, /*hidden argument*/NULL);
		UnityARMatrix4x4_t1132406930 * L_8 = (&___anchor0)->get_address_of_transform_1();
		Vector4_t1376926224  L_9 = L_8->get_column3_3();
		Matrix4x4_SetColumn_m567234837((&V_1), 3, L_9, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_10 = V_1;
		(&V_0)->set_transform_1(L_10);
		int64_t L_11 = (&___anchor0)->get_alignment_2();
		(&V_0)->set_alignment_2(L_11);
		Vector4_t1376926224 * L_12 = (&___anchor0)->get_address_of_center_3();
		float L_13 = L_12->get_x_1();
		Vector4_t1376926224 * L_14 = (&___anchor0)->get_address_of_center_3();
		float L_15 = L_14->get_y_2();
		Vector4_t1376926224 * L_16 = (&___anchor0)->get_address_of_center_3();
		float L_17 = L_16->get_z_3();
		Vector3_t596762001  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m1043686083((&L_18), L_13, L_15, L_17, /*hidden argument*/NULL);
		(&V_0)->set_center_3(L_18);
		Vector4_t1376926224 * L_19 = (&___anchor0)->get_address_of_extent_4();
		float L_20 = L_19->get_x_1();
		Vector4_t1376926224 * L_21 = (&___anchor0)->get_address_of_extent_4();
		float L_22 = L_21->get_y_2();
		Vector4_t1376926224 * L_23 = (&___anchor0)->get_address_of_extent_4();
		float L_24 = L_23->get_z_3();
		Vector3_t596762001  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m1043686083((&L_25), L_20, L_22, L_24, /*hidden argument*/NULL);
		(&V_0)->set_extent_4(L_25);
		ARPlaneAnchor_t1843157284  L_26 = V_0;
		return L_26;
	}
}
// UnityEngine.XR.iOS.ARHitTestResult UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetHitTestResultFromResultData(UnityEngine.XR.iOS.UnityARHitTestResult)
extern "C"  ARHitTestResult_t1803723679  UnityARSessionNativeInterface_GetHitTestResultFromResultData_m2693418722 (RuntimeObject * __this /* static, unused */, UnityARHitTestResult_t1002809393  ___resultData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetHitTestResultFromResultData_m2693418722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARHitTestResult_t1803723679  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (ARHitTestResult_t1803723679_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = (&___resultData0)->get_type_0();
		(&V_0)->set_type_0(L_0);
		double L_1 = (&___resultData0)->get_distance_1();
		(&V_0)->set_distance_1(L_1);
		Matrix4x4_t1288378485  L_2 = (&___resultData0)->get_localTransform_2();
		(&V_0)->set_localTransform_2(L_2);
		Matrix4x4_t1288378485  L_3 = (&___resultData0)->get_worldTransform_3();
		(&V_0)->set_worldTransform_3(L_3);
		bool L_4 = (&___resultData0)->get_isValid_5();
		(&V_0)->set_isValid_5(L_4);
		IntPtr_t L_5 = (&___resultData0)->get_anchor_4();
		IntPtr_t L_6 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_7 = IntPtr_op_Inequality_m69146234(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0077;
		}
	}
	{
		IntPtr_t L_8 = (&___resultData0)->get_anchor_4();
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t485009956_il2cpp_TypeInfo_var);
		String_t* L_9 = Marshal_PtrToStringAuto_m4261366353(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->set_anchorIdentifier_4(L_9);
	}

IL_0077:
	{
		ARHitTestResult_t1803723679  L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_added(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_added_m3990782978 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t1511263172  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__anchor_added_m3990782978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t1843157284  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorAdded_t1580091102 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorAddedEvent_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		UnityARAnchorData_t1511263172  L_1 = ___anchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARPlaneAnchor_t1843157284  L_2 = UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m1229793965(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ARAnchorAdded_t1580091102 * L_3 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorAddedEvent_1();
		ARPlaneAnchor_t1843157284  L_4 = V_0;
		NullCheck(L_3);
		ARAnchorAdded_Invoke_m1710052577(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_updated(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_updated_m4275459728 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t1511263172  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__anchor_updated_m4275459728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t1843157284  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t1492128022 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorUpdatedEvent_2();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		UnityARAnchorData_t1511263172  L_1 = ___anchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARPlaneAnchor_t1843157284  L_2 = UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m1229793965(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ARAnchorUpdated_t1492128022 * L_3 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorUpdatedEvent_2();
		ARPlaneAnchor_t1843157284  L_4 = V_0;
		NullCheck(L_3);
		ARAnchorUpdated_Invoke_m2598267505(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_removed(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_removed_m4149770974 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t1511263172  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__anchor_removed_m4149770974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t1843157284  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t3663543047 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorRemovedEvent_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		UnityARAnchorData_t1511263172  L_1 = ___anchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARPlaneAnchor_t1843157284  L_2 = UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m1229793965(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ARAnchorRemoved_t3663543047 * L_3 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARAnchorRemovedEvent_3();
		ARPlaneAnchor_t1843157284  L_4 = V_0;
		NullCheck(L_3);
		ARAnchorRemoved_Invoke_m1905220566(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_ar_session_failed(System.String)
extern "C"  void UnityARSessionNativeInterface__ar_session_failed_m1149369129 (RuntimeObject * __this /* static, unused */, String_t* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__ar_session_failed_m1149369129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral707593833, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARSessionFailed_t3599980200 * L_0 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARSessionFailedEvent_4();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARSessionFailed_t3599980200 * L_1 = ((UnityARSessionNativeInterface_t3869411053_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var))->get_ARSessionFailedEvent_4();
		String_t* L_2 = ___error0;
		NullCheck(L_1);
		ARSessionFailed_Invoke_m2486705370(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfigAndOptions(UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_RunWithConfigAndOptions_m3750156203 (UnityARSessionNativeInterface_t3869411053 * __this, ARKitWorldTackingSessionConfiguration_t2186350340  ___config0, int32_t ___runOptions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_RunWithConfigAndOptions_m3750156203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARKitWorldTackingSessionConfiguration_t2186350340  L_1 = ___config0;
		int32_t L_2 = ___runOptions1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_StartWorldTrackingSessionWithOptions_m2736903977(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfig(UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_RunWithConfig_m3364444856 (UnityARSessionNativeInterface_t3869411053 * __this, ARKitWorldTackingSessionConfiguration_t2186350340  ___config0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_RunWithConfig_m3364444856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARKitWorldTackingSessionConfiguration_t2186350340  L_1 = ___config0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_StartWorldTrackingSession_m2658584269(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::Run()
extern "C"  void UnityARSessionNativeInterface_Run_m2233507690 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	{
		ARKitWorldTackingSessionConfiguration_t2186350340  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ARKitWorldTackingSessionConfiguration__ctor_m2859336466((&L_0), 0, 1, (bool)0, (bool)0, /*hidden argument*/NULL);
		UnityARSessionNativeInterface_RunWithConfig_m3364444856(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfigAndOptions(UnityEngine.XR.iOS.ARKitSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_RunWithConfigAndOptions_m2810560210 (UnityARSessionNativeInterface_t3869411053 * __this, ARKitSessionConfiguration_t3142208763  ___config0, int32_t ___runOptions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_RunWithConfigAndOptions_m2810560210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARKitSessionConfiguration_t3142208763  L_1 = ___config0;
		int32_t L_2 = ___runOptions1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_StartSessionWithOptions_m984328055(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfig(UnityEngine.XR.iOS.ARKitSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_RunWithConfig_m152596073 (UnityARSessionNativeInterface_t3869411053 * __this, ARKitSessionConfiguration_t3142208763  ___config0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_RunWithConfig_m152596073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARKitSessionConfiguration_t3142208763  L_1 = ___config0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_StartSession_m283474087(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::Pause()
extern "C"  void UnityARSessionNativeInterface_Pause_m224144355 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_Pause_m224144355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_PauseSession_m3540433210(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult> UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  List_1_t547075962 * UnityARSessionNativeInterface_HitTest_m1126351971 (UnityARSessionNativeInterface_t3869411053 * __this, ARPoint_t1649490841  ___point0, int64_t ___types1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_HitTest_m1126351971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t547075962 * V_1 = NULL;
	int32_t V_2 = 0;
	UnityARHitTestResult_t1002809393  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARPoint_t1649490841  L_1 = ___point0;
		int64_t L_2 = ___types1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		int32_t L_3 = UnityARSessionNativeInterface_HitTest_m4214093291(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(Int32_t3425510919_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1813664034(NULL /*static, unused*/, _stringLiteral1984200360, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		List_1_t547075962 * L_8 = (List_1_t547075962 *)il2cpp_codegen_object_new(List_1_t547075962_il2cpp_TypeInfo_var);
		List_1__ctor_m2682833015(L_8, /*hidden argument*/List_1__ctor_m2682833015_RuntimeMethod_var);
		V_1 = L_8;
		V_2 = 0;
		goto IL_0047;
	}

IL_0030:
	{
		int32_t L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARHitTestResult_t1002809393  L_10 = UnityARSessionNativeInterface_GetLastHitTestResult_m2103824391(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		List_1_t547075962 * L_11 = V_1;
		UnityARHitTestResult_t1002809393  L_12 = V_3;
		ARHitTestResult_t1803723679  L_13 = UnityARSessionNativeInterface_GetHitTestResultFromResultData_m2693418722(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_Add_m1601798359(L_11, L_13, /*hidden argument*/List_1_Add_m1601798359_RuntimeMethod_var);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0030;
		}
	}
	{
		List_1_t547075962 * L_17 = V_1;
		return L_17;
	}
}
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARVideoTextureHandles()
extern "C"  ARTextureHandles_t1375402930  UnityARSessionNativeInterface_GetARVideoTextureHandles_m3043872758 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARVideoTextureHandles_m3043872758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		ARTextureHandles_t1375402930  L_0 = UnityARSessionNativeInterface_GetVideoTextureHandles_m1659577931(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARAmbientIntensity()
extern "C"  float UnityARSessionNativeInterface_GetARAmbientIntensity_m2324758640 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARAmbientIntensity_m2324758640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		float L_0 = UnityARSessionNativeInterface_GetAmbientIntensity_m2586499763(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARTrackingQuality()
extern "C"  int32_t UnityARSessionNativeInterface_GetARTrackingQuality_m2526139771 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARTrackingQuality_m2526139771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		int32_t L_0 = UnityARSessionNativeInterface_GetTrackingQuality_m3764224120(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARYUVTexCoordScale()
extern "C"  float UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1899687960 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1899687960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		float L_0 = UnityARSessionNativeInterface_GetYUVTexCoordScale_m2704052041(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::.cctor()
extern "C"  void UnityARSessionNativeInterface__cctor_m290712526 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_ARAnchorAdded_t1580091102 (ARAnchorAdded_t1580091102 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARPlaneAnchor_t1843157284_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARPlaneAnchor_t1843157284_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARPlaneAnchor_t1843157284_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARPlaneAnchor_t1843157284_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorAdded__ctor_m2351544190 (ARAnchorAdded_t1580091102 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorAdded_Invoke_m1710052577 (ARAnchorAdded_t1580091102 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARAnchorAdded_Invoke_m1710052577((ARAnchorAdded_t1580091102 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::BeginInvoke(UnityEngine.XR.iOS.ARPlaneAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARAnchorAdded_BeginInvoke_m3780076230 (ARAnchorAdded_t1580091102 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARAnchorAdded_BeginInvoke_m3780076230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARPlaneAnchor_t1843157284_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::EndInvoke(System.IAsyncResult)
extern "C"  void ARAnchorAdded_EndInvoke_m3254558261 (ARAnchorAdded_t1580091102 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARAnchorRemoved_t3663543047 (ARAnchorRemoved_t3663543047 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARPlaneAnchor_t1843157284_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARPlaneAnchor_t1843157284_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARPlaneAnchor_t1843157284_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARPlaneAnchor_t1843157284_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorRemoved__ctor_m1468226077 (ARAnchorRemoved_t3663543047 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorRemoved_Invoke_m1905220566 (ARAnchorRemoved_t3663543047 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARAnchorRemoved_Invoke_m1905220566((ARAnchorRemoved_t3663543047 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::BeginInvoke(UnityEngine.XR.iOS.ARPlaneAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARAnchorRemoved_BeginInvoke_m2521998422 (ARAnchorRemoved_t3663543047 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARAnchorRemoved_BeginInvoke_m2521998422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARPlaneAnchor_t1843157284_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::EndInvoke(System.IAsyncResult)
extern "C"  void ARAnchorRemoved_EndInvoke_m1017321714 (ARAnchorRemoved_t3663543047 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARAnchorUpdated_t1492128022 (ARAnchorUpdated_t1492128022 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARPlaneAnchor_t1843157284_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARPlaneAnchor_t1843157284_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARPlaneAnchor_t1843157284_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARPlaneAnchor_t1843157284_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorUpdated__ctor_m401717748 (ARAnchorUpdated_t1492128022 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorUpdated_Invoke_m2598267505 (ARAnchorUpdated_t1492128022 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARAnchorUpdated_Invoke_m2598267505((ARAnchorUpdated_t1492128022 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARPlaneAnchor_t1843157284  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::BeginInvoke(UnityEngine.XR.iOS.ARPlaneAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARAnchorUpdated_BeginInvoke_m1428606326 (ARAnchorUpdated_t1492128022 * __this, ARPlaneAnchor_t1843157284  ___anchorData0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARAnchorUpdated_BeginInvoke_m1428606326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARPlaneAnchor_t1843157284_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void ARAnchorUpdated_EndInvoke_m2394394976 (ARAnchorUpdated_t1492128022 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void ARFrameUpdate__ctor_m722081207 (ARFrameUpdate_t3333962149 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::Invoke(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void ARFrameUpdate_Invoke_m1537461562 (ARFrameUpdate_t3333962149 * __this, UnityARCamera_t2376600594  ___camera0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARFrameUpdate_Invoke_m1537461562((ARFrameUpdate_t3333962149 *)__this->get_prev_9(),___camera0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARCamera_t2376600594  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARCamera_t2376600594  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::BeginInvoke(UnityEngine.XR.iOS.UnityARCamera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARFrameUpdate_BeginInvoke_m2642825502 (ARFrameUpdate_t3333962149 * __this, UnityARCamera_t2376600594  ___camera0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARFrameUpdate_BeginInvoke_m2642825502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARCamera_t2376600594_il2cpp_TypeInfo_var, &___camera0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::EndInvoke(System.IAsyncResult)
extern "C"  void ARFrameUpdate_EndInvoke_m2049743523 (ARFrameUpdate_t3333962149 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARSessionFailed_t3599980200 (ARSessionFailed_t3599980200 * __this, String_t* ___error0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___error0' to native representation
	char* ____error0_marshaled = NULL;
	____error0_marshaled = il2cpp_codegen_marshal_string(___error0);

	// Native function invocation
	il2cppPInvokeFunc(____error0_marshaled);

	// Marshaling cleanup of parameter '___error0' native representation
	il2cpp_codegen_marshal_free(____error0_marshaled);
	____error0_marshaled = NULL;

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::.ctor(System.Object,System.IntPtr)
extern "C"  void ARSessionFailed__ctor_m2477073150 (ARSessionFailed_t3599980200 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::Invoke(System.String)
extern "C"  void ARSessionFailed_Invoke_m2486705370 (ARSessionFailed_t3599980200 * __this, String_t* ___error0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARSessionFailed_Invoke_m2486705370((ARSessionFailed_t3599980200 *)__this->get_prev_9(),___error0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___error0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___error0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___error0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___error0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___error0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARSessionFailed_BeginInvoke_m1225332687 (ARSessionFailed_t3599980200 * __this, String_t* ___error0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___error0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::EndInvoke(System.IAsyncResult)
extern "C"  void ARSessionFailed_EndInvoke_m4238957703 (ARSessionFailed_t3599980200 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorAdded_t3791587037 (internal_ARAnchorAdded_t3791587037 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t1511263172 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorAdded__ctor_m3202793875 (internal_ARAnchorAdded_t3791587037 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorAdded_Invoke_m992918404 (internal_ARAnchorAdded_t3791587037 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorAdded_Invoke_m992918404((internal_ARAnchorAdded_t3791587037 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorAdded_BeginInvoke_m1551770526 (internal_ARAnchorAdded_t3791587037 * __this, UnityARAnchorData_t1511263172  ___anchorData0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorAdded_BeginInvoke_m1551770526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t1511263172_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorAdded_EndInvoke_m1957483493 (internal_ARAnchorAdded_t3791587037 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorRemoved_t409959511 (internal_ARAnchorRemoved_t409959511 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t1511263172 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorRemoved__ctor_m2196994448 (internal_ARAnchorRemoved_t409959511 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorRemoved_Invoke_m4011290538 (internal_ARAnchorRemoved_t409959511 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorRemoved_Invoke_m4011290538((internal_ARAnchorRemoved_t409959511 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorRemoved_BeginInvoke_m3275603482 (internal_ARAnchorRemoved_t409959511 * __this, UnityARAnchorData_t1511263172  ___anchorData0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorRemoved_BeginInvoke_m3275603482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t1511263172_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorRemoved_EndInvoke_m660728720 (internal_ARAnchorRemoved_t409959511 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorUpdated_t2928809250 (internal_ARAnchorUpdated_t2928809250 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t1511263172 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorUpdated__ctor_m1845092063 (internal_ARAnchorUpdated_t2928809250 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorUpdated_Invoke_m2200307882 (internal_ARAnchorUpdated_t2928809250 * __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorUpdated_Invoke_m2200307882((internal_ARAnchorUpdated_t2928809250 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t1511263172  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorUpdated_BeginInvoke_m2660600822 (internal_ARAnchorUpdated_t2928809250 * __this, UnityARAnchorData_t1511263172  ___anchorData0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorUpdated_BeginInvoke_m2660600822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t1511263172_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorUpdated_EndInvoke_m636143843 (internal_ARAnchorUpdated_t2928809250 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARFrameUpdate_t2726389622 (internal_ARFrameUpdate_t2726389622 * __this, internal_UnityARCamera_t1506237410  ___camera0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(internal_UnityARCamera_t1506237410 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___camera0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARFrameUpdate__ctor_m2168096248 (internal_ARFrameUpdate_t2726389622 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::Invoke(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void internal_ARFrameUpdate_Invoke_m585242343 (internal_ARFrameUpdate_t2726389622 * __this, internal_UnityARCamera_t1506237410  ___camera0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARFrameUpdate_Invoke_m585242343((internal_ARFrameUpdate_t2726389622 *)__this->get_prev_9(),___camera0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, internal_UnityARCamera_t1506237410  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, internal_UnityARCamera_t1506237410  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::BeginInvoke(UnityEngine.XR.iOS.internal_UnityARCamera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARFrameUpdate_BeginInvoke_m4150912559 (internal_ARFrameUpdate_t2726389622 * __this, internal_UnityARCamera_t1506237410  ___camera0, AsyncCallback_t606388952 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARFrameUpdate_BeginInvoke_m4150912559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(internal_UnityARCamera_t1506237410_il2cpp_TypeInfo_var, &___camera0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARFrameUpdate_EndInvoke_m1392870627 (internal_ARFrameUpdate_t2726389622 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::.ctor()
extern "C"  void UnityARUtility__ctor_m2022745106 (UnityARUtility_t2651983316 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m990968439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::InitializePlanePrefab(UnityEngine.GameObject)
extern "C"  void UnityARUtility_InitializePlanePrefab_m2229940298 (RuntimeObject * __this /* static, unused */, GameObject_t3649338848 * ___go0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_InitializePlanePrefab_m2229940298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t3649338848 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t2651983316_il2cpp_TypeInfo_var);
		((UnityARUtility_t2651983316_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t2651983316_il2cpp_TypeInfo_var))->set_planePrefab_2(L_0);
		return;
	}
}
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::CreatePlaneInScene(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t3649338848 * UnityARUtility_CreatePlaneInScene_m2413657456 (RuntimeObject * __this /* static, unused */, ARPlaneAnchor_t1843157284  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_CreatePlaneInScene_m2413657456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t3649338848 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t2651983316_il2cpp_TypeInfo_var);
		GameObject_t3649338848 * L_0 = ((UnityARUtility_t2651983316_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t2651983316_il2cpp_TypeInfo_var))->get_planePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t2651983316_il2cpp_TypeInfo_var);
		GameObject_t3649338848 * L_2 = ((UnityARUtility_t2651983316_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t2651983316_il2cpp_TypeInfo_var))->get_planePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		GameObject_t3649338848 * L_3 = Object_Instantiate_TisGameObject_t3649338848_m4208004768(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3649338848_m4208004768_RuntimeMethod_var);
		V_0 = L_3;
		goto IL_0026;
	}

IL_0020:
	{
		GameObject_t3649338848 * L_4 = (GameObject_t3649338848 *)il2cpp_codegen_object_new(GameObject_t3649338848_il2cpp_TypeInfo_var);
		GameObject__ctor_m433497810(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0026:
	{
		GameObject_t3649338848 * L_5 = V_0;
		String_t* L_6 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_5);
		Object_set_name_m2740154093(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t3649338848 * L_7 = V_0;
		ARPlaneAnchor_t1843157284  L_8 = ___arPlaneAnchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t2651983316_il2cpp_TypeInfo_var);
		GameObject_t3649338848 * L_9 = UnityARUtility_UpdatePlaneWithAnchorTransform_m923713608(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::UpdatePlaneWithAnchorTransform(UnityEngine.GameObject,UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t3649338848 * UnityARUtility_UpdatePlaneWithAnchorTransform_m923713608 (RuntimeObject * __this /* static, unused */, GameObject_t3649338848 * ___plane0, ARPlaneAnchor_t1843157284  ___arPlaneAnchor1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_UpdatePlaneWithAnchorTransform_m923713608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t2678471223 * V_0 = NULL;
	{
		GameObject_t3649338848 * L_0 = ___plane0;
		NullCheck(L_0);
		Transform_t3933397867 * L_1 = GameObject_get_transform_m890220094(L_0, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_2 = (&___arPlaneAnchor1)->get_transform_1();
		Vector3_t596762001  L_3 = UnityARMatrixOps_GetPosition_m3553979848(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m2789033506(L_1, L_3, /*hidden argument*/NULL);
		GameObject_t3649338848 * L_4 = ___plane0;
		NullCheck(L_4);
		Transform_t3933397867 * L_5 = GameObject_get_transform_m890220094(L_4, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_6 = (&___arPlaneAnchor1)->get_transform_1();
		Quaternion_t3165733013  L_7 = UnityARMatrixOps_GetRotation_m3890376831(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_rotation_m1288069348(L_5, L_7, /*hidden argument*/NULL);
		GameObject_t3649338848 * L_8 = ___plane0;
		NullCheck(L_8);
		MeshFilter_t2678471223 * L_9 = GameObject_GetComponentInChildren_TisMeshFilter_t2678471223_m4019234351(L_8, /*hidden argument*/GameObject_GetComponentInChildren_TisMeshFilter_t2678471223_m4019234351_RuntimeMethod_var);
		V_0 = L_9;
		MeshFilter_t2678471223 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_10, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00c6;
		}
	}
	{
		MeshFilter_t2678471223 * L_12 = V_0;
		NullCheck(L_12);
		GameObject_t3649338848 * L_13 = Component_get_gameObject_m3065601689(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3933397867 * L_14 = GameObject_get_transform_m890220094(L_13, /*hidden argument*/NULL);
		Vector3_t596762001 * L_15 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_16 = L_15->get_x_1();
		Vector3_t596762001 * L_17 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_18 = L_17->get_y_2();
		Vector3_t596762001 * L_19 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_20 = L_19->get_z_3();
		Vector3_t596762001  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m1043686083((&L_21), ((float)((float)L_16*(float)(0.1f))), ((float)((float)L_18*(float)(0.1f))), ((float)((float)L_20*(float)(0.1f))), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localScale_m3940090091(L_14, L_21, /*hidden argument*/NULL);
		MeshFilter_t2678471223 * L_22 = V_0;
		NullCheck(L_22);
		GameObject_t3649338848 * L_23 = Component_get_gameObject_m3065601689(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t3933397867 * L_24 = GameObject_get_transform_m890220094(L_23, /*hidden argument*/NULL);
		Vector3_t596762001 * L_25 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_26 = L_25->get_x_1();
		Vector3_t596762001 * L_27 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_28 = L_27->get_y_2();
		Vector3_t596762001 * L_29 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_30 = L_29->get_z_3();
		Vector3_t596762001  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector3__ctor_m1043686083((&L_31), L_26, L_28, ((-L_30)), /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localPosition_m3583812093(L_24, L_31, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		GameObject_t3649338848 * L_32 = ___plane0;
		return L_32;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::.cctor()
extern "C"  void UnityARUtility__cctor_m1368721418 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::.ctor()
extern "C"  void UnityARVideo__ctor_m3189481088 (UnityARVideo_t2470488057 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::Start()
extern "C"  void UnityARVideo_Start_m2313235990 (UnityARVideo_t2470488057 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_Start_m2313235990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Session_6(L_0);
		__this->set_bCommandBufferInitialized_7((bool)0);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::InitializeCommandBuffer()
extern "C"  void UnityARVideo_InitializeCommandBuffer_m3792744751 (UnityARVideo_t2470488057 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_InitializeCommandBuffer_m3792744751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CommandBuffer_t1027034615 * L_0 = (CommandBuffer_t1027034615 *)il2cpp_codegen_object_new(CommandBuffer_t1027034615_il2cpp_TypeInfo_var);
		CommandBuffer__ctor_m762038758(L_0, /*hidden argument*/NULL);
		__this->set_m_VideoCommandBuffer_3(L_0);
		CommandBuffer_t1027034615 * L_1 = __this->get_m_VideoCommandBuffer_3();
		RenderTargetIdentifier_t2478716510  L_2 = RenderTargetIdentifier_op_Implicit_m407650415(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Material_t4055262778 * L_3 = __this->get_m_ClearMaterial_2();
		NullCheck(L_1);
		CommandBuffer_Blit_m382886313(L_1, (Texture_t1132728222 *)NULL, L_2, L_3, /*hidden argument*/NULL);
		Camera_t226495598 * L_4 = Component_GetComponent_TisCamera_t226495598_m3685067992(__this, /*hidden argument*/Component_GetComponent_TisCamera_t226495598_m3685067992_RuntimeMethod_var);
		CommandBuffer_t1027034615 * L_5 = __this->get_m_VideoCommandBuffer_3();
		NullCheck(L_4);
		Camera_AddCommandBuffer_m1530474523(L_4, ((int32_t)10), L_5, /*hidden argument*/NULL);
		__this->set_bCommandBufferInitialized_7((bool)1);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::OnDestroy()
extern "C"  void UnityARVideo_OnDestroy_m757367868 (UnityARVideo_t2470488057 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_OnDestroy_m757367868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t226495598 * L_0 = Component_GetComponent_TisCamera_t226495598_m3685067992(__this, /*hidden argument*/Component_GetComponent_TisCamera_t226495598_m3685067992_RuntimeMethod_var);
		CommandBuffer_t1027034615 * L_1 = __this->get_m_VideoCommandBuffer_3();
		NullCheck(L_0);
		Camera_RemoveCommandBuffer_m4279420586(L_0, ((int32_t)10), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::OnPreRender()
extern "C"  void UnityARVideo_OnPreRender_m1630060808 (UnityARVideo_t2470488057 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_OnPreRender_m1630060808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARTextureHandles_t1375402930  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Resolution_t3724314788  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Matrix4x4_t1288378485  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		UnityARSessionNativeInterface_t3869411053 * L_0 = __this->get_m_Session_6();
		NullCheck(L_0);
		ARTextureHandles_t1375402930  L_1 = UnityARSessionNativeInterface_GetARVideoTextureHandles_m3043872758(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = (&V_0)->get_textureY_0();
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_4 = IntPtr_op_Equality_m2427008840(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0038;
		}
	}
	{
		IntPtr_t L_5 = (&V_0)->get_textureCbCr_1();
		IntPtr_t L_6 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_7 = IntPtr_op_Equality_m2427008840(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0039;
		}
	}

IL_0038:
	{
		return;
	}

IL_0039:
	{
		bool L_8 = __this->get_bCommandBufferInitialized_7();
		if (L_8)
		{
			goto IL_004a;
		}
	}
	{
		UnityARVideo_InitializeCommandBuffer_m3792744751(__this, /*hidden argument*/NULL);
	}

IL_004a:
	{
		Resolution_t3724314788  L_9 = Screen_get_currentResolution_m3445211963(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = Resolution_get_width_m494188502((&V_1), /*hidden argument*/NULL);
		int32_t L_11 = Resolution_get_height_m1988903362((&V_1), /*hidden argument*/NULL);
		IntPtr_t L_12 = (&V_0)->get_textureY_0();
		Texture2D_t2870930912 * L_13 = Texture2D_CreateExternalTexture_m1925818354(NULL /*static, unused*/, L_10, L_11, ((int32_t)63), (bool)0, (bool)0, L_12, /*hidden argument*/NULL);
		__this->set__videoTextureY_4(L_13);
		Texture2D_t2870930912 * L_14 = __this->get__videoTextureY_4();
		NullCheck(L_14);
		Texture_set_filterMode_m3057534198(L_14, 1, /*hidden argument*/NULL);
		Texture2D_t2870930912 * L_15 = __this->get__videoTextureY_4();
		NullCheck(L_15);
		Texture_set_wrapMode_m2518460338(L_15, 0, /*hidden argument*/NULL);
		Texture2D_t2870930912 * L_16 = __this->get__videoTextureY_4();
		IntPtr_t L_17 = (&V_0)->get_textureY_0();
		NullCheck(L_16);
		Texture2D_UpdateExternalTexture_m703548091(L_16, L_17, /*hidden argument*/NULL);
		int32_t L_18 = Resolution_get_width_m494188502((&V_1), /*hidden argument*/NULL);
		int32_t L_19 = Resolution_get_height_m1988903362((&V_1), /*hidden argument*/NULL);
		IntPtr_t L_20 = (&V_0)->get_textureCbCr_1();
		Texture2D_t2870930912 * L_21 = Texture2D_CreateExternalTexture_m1925818354(NULL /*static, unused*/, L_18, L_19, ((int32_t)62), (bool)0, (bool)0, L_20, /*hidden argument*/NULL);
		__this->set__videoTextureCbCr_5(L_21);
		Texture2D_t2870930912 * L_22 = __this->get__videoTextureCbCr_5();
		NullCheck(L_22);
		Texture_set_filterMode_m3057534198(L_22, 1, /*hidden argument*/NULL);
		Texture2D_t2870930912 * L_23 = __this->get__videoTextureCbCr_5();
		NullCheck(L_23);
		Texture_set_wrapMode_m2518460338(L_23, 0, /*hidden argument*/NULL);
		Texture2D_t2870930912 * L_24 = __this->get__videoTextureCbCr_5();
		IntPtr_t L_25 = (&V_0)->get_textureCbCr_1();
		NullCheck(L_24);
		Texture2D_UpdateExternalTexture_m703548091(L_24, L_25, /*hidden argument*/NULL);
		Material_t4055262778 * L_26 = __this->get_m_ClearMaterial_2();
		Texture2D_t2870930912 * L_27 = __this->get__videoTextureY_4();
		NullCheck(L_26);
		Material_SetTexture_m1483606842(L_26, _stringLiteral813233302, L_27, /*hidden argument*/NULL);
		Material_t4055262778 * L_28 = __this->get_m_ClearMaterial_2();
		Texture2D_t2870930912 * L_29 = __this->get__videoTextureCbCr_5();
		NullCheck(L_28);
		Material_SetTexture_m1483606842(L_28, _stringLiteral2373698739, L_29, /*hidden argument*/NULL);
		V_2 = 0;
		V_3 = (0.0f);
		int32_t L_30 = Screen_get_orientation_m4014868403(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)1))))
		{
			goto IL_0138;
		}
	}
	{
		V_3 = (-90.0f);
		V_2 = 1;
		goto IL_0161;
	}

IL_0138:
	{
		int32_t L_31 = Screen_get_orientation_m4014868403(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_31) == ((uint32_t)2))))
		{
			goto IL_0150;
		}
	}
	{
		V_3 = (90.0f);
		V_2 = 1;
		goto IL_0161;
	}

IL_0150:
	{
		int32_t L_32 = Screen_get_orientation_m4014868403(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)4))))
		{
			goto IL_0161;
		}
	}
	{
		V_3 = (-180.0f);
	}

IL_0161:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t596762001_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_33 = Vector3_get_zero_m325886990(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_34 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3165733013_il2cpp_TypeInfo_var);
		Quaternion_t3165733013  L_35 = Quaternion_Euler_m437352610(NULL /*static, unused*/, (0.0f), (0.0f), L_34, /*hidden argument*/NULL);
		Vector3_t596762001  L_36 = Vector3_get_one_m2020581060(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1288378485_il2cpp_TypeInfo_var);
		Matrix4x4_t1288378485  L_37 = Matrix4x4_TRS_m1848776295(NULL /*static, unused*/, L_33, L_35, L_36, /*hidden argument*/NULL);
		V_4 = L_37;
		Material_t4055262778 * L_38 = __this->get_m_ClearMaterial_2();
		Matrix4x4_t1288378485  L_39 = V_4;
		NullCheck(L_38);
		Material_SetMatrix_m1397732260(L_38, _stringLiteral3027136166, L_39, /*hidden argument*/NULL);
		Material_t4055262778 * L_40 = __this->get_m_ClearMaterial_2();
		UnityARSessionNativeInterface_t3869411053 * L_41 = __this->get_m_Session_6();
		NullCheck(L_41);
		float L_42 = UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1899687960(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		Material_SetFloat_m272696061(L_40, _stringLiteral3578222424, L_42, /*hidden argument*/NULL);
		Material_t4055262778 * L_43 = __this->get_m_ClearMaterial_2();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		Material_SetInt_m130172955(L_43, _stringLiteral556172373, L_44, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityPointCloudExample::.ctor()
extern "C"  void UnityPointCloudExample__ctor_m1860152523 (UnityPointCloudExample_t595762290 * __this, const RuntimeMethod* method)
{
	{
		__this->set_numPointsToShow_2(((int32_t)100));
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityPointCloudExample::Start()
extern "C"  void UnityPointCloudExample_Start_m761710379 (UnityPointCloudExample_t595762290 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPointCloudExample_Start_m761710379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UnityPointCloudExample_ARFrameUpdated_m2014687293_RuntimeMethod_var);
		ARFrameUpdate_t3333962149 * L_1 = (ARFrameUpdate_t3333962149 *)il2cpp_codegen_object_new(ARFrameUpdate_t3333962149_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m722081207(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1467721625(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t3649338848 * L_2 = __this->get_PointCloudPrefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_2, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005c;
		}
	}
	{
		List_1_t2392691131 * L_4 = (List_1_t2392691131 *)il2cpp_codegen_object_new(List_1_t2392691131_il2cpp_TypeInfo_var);
		List_1__ctor_m267030108(L_4, /*hidden argument*/List_1__ctor_m267030108_RuntimeMethod_var);
		__this->set_pointCloudObjects_4(L_4);
		V_0 = 0;
		goto IL_004e;
	}

IL_0034:
	{
		List_1_t2392691131 * L_5 = __this->get_pointCloudObjects_4();
		GameObject_t3649338848 * L_6 = __this->get_PointCloudPrefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		GameObject_t3649338848 * L_7 = Object_Instantiate_TisGameObject_t3649338848_m4208004768(NULL /*static, unused*/, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t3649338848_m4208004768_RuntimeMethod_var);
		NullCheck(L_5);
		List_1_Add_m423025013(L_5, L_7, /*hidden argument*/List_1_Add_m423025013_RuntimeMethod_var);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_9 = V_0;
		uint32_t L_10 = __this->get_numPointsToShow_2();
		if ((((int64_t)(((int64_t)((int64_t)L_9)))) < ((int64_t)(((int64_t)((uint64_t)L_10))))))
		{
			goto IL_0034;
		}
	}

IL_005c:
	{
		return;
	}
}
// System.Void UnityPointCloudExample::ARFrameUpdated(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void UnityPointCloudExample_ARFrameUpdated_m2014687293 (UnityPointCloudExample_t595762290 * __this, UnityARCamera_t2376600594  ___camera0, const RuntimeMethod* method)
{
	{
		Vector3U5BU5D_t4153953548* L_0 = (&___camera0)->get_pointCloudData_4();
		__this->set_m_PointCloudData_5(L_0);
		return;
	}
}
// System.Void UnityPointCloudExample::Update()
extern "C"  void UnityPointCloudExample_Update_m617780277 (UnityPointCloudExample_t595762290 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPointCloudExample_Update_m617780277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector4_t1376926224  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t3649338848 * V_2 = NULL;
	{
		GameObject_t3649338848 * L_0 = __this->get_PointCloudPrefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008c;
		}
	}
	{
		Vector3U5BU5D_t4153953548* L_2 = __this->get_m_PointCloudData_5();
		if (!L_2)
		{
			goto IL_008c;
		}
	}
	{
		V_0 = 0;
		goto IL_0070;
	}

IL_0023:
	{
		Vector3U5BU5D_t4153953548* L_3 = __this->get_m_PointCloudData_5();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t1376926224_il2cpp_TypeInfo_var);
		Vector4_t1376926224  L_5 = Vector4_op_Implicit_m878911398(NULL /*static, unused*/, (*(Vector3_t596762001 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))), /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t2392691131 * L_6 = __this->get_pointCloudObjects_4();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		GameObject_t3649338848 * L_8 = List_1_get_Item_m1883443879(L_6, L_7, /*hidden argument*/List_1_get_Item_m1883443879_RuntimeMethod_var);
		V_2 = L_8;
		GameObject_t3649338848 * L_9 = V_2;
		NullCheck(L_9);
		Transform_t3933397867 * L_10 = GameObject_get_transform_m890220094(L_9, /*hidden argument*/NULL);
		float L_11 = (&V_1)->get_x_1();
		float L_12 = (&V_1)->get_y_2();
		float L_13 = (&V_1)->get_z_3();
		Vector3_t596762001  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m1043686083((&L_14), L_11, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m2789033506(L_10, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0070:
	{
		int32_t L_16 = V_0;
		Vector3U5BU5D_t4153953548* L_17 = __this->get_m_PointCloudData_5();
		NullCheck(L_17);
		uint32_t L_18 = __this->get_numPointsToShow_2();
		int64_t L_19 = Math_Min_m3904695542(NULL /*static, unused*/, (((int64_t)((int64_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length))))))), (((int64_t)((uint64_t)L_18))), /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_16)))) < ((int64_t)L_19)))
		{
			goto IL_0023;
		}
	}

IL_008c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
