﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// BallMaker
struct BallMaker_t1786680372;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1096588306;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2709947369;
// UnityEngine.GameObject
struct GameObject_t3649338848;
// System.String
struct String_t;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1885265068;
// UnityEngine.Renderer
struct Renderer_t674913088;
// UnityEngine.Camera
struct Camera_t226495598;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t3869411053;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>
struct List_1_t547075962;
// BallMover
struct BallMover_t4283490589;
// UnityEngine.Object
struct Object_t1008057425;
// UnityEngine.Transform
struct Transform_t3933397867;
// Ballz
struct Ballz_t2183783589;
// UnityEngine.Component
struct Component_t531478471;
// ColorChangedEvent
struct ColorChangedEvent_t347040188;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t3808368702;
// ColorImage
struct ColorImage_t2737809041;
// UnityEngine.UI.Image
struct Image_t126372159;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t1392064635;
// ColorLabel
struct ColorLabel_t538373442;
// UnityEngine.UI.Text
struct Text_t3069741234;
// UnityEngine.Events.UnityAction`3<System.Single,System.Single,System.Single>
struct UnityAction_3_t3771153942;
// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct UnityEvent_3_t876514367;
// ColorPicker
struct ColorPicker_t2582382693;
// HSVChangedEvent
struct HSVChangedEvent_t3546572410;
// System.NotImplementedException
struct NotImplementedException_t4033310348;
// ColorPickerTester
struct ColorPickerTester_t3436045112;
// UnityEngine.Material
struct Material_t4055262778;
// ColorPresets
struct ColorPresets_t321881905;
// ColorSlider
struct ColorSlider_t4061812907;
// UnityEngine.UI.Slider
struct Slider_t301630380;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t1040040728;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3918860037;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t2040196808;
// ColorSliderImage
struct ColorSliderImage_t664667956;
// UnityEngine.RectTransform
struct RectTransform_t1885177139;
// UnityEngine.UI.RawImage
struct RawImage_t3037409423;
// UnityEngine.Texture
struct Texture_t1132728222;
// UnityEngine.Texture2D
struct Texture2D_t2870930912;
// UnityEngine.Color32[]
struct Color32U5BU5D_t636217733;
// HexColorField
struct HexColorField_t1392034802;
// UnityEngine.UI.InputField
struct InputField_t3161868630;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t1556243238;
// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t1336681460;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t1193757028;
// UnityEngine.Events.UnityEvent`1<System.String>
struct UnityEvent_1_t3752985527;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3610061095;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t2953828243;
// System.Object[]
struct ObjectU5BU5D_t3384890222;
// System.String[]
struct StringU5BU5D_t1448570014;
// KnightControl
struct KnightControl_t1902388948;
// UnityEngine.Animation
struct Animation_t1557311624;
// UnityEngine.Collider
struct Collider_t1354100743;
// UnityEngine.AnimationState
struct AnimationState_t2216918921;
// ModeSwitcher
struct ModeSwitcher_t833699841;
// ParticlePainter
struct ParticlePainter_t294017160;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct ARFrameUpdate_t3333962149;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1777616458;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t3635081580;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t520968741;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3160831282;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t419044168;
// ScreenShot
struct ScreenShot_t707795496;
// SlimeControl
struct SlimeControl_t1232093680;
// SVBoxSlider
struct SVBoxSlider_t2769455468;
// UnityEngine.UI.BoxSlider
struct BoxSlider_t2780260910;
// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t2026169079;
// UnityEngine.Events.UnityAction`2<System.Single,System.Single>
struct UnityAction_2_t3878312970;
// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct UnityEvent_2_t1533210863;
// TiltWindow
struct TiltWindow_t1431928114;
// UnityEngine.UI.Selectable
struct Selectable_t3890617260;
// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_t2441083007;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1563322019;
// UnityEngine.XR.iOS.UnityARAmbient
struct UnityARAmbient_t3389999931;
// UnityEngine.Light
struct Light_t2513018095;
// System.IntPtr[]
struct IntPtrU5BU5D_t1854953685;
// System.Collections.IDictionary
struct IDictionary_t1267335557;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t124164409;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3977637013;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t4105631055;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t4153953548;
// UnityEngine.XR.iOS.ARHitTestResult[]
struct ARHitTestResultU5BU5D_t3516045062;
// System.Char[]
struct CharU5BU5D_t674980486;
// System.Byte
struct Byte_t3065488403;
// System.Double
struct Double_t1029397067;
// System.UInt16
struct UInt16_t2173916929;
// System.Void
struct Void_t2725935594;
// UnityEngine.Sprite
struct Sprite_t1309550511;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t487304461;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t421390435;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t3590231488;
// System.Int32[]
struct Int32U5BU5D_t595981822;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2392691131;
// System.IAsyncResult
struct IAsyncResult_t1614106113;
// System.AsyncCallback
struct AsyncCallback_t606388952;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct ARAnchorAdded_t1580091102;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct ARAnchorUpdated_t1492128022;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct ARAnchorRemoved_t3663543047;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct ARSessionFailed_t3599980200;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct internal_ARFrameUpdate_t2726389622;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct internal_ARAnchorAdded_t3791587037;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct internal_ARAnchorUpdated_t2928809250;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct internal_ARAnchorRemoved_t409959511;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t1646417403;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t23782843;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t1488940705;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t799251060;
// UnityEngine.Canvas
struct Canvas_t3680465181;
// UnityEngine.Events.UnityAction
struct UnityAction_t2037703605;
// UnityEngine.Mesh
struct Mesh_t540144243;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t1674489302;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t4135605540;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t2633969543;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2976076601;
// UnityEngine.UI.Graphic
struct Graphic_t1406460313;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1819317824;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t2754790973;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t4262405814;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3551258791;
// UnityEngine.TextGenerator
struct TextGenerator_t3308673789;
// UnityEngine.Coroutine
struct Coroutine_t3555871518;
// UnityEngine.Event
struct Event_t3603750770;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t2103047313;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3000894829;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t3857693239;
// UnityEngine.UI.FontData
struct FontData_t1355997048;

extern RuntimeClass* MaterialPropertyBlock_t2709947369_il2cpp_TypeInfo_var;
extern const uint32_t BallMaker_Start_m309396839_MetadataUsageId;
extern RuntimeClass* Quaternion_t3165733013_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t1008057425_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t3649338848_m3594403643_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_t1885265068_m3314343886_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2735595319;
extern const uint32_t BallMaker_CreateBall_m20175966_MetadataUsageId;
extern RuntimeClass* Input_t1121709983_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t59524482_il2cpp_TypeInfo_var;
extern RuntimeClass* ARPoint_t1649490841_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m655066853_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2437281809_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3632740371_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1007619642_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3562983614_RuntimeMethod_var;
extern const uint32_t BallMaker_Update_m3527128747_MetadataUsageId;
extern const uint32_t BallMover_CreateMoveBall_m1567691649_MetadataUsageId;
extern RuntimeClass* Vector3_t596762001_il2cpp_TypeInfo_var;
extern const uint32_t BallMover_Update_m3241756666_MetadataUsageId;
extern RuntimeClass* Mathf_t413863475_il2cpp_TypeInfo_var;
extern const uint32_t Ballz_Update_m184330856_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1__ctor_m1815935619_RuntimeMethod_var;
extern const uint32_t ColorChangedEvent__ctor_m420103119_MetadataUsageId;
extern RuntimeClass* UnityAction_1_t1392064635_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisImage_t126372159_m734387587_RuntimeMethod_var;
extern const RuntimeMethod* ColorImage_ColorChanged_m2018191545_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1__ctor_m4180551968_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var;
extern const uint32_t ColorImage_Awake_m1049520671_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1_RemoveListener_m1657194609_RuntimeMethod_var;
extern const uint32_t ColorImage_OnDestroy_m2347837237_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1853803690;
extern const uint32_t ColorLabel__ctor_m3991178045_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisText_t3069741234_m3241350479_RuntimeMethod_var;
extern const uint32_t ColorLabel_Awake_m2026245735_MetadataUsageId;
extern RuntimeClass* UnityAction_3_t3771153942_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ColorLabel_ColorChanged_m2301408063_RuntimeMethod_var;
extern const RuntimeMethod* ColorLabel_HSVChanged_m4075465524_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_3__ctor_m672494230_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_3_AddListener_m3442876925_RuntimeMethod_var;
extern const uint32_t ColorLabel_OnEnable_m2729080183_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_3_RemoveListener_m2900402221_RuntimeMethod_var;
extern const uint32_t ColorLabel_OnDestroy_m4045402800_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3581513187;
extern const uint32_t ColorLabel_UpdateValue_m2990534176_MetadataUsageId;
extern RuntimeClass* Int32_t3425510919_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3638548537;
extern const uint32_t ColorLabel_ConvertToDisplayString_m3783319992_MetadataUsageId;
extern RuntimeClass* ColorChangedEvent_t347040188_il2cpp_TypeInfo_var;
extern RuntimeClass* HSVChangedEvent_t3546572410_il2cpp_TypeInfo_var;
extern const uint32_t ColorPicker__ctor_m3964538034_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1_Invoke_m1164418121_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_3_Invoke_m2348520805_RuntimeMethod_var;
extern const uint32_t ColorPicker_SendChangedEvent_m1023062679_MetadataUsageId;
extern RuntimeClass* NotImplementedException_t4033310348_il2cpp_TypeInfo_var;
extern const uint32_t ColorPicker_GetValue_m3746747326_MetadataUsageId;
extern const RuntimeMethod* ColorPickerTester_U3CStartU3Em__0_m1156723077_RuntimeMethod_var;
extern const uint32_t ColorPickerTester_Start_m3073950392_MetadataUsageId;
extern const RuntimeMethod* ColorPresets_ColorChanged_m2470810136_RuntimeMethod_var;
extern const uint32_t ColorPresets_Awake_m2155330283_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisImage_t126372159_m3108703159_RuntimeMethod_var;
extern const uint32_t ColorPresets_CreatePresetButton_m932495736_MetadataUsageId;
extern RuntimeClass* UnityAction_1_t3918860037_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisSlider_t301630380_m2687307688_RuntimeMethod_var;
extern const RuntimeMethod* ColorSlider_ColorChanged_m2054110329_RuntimeMethod_var;
extern const RuntimeMethod* ColorSlider_HSVChanged_m3322884794_RuntimeMethod_var;
extern const RuntimeMethod* ColorSlider_SliderChanged_m2971956828_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1__ctor_m482530400_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m1802892215_RuntimeMethod_var;
extern const uint32_t ColorSlider_Awake_m3513452837_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1_RemoveListener_m1915413156_RuntimeMethod_var;
extern const uint32_t ColorSlider_OnDestroy_m2701140116_MetadataUsageId;
extern RuntimeClass* RectTransform_t1885177139_il2cpp_TypeInfo_var;
extern const uint32_t ColorSliderImage_get_rectTransform_m2176888620_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRawImage_t3037409423_m801911595_RuntimeMethod_var;
extern const uint32_t ColorSliderImage_Awake_m1145630542_MetadataUsageId;
extern const RuntimeMethod* ColorSliderImage_ColorChanged_m3348797575_RuntimeMethod_var;
extern const RuntimeMethod* ColorSliderImage_HSVChanged_m2525805608_RuntimeMethod_var;
extern const uint32_t ColorSliderImage_OnEnable_m2913045470_MetadataUsageId;
extern const uint32_t ColorSliderImage_OnDisable_m2796054081_MetadataUsageId;
extern const uint32_t ColorSliderImage_OnDestroy_m3945285673_MetadataUsageId;
extern RuntimeClass* Texture2D_t2870930912_il2cpp_TypeInfo_var;
extern RuntimeClass* Color32U5BU5D_t636217733_il2cpp_TypeInfo_var;
extern const uint32_t ColorSliderImage_RegenerateTexture_m629802786_MetadataUsageId;
extern RuntimeClass* UnityAction_1_t1336681460_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisInputField_t3161868630_m1543344974_RuntimeMethod_var;
extern const RuntimeMethod* HexColorField_UpdateColor_m3237283494_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1__ctor_m2651916199_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m3737383032_RuntimeMethod_var;
extern const RuntimeMethod* HexColorField_UpdateHex_m1669923459_RuntimeMethod_var;
extern const uint32_t HexColorField_Awake_m1129651162_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1_RemoveListener_m3968893251_RuntimeMethod_var;
extern const uint32_t HexColorField_OnDestroy_m2327818717_MetadataUsageId;
extern RuntimeClass* Debug_t3314485529_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3963194399;
extern const uint32_t HexColorField_UpdateColor_m3237283494_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t3384890222_il2cpp_TypeInfo_var;
extern RuntimeClass* Byte_t3065488403_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1202944861;
extern Il2CppCodeGenString* _stringLiteral1944985349;
extern const uint32_t HexColorField_ColorToHex_m4216070677_MetadataUsageId;
extern RuntimeClass* Regex_t1187425115_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t539237919_il2cpp_TypeInfo_var;
extern RuntimeClass* Color32_t2499566028_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral377735934;
extern Il2CppCodeGenString* _stringLiteral82137410;
extern const uint32_t HexColorField_HexToColor_m2425132568_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_3__ctor_m3450248554_RuntimeMethod_var;
extern const uint32_t HSVChangedEvent__ctor_m2671244103_MetadataUsageId;
extern RuntimeClass* StringU5BU5D_t1448570014_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2396611028;
extern Il2CppCodeGenString* _stringLiteral3789558989;
extern Il2CppCodeGenString* _stringLiteral2650072937;
extern Il2CppCodeGenString* _stringLiteral597313382;
extern const uint32_t HsvColor_ToString_m1904641755_MetadataUsageId;
extern RuntimeClass* HsvColor_t3806242730_il2cpp_TypeInfo_var;
extern const uint32_t HSVUtil_ConvertRgbToHsv_m4081435738_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisAnimation_t1557311624_m834402472_RuntimeMethod_var;
extern const uint32_t KnightControl_Start_m4145995205_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2086967534;
extern const uint32_t KnightControl_Update_m1395860105_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3368953453;
extern const uint32_t KnightControl_OnTriggerEnter_m306515042_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4060027686;
extern Il2CppCodeGenString* _stringLiteral3393300267;
extern const uint32_t KnightControl_OnTriggerExit_m2870755794_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1715509795;
extern Il2CppCodeGenString* _stringLiteral87740549;
extern const uint32_t KnightControl_Reverse_m1515466881_MetadataUsageId;
extern const uint32_t KnightControl_Forward_m2448336092_MetadataUsageId;
extern const uint32_t KnightControl_Wait_m4054482282_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4284855045;
extern Il2CppCodeGenString* _stringLiteral98951896;
extern const uint32_t KnightControl_Attack_m4293413112_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3730573748;
extern const uint32_t KnightControl_KnightDamaged_m167166218_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2083696160;
extern Il2CppCodeGenString* _stringLiteral4194900223;
extern const uint32_t KnightControl_KnightDead_m2753808469_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral32909362;
extern const uint32_t KnightControl_Revive_m3691166334_MetadataUsageId;
extern RuntimeClass* GUI_t3786480996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1950011964;
extern Il2CppCodeGenString* _stringLiteral154465382;
extern const uint32_t ModeSwitcher_OnGUI_m270521944_MetadataUsageId;
extern const uint32_t ParticlePainter__ctor_m3031644694_MetadataUsageId;
extern RuntimeClass* ARFrameUpdate_t3333962149_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3635081580_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t520968741_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ParticlePainter_ARFrameUpdated_m3938859628_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisParticleSystem_t1777616458_m895722352_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3365215536_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3297558810_RuntimeMethod_var;
extern const RuntimeMethod* ParticlePainter_U3CStartU3Em__0_m4245901003_RuntimeMethod_var;
extern const uint32_t ParticlePainter_Start_m2700506111_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t1288378485_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m684783150_RuntimeMethod_var;
extern const uint32_t ParticlePainter_ARFrameUpdated_m3938859628_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2194669068;
extern Il2CppCodeGenString* _stringLiteral3753373970;
extern Il2CppCodeGenString* _stringLiteral2701812837;
extern const uint32_t ParticlePainter_OnGUI_m975798185_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m1608859291_RuntimeMethod_var;
extern const uint32_t ParticlePainter_RestartPainting_m2557631278_MetadataUsageId;
extern RuntimeClass* ParticleU5BU5D_t419044168_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m3739057527_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2550167595_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1534598271_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2323590328_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3684835367_RuntimeMethod_var;
extern const uint32_t ParticlePainter_Update_m3795775040_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1601136640;
extern const uint32_t ScreenShot_Start_m1707547866_MetadataUsageId;
extern RuntimeClass* DateTime_t1819153659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1682919869;
extern Il2CppCodeGenString* _stringLiteral1812546333;
extern const uint32_t ScreenShot_Update_m691034463_MetadataUsageId;
extern const uint32_t SlimeControl_Start_m3521291899_MetadataUsageId;
extern const uint32_t SlimeControl_Update_m3521749286_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral700932277;
extern const uint32_t SlimeControl_OnTriggerEnter_m3552506635_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3347777996;
extern const uint32_t SlimeControl_OnTriggerExit_m2068307658_MetadataUsageId;
extern const uint32_t SlimeControl_Walk_m2369857677_MetadataUsageId;
extern const uint32_t SlimeControl_Wait_m2044302504_MetadataUsageId;
extern const uint32_t SlimeControl_Attack_m2278930363_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3020831419;
extern Il2CppCodeGenString* _stringLiteral3311888704;
extern Il2CppCodeGenString* _stringLiteral617751626;
extern const uint32_t SlimeControl_SlimeDamaged_m1220440998_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral722121443;
extern const uint32_t SlimeControl_Revive_m3625609039_MetadataUsageId;
extern const uint32_t SVBoxSlider_get_rectTransform_m4173866029_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisBoxSlider_t2780260910_m43308176_RuntimeMethod_var;
extern const uint32_t SVBoxSlider_Awake_m912103099_MetadataUsageId;
extern RuntimeClass* UnityAction_2_t3878312970_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SVBoxSlider_SliderChanged_m2185817435_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_2__ctor_m4219090612_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_2_AddListener_m3369274527_RuntimeMethod_var;
extern const RuntimeMethod* SVBoxSlider_HSVChanged_m2761716849_RuntimeMethod_var;
extern const uint32_t SVBoxSlider_OnEnable_m953190647_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_2_RemoveListener_m1528257211_RuntimeMethod_var;
extern const uint32_t SVBoxSlider_OnDisable_m2057716965_MetadataUsageId;
extern const uint32_t SVBoxSlider_OnDestroy_m1753957120_MetadataUsageId;
extern const uint32_t SVBoxSlider_RegenerateSVTexture_m4214705435_MetadataUsageId;
extern const uint32_t TiltWindow__ctor_m4145182152_MetadataUsageId;
extern const uint32_t TiltWindow_Update_m4026234967_MetadataUsageId;
extern RuntimeClass* BoxSliderEvent_t2026169079_il2cpp_TypeInfo_var;
extern RuntimeClass* Selectable_t3890617260_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider__ctor_m162539454_MetadataUsageId;
extern const RuntimeMethod* BoxSlider_SetClass_TisRectTransform_t1885177139_m1931764714_RuntimeMethod_var;
extern const uint32_t BoxSlider_set_handleRect_m1467039354_MetadataUsageId;
extern const RuntimeMethod* BoxSlider_SetStruct_TisSingle_t2847614712_m250040320_RuntimeMethod_var;
extern const uint32_t BoxSlider_set_minValue_m3177896132_MetadataUsageId;
extern const uint32_t BoxSlider_set_maxValue_m646776916_MetadataUsageId;
extern const RuntimeMethod* BoxSlider_SetStruct_TisBoolean_t362855854_m292852063_RuntimeMethod_var;
extern const uint32_t BoxSlider_set_wholeNumbers_m4022631289_MetadataUsageId;
extern const uint32_t BoxSlider_get_value_m1655722908_MetadataUsageId;
extern const uint32_t BoxSlider_get_normalizedValue_m1950536199_MetadataUsageId;
extern const uint32_t BoxSlider_set_normalizedValue_m1073637371_MetadataUsageId;
extern const uint32_t BoxSlider_get_valueY_m1180997838_MetadataUsageId;
extern const uint32_t BoxSlider_get_normalizedValueY_m266672788_MetadataUsageId;
extern const uint32_t BoxSlider_set_normalizedValueY_m815891677_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t1885177139_m2102537755_RuntimeMethod_var;
extern const uint32_t BoxSlider_UpdateCachedReferences_m977902596_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_2_Invoke_m2732613775_RuntimeMethod_var;
extern const uint32_t BoxSlider_Set_m3176380998_MetadataUsageId;
extern const uint32_t BoxSlider_SetY_m3865330729_MetadataUsageId;
extern const uint32_t BoxSlider_UpdateVisuals_m4195678704_MetadataUsageId;
extern RuntimeClass* RectTransformUtility_t2874646166_il2cpp_TypeInfo_var;
extern const uint32_t BoxSlider_UpdateDrag_m2830268571_MetadataUsageId;
extern const uint32_t BoxSlider_OnPointerDown_m730960263_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_2__ctor_m1813845425_RuntimeMethod_var;
extern const uint32_t BoxSliderEvent__ctor_m2331793288_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisLight_t2513018095_m2996527359_RuntimeMethod_var;
extern const uint32_t UnityARAmbient_Start_m4140057879_MetadataUsageId;
struct Vector3_t596762001 ;

struct GameObjectU5BU5D_t1488940705;
struct Color32U5BU5D_t636217733;
struct ObjectU5BU5D_t3384890222;
struct StringU5BU5D_t1448570014;
struct ParticleU5BU5D_t419044168;


#ifndef U3CMODULEU3E_T3896811617_H
#define U3CMODULEU3E_T3896811617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3896811617 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3896811617_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T82373287_H
#define EXCEPTION_T82373287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t82373287  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t1854953685* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t82373287 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t1854953685* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t1854953685** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t1854953685* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___inner_exception_1)); }
	inline Exception_t82373287 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t82373287 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t82373287 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t82373287, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t82373287, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t82373287, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T82373287_H
#ifndef UNITYEVENTBASE_T3260807274_H
#define UNITYEVENTBASE_T3260807274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3260807274  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t124164409 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3977637013 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3260807274, ___m_Calls_0)); }
	inline InvokableCallList_t124164409 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t124164409 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t124164409 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3260807274, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3977637013 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3977637013 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3977637013 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3260807274, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3260807274, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3260807274_H
#ifndef VALUETYPE_T3433162460_H
#define VALUETYPE_T3433162460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3433162460  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_com
{
};
#endif // VALUETYPE_T3433162460_H
#ifndef LIST_1_T520968741_H
#define LIST_1_T520968741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct  List_1_t520968741  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ParticleSystemU5BU5D_t4105631055* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t520968741, ____items_1)); }
	inline ParticleSystemU5BU5D_t4105631055* get__items_1() const { return ____items_1; }
	inline ParticleSystemU5BU5D_t4105631055** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ParticleSystemU5BU5D_t4105631055* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t520968741, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t520968741, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t520968741_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ParticleSystemU5BU5D_t4105631055* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t520968741_StaticFields, ___EmptyArray_4)); }
	inline ParticleSystemU5BU5D_t4105631055* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ParticleSystemU5BU5D_t4105631055** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ParticleSystemU5BU5D_t4105631055* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T520968741_H
#ifndef LIST_1_T3635081580_H
#define LIST_1_T3635081580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t3635081580  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t4153953548* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3635081580, ____items_1)); }
	inline Vector3U5BU5D_t4153953548* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t4153953548** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t4153953548* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3635081580, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3635081580, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3635081580_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t4153953548* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3635081580_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t4153953548* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t4153953548** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t4153953548* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3635081580_H
#ifndef HSVUTIL_T1113819789_H
#define HSVUTIL_T1113819789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVUtil
struct  HSVUtil_t1113819789  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T1113819789_H
#ifndef LIST_1_T547075962_H
#define LIST_1_T547075962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>
struct  List_1_t547075962  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ARHitTestResultU5BU5D_t3516045062* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t547075962, ____items_1)); }
	inline ARHitTestResultU5BU5D_t3516045062* get__items_1() const { return ____items_1; }
	inline ARHitTestResultU5BU5D_t3516045062** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ARHitTestResultU5BU5D_t3516045062* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t547075962, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t547075962, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t547075962_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ARHitTestResultU5BU5D_t3516045062* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t547075962_StaticFields, ___EmptyArray_4)); }
	inline ARHitTestResultU5BU5D_t3516045062* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ARHitTestResultU5BU5D_t3516045062** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ARHitTestResultU5BU5D_t3516045062* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T547075962_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t674980486* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t674980486* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t674980486** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t674980486* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ABSTRACTEVENTDATA_T3677302406_H
#define ABSTRACTEVENTDATA_T3677302406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t3677302406  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t3677302406, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T3677302406_H
#ifndef SINGLE_T2847614712_H
#define SINGLE_T2847614712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2847614712 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2847614712, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2847614712_H
#ifndef ENUM_T473240710_H
#define ENUM_T473240710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t473240710  : public ValueType_t3433162460
{
public:

public:
};

struct Enum_t473240710_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t674980486* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t473240710_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t674980486* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t674980486** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t674980486* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t473240710_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t473240710_marshaled_com
{
};
#endif // ENUM_T473240710_H
#ifndef COLOR32_T2499566028_H
#define COLOR32_T2499566028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t2499566028 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t2499566028, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t2499566028, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t2499566028, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t2499566028, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2499566028_H
#ifndef SYSTEMEXCEPTION_T500802787_H
#define SYSTEMEXCEPTION_T500802787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t500802787  : public Exception_t82373287
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T500802787_H
#ifndef RECT_T1992046353_H
#define RECT_T1992046353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t1992046353 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T1992046353_H
#ifndef BYTE_T3065488403_H
#define BYTE_T3065488403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t3065488403 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t3065488403, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T3065488403_H
#ifndef HSVCOLOR_T3806242730_H
#define HSVCOLOR_T3806242730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HsvColor
struct  HsvColor_t3806242730 
{
public:
	// System.Double HsvColor::H
	double ___H_0;
	// System.Double HsvColor::S
	double ___S_1;
	// System.Double HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t3806242730, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t3806242730, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t3806242730, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T3806242730_H
#ifndef UNITYEVENT_3_T876514367_H
#define UNITYEVENT_3_T876514367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct  UnityEvent_3_t876514367  : public UnityEventBase_t3260807274
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3384890222* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t876514367, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3384890222* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3384890222** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3384890222* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T876514367_H
#ifndef UNITYEVENT_1_T3752985527_H
#define UNITYEVENT_1_T3752985527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t3752985527  : public UnityEventBase_t3260807274
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3384890222* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3752985527, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3384890222* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3384890222** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3384890222* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3752985527_H
#ifndef CHAR_T539237919_H
#define CHAR_T539237919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t539237919 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t539237919, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t539237919_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t539237919_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t539237919_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t539237919_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t539237919_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t539237919_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t539237919_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t539237919_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T539237919_H
#ifndef TIMESPAN_T4158060032_H
#define TIMESPAN_T4158060032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t4158060032 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t4158060032, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t4158060032_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4158060032  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4158060032  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4158060032  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t4158060032_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t4158060032  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t4158060032 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t4158060032  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t4158060032_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t4158060032  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t4158060032 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t4158060032  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t4158060032_StaticFields, ___Zero_2)); }
	inline TimeSpan_t4158060032  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t4158060032 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t4158060032  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T4158060032_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T3165733013_H
#define QUATERNION_T3165733013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t3165733013 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t3165733013_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t3165733013  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t3165733013  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t3165733013 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t3165733013  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T3165733013_H
#ifndef UNITYEVENT_1_T3808368702_H
#define UNITYEVENT_1_T3808368702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3808368702  : public UnityEventBase_t3260807274
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3384890222* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3808368702, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3384890222* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3384890222** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3384890222* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3808368702_H
#ifndef VOID_T2725935594_H
#define VOID_T2725935594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2725935594 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2725935594_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T1306825633_H
#define DRIVENRECTTRANSFORMTRACKER_T1306825633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t1306825633 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T1306825633_H
#ifndef COLOR_T320819310_H
#define COLOR_T320819310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t320819310 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t320819310, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t320819310, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t320819310, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t320819310, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T320819310_H
#ifndef UNITYEVENT_1_T2040196808_H
#define UNITYEVENT_1_T2040196808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2040196808  : public UnityEventBase_t3260807274
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3384890222* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2040196808, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3384890222* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3384890222** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3384890222* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2040196808_H
#ifndef SPRITESTATE_T758977253_H
#define SPRITESTATE_T758977253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t758977253 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t1309550511 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t1309550511 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t1309550511 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t758977253, ___m_HighlightedSprite_0)); }
	inline Sprite_t1309550511 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t1309550511 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t1309550511 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t758977253, ___m_PressedSprite_1)); }
	inline Sprite_t1309550511 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t1309550511 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t1309550511 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t758977253, ___m_DisabledSprite_2)); }
	inline Sprite_t1309550511 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t1309550511 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t1309550511 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t758977253_marshaled_pinvoke
{
	Sprite_t1309550511 * ___m_HighlightedSprite_0;
	Sprite_t1309550511 * ___m_PressedSprite_1;
	Sprite_t1309550511 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t758977253_marshaled_com
{
	Sprite_t1309550511 * ___m_HighlightedSprite_0;
	Sprite_t1309550511 * ___m_PressedSprite_1;
	Sprite_t1309550511 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T758977253_H
#ifndef VECTOR2_T59524482_H
#define VECTOR2_T59524482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t59524482 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t59524482, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t59524482, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t59524482_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t59524482  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t59524482  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t59524482  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t59524482  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t59524482  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t59524482  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t59524482  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t59524482  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___zeroVector_2)); }
	inline Vector2_t59524482  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t59524482 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t59524482  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___oneVector_3)); }
	inline Vector2_t59524482  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t59524482 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t59524482  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___upVector_4)); }
	inline Vector2_t59524482  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t59524482 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t59524482  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___downVector_5)); }
	inline Vector2_t59524482  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t59524482 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t59524482  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___leftVector_6)); }
	inline Vector2_t59524482  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t59524482 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t59524482  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___rightVector_7)); }
	inline Vector2_t59524482  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t59524482 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t59524482  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t59524482  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t59524482 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t59524482  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t59524482  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t59524482 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t59524482  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T59524482_H
#ifndef UNITYEVENT_2_T1533210863_H
#define UNITYEVENT_2_T1533210863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct  UnityEvent_2_t1533210863  : public UnityEventBase_t3260807274
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3384890222* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1533210863, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3384890222* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3384890222** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3384890222* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1533210863_H
#ifndef BOOLEAN_T362855854_H
#define BOOLEAN_T362855854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t362855854 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t362855854, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t362855854_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t362855854_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t362855854_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T362855854_H
#ifndef DOUBLE_T1029397067_H
#define DOUBLE_T1029397067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t1029397067 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t1029397067, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T1029397067_H
#ifndef VECTOR3_T596762001_H
#define VECTOR3_T596762001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t596762001 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t596762001_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t596762001  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t596762001  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t596762001  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t596762001  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t596762001  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t596762001  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t596762001  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t596762001  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t596762001  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t596762001  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___zeroVector_4)); }
	inline Vector3_t596762001  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t596762001 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t596762001  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___oneVector_5)); }
	inline Vector3_t596762001  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t596762001 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t596762001  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___upVector_6)); }
	inline Vector3_t596762001  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t596762001 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t596762001  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___downVector_7)); }
	inline Vector3_t596762001  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t596762001 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t596762001  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___leftVector_8)); }
	inline Vector3_t596762001  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t596762001 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t596762001  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___rightVector_9)); }
	inline Vector3_t596762001  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t596762001 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t596762001  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___forwardVector_10)); }
	inline Vector3_t596762001  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t596762001 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t596762001  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___backVector_11)); }
	inline Vector3_t596762001  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t596762001 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t596762001  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t596762001  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t596762001 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t596762001  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t596762001  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t596762001 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t596762001  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T596762001_H
#ifndef INT32_T3425510919_H
#define INT32_T3425510919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t3425510919 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t3425510919, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T3425510919_H
#ifndef ARPOINT_T1649490841_H
#define ARPOINT_T1649490841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPoint
struct  ARPoint_t1649490841 
{
public:
	// System.Double UnityEngine.XR.iOS.ARPoint::x
	double ___x_0;
	// System.Double UnityEngine.XR.iOS.ARPoint::y
	double ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ARPoint_t1649490841, ___x_0)); }
	inline double get_x_0() const { return ___x_0; }
	inline double* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(double value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ARPoint_t1649490841, ___y_1)); }
	inline double get_y_1() const { return ___y_1; }
	inline double* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(double value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPOINT_T1649490841_H
#ifndef MATRIX4X4_T1288378485_H
#define MATRIX4X4_T1288378485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1288378485 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1288378485_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1288378485  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1288378485  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1288378485  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1288378485 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1288378485  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1288378485  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1288378485 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1288378485  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1288378485_H
#ifndef VECTOR4_T1376926224_H
#define VECTOR4_T1376926224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t1376926224 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t1376926224_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t1376926224  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t1376926224  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t1376926224  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t1376926224  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___zeroVector_5)); }
	inline Vector4_t1376926224  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t1376926224 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t1376926224  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___oneVector_6)); }
	inline Vector4_t1376926224  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t1376926224 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t1376926224  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t1376926224  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t1376926224 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t1376926224  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t1376926224  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t1376926224 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t1376926224  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T1376926224_H
#ifndef BASEEVENTDATA_T341072110_H
#define BASEEVENTDATA_T341072110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t341072110  : public AbstractEventData_t3677302406
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t487304461 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t341072110, ___m_EventSystem_1)); }
	inline EventSystem_t487304461 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t487304461 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t487304461 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T341072110_H
#ifndef HIDEFLAGS_T796833612_H
#define HIDEFLAGS_T796833612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t796833612 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t796833612, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T796833612_H
#ifndef ENUMERATOR_T688973221_H
#define ENUMERATOR_T688973221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
struct  Enumerator_t688973221 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3635081580 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Vector3_t596762001  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t688973221, ___l_0)); }
	inline List_1_t3635081580 * get_l_0() const { return ___l_0; }
	inline List_1_t3635081580 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3635081580 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t688973221, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t688973221, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t688973221, ___current_3)); }
	inline Vector3_t596762001  get_current_3() const { return ___current_3; }
	inline Vector3_t596762001 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector3_t596762001  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T688973221_H
#ifndef PARTICLE_T1268635397_H
#define PARTICLE_T1268635397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/Particle
struct  Particle_t1268635397 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t596762001  ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t596762001  ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t596762001  ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t596762001  ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t596762001  ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t596762001  ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t596762001  ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t596762001  ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t2499566028  ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_12;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_13;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_Position_0)); }
	inline Vector3_t596762001  get_m_Position_0() const { return ___m_Position_0; }
	inline Vector3_t596762001 * get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(Vector3_t596762001  value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_1() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_Velocity_1)); }
	inline Vector3_t596762001  get_m_Velocity_1() const { return ___m_Velocity_1; }
	inline Vector3_t596762001 * get_address_of_m_Velocity_1() { return &___m_Velocity_1; }
	inline void set_m_Velocity_1(Vector3_t596762001  value)
	{
		___m_Velocity_1 = value;
	}

	inline static int32_t get_offset_of_m_AnimatedVelocity_2() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_AnimatedVelocity_2)); }
	inline Vector3_t596762001  get_m_AnimatedVelocity_2() const { return ___m_AnimatedVelocity_2; }
	inline Vector3_t596762001 * get_address_of_m_AnimatedVelocity_2() { return &___m_AnimatedVelocity_2; }
	inline void set_m_AnimatedVelocity_2(Vector3_t596762001  value)
	{
		___m_AnimatedVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_InitialVelocity_3() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_InitialVelocity_3)); }
	inline Vector3_t596762001  get_m_InitialVelocity_3() const { return ___m_InitialVelocity_3; }
	inline Vector3_t596762001 * get_address_of_m_InitialVelocity_3() { return &___m_InitialVelocity_3; }
	inline void set_m_InitialVelocity_3(Vector3_t596762001  value)
	{
		___m_InitialVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_AxisOfRotation_4() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_AxisOfRotation_4)); }
	inline Vector3_t596762001  get_m_AxisOfRotation_4() const { return ___m_AxisOfRotation_4; }
	inline Vector3_t596762001 * get_address_of_m_AxisOfRotation_4() { return &___m_AxisOfRotation_4; }
	inline void set_m_AxisOfRotation_4(Vector3_t596762001  value)
	{
		___m_AxisOfRotation_4 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_Rotation_5)); }
	inline Vector3_t596762001  get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline Vector3_t596762001 * get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(Vector3_t596762001  value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocity_6() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_AngularVelocity_6)); }
	inline Vector3_t596762001  get_m_AngularVelocity_6() const { return ___m_AngularVelocity_6; }
	inline Vector3_t596762001 * get_address_of_m_AngularVelocity_6() { return &___m_AngularVelocity_6; }
	inline void set_m_AngularVelocity_6(Vector3_t596762001  value)
	{
		___m_AngularVelocity_6 = value;
	}

	inline static int32_t get_offset_of_m_StartSize_7() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_StartSize_7)); }
	inline Vector3_t596762001  get_m_StartSize_7() const { return ___m_StartSize_7; }
	inline Vector3_t596762001 * get_address_of_m_StartSize_7() { return &___m_StartSize_7; }
	inline void set_m_StartSize_7(Vector3_t596762001  value)
	{
		___m_StartSize_7 = value;
	}

	inline static int32_t get_offset_of_m_StartColor_8() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_StartColor_8)); }
	inline Color32_t2499566028  get_m_StartColor_8() const { return ___m_StartColor_8; }
	inline Color32_t2499566028 * get_address_of_m_StartColor_8() { return &___m_StartColor_8; }
	inline void set_m_StartColor_8(Color32_t2499566028  value)
	{
		___m_StartColor_8 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_9() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_RandomSeed_9)); }
	inline uint32_t get_m_RandomSeed_9() const { return ___m_RandomSeed_9; }
	inline uint32_t* get_address_of_m_RandomSeed_9() { return &___m_RandomSeed_9; }
	inline void set_m_RandomSeed_9(uint32_t value)
	{
		___m_RandomSeed_9 = value;
	}

	inline static int32_t get_offset_of_m_Lifetime_10() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_Lifetime_10)); }
	inline float get_m_Lifetime_10() const { return ___m_Lifetime_10; }
	inline float* get_address_of_m_Lifetime_10() { return &___m_Lifetime_10; }
	inline void set_m_Lifetime_10(float value)
	{
		___m_Lifetime_10 = value;
	}

	inline static int32_t get_offset_of_m_StartLifetime_11() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_StartLifetime_11)); }
	inline float get_m_StartLifetime_11() const { return ___m_StartLifetime_11; }
	inline float* get_address_of_m_StartLifetime_11() { return &___m_StartLifetime_11; }
	inline void set_m_StartLifetime_11(float value)
	{
		___m_StartLifetime_11 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator0_12() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_EmitAccumulator0_12)); }
	inline float get_m_EmitAccumulator0_12() const { return ___m_EmitAccumulator0_12; }
	inline float* get_address_of_m_EmitAccumulator0_12() { return &___m_EmitAccumulator0_12; }
	inline void set_m_EmitAccumulator0_12(float value)
	{
		___m_EmitAccumulator0_12 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator1_13() { return static_cast<int32_t>(offsetof(Particle_t1268635397, ___m_EmitAccumulator1_13)); }
	inline float get_m_EmitAccumulator1_13() const { return ___m_EmitAccumulator1_13; }
	inline float* get_address_of_m_EmitAccumulator1_13() { return &___m_EmitAccumulator1_13; }
	inline void set_m_EmitAccumulator1_13(float value)
	{
		___m_EmitAccumulator1_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLE_T1268635397_H
#ifndef KEYCODE_T94007344_H
#define KEYCODE_T94007344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t94007344 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t94007344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T94007344_H
#ifndef TOUCHTYPE_T706641163_H
#define TOUCHTYPE_T706641163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t706641163 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t706641163, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T706641163_H
#ifndef DIRECTION_T1918642359_H
#define DIRECTION_T1918642359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t1918642359 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1918642359, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1918642359_H
#ifndef MODE_T3803593892_H
#define MODE_T3803593892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t3803593892 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3803593892, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3803593892_H
#ifndef DIRECTION_T1434896290_H
#define DIRECTION_T1434896290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Direction
struct  Direction_t1434896290 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1434896290, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1434896290_H
#ifndef SUBMITEVENT_T1556243238_H
#define SUBMITEVENT_T1556243238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/SubmitEvent
struct  SubmitEvent_t1556243238  : public UnityEvent_1_t3752985527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMITEVENT_T1556243238_H
#ifndef ONCHANGEEVENT_T2953828243_H
#define ONCHANGEEVENT_T2953828243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/OnChangeEvent
struct  OnChangeEvent_t2953828243  : public UnityEvent_1_t3752985527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCHANGEEVENT_T2953828243_H
#ifndef NUMBERSTYLES_T2214297138_H
#define NUMBERSTYLES_T2214297138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberStyles
struct  NumberStyles_t2214297138 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NumberStyles_t2214297138, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERSTYLES_T2214297138_H
#ifndef MATERIALPROPERTYBLOCK_T2709947369_H
#define MATERIALPROPERTYBLOCK_T2709947369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MaterialPropertyBlock
struct  MaterialPropertyBlock_t2709947369  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(MaterialPropertyBlock_t2709947369, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTYBLOCK_T2709947369_H
#ifndef AXIS_T2687941421_H
#define AXIS_T2687941421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Axis
struct  Axis_t2687941421 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t2687941421, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T2687941421_H
#ifndef INPUTBUTTON_T1590693994_H
#define INPUTBUTTON_T1590693994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t1590693994 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t1590693994, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T1590693994_H
#ifndef DRIVENTRANSFORMPROPERTIES_T3932242910_H
#define DRIVENTRANSFORMPROPERTIES_T3932242910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenTransformProperties
struct  DrivenTransformProperties_t3932242910 
{
public:
	// System.Int32 UnityEngine.DrivenTransformProperties::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DrivenTransformProperties_t3932242910, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENTRANSFORMPROPERTIES_T3932242910_H
#ifndef UNITYARMATRIX4X4_T1132406930_H
#define UNITYARMATRIX4X4_T1132406930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_t1132406930 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_t1376926224  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_t1376926224  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_t1376926224  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_t1376926224  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column0_0)); }
	inline Vector4_t1376926224  get_column0_0() const { return ___column0_0; }
	inline Vector4_t1376926224 * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_t1376926224  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column1_1)); }
	inline Vector4_t1376926224  get_column1_1() const { return ___column1_1; }
	inline Vector4_t1376926224 * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_t1376926224  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column2_2)); }
	inline Vector4_t1376926224  get_column2_2() const { return ___column2_2; }
	inline Vector4_t1376926224 * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_t1376926224  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column3_3)); }
	inline Vector4_t1376926224  get_column3_3() const { return ___column3_3; }
	inline Vector4_t1376926224 * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_t1376926224  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_T1132406930_H
#ifndef CANVASUPDATE_T1560734211_H
#define CANVASUPDATE_T1560734211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasUpdate
struct  CanvasUpdate_t1560734211 
{
public:
	// System.Int32 UnityEngine.UI.CanvasUpdate::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CanvasUpdate_t1560734211, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASUPDATE_T1560734211_H
#ifndef BOXSLIDEREVENT_T2026169079_H
#define BOXSLIDEREVENT_T2026169079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct  BoxSliderEvent_t2026169079  : public UnityEvent_2_t1533210863
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDEREVENT_T2026169079_H
#ifndef FILLMETHOD_T46703468_H
#define FILLMETHOD_T46703468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t46703468 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t46703468, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T46703468_H
#ifndef DELEGATE_T69892740_H
#define DELEGATE_T69892740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t69892740  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t421390435 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___data_8)); }
	inline DelegateData_t421390435 * get_data_8() const { return ___data_8; }
	inline DelegateData_t421390435 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t421390435 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T69892740_H
#ifndef RAYCASTRESULT_T1715322097_H
#define RAYCASTRESULT_T1715322097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t1715322097 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t3649338848 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t3590231488 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t596762001  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t596762001  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t59524482  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___m_GameObject_0)); }
	inline GameObject_t3649338848 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t3649338848 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t3649338848 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___module_1)); }
	inline BaseRaycaster_t3590231488 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t3590231488 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t3590231488 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___worldPosition_7)); }
	inline Vector3_t596762001  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t596762001 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t596762001  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___worldNormal_8)); }
	inline Vector3_t596762001  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t596762001 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t596762001  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t1715322097, ___screenPosition_9)); }
	inline Vector2_t59524482  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t59524482 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t59524482  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t1715322097_marshaled_pinvoke
{
	GameObject_t3649338848 * ___m_GameObject_0;
	BaseRaycaster_t3590231488 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t596762001  ___worldPosition_7;
	Vector3_t596762001  ___worldNormal_8;
	Vector2_t59524482  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t1715322097_marshaled_com
{
	GameObject_t3649338848 * ___m_GameObject_0;
	BaseRaycaster_t3590231488 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t596762001  ___worldPosition_7;
	Vector3_t596762001  ___worldNormal_8;
	Vector2_t59524482  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T1715322097_H
#ifndef TOUCHPHASE_T2982654895_H
#define TOUCHPHASE_T2982654895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t2982654895 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t2982654895, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T2982654895_H
#ifndef ARHITTESTRESULTTYPE_T270086150_H
#define ARHITTESTRESULTTYPE_T270086150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResultType
struct  ARHitTestResultType_t270086150 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARHitTestResultType::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARHitTestResultType_t270086150, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARHITTESTRESULTTYPE_T270086150_H
#ifndef SELECTIONSTATE_T2653550480_H
#define SELECTIONSTATE_T2653550480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2653550480 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2653550480, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2653550480_H
#ifndef COLORBLOCK_T625039033_H
#define COLORBLOCK_T625039033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t625039033 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t320819310  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t320819310  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t320819310  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t320819310  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_NormalColor_0)); }
	inline Color_t320819310  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t320819310 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t320819310  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_HighlightedColor_1)); }
	inline Color_t320819310  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t320819310 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t320819310  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_PressedColor_2)); }
	inline Color_t320819310  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t320819310 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t320819310  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_DisabledColor_3)); }
	inline Color_t320819310  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t320819310 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t320819310  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T625039033_H
#ifndef OBJECT_T1008057425_H
#define OBJECT_T1008057425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1008057425  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1008057425, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1008057425_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1008057425_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1008057425_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1008057425_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1008057425_H
#ifndef TRANSITION_T327687789_H
#define TRANSITION_T327687789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t327687789 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t327687789, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T327687789_H
#ifndef TYPE_T125599537_H
#define TYPE_T125599537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t125599537 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t125599537, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T125599537_H
#ifndef DATETIMEKIND_T55135917_H
#define DATETIMEKIND_T55135917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t55135917 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t55135917, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T55135917_H
#ifndef ARTRACKINGSTATEREASON_T2365446872_H
#define ARTRACKINGSTATEREASON_T2365446872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t2365446872 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t2365446872, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T2365446872_H
#ifndef ARTRACKINGSTATE_T3679923289_H
#define ARTRACKINGSTATE_T3679923289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t3679923289 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t3679923289, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T3679923289_H
#ifndef TRACKEDREFERENCE_T3940723406_H
#define TRACKEDREFERENCE_T3940723406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TrackedReference
struct  TrackedReference_t3940723406  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TrackedReference_t3940723406, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_t3940723406_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_t3940723406_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // TRACKEDREFERENCE_T3940723406_H
#ifndef COLORCHANGEDEVENT_T347040188_H
#define COLORCHANGEDEVENT_T347040188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChangedEvent
struct  ColorChangedEvent_t347040188  : public UnityEvent_1_t3808368702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGEDEVENT_T347040188_H
#ifndef LINETYPE_T4134456689_H
#define LINETYPE_T4134456689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/LineType
struct  LineType_t4134456689 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineType_t4134456689, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T4134456689_H
#ifndef HSVCHANGEDEVENT_T3546572410_H
#define HSVCHANGEDEVENT_T3546572410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVChangedEvent
struct  HSVChangedEvent_t3546572410  : public UnityEvent_3_t876514367
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCHANGEDEVENT_T3546572410_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T491524606_H
#define TOUCHSCREENKEYBOARDTYPE_T491524606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t491524606 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t491524606, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T491524606_H
#ifndef SLIDEREVENT_T1040040728_H
#define SLIDEREVENT_T1040040728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/SliderEvent
struct  SliderEvent_t1040040728  : public UnityEvent_1_t2040196808
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDEREVENT_T1040040728_H
#ifndef CONTENTTYPE_T2687439383_H
#define CONTENTTYPE_T2687439383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/ContentType
struct  ContentType_t2687439383 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t2687439383, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T2687439383_H
#ifndef COLORVALUES_T1727074643_H
#define COLORVALUES_T1727074643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorValues
struct  ColorValues_t1727074643 
{
public:
	// System.Int32 ColorValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorValues_t1727074643, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORVALUES_T1727074643_H
#ifndef NOTIMPLEMENTEDEXCEPTION_T4033310348_H
#define NOTIMPLEMENTEDEXCEPTION_T4033310348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_t4033310348  : public SystemException_t500802787
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_T4033310348_H
#ifndef INPUTTYPE_T3397888567_H
#define INPUTTYPE_T3397888567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/InputType
struct  InputType_t3397888567 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t3397888567, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T3397888567_H
#ifndef CHARACTERVALIDATION_T2578987709_H
#define CHARACTERVALIDATION_T2578987709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/CharacterValidation
struct  CharacterValidation_t2578987709 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharacterValidation_t2578987709, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T2578987709_H
#ifndef DATETIME_T1819153659_H
#define DATETIME_T1819153659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t1819153659 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t4158060032  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t1819153659, ___ticks_0)); }
	inline TimeSpan_t4158060032  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t4158060032 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t4158060032  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t1819153659, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t1819153659_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t1819153659  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t1819153659  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1448570014* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1448570014* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1448570014* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1448570014* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1448570014* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1448570014* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1448570014* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t595981822* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t595981822* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___MaxValue_2)); }
	inline DateTime_t1819153659  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t1819153659 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t1819153659  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___MinValue_3)); }
	inline DateTime_t1819153659  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t1819153659 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t1819153659  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1448570014* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1448570014** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1448570014* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1448570014* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1448570014** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1448570014* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1448570014* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1448570014** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1448570014* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1448570014* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1448570014** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1448570014* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1448570014* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1448570014** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1448570014* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1448570014* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1448570014** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1448570014* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1448570014* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1448570014** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1448570014* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t595981822* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t595981822** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t595981822* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t595981822* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t595981822** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t595981822* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t1819153659_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T1819153659_H
#ifndef TEXTURE_T1132728222_H
#define TEXTURE_T1132728222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t1132728222  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T1132728222_H
#ifndef TOUCH_T3132414106_H
#define TOUCH_T3132414106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t3132414106 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t59524482  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t59524482  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t59524482  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Position_1)); }
	inline Vector2_t59524482  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t59524482 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t59524482  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_RawPosition_2)); }
	inline Vector2_t59524482  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t59524482 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t59524482  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_PositionDelta_3)); }
	inline Vector2_t59524482  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t59524482 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t59524482  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t3132414106, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T3132414106_H
#ifndef ARHITTESTRESULT_T1803723679_H
#define ARHITTESTRESULT_T1803723679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResult
struct  ARHitTestResult_t1803723679 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.ARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.ARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::localTransform
	Matrix4x4_t1288378485  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::worldTransform
	Matrix4x4_t1288378485  ___worldTransform_3;
	// System.String UnityEngine.XR.iOS.ARHitTestResult::anchorIdentifier
	String_t* ___anchorIdentifier_4;
	// System.Boolean UnityEngine.XR.iOS.ARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___localTransform_2)); }
	inline Matrix4x4_t1288378485  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t1288378485 * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t1288378485  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___worldTransform_3)); }
	inline Matrix4x4_t1288378485  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t1288378485 * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t1288378485  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchorIdentifier_4() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___anchorIdentifier_4)); }
	inline String_t* get_anchorIdentifier_4() const { return ___anchorIdentifier_4; }
	inline String_t** get_address_of_anchorIdentifier_4() { return &___anchorIdentifier_4; }
	inline void set_anchorIdentifier_4(String_t* value)
	{
		___anchorIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___anchorIdentifier_4), value);
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(ARHitTestResult_t1803723679, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t1803723679_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1288378485  ___localTransform_2;
	Matrix4x4_t1288378485  ___worldTransform_3;
	char* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t1803723679_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1288378485  ___localTransform_2;
	Matrix4x4_t1288378485  ___worldTransform_3;
	Il2CppChar* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
#endif // ARHITTESTRESULT_T1803723679_H
#ifndef UNITYARCAMERA_T2376600594_H
#define UNITYARCAMERA_T2376600594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARCamera
struct  UnityARCamera_t2376600594 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::worldTransform
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.Vector3[] UnityEngine.XR.iOS.UnityARCamera::pointCloudData
	Vector3U5BU5D_t4153953548* ___pointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t1132406930  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t1132406930  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t1132406930  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t1132406930  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_pointCloudData_4() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___pointCloudData_4)); }
	inline Vector3U5BU5D_t4153953548* get_pointCloudData_4() const { return ___pointCloudData_4; }
	inline Vector3U5BU5D_t4153953548** get_address_of_pointCloudData_4() { return &___pointCloudData_4; }
	inline void set_pointCloudData_4(Vector3U5BU5D_t4153953548* value)
	{
		___pointCloudData_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t2376600594_marshaled_pinvoke
{
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t596762001 * ___pointCloudData_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t2376600594_marshaled_com
{
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t596762001 * ___pointCloudData_4;
};
#endif // UNITYARCAMERA_T2376600594_H
#ifndef MATERIAL_T4055262778_H
#define MATERIAL_T4055262778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t4055262778  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T4055262778_H
#ifndef POINTEREVENTDATA_T1563322019_H
#define POINTEREVENTDATA_T1563322019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t1563322019  : public BaseEventData_t341072110
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t3649338848 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t3649338848 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t3649338848 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t3649338848 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t3649338848 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t1715322097  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t1715322097  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t2392691131 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t59524482  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t59524482  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t59524482  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t596762001  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t596762001  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t59524482  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t3649338848 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t3649338848 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t3649338848 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___m_PointerPress_3)); }
	inline GameObject_t3649338848 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t3649338848 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t3649338848 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t3649338848 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t3649338848 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t3649338848 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t3649338848 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t3649338848 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t3649338848 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t3649338848 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t3649338848 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t3649338848 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t1715322097  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t1715322097 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t1715322097  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t1715322097  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t1715322097 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t1715322097  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___hovered_9)); }
	inline List_1_t2392691131 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t2392691131 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t2392691131 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t59524482  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t59524482 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t59524482  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t59524482  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t59524482 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t59524482  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t59524482  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t59524482 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t59524482  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t596762001  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t596762001 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t596762001  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t596762001  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t596762001 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t596762001  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t59524482  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t59524482 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t59524482  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t1563322019, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T1563322019_H
#ifndef ANIMATIONSTATE_T2216918921_H
#define ANIMATIONSTATE_T2216918921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationState
struct  AnimationState_t2216918921  : public TrackedReference_t3940723406
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T2216918921_H
#ifndef NAVIGATION_T2887492880_H
#define NAVIGATION_T2887492880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t2887492880 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3890617260 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3890617260 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3890617260 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3890617260 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_SelectOnUp_1)); }
	inline Selectable_t3890617260 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3890617260 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3890617260 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_SelectOnDown_2)); }
	inline Selectable_t3890617260 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3890617260 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3890617260 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_SelectOnLeft_3)); }
	inline Selectable_t3890617260 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3890617260 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3890617260 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_SelectOnRight_4)); }
	inline Selectable_t3890617260 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3890617260 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3890617260 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t2887492880_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3890617260 * ___m_SelectOnUp_1;
	Selectable_t3890617260 * ___m_SelectOnDown_2;
	Selectable_t3890617260 * ___m_SelectOnLeft_3;
	Selectable_t3890617260 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t2887492880_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3890617260 * ___m_SelectOnUp_1;
	Selectable_t3890617260 * ___m_SelectOnDown_2;
	Selectable_t3890617260 * ___m_SelectOnLeft_3;
	Selectable_t3890617260 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T2887492880_H
#ifndef COMPONENT_T531478471_H
#define COMPONENT_T531478471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t531478471  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T531478471_H
#ifndef GAMEOBJECT_T3649338848_H
#define GAMEOBJECT_T3649338848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t3649338848  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T3649338848_H
#ifndef MULTICASTDELEGATE_T1138444986_H
#define MULTICASTDELEGATE_T1138444986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1138444986  : public Delegate_t69892740
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1138444986 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1138444986 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1138444986, ___prev_9)); }
	inline MulticastDelegate_t1138444986 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1138444986 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1138444986 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1138444986, ___kpm_next_10)); }
	inline MulticastDelegate_t1138444986 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1138444986 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1138444986 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1138444986_H
#ifndef UNITYACTION_2_T3878312970_H
#define UNITYACTION_2_T3878312970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<System.Single,System.Single>
struct  UnityAction_2_t3878312970  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T3878312970_H
#ifndef BEHAVIOUR_T2200997390_H
#define BEHAVIOUR_T2200997390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2200997390  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2200997390_H
#ifndef UNITYACTION_1_T1336681460_H
#define UNITYACTION_1_T1336681460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.String>
struct  UnityAction_1_t1336681460  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1336681460_H
#ifndef UNITYACTION_1_T3918860037_H
#define UNITYACTION_1_T3918860037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Single>
struct  UnityAction_1_t3918860037  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3918860037_H
#ifndef UNITYACTION_3_T3771153942_H
#define UNITYACTION_3_T3771153942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`3<System.Single,System.Single,System.Single>
struct  UnityAction_3_t3771153942  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_3_T3771153942_H
#ifndef UNITYACTION_1_T1392064635_H
#define UNITYACTION_1_T1392064635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct  UnityAction_1_t1392064635  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1392064635_H
#ifndef TEXTURE2D_T2870930912_H
#define TEXTURE2D_T2870930912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t2870930912  : public Texture_t1132728222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T2870930912_H
#ifndef COLLIDER_T1354100743_H
#define COLLIDER_T1354100743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1354100743  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1354100743_H
#ifndef UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#define UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct  UnityARSessionNativeInterface_t3869411053  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::m_NativeARSession
	IntPtr_t ___m_NativeARSession_5;

public:
	inline static int32_t get_offset_of_m_NativeARSession_5() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053, ___m_NativeARSession_5)); }
	inline IntPtr_t get_m_NativeARSession_5() const { return ___m_NativeARSession_5; }
	inline IntPtr_t* get_address_of_m_NativeARSession_5() { return &___m_NativeARSession_5; }
	inline void set_m_NativeARSession_5(IntPtr_t value)
	{
		___m_NativeARSession_5 = value;
	}
};

struct UnityARSessionNativeInterface_t3869411053_StaticFields
{
public:
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFrameUpdatedEvent
	ARFrameUpdate_t3333962149 * ___ARFrameUpdatedEvent_0;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorAddedEvent
	ARAnchorAdded_t1580091102 * ___ARAnchorAddedEvent_1;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorUpdatedEvent
	ARAnchorUpdated_t1492128022 * ___ARAnchorUpdatedEvent_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorRemovedEvent
	ARAnchorRemoved_t3663543047 * ___ARAnchorRemovedEvent_3;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionFailedEvent
	ARSessionFailed_t3599980200 * ___ARSessionFailedEvent_4;
	// UnityEngine.XR.iOS.UnityARCamera UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_Camera
	UnityARCamera_t2376600594  ___s_Camera_6;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_UnityARSessionNativeInterface
	UnityARSessionNativeInterface_t3869411053 * ___s_UnityARSessionNativeInterface_7;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache0
	internal_ARFrameUpdate_t2726389622 * ___U3CU3Ef__mgU24cache0_8;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache1
	internal_ARAnchorAdded_t3791587037 * ___U3CU3Ef__mgU24cache1_9;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache2
	internal_ARAnchorUpdated_t2928809250 * ___U3CU3Ef__mgU24cache2_10;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache3
	internal_ARAnchorRemoved_t409959511 * ___U3CU3Ef__mgU24cache3_11;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache4
	ARSessionFailed_t3599980200 * ___U3CU3Ef__mgU24cache4_12;

public:
	inline static int32_t get_offset_of_ARFrameUpdatedEvent_0() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARFrameUpdatedEvent_0)); }
	inline ARFrameUpdate_t3333962149 * get_ARFrameUpdatedEvent_0() const { return ___ARFrameUpdatedEvent_0; }
	inline ARFrameUpdate_t3333962149 ** get_address_of_ARFrameUpdatedEvent_0() { return &___ARFrameUpdatedEvent_0; }
	inline void set_ARFrameUpdatedEvent_0(ARFrameUpdate_t3333962149 * value)
	{
		___ARFrameUpdatedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ARFrameUpdatedEvent_0), value);
	}

	inline static int32_t get_offset_of_ARAnchorAddedEvent_1() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorAddedEvent_1)); }
	inline ARAnchorAdded_t1580091102 * get_ARAnchorAddedEvent_1() const { return ___ARAnchorAddedEvent_1; }
	inline ARAnchorAdded_t1580091102 ** get_address_of_ARAnchorAddedEvent_1() { return &___ARAnchorAddedEvent_1; }
	inline void set_ARAnchorAddedEvent_1(ARAnchorAdded_t1580091102 * value)
	{
		___ARAnchorAddedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorAddedEvent_1), value);
	}

	inline static int32_t get_offset_of_ARAnchorUpdatedEvent_2() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorUpdatedEvent_2)); }
	inline ARAnchorUpdated_t1492128022 * get_ARAnchorUpdatedEvent_2() const { return ___ARAnchorUpdatedEvent_2; }
	inline ARAnchorUpdated_t1492128022 ** get_address_of_ARAnchorUpdatedEvent_2() { return &___ARAnchorUpdatedEvent_2; }
	inline void set_ARAnchorUpdatedEvent_2(ARAnchorUpdated_t1492128022 * value)
	{
		___ARAnchorUpdatedEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorUpdatedEvent_2), value);
	}

	inline static int32_t get_offset_of_ARAnchorRemovedEvent_3() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorRemovedEvent_3)); }
	inline ARAnchorRemoved_t3663543047 * get_ARAnchorRemovedEvent_3() const { return ___ARAnchorRemovedEvent_3; }
	inline ARAnchorRemoved_t3663543047 ** get_address_of_ARAnchorRemovedEvent_3() { return &___ARAnchorRemovedEvent_3; }
	inline void set_ARAnchorRemovedEvent_3(ARAnchorRemoved_t3663543047 * value)
	{
		___ARAnchorRemovedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorRemovedEvent_3), value);
	}

	inline static int32_t get_offset_of_ARSessionFailedEvent_4() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARSessionFailedEvent_4)); }
	inline ARSessionFailed_t3599980200 * get_ARSessionFailedEvent_4() const { return ___ARSessionFailedEvent_4; }
	inline ARSessionFailed_t3599980200 ** get_address_of_ARSessionFailedEvent_4() { return &___ARSessionFailedEvent_4; }
	inline void set_ARSessionFailedEvent_4(ARSessionFailed_t3599980200 * value)
	{
		___ARSessionFailedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionFailedEvent_4), value);
	}

	inline static int32_t get_offset_of_s_Camera_6() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___s_Camera_6)); }
	inline UnityARCamera_t2376600594  get_s_Camera_6() const { return ___s_Camera_6; }
	inline UnityARCamera_t2376600594 * get_address_of_s_Camera_6() { return &___s_Camera_6; }
	inline void set_s_Camera_6(UnityARCamera_t2376600594  value)
	{
		___s_Camera_6 = value;
	}

	inline static int32_t get_offset_of_s_UnityARSessionNativeInterface_7() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___s_UnityARSessionNativeInterface_7)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_s_UnityARSessionNativeInterface_7() const { return ___s_UnityARSessionNativeInterface_7; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_s_UnityARSessionNativeInterface_7() { return &___s_UnityARSessionNativeInterface_7; }
	inline void set_s_UnityARSessionNativeInterface_7(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___s_UnityARSessionNativeInterface_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnityARSessionNativeInterface_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline internal_ARFrameUpdate_t2726389622 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline internal_ARFrameUpdate_t2726389622 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(internal_ARFrameUpdate_t2726389622 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_9() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache1_9)); }
	inline internal_ARAnchorAdded_t3791587037 * get_U3CU3Ef__mgU24cache1_9() const { return ___U3CU3Ef__mgU24cache1_9; }
	inline internal_ARAnchorAdded_t3791587037 ** get_address_of_U3CU3Ef__mgU24cache1_9() { return &___U3CU3Ef__mgU24cache1_9; }
	inline void set_U3CU3Ef__mgU24cache1_9(internal_ARAnchorAdded_t3791587037 * value)
	{
		___U3CU3Ef__mgU24cache1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_10() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache2_10)); }
	inline internal_ARAnchorUpdated_t2928809250 * get_U3CU3Ef__mgU24cache2_10() const { return ___U3CU3Ef__mgU24cache2_10; }
	inline internal_ARAnchorUpdated_t2928809250 ** get_address_of_U3CU3Ef__mgU24cache2_10() { return &___U3CU3Ef__mgU24cache2_10; }
	inline void set_U3CU3Ef__mgU24cache2_10(internal_ARAnchorUpdated_t2928809250 * value)
	{
		___U3CU3Ef__mgU24cache2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_11() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache3_11)); }
	inline internal_ARAnchorRemoved_t409959511 * get_U3CU3Ef__mgU24cache3_11() const { return ___U3CU3Ef__mgU24cache3_11; }
	inline internal_ARAnchorRemoved_t409959511 ** get_address_of_U3CU3Ef__mgU24cache3_11() { return &___U3CU3Ef__mgU24cache3_11; }
	inline void set_U3CU3Ef__mgU24cache3_11(internal_ARAnchorRemoved_t409959511 * value)
	{
		___U3CU3Ef__mgU24cache3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_12() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache4_12)); }
	inline ARSessionFailed_t3599980200 * get_U3CU3Ef__mgU24cache4_12() const { return ___U3CU3Ef__mgU24cache4_12; }
	inline ARSessionFailed_t3599980200 ** get_address_of_U3CU3Ef__mgU24cache4_12() { return &___U3CU3Ef__mgU24cache4_12; }
	inline void set_U3CU3Ef__mgU24cache4_12(ARSessionFailed_t3599980200 * value)
	{
		___U3CU3Ef__mgU24cache4_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#ifndef TRANSFORM_T3933397867_H
#define TRANSFORM_T3933397867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3933397867  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3933397867_H
#ifndef PARTICLESYSTEM_T1777616458_H
#define PARTICLESYSTEM_T1777616458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t1777616458  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T1777616458_H
#ifndef ENUMERATOR_T1895934899_H
#define ENUMERATOR_T1895934899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>
struct  Enumerator_t1895934899 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t547075962 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ARHitTestResult_t1803723679  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1895934899, ___l_0)); }
	inline List_1_t547075962 * get_l_0() const { return ___l_0; }
	inline List_1_t547075962 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t547075962 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1895934899, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1895934899, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1895934899, ___current_3)); }
	inline ARHitTestResult_t1803723679  get_current_3() const { return ___current_3; }
	inline ARHitTestResult_t1803723679 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ARHitTestResult_t1803723679  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1895934899_H
#ifndef RENDERER_T674913088_H
#define RENDERER_T674913088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t674913088  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T674913088_H
#ifndef ARFRAMEUPDATE_T3333962149_H
#define ARFRAMEUPDATE_T3333962149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct  ARFrameUpdate_t3333962149  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAMEUPDATE_T3333962149_H
#ifndef CAMERA_T226495598_H
#define CAMERA_T226495598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t226495598  : public Behaviour_t2200997390
{
public:

public:
};

struct Camera_t226495598_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t1646417403 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t1646417403 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t1646417403 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t226495598_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t1646417403 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t1646417403 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t1646417403 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t226495598_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t1646417403 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t1646417403 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t1646417403 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t226495598_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t1646417403 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t1646417403 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t1646417403 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T226495598_H
#ifndef MESHRENDERER_T1885265068_H
#define MESHRENDERER_T1885265068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t1885265068  : public Renderer_t674913088
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T1885265068_H
#ifndef RECTTRANSFORM_T1885177139_H
#define RECTTRANSFORM_T1885177139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t1885177139  : public Transform_t3933397867
{
public:

public:
};

struct RectTransform_t1885177139_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t23782843 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t1885177139_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t23782843 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t23782843 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t23782843 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T1885177139_H
#ifndef MONOBEHAVIOUR_T1096588306_H
#define MONOBEHAVIOUR_T1096588306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1096588306  : public Behaviour_t2200997390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1096588306_H
#ifndef ANIMATION_T1557311624_H
#define ANIMATION_T1557311624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation
struct  Animation_t1557311624  : public Behaviour_t2200997390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T1557311624_H
#ifndef LIGHT_T2513018095_H
#define LIGHT_T2513018095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Light
struct  Light_t2513018095  : public Behaviour_t2200997390
{
public:
	// System.Int32 UnityEngine.Light::m_BakedIndex
	int32_t ___m_BakedIndex_2;

public:
	inline static int32_t get_offset_of_m_BakedIndex_2() { return static_cast<int32_t>(offsetof(Light_t2513018095, ___m_BakedIndex_2)); }
	inline int32_t get_m_BakedIndex_2() const { return ___m_BakedIndex_2; }
	inline int32_t* get_address_of_m_BakedIndex_2() { return &___m_BakedIndex_2; }
	inline void set_m_BakedIndex_2(int32_t value)
	{
		___m_BakedIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHT_T2513018095_H
#ifndef BALLZ_T2183783589_H
#define BALLZ_T2183783589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ballz
struct  Ballz_t2183783589  : public MonoBehaviour_t1096588306
{
public:
	// System.Single Ballz::yDistanceThreshold
	float ___yDistanceThreshold_2;
	// System.Single Ballz::startingY
	float ___startingY_3;

public:
	inline static int32_t get_offset_of_yDistanceThreshold_2() { return static_cast<int32_t>(offsetof(Ballz_t2183783589, ___yDistanceThreshold_2)); }
	inline float get_yDistanceThreshold_2() const { return ___yDistanceThreshold_2; }
	inline float* get_address_of_yDistanceThreshold_2() { return &___yDistanceThreshold_2; }
	inline void set_yDistanceThreshold_2(float value)
	{
		___yDistanceThreshold_2 = value;
	}

	inline static int32_t get_offset_of_startingY_3() { return static_cast<int32_t>(offsetof(Ballz_t2183783589, ___startingY_3)); }
	inline float get_startingY_3() const { return ___startingY_3; }
	inline float* get_address_of_startingY_3() { return &___startingY_3; }
	inline void set_startingY_3(float value)
	{
		___startingY_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLZ_T2183783589_H
#ifndef COLORIMAGE_T2737809041_H
#define COLORIMAGE_T2737809041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorImage
struct  ColorImage_t2737809041  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorImage::picker
	ColorPicker_t2582382693 * ___picker_2;
	// UnityEngine.UI.Image ColorImage::image
	Image_t126372159 * ___image_3;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorImage_t2737809041, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(ColorImage_t2737809041, ___image_3)); }
	inline Image_t126372159 * get_image_3() const { return ___image_3; }
	inline Image_t126372159 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t126372159 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMAGE_T2737809041_H
#ifndef COLORPICKERTESTER_T3436045112_H
#define COLORPICKERTESTER_T3436045112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPickerTester
struct  ColorPickerTester_t3436045112  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Renderer ColorPickerTester::renderer
	Renderer_t674913088 * ___renderer_2;
	// ColorPicker ColorPickerTester::picker
	ColorPicker_t2582382693 * ___picker_3;

public:
	inline static int32_t get_offset_of_renderer_2() { return static_cast<int32_t>(offsetof(ColorPickerTester_t3436045112, ___renderer_2)); }
	inline Renderer_t674913088 * get_renderer_2() const { return ___renderer_2; }
	inline Renderer_t674913088 ** get_address_of_renderer_2() { return &___renderer_2; }
	inline void set_renderer_2(Renderer_t674913088 * value)
	{
		___renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_2), value);
	}

	inline static int32_t get_offset_of_picker_3() { return static_cast<int32_t>(offsetof(ColorPickerTester_t3436045112, ___picker_3)); }
	inline ColorPicker_t2582382693 * get_picker_3() const { return ___picker_3; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_3() { return &___picker_3; }
	inline void set_picker_3(ColorPicker_t2582382693 * value)
	{
		___picker_3 = value;
		Il2CppCodeGenWriteBarrier((&___picker_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERTESTER_T3436045112_H
#ifndef COLORPICKER_T2582382693_H
#define COLORPICKER_T2582382693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPicker
struct  ColorPicker_t2582382693  : public MonoBehaviour_t1096588306
{
public:
	// System.Single ColorPicker::_hue
	float ____hue_2;
	// System.Single ColorPicker::_saturation
	float ____saturation_3;
	// System.Single ColorPicker::_brightness
	float ____brightness_4;
	// System.Single ColorPicker::_red
	float ____red_5;
	// System.Single ColorPicker::_green
	float ____green_6;
	// System.Single ColorPicker::_blue
	float ____blue_7;
	// System.Single ColorPicker::_alpha
	float ____alpha_8;
	// ColorChangedEvent ColorPicker::onValueChanged
	ColorChangedEvent_t347040188 * ___onValueChanged_9;
	// HSVChangedEvent ColorPicker::onHSVChanged
	HSVChangedEvent_t3546572410 * ___onHSVChanged_10;

public:
	inline static int32_t get_offset_of__hue_2() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____hue_2)); }
	inline float get__hue_2() const { return ____hue_2; }
	inline float* get_address_of__hue_2() { return &____hue_2; }
	inline void set__hue_2(float value)
	{
		____hue_2 = value;
	}

	inline static int32_t get_offset_of__saturation_3() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____saturation_3)); }
	inline float get__saturation_3() const { return ____saturation_3; }
	inline float* get_address_of__saturation_3() { return &____saturation_3; }
	inline void set__saturation_3(float value)
	{
		____saturation_3 = value;
	}

	inline static int32_t get_offset_of__brightness_4() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____brightness_4)); }
	inline float get__brightness_4() const { return ____brightness_4; }
	inline float* get_address_of__brightness_4() { return &____brightness_4; }
	inline void set__brightness_4(float value)
	{
		____brightness_4 = value;
	}

	inline static int32_t get_offset_of__red_5() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____red_5)); }
	inline float get__red_5() const { return ____red_5; }
	inline float* get_address_of__red_5() { return &____red_5; }
	inline void set__red_5(float value)
	{
		____red_5 = value;
	}

	inline static int32_t get_offset_of__green_6() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____green_6)); }
	inline float get__green_6() const { return ____green_6; }
	inline float* get_address_of__green_6() { return &____green_6; }
	inline void set__green_6(float value)
	{
		____green_6 = value;
	}

	inline static int32_t get_offset_of__blue_7() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____blue_7)); }
	inline float get__blue_7() const { return ____blue_7; }
	inline float* get_address_of__blue_7() { return &____blue_7; }
	inline void set__blue_7(float value)
	{
		____blue_7 = value;
	}

	inline static int32_t get_offset_of__alpha_8() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____alpha_8)); }
	inline float get__alpha_8() const { return ____alpha_8; }
	inline float* get_address_of__alpha_8() { return &____alpha_8; }
	inline void set__alpha_8(float value)
	{
		____alpha_8 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_9() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ___onValueChanged_9)); }
	inline ColorChangedEvent_t347040188 * get_onValueChanged_9() const { return ___onValueChanged_9; }
	inline ColorChangedEvent_t347040188 ** get_address_of_onValueChanged_9() { return &___onValueChanged_9; }
	inline void set_onValueChanged_9(ColorChangedEvent_t347040188 * value)
	{
		___onValueChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_9), value);
	}

	inline static int32_t get_offset_of_onHSVChanged_10() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ___onHSVChanged_10)); }
	inline HSVChangedEvent_t3546572410 * get_onHSVChanged_10() const { return ___onHSVChanged_10; }
	inline HSVChangedEvent_t3546572410 ** get_address_of_onHSVChanged_10() { return &___onHSVChanged_10; }
	inline void set_onHSVChanged_10(HSVChangedEvent_t3546572410 * value)
	{
		___onHSVChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___onHSVChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKER_T2582382693_H
#ifndef BALLMOVER_T4283490589_H
#define BALLMOVER_T4283490589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMover
struct  BallMover_t4283490589  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.GameObject BallMover::collBallPrefab
	GameObject_t3649338848 * ___collBallPrefab_2;
	// UnityEngine.GameObject BallMover::collBallGO
	GameObject_t3649338848 * ___collBallGO_3;

public:
	inline static int32_t get_offset_of_collBallPrefab_2() { return static_cast<int32_t>(offsetof(BallMover_t4283490589, ___collBallPrefab_2)); }
	inline GameObject_t3649338848 * get_collBallPrefab_2() const { return ___collBallPrefab_2; }
	inline GameObject_t3649338848 ** get_address_of_collBallPrefab_2() { return &___collBallPrefab_2; }
	inline void set_collBallPrefab_2(GameObject_t3649338848 * value)
	{
		___collBallPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___collBallPrefab_2), value);
	}

	inline static int32_t get_offset_of_collBallGO_3() { return static_cast<int32_t>(offsetof(BallMover_t4283490589, ___collBallGO_3)); }
	inline GameObject_t3649338848 * get_collBallGO_3() const { return ___collBallGO_3; }
	inline GameObject_t3649338848 ** get_address_of_collBallGO_3() { return &___collBallGO_3; }
	inline void set_collBallGO_3(GameObject_t3649338848 * value)
	{
		___collBallGO_3 = value;
		Il2CppCodeGenWriteBarrier((&___collBallGO_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMOVER_T4283490589_H
#ifndef COLORLABEL_T538373442_H
#define COLORLABEL_T538373442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorLabel
struct  ColorLabel_t538373442  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorLabel::picker
	ColorPicker_t2582382693 * ___picker_2;
	// ColorValues ColorLabel::type
	int32_t ___type_3;
	// System.String ColorLabel::prefix
	String_t* ___prefix_4;
	// System.Single ColorLabel::minValue
	float ___minValue_5;
	// System.Single ColorLabel::maxValue
	float ___maxValue_6;
	// System.Int32 ColorLabel::precision
	int32_t ___precision_7;
	// UnityEngine.UI.Text ColorLabel::label
	Text_t3069741234 * ___label_8;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_prefix_4() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___prefix_4)); }
	inline String_t* get_prefix_4() const { return ___prefix_4; }
	inline String_t** get_address_of_prefix_4() { return &___prefix_4; }
	inline void set_prefix_4(String_t* value)
	{
		___prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_4), value);
	}

	inline static int32_t get_offset_of_minValue_5() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___minValue_5)); }
	inline float get_minValue_5() const { return ___minValue_5; }
	inline float* get_address_of_minValue_5() { return &___minValue_5; }
	inline void set_minValue_5(float value)
	{
		___minValue_5 = value;
	}

	inline static int32_t get_offset_of_maxValue_6() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___maxValue_6)); }
	inline float get_maxValue_6() const { return ___maxValue_6; }
	inline float* get_address_of_maxValue_6() { return &___maxValue_6; }
	inline void set_maxValue_6(float value)
	{
		___maxValue_6 = value;
	}

	inline static int32_t get_offset_of_precision_7() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___precision_7)); }
	inline int32_t get_precision_7() const { return ___precision_7; }
	inline int32_t* get_address_of_precision_7() { return &___precision_7; }
	inline void set_precision_7(int32_t value)
	{
		___precision_7 = value;
	}

	inline static int32_t get_offset_of_label_8() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___label_8)); }
	inline Text_t3069741234 * get_label_8() const { return ___label_8; }
	inline Text_t3069741234 ** get_address_of_label_8() { return &___label_8; }
	inline void set_label_8(Text_t3069741234 * value)
	{
		___label_8 = value;
		Il2CppCodeGenWriteBarrier((&___label_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLABEL_T538373442_H
#ifndef BALLMAKER_T1786680372_H
#define BALLMAKER_T1786680372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMaker
struct  BallMaker_t1786680372  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.GameObject BallMaker::ballPrefab
	GameObject_t3649338848 * ___ballPrefab_2;
	// System.Single BallMaker::createHeight
	float ___createHeight_3;
	// UnityEngine.MaterialPropertyBlock BallMaker::props
	MaterialPropertyBlock_t2709947369 * ___props_4;

public:
	inline static int32_t get_offset_of_ballPrefab_2() { return static_cast<int32_t>(offsetof(BallMaker_t1786680372, ___ballPrefab_2)); }
	inline GameObject_t3649338848 * get_ballPrefab_2() const { return ___ballPrefab_2; }
	inline GameObject_t3649338848 ** get_address_of_ballPrefab_2() { return &___ballPrefab_2; }
	inline void set_ballPrefab_2(GameObject_t3649338848 * value)
	{
		___ballPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballPrefab_2), value);
	}

	inline static int32_t get_offset_of_createHeight_3() { return static_cast<int32_t>(offsetof(BallMaker_t1786680372, ___createHeight_3)); }
	inline float get_createHeight_3() const { return ___createHeight_3; }
	inline float* get_address_of_createHeight_3() { return &___createHeight_3; }
	inline void set_createHeight_3(float value)
	{
		___createHeight_3 = value;
	}

	inline static int32_t get_offset_of_props_4() { return static_cast<int32_t>(offsetof(BallMaker_t1786680372, ___props_4)); }
	inline MaterialPropertyBlock_t2709947369 * get_props_4() const { return ___props_4; }
	inline MaterialPropertyBlock_t2709947369 ** get_address_of_props_4() { return &___props_4; }
	inline void set_props_4(MaterialPropertyBlock_t2709947369 * value)
	{
		___props_4 = value;
		Il2CppCodeGenWriteBarrier((&___props_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMAKER_T1786680372_H
#ifndef COLORPRESETS_T321881905_H
#define COLORPRESETS_T321881905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPresets
struct  ColorPresets_t321881905  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorPresets::picker
	ColorPicker_t2582382693 * ___picker_2;
	// UnityEngine.GameObject[] ColorPresets::presets
	GameObjectU5BU5D_t1488940705* ___presets_3;
	// UnityEngine.UI.Image ColorPresets::createPresetImage
	Image_t126372159 * ___createPresetImage_4;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorPresets_t321881905, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_presets_3() { return static_cast<int32_t>(offsetof(ColorPresets_t321881905, ___presets_3)); }
	inline GameObjectU5BU5D_t1488940705* get_presets_3() const { return ___presets_3; }
	inline GameObjectU5BU5D_t1488940705** get_address_of_presets_3() { return &___presets_3; }
	inline void set_presets_3(GameObjectU5BU5D_t1488940705* value)
	{
		___presets_3 = value;
		Il2CppCodeGenWriteBarrier((&___presets_3), value);
	}

	inline static int32_t get_offset_of_createPresetImage_4() { return static_cast<int32_t>(offsetof(ColorPresets_t321881905, ___createPresetImage_4)); }
	inline Image_t126372159 * get_createPresetImage_4() const { return ___createPresetImage_4; }
	inline Image_t126372159 ** get_address_of_createPresetImage_4() { return &___createPresetImage_4; }
	inline void set_createPresetImage_4(Image_t126372159 * value)
	{
		___createPresetImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___createPresetImage_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPRESETS_T321881905_H
#ifndef SVBOXSLIDER_T2769455468_H
#define SVBOXSLIDER_T2769455468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVBoxSlider
struct  SVBoxSlider_t2769455468  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker SVBoxSlider::picker
	ColorPicker_t2582382693 * ___picker_2;
	// UnityEngine.UI.BoxSlider SVBoxSlider::slider
	BoxSlider_t2780260910 * ___slider_3;
	// UnityEngine.UI.RawImage SVBoxSlider::image
	RawImage_t3037409423 * ___image_4;
	// System.Single SVBoxSlider::lastH
	float ___lastH_5;
	// System.Boolean SVBoxSlider::listen
	bool ___listen_6;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___slider_3)); }
	inline BoxSlider_t2780260910 * get_slider_3() const { return ___slider_3; }
	inline BoxSlider_t2780260910 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(BoxSlider_t2780260910 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier((&___slider_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___image_4)); }
	inline RawImage_t3037409423 * get_image_4() const { return ___image_4; }
	inline RawImage_t3037409423 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(RawImage_t3037409423 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_lastH_5() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___lastH_5)); }
	inline float get_lastH_5() const { return ___lastH_5; }
	inline float* get_address_of_lastH_5() { return &___lastH_5; }
	inline void set_lastH_5(float value)
	{
		___lastH_5 = value;
	}

	inline static int32_t get_offset_of_listen_6() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___listen_6)); }
	inline bool get_listen_6() const { return ___listen_6; }
	inline bool* get_address_of_listen_6() { return &___listen_6; }
	inline void set_listen_6(bool value)
	{
		___listen_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVBOXSLIDER_T2769455468_H
#ifndef UNITYARAMBIENT_T3389999931_H
#define UNITYARAMBIENT_T3389999931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAmbient
struct  UnityARAmbient_t3389999931  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Light UnityEngine.XR.iOS.UnityARAmbient::l
	Light_t2513018095 * ___l_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARAmbient::m_Session
	UnityARSessionNativeInterface_t3869411053 * ___m_Session_3;

public:
	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(UnityARAmbient_t3389999931, ___l_2)); }
	inline Light_t2513018095 * get_l_2() const { return ___l_2; }
	inline Light_t2513018095 ** get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(Light_t2513018095 * value)
	{
		___l_2 = value;
		Il2CppCodeGenWriteBarrier((&___l_2), value);
	}

	inline static int32_t get_offset_of_m_Session_3() { return static_cast<int32_t>(offsetof(UnityARAmbient_t3389999931, ___m_Session_3)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_m_Session_3() const { return ___m_Session_3; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_m_Session_3() { return &___m_Session_3; }
	inline void set_m_Session_3(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___m_Session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARAMBIENT_T3389999931_H
#ifndef SLIMECONTROL_T1232093680_H
#define SLIMECONTROL_T1232093680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SlimeControl
struct  SlimeControl_t1232093680  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Animation SlimeControl::animation
	Animation_t1557311624 * ___animation_2;
	// System.Boolean SlimeControl::shouldMove
	bool ___shouldMove_3;
	// System.Boolean SlimeControl::SlimeDeath
	bool ___SlimeDeath_4;

public:
	inline static int32_t get_offset_of_animation_2() { return static_cast<int32_t>(offsetof(SlimeControl_t1232093680, ___animation_2)); }
	inline Animation_t1557311624 * get_animation_2() const { return ___animation_2; }
	inline Animation_t1557311624 ** get_address_of_animation_2() { return &___animation_2; }
	inline void set_animation_2(Animation_t1557311624 * value)
	{
		___animation_2 = value;
		Il2CppCodeGenWriteBarrier((&___animation_2), value);
	}

	inline static int32_t get_offset_of_shouldMove_3() { return static_cast<int32_t>(offsetof(SlimeControl_t1232093680, ___shouldMove_3)); }
	inline bool get_shouldMove_3() const { return ___shouldMove_3; }
	inline bool* get_address_of_shouldMove_3() { return &___shouldMove_3; }
	inline void set_shouldMove_3(bool value)
	{
		___shouldMove_3 = value;
	}

	inline static int32_t get_offset_of_SlimeDeath_4() { return static_cast<int32_t>(offsetof(SlimeControl_t1232093680, ___SlimeDeath_4)); }
	inline bool get_SlimeDeath_4() const { return ___SlimeDeath_4; }
	inline bool* get_address_of_SlimeDeath_4() { return &___SlimeDeath_4; }
	inline void set_SlimeDeath_4(bool value)
	{
		___SlimeDeath_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIMECONTROL_T1232093680_H
#ifndef SCREENSHOT_T707795496_H
#define SCREENSHOT_T707795496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenShot
struct  ScreenShot_t707795496  : public MonoBehaviour_t1096588306
{
public:
	// System.String ScreenShot::format
	String_t* ___format_2;

public:
	inline static int32_t get_offset_of_format_2() { return static_cast<int32_t>(offsetof(ScreenShot_t707795496, ___format_2)); }
	inline String_t* get_format_2() const { return ___format_2; }
	inline String_t** get_address_of_format_2() { return &___format_2; }
	inline void set_format_2(String_t* value)
	{
		___format_2 = value;
		Il2CppCodeGenWriteBarrier((&___format_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOT_T707795496_H
#ifndef TILTWINDOW_T1431928114_H
#define TILTWINDOW_T1431928114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltWindow
struct  TiltWindow_t1431928114  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Vector2 TiltWindow::range
	Vector2_t59524482  ___range_2;
	// UnityEngine.Transform TiltWindow::mTrans
	Transform_t3933397867 * ___mTrans_3;
	// UnityEngine.Quaternion TiltWindow::mStart
	Quaternion_t3165733013  ___mStart_4;
	// UnityEngine.Vector2 TiltWindow::mRot
	Vector2_t59524482  ___mRot_5;

public:
	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(TiltWindow_t1431928114, ___range_2)); }
	inline Vector2_t59524482  get_range_2() const { return ___range_2; }
	inline Vector2_t59524482 * get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(Vector2_t59524482  value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(TiltWindow_t1431928114, ___mTrans_3)); }
	inline Transform_t3933397867 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3933397867 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3933397867 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mStart_4() { return static_cast<int32_t>(offsetof(TiltWindow_t1431928114, ___mStart_4)); }
	inline Quaternion_t3165733013  get_mStart_4() const { return ___mStart_4; }
	inline Quaternion_t3165733013 * get_address_of_mStart_4() { return &___mStart_4; }
	inline void set_mStart_4(Quaternion_t3165733013  value)
	{
		___mStart_4 = value;
	}

	inline static int32_t get_offset_of_mRot_5() { return static_cast<int32_t>(offsetof(TiltWindow_t1431928114, ___mRot_5)); }
	inline Vector2_t59524482  get_mRot_5() const { return ___mRot_5; }
	inline Vector2_t59524482 * get_address_of_mRot_5() { return &___mRot_5; }
	inline void set_mRot_5(Vector2_t59524482  value)
	{
		___mRot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T1431928114_H
#ifndef UIBEHAVIOUR_T2441083007_H
#define UIBEHAVIOUR_T2441083007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t2441083007  : public MonoBehaviour_t1096588306
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T2441083007_H
#ifndef PARTICLEPAINTER_T294017160_H
#define PARTICLEPAINTER_T294017160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlePainter
struct  ParticlePainter_t294017160  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.ParticleSystem ParticlePainter::painterParticlePrefab
	ParticleSystem_t1777616458 * ___painterParticlePrefab_2;
	// System.Single ParticlePainter::minDistanceThreshold
	float ___minDistanceThreshold_3;
	// System.Single ParticlePainter::maxDistanceThreshold
	float ___maxDistanceThreshold_4;
	// System.Boolean ParticlePainter::frameUpdated
	bool ___frameUpdated_5;
	// System.Single ParticlePainter::particleSize
	float ___particleSize_6;
	// System.Single ParticlePainter::penDistance
	float ___penDistance_7;
	// ColorPicker ParticlePainter::colorPicker
	ColorPicker_t2582382693 * ___colorPicker_8;
	// UnityEngine.ParticleSystem ParticlePainter::currentPS
	ParticleSystem_t1777616458 * ___currentPS_9;
	// UnityEngine.ParticleSystem/Particle[] ParticlePainter::particles
	ParticleU5BU5D_t419044168* ___particles_10;
	// UnityEngine.Vector3 ParticlePainter::previousPosition
	Vector3_t596762001  ___previousPosition_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ParticlePainter::currentPaintVertices
	List_1_t3635081580 * ___currentPaintVertices_12;
	// UnityEngine.Color ParticlePainter::currentColor
	Color_t320819310  ___currentColor_13;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> ParticlePainter::paintSystems
	List_1_t520968741 * ___paintSystems_14;
	// System.Int32 ParticlePainter::paintMode
	int32_t ___paintMode_15;

public:
	inline static int32_t get_offset_of_painterParticlePrefab_2() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___painterParticlePrefab_2)); }
	inline ParticleSystem_t1777616458 * get_painterParticlePrefab_2() const { return ___painterParticlePrefab_2; }
	inline ParticleSystem_t1777616458 ** get_address_of_painterParticlePrefab_2() { return &___painterParticlePrefab_2; }
	inline void set_painterParticlePrefab_2(ParticleSystem_t1777616458 * value)
	{
		___painterParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___painterParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_minDistanceThreshold_3() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___minDistanceThreshold_3)); }
	inline float get_minDistanceThreshold_3() const { return ___minDistanceThreshold_3; }
	inline float* get_address_of_minDistanceThreshold_3() { return &___minDistanceThreshold_3; }
	inline void set_minDistanceThreshold_3(float value)
	{
		___minDistanceThreshold_3 = value;
	}

	inline static int32_t get_offset_of_maxDistanceThreshold_4() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___maxDistanceThreshold_4)); }
	inline float get_maxDistanceThreshold_4() const { return ___maxDistanceThreshold_4; }
	inline float* get_address_of_maxDistanceThreshold_4() { return &___maxDistanceThreshold_4; }
	inline void set_maxDistanceThreshold_4(float value)
	{
		___maxDistanceThreshold_4 = value;
	}

	inline static int32_t get_offset_of_frameUpdated_5() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___frameUpdated_5)); }
	inline bool get_frameUpdated_5() const { return ___frameUpdated_5; }
	inline bool* get_address_of_frameUpdated_5() { return &___frameUpdated_5; }
	inline void set_frameUpdated_5(bool value)
	{
		___frameUpdated_5 = value;
	}

	inline static int32_t get_offset_of_particleSize_6() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___particleSize_6)); }
	inline float get_particleSize_6() const { return ___particleSize_6; }
	inline float* get_address_of_particleSize_6() { return &___particleSize_6; }
	inline void set_particleSize_6(float value)
	{
		___particleSize_6 = value;
	}

	inline static int32_t get_offset_of_penDistance_7() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___penDistance_7)); }
	inline float get_penDistance_7() const { return ___penDistance_7; }
	inline float* get_address_of_penDistance_7() { return &___penDistance_7; }
	inline void set_penDistance_7(float value)
	{
		___penDistance_7 = value;
	}

	inline static int32_t get_offset_of_colorPicker_8() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___colorPicker_8)); }
	inline ColorPicker_t2582382693 * get_colorPicker_8() const { return ___colorPicker_8; }
	inline ColorPicker_t2582382693 ** get_address_of_colorPicker_8() { return &___colorPicker_8; }
	inline void set_colorPicker_8(ColorPicker_t2582382693 * value)
	{
		___colorPicker_8 = value;
		Il2CppCodeGenWriteBarrier((&___colorPicker_8), value);
	}

	inline static int32_t get_offset_of_currentPS_9() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___currentPS_9)); }
	inline ParticleSystem_t1777616458 * get_currentPS_9() const { return ___currentPS_9; }
	inline ParticleSystem_t1777616458 ** get_address_of_currentPS_9() { return &___currentPS_9; }
	inline void set_currentPS_9(ParticleSystem_t1777616458 * value)
	{
		___currentPS_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_9), value);
	}

	inline static int32_t get_offset_of_particles_10() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___particles_10)); }
	inline ParticleU5BU5D_t419044168* get_particles_10() const { return ___particles_10; }
	inline ParticleU5BU5D_t419044168** get_address_of_particles_10() { return &___particles_10; }
	inline void set_particles_10(ParticleU5BU5D_t419044168* value)
	{
		___particles_10 = value;
		Il2CppCodeGenWriteBarrier((&___particles_10), value);
	}

	inline static int32_t get_offset_of_previousPosition_11() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___previousPosition_11)); }
	inline Vector3_t596762001  get_previousPosition_11() const { return ___previousPosition_11; }
	inline Vector3_t596762001 * get_address_of_previousPosition_11() { return &___previousPosition_11; }
	inline void set_previousPosition_11(Vector3_t596762001  value)
	{
		___previousPosition_11 = value;
	}

	inline static int32_t get_offset_of_currentPaintVertices_12() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___currentPaintVertices_12)); }
	inline List_1_t3635081580 * get_currentPaintVertices_12() const { return ___currentPaintVertices_12; }
	inline List_1_t3635081580 ** get_address_of_currentPaintVertices_12() { return &___currentPaintVertices_12; }
	inline void set_currentPaintVertices_12(List_1_t3635081580 * value)
	{
		___currentPaintVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentPaintVertices_12), value);
	}

	inline static int32_t get_offset_of_currentColor_13() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___currentColor_13)); }
	inline Color_t320819310  get_currentColor_13() const { return ___currentColor_13; }
	inline Color_t320819310 * get_address_of_currentColor_13() { return &___currentColor_13; }
	inline void set_currentColor_13(Color_t320819310  value)
	{
		___currentColor_13 = value;
	}

	inline static int32_t get_offset_of_paintSystems_14() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___paintSystems_14)); }
	inline List_1_t520968741 * get_paintSystems_14() const { return ___paintSystems_14; }
	inline List_1_t520968741 ** get_address_of_paintSystems_14() { return &___paintSystems_14; }
	inline void set_paintSystems_14(List_1_t520968741 * value)
	{
		___paintSystems_14 = value;
		Il2CppCodeGenWriteBarrier((&___paintSystems_14), value);
	}

	inline static int32_t get_offset_of_paintMode_15() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___paintMode_15)); }
	inline int32_t get_paintMode_15() const { return ___paintMode_15; }
	inline int32_t* get_address_of_paintMode_15() { return &___paintMode_15; }
	inline void set_paintMode_15(int32_t value)
	{
		___paintMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPAINTER_T294017160_H
#ifndef MODESWITCHER_T833699841_H
#define MODESWITCHER_T833699841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModeSwitcher
struct  ModeSwitcher_t833699841  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.GameObject ModeSwitcher::ballMake
	GameObject_t3649338848 * ___ballMake_2;
	// UnityEngine.GameObject ModeSwitcher::ballMove
	GameObject_t3649338848 * ___ballMove_3;
	// System.Int32 ModeSwitcher::appMode
	int32_t ___appMode_4;

public:
	inline static int32_t get_offset_of_ballMake_2() { return static_cast<int32_t>(offsetof(ModeSwitcher_t833699841, ___ballMake_2)); }
	inline GameObject_t3649338848 * get_ballMake_2() const { return ___ballMake_2; }
	inline GameObject_t3649338848 ** get_address_of_ballMake_2() { return &___ballMake_2; }
	inline void set_ballMake_2(GameObject_t3649338848 * value)
	{
		___ballMake_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballMake_2), value);
	}

	inline static int32_t get_offset_of_ballMove_3() { return static_cast<int32_t>(offsetof(ModeSwitcher_t833699841, ___ballMove_3)); }
	inline GameObject_t3649338848 * get_ballMove_3() const { return ___ballMove_3; }
	inline GameObject_t3649338848 ** get_address_of_ballMove_3() { return &___ballMove_3; }
	inline void set_ballMove_3(GameObject_t3649338848 * value)
	{
		___ballMove_3 = value;
		Il2CppCodeGenWriteBarrier((&___ballMove_3), value);
	}

	inline static int32_t get_offset_of_appMode_4() { return static_cast<int32_t>(offsetof(ModeSwitcher_t833699841, ___appMode_4)); }
	inline int32_t get_appMode_4() const { return ___appMode_4; }
	inline int32_t* get_address_of_appMode_4() { return &___appMode_4; }
	inline void set_appMode_4(int32_t value)
	{
		___appMode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODESWITCHER_T833699841_H
#ifndef COLORSLIDER_T4061812907_H
#define COLORSLIDER_T4061812907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSlider
struct  ColorSlider_t4061812907  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorSlider::hsvpicker
	ColorPicker_t2582382693 * ___hsvpicker_2;
	// ColorValues ColorSlider::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider ColorSlider::slider
	Slider_t301630380 * ___slider_4;
	// System.Boolean ColorSlider::listen
	bool ___listen_5;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(ColorSlider_t4061812907, ___hsvpicker_2)); }
	inline ColorPicker_t2582382693 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t2582382693 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSlider_t4061812907, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(ColorSlider_t4061812907, ___slider_4)); }
	inline Slider_t301630380 * get_slider_4() const { return ___slider_4; }
	inline Slider_t301630380 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t301630380 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_listen_5() { return static_cast<int32_t>(offsetof(ColorSlider_t4061812907, ___listen_5)); }
	inline bool get_listen_5() const { return ___listen_5; }
	inline bool* get_address_of_listen_5() { return &___listen_5; }
	inline void set_listen_5(bool value)
	{
		___listen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDER_T4061812907_H
#ifndef KNIGHTCONTROL_T1902388948_H
#define KNIGHTCONTROL_T1902388948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KnightControl
struct  KnightControl_t1902388948  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Animation KnightControl::animation
	Animation_t1557311624 * ___animation_2;
	// System.Boolean KnightControl::detectedMonster
	bool ___detectedMonster_3;
	// System.Boolean KnightControl::moveForward
	bool ___moveForward_4;
	// System.Boolean KnightControl::moveReverse
	bool ___moveReverse_5;
	// System.Boolean KnightControl::KnightDeath
	bool ___KnightDeath_6;

public:
	inline static int32_t get_offset_of_animation_2() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___animation_2)); }
	inline Animation_t1557311624 * get_animation_2() const { return ___animation_2; }
	inline Animation_t1557311624 ** get_address_of_animation_2() { return &___animation_2; }
	inline void set_animation_2(Animation_t1557311624 * value)
	{
		___animation_2 = value;
		Il2CppCodeGenWriteBarrier((&___animation_2), value);
	}

	inline static int32_t get_offset_of_detectedMonster_3() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___detectedMonster_3)); }
	inline bool get_detectedMonster_3() const { return ___detectedMonster_3; }
	inline bool* get_address_of_detectedMonster_3() { return &___detectedMonster_3; }
	inline void set_detectedMonster_3(bool value)
	{
		___detectedMonster_3 = value;
	}

	inline static int32_t get_offset_of_moveForward_4() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___moveForward_4)); }
	inline bool get_moveForward_4() const { return ___moveForward_4; }
	inline bool* get_address_of_moveForward_4() { return &___moveForward_4; }
	inline void set_moveForward_4(bool value)
	{
		___moveForward_4 = value;
	}

	inline static int32_t get_offset_of_moveReverse_5() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___moveReverse_5)); }
	inline bool get_moveReverse_5() const { return ___moveReverse_5; }
	inline bool* get_address_of_moveReverse_5() { return &___moveReverse_5; }
	inline void set_moveReverse_5(bool value)
	{
		___moveReverse_5 = value;
	}

	inline static int32_t get_offset_of_KnightDeath_6() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___KnightDeath_6)); }
	inline bool get_KnightDeath_6() const { return ___KnightDeath_6; }
	inline bool* get_address_of_KnightDeath_6() { return &___KnightDeath_6; }
	inline void set_KnightDeath_6(bool value)
	{
		___KnightDeath_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNIGHTCONTROL_T1902388948_H
#ifndef COLORSLIDERIMAGE_T664667956_H
#define COLORSLIDERIMAGE_T664667956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSliderImage
struct  ColorSliderImage_t664667956  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorSliderImage::picker
	ColorPicker_t2582382693 * ___picker_2;
	// ColorValues ColorSliderImage::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider/Direction ColorSliderImage::direction
	int32_t ___direction_4;
	// UnityEngine.UI.RawImage ColorSliderImage::image
	RawImage_t3037409423 * ___image_5;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorSliderImage_t664667956, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSliderImage_t664667956, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(ColorSliderImage_t664667956, ___direction_4)); }
	inline int32_t get_direction_4() const { return ___direction_4; }
	inline int32_t* get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(int32_t value)
	{
		___direction_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ColorSliderImage_t664667956, ___image_5)); }
	inline RawImage_t3037409423 * get_image_5() const { return ___image_5; }
	inline RawImage_t3037409423 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t3037409423 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDERIMAGE_T664667956_H
#ifndef HEXCOLORFIELD_T1392034802_H
#define HEXCOLORFIELD_T1392034802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HexColorField
struct  HexColorField_t1392034802  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker HexColorField::hsvpicker
	ColorPicker_t2582382693 * ___hsvpicker_2;
	// System.Boolean HexColorField::displayAlpha
	bool ___displayAlpha_3;
	// UnityEngine.UI.InputField HexColorField::hexInputField
	InputField_t3161868630 * ___hexInputField_4;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(HexColorField_t1392034802, ___hsvpicker_2)); }
	inline ColorPicker_t2582382693 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t2582382693 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_displayAlpha_3() { return static_cast<int32_t>(offsetof(HexColorField_t1392034802, ___displayAlpha_3)); }
	inline bool get_displayAlpha_3() const { return ___displayAlpha_3; }
	inline bool* get_address_of_displayAlpha_3() { return &___displayAlpha_3; }
	inline void set_displayAlpha_3(bool value)
	{
		___displayAlpha_3 = value;
	}

	inline static int32_t get_offset_of_hexInputField_4() { return static_cast<int32_t>(offsetof(HexColorField_t1392034802, ___hexInputField_4)); }
	inline InputField_t3161868630 * get_hexInputField_4() const { return ___hexInputField_4; }
	inline InputField_t3161868630 ** get_address_of_hexInputField_4() { return &___hexInputField_4; }
	inline void set_hexInputField_4(InputField_t3161868630 * value)
	{
		___hexInputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___hexInputField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXCOLORFIELD_T1392034802_H
#ifndef GRAPHIC_T1406460313_H
#define GRAPHIC_T1406460313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1406460313  : public UIBehaviour_t2441083007
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t4055262778 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t320819310  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t1885177139 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t799251060 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3680465181 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t2037703605 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t2037703605 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t2037703605 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t4135605540 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_Material_4)); }
	inline Material_t4055262778 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t4055262778 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t4055262778 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_Color_5)); }
	inline Color_t320819310  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t320819310 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t320819310  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_RectTransform_7)); }
	inline RectTransform_t1885177139 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t1885177139 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t1885177139 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t799251060 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t799251060 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t799251060 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_Canvas_9)); }
	inline Canvas_t3680465181 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3680465181 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3680465181 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t2037703605 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t2037703605 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t2037703605 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t2037703605 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t2037703605 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t2037703605 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t2037703605 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t2037703605 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t2037703605 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t4135605540 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t4135605540 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t4135605540 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1406460313, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1406460313_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t4055262778 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t2870930912 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t540144243 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t1674489302 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1406460313_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t4055262778 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t4055262778 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t4055262778 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1406460313_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t2870930912 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t2870930912 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t2870930912 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1406460313_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t540144243 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t540144243 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t540144243 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1406460313_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t1674489302 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t1674489302 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t1674489302 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1406460313_H
#ifndef SELECTABLE_T3890617260_H
#define SELECTABLE_T3890617260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3890617260  : public UIBehaviour_t2441083007
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t2887492880  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t625039033  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t758977253  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2976076601 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1406460313 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1819317824 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_Navigation_3)); }
	inline Navigation_t2887492880  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t2887492880 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t2887492880  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_Colors_5)); }
	inline ColorBlock_t625039033  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t625039033 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t625039033  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_SpriteState_6)); }
	inline SpriteState_t758977253  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t758977253 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t758977253  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2976076601 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2976076601 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2976076601 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_TargetGraphic_9)); }
	inline Graphic_t1406460313 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1406460313 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1406460313 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_CanvasGroupCache_15)); }
	inline List_1_t1819317824 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1819317824 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1819317824 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3890617260_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t2633969543 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3890617260_StaticFields, ___s_List_2)); }
	inline List_1_t2633969543 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t2633969543 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t2633969543 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3890617260_H
#ifndef INPUTFIELD_T3161868630_H
#define INPUTFIELD_T3161868630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField
struct  InputField_t3161868630  : public Selectable_t3890617260
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_t2754790973 * ___m_Keyboard_16;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_t3069741234 * ___m_TextComponent_18;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_t1406460313 * ___m_Placeholder_19;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_20;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_21;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_22;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_23;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_24;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_25;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_26;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_27;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnEndEdit
	SubmitEvent_t1556243238 * ___m_OnEndEdit_28;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_t2953828243 * ___m_OnValueChanged_29;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t4262405814 * ___m_OnValidateInput_30;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_t320819310  ___m_CaretColor_31;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_32;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_t320819310  ___m_SelectionColor_33;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_34;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_35;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_36;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_37;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_38;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_39;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t1885177139 * ___caretRectTrans_40;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_t3551258791* ___m_CursorVerts_41;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t3308673789 * ___m_InputTextCache_42;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_t799251060 * ___m_CachedInputRenderer_43;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_44;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t540144243 * ___m_Mesh_45;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_46;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_47;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_48;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_49;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_52;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t3555871518 * ___m_BlinkCoroutine_53;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_54;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_55;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_56;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t3555871518 * ___m_DragCoroutine_57;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_58;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_59;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_60;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_t3603750770 * ___m_ProcessingEvent_62;

public:
	inline static int32_t get_offset_of_m_Keyboard_16() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_Keyboard_16)); }
	inline TouchScreenKeyboard_t2754790973 * get_m_Keyboard_16() const { return ___m_Keyboard_16; }
	inline TouchScreenKeyboard_t2754790973 ** get_address_of_m_Keyboard_16() { return &___m_Keyboard_16; }
	inline void set_m_Keyboard_16(TouchScreenKeyboard_t2754790973 * value)
	{
		___m_Keyboard_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_16), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_18() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_TextComponent_18)); }
	inline Text_t3069741234 * get_m_TextComponent_18() const { return ___m_TextComponent_18; }
	inline Text_t3069741234 ** get_address_of_m_TextComponent_18() { return &___m_TextComponent_18; }
	inline void set_m_TextComponent_18(Text_t3069741234 * value)
	{
		___m_TextComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_18), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_19() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_Placeholder_19)); }
	inline Graphic_t1406460313 * get_m_Placeholder_19() const { return ___m_Placeholder_19; }
	inline Graphic_t1406460313 ** get_address_of_m_Placeholder_19() { return &___m_Placeholder_19; }
	inline void set_m_Placeholder_19(Graphic_t1406460313 * value)
	{
		___m_Placeholder_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_19), value);
	}

	inline static int32_t get_offset_of_m_ContentType_20() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_ContentType_20)); }
	inline int32_t get_m_ContentType_20() const { return ___m_ContentType_20; }
	inline int32_t* get_address_of_m_ContentType_20() { return &___m_ContentType_20; }
	inline void set_m_ContentType_20(int32_t value)
	{
		___m_ContentType_20 = value;
	}

	inline static int32_t get_offset_of_m_InputType_21() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_InputType_21)); }
	inline int32_t get_m_InputType_21() const { return ___m_InputType_21; }
	inline int32_t* get_address_of_m_InputType_21() { return &___m_InputType_21; }
	inline void set_m_InputType_21(int32_t value)
	{
		___m_InputType_21 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_22() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_AsteriskChar_22)); }
	inline Il2CppChar get_m_AsteriskChar_22() const { return ___m_AsteriskChar_22; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_22() { return &___m_AsteriskChar_22; }
	inline void set_m_AsteriskChar_22(Il2CppChar value)
	{
		___m_AsteriskChar_22 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_23() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_KeyboardType_23)); }
	inline int32_t get_m_KeyboardType_23() const { return ___m_KeyboardType_23; }
	inline int32_t* get_address_of_m_KeyboardType_23() { return &___m_KeyboardType_23; }
	inline void set_m_KeyboardType_23(int32_t value)
	{
		___m_KeyboardType_23 = value;
	}

	inline static int32_t get_offset_of_m_LineType_24() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_LineType_24)); }
	inline int32_t get_m_LineType_24() const { return ___m_LineType_24; }
	inline int32_t* get_address_of_m_LineType_24() { return &___m_LineType_24; }
	inline void set_m_LineType_24(int32_t value)
	{
		___m_LineType_24 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_25() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_HideMobileInput_25)); }
	inline bool get_m_HideMobileInput_25() const { return ___m_HideMobileInput_25; }
	inline bool* get_address_of_m_HideMobileInput_25() { return &___m_HideMobileInput_25; }
	inline void set_m_HideMobileInput_25(bool value)
	{
		___m_HideMobileInput_25 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_26() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CharacterValidation_26)); }
	inline int32_t get_m_CharacterValidation_26() const { return ___m_CharacterValidation_26; }
	inline int32_t* get_address_of_m_CharacterValidation_26() { return &___m_CharacterValidation_26; }
	inline void set_m_CharacterValidation_26(int32_t value)
	{
		___m_CharacterValidation_26 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_27() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CharacterLimit_27)); }
	inline int32_t get_m_CharacterLimit_27() const { return ___m_CharacterLimit_27; }
	inline int32_t* get_address_of_m_CharacterLimit_27() { return &___m_CharacterLimit_27; }
	inline void set_m_CharacterLimit_27(int32_t value)
	{
		___m_CharacterLimit_27 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_28() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_OnEndEdit_28)); }
	inline SubmitEvent_t1556243238 * get_m_OnEndEdit_28() const { return ___m_OnEndEdit_28; }
	inline SubmitEvent_t1556243238 ** get_address_of_m_OnEndEdit_28() { return &___m_OnEndEdit_28; }
	inline void set_m_OnEndEdit_28(SubmitEvent_t1556243238 * value)
	{
		___m_OnEndEdit_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndEdit_28), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_29() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_OnValueChanged_29)); }
	inline OnChangeEvent_t2953828243 * get_m_OnValueChanged_29() const { return ___m_OnValueChanged_29; }
	inline OnChangeEvent_t2953828243 ** get_address_of_m_OnValueChanged_29() { return &___m_OnValueChanged_29; }
	inline void set_m_OnValueChanged_29(OnChangeEvent_t2953828243 * value)
	{
		___m_OnValueChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_29), value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_30() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_OnValidateInput_30)); }
	inline OnValidateInput_t4262405814 * get_m_OnValidateInput_30() const { return ___m_OnValidateInput_30; }
	inline OnValidateInput_t4262405814 ** get_address_of_m_OnValidateInput_30() { return &___m_OnValidateInput_30; }
	inline void set_m_OnValidateInput_30(OnValidateInput_t4262405814 * value)
	{
		___m_OnValidateInput_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValidateInput_30), value);
	}

	inline static int32_t get_offset_of_m_CaretColor_31() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CaretColor_31)); }
	inline Color_t320819310  get_m_CaretColor_31() const { return ___m_CaretColor_31; }
	inline Color_t320819310 * get_address_of_m_CaretColor_31() { return &___m_CaretColor_31; }
	inline void set_m_CaretColor_31(Color_t320819310  value)
	{
		___m_CaretColor_31 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_32() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CustomCaretColor_32)); }
	inline bool get_m_CustomCaretColor_32() const { return ___m_CustomCaretColor_32; }
	inline bool* get_address_of_m_CustomCaretColor_32() { return &___m_CustomCaretColor_32; }
	inline void set_m_CustomCaretColor_32(bool value)
	{
		___m_CustomCaretColor_32 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_33() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_SelectionColor_33)); }
	inline Color_t320819310  get_m_SelectionColor_33() const { return ___m_SelectionColor_33; }
	inline Color_t320819310 * get_address_of_m_SelectionColor_33() { return &___m_SelectionColor_33; }
	inline void set_m_SelectionColor_33(Color_t320819310  value)
	{
		___m_SelectionColor_33 = value;
	}

	inline static int32_t get_offset_of_m_Text_34() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_Text_34)); }
	inline String_t* get_m_Text_34() const { return ___m_Text_34; }
	inline String_t** get_address_of_m_Text_34() { return &___m_Text_34; }
	inline void set_m_Text_34(String_t* value)
	{
		___m_Text_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_34), value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_35() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CaretBlinkRate_35)); }
	inline float get_m_CaretBlinkRate_35() const { return ___m_CaretBlinkRate_35; }
	inline float* get_address_of_m_CaretBlinkRate_35() { return &___m_CaretBlinkRate_35; }
	inline void set_m_CaretBlinkRate_35(float value)
	{
		___m_CaretBlinkRate_35 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_36() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CaretWidth_36)); }
	inline int32_t get_m_CaretWidth_36() const { return ___m_CaretWidth_36; }
	inline int32_t* get_address_of_m_CaretWidth_36() { return &___m_CaretWidth_36; }
	inline void set_m_CaretWidth_36(int32_t value)
	{
		___m_CaretWidth_36 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_37() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_ReadOnly_37)); }
	inline bool get_m_ReadOnly_37() const { return ___m_ReadOnly_37; }
	inline bool* get_address_of_m_ReadOnly_37() { return &___m_ReadOnly_37; }
	inline void set_m_ReadOnly_37(bool value)
	{
		___m_ReadOnly_37 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_38() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CaretPosition_38)); }
	inline int32_t get_m_CaretPosition_38() const { return ___m_CaretPosition_38; }
	inline int32_t* get_address_of_m_CaretPosition_38() { return &___m_CaretPosition_38; }
	inline void set_m_CaretPosition_38(int32_t value)
	{
		___m_CaretPosition_38 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_39() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CaretSelectPosition_39)); }
	inline int32_t get_m_CaretSelectPosition_39() const { return ___m_CaretSelectPosition_39; }
	inline int32_t* get_address_of_m_CaretSelectPosition_39() { return &___m_CaretSelectPosition_39; }
	inline void set_m_CaretSelectPosition_39(int32_t value)
	{
		___m_CaretSelectPosition_39 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_40() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___caretRectTrans_40)); }
	inline RectTransform_t1885177139 * get_caretRectTrans_40() const { return ___caretRectTrans_40; }
	inline RectTransform_t1885177139 ** get_address_of_caretRectTrans_40() { return &___caretRectTrans_40; }
	inline void set_caretRectTrans_40(RectTransform_t1885177139 * value)
	{
		___caretRectTrans_40 = value;
		Il2CppCodeGenWriteBarrier((&___caretRectTrans_40), value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_41() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CursorVerts_41)); }
	inline UIVertexU5BU5D_t3551258791* get_m_CursorVerts_41() const { return ___m_CursorVerts_41; }
	inline UIVertexU5BU5D_t3551258791** get_address_of_m_CursorVerts_41() { return &___m_CursorVerts_41; }
	inline void set_m_CursorVerts_41(UIVertexU5BU5D_t3551258791* value)
	{
		___m_CursorVerts_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_CursorVerts_41), value);
	}

	inline static int32_t get_offset_of_m_InputTextCache_42() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_InputTextCache_42)); }
	inline TextGenerator_t3308673789 * get_m_InputTextCache_42() const { return ___m_InputTextCache_42; }
	inline TextGenerator_t3308673789 ** get_address_of_m_InputTextCache_42() { return &___m_InputTextCache_42; }
	inline void set_m_InputTextCache_42(TextGenerator_t3308673789 * value)
	{
		___m_InputTextCache_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputTextCache_42), value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_43() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CachedInputRenderer_43)); }
	inline CanvasRenderer_t799251060 * get_m_CachedInputRenderer_43() const { return ___m_CachedInputRenderer_43; }
	inline CanvasRenderer_t799251060 ** get_address_of_m_CachedInputRenderer_43() { return &___m_CachedInputRenderer_43; }
	inline void set_m_CachedInputRenderer_43(CanvasRenderer_t799251060 * value)
	{
		___m_CachedInputRenderer_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedInputRenderer_43), value);
	}

	inline static int32_t get_offset_of_m_PreventFontCallback_44() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_PreventFontCallback_44)); }
	inline bool get_m_PreventFontCallback_44() const { return ___m_PreventFontCallback_44; }
	inline bool* get_address_of_m_PreventFontCallback_44() { return &___m_PreventFontCallback_44; }
	inline void set_m_PreventFontCallback_44(bool value)
	{
		___m_PreventFontCallback_44 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_45() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_Mesh_45)); }
	inline Mesh_t540144243 * get_m_Mesh_45() const { return ___m_Mesh_45; }
	inline Mesh_t540144243 ** get_address_of_m_Mesh_45() { return &___m_Mesh_45; }
	inline void set_m_Mesh_45(Mesh_t540144243 * value)
	{
		___m_Mesh_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_45), value);
	}

	inline static int32_t get_offset_of_m_AllowInput_46() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_AllowInput_46)); }
	inline bool get_m_AllowInput_46() const { return ___m_AllowInput_46; }
	inline bool* get_address_of_m_AllowInput_46() { return &___m_AllowInput_46; }
	inline void set_m_AllowInput_46(bool value)
	{
		___m_AllowInput_46 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_47() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_ShouldActivateNextUpdate_47)); }
	inline bool get_m_ShouldActivateNextUpdate_47() const { return ___m_ShouldActivateNextUpdate_47; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_47() { return &___m_ShouldActivateNextUpdate_47; }
	inline void set_m_ShouldActivateNextUpdate_47(bool value)
	{
		___m_ShouldActivateNextUpdate_47 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_48() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_UpdateDrag_48)); }
	inline bool get_m_UpdateDrag_48() const { return ___m_UpdateDrag_48; }
	inline bool* get_address_of_m_UpdateDrag_48() { return &___m_UpdateDrag_48; }
	inline void set_m_UpdateDrag_48(bool value)
	{
		___m_UpdateDrag_48 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_49() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_DragPositionOutOfBounds_49)); }
	inline bool get_m_DragPositionOutOfBounds_49() const { return ___m_DragPositionOutOfBounds_49; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_49() { return &___m_DragPositionOutOfBounds_49; }
	inline void set_m_DragPositionOutOfBounds_49(bool value)
	{
		___m_DragPositionOutOfBounds_49 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_52() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_CaretVisible_52)); }
	inline bool get_m_CaretVisible_52() const { return ___m_CaretVisible_52; }
	inline bool* get_address_of_m_CaretVisible_52() { return &___m_CaretVisible_52; }
	inline void set_m_CaretVisible_52(bool value)
	{
		___m_CaretVisible_52 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_53() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_BlinkCoroutine_53)); }
	inline Coroutine_t3555871518 * get_m_BlinkCoroutine_53() const { return ___m_BlinkCoroutine_53; }
	inline Coroutine_t3555871518 ** get_address_of_m_BlinkCoroutine_53() { return &___m_BlinkCoroutine_53; }
	inline void set_m_BlinkCoroutine_53(Coroutine_t3555871518 * value)
	{
		___m_BlinkCoroutine_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlinkCoroutine_53), value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_54() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_BlinkStartTime_54)); }
	inline float get_m_BlinkStartTime_54() const { return ___m_BlinkStartTime_54; }
	inline float* get_address_of_m_BlinkStartTime_54() { return &___m_BlinkStartTime_54; }
	inline void set_m_BlinkStartTime_54(float value)
	{
		___m_BlinkStartTime_54 = value;
	}

	inline static int32_t get_offset_of_m_DrawStart_55() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_DrawStart_55)); }
	inline int32_t get_m_DrawStart_55() const { return ___m_DrawStart_55; }
	inline int32_t* get_address_of_m_DrawStart_55() { return &___m_DrawStart_55; }
	inline void set_m_DrawStart_55(int32_t value)
	{
		___m_DrawStart_55 = value;
	}

	inline static int32_t get_offset_of_m_DrawEnd_56() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_DrawEnd_56)); }
	inline int32_t get_m_DrawEnd_56() const { return ___m_DrawEnd_56; }
	inline int32_t* get_address_of_m_DrawEnd_56() { return &___m_DrawEnd_56; }
	inline void set_m_DrawEnd_56(int32_t value)
	{
		___m_DrawEnd_56 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_57() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_DragCoroutine_57)); }
	inline Coroutine_t3555871518 * get_m_DragCoroutine_57() const { return ___m_DragCoroutine_57; }
	inline Coroutine_t3555871518 ** get_address_of_m_DragCoroutine_57() { return &___m_DragCoroutine_57; }
	inline void set_m_DragCoroutine_57(Coroutine_t3555871518 * value)
	{
		___m_DragCoroutine_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragCoroutine_57), value);
	}

	inline static int32_t get_offset_of_m_OriginalText_58() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_OriginalText_58)); }
	inline String_t* get_m_OriginalText_58() const { return ___m_OriginalText_58; }
	inline String_t** get_address_of_m_OriginalText_58() { return &___m_OriginalText_58; }
	inline void set_m_OriginalText_58(String_t* value)
	{
		___m_OriginalText_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalText_58), value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_59() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_WasCanceled_59)); }
	inline bool get_m_WasCanceled_59() const { return ___m_WasCanceled_59; }
	inline bool* get_address_of_m_WasCanceled_59() { return &___m_WasCanceled_59; }
	inline void set_m_WasCanceled_59(bool value)
	{
		___m_WasCanceled_59 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_60() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_HasDoneFocusTransition_60)); }
	inline bool get_m_HasDoneFocusTransition_60() const { return ___m_HasDoneFocusTransition_60; }
	inline bool* get_address_of_m_HasDoneFocusTransition_60() { return &___m_HasDoneFocusTransition_60; }
	inline void set_m_HasDoneFocusTransition_60(bool value)
	{
		___m_HasDoneFocusTransition_60 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_62() { return static_cast<int32_t>(offsetof(InputField_t3161868630, ___m_ProcessingEvent_62)); }
	inline Event_t3603750770 * get_m_ProcessingEvent_62() const { return ___m_ProcessingEvent_62; }
	inline Event_t3603750770 ** get_address_of_m_ProcessingEvent_62() { return &___m_ProcessingEvent_62; }
	inline void set_m_ProcessingEvent_62(Event_t3603750770 * value)
	{
		___m_ProcessingEvent_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessingEvent_62), value);
	}
};

struct InputField_t3161868630_StaticFields
{
public:
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t674980486* ___kSeparators_17;

public:
	inline static int32_t get_offset_of_kSeparators_17() { return static_cast<int32_t>(offsetof(InputField_t3161868630_StaticFields, ___kSeparators_17)); }
	inline CharU5BU5D_t674980486* get_kSeparators_17() const { return ___kSeparators_17; }
	inline CharU5BU5D_t674980486** get_address_of_kSeparators_17() { return &___kSeparators_17; }
	inline void set_kSeparators_17(CharU5BU5D_t674980486* value)
	{
		___kSeparators_17 = value;
		Il2CppCodeGenWriteBarrier((&___kSeparators_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELD_T3161868630_H
#ifndef MASKABLEGRAPHIC_T2666258263_H
#define MASKABLEGRAPHIC_T2666258263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t2666258263  : public Graphic_t1406460313
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t4055262778 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t2103047313 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3000894829 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t4153953548* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_MaskMaterial_20)); }
	inline Material_t4055262778 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t4055262778 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t4055262778 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_ParentMask_21)); }
	inline RectMask2D_t2103047313 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t2103047313 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t2103047313 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3000894829 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3000894829 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3000894829 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2666258263, ___m_Corners_27)); }
	inline Vector3U5BU5D_t4153953548* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t4153953548** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t4153953548* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T2666258263_H
#ifndef BOXSLIDER_T2780260910_H
#define BOXSLIDER_T2780260910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider
struct  BoxSlider_t2780260910  : public Selectable_t3890617260
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleRect
	RectTransform_t1885177139 * ___m_HandleRect_16;
	// System.Single UnityEngine.UI.BoxSlider::m_MinValue
	float ___m_MinValue_17;
	// System.Single UnityEngine.UI.BoxSlider::m_MaxValue
	float ___m_MaxValue_18;
	// System.Boolean UnityEngine.UI.BoxSlider::m_WholeNumbers
	bool ___m_WholeNumbers_19;
	// System.Single UnityEngine.UI.BoxSlider::m_Value
	float ___m_Value_20;
	// System.Single UnityEngine.UI.BoxSlider::m_ValueY
	float ___m_ValueY_21;
	// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::m_OnValueChanged
	BoxSliderEvent_t2026169079 * ___m_OnValueChanged_22;
	// UnityEngine.Transform UnityEngine.UI.BoxSlider::m_HandleTransform
	Transform_t3933397867 * ___m_HandleTransform_23;
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleContainerRect
	RectTransform_t1885177139 * ___m_HandleContainerRect_24;
	// UnityEngine.Vector2 UnityEngine.UI.BoxSlider::m_Offset
	Vector2_t59524482  ___m_Offset_25;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.BoxSlider::m_Tracker
	DrivenRectTransformTracker_t1306825633  ___m_Tracker_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_HandleRect_16)); }
	inline RectTransform_t1885177139 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t1885177139 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t1885177139 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_MinValue_17() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_MinValue_17)); }
	inline float get_m_MinValue_17() const { return ___m_MinValue_17; }
	inline float* get_address_of_m_MinValue_17() { return &___m_MinValue_17; }
	inline void set_m_MinValue_17(float value)
	{
		___m_MinValue_17 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_18() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_MaxValue_18)); }
	inline float get_m_MaxValue_18() const { return ___m_MaxValue_18; }
	inline float* get_address_of_m_MaxValue_18() { return &___m_MaxValue_18; }
	inline void set_m_MaxValue_18(float value)
	{
		___m_MaxValue_18 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_19() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_WholeNumbers_19)); }
	inline bool get_m_WholeNumbers_19() const { return ___m_WholeNumbers_19; }
	inline bool* get_address_of_m_WholeNumbers_19() { return &___m_WholeNumbers_19; }
	inline void set_m_WholeNumbers_19(bool value)
	{
		___m_WholeNumbers_19 = value;
	}

	inline static int32_t get_offset_of_m_Value_20() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_Value_20)); }
	inline float get_m_Value_20() const { return ___m_Value_20; }
	inline float* get_address_of_m_Value_20() { return &___m_Value_20; }
	inline void set_m_Value_20(float value)
	{
		___m_Value_20 = value;
	}

	inline static int32_t get_offset_of_m_ValueY_21() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_ValueY_21)); }
	inline float get_m_ValueY_21() const { return ___m_ValueY_21; }
	inline float* get_address_of_m_ValueY_21() { return &___m_ValueY_21; }
	inline void set_m_ValueY_21(float value)
	{
		___m_ValueY_21 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_22() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_OnValueChanged_22)); }
	inline BoxSliderEvent_t2026169079 * get_m_OnValueChanged_22() const { return ___m_OnValueChanged_22; }
	inline BoxSliderEvent_t2026169079 ** get_address_of_m_OnValueChanged_22() { return &___m_OnValueChanged_22; }
	inline void set_m_OnValueChanged_22(BoxSliderEvent_t2026169079 * value)
	{
		___m_OnValueChanged_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_22), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_23() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_HandleTransform_23)); }
	inline Transform_t3933397867 * get_m_HandleTransform_23() const { return ___m_HandleTransform_23; }
	inline Transform_t3933397867 ** get_address_of_m_HandleTransform_23() { return &___m_HandleTransform_23; }
	inline void set_m_HandleTransform_23(Transform_t3933397867 * value)
	{
		___m_HandleTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_23), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_24() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_HandleContainerRect_24)); }
	inline RectTransform_t1885177139 * get_m_HandleContainerRect_24() const { return ___m_HandleContainerRect_24; }
	inline RectTransform_t1885177139 ** get_address_of_m_HandleContainerRect_24() { return &___m_HandleContainerRect_24; }
	inline void set_m_HandleContainerRect_24(RectTransform_t1885177139 * value)
	{
		___m_HandleContainerRect_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_24), value);
	}

	inline static int32_t get_offset_of_m_Offset_25() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_Offset_25)); }
	inline Vector2_t59524482  get_m_Offset_25() const { return ___m_Offset_25; }
	inline Vector2_t59524482 * get_address_of_m_Offset_25() { return &___m_Offset_25; }
	inline void set_m_Offset_25(Vector2_t59524482  value)
	{
		___m_Offset_25 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_26() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_Tracker_26)); }
	inline DrivenRectTransformTracker_t1306825633  get_m_Tracker_26() const { return ___m_Tracker_26; }
	inline DrivenRectTransformTracker_t1306825633 * get_address_of_m_Tracker_26() { return &___m_Tracker_26; }
	inline void set_m_Tracker_26(DrivenRectTransformTracker_t1306825633  value)
	{
		___m_Tracker_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDER_T2780260910_H
#ifndef SLIDER_T301630380_H
#define SLIDER_T301630380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider
struct  Slider_t301630380  : public Selectable_t3890617260
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t1885177139 * ___m_FillRect_16;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t1885177139 * ___m_HandleRect_17;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_18;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_19;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_20;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_21;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_22;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t1040040728 * ___m_OnValueChanged_23;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t126372159 * ___m_FillImage_24;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_t3933397867 * ___m_FillTransform_25;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t1885177139 * ___m_FillContainerRect_26;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_t3933397867 * ___m_HandleTransform_27;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t1885177139 * ___m_HandleContainerRect_28;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_t59524482  ___m_Offset_29;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t1306825633  ___m_Tracker_30;

public:
	inline static int32_t get_offset_of_m_FillRect_16() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_FillRect_16)); }
	inline RectTransform_t1885177139 * get_m_FillRect_16() const { return ___m_FillRect_16; }
	inline RectTransform_t1885177139 ** get_address_of_m_FillRect_16() { return &___m_FillRect_16; }
	inline void set_m_FillRect_16(RectTransform_t1885177139 * value)
	{
		___m_FillRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillRect_16), value);
	}

	inline static int32_t get_offset_of_m_HandleRect_17() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_HandleRect_17)); }
	inline RectTransform_t1885177139 * get_m_HandleRect_17() const { return ___m_HandleRect_17; }
	inline RectTransform_t1885177139 ** get_address_of_m_HandleRect_17() { return &___m_HandleRect_17; }
	inline void set_m_HandleRect_17(RectTransform_t1885177139 * value)
	{
		___m_HandleRect_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_17), value);
	}

	inline static int32_t get_offset_of_m_Direction_18() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_Direction_18)); }
	inline int32_t get_m_Direction_18() const { return ___m_Direction_18; }
	inline int32_t* get_address_of_m_Direction_18() { return &___m_Direction_18; }
	inline void set_m_Direction_18(int32_t value)
	{
		___m_Direction_18 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_19() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_MinValue_19)); }
	inline float get_m_MinValue_19() const { return ___m_MinValue_19; }
	inline float* get_address_of_m_MinValue_19() { return &___m_MinValue_19; }
	inline void set_m_MinValue_19(float value)
	{
		___m_MinValue_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_20() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_MaxValue_20)); }
	inline float get_m_MaxValue_20() const { return ___m_MaxValue_20; }
	inline float* get_address_of_m_MaxValue_20() { return &___m_MaxValue_20; }
	inline void set_m_MaxValue_20(float value)
	{
		___m_MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_21() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_WholeNumbers_21)); }
	inline bool get_m_WholeNumbers_21() const { return ___m_WholeNumbers_21; }
	inline bool* get_address_of_m_WholeNumbers_21() { return &___m_WholeNumbers_21; }
	inline void set_m_WholeNumbers_21(bool value)
	{
		___m_WholeNumbers_21 = value;
	}

	inline static int32_t get_offset_of_m_Value_22() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_Value_22)); }
	inline float get_m_Value_22() const { return ___m_Value_22; }
	inline float* get_address_of_m_Value_22() { return &___m_Value_22; }
	inline void set_m_Value_22(float value)
	{
		___m_Value_22 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_23() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_OnValueChanged_23)); }
	inline SliderEvent_t1040040728 * get_m_OnValueChanged_23() const { return ___m_OnValueChanged_23; }
	inline SliderEvent_t1040040728 ** get_address_of_m_OnValueChanged_23() { return &___m_OnValueChanged_23; }
	inline void set_m_OnValueChanged_23(SliderEvent_t1040040728 * value)
	{
		___m_OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_m_FillImage_24() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_FillImage_24)); }
	inline Image_t126372159 * get_m_FillImage_24() const { return ___m_FillImage_24; }
	inline Image_t126372159 ** get_address_of_m_FillImage_24() { return &___m_FillImage_24; }
	inline void set_m_FillImage_24(Image_t126372159 * value)
	{
		___m_FillImage_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillImage_24), value);
	}

	inline static int32_t get_offset_of_m_FillTransform_25() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_FillTransform_25)); }
	inline Transform_t3933397867 * get_m_FillTransform_25() const { return ___m_FillTransform_25; }
	inline Transform_t3933397867 ** get_address_of_m_FillTransform_25() { return &___m_FillTransform_25; }
	inline void set_m_FillTransform_25(Transform_t3933397867 * value)
	{
		___m_FillTransform_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillTransform_25), value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_26() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_FillContainerRect_26)); }
	inline RectTransform_t1885177139 * get_m_FillContainerRect_26() const { return ___m_FillContainerRect_26; }
	inline RectTransform_t1885177139 ** get_address_of_m_FillContainerRect_26() { return &___m_FillContainerRect_26; }
	inline void set_m_FillContainerRect_26(RectTransform_t1885177139 * value)
	{
		___m_FillContainerRect_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillContainerRect_26), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_27() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_HandleTransform_27)); }
	inline Transform_t3933397867 * get_m_HandleTransform_27() const { return ___m_HandleTransform_27; }
	inline Transform_t3933397867 ** get_address_of_m_HandleTransform_27() { return &___m_HandleTransform_27; }
	inline void set_m_HandleTransform_27(Transform_t3933397867 * value)
	{
		___m_HandleTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_27), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_28() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_HandleContainerRect_28)); }
	inline RectTransform_t1885177139 * get_m_HandleContainerRect_28() const { return ___m_HandleContainerRect_28; }
	inline RectTransform_t1885177139 ** get_address_of_m_HandleContainerRect_28() { return &___m_HandleContainerRect_28; }
	inline void set_m_HandleContainerRect_28(RectTransform_t1885177139 * value)
	{
		___m_HandleContainerRect_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_28), value);
	}

	inline static int32_t get_offset_of_m_Offset_29() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_Offset_29)); }
	inline Vector2_t59524482  get_m_Offset_29() const { return ___m_Offset_29; }
	inline Vector2_t59524482 * get_address_of_m_Offset_29() { return &___m_Offset_29; }
	inline void set_m_Offset_29(Vector2_t59524482  value)
	{
		___m_Offset_29 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_30() { return static_cast<int32_t>(offsetof(Slider_t301630380, ___m_Tracker_30)); }
	inline DrivenRectTransformTracker_t1306825633  get_m_Tracker_30() const { return ___m_Tracker_30; }
	inline DrivenRectTransformTracker_t1306825633 * get_address_of_m_Tracker_30() { return &___m_Tracker_30; }
	inline void set_m_Tracker_30(DrivenRectTransformTracker_t1306825633  value)
	{
		___m_Tracker_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDER_T301630380_H
#ifndef IMAGE_T126372159_H
#define IMAGE_T126372159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t126372159  : public MaskableGraphic_t2666258263
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t1309550511 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t1309550511 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_Sprite_29)); }
	inline Sprite_t1309550511 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t1309550511 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t1309550511 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_OverrideSprite_30)); }
	inline Sprite_t1309550511 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t1309550511 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t1309550511 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t126372159, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t126372159_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t4055262778 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t3857693239* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t3857693239* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t4153953548* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t4153953548* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t126372159_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t4055262778 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t4055262778 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t4055262778 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t126372159_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t3857693239* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t3857693239** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t3857693239* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t126372159_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t3857693239* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t3857693239** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t3857693239* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t126372159_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t4153953548* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t4153953548** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t4153953548* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t126372159_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t4153953548* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t4153953548** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t4153953548* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T126372159_H
#ifndef RAWIMAGE_T3037409423_H
#define RAWIMAGE_T3037409423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t3037409423  : public MaskableGraphic_t2666258263
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t1132728222 * ___m_Texture_28;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t1992046353  ___m_UVRect_29;

public:
	inline static int32_t get_offset_of_m_Texture_28() { return static_cast<int32_t>(offsetof(RawImage_t3037409423, ___m_Texture_28)); }
	inline Texture_t1132728222 * get_m_Texture_28() const { return ___m_Texture_28; }
	inline Texture_t1132728222 ** get_address_of_m_Texture_28() { return &___m_Texture_28; }
	inline void set_m_Texture_28(Texture_t1132728222 * value)
	{
		___m_Texture_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_28), value);
	}

	inline static int32_t get_offset_of_m_UVRect_29() { return static_cast<int32_t>(offsetof(RawImage_t3037409423, ___m_UVRect_29)); }
	inline Rect_t1992046353  get_m_UVRect_29() const { return ___m_UVRect_29; }
	inline Rect_t1992046353 * get_address_of_m_UVRect_29() { return &___m_UVRect_29; }
	inline void set_m_UVRect_29(Rect_t1992046353  value)
	{
		___m_UVRect_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T3037409423_H
#ifndef TEXT_T3069741234_H
#define TEXT_T3069741234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t3069741234  : public MaskableGraphic_t2666258263
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t1355997048 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3308673789 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3308673789 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t3551258791* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t3069741234, ___m_FontData_28)); }
	inline FontData_t1355997048 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t1355997048 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t1355997048 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t3069741234, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t3069741234, ___m_TextCache_30)); }
	inline TextGenerator_t3308673789 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3308673789 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3308673789 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t3069741234, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3308673789 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3308673789 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3308673789 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t3069741234, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t3069741234, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t3551258791* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t3551258791** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t3551258791* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t3069741234_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t4055262778 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t3069741234_StaticFields, ___s_DefaultText_32)); }
	inline Material_t4055262778 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t4055262778 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t4055262778 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T3069741234_H
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t1488940705  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t3649338848 * m_Items[1];

public:
	inline GameObject_t3649338848 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t3649338848 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t3649338848 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t3649338848 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t3649338848 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t3649338848 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t636217733  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t2499566028  m_Items[1];

public:
	inline Color32_t2499566028  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t2499566028 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t2499566028  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t2499566028  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t2499566028 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t2499566028  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3384890222  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1448570014  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t419044168  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Particle_t1268635397  m_Items[1];

public:
	inline Particle_t1268635397  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Particle_t1268635397 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Particle_t1268635397  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Particle_t1268635397  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Particle_t1268635397 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Particle_t1268635397  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m1907148054_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t596762001  p1, Quaternion_t3165733013  p2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m3090413657_gshared (GameObject_t3649338848 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::get_Count()
extern "C"  int32_t List_1_get_Count_m655066853_gshared (List_1_t547075962 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::GetEnumerator()
extern "C"  Enumerator_t1895934899  List_1_GetEnumerator_m2437281809_gshared (List_1_t547075962 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::get_Current()
extern "C"  ARHitTestResult_t1803723679  Enumerator_get_Current_m3632740371_gshared (Enumerator_t1895934899 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1007619642_gshared (Enumerator_t1895934899 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3562983614_gshared (Enumerator_t1895934899 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern "C"  void UnityEvent_1__ctor_m1815935619_gshared (UnityEvent_1_t3808368702 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m3649492292_gshared (Component_t531478471 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m4180551968_gshared (UnityAction_1_t1392064635 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_AddListener_m2001044386_gshared (UnityEvent_1_t3808368702 * __this, UnityAction_1_t1392064635 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_RemoveListener_m1657194609_gshared (UnityEvent_1_t3808368702 * __this, UnityAction_1_t1392064635 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`3<System.Single,System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m672494230_gshared (UnityAction_3_t3771153942 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>::AddListener(UnityEngine.Events.UnityAction`3<!0,!1,!2>)
extern "C"  void UnityEvent_3_AddListener_m3442876925_gshared (UnityEvent_3_t876514367 * __this, UnityAction_3_t3771153942 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>::RemoveListener(UnityEngine.Events.UnityAction`3<!0,!1,!2>)
extern "C"  void UnityEvent_3_RemoveListener_m2900402221_gshared (UnityEvent_3_t876514367 * __this, UnityAction_3_t3771153942 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(!0)
extern "C"  void UnityEvent_1_Invoke_m1164418121_gshared (UnityEvent_1_t3808368702 * __this, Color_t320819310  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>::Invoke(!0,!1,!2)
extern "C"  void UnityEvent_3_Invoke_m2348520805_gshared (UnityEvent_3_t876514367 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m482530400_gshared (UnityAction_1_t3918860037 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_AddListener_m1802892215_gshared (UnityEvent_1_t2040196808 * __this, UnityAction_1_t3918860037 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_RemoveListener_m1915413156_gshared (UnityEvent_1_t2040196808 * __this, UnityAction_1_t3918860037 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m1269287345_gshared (UnityAction_1_t1193757028 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_AddListener_m2358688097_gshared (UnityEvent_1_t3610061095 * __this, UnityAction_1_t1193757028 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_RemoveListener_m313363161_gshared (UnityEvent_1_t3610061095 * __this, UnityAction_1_t1193757028 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>::.ctor()
extern "C"  void UnityEvent_3__ctor_m3450248554_gshared (UnityEvent_3_t876514367 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m3859482884_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m3365215536_gshared (List_1_t3635081580 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m112108964_gshared (List_1_t3160831282 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C"  void List_1_Add_m684783150_gshared (List_1_t3635081580 * __this, Vector3_t596762001  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m2991374128_gshared (List_1_t3160831282 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C"  int32_t List_1_get_Count_m3739057527_gshared (List_1_t3635081580 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C"  Enumerator_t688973221  List_1_GetEnumerator_m2550167595_gshared (List_1_t3635081580 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t596762001  Enumerator_get_Current_m1534598271_gshared (Enumerator_t688973221 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2323590328_gshared (Enumerator_t688973221 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m3684835367_gshared (Enumerator_t688973221 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m4219090612_gshared (UnityAction_2_t3878312970 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Single,System.Single>::AddListener(UnityEngine.Events.UnityAction`2<!0,!1>)
extern "C"  void UnityEvent_2_AddListener_m3369274527_gshared (UnityEvent_2_t1533210863 * __this, UnityAction_2_t3878312970 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Single,System.Single>::RemoveListener(UnityEngine.Events.UnityAction`2<!0,!1>)
extern "C"  void UnityEvent_2_RemoveListener_m1528257211_gshared (UnityEvent_2_t1533210863 * __this, UnityAction_2_t3878312970 * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.BoxSlider::SetClass<System.Object>(T&,T)
extern "C"  bool BoxSlider_SetClass_TisRuntimeObject_m4172831646_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject ** ___currentValue0, RuntimeObject * ___newValue1, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.BoxSlider::SetStruct<System.Single>(T&,T)
extern "C"  bool BoxSlider_SetStruct_TisSingle_t2847614712_m250040320_gshared (RuntimeObject * __this /* static, unused */, float* ___currentValue0, float ___newValue1, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.BoxSlider::SetStruct<System.Boolean>(T&,T)
extern "C"  bool BoxSlider_SetStruct_TisBoolean_t362855854_m292852063_gshared (RuntimeObject * __this /* static, unused */, bool* ___currentValue0, bool ___newValue1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Single,System.Single>::Invoke(!0,!1)
extern "C"  void UnityEvent_2_Invoke_m2732613775_gshared (UnityEvent_2_t1533210863 * __this, float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Single,System.Single>::.ctor()
extern "C"  void UnityEvent_2__ctor_m1813845425_gshared (UnityEvent_2_t1533210863 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m4062598822 (MonoBehaviour_t1096588306 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
extern "C"  void MaterialPropertyBlock__ctor_m1005441945 (MaterialPropertyBlock_t2709947369 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t3165733013  Quaternion_get_identity_m2874084265 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t3649338848_m3594403643(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t3649338848 * (*) (RuntimeObject * /* static, unused */, GameObject_t3649338848 *, Vector3_t596762001 , Quaternion_t3165733013 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1907148054_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m3146616867 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2728568087 (Color_t320819310 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.String,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m2611042094 (MaterialPropertyBlock_t2709947369 * __this, String_t* p0, Color_t320819310  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t1885265068_m3314343886(__this, method) ((  MeshRenderer_t1885265068 * (*) (GameObject_t3649338848 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m3090413657_gshared)(__this, method)
// System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern "C"  void Renderer_SetPropertyBlock_m656619281 (Renderer_t674913088 * __this, MaterialPropertyBlock_t2709947369 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m4114401638 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t3132414106  Input_GetTouch_m2909242247 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m993859056 (Touch_t3132414106 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t226495598 * Camera_get_main_m2671983186 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t59524482  Touch_get_position_m2291334870 (Touch_t3132414106 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t596762001  Vector2_op_Implicit_m2201252505 (RuntimeObject * __this /* static, unused */, Vector2_t59524482  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t596762001  Camera_ScreenToViewportPoint_m474730427 (Camera_t226495598 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARSessionNativeInterface()
extern "C"  UnityARSessionNativeInterface_t3869411053 * UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult> UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  List_1_t547075962 * UnityARSessionNativeInterface_HitTest_m1126351971 (UnityARSessionNativeInterface_t3869411053 * __this, ARPoint_t1649490841  p0, int64_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::get_Count()
#define List_1_get_Count_m655066853(__this, method) ((  int32_t (*) (List_1_t547075962 *, const RuntimeMethod*))List_1_get_Count_m655066853_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::GetEnumerator()
#define List_1_GetEnumerator_m2437281809(__this, method) ((  Enumerator_t1895934899  (*) (List_1_t547075962 *, const RuntimeMethod*))List_1_GetEnumerator_m2437281809_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::get_Current()
#define Enumerator_get_Current_m3632740371(__this, method) ((  ARHitTestResult_t1803723679  (*) (Enumerator_t1895934899 *, const RuntimeMethod*))Enumerator_get_Current_m3632740371_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.XR.iOS.UnityARMatrixOps::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t596762001  UnityARMatrixOps_GetPosition_m3553979848 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1288378485  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1043686083 (Vector3_t596762001 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void BallMaker::CreateBall(UnityEngine.Vector3)
extern "C"  void BallMaker_CreateBall_m20175966 (BallMaker_t1786680372 * __this, Vector3_t596762001  ___atPosition0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::MoveNext()
#define Enumerator_MoveNext_m1007619642(__this, method) ((  bool (*) (Enumerator_t1895934899 *, const RuntimeMethod*))Enumerator_MoveNext_m1007619642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::Dispose()
#define Enumerator_Dispose_m3562983614(__this, method) ((  void (*) (Enumerator_t1895934899 *, const RuntimeMethod*))Enumerator_Dispose_m3562983614_gshared)(__this, method)
// System.Void BallMover::CreateMoveBall(UnityEngine.Vector3)
extern "C"  void BallMover_CreateMoveBall_m1567691649 (BallMover_t4283490589 * __this, Vector3_t596762001  ___explodePosition0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1545791448 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, Object_t1008057425 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3933397867 * GameObject_get_transform_m890220094 (GameObject_t3649338848 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t596762001  Transform_get_position_m3654347341 (Transform_t3933397867 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t596762001  Vector3_MoveTowards_m2110482316 (RuntimeObject * __this /* static, unused */, Vector3_t596762001  p0, Vector3_t596762001  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2789033506 (Transform_t3933397867 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m1839175560 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3933397867 * Component_get_transform_m2033240428 (Component_t531478471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t3649338848 * Component_get_gameObject_m3065601689 (Component_t531478471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
#define UnityEvent_1__ctor_m1815935619(__this, method) ((  void (*) (UnityEvent_1_t3808368702 *, const RuntimeMethod*))UnityEvent_1__ctor_m1815935619_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t126372159_m734387587(__this, method) ((  Image_t126372159 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m4180551968(__this, p0, p1, method) ((  void (*) (UnityAction_1_t1392064635 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))UnityAction_1__ctor_m4180551968_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_AddListener_m2001044386(__this, p0, method) ((  void (*) (UnityEvent_1_t3808368702 *, UnityAction_1_t1392064635 *, const RuntimeMethod*))UnityEvent_1_AddListener_m2001044386_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_RemoveListener_m1657194609(__this, p0, method) ((  void (*) (UnityEvent_1_t3808368702 *, UnityAction_1_t1392064635 *, const RuntimeMethod*))UnityEvent_1_RemoveListener_m1657194609_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t3069741234_m3241350479(__this, method) ((  Text_t3069741234 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m2992201115 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`3<System.Single,System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
#define UnityAction_3__ctor_m672494230(__this, p0, p1, method) ((  void (*) (UnityAction_3_t3771153942 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))UnityAction_3__ctor_m672494230_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>::AddListener(UnityEngine.Events.UnityAction`3<!0,!1,!2>)
#define UnityEvent_3_AddListener_m3442876925(__this, p0, method) ((  void (*) (UnityEvent_3_t876514367 *, UnityAction_3_t3771153942 *, const RuntimeMethod*))UnityEvent_3_AddListener_m3442876925_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>::RemoveListener(UnityEngine.Events.UnityAction`3<!0,!1,!2>)
#define UnityEvent_3_RemoveListener_m2900402221(__this, p0, method) ((  void (*) (UnityEvent_3_t876514367 *, UnityAction_3_t3771153942 *, const RuntimeMethod*))UnityEvent_3_RemoveListener_m2900402221_gshared)(__this, p0, method)
// System.Void ColorLabel::UpdateValue()
extern "C"  void ColorLabel_UpdateValue_m2990534176 (ColorLabel_t538373442 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m528327324 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, Object_t1008057425 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2023871361 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::GetValue(ColorValues)
extern "C"  float ColorPicker_GetValue_m3746747326 (ColorPicker_t2582382693 * __this, int32_t ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String ColorLabel::ConvertToDisplayString(System.Single)
extern "C"  String_t* ColorLabel_ConvertToDisplayString_m3783319992 (ColorLabel_t538373442 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m3706473663 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString(System.String)
extern "C"  String_t* Single_ToString_m1876090627 (float* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m464692734 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m969347708 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorChangedEvent::.ctor()
extern "C"  void ColorChangedEvent__ctor_m420103119 (ColorChangedEvent_t347040188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HSVChangedEvent::.ctor()
extern "C"  void HSVChangedEvent__ctor_m2671244103 (HSVChangedEvent_t3546572410 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1438150939 (Color_t320819310 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color ColorPicker::get_CurrentColor()
extern "C"  Color_t320819310  ColorPicker_get_CurrentColor_m3098018243 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Equality_m441720939 (RuntimeObject * __this /* static, unused */, Color_t320819310  p0, Color_t320819310  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::RGBChanged()
extern "C"  void ColorPicker_RGBChanged_m676920536 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::SendChangedEvent()
extern "C"  void ColorPicker_SendChangedEvent_m1023062679 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::HSVChanged()
extern "C"  void ColorPicker_HSVChanged_m1999027478 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HsvColor HSVUtil::ConvertRgbToHsv(UnityEngine.Color)
extern "C"  HsvColor_t3806242730  HSVUtil_ConvertRgbToHsv_m1523836538 (RuntimeObject * __this /* static, unused */, Color_t320819310  ___color0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single HsvColor::get_normalizedH()
extern "C"  float HsvColor_get_normalizedH_m2453528821 (HsvColor_t3806242730 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single HsvColor::get_normalizedS()
extern "C"  float HsvColor_get_normalizedS_m3606923594 (HsvColor_t3806242730 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single HsvColor::get_normalizedV()
extern "C"  float HsvColor_get_normalizedV_m2333326027 (HsvColor_t3806242730 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HSVUtil::ConvertHsvToRgb(System.Double,System.Double,System.Double,System.Single)
extern "C"  Color_t320819310  HSVUtil_ConvertHsvToRgb_m3345724273 (RuntimeObject * __this /* static, unused */, double ___h0, double ___s1, double ___v2, float ___alpha3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(!0)
#define UnityEvent_1_Invoke_m1164418121(__this, p0, method) ((  void (*) (UnityEvent_1_t3808368702 *, Color_t320819310 , const RuntimeMethod*))UnityEvent_1_Invoke_m1164418121_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>::Invoke(!0,!1,!2)
#define UnityEvent_3_Invoke_m2348520805(__this, p0, p1, p2, method) ((  void (*) (UnityEvent_3_t876514367 *, float, float, float, const RuntimeMethod*))UnityEvent_3_Invoke_m2348520805_gshared)(__this, p0, p1, p2, method)
// System.Void ColorPicker::set_R(System.Single)
extern "C"  void ColorPicker_set_R_m1448051533 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_G(System.Single)
extern "C"  void ColorPicker_set_G_m643121640 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_B(System.Single)
extern "C"  void ColorPicker_set_B_m3133411859 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_A(System.Single)
extern "C"  void ColorPicker_set_A_m263421143 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_H(System.Single)
extern "C"  void ColorPicker_set_H_m1627930837 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_S(System.Single)
extern "C"  void ColorPicker_set_S_m3068359377 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::set_V(System.Single)
extern "C"  void ColorPicker_set_V_m3298641509 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_R()
extern "C"  float ColorPicker_get_R_m371651317 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_G()
extern "C"  float ColorPicker_get_G_m908827283 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_B()
extern "C"  float ColorPicker_get_B_m2190872939 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_A()
extern "C"  float ColorPicker_get_A_m340059024 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_H()
extern "C"  float ColorPicker_get_H_m1137646974 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_S()
extern "C"  float ColorPicker_get_S_m220126169 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ColorPicker::get_V()
extern "C"  float ColorPicker_get_V_m286556906 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor(System.String)
extern "C"  void NotImplementedException__ctor_m1129172726 (NotImplementedException_t4033310348 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t4055262778 * Renderer_get_material_m1063092742 (Renderer_t674913088 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m3512933588 (Material_t4055262778 * __this, Color_t320819310  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C"  bool GameObject_get_activeSelf_m3172293468 (GameObject_t3649338848 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m3661033924 (GameObject_t3649338848 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t126372159_m3108703159(__this, method) ((  Image_t126372159 * (*) (GameObject_t3649338848 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m3090413657_gshared)(__this, method)
// System.Void ColorPicker::set_CurrentColor(UnityEngine.Color)
extern "C"  void ColorPicker_set_CurrentColor_m2827670769 (ColorPicker_t2582382693 * __this, Color_t320819310  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Slider>()
#define Component_GetComponent_TisSlider_t301630380_m2687307688(__this, method) ((  Slider_t301630380 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::get_onValueChanged()
extern "C"  SliderEvent_t1040040728 * Slider_get_onValueChanged_m3507164652 (Slider_t301630380 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m482530400(__this, p0, p1, method) ((  void (*) (UnityAction_1_t3918860037 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))UnityAction_1__ctor_m482530400_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_AddListener_m1802892215(__this, p0, method) ((  void (*) (UnityEvent_1_t2040196808 *, UnityAction_1_t3918860037 *, const RuntimeMethod*))UnityEvent_1_AddListener_m1802892215_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_RemoveListener_m1915413156(__this, p0, method) ((  void (*) (UnityEvent_1_t2040196808 *, UnityAction_1_t3918860037 *, const RuntimeMethod*))UnityEvent_1_RemoveListener_m1915413156_gshared)(__this, p0, method)
// System.Void UnityEngine.UI.Slider::set_normalizedValue(System.Single)
extern "C"  void Slider_set_normalizedValue_m97916338 (Slider_t301630380 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Slider::get_normalizedValue()
extern "C"  float Slider_get_normalizedValue_m1903738556 (Slider_t301630380 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ColorPicker::AssignColor(ColorValues,System.Single)
extern "C"  void ColorPicker_AssignColor_m1478515366 (ColorPicker_t2582382693 * __this, int32_t ___type0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.RawImage>()
#define Component_GetComponent_TisRawImage_t3037409423_m801911595(__this, method) ((  RawImage_t3037409423 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// System.Void ColorSliderImage::RegenerateTexture()
extern "C"  void ColorSliderImage_RegenerateTexture_m629802786 (ColorSliderImage_t664667956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.UI.RawImage::get_texture()
extern "C"  Texture_t1132728222 * RawImage_get_texture_m429915103 (RawImage_t3037409423 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m1495482491 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t320819310  Color_get_black_m4120693049 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t2499566028  Color32_op_Implicit_m4104578668 (RuntimeObject * __this /* static, unused */, Color_t320819310  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m2324518131 (Texture2D_t2870930912 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m3192056770 (Object_t1008057425 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m3889745670 (Color32_t2499566028 * __this, uint8_t p0, uint8_t p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m3029225869 (Texture2D_t2870930912 * __this, Color32U5BU5D_t636217733* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m4102782407 (Texture2D_t2870930912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
extern "C"  void RawImage_set_texture_m337957835 (RawImage_t3037409423 * __this, Texture_t1132728222 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1383055445 (Rect_t1992046353 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::set_uvRect(UnityEngine.Rect)
extern "C"  void RawImage_set_uvRect_m2592252070 (RawImage_t3037409423 * __this, Rect_t1992046353  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.InputField>()
#define Component_GetComponent_TisInputField_t3161868630_m1543344974(__this, method) ((  InputField_t3161868630 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::get_onEndEdit()
extern "C"  SubmitEvent_t1556243238 * InputField_get_onEndEdit_m261907968 (InputField_t3161868630 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`1<System.String>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m2651916199(__this, p0, p1, method) ((  void (*) (UnityAction_1_t1336681460 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))UnityAction_1__ctor_m1269287345_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_AddListener_m3737383032(__this, p0, method) ((  void (*) (UnityEvent_1_t3752985527 *, UnityAction_1_t1336681460 *, const RuntimeMethod*))UnityEvent_1_AddListener_m2358688097_gshared)(__this, p0, method)
// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::get_onValueChanged()
extern "C"  OnChangeEvent_t2953828243 * InputField_get_onValueChanged_m1315780744 (InputField_t3161868630 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_RemoveListener_m3968893251(__this, p0, method) ((  void (*) (UnityEvent_1_t3752985527 *, UnityAction_1_t1336681460 *, const RuntimeMethod*))UnityEvent_1_RemoveListener_m313363161_gshared)(__this, p0, method)
// System.String HexColorField::ColorToHex(UnityEngine.Color32)
extern "C"  String_t* HexColorField_ColorToHex_m4216070677 (HexColorField_t1392034802 * __this, Color32_t2499566028  ___color0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField::set_text(System.String)
extern "C"  void InputField_set_text_m97417596 (InputField_t3161868630 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HexColorField::HexToColor(System.String,UnityEngine.Color32&)
extern "C"  bool HexColorField_HexToColor_m2425132568 (RuntimeObject * __this /* static, unused */, String_t* ___hex0, Color32_t2499566028 * ___color1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t320819310  Color32_op_Implicit_m3656906645 (RuntimeObject * __this /* static, unused */, Color32_t2499566028  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m2782172005 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m3337466354 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t3384890222* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* String_Format_m223808606 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Regex::IsMatch(System.String,System.String)
extern "C"  bool Regex_IsMatch_m11553654 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m2373373075 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m1547709748 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1525098696 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Byte::Parse(System.String,System.Globalization.NumberStyles)
extern "C"  uint8_t Byte_Parse_m1387626950 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m1478298497 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m4154442594 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>::.ctor()
#define UnityEvent_3__ctor_m3450248554(__this, method) ((  void (*) (UnityEvent_3_t876514367 *, const RuntimeMethod*))UnityEvent_3__ctor_m3450248554_gshared)(__this, method)
// System.Void HsvColor::.ctor(System.Double,System.Double,System.Double)
extern "C"  void HsvColor__ctor_m3228940163 (HsvColor_t3806242730 * __this, double ___h0, double ___s1, double ___v2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HsvColor::set_normalizedH(System.Single)
extern "C"  void HsvColor_set_normalizedH_m3701493110 (HsvColor_t3806242730 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HsvColor::set_normalizedS(System.Single)
extern "C"  void HsvColor_set_normalizedS_m1285062888 (HsvColor_t3806242730 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HsvColor::set_normalizedV(System.Single)
extern "C"  void HsvColor_set_normalizedV_m3696861371 (HsvColor_t3806242730 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Double::ToString(System.String)
extern "C"  String_t* Double_ToString_m519012613 (double* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m974927111 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1448570014* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HsvColor::ToString()
extern "C"  String_t* HsvColor_ToString_m1904641755 (HsvColor_t3806242730 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HsvColor HSVUtil::ConvertRgbToHsv(System.Double,System.Double,System.Double)
extern "C"  HsvColor_t3806242730  HSVUtil_ConvertRgbToHsv_m4081435738 (RuntimeObject * __this /* static, unused */, double ___r0, double ___b1, double ___g2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Min(System.Double,System.Double)
extern "C"  double Math_Min_m3975942609 (RuntimeObject * __this /* static, unused */, double p0, double p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Max(System.Double,System.Double)
extern "C"  double Math_Max_m3206487624 (RuntimeObject * __this /* static, unused */, double p0, double p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t1557311624_m834402472(__this, method) ((  Animation_t1557311624 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t596762001  Vector3_get_forward_m4168834013 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m1087255831 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t596762001  Vector3_op_Multiply_m3577861744 (RuntimeObject * __this /* static, unused */, Vector3_t596762001  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t596762001  Transform_get_localScale_m1140650081 (Transform_t3933397867 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m1352386541 (Transform_t3933397867 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t596762001  Vector3_get_back_m2431645669 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::get_isPlaying()
extern "C"  bool Animation_get_isPlaying_m2318637521 (Animation_t1557311624 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Stop(System.String)
extern "C"  void Animation_Stop_m239272431 (Animation_t1557311624 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void KnightControl::Wait()
extern "C"  void KnightControl_Wait_m4054482282 (KnightControl_t1902388948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::IsPlaying(System.String)
extern "C"  bool Animation_IsPlaying_m843324840 (Animation_t1557311624 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C"  bool Animation_Play_m2990701052 (Animation_t1557311624 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
extern "C"  AnimationState_t2216918921 * Animation_get_Item_m86925464 (Animation_t1557311624 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C"  void AnimationState_set_speed_m1524741311 (AnimationState_t2216918921 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Stop()
extern "C"  void Animation_Stop_m2243275541 (Animation_t1557311624 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m4062517734 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern "C"  bool GUI_Button_m1880561377 (RuntimeObject * __this /* static, unused */, Rect_t1992046353  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ModeSwitcher::EnableBallCreation(System.Boolean)
extern "C"  void ModeSwitcher_EnableBallCreation_m1117476675 (ModeSwitcher_t833699841 * __this, bool ___enable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t596762001  Vector3_get_zero_m325886990 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t320819310  Color_get_white_m3957571917 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void ARFrameUpdate__ctor_m722081207 (ARFrameUpdate_t3333962149 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1467721625 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t3333962149 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.ParticleSystem>(!!0)
#define Object_Instantiate_TisParticleSystem_t1777616458_m895722352(__this /* static, unused */, p0, method) ((  ParticleSystem_t1777616458 * (*) (RuntimeObject * /* static, unused */, ParticleSystem_t1777616458 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m3859482884_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m3365215536(__this, method) ((  void (*) (List_1_t3635081580 *, const RuntimeMethod*))List_1__ctor_m3365215536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::.ctor()
#define List_1__ctor_m3297558810(__this, method) ((  void (*) (List_1_t520968741 *, const RuntimeMethod*))List_1__ctor_m112108964_gshared)(__this, method)
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetColumn_m567234837 (Matrix4x4_t1288378485 * __this, int32_t p0, Vector4_t1376926224  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t596762001  Transform_get_forward_m3993346221 (Transform_t3933397867 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t596762001  Vector3_op_Addition_m901452055 (RuntimeObject * __this /* static, unused */, Vector3_t596762001  p0, Vector3_t596762001  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m3935979045 (RuntimeObject * __this /* static, unused */, Vector3_t596762001  p0, Vector3_t596762001  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
#define List_1_Add_m684783150(__this, p0, method) ((  void (*) (List_1_t3635081580 *, Vector3_t596762001 , const RuntimeMethod*))List_1_Add_m684783150_gshared)(__this, p0, method)
// System.Void ParticlePainter::RestartPainting()
extern "C"  void ParticlePainter_RestartPainting_m2557631278 (ParticlePainter_t294017160 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Add(!0)
#define List_1_Add_m1608859291(__this, p0, method) ((  void (*) (List_1_t520968741 *, ParticleSystem_t1777616458 *, const RuntimeMethod*))List_1_Add_m2991374128_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
#define List_1_get_Count_m3739057527(__this, method) ((  int32_t (*) (List_1_t3635081580 *, const RuntimeMethod*))List_1_get_Count_m3739057527_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
#define List_1_GetEnumerator_m2550167595(__this, method) ((  Enumerator_t688973221  (*) (List_1_t3635081580 *, const RuntimeMethod*))List_1_GetEnumerator_m2550167595_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
#define Enumerator_get_Current_m1534598271(__this, method) ((  Vector3_t596762001  (*) (Enumerator_t688973221 *, const RuntimeMethod*))Enumerator_get_Current_m1534598271_gshared)(__this, method)
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m2752520204 (Particle_t1268635397 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C"  void Particle_set_startColor_m577033657 (Particle_t1268635397 * __this, Color32_t2499566028  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C"  void Particle_set_startSize_m2538937397 (Particle_t1268635397 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
#define Enumerator_MoveNext_m2323590328(__this, method) ((  bool (*) (Enumerator_t688973221 *, const RuntimeMethod*))Enumerator_MoveNext_m2323590328_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
#define Enumerator_Dispose_m3684835367(__this, method) ((  void (*) (Enumerator_t688973221 *, const RuntimeMethod*))Enumerator_Dispose_m3684835367_gshared)(__this, method)
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern "C"  void ParticleSystem_SetParticles_m1829004744 (ParticleSystem_t1777616458 * __this, ParticleU5BU5D_t419044168* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m1060467690 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Application::get_dataPath()
extern "C"  String_t* Application_get_dataPath_m192717127 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::get_Now()
extern "C"  DateTime_t1819153659  DateTime_get_Now_m938408713 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTime::ToString(System.String)
extern "C"  String_t* DateTime_ToString_m1624830502 (DateTime_t1819153659 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m716071259 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScreenCapture::CaptureScreenshot(System.String)
extern "C"  void ScreenCapture_CaptureScreenshot_m4292871561 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SlimeControl::Wait()
extern "C"  void SlimeControl_Wait_m2044302504 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m475641833 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.BoxSlider>()
#define Component_GetComponent_TisBoxSlider_t2780260910_m43308176(__this, method) ((  BoxSlider_t2780260910 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// System.Void SVBoxSlider::RegenerateSVTexture()
extern "C"  void SVBoxSlider_RegenerateSVTexture_m4214705435 (SVBoxSlider_t2769455468 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::get_onValueChanged()
extern "C"  BoxSliderEvent_t2026169079 * BoxSlider_get_onValueChanged_m180314062 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`2<System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
#define UnityAction_2__ctor_m4219090612(__this, p0, p1, method) ((  void (*) (UnityAction_2_t3878312970 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))UnityAction_2__ctor_m4219090612_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Single,System.Single>::AddListener(UnityEngine.Events.UnityAction`2<!0,!1>)
#define UnityEvent_2_AddListener_m3369274527(__this, p0, method) ((  void (*) (UnityEvent_2_t1533210863 *, UnityAction_2_t3878312970 *, const RuntimeMethod*))UnityEvent_2_AddListener_m3369274527_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Single,System.Single>::RemoveListener(UnityEngine.Events.UnityAction`2<!0,!1>)
#define UnityEvent_2_RemoveListener_m1528257211(__this, p0, method) ((  void (*) (UnityEvent_2_t1533210863 *, UnityAction_2_t3878312970 *, const RuntimeMethod*))UnityEvent_2_RemoveListener_m1528257211_gshared)(__this, p0, method)
// System.Single UnityEngine.UI.BoxSlider::get_normalizedValue()
extern "C"  float BoxSlider_get_normalizedValue_m1950536199 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_normalizedValue(System.Single)
extern "C"  void BoxSlider_set_normalizedValue_m1073637371 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_normalizedValueY()
extern "C"  float BoxSlider_get_normalizedValueY_m266672788 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_normalizedValueY(System.Single)
extern "C"  void BoxSlider_set_normalizedValueY_m815891677 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m761225203 (Texture2D_t2870930912 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, Color32U5BU5D_t636217733* p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3619854127 (Vector2_t59524482 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t59524482  Vector2_get_zero_m3701231313 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t3165733013  Transform_get_localRotation_m3136276044 (Transform_t3933397867 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t596762001  Input_get_mousePosition_m443992087 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m2303689024 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m934937530 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t59524482  Vector2_Lerp_m1060570619 (RuntimeObject * __this /* static, unused */, Vector2_t59524482  p0, Vector2_t59524482  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t3165733013  Quaternion_Euler_m437352610 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t3165733013  Quaternion_op_Multiply_m332349538 (RuntimeObject * __this /* static, unused */, Quaternion_t3165733013  p0, Quaternion_t3165733013  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m1950704683 (Transform_t3933397867 * __this, Quaternion_t3165733013  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider/BoxSliderEvent::.ctor()
extern "C"  void BoxSliderEvent__ctor_m2331793288 (BoxSliderEvent_t2026169079 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Selectable::.ctor()
extern "C"  void Selectable__ctor_m1063272198 (Selectable_t3890617260 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.BoxSlider::SetClass<UnityEngine.RectTransform>(T&,T)
#define BoxSlider_SetClass_TisRectTransform_t1885177139_m1931764714(__this /* static, unused */, ___currentValue0, ___newValue1, method) ((  bool (*) (RuntimeObject * /* static, unused */, RectTransform_t1885177139 **, RectTransform_t1885177139 *, const RuntimeMethod*))BoxSlider_SetClass_TisRuntimeObject_m4172831646_gshared)(__this /* static, unused */, ___currentValue0, ___newValue1, method)
// System.Void UnityEngine.UI.BoxSlider::UpdateCachedReferences()
extern "C"  void BoxSlider_UpdateCachedReferences_m977902596 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::UpdateVisuals()
extern "C"  void BoxSlider_UpdateVisuals_m4195678704 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.BoxSlider::SetStruct<System.Single>(T&,T)
#define BoxSlider_SetStruct_TisSingle_t2847614712_m250040320(__this /* static, unused */, ___currentValue0, ___newValue1, method) ((  bool (*) (RuntimeObject * /* static, unused */, float*, float, const RuntimeMethod*))BoxSlider_SetStruct_TisSingle_t2847614712_m250040320_gshared)(__this /* static, unused */, ___currentValue0, ___newValue1, method)
// System.Void UnityEngine.UI.BoxSlider::Set(System.Single)
extern "C"  void BoxSlider_Set_m1701966247 (BoxSlider_t2780260910 * __this, float ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::SetY(System.Single)
extern "C"  void BoxSlider_SetY_m3783125856 (BoxSlider_t2780260910 * __this, float ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.BoxSlider::SetStruct<System.Boolean>(T&,T)
#define BoxSlider_SetStruct_TisBoolean_t362855854_m292852063(__this /* static, unused */, ___currentValue0, ___newValue1, method) ((  bool (*) (RuntimeObject * /* static, unused */, bool*, bool, const RuntimeMethod*))BoxSlider_SetStruct_TisBoolean_t362855854_m292852063_gshared)(__this /* static, unused */, ___currentValue0, ___newValue1, method)
// System.Boolean UnityEngine.UI.BoxSlider::get_wholeNumbers()
extern "C"  bool BoxSlider_get_wholeNumbers_m366103797 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_minValue()
extern "C"  float BoxSlider_get_minValue_m43333998 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_maxValue()
extern "C"  float BoxSlider_get_maxValue_m1548328961 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m2110007471 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_value()
extern "C"  float BoxSlider_get_value_m1655722908 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_InverseLerp_m4028041258 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m44200011 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_value(System.Single)
extern "C"  void BoxSlider_set_value_m2367170594 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.BoxSlider::get_valueY()
extern "C"  float BoxSlider_get_valueY_m1180997838 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::set_valueY(System.Single)
extern "C"  void BoxSlider_set_valueY_m3094914561 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Selectable::OnEnable()
extern "C"  void Selectable_OnEnable_m1471812096 (Selectable_t3890617260 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::Set(System.Single,System.Boolean)
extern "C"  void BoxSlider_Set_m3176380998 (BoxSlider_t2780260910 * __this, float ___input0, bool ___sendCallback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::SetY(System.Single,System.Boolean)
extern "C"  void BoxSlider_SetY_m3865330729 (BoxSlider_t2780260910 * __this, float ___input0, bool ___sendCallback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m1227380991 (DrivenRectTransformTracker_t1306825633 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Selectable::OnDisable()
extern "C"  void Selectable_OnDisable_m2837870712 (Selectable_t3890617260 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3672469481 (RuntimeObject * __this /* static, unused */, Object_t1008057425 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3933397867 * Transform_get_parent_m901998732 (Transform_t3933397867 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t1885177139_m2102537755(__this, method) ((  RectTransform_t1885177139 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`2<System.Single,System.Single>::Invoke(!0,!1)
#define UnityEvent_2_Invoke_m2732613775(__this, p0, p1, method) ((  void (*) (UnityEvent_2_t1533210863 *, float, float, const RuntimeMethod*))UnityEvent_2_Invoke_m2732613775_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.EventSystems.UIBehaviour::OnRectTransformDimensionsChange()
extern "C"  void UIBehaviour_OnRectTransformDimensionsChange_m1031470322 (UIBehaviour_t2441083007 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m3357982639 (DrivenRectTransformTracker_t1306825633 * __this, Object_t1008057425 * p0, RectTransform_t1885177139 * p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t59524482  Vector2_get_one_m242331669 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m4254885928 (Vector2_t59524482 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m1050503853 (RectTransform_t1885177139 * __this, Vector2_t59524482  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2185008930 (RectTransform_t1885177139 * __this, Vector2_t59524482  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t1992046353  RectTransform_get_rect_m562447867 (RectTransform_t1885177139 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t59524482  Rect_get_size_m455598348 (Rect_t1992046353 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m3603954497 (Vector2_t59524482 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
extern "C"  Vector2_t59524482  PointerEventData_get_position_m4019811242 (PointerEventData_t1563322019 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m3576743012 (RuntimeObject * __this /* static, unused */, RectTransform_t1885177139 * p0, Vector2_t59524482  p1, Camera_t226495598 * p2, Vector2_t59524482 * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t59524482  Rect_get_position_m2983583565 (Rect_t1992046353 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t59524482  Vector2_op_Subtraction_m49443056 (RuntimeObject * __this /* static, unused */, Vector2_t59524482  p0, Vector2_t59524482  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m3153283104 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::get_button()
extern "C"  int32_t PointerEventData_get_button_m2484257731 (PointerEventData_t1563322019 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.BoxSlider::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  bool BoxSlider_MayDrag_m1768002497 (BoxSlider_t2780260910 * __this, PointerEventData_t1563322019 * ___eventData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Selectable::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Selectable_OnPointerDown_m3466118125 (Selectable_t3890617260 * __this, PointerEventData_t1563322019 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.EventSystems.PointerEventData::get_enterEventCamera()
extern "C"  Camera_t226495598 * PointerEventData_get_enterEventCamera_m4139929799 (PointerEventData_t1563322019 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m326099737 (RuntimeObject * __this /* static, unused */, RectTransform_t1885177139 * p0, Vector2_t59524482  p1, Camera_t226495598 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.EventSystems.PointerEventData::get_pressEventCamera()
extern "C"  Camera_t226495598 * PointerEventData_get_pressEventCamera_m2916200056 (PointerEventData_t1563322019 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BoxSlider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
extern "C"  void BoxSlider_UpdateDrag_m2830268571 (BoxSlider_t2780260910 * __this, PointerEventData_t1563322019 * ___eventData0, Camera_t226495598 * ___cam1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_useDragThreshold(System.Boolean)
extern "C"  void PointerEventData_set_useDragThreshold_m4047126439 (PointerEventData_t1563322019 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsDestroyed()
extern "C"  bool UIBehaviour_IsDestroyed_m2514623896 (UIBehaviour_t2441083007 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`2<System.Single,System.Single>::.ctor()
#define UnityEvent_2__ctor_m1813845425(__this, method) ((  void (*) (UnityEvent_2_t1533210863 *, const RuntimeMethod*))UnityEvent_2__ctor_m1813845425_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Light>()
#define Component_GetComponent_TisLight_t2513018095_m2996527359(__this, method) ((  Light_t2513018095 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARAmbientIntensity()
extern "C"  float UnityARSessionNativeInterface_GetARAmbientIntensity_m2324758640 (UnityARSessionNativeInterface_t3869411053 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C"  void Light_set_intensity_m3261273135 (Light_t2513018095 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BallMaker::.ctor()
extern "C"  void BallMaker__ctor_m901909986 (BallMaker_t1786680372 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BallMaker::Start()
extern "C"  void BallMaker_Start_m309396839 (BallMaker_t1786680372 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BallMaker_Start_m309396839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MaterialPropertyBlock_t2709947369 * L_0 = (MaterialPropertyBlock_t2709947369 *)il2cpp_codegen_object_new(MaterialPropertyBlock_t2709947369_il2cpp_TypeInfo_var);
		MaterialPropertyBlock__ctor_m1005441945(L_0, /*hidden argument*/NULL);
		__this->set_props_4(L_0);
		return;
	}
}
// System.Void BallMaker::CreateBall(UnityEngine.Vector3)
extern "C"  void BallMaker_CreateBall_m20175966 (BallMaker_t1786680372 * __this, Vector3_t596762001  ___atPosition0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BallMaker_CreateBall_m20175966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t3649338848 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	MeshRenderer_t1885265068 * V_4 = NULL;
	{
		GameObject_t3649338848 * L_0 = __this->get_ballPrefab_2();
		Vector3_t596762001  L_1 = ___atPosition0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3165733013_il2cpp_TypeInfo_var);
		Quaternion_t3165733013  L_2 = Quaternion_get_identity_m2874084265(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		GameObject_t3649338848 * L_3 = Object_Instantiate_TisGameObject_t3649338848_m3594403643(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3649338848_m3594403643_RuntimeMethod_var);
		V_0 = L_3;
		float L_4 = Random_Range_m3146616867(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = Random_Range_m3146616867(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Random_Range_m3146616867(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_3 = L_6;
		MaterialPropertyBlock_t2709947369 * L_7 = __this->get_props_4();
		float L_8 = V_1;
		float L_9 = V_2;
		float L_10 = V_3;
		Color_t320819310  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m2728568087((&L_11), L_8, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		MaterialPropertyBlock_SetColor_m2611042094(L_7, _stringLiteral2735595319, L_11, /*hidden argument*/NULL);
		GameObject_t3649338848 * L_12 = V_0;
		NullCheck(L_12);
		MeshRenderer_t1885265068 * L_13 = GameObject_GetComponent_TisMeshRenderer_t1885265068_m3314343886(L_12, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1885265068_m3314343886_RuntimeMethod_var);
		V_4 = L_13;
		MeshRenderer_t1885265068 * L_14 = V_4;
		MaterialPropertyBlock_t2709947369 * L_15 = __this->get_props_4();
		NullCheck(L_14);
		Renderer_SetPropertyBlock_m656619281(L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BallMaker::Update()
extern "C"  void BallMaker_Update_m3527128747 (BallMaker_t1786680372 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BallMaker_Update_m3527128747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t3132414106  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t596762001  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ARPoint_t1649490841  V_2;
	memset(&V_2, 0, sizeof(V_2));
	ARPoint_t1649490841  V_3;
	memset(&V_3, 0, sizeof(V_3));
	List_1_t547075962 * V_4 = NULL;
	ARHitTestResult_t1803723679  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Enumerator_t1895934899  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t596762001  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1121709983_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m4114401638(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_00ea;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1121709983_il2cpp_TypeInfo_var);
		Touch_t3132414106  L_1 = Input_GetTouch_m2909242247(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = Touch_get_phase_m993859056((&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_00ea;
		}
	}
	{
		Camera_t226495598 * L_3 = Camera_get_main_m2671983186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t59524482  L_4 = Touch_get_position_m2291334870((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_5 = Vector2_op_Implicit_m2201252505(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t596762001  L_6 = Camera_ScreenToViewportPoint_m474730427(L_3, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Initobj (ARPoint_t1649490841_il2cpp_TypeInfo_var, (&V_3));
		float L_7 = (&V_1)->get_x_1();
		(&V_3)->set_x_0((((double)((double)L_7))));
		float L_8 = (&V_1)->get_y_2();
		(&V_3)->set_y_1((((double)((double)L_8))));
		ARPoint_t1649490841  L_9 = V_3;
		V_2 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_10 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARPoint_t1649490841  L_11 = V_2;
		NullCheck(L_10);
		List_1_t547075962 * L_12 = UnityARSessionNativeInterface_HitTest_m1126351971(L_10, L_11, (((int64_t)((int64_t)((int32_t)16)))), /*hidden argument*/NULL);
		V_4 = L_12;
		List_1_t547075962 * L_13 = V_4;
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m655066853(L_13, /*hidden argument*/List_1_get_Count_m655066853_RuntimeMethod_var);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_00ea;
		}
	}
	{
		List_1_t547075962 * L_15 = V_4;
		NullCheck(L_15);
		Enumerator_t1895934899  L_16 = List_1_GetEnumerator_m2437281809(L_15, /*hidden argument*/List_1_GetEnumerator_m2437281809_RuntimeMethod_var);
		V_6 = L_16;
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cb;
		}

IL_0088:
		{
			ARHitTestResult_t1803723679  L_17 = Enumerator_get_Current_m3632740371((&V_6), /*hidden argument*/Enumerator_get_Current_m3632740371_RuntimeMethod_var);
			V_5 = L_17;
			Matrix4x4_t1288378485  L_18 = (&V_5)->get_worldTransform_3();
			Vector3_t596762001  L_19 = UnityARMatrixOps_GetPosition_m3553979848(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			V_7 = L_19;
			float L_20 = (&V_7)->get_x_1();
			float L_21 = (&V_7)->get_y_2();
			float L_22 = __this->get_createHeight_3();
			float L_23 = (&V_7)->get_z_3();
			Vector3_t596762001  L_24;
			memset(&L_24, 0, sizeof(L_24));
			Vector3__ctor_m1043686083((&L_24), L_20, ((float)((float)L_21+(float)L_22)), L_23, /*hidden argument*/NULL);
			BallMaker_CreateBall_m20175966(__this, L_24, /*hidden argument*/NULL);
			goto IL_00d7;
		}

IL_00cb:
		{
			bool L_25 = Enumerator_MoveNext_m1007619642((&V_6), /*hidden argument*/Enumerator_MoveNext_m1007619642_RuntimeMethod_var);
			if (L_25)
			{
				goto IL_0088;
			}
		}

IL_00d7:
		{
			IL2CPP_LEAVE(0xEA, FINALLY_00dc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_00dc;
	}

FINALLY_00dc:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3562983614((&V_6), /*hidden argument*/Enumerator_Dispose_m3562983614_RuntimeMethod_var);
		IL2CPP_END_FINALLY(220)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(220)
	{
		IL2CPP_JUMP_TBL(0xEA, IL_00ea)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_00ea:
	{
		return;
	}
}
// System.Void BallMover::.ctor()
extern "C"  void BallMover__ctor_m2595263547 (BallMover_t4283490589 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BallMover::Start()
extern "C"  void BallMover_Start_m3806074666 (BallMover_t4283490589 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void BallMover::CreateMoveBall(UnityEngine.Vector3)
extern "C"  void BallMover_CreateMoveBall_m1567691649 (BallMover_t4283490589 * __this, Vector3_t596762001  ___explodePosition0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BallMover_CreateMoveBall_m1567691649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t3649338848 * L_0 = __this->get_collBallPrefab_2();
		Vector3_t596762001  L_1 = ___explodePosition0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3165733013_il2cpp_TypeInfo_var);
		Quaternion_t3165733013  L_2 = Quaternion_get_identity_m2874084265(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		GameObject_t3649338848 * L_3 = Object_Instantiate_TisGameObject_t3649338848_m3594403643(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3649338848_m3594403643_RuntimeMethod_var);
		__this->set_collBallGO_3(L_3);
		return;
	}
}
// System.Void BallMover::Update()
extern "C"  void BallMover_Update_m3241756666 (BallMover_t4283490589 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BallMover_Update_m3241756666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t3132414106  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t596762001  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ARPoint_t1649490841  V_2;
	memset(&V_2, 0, sizeof(V_2));
	ARPoint_t1649490841  V_3;
	memset(&V_3, 0, sizeof(V_3));
	List_1_t547075962 * V_4 = NULL;
	ARHitTestResult_t1803723679  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Enumerator_t1895934899  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t596762001  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t596762001  V_8;
	memset(&V_8, 0, sizeof(V_8));
	ARPoint_t1649490841  V_9;
	memset(&V_9, 0, sizeof(V_9));
	List_1_t547075962 * V_10 = NULL;
	ARHitTestResult_t1803723679  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Enumerator_t1895934899  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t596762001  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1121709983_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m4114401638(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_01e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1121709983_il2cpp_TypeInfo_var);
		Touch_t3132414106  L_1 = Input_GetTouch_m2909242247(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = Touch_get_phase_m993859056((&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_00d0;
		}
	}
	{
		Camera_t226495598 * L_3 = Camera_get_main_m2671983186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t59524482  L_4 = Touch_get_position_m2291334870((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_5 = Vector2_op_Implicit_m2201252505(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t596762001  L_6 = Camera_ScreenToViewportPoint_m474730427(L_3, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Initobj (ARPoint_t1649490841_il2cpp_TypeInfo_var, (&V_3));
		float L_7 = (&V_1)->get_x_1();
		(&V_3)->set_x_0((((double)((double)L_7))));
		float L_8 = (&V_1)->get_y_2();
		(&V_3)->set_y_1((((double)((double)L_8))));
		ARPoint_t1649490841  L_9 = V_3;
		V_2 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_10 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARPoint_t1649490841  L_11 = V_2;
		NullCheck(L_10);
		List_1_t547075962 * L_12 = UnityARSessionNativeInterface_HitTest_m1126351971(L_10, L_11, (((int64_t)((int64_t)((int32_t)16)))), /*hidden argument*/NULL);
		V_4 = L_12;
		List_1_t547075962 * L_13 = V_4;
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m655066853(L_13, /*hidden argument*/List_1_get_Count_m655066853_RuntimeMethod_var);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_00cb;
		}
	}
	{
		List_1_t547075962 * L_15 = V_4;
		NullCheck(L_15);
		Enumerator_t1895934899  L_16 = List_1_GetEnumerator_m2437281809(L_15, /*hidden argument*/List_1_GetEnumerator_m2437281809_RuntimeMethod_var);
		V_6 = L_16;
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ac;
		}

IL_0088:
		{
			ARHitTestResult_t1803723679  L_17 = Enumerator_get_Current_m3632740371((&V_6), /*hidden argument*/Enumerator_get_Current_m3632740371_RuntimeMethod_var);
			V_5 = L_17;
			Matrix4x4_t1288378485  L_18 = (&V_5)->get_worldTransform_3();
			Vector3_t596762001  L_19 = UnityARMatrixOps_GetPosition_m3553979848(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			V_7 = L_19;
			Vector3_t596762001  L_20 = V_7;
			BallMover_CreateMoveBall_m1567691649(__this, L_20, /*hidden argument*/NULL);
			goto IL_00b8;
		}

IL_00ac:
		{
			bool L_21 = Enumerator_MoveNext_m1007619642((&V_6), /*hidden argument*/Enumerator_MoveNext_m1007619642_RuntimeMethod_var);
			if (L_21)
			{
				goto IL_0088;
			}
		}

IL_00b8:
		{
			IL2CPP_LEAVE(0xCB, FINALLY_00bd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_00bd;
	}

FINALLY_00bd:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3562983614((&V_6), /*hidden argument*/Enumerator_Dispose_m3562983614_RuntimeMethod_var);
		IL2CPP_END_FINALLY(189)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(189)
	{
		IL2CPP_JUMP_TBL(0xCB, IL_00cb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_00cb:
	{
		goto IL_01e6;
	}

IL_00d0:
	{
		int32_t L_22 = Touch_get_phase_m993859056((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)1))))
		{
			goto IL_01c7;
		}
	}
	{
		GameObject_t3649338848 * L_23 = __this->get_collBallGO_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_23, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_01c7;
		}
	}
	{
		Camera_t226495598 * L_25 = Camera_get_main_m2671983186(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t59524482  L_26 = Touch_get_position_m2291334870((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_27 = Vector2_op_Implicit_m2201252505(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t596762001  L_28 = Camera_ScreenToViewportPoint_m474730427(L_25, L_27, /*hidden argument*/NULL);
		V_8 = L_28;
		Initobj (ARPoint_t1649490841_il2cpp_TypeInfo_var, (&V_3));
		float L_29 = (&V_8)->get_x_1();
		(&V_3)->set_x_0((((double)((double)L_29))));
		float L_30 = (&V_8)->get_y_2();
		(&V_3)->set_y_1((((double)((double)L_30))));
		ARPoint_t1649490841  L_31 = V_3;
		V_9 = L_31;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_32 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARPoint_t1649490841  L_33 = V_9;
		NullCheck(L_32);
		List_1_t547075962 * L_34 = UnityARSessionNativeInterface_HitTest_m1126351971(L_32, L_33, (((int64_t)((int64_t)((int32_t)16)))), /*hidden argument*/NULL);
		V_10 = L_34;
		List_1_t547075962 * L_35 = V_10;
		NullCheck(L_35);
		int32_t L_36 = List_1_get_Count_m655066853(L_35, /*hidden argument*/List_1_get_Count_m655066853_RuntimeMethod_var);
		if ((((int32_t)L_36) <= ((int32_t)0)))
		{
			goto IL_01c2;
		}
	}
	{
		List_1_t547075962 * L_37 = V_10;
		NullCheck(L_37);
		Enumerator_t1895934899  L_38 = List_1_GetEnumerator_m2437281809(L_37, /*hidden argument*/List_1_GetEnumerator_m2437281809_RuntimeMethod_var);
		V_12 = L_38;
	}

IL_0156:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01a3;
		}

IL_015b:
		{
			ARHitTestResult_t1803723679  L_39 = Enumerator_get_Current_m3632740371((&V_12), /*hidden argument*/Enumerator_get_Current_m3632740371_RuntimeMethod_var);
			V_11 = L_39;
			Matrix4x4_t1288378485  L_40 = (&V_11)->get_worldTransform_3();
			Vector3_t596762001  L_41 = UnityARMatrixOps_GetPosition_m3553979848(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
			V_13 = L_41;
			GameObject_t3649338848 * L_42 = __this->get_collBallGO_3();
			NullCheck(L_42);
			Transform_t3933397867 * L_43 = GameObject_get_transform_m890220094(L_42, /*hidden argument*/NULL);
			GameObject_t3649338848 * L_44 = __this->get_collBallGO_3();
			NullCheck(L_44);
			Transform_t3933397867 * L_45 = GameObject_get_transform_m890220094(L_44, /*hidden argument*/NULL);
			NullCheck(L_45);
			Vector3_t596762001  L_46 = Transform_get_position_m3654347341(L_45, /*hidden argument*/NULL);
			Vector3_t596762001  L_47 = V_13;
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t596762001_il2cpp_TypeInfo_var);
			Vector3_t596762001  L_48 = Vector3_MoveTowards_m2110482316(NULL /*static, unused*/, L_46, L_47, (0.05f), /*hidden argument*/NULL);
			NullCheck(L_43);
			Transform_set_position_m2789033506(L_43, L_48, /*hidden argument*/NULL);
			goto IL_01af;
		}

IL_01a3:
		{
			bool L_49 = Enumerator_MoveNext_m1007619642((&V_12), /*hidden argument*/Enumerator_MoveNext_m1007619642_RuntimeMethod_var);
			if (L_49)
			{
				goto IL_015b;
			}
		}

IL_01af:
		{
			IL2CPP_LEAVE(0x1C2, FINALLY_01b4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_01b4;
	}

FINALLY_01b4:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3562983614((&V_12), /*hidden argument*/Enumerator_Dispose_m3562983614_RuntimeMethod_var);
		IL2CPP_END_FINALLY(436)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(436)
	{
		IL2CPP_JUMP_TBL(0x1C2, IL_01c2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_01c2:
	{
		goto IL_01e6;
	}

IL_01c7:
	{
		int32_t L_50 = Touch_get_phase_m993859056((&V_0), /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)2)))
		{
			goto IL_01e6;
		}
	}
	{
		GameObject_t3649338848 * L_51 = __this->get_collBallGO_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_Destroy_m1839175560(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		__this->set_collBallGO_3((GameObject_t3649338848 *)NULL);
	}

IL_01e6:
	{
		return;
	}
}
// System.Void Ballz::.ctor()
extern "C"  void Ballz__ctor_m4206650695 (Ballz_t2183783589 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ballz::Start()
extern "C"  void Ballz_Start_m2157705226 (Ballz_t2183783589 * __this, const RuntimeMethod* method)
{
	Vector3_t596762001  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3933397867 * L_0 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t596762001  L_1 = Transform_get_position_m3654347341(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		__this->set_startingY_3(L_2);
		return;
	}
}
// System.Void Ballz::Update()
extern "C"  void Ballz_Update_m184330856 (Ballz_t2183783589 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ballz_Update_m184330856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t596762001  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_startingY_3();
		Transform_t3933397867 * L_1 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t596762001  L_2 = Transform_get_position_m3654347341(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_4 = fabsf(((float)((float)L_0-(float)L_3)));
		float L_5 = __this->get_yDistanceThreshold_2();
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0035;
		}
	}
	{
		GameObject_t3649338848 * L_6 = Component_get_gameObject_m3065601689(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_Destroy_m1839175560(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void ColorChangedEvent::.ctor()
extern "C"  void ColorChangedEvent__ctor_m420103119 (ColorChangedEvent_t347040188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorChangedEvent__ctor_m420103119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m1815935619(__this, /*hidden argument*/UnityEvent_1__ctor_m1815935619_RuntimeMethod_var);
		return;
	}
}
// System.Void ColorImage::.ctor()
extern "C"  void ColorImage__ctor_m3292321612 (ColorImage_t2737809041 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorImage::Awake()
extern "C"  void ColorImage_Awake_m1049520671 (ColorImage_t2737809041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorImage_Awake_m1049520671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t126372159 * L_0 = Component_GetComponent_TisImage_t126372159_m734387587(__this, /*hidden argument*/Component_GetComponent_TisImage_t126372159_m734387587_RuntimeMethod_var);
		__this->set_image_3(L_0);
		ColorPicker_t2582382693 * L_1 = __this->get_picker_2();
		NullCheck(L_1);
		ColorChangedEvent_t347040188 * L_2 = L_1->get_onValueChanged_9();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ColorImage_ColorChanged_m2018191545_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_4 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_4, __this, L_3, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_2);
		UnityEvent_1_AddListener_m2001044386(L_2, L_4, /*hidden argument*/UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var);
		return;
	}
}
// System.Void ColorImage::OnDestroy()
extern "C"  void ColorImage_OnDestroy_m2347837237 (ColorImage_t2737809041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorImage_OnDestroy_m2347837237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		NullCheck(L_0);
		ColorChangedEvent_t347040188 * L_1 = L_0->get_onValueChanged_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ColorImage_ColorChanged_m2018191545_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_3 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_1);
		UnityEvent_1_RemoveListener_m1657194609(L_1, L_3, /*hidden argument*/UnityEvent_1_RemoveListener_m1657194609_RuntimeMethod_var);
		return;
	}
}
// System.Void ColorImage::ColorChanged(UnityEngine.Color)
extern "C"  void ColorImage_ColorChanged_m2018191545 (ColorImage_t2737809041 * __this, Color_t320819310  ___newColor0, const RuntimeMethod* method)
{
	{
		Image_t126372159 * L_0 = __this->get_image_3();
		Color_t320819310  L_1 = ___newColor0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t320819310  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void ColorLabel::.ctor()
extern "C"  void ColorLabel__ctor_m3991178045 (ColorLabel_t538373442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel__ctor_m3991178045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_prefix_4(_stringLiteral1853803690);
		__this->set_maxValue_6((255.0f));
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorLabel::Awake()
extern "C"  void ColorLabel_Awake_m2026245735 (ColorLabel_t538373442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_Awake_m2026245735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t3069741234 * L_0 = Component_GetComponent_TisText_t3069741234_m3241350479(__this, /*hidden argument*/Component_GetComponent_TisText_t3069741234_m3241350479_RuntimeMethod_var);
		__this->set_label_8(L_0);
		return;
	}
}
// System.Void ColorLabel::OnEnable()
extern "C"  void ColorLabel_OnEnable_m2729080183 (ColorLabel_t538373442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_OnEnable_m2729080183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2992201115(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		ColorPicker_t2582382693 * L_1 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		ColorPicker_t2582382693 * L_3 = __this->get_picker_2();
		NullCheck(L_3);
		ColorChangedEvent_t347040188 * L_4 = L_3->get_onValueChanged_9();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)ColorLabel_ColorChanged_m2301408063_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_6 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_6, __this, L_5, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_4);
		UnityEvent_1_AddListener_m2001044386(L_4, L_6, /*hidden argument*/UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_7 = __this->get_picker_2();
		NullCheck(L_7);
		HSVChangedEvent_t3546572410 * L_8 = L_7->get_onHSVChanged_10();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)ColorLabel_HSVChanged_m4075465524_RuntimeMethod_var);
		UnityAction_3_t3771153942 * L_10 = (UnityAction_3_t3771153942 *)il2cpp_codegen_object_new(UnityAction_3_t3771153942_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m672494230(L_10, __this, L_9, /*hidden argument*/UnityAction_3__ctor_m672494230_RuntimeMethod_var);
		NullCheck(L_8);
		UnityEvent_3_AddListener_m3442876925(L_8, L_10, /*hidden argument*/UnityEvent_3_AddListener_m3442876925_RuntimeMethod_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void ColorLabel::OnDestroy()
extern "C"  void ColorLabel_OnDestroy_m4045402800 (ColorLabel_t538373442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_OnDestroy_m4045402800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		ColorPicker_t2582382693 * L_2 = __this->get_picker_2();
		NullCheck(L_2);
		ColorChangedEvent_t347040188 * L_3 = L_2->get_onValueChanged_9();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)ColorLabel_ColorChanged_m2301408063_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_5 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_5, __this, L_4, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_3);
		UnityEvent_1_RemoveListener_m1657194609(L_3, L_5, /*hidden argument*/UnityEvent_1_RemoveListener_m1657194609_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_6 = __this->get_picker_2();
		NullCheck(L_6);
		HSVChangedEvent_t3546572410 * L_7 = L_6->get_onHSVChanged_10();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)ColorLabel_HSVChanged_m4075465524_RuntimeMethod_var);
		UnityAction_3_t3771153942 * L_9 = (UnityAction_3_t3771153942 *)il2cpp_codegen_object_new(UnityAction_3_t3771153942_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m672494230(L_9, __this, L_8, /*hidden argument*/UnityAction_3__ctor_m672494230_RuntimeMethod_var);
		NullCheck(L_7);
		UnityEvent_3_RemoveListener_m2900402221(L_7, L_9, /*hidden argument*/UnityEvent_3_RemoveListener_m2900402221_RuntimeMethod_var);
	}

IL_0049:
	{
		return;
	}
}
// System.Void ColorLabel::ColorChanged(UnityEngine.Color)
extern "C"  void ColorLabel_ColorChanged_m2301408063 (ColorLabel_t538373442 * __this, Color_t320819310  ___color0, const RuntimeMethod* method)
{
	{
		ColorLabel_UpdateValue_m2990534176(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorLabel::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorLabel_HSVChanged_m4075465524 (ColorLabel_t538373442 * __this, float ___hue0, float ___sateration1, float ___value2, const RuntimeMethod* method)
{
	{
		ColorLabel_UpdateValue_m2990534176(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorLabel::UpdateValue()
extern "C"  void ColorLabel_UpdateValue_m2990534176 (ColorLabel_t538373442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_UpdateValue_m2990534176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m528327324(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Text_t3069741234 * L_2 = __this->get_label_8();
		String_t* L_3 = __this->get_prefix_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2023871361(NULL /*static, unused*/, L_3, _stringLiteral3581513187, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		goto IL_0075;
	}

IL_0031:
	{
		float L_5 = __this->get_minValue_5();
		ColorPicker_t2582382693 * L_6 = __this->get_picker_2();
		int32_t L_7 = __this->get_type_3();
		NullCheck(L_6);
		float L_8 = ColorPicker_GetValue_m3746747326(L_6, L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_maxValue_6();
		float L_10 = __this->get_minValue_5();
		V_0 = ((float)((float)L_5+(float)((float)((float)L_8*(float)((float)((float)L_9-(float)L_10))))));
		Text_t3069741234 * L_11 = __this->get_label_8();
		String_t* L_12 = __this->get_prefix_4();
		float L_13 = V_0;
		String_t* L_14 = ColorLabel_ConvertToDisplayString_m3783319992(__this, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2023871361(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_15);
	}

IL_0075:
	{
		return;
	}
}
// System.String ColorLabel::ConvertToDisplayString(System.Single)
extern "C"  String_t* ColorLabel_ConvertToDisplayString_m3783319992 (ColorLabel_t538373442 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorLabel_ConvertToDisplayString_m3783319992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_precision_7();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = __this->get_precision_7();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t3425510919_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3706473663(NULL /*static, unused*/, _stringLiteral3638548537, L_3, /*hidden argument*/NULL);
		String_t* L_5 = Single_ToString_m1876090627((&___value0), L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0029:
	{
		float L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_FloorToInt_m464692734(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		String_t* L_8 = Int32_ToString_m969347708((&V_0), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void ColorPicker::.ctor()
extern "C"  void ColorPicker__ctor_m3964538034 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPicker__ctor_m3964538034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__alpha_8((1.0f));
		ColorChangedEvent_t347040188 * L_0 = (ColorChangedEvent_t347040188 *)il2cpp_codegen_object_new(ColorChangedEvent_t347040188_il2cpp_TypeInfo_var);
		ColorChangedEvent__ctor_m420103119(L_0, /*hidden argument*/NULL);
		__this->set_onValueChanged_9(L_0);
		HSVChangedEvent_t3546572410 * L_1 = (HSVChangedEvent_t3546572410 *)il2cpp_codegen_object_new(HSVChangedEvent_t3546572410_il2cpp_TypeInfo_var);
		HSVChangedEvent__ctor_m2671244103(L_1, /*hidden argument*/NULL);
		__this->set_onHSVChanged_10(L_1);
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color ColorPicker::get_CurrentColor()
extern "C"  Color_t320819310  ColorPicker_get_CurrentColor_m3098018243 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__red_5();
		float L_1 = __this->get__green_6();
		float L_2 = __this->get__blue_7();
		float L_3 = __this->get__alpha_8();
		Color_t320819310  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m1438150939((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void ColorPicker::set_CurrentColor(UnityEngine.Color)
extern "C"  void ColorPicker_set_CurrentColor_m2827670769 (ColorPicker_t2582382693 * __this, Color_t320819310  ___value0, const RuntimeMethod* method)
{
	{
		Color_t320819310  L_0 = ColorPicker_get_CurrentColor_m3098018243(__this, /*hidden argument*/NULL);
		Color_t320819310  L_1 = ___value0;
		bool L_2 = Color_op_Equality_m441720939(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		float L_3 = (&___value0)->get_r_0();
		__this->set__red_5(L_3);
		float L_4 = (&___value0)->get_g_1();
		__this->set__green_6(L_4);
		float L_5 = (&___value0)->get_b_2();
		__this->set__blue_7(L_5);
		float L_6 = (&___value0)->get_a_3();
		__this->set__alpha_8(L_6);
		ColorPicker_RGBChanged_m676920536(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPicker::Start()
extern "C"  void ColorPicker_Start_m890608673 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_H()
extern "C"  float ColorPicker_get_H_m1137646974 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__hue_2();
		return L_0;
	}
}
// System.Void ColorPicker::set_H(System.Single)
extern "C"  void ColorPicker_set_H_m1627930837 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__hue_2();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__hue_2(L_2);
		ColorPicker_HSVChanged_m1999027478(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_S()
extern "C"  float ColorPicker_get_S_m220126169 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__saturation_3();
		return L_0;
	}
}
// System.Void ColorPicker::set_S(System.Single)
extern "C"  void ColorPicker_set_S_m3068359377 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__saturation_3();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__saturation_3(L_2);
		ColorPicker_HSVChanged_m1999027478(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_V()
extern "C"  float ColorPicker_get_V_m286556906 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__brightness_4();
		return L_0;
	}
}
// System.Void ColorPicker::set_V(System.Single)
extern "C"  void ColorPicker_set_V_m3298641509 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__brightness_4();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__brightness_4(L_2);
		ColorPicker_HSVChanged_m1999027478(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_R()
extern "C"  float ColorPicker_get_R_m371651317 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__red_5();
		return L_0;
	}
}
// System.Void ColorPicker::set_R(System.Single)
extern "C"  void ColorPicker_set_R_m1448051533 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__red_5();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__red_5(L_2);
		ColorPicker_RGBChanged_m676920536(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_G()
extern "C"  float ColorPicker_get_G_m908827283 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__green_6();
		return L_0;
	}
}
// System.Void ColorPicker::set_G(System.Single)
extern "C"  void ColorPicker_set_G_m643121640 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__green_6();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__green_6(L_2);
		ColorPicker_RGBChanged_m676920536(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_B()
extern "C"  float ColorPicker_get_B_m2190872939 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__blue_7();
		return L_0;
	}
}
// System.Void ColorPicker::set_B(System.Single)
extern "C"  void ColorPicker_set_B_m3133411859 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__blue_7();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__blue_7(L_2);
		ColorPicker_RGBChanged_m676920536(__this, /*hidden argument*/NULL);
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ColorPicker::get_A()
extern "C"  float ColorPicker_get_A_m340059024 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__alpha_8();
		return L_0;
	}
}
// System.Void ColorPicker::set_A(System.Single)
extern "C"  void ColorPicker_set_A_m263421143 (ColorPicker_t2582382693 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get__alpha_8();
		float L_1 = ___value0;
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		float L_2 = ___value0;
		__this->set__alpha_8(L_2);
		ColorPicker_SendChangedEvent_m1023062679(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPicker::RGBChanged()
extern "C"  void ColorPicker_RGBChanged_m676920536 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	HsvColor_t3806242730  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t320819310  L_0 = ColorPicker_get_CurrentColor_m3098018243(__this, /*hidden argument*/NULL);
		HsvColor_t3806242730  L_1 = HSVUtil_ConvertRgbToHsv_m1523836538(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = HsvColor_get_normalizedH_m2453528821((&V_0), /*hidden argument*/NULL);
		__this->set__hue_2(L_2);
		float L_3 = HsvColor_get_normalizedS_m3606923594((&V_0), /*hidden argument*/NULL);
		__this->set__saturation_3(L_3);
		float L_4 = HsvColor_get_normalizedV_m2333326027((&V_0), /*hidden argument*/NULL);
		__this->set__brightness_4(L_4);
		return;
	}
}
// System.Void ColorPicker::HSVChanged()
extern "C"  void ColorPicker_HSVChanged_m1999027478 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	Color_t320819310  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get__hue_2();
		float L_1 = __this->get__saturation_3();
		float L_2 = __this->get__brightness_4();
		float L_3 = __this->get__alpha_8();
		Color_t320819310  L_4 = HSVUtil_ConvertHsvToRgb_m3345724273(NULL /*static, unused*/, (((double)((double)((float)((float)L_0*(float)(360.0f)))))), (((double)((double)L_1))), (((double)((double)L_2))), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_r_0();
		__this->set__red_5(L_5);
		float L_6 = (&V_0)->get_g_1();
		__this->set__green_6(L_6);
		float L_7 = (&V_0)->get_b_2();
		__this->set__blue_7(L_7);
		return;
	}
}
// System.Void ColorPicker::SendChangedEvent()
extern "C"  void ColorPicker_SendChangedEvent_m1023062679 (ColorPicker_t2582382693 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPicker_SendChangedEvent_m1023062679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorChangedEvent_t347040188 * L_0 = __this->get_onValueChanged_9();
		Color_t320819310  L_1 = ColorPicker_get_CurrentColor_m3098018243(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_1_Invoke_m1164418121(L_0, L_1, /*hidden argument*/UnityEvent_1_Invoke_m1164418121_RuntimeMethod_var);
		HSVChangedEvent_t3546572410 * L_2 = __this->get_onHSVChanged_10();
		float L_3 = __this->get__hue_2();
		float L_4 = __this->get__saturation_3();
		float L_5 = __this->get__brightness_4();
		NullCheck(L_2);
		UnityEvent_3_Invoke_m2348520805(L_2, L_3, L_4, L_5, /*hidden argument*/UnityEvent_3_Invoke_m2348520805_RuntimeMethod_var);
		return;
	}
}
// System.Void ColorPicker::AssignColor(ColorValues,System.Single)
extern "C"  void ColorPicker_AssignColor_m1478515366 (ColorPicker_t2582382693 * __this, int32_t ___type0, float ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___type0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0027;
			}
			case 1:
			{
				goto IL_0033;
			}
			case 2:
			{
				goto IL_003f;
			}
			case 3:
			{
				goto IL_004b;
			}
			case 4:
			{
				goto IL_0057;
			}
			case 5:
			{
				goto IL_0063;
			}
			case 6:
			{
				goto IL_006f;
			}
		}
	}
	{
		goto IL_007b;
	}

IL_0027:
	{
		float L_1 = ___value1;
		ColorPicker_set_R_m1448051533(__this, L_1, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0033:
	{
		float L_2 = ___value1;
		ColorPicker_set_G_m643121640(__this, L_2, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_003f:
	{
		float L_3 = ___value1;
		ColorPicker_set_B_m3133411859(__this, L_3, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_004b:
	{
		float L_4 = ___value1;
		ColorPicker_set_A_m263421143(__this, L_4, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0057:
	{
		float L_5 = ___value1;
		ColorPicker_set_H_m1627930837(__this, L_5, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0063:
	{
		float L_6 = ___value1;
		ColorPicker_set_S_m3068359377(__this, L_6, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_006f:
	{
		float L_7 = ___value1;
		ColorPicker_set_V_m3298641509(__this, L_7, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_007b:
	{
		goto IL_0080;
	}

IL_0080:
	{
		return;
	}
}
// System.Single ColorPicker::GetValue(ColorValues)
extern "C"  float ColorPicker_GetValue_m3746747326 (ColorPicker_t2582382693 * __this, int32_t ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPicker_GetValue_m3746747326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___type0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0027;
			}
			case 1:
			{
				goto IL_002e;
			}
			case 2:
			{
				goto IL_0035;
			}
			case 3:
			{
				goto IL_003c;
			}
			case 4:
			{
				goto IL_0043;
			}
			case 5:
			{
				goto IL_004a;
			}
			case 6:
			{
				goto IL_0051;
			}
		}
	}
	{
		goto IL_0058;
	}

IL_0027:
	{
		float L_1 = ColorPicker_get_R_m371651317(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_002e:
	{
		float L_2 = ColorPicker_get_G_m908827283(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0035:
	{
		float L_3 = ColorPicker_get_B_m2190872939(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_003c:
	{
		float L_4 = ColorPicker_get_A_m340059024(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0043:
	{
		float L_5 = ColorPicker_get_H_m1137646974(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_004a:
	{
		float L_6 = ColorPicker_get_S_m220126169(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_0051:
	{
		float L_7 = ColorPicker_get_V_m286556906(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NotImplementedException_t4033310348 * L_9 = (NotImplementedException_t4033310348 *)il2cpp_codegen_object_new(NotImplementedException_t4033310348_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1129172726(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}
}
// System.Void ColorPickerTester::.ctor()
extern "C"  void ColorPickerTester__ctor_m2356004386 (ColorPickerTester_t3436045112 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPickerTester::Start()
extern "C"  void ColorPickerTester_Start_m3073950392 (ColorPickerTester_t3436045112 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPickerTester_Start_m3073950392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_3();
		NullCheck(L_0);
		ColorChangedEvent_t347040188 * L_1 = L_0->get_onValueChanged_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ColorPickerTester_U3CStartU3Em__0_m1156723077_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_3 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_1);
		UnityEvent_1_AddListener_m2001044386(L_1, L_3, /*hidden argument*/UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var);
		Renderer_t674913088 * L_4 = __this->get_renderer_2();
		NullCheck(L_4);
		Material_t4055262778 * L_5 = Renderer_get_material_m1063092742(L_4, /*hidden argument*/NULL);
		ColorPicker_t2582382693 * L_6 = __this->get_picker_3();
		NullCheck(L_6);
		Color_t320819310  L_7 = ColorPicker_get_CurrentColor_m3098018243(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_color_m3512933588(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPickerTester::Update()
extern "C"  void ColorPickerTester_Update_m693525812 (ColorPickerTester_t3436045112 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ColorPickerTester::<Start>m__0(UnityEngine.Color)
extern "C"  void ColorPickerTester_U3CStartU3Em__0_m1156723077 (ColorPickerTester_t3436045112 * __this, Color_t320819310  ___color0, const RuntimeMethod* method)
{
	{
		Renderer_t674913088 * L_0 = __this->get_renderer_2();
		NullCheck(L_0);
		Material_t4055262778 * L_1 = Renderer_get_material_m1063092742(L_0, /*hidden argument*/NULL);
		Color_t320819310  L_2 = ___color0;
		NullCheck(L_1);
		Material_set_color_m3512933588(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPresets::.ctor()
extern "C"  void ColorPresets__ctor_m3362894943 (ColorPresets_t321881905 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPresets::Awake()
extern "C"  void ColorPresets_Awake_m2155330283 (ColorPresets_t321881905 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPresets_Awake_m2155330283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		NullCheck(L_0);
		ColorChangedEvent_t347040188 * L_1 = L_0->get_onValueChanged_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ColorPresets_ColorChanged_m2470810136_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_3 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_1);
		UnityEvent_1_AddListener_m2001044386(L_1, L_3, /*hidden argument*/UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var);
		return;
	}
}
// System.Void ColorPresets::CreatePresetButton()
extern "C"  void ColorPresets_CreatePresetButton_m932495736 (ColorPresets_t321881905 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorPresets_CreatePresetButton_m932495736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_004d;
	}

IL_0007:
	{
		GameObjectU5BU5D_t1488940705* L_0 = __this->get_presets_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t3649338848 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m3172293468(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0049;
		}
	}
	{
		GameObjectU5BU5D_t1488940705* L_5 = __this->get_presets_3();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t3649338848 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		GameObject_SetActive_m3661033924(L_8, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t1488940705* L_9 = __this->get_presets_3();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_t3649338848 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Image_t126372159 * L_13 = GameObject_GetComponent_TisImage_t126372159_m3108703159(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t126372159_m3108703159_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_14 = __this->get_picker_2();
		NullCheck(L_14);
		Color_t320819310  L_15 = ColorPicker_get_CurrentColor_m3098018243(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t320819310  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_15);
		goto IL_005b;
	}

IL_0049:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_17 = V_0;
		GameObjectU5BU5D_t1488940705* L_18 = __this->get_presets_3();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_18)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_005b:
	{
		return;
	}
}
// System.Void ColorPresets::PresetSelect(UnityEngine.UI.Image)
extern "C"  void ColorPresets_PresetSelect_m3262786394 (ColorPresets_t321881905 * __this, Image_t126372159 * ___sender0, const RuntimeMethod* method)
{
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		Image_t126372159 * L_1 = ___sender0;
		NullCheck(L_1);
		Color_t320819310  L_2 = VirtFuncInvoker0< Color_t320819310  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		NullCheck(L_0);
		ColorPicker_set_CurrentColor_m2827670769(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorPresets::ColorChanged(UnityEngine.Color)
extern "C"  void ColorPresets_ColorChanged_m2470810136 (ColorPresets_t321881905 * __this, Color_t320819310  ___color0, const RuntimeMethod* method)
{
	{
		Image_t126372159 * L_0 = __this->get_createPresetImage_4();
		Color_t320819310  L_1 = ___color0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t320819310  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void ColorSlider::.ctor()
extern "C"  void ColorSlider__ctor_m1920485086 (ColorSlider_t4061812907 * __this, const RuntimeMethod* method)
{
	{
		__this->set_listen_5((bool)1);
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorSlider::Awake()
extern "C"  void ColorSlider_Awake_m3513452837 (ColorSlider_t4061812907 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSlider_Awake_m3513452837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Slider_t301630380 * L_0 = Component_GetComponent_TisSlider_t301630380_m2687307688(__this, /*hidden argument*/Component_GetComponent_TisSlider_t301630380_m2687307688_RuntimeMethod_var);
		__this->set_slider_4(L_0);
		ColorPicker_t2582382693 * L_1 = __this->get_hsvpicker_2();
		NullCheck(L_1);
		ColorChangedEvent_t347040188 * L_2 = L_1->get_onValueChanged_9();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ColorSlider_ColorChanged_m2054110329_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_4 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_4, __this, L_3, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_2);
		UnityEvent_1_AddListener_m2001044386(L_2, L_4, /*hidden argument*/UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_5 = __this->get_hsvpicker_2();
		NullCheck(L_5);
		HSVChangedEvent_t3546572410 * L_6 = L_5->get_onHSVChanged_10();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)ColorSlider_HSVChanged_m3322884794_RuntimeMethod_var);
		UnityAction_3_t3771153942 * L_8 = (UnityAction_3_t3771153942 *)il2cpp_codegen_object_new(UnityAction_3_t3771153942_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m672494230(L_8, __this, L_7, /*hidden argument*/UnityAction_3__ctor_m672494230_RuntimeMethod_var);
		NullCheck(L_6);
		UnityEvent_3_AddListener_m3442876925(L_6, L_8, /*hidden argument*/UnityEvent_3_AddListener_m3442876925_RuntimeMethod_var);
		Slider_t301630380 * L_9 = __this->get_slider_4();
		NullCheck(L_9);
		SliderEvent_t1040040728 * L_10 = Slider_get_onValueChanged_m3507164652(L_9, /*hidden argument*/NULL);
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)ColorSlider_SliderChanged_m2971956828_RuntimeMethod_var);
		UnityAction_1_t3918860037 * L_12 = (UnityAction_1_t3918860037 *)il2cpp_codegen_object_new(UnityAction_1_t3918860037_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m482530400(L_12, __this, L_11, /*hidden argument*/UnityAction_1__ctor_m482530400_RuntimeMethod_var);
		NullCheck(L_10);
		UnityEvent_1_AddListener_m1802892215(L_10, L_12, /*hidden argument*/UnityEvent_1_AddListener_m1802892215_RuntimeMethod_var);
		return;
	}
}
// System.Void ColorSlider::OnDestroy()
extern "C"  void ColorSlider_OnDestroy_m2701140116 (ColorSlider_t4061812907 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSlider_OnDestroy_m2701140116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t2582382693 * L_0 = __this->get_hsvpicker_2();
		NullCheck(L_0);
		ColorChangedEvent_t347040188 * L_1 = L_0->get_onValueChanged_9();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ColorSlider_ColorChanged_m2054110329_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_3 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_1);
		UnityEvent_1_RemoveListener_m1657194609(L_1, L_3, /*hidden argument*/UnityEvent_1_RemoveListener_m1657194609_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_4 = __this->get_hsvpicker_2();
		NullCheck(L_4);
		HSVChangedEvent_t3546572410 * L_5 = L_4->get_onHSVChanged_10();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)ColorSlider_HSVChanged_m3322884794_RuntimeMethod_var);
		UnityAction_3_t3771153942 * L_7 = (UnityAction_3_t3771153942 *)il2cpp_codegen_object_new(UnityAction_3_t3771153942_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m672494230(L_7, __this, L_6, /*hidden argument*/UnityAction_3__ctor_m672494230_RuntimeMethod_var);
		NullCheck(L_5);
		UnityEvent_3_RemoveListener_m2900402221(L_5, L_7, /*hidden argument*/UnityEvent_3_RemoveListener_m2900402221_RuntimeMethod_var);
		Slider_t301630380 * L_8 = __this->get_slider_4();
		NullCheck(L_8);
		SliderEvent_t1040040728 * L_9 = Slider_get_onValueChanged_m3507164652(L_8, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)ColorSlider_SliderChanged_m2971956828_RuntimeMethod_var);
		UnityAction_1_t3918860037 * L_11 = (UnityAction_1_t3918860037 *)il2cpp_codegen_object_new(UnityAction_1_t3918860037_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m482530400(L_11, __this, L_10, /*hidden argument*/UnityAction_1__ctor_m482530400_RuntimeMethod_var);
		NullCheck(L_9);
		UnityEvent_1_RemoveListener_m1915413156(L_9, L_11, /*hidden argument*/UnityEvent_1_RemoveListener_m1915413156_RuntimeMethod_var);
		return;
	}
}
// System.Void ColorSlider::ColorChanged(UnityEngine.Color)
extern "C"  void ColorSlider_ColorChanged_m2054110329 (ColorSlider_t4061812907 * __this, Color_t320819310  ___newColor0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		__this->set_listen_5((bool)0);
		int32_t L_0 = __this->get_type_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_0040;
			}
			case 2:
			{
				goto IL_0057;
			}
			case 3:
			{
				goto IL_006e;
			}
		}
	}
	{
		goto IL_0085;
	}

IL_0029:
	{
		Slider_t301630380 * L_2 = __this->get_slider_4();
		float L_3 = (&___newColor0)->get_r_0();
		NullCheck(L_2);
		Slider_set_normalizedValue_m97916338(L_2, L_3, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0040:
	{
		Slider_t301630380 * L_4 = __this->get_slider_4();
		float L_5 = (&___newColor0)->get_g_1();
		NullCheck(L_4);
		Slider_set_normalizedValue_m97916338(L_4, L_5, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0057:
	{
		Slider_t301630380 * L_6 = __this->get_slider_4();
		float L_7 = (&___newColor0)->get_b_2();
		NullCheck(L_6);
		Slider_set_normalizedValue_m97916338(L_6, L_7, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_006e:
	{
		Slider_t301630380 * L_8 = __this->get_slider_4();
		float L_9 = (&___newColor0)->get_a_3();
		NullCheck(L_8);
		Slider_set_normalizedValue_m97916338(L_8, L_9, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0085:
	{
		goto IL_008a;
	}

IL_008a:
	{
		return;
	}
}
// System.Void ColorSlider::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorSlider_HSVChanged_m3322884794 (ColorSlider_t4061812907 * __this, float ___hue0, float ___saturation1, float ___value2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		__this->set_listen_5((bool)0);
		int32_t L_0 = __this->get_type_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)4)))
		{
			case 0:
			{
				goto IL_0027;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_0049;
			}
		}
	}
	{
		goto IL_005a;
	}

IL_0027:
	{
		Slider_t301630380 * L_2 = __this->get_slider_4();
		float L_3 = ___hue0;
		NullCheck(L_2);
		Slider_set_normalizedValue_m97916338(L_2, L_3, /*hidden argument*/NULL);
		goto IL_005f;
	}

IL_0038:
	{
		Slider_t301630380 * L_4 = __this->get_slider_4();
		float L_5 = ___saturation1;
		NullCheck(L_4);
		Slider_set_normalizedValue_m97916338(L_4, L_5, /*hidden argument*/NULL);
		goto IL_005f;
	}

IL_0049:
	{
		Slider_t301630380 * L_6 = __this->get_slider_4();
		float L_7 = ___value2;
		NullCheck(L_6);
		Slider_set_normalizedValue_m97916338(L_6, L_7, /*hidden argument*/NULL);
		goto IL_005f;
	}

IL_005a:
	{
		goto IL_005f;
	}

IL_005f:
	{
		return;
	}
}
// System.Void ColorSlider::SliderChanged(System.Single)
extern "C"  void ColorSlider_SliderChanged_m2971956828 (ColorSlider_t4061812907 * __this, float ___newValue0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_listen_5();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		Slider_t301630380 * L_1 = __this->get_slider_4();
		NullCheck(L_1);
		float L_2 = Slider_get_normalizedValue_m1903738556(L_1, /*hidden argument*/NULL);
		___newValue0 = L_2;
		ColorPicker_t2582382693 * L_3 = __this->get_hsvpicker_2();
		int32_t L_4 = __this->get_type_3();
		float L_5 = ___newValue0;
		NullCheck(L_3);
		ColorPicker_AssignColor_m1478515366(L_3, L_4, L_5, /*hidden argument*/NULL);
	}

IL_002a:
	{
		__this->set_listen_5((bool)1);
		return;
	}
}
// System.Void ColorSliderImage::.ctor()
extern "C"  void ColorSliderImage__ctor_m469266086 (ColorSliderImage_t664667956 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform ColorSliderImage::get_rectTransform()
extern "C"  RectTransform_t1885177139 * ColorSliderImage_get_rectTransform_m2176888620 (ColorSliderImage_t664667956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_get_rectTransform_m2176888620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3933397867 * L_0 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		return ((RectTransform_t1885177139 *)IsInstSealed((RuntimeObject*)L_0, RectTransform_t1885177139_il2cpp_TypeInfo_var));
	}
}
// System.Void ColorSliderImage::Awake()
extern "C"  void ColorSliderImage_Awake_m1145630542 (ColorSliderImage_t664667956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_Awake_m1145630542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t3037409423 * L_0 = Component_GetComponent_TisRawImage_t3037409423_m801911595(__this, /*hidden argument*/Component_GetComponent_TisRawImage_t3037409423_m801911595_RuntimeMethod_var);
		__this->set_image_5(L_0);
		ColorSliderImage_RegenerateTexture_m629802786(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorSliderImage::OnEnable()
extern "C"  void ColorSliderImage_OnEnable_m2913045470 (ColorSliderImage_t664667956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_OnEnable_m2913045470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		bool L_2 = Application_get_isPlaying_m2992201115(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		ColorPicker_t2582382693 * L_3 = __this->get_picker_2();
		NullCheck(L_3);
		ColorChangedEvent_t347040188 * L_4 = L_3->get_onValueChanged_9();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)ColorSliderImage_ColorChanged_m3348797575_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_6 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_6, __this, L_5, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_4);
		UnityEvent_1_AddListener_m2001044386(L_4, L_6, /*hidden argument*/UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_7 = __this->get_picker_2();
		NullCheck(L_7);
		HSVChangedEvent_t3546572410 * L_8 = L_7->get_onHSVChanged_10();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)ColorSliderImage_HSVChanged_m2525805608_RuntimeMethod_var);
		UnityAction_3_t3771153942 * L_10 = (UnityAction_3_t3771153942 *)il2cpp_codegen_object_new(UnityAction_3_t3771153942_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m672494230(L_10, __this, L_9, /*hidden argument*/UnityAction_3__ctor_m672494230_RuntimeMethod_var);
		NullCheck(L_8);
		UnityEvent_3_AddListener_m3442876925(L_8, L_10, /*hidden argument*/UnityEvent_3_AddListener_m3442876925_RuntimeMethod_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void ColorSliderImage::OnDisable()
extern "C"  void ColorSliderImage_OnDisable_m2796054081 (ColorSliderImage_t664667956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_OnDisable_m2796054081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		ColorPicker_t2582382693 * L_2 = __this->get_picker_2();
		NullCheck(L_2);
		ColorChangedEvent_t347040188 * L_3 = L_2->get_onValueChanged_9();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)ColorSliderImage_ColorChanged_m3348797575_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_5 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_5, __this, L_4, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_3);
		UnityEvent_1_RemoveListener_m1657194609(L_3, L_5, /*hidden argument*/UnityEvent_1_RemoveListener_m1657194609_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_6 = __this->get_picker_2();
		NullCheck(L_6);
		HSVChangedEvent_t3546572410 * L_7 = L_6->get_onHSVChanged_10();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)ColorSliderImage_HSVChanged_m2525805608_RuntimeMethod_var);
		UnityAction_3_t3771153942 * L_9 = (UnityAction_3_t3771153942 *)il2cpp_codegen_object_new(UnityAction_3_t3771153942_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m672494230(L_9, __this, L_8, /*hidden argument*/UnityAction_3__ctor_m672494230_RuntimeMethod_var);
		NullCheck(L_7);
		UnityEvent_3_RemoveListener_m2900402221(L_7, L_9, /*hidden argument*/UnityEvent_3_RemoveListener_m2900402221_RuntimeMethod_var);
	}

IL_0049:
	{
		return;
	}
}
// System.Void ColorSliderImage::OnDestroy()
extern "C"  void ColorSliderImage_OnDestroy_m3945285673 (ColorSliderImage_t664667956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_OnDestroy_m3945285673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t3037409423 * L_0 = __this->get_image_5();
		NullCheck(L_0);
		Texture_t1132728222 * L_1 = RawImage_get_texture_m429915103(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		RawImage_t3037409423 * L_3 = __this->get_image_5();
		NullCheck(L_3);
		Texture_t1132728222 * L_4 = RawImage_get_texture_m429915103(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m1495482491(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void ColorSliderImage::ColorChanged(UnityEngine.Color)
extern "C"  void ColorSliderImage_ColorChanged_m3348797575 (ColorSliderImage_t664667956 * __this, Color_t320819310  ___newColor0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002e;
			}
			case 1:
			{
				goto IL_002e;
			}
			case 2:
			{
				goto IL_002e;
			}
			case 3:
			{
				goto IL_0039;
			}
			case 4:
			{
				goto IL_0039;
			}
			case 5:
			{
				goto IL_002e;
			}
			case 6:
			{
				goto IL_002e;
			}
		}
	}
	{
		goto IL_0039;
	}

IL_002e:
	{
		ColorSliderImage_RegenerateTexture_m629802786(__this, /*hidden argument*/NULL);
		goto IL_003e;
	}

IL_0039:
	{
		goto IL_003e;
	}

IL_003e:
	{
		return;
	}
}
// System.Void ColorSliderImage::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void ColorSliderImage_HSVChanged_m2525805608 (ColorSliderImage_t664667956 * __this, float ___hue0, float ___saturation1, float ___value2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002e;
			}
			case 1:
			{
				goto IL_002e;
			}
			case 2:
			{
				goto IL_002e;
			}
			case 3:
			{
				goto IL_0039;
			}
			case 4:
			{
				goto IL_0039;
			}
			case 5:
			{
				goto IL_002e;
			}
			case 6:
			{
				goto IL_002e;
			}
		}
	}
	{
		goto IL_0039;
	}

IL_002e:
	{
		ColorSliderImage_RegenerateTexture_m629802786(__this, /*hidden argument*/NULL);
		goto IL_003e;
	}

IL_0039:
	{
		goto IL_003e;
	}

IL_003e:
	{
		return;
	}
}
// System.Void ColorSliderImage::RegenerateTexture()
extern "C"  void ColorSliderImage_RegenerateTexture_m629802786 (ColorSliderImage_t664667956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorSliderImage_RegenerateTexture_m629802786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t2499566028  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Texture2D_t2870930912 * V_4 = NULL;
	Color32U5BU5D_t636217733* V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	uint8_t V_11 = 0x0;
	uint8_t V_12 = 0x0;
	uint8_t V_13 = 0x0;
	uint8_t V_14 = 0x0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	Color_t320819310  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	float G_B6_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B12_0 = 0.0f;
	int32_t G_B15_0 = 0;
	int32_t G_B18_0 = 0;
	Color32U5BU5D_t636217733* G_B32_0 = NULL;
	Color32U5BU5D_t636217733* G_B31_0 = NULL;
	int32_t G_B33_0 = 0;
	Color32U5BU5D_t636217733* G_B33_1 = NULL;
	Color32U5BU5D_t636217733* G_B39_0 = NULL;
	Color32U5BU5D_t636217733* G_B38_0 = NULL;
	int32_t G_B40_0 = 0;
	Color32U5BU5D_t636217733* G_B40_1 = NULL;
	Color32U5BU5D_t636217733* G_B46_0 = NULL;
	Color32U5BU5D_t636217733* G_B45_0 = NULL;
	int32_t G_B47_0 = 0;
	Color32U5BU5D_t636217733* G_B47_1 = NULL;
	Color32U5BU5D_t636217733* G_B53_0 = NULL;
	Color32U5BU5D_t636217733* G_B52_0 = NULL;
	int32_t G_B54_0 = 0;
	Color32U5BU5D_t636217733* G_B54_1 = NULL;
	Color32U5BU5D_t636217733* G_B60_0 = NULL;
	Color32U5BU5D_t636217733* G_B59_0 = NULL;
	int32_t G_B61_0 = 0;
	Color32U5BU5D_t636217733* G_B61_1 = NULL;
	Color32U5BU5D_t636217733* G_B67_0 = NULL;
	Color32U5BU5D_t636217733* G_B66_0 = NULL;
	int32_t G_B68_0 = 0;
	Color32U5BU5D_t636217733* G_B68_1 = NULL;
	Color32U5BU5D_t636217733* G_B74_0 = NULL;
	Color32U5BU5D_t636217733* G_B73_0 = NULL;
	int32_t G_B75_0 = 0;
	Color32U5BU5D_t636217733* G_B75_1 = NULL;
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ColorPicker_t2582382693 * L_2 = __this->get_picker_2();
		NullCheck(L_2);
		Color_t320819310  L_3 = ColorPicker_get_CurrentColor_m3098018243(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0026;
	}

IL_0021:
	{
		Color_t320819310  L_4 = Color_get_black_m4120693049(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0026:
	{
		Color32_t2499566028  L_5 = Color32_op_Implicit_m4104578668(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_5;
		ColorPicker_t2582382693 * L_6 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_6, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		ColorPicker_t2582382693 * L_8 = __this->get_picker_2();
		NullCheck(L_8);
		float L_9 = ColorPicker_get_H_m1137646974(L_8, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0052;
	}

IL_004d:
	{
		G_B6_0 = (0.0f);
	}

IL_0052:
	{
		V_1 = G_B6_0;
		ColorPicker_t2582382693 * L_10 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_10, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0074;
		}
	}
	{
		ColorPicker_t2582382693 * L_12 = __this->get_picker_2();
		NullCheck(L_12);
		float L_13 = ColorPicker_get_S_m220126169(L_12, /*hidden argument*/NULL);
		G_B9_0 = L_13;
		goto IL_0079;
	}

IL_0074:
	{
		G_B9_0 = (0.0f);
	}

IL_0079:
	{
		V_2 = G_B9_0;
		ColorPicker_t2582382693 * L_14 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_14, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_009b;
		}
	}
	{
		ColorPicker_t2582382693 * L_16 = __this->get_picker_2();
		NullCheck(L_16);
		float L_17 = ColorPicker_get_V_m286556906(L_16, /*hidden argument*/NULL);
		G_B12_0 = L_17;
		goto IL_00a0;
	}

IL_009b:
	{
		G_B12_0 = (0.0f);
	}

IL_00a0:
	{
		V_3 = G_B12_0;
		int32_t L_18 = __this->get_direction_4();
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			goto IL_00b8;
		}
	}
	{
		int32_t L_19 = __this->get_direction_4();
		G_B15_0 = ((((int32_t)L_19) == ((int32_t)3))? 1 : 0);
		goto IL_00b9;
	}

IL_00b8:
	{
		G_B15_0 = 1;
	}

IL_00b9:
	{
		V_6 = (bool)G_B15_0;
		int32_t L_20 = __this->get_direction_4();
		if ((((int32_t)L_20) == ((int32_t)3)))
		{
			goto IL_00d2;
		}
	}
	{
		int32_t L_21 = __this->get_direction_4();
		G_B18_0 = ((((int32_t)L_21) == ((int32_t)1))? 1 : 0);
		goto IL_00d3;
	}

IL_00d2:
	{
		G_B18_0 = 1;
	}

IL_00d3:
	{
		V_7 = (bool)G_B18_0;
		int32_t L_22 = __this->get_type_3();
		V_9 = L_22;
		int32_t L_23 = V_9;
		switch (L_23)
		{
			case 0:
			{
				goto IL_0105;
			}
			case 1:
			{
				goto IL_0105;
			}
			case 2:
			{
				goto IL_0105;
			}
			case 3:
			{
				goto IL_0105;
			}
			case 4:
			{
				goto IL_0111;
			}
			case 5:
			{
				goto IL_011d;
			}
			case 6:
			{
				goto IL_011d;
			}
		}
	}
	{
		goto IL_0126;
	}

IL_0105:
	{
		V_8 = ((int32_t)255);
		goto IL_0131;
	}

IL_0111:
	{
		V_8 = ((int32_t)360);
		goto IL_0131;
	}

IL_011d:
	{
		V_8 = ((int32_t)100);
		goto IL_0131;
	}

IL_0126:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NotImplementedException_t4033310348 * L_25 = (NotImplementedException_t4033310348 *)il2cpp_codegen_object_new(NotImplementedException_t4033310348_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1129172726(L_25, L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_0131:
	{
		bool L_26 = V_6;
		if (!L_26)
		{
			goto IL_0147;
		}
	}
	{
		int32_t L_27 = V_8;
		Texture2D_t2870930912 * L_28 = (Texture2D_t2870930912 *)il2cpp_codegen_object_new(Texture2D_t2870930912_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2324518131(L_28, 1, L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		goto IL_0151;
	}

IL_0147:
	{
		int32_t L_29 = V_8;
		Texture2D_t2870930912 * L_30 = (Texture2D_t2870930912 *)il2cpp_codegen_object_new(Texture2D_t2870930912_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2324518131(L_30, L_29, 1, /*hidden argument*/NULL);
		V_4 = L_30;
	}

IL_0151:
	{
		Texture2D_t2870930912 * L_31 = V_4;
		NullCheck(L_31);
		Object_set_hideFlags_m3192056770(L_31, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_32 = V_8;
		V_5 = ((Color32U5BU5D_t636217733*)SZArrayNew(Color32U5BU5D_t636217733_il2cpp_TypeInfo_var, (uint32_t)L_32));
		int32_t L_33 = __this->get_type_3();
		V_10 = L_33;
		int32_t L_34 = V_10;
		switch (L_34)
		{
			case 0:
			{
				goto IL_0193;
			}
			case 1:
			{
				goto IL_01eb;
			}
			case 2:
			{
				goto IL_0243;
			}
			case 3:
			{
				goto IL_029b;
			}
			case 4:
			{
				goto IL_02e9;
			}
			case 5:
			{
				goto IL_034a;
			}
			case 6:
			{
				goto IL_03a8;
			}
		}
	}
	{
		goto IL_0406;
	}

IL_0193:
	{
		V_11 = (uint8_t)0;
		goto IL_01dd;
	}

IL_019b:
	{
		Color32U5BU5D_t636217733* L_35 = V_5;
		bool L_36 = V_7;
		G_B31_0 = L_35;
		if (!L_36)
		{
			G_B32_0 = L_35;
			goto IL_01b0;
		}
	}
	{
		int32_t L_37 = V_8;
		uint8_t L_38 = V_11;
		G_B33_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_37-(int32_t)1))-(int32_t)L_38));
		G_B33_1 = G_B31_0;
		goto IL_01b2;
	}

IL_01b0:
	{
		uint8_t L_39 = V_11;
		G_B33_0 = ((int32_t)(L_39));
		G_B33_1 = G_B32_0;
	}

IL_01b2:
	{
		NullCheck(G_B33_1);
		uint8_t L_40 = V_11;
		uint8_t L_41 = (&V_0)->get_g_1();
		uint8_t L_42 = (&V_0)->get_b_2();
		Color32_t2499566028  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Color32__ctor_m3889745670((&L_43), L_40, L_41, L_42, (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		*(Color32_t2499566028 *)((G_B33_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B33_0))) = L_43;
		uint8_t L_44 = V_11;
		V_11 = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_44+(int32_t)1)))));
	}

IL_01dd:
	{
		uint8_t L_45 = V_11;
		int32_t L_46 = V_8;
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_019b;
		}
	}
	{
		goto IL_0411;
	}

IL_01eb:
	{
		V_12 = (uint8_t)0;
		goto IL_0235;
	}

IL_01f3:
	{
		Color32U5BU5D_t636217733* L_47 = V_5;
		bool L_48 = V_7;
		G_B38_0 = L_47;
		if (!L_48)
		{
			G_B39_0 = L_47;
			goto IL_0208;
		}
	}
	{
		int32_t L_49 = V_8;
		uint8_t L_50 = V_12;
		G_B40_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_49-(int32_t)1))-(int32_t)L_50));
		G_B40_1 = G_B38_0;
		goto IL_020a;
	}

IL_0208:
	{
		uint8_t L_51 = V_12;
		G_B40_0 = ((int32_t)(L_51));
		G_B40_1 = G_B39_0;
	}

IL_020a:
	{
		NullCheck(G_B40_1);
		uint8_t L_52 = (&V_0)->get_r_0();
		uint8_t L_53 = V_12;
		uint8_t L_54 = (&V_0)->get_b_2();
		Color32_t2499566028  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Color32__ctor_m3889745670((&L_55), L_52, L_53, L_54, (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		*(Color32_t2499566028 *)((G_B40_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B40_0))) = L_55;
		uint8_t L_56 = V_12;
		V_12 = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_56+(int32_t)1)))));
	}

IL_0235:
	{
		uint8_t L_57 = V_12;
		int32_t L_58 = V_8;
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_01f3;
		}
	}
	{
		goto IL_0411;
	}

IL_0243:
	{
		V_13 = (uint8_t)0;
		goto IL_028d;
	}

IL_024b:
	{
		Color32U5BU5D_t636217733* L_59 = V_5;
		bool L_60 = V_7;
		G_B45_0 = L_59;
		if (!L_60)
		{
			G_B46_0 = L_59;
			goto IL_0260;
		}
	}
	{
		int32_t L_61 = V_8;
		uint8_t L_62 = V_13;
		G_B47_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_61-(int32_t)1))-(int32_t)L_62));
		G_B47_1 = G_B45_0;
		goto IL_0262;
	}

IL_0260:
	{
		uint8_t L_63 = V_13;
		G_B47_0 = ((int32_t)(L_63));
		G_B47_1 = G_B46_0;
	}

IL_0262:
	{
		NullCheck(G_B47_1);
		uint8_t L_64 = (&V_0)->get_r_0();
		uint8_t L_65 = (&V_0)->get_g_1();
		uint8_t L_66 = V_13;
		Color32_t2499566028  L_67;
		memset(&L_67, 0, sizeof(L_67));
		Color32__ctor_m3889745670((&L_67), L_64, L_65, L_66, (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		*(Color32_t2499566028 *)((G_B47_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B47_0))) = L_67;
		uint8_t L_68 = V_13;
		V_13 = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_68+(int32_t)1)))));
	}

IL_028d:
	{
		uint8_t L_69 = V_13;
		int32_t L_70 = V_8;
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_024b;
		}
	}
	{
		goto IL_0411;
	}

IL_029b:
	{
		V_14 = (uint8_t)0;
		goto IL_02db;
	}

IL_02a3:
	{
		Color32U5BU5D_t636217733* L_71 = V_5;
		bool L_72 = V_7;
		G_B52_0 = L_71;
		if (!L_72)
		{
			G_B53_0 = L_71;
			goto IL_02b8;
		}
	}
	{
		int32_t L_73 = V_8;
		uint8_t L_74 = V_14;
		G_B54_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)1))-(int32_t)L_74));
		G_B54_1 = G_B52_0;
		goto IL_02ba;
	}

IL_02b8:
	{
		uint8_t L_75 = V_14;
		G_B54_0 = ((int32_t)(L_75));
		G_B54_1 = G_B53_0;
	}

IL_02ba:
	{
		NullCheck(G_B54_1);
		uint8_t L_76 = V_14;
		uint8_t L_77 = V_14;
		uint8_t L_78 = V_14;
		Color32_t2499566028  L_79;
		memset(&L_79, 0, sizeof(L_79));
		Color32__ctor_m3889745670((&L_79), L_76, L_77, L_78, (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		*(Color32_t2499566028 *)((G_B54_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B54_0))) = L_79;
		uint8_t L_80 = V_14;
		V_14 = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_80+(int32_t)1)))));
	}

IL_02db:
	{
		uint8_t L_81 = V_14;
		int32_t L_82 = V_8;
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_02a3;
		}
	}
	{
		goto IL_0411;
	}

IL_02e9:
	{
		V_15 = 0;
		goto IL_033c;
	}

IL_02f1:
	{
		Color32U5BU5D_t636217733* L_83 = V_5;
		bool L_84 = V_7;
		G_B59_0 = L_83;
		if (!L_84)
		{
			G_B60_0 = L_83;
			goto IL_0306;
		}
	}
	{
		int32_t L_85 = V_8;
		int32_t L_86 = V_15;
		G_B61_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_85-(int32_t)1))-(int32_t)L_86));
		G_B61_1 = G_B59_0;
		goto IL_0308;
	}

IL_0306:
	{
		int32_t L_87 = V_15;
		G_B61_0 = L_87;
		G_B61_1 = G_B60_0;
	}

IL_0308:
	{
		NullCheck(G_B61_1);
		int32_t L_88 = V_15;
		Color_t320819310  L_89 = HSVUtil_ConvertHsvToRgb_m3345724273(NULL /*static, unused*/, (((double)((double)L_88))), (1.0), (1.0), (1.0f), /*hidden argument*/NULL);
		Color32_t2499566028  L_90 = Color32_op_Implicit_m4104578668(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		*(Color32_t2499566028 *)((G_B61_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B61_0))) = L_90;
		int32_t L_91 = V_15;
		V_15 = ((int32_t)((int32_t)L_91+(int32_t)1));
	}

IL_033c:
	{
		int32_t L_92 = V_15;
		int32_t L_93 = V_8;
		if ((((int32_t)L_92) < ((int32_t)L_93)))
		{
			goto IL_02f1;
		}
	}
	{
		goto IL_0411;
	}

IL_034a:
	{
		V_16 = 0;
		goto IL_039a;
	}

IL_0352:
	{
		Color32U5BU5D_t636217733* L_94 = V_5;
		bool L_95 = V_7;
		G_B66_0 = L_94;
		if (!L_95)
		{
			G_B67_0 = L_94;
			goto IL_0367;
		}
	}
	{
		int32_t L_96 = V_8;
		int32_t L_97 = V_16;
		G_B68_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_96-(int32_t)1))-(int32_t)L_97));
		G_B68_1 = G_B66_0;
		goto IL_0369;
	}

IL_0367:
	{
		int32_t L_98 = V_16;
		G_B68_0 = L_98;
		G_B68_1 = G_B67_0;
	}

IL_0369:
	{
		NullCheck(G_B68_1);
		float L_99 = V_1;
		int32_t L_100 = V_16;
		int32_t L_101 = V_8;
		float L_102 = V_3;
		Color_t320819310  L_103 = HSVUtil_ConvertHsvToRgb_m3345724273(NULL /*static, unused*/, (((double)((double)((float)((float)L_99*(float)(360.0f)))))), (((double)((double)((float)((float)(((float)((float)L_100)))/(float)(((float)((float)L_101)))))))), (((double)((double)L_102))), (1.0f), /*hidden argument*/NULL);
		Color32_t2499566028  L_104 = Color32_op_Implicit_m4104578668(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		*(Color32_t2499566028 *)((G_B68_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B68_0))) = L_104;
		int32_t L_105 = V_16;
		V_16 = ((int32_t)((int32_t)L_105+(int32_t)1));
	}

IL_039a:
	{
		int32_t L_106 = V_16;
		int32_t L_107 = V_8;
		if ((((int32_t)L_106) < ((int32_t)L_107)))
		{
			goto IL_0352;
		}
	}
	{
		goto IL_0411;
	}

IL_03a8:
	{
		V_17 = 0;
		goto IL_03f8;
	}

IL_03b0:
	{
		Color32U5BU5D_t636217733* L_108 = V_5;
		bool L_109 = V_7;
		G_B73_0 = L_108;
		if (!L_109)
		{
			G_B74_0 = L_108;
			goto IL_03c5;
		}
	}
	{
		int32_t L_110 = V_8;
		int32_t L_111 = V_17;
		G_B75_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_110-(int32_t)1))-(int32_t)L_111));
		G_B75_1 = G_B73_0;
		goto IL_03c7;
	}

IL_03c5:
	{
		int32_t L_112 = V_17;
		G_B75_0 = L_112;
		G_B75_1 = G_B74_0;
	}

IL_03c7:
	{
		NullCheck(G_B75_1);
		float L_113 = V_1;
		float L_114 = V_2;
		int32_t L_115 = V_17;
		int32_t L_116 = V_8;
		Color_t320819310  L_117 = HSVUtil_ConvertHsvToRgb_m3345724273(NULL /*static, unused*/, (((double)((double)((float)((float)L_113*(float)(360.0f)))))), (((double)((double)L_114))), (((double)((double)((float)((float)(((float)((float)L_115)))/(float)(((float)((float)L_116)))))))), (1.0f), /*hidden argument*/NULL);
		Color32_t2499566028  L_118 = Color32_op_Implicit_m4104578668(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		*(Color32_t2499566028 *)((G_B75_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(G_B75_0))) = L_118;
		int32_t L_119 = V_17;
		V_17 = ((int32_t)((int32_t)L_119+(int32_t)1));
	}

IL_03f8:
	{
		int32_t L_120 = V_17;
		int32_t L_121 = V_8;
		if ((((int32_t)L_120) < ((int32_t)L_121)))
		{
			goto IL_03b0;
		}
	}
	{
		goto IL_0411;
	}

IL_0406:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_122 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NotImplementedException_t4033310348 * L_123 = (NotImplementedException_t4033310348 *)il2cpp_codegen_object_new(NotImplementedException_t4033310348_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1129172726(L_123, L_122, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_123);
	}

IL_0411:
	{
		Texture2D_t2870930912 * L_124 = V_4;
		Color32U5BU5D_t636217733* L_125 = V_5;
		NullCheck(L_124);
		Texture2D_SetPixels32_m3029225869(L_124, L_125, /*hidden argument*/NULL);
		Texture2D_t2870930912 * L_126 = V_4;
		NullCheck(L_126);
		Texture2D_Apply_m4102782407(L_126, /*hidden argument*/NULL);
		RawImage_t3037409423 * L_127 = __this->get_image_5();
		NullCheck(L_127);
		Texture_t1132728222 * L_128 = RawImage_get_texture_m429915103(L_127, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_129 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_128, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_129)
		{
			goto IL_0447;
		}
	}
	{
		RawImage_t3037409423 * L_130 = __this->get_image_5();
		NullCheck(L_130);
		Texture_t1132728222 * L_131 = RawImage_get_texture_m429915103(L_130, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m1495482491(NULL /*static, unused*/, L_131, /*hidden argument*/NULL);
	}

IL_0447:
	{
		RawImage_t3037409423 * L_132 = __this->get_image_5();
		Texture2D_t2870930912 * L_133 = V_4;
		NullCheck(L_132);
		RawImage_set_texture_m337957835(L_132, L_133, /*hidden argument*/NULL);
		int32_t L_134 = __this->get_direction_4();
		V_18 = L_134;
		int32_t L_135 = V_18;
		switch (L_135)
		{
			case 0:
			{
				goto IL_04a1;
			}
			case 1:
			{
				goto IL_04a1;
			}
			case 2:
			{
				goto IL_0478;
			}
			case 3:
			{
				goto IL_0478;
			}
		}
	}
	{
		goto IL_04ca;
	}

IL_0478:
	{
		RawImage_t3037409423 * L_136 = __this->get_image_5();
		Rect_t1992046353  L_137;
		memset(&L_137, 0, sizeof(L_137));
		Rect__ctor_m1383055445((&L_137), (0.0f), (0.0f), (2.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_136);
		RawImage_set_uvRect_m2592252070(L_136, L_137, /*hidden argument*/NULL);
		goto IL_04cf;
	}

IL_04a1:
	{
		RawImage_t3037409423 * L_138 = __this->get_image_5();
		Rect_t1992046353  L_139;
		memset(&L_139, 0, sizeof(L_139));
		Rect__ctor_m1383055445((&L_139), (0.0f), (0.0f), (1.0f), (2.0f), /*hidden argument*/NULL);
		NullCheck(L_138);
		RawImage_set_uvRect_m2592252070(L_138, L_139, /*hidden argument*/NULL);
		goto IL_04cf;
	}

IL_04ca:
	{
		goto IL_04cf;
	}

IL_04cf:
	{
		return;
	}
}
// System.Void HexColorField::.ctor()
extern "C"  void HexColorField__ctor_m1922969058 (HexColorField_t1392034802 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HexColorField::Awake()
extern "C"  void HexColorField_Awake_m1129651162 (HexColorField_t1392034802 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_Awake_m1129651162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t3161868630 * L_0 = Component_GetComponent_TisInputField_t3161868630_m1543344974(__this, /*hidden argument*/Component_GetComponent_TisInputField_t3161868630_m1543344974_RuntimeMethod_var);
		__this->set_hexInputField_4(L_0);
		InputField_t3161868630 * L_1 = __this->get_hexInputField_4();
		NullCheck(L_1);
		SubmitEvent_t1556243238 * L_2 = InputField_get_onEndEdit_m261907968(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)HexColorField_UpdateColor_m3237283494_RuntimeMethod_var);
		UnityAction_1_t1336681460 * L_4 = (UnityAction_1_t1336681460 *)il2cpp_codegen_object_new(UnityAction_1_t1336681460_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2651916199(L_4, __this, L_3, /*hidden argument*/UnityAction_1__ctor_m2651916199_RuntimeMethod_var);
		NullCheck(L_2);
		UnityEvent_1_AddListener_m3737383032(L_2, L_4, /*hidden argument*/UnityEvent_1_AddListener_m3737383032_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_5 = __this->get_hsvpicker_2();
		NullCheck(L_5);
		ColorChangedEvent_t347040188 * L_6 = L_5->get_onValueChanged_9();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)HexColorField_UpdateHex_m1669923459_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_8 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_8, __this, L_7, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_6);
		UnityEvent_1_AddListener_m2001044386(L_6, L_8, /*hidden argument*/UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var);
		return;
	}
}
// System.Void HexColorField::OnDestroy()
extern "C"  void HexColorField_OnDestroy_m2327818717 (HexColorField_t1392034802 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_OnDestroy_m2327818717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t3161868630 * L_0 = __this->get_hexInputField_4();
		NullCheck(L_0);
		OnChangeEvent_t2953828243 * L_1 = InputField_get_onValueChanged_m1315780744(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)HexColorField_UpdateColor_m3237283494_RuntimeMethod_var);
		UnityAction_1_t1336681460 * L_3 = (UnityAction_1_t1336681460 *)il2cpp_codegen_object_new(UnityAction_1_t1336681460_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m2651916199(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m2651916199_RuntimeMethod_var);
		NullCheck(L_1);
		UnityEvent_1_RemoveListener_m3968893251(L_1, L_3, /*hidden argument*/UnityEvent_1_RemoveListener_m3968893251_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_4 = __this->get_hsvpicker_2();
		NullCheck(L_4);
		ColorChangedEvent_t347040188 * L_5 = L_4->get_onValueChanged_9();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)HexColorField_UpdateHex_m1669923459_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_7 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_7, __this, L_6, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_5);
		UnityEvent_1_RemoveListener_m1657194609(L_5, L_7, /*hidden argument*/UnityEvent_1_RemoveListener_m1657194609_RuntimeMethod_var);
		return;
	}
}
// System.Void HexColorField::UpdateHex(UnityEngine.Color)
extern "C"  void HexColorField_UpdateHex_m1669923459 (HexColorField_t1392034802 * __this, Color_t320819310  ___newColor0, const RuntimeMethod* method)
{
	{
		InputField_t3161868630 * L_0 = __this->get_hexInputField_4();
		Color_t320819310  L_1 = ___newColor0;
		Color32_t2499566028  L_2 = Color32_op_Implicit_m4104578668(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_3 = HexColorField_ColorToHex_m4216070677(__this, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		InputField_set_text_m97417596(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HexColorField::UpdateColor(System.String)
extern "C"  void HexColorField_UpdateColor_m3237283494 (HexColorField_t1392034802 * __this, String_t* ___newHex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_UpdateColor_m3237283494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t2499566028  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___newHex0;
		bool L_1 = HexColorField_HexToColor_m2425132568(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ColorPicker_t2582382693 * L_2 = __this->get_hsvpicker_2();
		Color32_t2499566028  L_3 = V_0;
		Color_t320819310  L_4 = Color32_op_Implicit_m3656906645(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ColorPicker_set_CurrentColor_m2827670769(L_2, L_4, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral3963194399, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.String HexColorField::ColorToHex(UnityEngine.Color32)
extern "C"  String_t* HexColorField_ColorToHex_m4216070677 (HexColorField_t1392034802 * __this, Color32_t2499566028  ___color0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_ColorToHex_m4216070677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_displayAlpha_3();
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t3384890222* L_1 = ((ObjectU5BU5D_t3384890222*)SZArrayNew(ObjectU5BU5D_t3384890222_il2cpp_TypeInfo_var, (uint32_t)4));
		uint8_t L_2 = (&___color0)->get_r_0();
		uint8_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Byte_t3065488403_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3384890222* L_5 = L_1;
		uint8_t L_6 = (&___color0)->get_g_1();
		uint8_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Byte_t3065488403_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3384890222* L_9 = L_5;
		uint8_t L_10 = (&___color0)->get_b_2();
		uint8_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Byte_t3065488403_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3384890222* L_13 = L_9;
		uint8_t L_14 = (&___color0)->get_a_3();
		uint8_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Byte_t3065488403_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m3337466354(NULL /*static, unused*/, _stringLiteral1202944861, L_13, /*hidden argument*/NULL);
		return L_17;
	}

IL_0058:
	{
		uint8_t L_18 = (&___color0)->get_r_0();
		uint8_t L_19 = L_18;
		RuntimeObject * L_20 = Box(Byte_t3065488403_il2cpp_TypeInfo_var, &L_19);
		uint8_t L_21 = (&___color0)->get_g_1();
		uint8_t L_22 = L_21;
		RuntimeObject * L_23 = Box(Byte_t3065488403_il2cpp_TypeInfo_var, &L_22);
		uint8_t L_24 = (&___color0)->get_b_2();
		uint8_t L_25 = L_24;
		RuntimeObject * L_26 = Box(Byte_t3065488403_il2cpp_TypeInfo_var, &L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Format_m223808606(NULL /*static, unused*/, _stringLiteral1944985349, L_20, L_23, L_26, /*hidden argument*/NULL);
		return L_27;
	}
}
// System.Boolean HexColorField::HexToColor(System.String,UnityEngine.Color32&)
extern "C"  bool HexColorField_HexToColor_m2425132568 (RuntimeObject * __this /* static, unused */, String_t* ___hex0, Color32_t2499566028 * ___color1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexColorField_HexToColor_m2425132568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___hex0;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t1187425115_il2cpp_TypeInfo_var);
		bool L_1 = Regex_IsMatch_m11553654(NULL /*static, unused*/, L_0, _stringLiteral377735934, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0255;
		}
	}
	{
		String_t* L_2 = ___hex0;
		NullCheck(L_2);
		bool L_3 = String_StartsWith_m2373373075(L_2, _stringLiteral82137410, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		V_0 = G_B4_0;
		String_t* L_4 = ___hex0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1547709748(L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)((int32_t)L_6+(int32_t)8))))))
		{
			goto IL_008f;
		}
	}
	{
		Color32_t2499566028 * L_7 = ___color1;
		String_t* L_8 = ___hex0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		String_t* L_10 = String_Substring_m1525098696(L_8, L_9, 2, /*hidden argument*/NULL);
		uint8_t L_11 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_10, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_12 = ___hex0;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		String_t* L_14 = String_Substring_m1525098696(L_12, ((int32_t)((int32_t)L_13+(int32_t)2)), 2, /*hidden argument*/NULL);
		uint8_t L_15 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_14, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_16 = ___hex0;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		String_t* L_18 = String_Substring_m1525098696(L_16, ((int32_t)((int32_t)L_17+(int32_t)4)), 2, /*hidden argument*/NULL);
		uint8_t L_19 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_18, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_20 = ___hex0;
		int32_t L_21 = V_0;
		NullCheck(L_20);
		String_t* L_22 = String_Substring_m1525098696(L_20, ((int32_t)((int32_t)L_21+(int32_t)6)), 2, /*hidden argument*/NULL);
		uint8_t L_23 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_22, ((int32_t)512), /*hidden argument*/NULL);
		Color32__ctor_m3889745670(L_7, L_11, L_15, L_19, L_23, /*hidden argument*/NULL);
		goto IL_0253;
	}

IL_008f:
	{
		String_t* L_24 = ___hex0;
		NullCheck(L_24);
		int32_t L_25 = String_get_Length_m1547709748(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_0;
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)((int32_t)L_26+(int32_t)6))))))
		{
			goto IL_00e7;
		}
	}
	{
		Color32_t2499566028 * L_27 = ___color1;
		String_t* L_28 = ___hex0;
		int32_t L_29 = V_0;
		NullCheck(L_28);
		String_t* L_30 = String_Substring_m1525098696(L_28, L_29, 2, /*hidden argument*/NULL);
		uint8_t L_31 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_30, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_32 = ___hex0;
		int32_t L_33 = V_0;
		NullCheck(L_32);
		String_t* L_34 = String_Substring_m1525098696(L_32, ((int32_t)((int32_t)L_33+(int32_t)2)), 2, /*hidden argument*/NULL);
		uint8_t L_35 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_34, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_36 = ___hex0;
		int32_t L_37 = V_0;
		NullCheck(L_36);
		String_t* L_38 = String_Substring_m1525098696(L_36, ((int32_t)((int32_t)L_37+(int32_t)4)), 2, /*hidden argument*/NULL);
		uint8_t L_39 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_38, ((int32_t)512), /*hidden argument*/NULL);
		Color32__ctor_m3889745670(L_27, L_31, L_35, L_39, (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		goto IL_0253;
	}

IL_00e7:
	{
		String_t* L_40 = ___hex0;
		NullCheck(L_40);
		int32_t L_41 = String_get_Length_m1547709748(L_40, /*hidden argument*/NULL);
		int32_t L_42 = V_0;
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)((int32_t)L_42+(int32_t)4))))))
		{
			goto IL_01bc;
		}
	}
	{
		Color32_t2499566028 * L_43 = ___color1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		String_t* L_45 = ___hex0;
		int32_t L_46 = V_0;
		NullCheck(L_45);
		Il2CppChar L_47 = String_get_Chars_m1478298497(L_45, L_46, /*hidden argument*/NULL);
		Il2CppChar L_48 = L_47;
		RuntimeObject * L_49 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_48);
		String_t* L_50 = ___hex0;
		int32_t L_51 = V_0;
		NullCheck(L_50);
		Il2CppChar L_52 = String_get_Chars_m1478298497(L_50, L_51, /*hidden argument*/NULL);
		Il2CppChar L_53 = L_52;
		RuntimeObject * L_54 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_53);
		String_t* L_55 = String_Concat_m4154442594(NULL /*static, unused*/, L_44, L_49, L_54, /*hidden argument*/NULL);
		uint8_t L_56 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_55, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_57 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		String_t* L_58 = ___hex0;
		int32_t L_59 = V_0;
		NullCheck(L_58);
		Il2CppChar L_60 = String_get_Chars_m1478298497(L_58, ((int32_t)((int32_t)L_59+(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_61 = L_60;
		RuntimeObject * L_62 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_61);
		String_t* L_63 = ___hex0;
		int32_t L_64 = V_0;
		NullCheck(L_63);
		Il2CppChar L_65 = String_get_Chars_m1478298497(L_63, ((int32_t)((int32_t)L_64+(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_66 = L_65;
		RuntimeObject * L_67 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_66);
		String_t* L_68 = String_Concat_m4154442594(NULL /*static, unused*/, L_57, L_62, L_67, /*hidden argument*/NULL);
		uint8_t L_69 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_68, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_70 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		String_t* L_71 = ___hex0;
		int32_t L_72 = V_0;
		NullCheck(L_71);
		Il2CppChar L_73 = String_get_Chars_m1478298497(L_71, ((int32_t)((int32_t)L_72+(int32_t)2)), /*hidden argument*/NULL);
		Il2CppChar L_74 = L_73;
		RuntimeObject * L_75 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_74);
		String_t* L_76 = ___hex0;
		int32_t L_77 = V_0;
		NullCheck(L_76);
		Il2CppChar L_78 = String_get_Chars_m1478298497(L_76, ((int32_t)((int32_t)L_77+(int32_t)2)), /*hidden argument*/NULL);
		Il2CppChar L_79 = L_78;
		RuntimeObject * L_80 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_79);
		String_t* L_81 = String_Concat_m4154442594(NULL /*static, unused*/, L_70, L_75, L_80, /*hidden argument*/NULL);
		uint8_t L_82 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_81, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_83 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		String_t* L_84 = ___hex0;
		int32_t L_85 = V_0;
		NullCheck(L_84);
		Il2CppChar L_86 = String_get_Chars_m1478298497(L_84, ((int32_t)((int32_t)L_85+(int32_t)3)), /*hidden argument*/NULL);
		Il2CppChar L_87 = L_86;
		RuntimeObject * L_88 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_87);
		String_t* L_89 = ___hex0;
		int32_t L_90 = V_0;
		NullCheck(L_89);
		Il2CppChar L_91 = String_get_Chars_m1478298497(L_89, ((int32_t)((int32_t)L_90+(int32_t)3)), /*hidden argument*/NULL);
		Il2CppChar L_92 = L_91;
		RuntimeObject * L_93 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_92);
		String_t* L_94 = String_Concat_m4154442594(NULL /*static, unused*/, L_83, L_88, L_93, /*hidden argument*/NULL);
		uint8_t L_95 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_94, ((int32_t)512), /*hidden argument*/NULL);
		Color32__ctor_m3889745670(L_43, L_56, L_69, L_82, L_95, /*hidden argument*/NULL);
		goto IL_0253;
	}

IL_01bc:
	{
		Color32_t2499566028 * L_96 = ___color1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		String_t* L_98 = ___hex0;
		int32_t L_99 = V_0;
		NullCheck(L_98);
		Il2CppChar L_100 = String_get_Chars_m1478298497(L_98, L_99, /*hidden argument*/NULL);
		Il2CppChar L_101 = L_100;
		RuntimeObject * L_102 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_101);
		String_t* L_103 = ___hex0;
		int32_t L_104 = V_0;
		NullCheck(L_103);
		Il2CppChar L_105 = String_get_Chars_m1478298497(L_103, L_104, /*hidden argument*/NULL);
		Il2CppChar L_106 = L_105;
		RuntimeObject * L_107 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_106);
		String_t* L_108 = String_Concat_m4154442594(NULL /*static, unused*/, L_97, L_102, L_107, /*hidden argument*/NULL);
		uint8_t L_109 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_108, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_110 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		String_t* L_111 = ___hex0;
		int32_t L_112 = V_0;
		NullCheck(L_111);
		Il2CppChar L_113 = String_get_Chars_m1478298497(L_111, ((int32_t)((int32_t)L_112+(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_114 = L_113;
		RuntimeObject * L_115 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_114);
		String_t* L_116 = ___hex0;
		int32_t L_117 = V_0;
		NullCheck(L_116);
		Il2CppChar L_118 = String_get_Chars_m1478298497(L_116, ((int32_t)((int32_t)L_117+(int32_t)1)), /*hidden argument*/NULL);
		Il2CppChar L_119 = L_118;
		RuntimeObject * L_120 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_119);
		String_t* L_121 = String_Concat_m4154442594(NULL /*static, unused*/, L_110, L_115, L_120, /*hidden argument*/NULL);
		uint8_t L_122 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_121, ((int32_t)512), /*hidden argument*/NULL);
		String_t* L_123 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		String_t* L_124 = ___hex0;
		int32_t L_125 = V_0;
		NullCheck(L_124);
		Il2CppChar L_126 = String_get_Chars_m1478298497(L_124, ((int32_t)((int32_t)L_125+(int32_t)2)), /*hidden argument*/NULL);
		Il2CppChar L_127 = L_126;
		RuntimeObject * L_128 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_127);
		String_t* L_129 = ___hex0;
		int32_t L_130 = V_0;
		NullCheck(L_129);
		Il2CppChar L_131 = String_get_Chars_m1478298497(L_129, ((int32_t)((int32_t)L_130+(int32_t)2)), /*hidden argument*/NULL);
		Il2CppChar L_132 = L_131;
		RuntimeObject * L_133 = Box(Char_t539237919_il2cpp_TypeInfo_var, &L_132);
		String_t* L_134 = String_Concat_m4154442594(NULL /*static, unused*/, L_123, L_128, L_133, /*hidden argument*/NULL);
		uint8_t L_135 = Byte_Parse_m1387626950(NULL /*static, unused*/, L_134, ((int32_t)512), /*hidden argument*/NULL);
		Color32__ctor_m3889745670(L_96, L_109, L_122, L_135, (uint8_t)((int32_t)255), /*hidden argument*/NULL);
	}

IL_0253:
	{
		return (bool)1;
	}

IL_0255:
	{
		Color32_t2499566028 * L_136 = ___color1;
		Initobj (Color32_t2499566028_il2cpp_TypeInfo_var, L_136);
		return (bool)0;
	}
}
// System.Void HSVChangedEvent::.ctor()
extern "C"  void HSVChangedEvent__ctor_m2671244103 (HSVChangedEvent_t3546572410 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HSVChangedEvent__ctor_m2671244103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_3__ctor_m3450248554(__this, /*hidden argument*/UnityEvent_3__ctor_m3450248554_RuntimeMethod_var);
		return;
	}
}
// System.Void HsvColor::.ctor(System.Double,System.Double,System.Double)
extern "C"  void HsvColor__ctor_m3228940163 (HsvColor_t3806242730 * __this, double ___h0, double ___s1, double ___v2, const RuntimeMethod* method)
{
	{
		double L_0 = ___h0;
		__this->set_H_0(L_0);
		double L_1 = ___s1;
		__this->set_S_1(L_1);
		double L_2 = ___v2;
		__this->set_V_2(L_2);
		return;
	}
}
extern "C"  void HsvColor__ctor_m3228940163_AdjustorThunk (RuntimeObject * __this, double ___h0, double ___s1, double ___v2, const RuntimeMethod* method)
{
	HsvColor_t3806242730 * _thisAdjusted = reinterpret_cast<HsvColor_t3806242730 *>(__this + 1);
	HsvColor__ctor_m3228940163(_thisAdjusted, ___h0, ___s1, ___v2, method);
}
// System.Single HsvColor::get_normalizedH()
extern "C"  float HsvColor_get_normalizedH_m2453528821 (HsvColor_t3806242730 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_H_0();
		return ((float)((float)(((float)((float)L_0)))/(float)(360.0f)));
	}
}
extern "C"  float HsvColor_get_normalizedH_m2453528821_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	HsvColor_t3806242730 * _thisAdjusted = reinterpret_cast<HsvColor_t3806242730 *>(__this + 1);
	return HsvColor_get_normalizedH_m2453528821(_thisAdjusted, method);
}
// System.Void HsvColor::set_normalizedH(System.Single)
extern "C"  void HsvColor_set_normalizedH_m3701493110 (HsvColor_t3806242730 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_H_0(((double)((double)(((double)((double)L_0)))*(double)(360.0))));
		return;
	}
}
extern "C"  void HsvColor_set_normalizedH_m3701493110_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	HsvColor_t3806242730 * _thisAdjusted = reinterpret_cast<HsvColor_t3806242730 *>(__this + 1);
	HsvColor_set_normalizedH_m3701493110(_thisAdjusted, ___value0, method);
}
// System.Single HsvColor::get_normalizedS()
extern "C"  float HsvColor_get_normalizedS_m3606923594 (HsvColor_t3806242730 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_S_1();
		return (((float)((float)L_0)));
	}
}
extern "C"  float HsvColor_get_normalizedS_m3606923594_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	HsvColor_t3806242730 * _thisAdjusted = reinterpret_cast<HsvColor_t3806242730 *>(__this + 1);
	return HsvColor_get_normalizedS_m3606923594(_thisAdjusted, method);
}
// System.Void HsvColor::set_normalizedS(System.Single)
extern "C"  void HsvColor_set_normalizedS_m1285062888 (HsvColor_t3806242730 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_S_1((((double)((double)L_0))));
		return;
	}
}
extern "C"  void HsvColor_set_normalizedS_m1285062888_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	HsvColor_t3806242730 * _thisAdjusted = reinterpret_cast<HsvColor_t3806242730 *>(__this + 1);
	HsvColor_set_normalizedS_m1285062888(_thisAdjusted, ___value0, method);
}
// System.Single HsvColor::get_normalizedV()
extern "C"  float HsvColor_get_normalizedV_m2333326027 (HsvColor_t3806242730 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_V_2();
		return (((float)((float)L_0)));
	}
}
extern "C"  float HsvColor_get_normalizedV_m2333326027_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	HsvColor_t3806242730 * _thisAdjusted = reinterpret_cast<HsvColor_t3806242730 *>(__this + 1);
	return HsvColor_get_normalizedV_m2333326027(_thisAdjusted, method);
}
// System.Void HsvColor::set_normalizedV(System.Single)
extern "C"  void HsvColor_set_normalizedV_m3696861371 (HsvColor_t3806242730 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_V_2((((double)((double)L_0))));
		return;
	}
}
extern "C"  void HsvColor_set_normalizedV_m3696861371_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	HsvColor_t3806242730 * _thisAdjusted = reinterpret_cast<HsvColor_t3806242730 *>(__this + 1);
	HsvColor_set_normalizedV_m3696861371(_thisAdjusted, ___value0, method);
}
// System.String HsvColor::ToString()
extern "C"  String_t* HsvColor_ToString_m1904641755 (HsvColor_t3806242730 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HsvColor_ToString_m1904641755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1448570014* L_0 = ((StringU5BU5D_t1448570014*)SZArrayNew(StringU5BU5D_t1448570014_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2396611028);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2396611028);
		StringU5BU5D_t1448570014* L_1 = L_0;
		double* L_2 = __this->get_address_of_H_0();
		String_t* L_3 = Double_ToString_m519012613(L_2, _stringLiteral3789558989, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1448570014* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral2650072937);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2650072937);
		StringU5BU5D_t1448570014* L_5 = L_4;
		double* L_6 = __this->get_address_of_S_1();
		String_t* L_7 = Double_ToString_m519012613(L_6, _stringLiteral3789558989, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1448570014* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral2650072937);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2650072937);
		StringU5BU5D_t1448570014* L_9 = L_8;
		double* L_10 = __this->get_address_of_V_2();
		String_t* L_11 = Double_ToString_m519012613(L_10, _stringLiteral3789558989, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t1448570014* L_12 = L_9;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral597313382);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral597313382);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m974927111(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* HsvColor_ToString_m1904641755_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	HsvColor_t3806242730 * _thisAdjusted = reinterpret_cast<HsvColor_t3806242730 *>(__this + 1);
	return HsvColor_ToString_m1904641755(_thisAdjusted, method);
}
// HsvColor HSVUtil::ConvertRgbToHsv(UnityEngine.Color)
extern "C"  HsvColor_t3806242730  HSVUtil_ConvertRgbToHsv_m1523836538 (RuntimeObject * __this /* static, unused */, Color_t320819310  ___color0, const RuntimeMethod* method)
{
	{
		float L_0 = (&___color0)->get_r_0();
		float L_1 = (&___color0)->get_g_1();
		float L_2 = (&___color0)->get_b_2();
		HsvColor_t3806242730  L_3 = HSVUtil_ConvertRgbToHsv_m4081435738(NULL /*static, unused*/, (((double)((double)(((int32_t)((int32_t)((float)((float)L_0*(float)(255.0f))))))))), (((double)((double)(((int32_t)((int32_t)((float)((float)L_1*(float)(255.0f))))))))), (((double)((double)(((int32_t)((int32_t)((float)((float)L_2*(float)(255.0f))))))))), /*hidden argument*/NULL);
		return L_3;
	}
}
// HsvColor HSVUtil::ConvertRgbToHsv(System.Double,System.Double,System.Double)
extern "C"  HsvColor_t3806242730  HSVUtil_ConvertRgbToHsv_m4081435738 (RuntimeObject * __this /* static, unused */, double ___r0, double ___b1, double ___g2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HSVUtil_ConvertRgbToHsv_m4081435738_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	double V_4 = 0.0;
	HsvColor_t3806242730  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		V_2 = (0.0);
		double L_0 = ___r0;
		double L_1 = ___g2;
		double L_2 = Math_Min_m3975942609(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		double L_3 = ___b1;
		double L_4 = Math_Min_m3975942609(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		double L_5 = ___r0;
		double L_6 = ___g2;
		double L_7 = Math_Max_m3206487624(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		double L_8 = ___b1;
		double L_9 = Math_Max_m3206487624(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		double L_10 = V_4;
		double L_11 = V_1;
		V_0 = ((double)((double)L_10-(double)L_11));
		double L_12 = V_4;
		if ((!(((double)L_12) == ((double)(0.0)))))
		{
			goto IL_004b;
		}
	}
	{
		V_3 = (0.0);
		goto IL_0050;
	}

IL_004b:
	{
		double L_13 = V_0;
		double L_14 = V_4;
		V_3 = ((double)((double)L_13/(double)L_14));
	}

IL_0050:
	{
		double L_15 = V_3;
		if ((!(((double)L_15) == ((double)(0.0)))))
		{
			goto IL_006e;
		}
	}
	{
		V_2 = (360.0);
		goto IL_00dd;
	}

IL_006e:
	{
		double L_16 = ___r0;
		double L_17 = V_4;
		if ((!(((double)L_16) == ((double)L_17))))
		{
			goto IL_0081;
		}
	}
	{
		double L_18 = ___g2;
		double L_19 = ___b1;
		double L_20 = V_0;
		V_2 = ((double)((double)((double)((double)L_18-(double)L_19))/(double)L_20));
		goto IL_00b6;
	}

IL_0081:
	{
		double L_21 = ___g2;
		double L_22 = V_4;
		if ((!(((double)L_21) == ((double)L_22))))
		{
			goto IL_009e;
		}
	}
	{
		double L_23 = ___b1;
		double L_24 = ___r0;
		double L_25 = V_0;
		V_2 = ((double)((double)(2.0)+(double)((double)((double)((double)((double)L_23-(double)L_24))/(double)L_25))));
		goto IL_00b6;
	}

IL_009e:
	{
		double L_26 = ___b1;
		double L_27 = V_4;
		if ((!(((double)L_26) == ((double)L_27))))
		{
			goto IL_00b6;
		}
	}
	{
		double L_28 = ___r0;
		double L_29 = ___g2;
		double L_30 = V_0;
		V_2 = ((double)((double)(4.0)+(double)((double)((double)((double)((double)L_28-(double)L_29))/(double)L_30))));
	}

IL_00b6:
	{
		double L_31 = V_2;
		V_2 = ((double)((double)L_31*(double)(60.0)));
		double L_32 = V_2;
		if ((!(((double)L_32) <= ((double)(0.0)))))
		{
			goto IL_00dd;
		}
	}
	{
		double L_33 = V_2;
		V_2 = ((double)((double)L_33+(double)(360.0)));
	}

IL_00dd:
	{
		Initobj (HsvColor_t3806242730_il2cpp_TypeInfo_var, (&V_5));
		double L_34 = V_2;
		(&V_5)->set_H_0(((double)((double)(360.0)-(double)L_34)));
		double L_35 = V_3;
		(&V_5)->set_S_1(L_35);
		double L_36 = V_4;
		(&V_5)->set_V_2(((double)((double)L_36/(double)(255.0))));
		HsvColor_t3806242730  L_37 = V_5;
		return L_37;
	}
}
// UnityEngine.Color HSVUtil::ConvertHsvToRgb(System.Double,System.Double,System.Double,System.Single)
extern "C"  Color_t320819310  HSVUtil_ConvertHsvToRgb_m3345724273 (RuntimeObject * __this /* static, unused */, double ___h0, double ___s1, double ___v2, float ___alpha3, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	int32_t V_3 = 0;
	double V_4 = 0.0;
	double V_5 = 0.0;
	double V_6 = 0.0;
	double V_7 = 0.0;
	{
		V_0 = (0.0);
		V_1 = (0.0);
		V_2 = (0.0);
		double L_0 = ___s1;
		if ((!(((double)L_0) == ((double)(0.0)))))
		{
			goto IL_0038;
		}
	}
	{
		double L_1 = ___v2;
		V_0 = L_1;
		double L_2 = ___v2;
		V_1 = L_2;
		double L_3 = ___v2;
		V_2 = L_3;
		goto IL_0117;
	}

IL_0038:
	{
		double L_4 = ___h0;
		if ((!(((double)L_4) == ((double)(360.0)))))
		{
			goto IL_0057;
		}
	}
	{
		___h0 = (0.0);
		goto IL_0064;
	}

IL_0057:
	{
		double L_5 = ___h0;
		___h0 = ((double)((double)L_5/(double)(60.0)));
	}

IL_0064:
	{
		double L_6 = ___h0;
		V_3 = (((int32_t)((int32_t)L_6)));
		double L_7 = ___h0;
		int32_t L_8 = V_3;
		V_4 = ((double)((double)L_7-(double)(((double)((double)L_8)))));
		double L_9 = ___v2;
		double L_10 = ___s1;
		V_5 = ((double)((double)L_9*(double)((double)((double)(1.0)-(double)L_10))));
		double L_11 = ___v2;
		double L_12 = ___s1;
		double L_13 = V_4;
		V_6 = ((double)((double)L_11*(double)((double)((double)(1.0)-(double)((double)((double)L_12*(double)L_13))))));
		double L_14 = ___v2;
		double L_15 = ___s1;
		double L_16 = V_4;
		V_7 = ((double)((double)L_14*(double)((double)((double)(1.0)-(double)((double)((double)L_15*(double)((double)((double)(1.0)-(double)L_16))))))));
		int32_t L_17 = V_3;
		switch (L_17)
		{
			case 0:
			{
				goto IL_00c9;
			}
			case 1:
			{
				goto IL_00d6;
			}
			case 2:
			{
				goto IL_00e3;
			}
			case 3:
			{
				goto IL_00f0;
			}
			case 4:
			{
				goto IL_00fd;
			}
		}
	}
	{
		goto IL_010a;
	}

IL_00c9:
	{
		double L_18 = ___v2;
		V_0 = L_18;
		double L_19 = V_7;
		V_1 = L_19;
		double L_20 = V_5;
		V_2 = L_20;
		goto IL_0117;
	}

IL_00d6:
	{
		double L_21 = V_6;
		V_0 = L_21;
		double L_22 = ___v2;
		V_1 = L_22;
		double L_23 = V_5;
		V_2 = L_23;
		goto IL_0117;
	}

IL_00e3:
	{
		double L_24 = V_5;
		V_0 = L_24;
		double L_25 = ___v2;
		V_1 = L_25;
		double L_26 = V_7;
		V_2 = L_26;
		goto IL_0117;
	}

IL_00f0:
	{
		double L_27 = V_5;
		V_0 = L_27;
		double L_28 = V_6;
		V_1 = L_28;
		double L_29 = ___v2;
		V_2 = L_29;
		goto IL_0117;
	}

IL_00fd:
	{
		double L_30 = V_7;
		V_0 = L_30;
		double L_31 = V_5;
		V_1 = L_31;
		double L_32 = ___v2;
		V_2 = L_32;
		goto IL_0117;
	}

IL_010a:
	{
		double L_33 = ___v2;
		V_0 = L_33;
		double L_34 = V_5;
		V_1 = L_34;
		double L_35 = V_6;
		V_2 = L_35;
		goto IL_0117;
	}

IL_0117:
	{
		double L_36 = V_0;
		double L_37 = V_1;
		double L_38 = V_2;
		float L_39 = ___alpha3;
		Color_t320819310  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Color__ctor_m1438150939((&L_40), (((float)((float)L_36))), (((float)((float)L_37))), (((float)((float)L_38))), L_39, /*hidden argument*/NULL);
		return L_40;
	}
}
// System.Void KnightControl::.ctor()
extern "C"  void KnightControl__ctor_m242124982 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KnightControl::Start()
extern "C"  void KnightControl_Start_m4145995205 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_Start_m4145995205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = Component_GetComponent_TisAnimation_t1557311624_m834402472(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t1557311624_m834402472_RuntimeMethod_var);
		__this->set_animation_2(L_0);
		return;
	}
}
// System.Void KnightControl::Update()
extern "C"  void KnightControl_Update_m1395860105 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_Update_m1395860105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t596762001  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t596762001  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_moveForward_4();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		Transform_t3933397867 * L_1 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t596762001_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_2 = Vector3_get_forward_m4168834013(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m1087255831(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t596762001  L_4 = Vector3_op_Multiply_m3577861744(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Transform_t3933397867 * L_5 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t596762001  L_6 = Transform_get_localScale_m1140650081(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (&V_0)->get_x_1();
		Vector3_t596762001  L_8 = Vector3_op_Multiply_m3577861744(NULL /*static, unused*/, L_4, ((float)((float)L_7*(float)(0.1f))), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Translate_m1352386541(L_1, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		bool L_9 = __this->get_moveReverse_5();
		if (!L_9)
		{
			goto IL_0086;
		}
	}
	{
		Transform_t3933397867 * L_10 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t596762001_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_11 = Vector3_get_back_m2431645669(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = Time_get_deltaTime_m1087255831(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t596762001  L_13 = Vector3_op_Multiply_m3577861744(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Transform_t3933397867 * L_14 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t596762001  L_15 = Transform_get_localScale_m1140650081(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = (&V_1)->get_x_1();
		Vector3_t596762001  L_17 = Vector3_op_Multiply_m3577861744(NULL /*static, unused*/, L_13, ((float)((float)L_16*(float)(0.1f))), /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_Translate_m1352386541(L_10, L_17, /*hidden argument*/NULL);
	}

IL_0086:
	{
		bool L_18 = __this->get_detectedMonster_3();
		if (!L_18)
		{
			goto IL_00b8;
		}
	}
	{
		Animation_t1557311624 * L_19 = __this->get_animation_2();
		NullCheck(L_19);
		bool L_20 = Animation_get_isPlaying_m2318637521(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		Animation_t1557311624 * L_21 = __this->get_animation_2();
		NullCheck(L_21);
		Animation_Stop_m239272431(L_21, _stringLiteral2086967534, /*hidden argument*/NULL);
		__this->set_moveForward_4((bool)0);
	}

IL_00b8:
	{
		bool L_22 = __this->get_KnightDeath_6();
		if (L_22)
		{
			goto IL_00d9;
		}
	}
	{
		Animation_t1557311624 * L_23 = __this->get_animation_2();
		NullCheck(L_23);
		bool L_24 = Animation_get_isPlaying_m2318637521(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00d9;
		}
	}
	{
		KnightControl_Wait_m4054482282(__this, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		return;
	}
}
// System.Void KnightControl::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void KnightControl_OnTriggerEnter_m306515042 (KnightControl_t1902388948 * __this, Collider_t1354100743 * ___col0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_OnTriggerEnter_m306515042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral3368953453, /*hidden argument*/NULL);
		__this->set_detectedMonster_3((bool)1);
		return;
	}
}
// System.Void KnightControl::OnTriggerExit(UnityEngine.Collider)
extern "C"  void KnightControl_OnTriggerExit_m2870755794 (KnightControl_t1902388948 * __this, Collider_t1354100743 * ___col0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_OnTriggerExit_m2870755794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral4060027686, /*hidden argument*/NULL);
		__this->set_detectedMonster_3((bool)0);
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_IsPlaying_m843324840(L_0, _stringLiteral3393300267, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		Animation_Play_m2990701052(L_2, _stringLiteral2086967534, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void KnightControl::Reverse()
extern "C"  void KnightControl_Reverse_m1515466881 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_Reverse_m1515466881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		AnimationState_t2216918921 * L_1 = Animation_get_Item_m86925464(L_0, _stringLiteral2086967534, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationState_set_speed_m1524741311(L_1, (-1.0f), /*hidden argument*/NULL);
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		bool L_3 = Animation_IsPlaying_m843324840(L_2, _stringLiteral3393300267, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral1715509795, /*hidden argument*/NULL);
		__this->set_moveForward_4((bool)0);
		__this->set_moveReverse_5((bool)1);
		Animation_t1557311624 * L_4 = __this->get_animation_2();
		NullCheck(L_4);
		Animation_Play_m2990701052(L_4, _stringLiteral2086967534, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_005d:
	{
		Animation_t1557311624 * L_5 = __this->get_animation_2();
		NullCheck(L_5);
		bool L_6 = Animation_IsPlaying_m843324840(L_5, _stringLiteral2086967534, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0090;
		}
	}
	{
		bool L_7 = __this->get_moveForward_4();
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		__this->set_moveForward_4((bool)0);
		__this->set_moveReverse_5((bool)1);
		goto IL_00d6;
	}

IL_0090:
	{
		Animation_t1557311624 * L_8 = __this->get_animation_2();
		NullCheck(L_8);
		bool L_9 = Animation_IsPlaying_m843324840(L_8, _stringLiteral2086967534, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00d6;
		}
	}
	{
		Animation_t1557311624 * L_10 = __this->get_animation_2();
		NullCheck(L_10);
		bool L_11 = Animation_IsPlaying_m843324840(L_10, _stringLiteral3393300267, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral87740549, /*hidden argument*/NULL);
		Animation_t1557311624 * L_12 = __this->get_animation_2();
		NullCheck(L_12);
		Animation_Stop_m2243275541(L_12, /*hidden argument*/NULL);
		__this->set_moveReverse_5((bool)0);
	}

IL_00d6:
	{
		return;
	}
}
// System.Void KnightControl::Forward()
extern "C"  void KnightControl_Forward_m2448336092 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_Forward_m2448336092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		AnimationState_t2216918921 * L_1 = Animation_get_Item_m86925464(L_0, _stringLiteral2086967534, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationState_set_speed_m1524741311(L_1, (1.0f), /*hidden argument*/NULL);
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		bool L_3 = Animation_IsPlaying_m843324840(L_2, _stringLiteral3393300267, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral1715509795, /*hidden argument*/NULL);
		__this->set_moveForward_4((bool)1);
		__this->set_moveReverse_5((bool)0);
		Animation_t1557311624 * L_4 = __this->get_animation_2();
		NullCheck(L_4);
		Animation_Play_m2990701052(L_4, _stringLiteral2086967534, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_005d:
	{
		Animation_t1557311624 * L_5 = __this->get_animation_2();
		NullCheck(L_5);
		bool L_6 = Animation_IsPlaying_m843324840(L_5, _stringLiteral2086967534, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0090;
		}
	}
	{
		bool L_7 = __this->get_moveReverse_5();
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		__this->set_moveForward_4((bool)1);
		__this->set_moveReverse_5((bool)0);
		goto IL_00d6;
	}

IL_0090:
	{
		Animation_t1557311624 * L_8 = __this->get_animation_2();
		NullCheck(L_8);
		bool L_9 = Animation_IsPlaying_m843324840(L_8, _stringLiteral2086967534, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00d6;
		}
	}
	{
		Animation_t1557311624 * L_10 = __this->get_animation_2();
		NullCheck(L_10);
		bool L_11 = Animation_IsPlaying_m843324840(L_10, _stringLiteral3393300267, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral87740549, /*hidden argument*/NULL);
		Animation_t1557311624 * L_12 = __this->get_animation_2();
		NullCheck(L_12);
		Animation_Stop_m2243275541(L_12, /*hidden argument*/NULL);
		__this->set_moveForward_4((bool)0);
	}

IL_00d6:
	{
		return;
	}
}
// System.Void KnightControl::Wait()
extern "C"  void KnightControl_Wait_m4054482282 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_Wait_m4054482282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		Animation_Play_m2990701052(L_0, _stringLiteral3393300267, /*hidden argument*/NULL);
		return;
	}
}
// System.Void KnightControl::Attack()
extern "C"  void KnightControl_Attack_m4293413112 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_Attack_m4293413112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_get_isPlaying_m2318637521(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		__this->set_moveForward_4((bool)0);
		__this->set_moveReverse_5((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral4284855045, /*hidden argument*/NULL);
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		Animation_Play_m2990701052(L_2, _stringLiteral98951896, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void KnightControl::KnightDamaged()
extern "C"  void KnightControl_KnightDamaged_m167166218 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_KnightDamaged_m167166218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_get_isPlaying_m2318637521(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		Animation_Play_m2990701052(L_2, _stringLiteral3730573748, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void KnightControl::KnightDead()
extern "C"  void KnightControl_KnightDead_m2753808469 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_KnightDead_m2753808469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_IsPlaying_m843324840(L_0, _stringLiteral3393300267, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral2083696160, /*hidden argument*/NULL);
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		Animation_Play_m2990701052(L_2, _stringLiteral4194900223, /*hidden argument*/NULL);
		__this->set_KnightDeath_6((bool)1);
	}

IL_0037:
	{
		return;
	}
}
// System.Void KnightControl::Revive()
extern "C"  void KnightControl_Revive_m3691166334 (KnightControl_t1902388948 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KnightControl_Revive_m3691166334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_get_isPlaying_m2318637521(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		__this->set_KnightDeath_6((bool)0);
		goto IL_0026;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral32909362, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void ModeSwitcher::.ctor()
extern "C"  void ModeSwitcher__ctor_m2115189420 (ModeSwitcher_t833699841 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModeSwitcher::Start()
extern "C"  void ModeSwitcher_Start_m4201649904 (ModeSwitcher_t833699841 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ModeSwitcher::Update()
extern "C"  void ModeSwitcher_Update_m1259867930 (ModeSwitcher_t833699841 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ModeSwitcher::EnableBallCreation(System.Boolean)
extern "C"  void ModeSwitcher_EnableBallCreation_m1117476675 (ModeSwitcher_t833699841 * __this, bool ___enable0, const RuntimeMethod* method)
{
	{
		GameObject_t3649338848 * L_0 = __this->get_ballMake_2();
		bool L_1 = ___enable0;
		NullCheck(L_0);
		GameObject_SetActive_m3661033924(L_0, L_1, /*hidden argument*/NULL);
		GameObject_t3649338848 * L_2 = __this->get_ballMove_3();
		bool L_3 = ___enable0;
		NullCheck(L_2);
		GameObject_SetActive_m3661033924(L_2, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModeSwitcher::OnGUI()
extern "C"  void ModeSwitcher_OnGUI_m270521944 (ModeSwitcher_t833699841 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ModeSwitcher_OnGUI_m270521944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		int32_t L_0 = __this->get_appMode_4();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = _stringLiteral1950011964;
		goto IL_001a;
	}

IL_0015:
	{
		G_B3_0 = _stringLiteral154465382;
	}

IL_001a:
	{
		V_0 = G_B3_0;
		int32_t L_1 = Screen_get_width_m4062517734(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1992046353  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m1383055445((&L_2), ((float)((float)(((float)((float)L_1)))-(float)(150.0f))), (0.0f), (150.0f), (100.0f), /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_4 = GUI_Button_m1880561377(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_5 = __this->get_appMode_4();
		__this->set_appMode_4(((int32_t)((int32_t)((int32_t)((int32_t)L_5+(int32_t)1))%(int32_t)2)));
		int32_t L_6 = __this->get_appMode_4();
		ModeSwitcher_EnableBallCreation_m1117476675(__this, (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void ParticlePainter::.ctor()
extern "C"  void ParticlePainter__ctor_m3031644694 (ParticlePainter_t294017160 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticlePainter__ctor_m3031644694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_particleSize_6((0.1f));
		__this->set_penDistance_7((0.2f));
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t596762001_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_0 = Vector3_get_zero_m325886990(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_previousPosition_11(L_0);
		Color_t320819310  L_1 = Color_get_white_m3957571917(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentColor_13(L_1);
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ParticlePainter::Start()
extern "C"  void ParticlePainter_Start_m2700506111 (ParticlePainter_t294017160 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticlePainter_Start_m2700506111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)ParticlePainter_ARFrameUpdated_m3938859628_RuntimeMethod_var);
		ARFrameUpdate_t3333962149 * L_1 = (ARFrameUpdate_t3333962149 *)il2cpp_codegen_object_new(ARFrameUpdate_t3333962149_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m722081207(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m1467721625(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ParticleSystem_t1777616458 * L_2 = __this->get_painterParticlePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		ParticleSystem_t1777616458 * L_3 = Object_Instantiate_TisParticleSystem_t1777616458_m895722352(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisParticleSystem_t1777616458_m895722352_RuntimeMethod_var);
		__this->set_currentPS_9(L_3);
		List_1_t3635081580 * L_4 = (List_1_t3635081580 *)il2cpp_codegen_object_new(List_1_t3635081580_il2cpp_TypeInfo_var);
		List_1__ctor_m3365215536(L_4, /*hidden argument*/List_1__ctor_m3365215536_RuntimeMethod_var);
		__this->set_currentPaintVertices_12(L_4);
		List_1_t520968741 * L_5 = (List_1_t520968741 *)il2cpp_codegen_object_new(List_1_t520968741_il2cpp_TypeInfo_var);
		List_1__ctor_m3297558810(L_5, /*hidden argument*/List_1__ctor_m3297558810_RuntimeMethod_var);
		__this->set_paintSystems_14(L_5);
		__this->set_frameUpdated_5((bool)0);
		ColorPicker_t2582382693 * L_6 = __this->get_colorPicker_8();
		NullCheck(L_6);
		ColorChangedEvent_t347040188 * L_7 = L_6->get_onValueChanged_9();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)ParticlePainter_U3CStartU3Em__0_m4245901003_RuntimeMethod_var);
		UnityAction_1_t1392064635 * L_9 = (UnityAction_1_t1392064635 *)il2cpp_codegen_object_new(UnityAction_1_t1392064635_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4180551968(L_9, __this, L_8, /*hidden argument*/UnityAction_1__ctor_m4180551968_RuntimeMethod_var);
		NullCheck(L_7);
		UnityEvent_1_AddListener_m2001044386(L_7, L_9, /*hidden argument*/UnityEvent_1_AddListener_m2001044386_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_10 = __this->get_colorPicker_8();
		NullCheck(L_10);
		GameObject_t3649338848 * L_11 = Component_get_gameObject_m3065601689(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_SetActive_m3661033924(L_11, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ParticlePainter::ARFrameUpdated(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void ParticlePainter_ARFrameUpdated_m3938859628 (ParticlePainter_t294017160 * __this, UnityARCamera_t2376600594  ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticlePainter_ARFrameUpdated_m3938859628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t1288378485  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t596762001  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (Matrix4x4_t1288378485_il2cpp_TypeInfo_var, (&V_0));
		UnityARMatrix4x4_t1132406930 * L_0 = (&___camera0)->get_address_of_worldTransform_0();
		Vector4_t1376926224  L_1 = L_0->get_column3_3();
		Matrix4x4_SetColumn_m567234837((&V_0), 3, L_1, /*hidden argument*/NULL);
		Matrix4x4_t1288378485  L_2 = V_0;
		Vector3_t596762001  L_3 = UnityARMatrixOps_GetPosition_m3553979848(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Camera_t226495598 * L_4 = Camera_get_main_m2671983186(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3933397867 * L_5 = Component_get_transform_m2033240428(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t596762001  L_6 = Transform_get_forward_m3993346221(L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_penDistance_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t596762001_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_8 = Vector3_op_Multiply_m3577861744(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t596762001  L_9 = Vector3_op_Addition_m901452055(NULL /*static, unused*/, L_3, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Vector3_t596762001  L_10 = V_1;
		Vector3_t596762001  L_11 = __this->get_previousPosition_11();
		float L_12 = Vector3_Distance_m3935979045(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = __this->get_minDistanceThreshold_3();
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = __this->get_paintMode_15();
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_0071;
		}
	}
	{
		List_1_t3635081580 * L_15 = __this->get_currentPaintVertices_12();
		Vector3_t596762001  L_16 = V_1;
		NullCheck(L_15);
		List_1_Add_m684783150(L_15, L_16, /*hidden argument*/List_1_Add_m684783150_RuntimeMethod_var);
	}

IL_0071:
	{
		__this->set_frameUpdated_5((bool)1);
		Vector3_t596762001  L_17 = V_1;
		__this->set_previousPosition_11(L_17);
	}

IL_007f:
	{
		return;
	}
}
// System.Void ParticlePainter::OnGUI()
extern "C"  void ParticlePainter_OnGUI_m975798185 (ParticlePainter_t294017160 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticlePainter_OnGUI_m975798185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B5_0 = NULL;
	{
		int32_t L_0 = __this->get_paintMode_15();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		G_B5_0 = _stringLiteral2194669068;
		goto IL_0030;
	}

IL_0015:
	{
		int32_t L_1 = __this->get_paintMode_15();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_002b;
		}
	}
	{
		G_B5_0 = _stringLiteral3753373970;
		goto IL_0030;
	}

IL_002b:
	{
		G_B5_0 = _stringLiteral2701812837;
	}

IL_0030:
	{
		V_0 = G_B5_0;
		int32_t L_2 = Screen_get_width_m4062517734(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1992046353  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1383055445((&L_3), ((float)((float)(((float)((float)L_2)))-(float)(100.0f))), (0.0f), (100.0f), (50.0f), /*hidden argument*/NULL);
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_5 = GUI_Button_m1880561377(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0097;
		}
	}
	{
		int32_t L_6 = __this->get_paintMode_15();
		__this->set_paintMode_15(((int32_t)((int32_t)((int32_t)((int32_t)L_6+(int32_t)1))%(int32_t)3)));
		ColorPicker_t2582382693 * L_7 = __this->get_colorPicker_8();
		NullCheck(L_7);
		GameObject_t3649338848 * L_8 = Component_get_gameObject_m3065601689(L_7, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_paintMode_15();
		NullCheck(L_8);
		GameObject_SetActive_m3661033924(L_8, (bool)((((int32_t)L_9) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_10 = __this->get_paintMode_15();
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_0097;
		}
	}
	{
		ParticlePainter_RestartPainting_m2557631278(__this, /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.Void ParticlePainter::RestartPainting()
extern "C"  void ParticlePainter_RestartPainting_m2557631278 (ParticlePainter_t294017160 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticlePainter_RestartPainting_m2557631278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t520968741 * L_0 = __this->get_paintSystems_14();
		ParticleSystem_t1777616458 * L_1 = __this->get_currentPS_9();
		NullCheck(L_0);
		List_1_Add_m1608859291(L_0, L_1, /*hidden argument*/List_1_Add_m1608859291_RuntimeMethod_var);
		ParticleSystem_t1777616458 * L_2 = __this->get_painterParticlePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		ParticleSystem_t1777616458 * L_3 = Object_Instantiate_TisParticleSystem_t1777616458_m895722352(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisParticleSystem_t1777616458_m895722352_RuntimeMethod_var);
		__this->set_currentPS_9(L_3);
		List_1_t3635081580 * L_4 = (List_1_t3635081580 *)il2cpp_codegen_object_new(List_1_t3635081580_il2cpp_TypeInfo_var);
		List_1__ctor_m3365215536(L_4, /*hidden argument*/List_1__ctor_m3365215536_RuntimeMethod_var);
		__this->set_currentPaintVertices_12(L_4);
		return;
	}
}
// System.Void ParticlePainter::Update()
extern "C"  void ParticlePainter_Update_m3795775040 (ParticlePainter_t294017160 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticlePainter_Update_m3795775040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ParticleU5BU5D_t419044168* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t596762001  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Enumerator_t688973221  V_4;
	memset(&V_4, 0, sizeof(V_4));
	ParticleU5BU5D_t419044168* V_5 = NULL;
	Exception_t82373287 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t82373287 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_frameUpdated_5();
		if (!L_0)
		{
			goto IL_00f1;
		}
	}
	{
		int32_t L_1 = __this->get_paintMode_15();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_00f1;
		}
	}
	{
		List_1_t3635081580 * L_2 = __this->get_currentPaintVertices_12();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m3739057527(L_2, /*hidden argument*/List_1_get_Count_m3739057527_RuntimeMethod_var);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_00c2;
		}
	}
	{
		List_1_t3635081580 * L_4 = __this->get_currentPaintVertices_12();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m3739057527(L_4, /*hidden argument*/List_1_get_Count_m3739057527_RuntimeMethod_var);
		V_0 = L_5;
		int32_t L_6 = V_0;
		V_1 = ((ParticleU5BU5D_t419044168*)SZArrayNew(ParticleU5BU5D_t419044168_il2cpp_TypeInfo_var, (uint32_t)L_6));
		V_2 = 0;
		List_1_t3635081580 * L_7 = __this->get_currentPaintVertices_12();
		NullCheck(L_7);
		Enumerator_t688973221  L_8 = List_1_GetEnumerator_m2550167595(L_7, /*hidden argument*/List_1_GetEnumerator_m2550167595_RuntimeMethod_var);
		V_4 = L_8;
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0091;
		}

IL_004f:
		{
			Vector3_t596762001  L_9 = Enumerator_get_Current_m1534598271((&V_4), /*hidden argument*/Enumerator_get_Current_m1534598271_RuntimeMethod_var);
			V_3 = L_9;
			ParticleU5BU5D_t419044168* L_10 = V_1;
			int32_t L_11 = V_2;
			NullCheck(L_10);
			Vector3_t596762001  L_12 = V_3;
			Particle_set_position_m2752520204(((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11))), L_12, /*hidden argument*/NULL);
			ParticleU5BU5D_t419044168* L_13 = V_1;
			int32_t L_14 = V_2;
			NullCheck(L_13);
			Color_t320819310  L_15 = __this->get_currentColor_13();
			Color32_t2499566028  L_16 = Color32_op_Implicit_m4104578668(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			Particle_set_startColor_m577033657(((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14))), L_16, /*hidden argument*/NULL);
			ParticleU5BU5D_t419044168* L_17 = V_1;
			int32_t L_18 = V_2;
			NullCheck(L_17);
			float L_19 = __this->get_particleSize_6();
			Particle_set_startSize_m2538937397(((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), L_19, /*hidden argument*/NULL);
			int32_t L_20 = V_2;
			V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
		}

IL_0091:
		{
			bool L_21 = Enumerator_MoveNext_m2323590328((&V_4), /*hidden argument*/Enumerator_MoveNext_m2323590328_RuntimeMethod_var);
			if (L_21)
			{
				goto IL_004f;
			}
		}

IL_009d:
		{
			IL2CPP_LEAVE(0xB0, FINALLY_00a2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t82373287 *)e.ex;
		goto FINALLY_00a2;
	}

FINALLY_00a2:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3684835367((&V_4), /*hidden argument*/Enumerator_Dispose_m3684835367_RuntimeMethod_var);
		IL2CPP_END_FINALLY(162)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(162)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t82373287 *)
	}

IL_00b0:
	{
		ParticleSystem_t1777616458 * L_22 = __this->get_currentPS_9();
		ParticleU5BU5D_t419044168* L_23 = V_1;
		int32_t L_24 = V_0;
		NullCheck(L_22);
		ParticleSystem_SetParticles_m1829004744(L_22, L_23, L_24, /*hidden argument*/NULL);
		goto IL_00ea;
	}

IL_00c2:
	{
		V_5 = ((ParticleU5BU5D_t419044168*)SZArrayNew(ParticleU5BU5D_t419044168_il2cpp_TypeInfo_var, (uint32_t)1));
		ParticleU5BU5D_t419044168* L_25 = V_5;
		NullCheck(L_25);
		Particle_set_startSize_m2538937397(((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), (0.0f), /*hidden argument*/NULL);
		ParticleSystem_t1777616458 * L_26 = __this->get_currentPS_9();
		ParticleU5BU5D_t419044168* L_27 = V_5;
		NullCheck(L_26);
		ParticleSystem_SetParticles_m1829004744(L_26, L_27, 1, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		__this->set_frameUpdated_5((bool)0);
	}

IL_00f1:
	{
		return;
	}
}
// System.Void ParticlePainter::<Start>m__0(UnityEngine.Color)
extern "C"  void ParticlePainter_U3CStartU3Em__0_m4245901003 (ParticlePainter_t294017160 * __this, Color_t320819310  ___newColor0, const RuntimeMethod* method)
{
	{
		Color_t320819310  L_0 = ___newColor0;
		__this->set_currentColor_13(L_0);
		return;
	}
}
// System.Void ScreenShot::.ctor()
extern "C"  void ScreenShot__ctor_m2547647503 (ScreenShot_t707795496 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScreenShot::Start()
extern "C"  void ScreenShot_Start_m1707547866 (ScreenShot_t707795496 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenShot_Start_m1707547866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_format_2(_stringLiteral1601136640);
		return;
	}
}
// System.Void ScreenShot::Update()
extern "C"  void ScreenShot_Update_m691034463 (ScreenShot_t707795496 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenShot_Update_m691034463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t1819153659  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1121709983_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1060467690(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_1 = Application_get_dataPath_m192717127(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1819153659_il2cpp_TypeInfo_var);
		DateTime_t1819153659  L_2 = DateTime_get_Now_m938408713(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = __this->get_format_2();
		String_t* L_4 = DateTime_ToString_m1624830502((&V_0), L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m716071259(NULL /*static, unused*/, L_1, _stringLiteral1682919869, L_4, _stringLiteral1812546333, /*hidden argument*/NULL);
		ScreenCapture_CaptureScreenshot_m4292871561(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void SlimeControl::.ctor()
extern "C"  void SlimeControl__ctor_m2753630247 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SlimeControl::Start()
extern "C"  void SlimeControl_Start_m3521291899 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_Start_m3521291899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = Component_GetComponent_TisAnimation_t1557311624_m834402472(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t1557311624_m834402472_RuntimeMethod_var);
		__this->set_animation_2(L_0);
		return;
	}
}
// System.Void SlimeControl::Update()
extern "C"  void SlimeControl_Update_m3521749286 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_Update_m3521749286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t596762001  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_shouldMove_3();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		Transform_t3933397867 * L_1 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t596762001_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_2 = Vector3_get_forward_m4168834013(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m1087255831(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t596762001  L_4 = Vector3_op_Multiply_m3577861744(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Transform_t3933397867 * L_5 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t596762001  L_6 = Transform_get_localScale_m1140650081(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (&V_0)->get_x_1();
		Vector3_t596762001  L_8 = Vector3_op_Multiply_m3577861744(NULL /*static, unused*/, L_4, ((float)((float)L_7*(float)(0.1f))), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Translate_m1352386541(L_1, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		bool L_9 = __this->get_SlimeDeath_4();
		if (L_9)
		{
			goto IL_0064;
		}
	}
	{
		Animation_t1557311624 * L_10 = __this->get_animation_2();
		NullCheck(L_10);
		bool L_11 = Animation_get_isPlaying_m2318637521(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0064;
		}
	}
	{
		SlimeControl_Wait_m2044302504(__this, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void SlimeControl::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void SlimeControl_OnTriggerEnter_m3552506635 (SlimeControl_t1232093680 * __this, Collider_t1354100743 * ___col0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_OnTriggerEnter_m3552506635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral700932277, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SlimeControl::OnTriggerExit(UnityEngine.Collider)
extern "C"  void SlimeControl_OnTriggerExit_m2068307658 (SlimeControl_t1232093680 * __this, Collider_t1354100743 * ___col0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_OnTriggerExit_m2068307658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral3347777996, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SlimeControl::Walk()
extern "C"  void SlimeControl_Walk_m2369857677 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_Walk_m2369857677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_get_isPlaying_m2318637521(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		Animation_Play_m2990701052(L_2, _stringLiteral2086967534, /*hidden argument*/NULL);
		__this->set_shouldMove_3((bool)1);
		goto IL_003f;
	}

IL_002d:
	{
		Animation_t1557311624 * L_3 = __this->get_animation_2();
		NullCheck(L_3);
		Animation_Stop_m2243275541(L_3, /*hidden argument*/NULL);
		__this->set_shouldMove_3((bool)0);
	}

IL_003f:
	{
		return;
	}
}
// System.Void SlimeControl::Wait()
extern "C"  void SlimeControl_Wait_m2044302504 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_Wait_m2044302504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_get_isPlaying_m2318637521(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		Animation_Play_m2990701052(L_2, _stringLiteral3393300267, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0026:
	{
		Animation_t1557311624 * L_3 = __this->get_animation_2();
		NullCheck(L_3);
		Animation_Stop_m2243275541(L_3, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void SlimeControl::Attack()
extern "C"  void SlimeControl_Attack_m2278930363 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_Attack_m2278930363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_get_isPlaying_m2318637521(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		Animation_t1557311624 * L_2 = __this->get_animation_2();
		NullCheck(L_2);
		Animation_Play_m2990701052(L_2, _stringLiteral98951896, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0026:
	{
		Animation_t1557311624 * L_3 = __this->get_animation_2();
		NullCheck(L_3);
		Animation_Stop_m2243275541(L_3, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void SlimeControl::SlimeDamaged()
extern "C"  void SlimeControl_SlimeDamaged_m1220440998 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_SlimeDamaged_m1220440998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_SlimeDeath_4();
		if (L_0)
		{
			goto IL_008f;
		}
	}
	{
		int32_t L_1 = Random_Range_m475641833(NULL /*static, unused*/, 0, 3, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0068;
		}
	}
	{
		Animation_t1557311624 * L_3 = __this->get_animation_2();
		NullCheck(L_3);
		bool L_4 = Animation_IsPlaying_m843324840(L_3, _stringLiteral3393300267, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral3020831419, /*hidden argument*/NULL);
		Animation_t1557311624 * L_5 = __this->get_animation_2();
		NullCheck(L_5);
		Animation_Play_m2990701052(L_5, _stringLiteral3730573748, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral87740549, /*hidden argument*/NULL);
		Animation_t1557311624 * L_6 = __this->get_animation_2();
		NullCheck(L_6);
		Animation_Stop_m2243275541(L_6, /*hidden argument*/NULL);
	}

IL_0063:
	{
		goto IL_008a;
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral3311888704, /*hidden argument*/NULL);
		Animation_t1557311624 * L_7 = __this->get_animation_2();
		NullCheck(L_7);
		Animation_Play_m2990701052(L_7, _stringLiteral4194900223, /*hidden argument*/NULL);
		__this->set_SlimeDeath_4((bool)1);
	}

IL_008a:
	{
		goto IL_0099;
	}

IL_008f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral617751626, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.Void SlimeControl::Revive()
extern "C"  void SlimeControl_Revive_m3625609039 (SlimeControl_t1232093680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlimeControl_Revive_m3625609039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = __this->get_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_get_isPlaying_m2318637521(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		__this->set_SlimeDeath_4((bool)0);
		goto IL_0026;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3314485529_il2cpp_TypeInfo_var);
		Debug_Log_m2782172005(NULL /*static, unused*/, _stringLiteral722121443, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void SVBoxSlider::.ctor()
extern "C"  void SVBoxSlider__ctor_m581354701 (SVBoxSlider_t2769455468 * __this, const RuntimeMethod* method)
{
	{
		__this->set_lastH_5((-1.0f));
		__this->set_listen_6((bool)1);
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform SVBoxSlider::get_rectTransform()
extern "C"  RectTransform_t1885177139 * SVBoxSlider_get_rectTransform_m4173866029 (SVBoxSlider_t2769455468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_get_rectTransform_m4173866029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3933397867 * L_0 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		return ((RectTransform_t1885177139 *)IsInstSealed((RuntimeObject*)L_0, RectTransform_t1885177139_il2cpp_TypeInfo_var));
	}
}
// System.Void SVBoxSlider::Awake()
extern "C"  void SVBoxSlider_Awake_m912103099 (SVBoxSlider_t2769455468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_Awake_m912103099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BoxSlider_t2780260910 * L_0 = Component_GetComponent_TisBoxSlider_t2780260910_m43308176(__this, /*hidden argument*/Component_GetComponent_TisBoxSlider_t2780260910_m43308176_RuntimeMethod_var);
		__this->set_slider_3(L_0);
		RawImage_t3037409423 * L_1 = Component_GetComponent_TisRawImage_t3037409423_m801911595(__this, /*hidden argument*/Component_GetComponent_TisRawImage_t3037409423_m801911595_RuntimeMethod_var);
		__this->set_image_4(L_1);
		SVBoxSlider_RegenerateSVTexture_m4214705435(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SVBoxSlider::OnEnable()
extern "C"  void SVBoxSlider_OnEnable_m953190647 (SVBoxSlider_t2769455468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_OnEnable_m953190647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2992201115(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		ColorPicker_t2582382693 * L_1 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		BoxSlider_t2780260910 * L_3 = __this->get_slider_3();
		NullCheck(L_3);
		BoxSliderEvent_t2026169079 * L_4 = BoxSlider_get_onValueChanged_m180314062(L_3, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)SVBoxSlider_SliderChanged_m2185817435_RuntimeMethod_var);
		UnityAction_2_t3878312970 * L_6 = (UnityAction_2_t3878312970 *)il2cpp_codegen_object_new(UnityAction_2_t3878312970_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m4219090612(L_6, __this, L_5, /*hidden argument*/UnityAction_2__ctor_m4219090612_RuntimeMethod_var);
		NullCheck(L_4);
		UnityEvent_2_AddListener_m3369274527(L_4, L_6, /*hidden argument*/UnityEvent_2_AddListener_m3369274527_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_7 = __this->get_picker_2();
		NullCheck(L_7);
		HSVChangedEvent_t3546572410 * L_8 = L_7->get_onHSVChanged_10();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)SVBoxSlider_HSVChanged_m2761716849_RuntimeMethod_var);
		UnityAction_3_t3771153942 * L_10 = (UnityAction_3_t3771153942 *)il2cpp_codegen_object_new(UnityAction_3_t3771153942_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m672494230(L_10, __this, L_9, /*hidden argument*/UnityAction_3__ctor_m672494230_RuntimeMethod_var);
		NullCheck(L_8);
		UnityEvent_3_AddListener_m3442876925(L_8, L_10, /*hidden argument*/UnityEvent_3_AddListener_m3442876925_RuntimeMethod_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void SVBoxSlider::OnDisable()
extern "C"  void SVBoxSlider_OnDisable_m2057716965 (SVBoxSlider_t2769455468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_OnDisable_m2057716965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		BoxSlider_t2780260910 * L_2 = __this->get_slider_3();
		NullCheck(L_2);
		BoxSliderEvent_t2026169079 * L_3 = BoxSlider_get_onValueChanged_m180314062(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)SVBoxSlider_SliderChanged_m2185817435_RuntimeMethod_var);
		UnityAction_2_t3878312970 * L_5 = (UnityAction_2_t3878312970 *)il2cpp_codegen_object_new(UnityAction_2_t3878312970_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m4219090612(L_5, __this, L_4, /*hidden argument*/UnityAction_2__ctor_m4219090612_RuntimeMethod_var);
		NullCheck(L_3);
		UnityEvent_2_RemoveListener_m1528257211(L_3, L_5, /*hidden argument*/UnityEvent_2_RemoveListener_m1528257211_RuntimeMethod_var);
		ColorPicker_t2582382693 * L_6 = __this->get_picker_2();
		NullCheck(L_6);
		HSVChangedEvent_t3546572410 * L_7 = L_6->get_onHSVChanged_10();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)SVBoxSlider_HSVChanged_m2761716849_RuntimeMethod_var);
		UnityAction_3_t3771153942 * L_9 = (UnityAction_3_t3771153942 *)il2cpp_codegen_object_new(UnityAction_3_t3771153942_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_m672494230(L_9, __this, L_8, /*hidden argument*/UnityAction_3__ctor_m672494230_RuntimeMethod_var);
		NullCheck(L_7);
		UnityEvent_3_RemoveListener_m2900402221(L_7, L_9, /*hidden argument*/UnityEvent_3_RemoveListener_m2900402221_RuntimeMethod_var);
	}

IL_0049:
	{
		return;
	}
}
// System.Void SVBoxSlider::OnDestroy()
extern "C"  void SVBoxSlider_OnDestroy_m1753957120 (SVBoxSlider_t2769455468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_OnDestroy_m1753957120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t3037409423 * L_0 = __this->get_image_4();
		NullCheck(L_0);
		Texture_t1132728222 * L_1 = RawImage_get_texture_m429915103(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		RawImage_t3037409423 * L_3 = __this->get_image_4();
		NullCheck(L_3);
		Texture_t1132728222 * L_4 = RawImage_get_texture_m429915103(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m1495482491(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void SVBoxSlider::SliderChanged(System.Single,System.Single)
extern "C"  void SVBoxSlider_SliderChanged_m2185817435 (SVBoxSlider_t2769455468 * __this, float ___saturation0, float ___value1, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_listen_6();
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		ColorPicker_t2582382693 * L_1 = __this->get_picker_2();
		float L_2 = ___saturation0;
		NullCheck(L_1);
		ColorPicker_AssignColor_m1478515366(L_1, 5, L_2, /*hidden argument*/NULL);
		ColorPicker_t2582382693 * L_3 = __this->get_picker_2();
		float L_4 = ___value1;
		NullCheck(L_3);
		ColorPicker_AssignColor_m1478515366(L_3, 6, L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		__this->set_listen_6((bool)1);
		return;
	}
}
// System.Void SVBoxSlider::HSVChanged(System.Single,System.Single,System.Single)
extern "C"  void SVBoxSlider_HSVChanged_m2761716849 (SVBoxSlider_t2769455468 * __this, float ___h0, float ___s1, float ___v2, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_lastH_5();
		float L_1 = ___h0;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		float L_2 = ___h0;
		__this->set_lastH_5(L_2);
		SVBoxSlider_RegenerateSVTexture_m4214705435(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		float L_3 = ___s1;
		BoxSlider_t2780260910 * L_4 = __this->get_slider_3();
		NullCheck(L_4);
		float L_5 = BoxSlider_get_normalizedValue_m1950536199(L_4, /*hidden argument*/NULL);
		if ((((float)L_3) == ((float)L_5)))
		{
			goto IL_003d;
		}
	}
	{
		__this->set_listen_6((bool)0);
		BoxSlider_t2780260910 * L_6 = __this->get_slider_3();
		float L_7 = ___s1;
		NullCheck(L_6);
		BoxSlider_set_normalizedValue_m1073637371(L_6, L_7, /*hidden argument*/NULL);
	}

IL_003d:
	{
		float L_8 = ___v2;
		BoxSlider_t2780260910 * L_9 = __this->get_slider_3();
		NullCheck(L_9);
		float L_10 = BoxSlider_get_normalizedValueY_m266672788(L_9, /*hidden argument*/NULL);
		if ((((float)L_8) == ((float)L_10)))
		{
			goto IL_0061;
		}
	}
	{
		__this->set_listen_6((bool)0);
		BoxSlider_t2780260910 * L_11 = __this->get_slider_3();
		float L_12 = ___v2;
		NullCheck(L_11);
		BoxSlider_set_normalizedValueY_m815891677(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
// System.Void SVBoxSlider::RegenerateSVTexture()
extern "C"  void SVBoxSlider_RegenerateSVTexture_m4214705435 (SVBoxSlider_t2769455468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SVBoxSlider_RegenerateSVTexture_m4214705435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	Texture2D_t2870930912 * V_1 = NULL;
	int32_t V_2 = 0;
	Color32U5BU5D_t636217733* V_3 = NULL;
	int32_t V_4 = 0;
	float G_B3_0 = 0.0f;
	{
		ColorPicker_t2582382693 * L_0 = __this->get_picker_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_0, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		ColorPicker_t2582382693 * L_2 = __this->get_picker_2();
		NullCheck(L_2);
		float L_3 = ColorPicker_get_H_m1137646974(L_2, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)L_3*(float)(360.0f)));
		goto IL_002c;
	}

IL_0027:
	{
		G_B3_0 = (0.0f);
	}

IL_002c:
	{
		V_0 = (((double)((double)G_B3_0)));
		RawImage_t3037409423 * L_4 = __this->get_image_4();
		NullCheck(L_4);
		Texture_t1132728222 * L_5 = RawImage_get_texture_m429915103(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_5, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0054;
		}
	}
	{
		RawImage_t3037409423 * L_7 = __this->get_image_4();
		NullCheck(L_7);
		Texture_t1132728222 * L_8 = RawImage_get_texture_m429915103(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m1495482491(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0054:
	{
		Texture2D_t2870930912 * L_9 = (Texture2D_t2870930912 *)il2cpp_codegen_object_new(Texture2D_t2870930912_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2324518131(L_9, ((int32_t)100), ((int32_t)100), /*hidden argument*/NULL);
		V_1 = L_9;
		Texture2D_t2870930912 * L_10 = V_1;
		NullCheck(L_10);
		Object_set_hideFlags_m3192056770(L_10, ((int32_t)52), /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_00cc;
	}

IL_006d:
	{
		V_3 = ((Color32U5BU5D_t636217733*)SZArrayNew(Color32U5BU5D_t636217733_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100)));
		V_4 = 0;
		goto IL_00b3;
	}

IL_007d:
	{
		Color32U5BU5D_t636217733* L_11 = V_3;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		double L_13 = V_0;
		int32_t L_14 = V_2;
		int32_t L_15 = V_4;
		Color_t320819310  L_16 = HSVUtil_ConvertHsvToRgb_m3345724273(NULL /*static, unused*/, L_13, (((double)((double)((float)((float)(((float)((float)L_14)))/(float)(100.0f)))))), (((double)((double)((float)((float)(((float)((float)L_15)))/(float)(100.0f)))))), (1.0f), /*hidden argument*/NULL);
		Color32_t2499566028  L_17 = Color32_op_Implicit_m4104578668(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		*(Color32_t2499566028 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12))) = L_17;
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_00b3:
	{
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) < ((int32_t)((int32_t)100))))
		{
			goto IL_007d;
		}
	}
	{
		Texture2D_t2870930912 * L_20 = V_1;
		int32_t L_21 = V_2;
		Color32U5BU5D_t636217733* L_22 = V_3;
		NullCheck(L_20);
		Texture2D_SetPixels32_m761225203(L_20, L_21, 0, 1, ((int32_t)100), L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00cc:
	{
		int32_t L_24 = V_2;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)100))))
		{
			goto IL_006d;
		}
	}
	{
		Texture2D_t2870930912 * L_25 = V_1;
		NullCheck(L_25);
		Texture2D_Apply_m4102782407(L_25, /*hidden argument*/NULL);
		RawImage_t3037409423 * L_26 = __this->get_image_4();
		Texture2D_t2870930912 * L_27 = V_1;
		NullCheck(L_26);
		RawImage_set_texture_m337957835(L_26, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TiltWindow::.ctor()
extern "C"  void TiltWindow__ctor_m4145182152 (TiltWindow_t1431928114 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TiltWindow__ctor_m4145182152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t59524482  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3619854127((&L_0), (5.0f), (3.0f), /*hidden argument*/NULL);
		__this->set_range_2(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector2_t59524482  L_1 = Vector2_get_zero_m3701231313(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mRot_5(L_1);
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TiltWindow::Start()
extern "C"  void TiltWindow_Start_m3575917805 (TiltWindow_t1431928114 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3933397867 * L_0 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		__this->set_mTrans_3(L_0);
		Transform_t3933397867 * L_1 = __this->get_mTrans_3();
		NullCheck(L_1);
		Quaternion_t3165733013  L_2 = Transform_get_localRotation_m3136276044(L_1, /*hidden argument*/NULL);
		__this->set_mStart_4(L_2);
		return;
	}
}
// System.Void TiltWindow::Update()
extern "C"  void TiltWindow_Update_m4026234967 (TiltWindow_t1431928114 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TiltWindow_Update_m4026234967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t596762001  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1121709983_il2cpp_TypeInfo_var);
		Vector3_t596762001  L_0 = Input_get_mousePosition_m443992087(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Screen_get_width_m4062517734(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)((float)L_1)))*(float)(0.5f)));
		int32_t L_2 = Screen_get_height_m2303689024(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((float)((float)(((float)((float)L_2)))*(float)(0.5f)));
		float L_3 = (&V_0)->get_x_1();
		float L_4 = V_1;
		float L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp_m934937530(NULL /*static, unused*/, ((float)((float)((float)((float)L_3-(float)L_4))/(float)L_5)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = (&V_0)->get_y_2();
		float L_8 = V_2;
		float L_9 = V_2;
		float L_10 = Mathf_Clamp_m934937530(NULL /*static, unused*/, ((float)((float)((float)((float)L_7-(float)L_8))/(float)L_9)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_10;
		Vector2_t59524482  L_11 = __this->get_mRot_5();
		float L_12 = V_3;
		float L_13 = V_4;
		Vector2_t59524482  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m3619854127((&L_14), L_12, L_13, /*hidden argument*/NULL);
		float L_15 = Time_get_deltaTime_m1087255831(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector2_t59524482  L_16 = Vector2_Lerp_m1060570619(NULL /*static, unused*/, L_11, L_14, ((float)((float)L_15*(float)(5.0f))), /*hidden argument*/NULL);
		__this->set_mRot_5(L_16);
		Transform_t3933397867 * L_17 = __this->get_mTrans_3();
		Quaternion_t3165733013  L_18 = __this->get_mStart_4();
		Vector2_t59524482 * L_19 = __this->get_address_of_mRot_5();
		float L_20 = L_19->get_y_1();
		Vector2_t59524482 * L_21 = __this->get_address_of_range_2();
		float L_22 = L_21->get_y_1();
		Vector2_t59524482 * L_23 = __this->get_address_of_mRot_5();
		float L_24 = L_23->get_x_0();
		Vector2_t59524482 * L_25 = __this->get_address_of_range_2();
		float L_26 = L_25->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3165733013_il2cpp_TypeInfo_var);
		Quaternion_t3165733013  L_27 = Quaternion_Euler_m437352610(NULL /*static, unused*/, ((float)((float)((-L_20))*(float)L_22)), ((float)((float)L_24*(float)L_26)), (0.0f), /*hidden argument*/NULL);
		Quaternion_t3165733013  L_28 = Quaternion_op_Multiply_m332349538(NULL /*static, unused*/, L_18, L_27, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localRotation_m1950704683(L_17, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::.ctor()
extern "C"  void BoxSlider__ctor_m162539454 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider__ctor_m162539454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_MaxValue_18((1.0f));
		__this->set_m_Value_20((1.0f));
		__this->set_m_ValueY_21((1.0f));
		BoxSliderEvent_t2026169079 * L_0 = (BoxSliderEvent_t2026169079 *)il2cpp_codegen_object_new(BoxSliderEvent_t2026169079_il2cpp_TypeInfo_var);
		BoxSliderEvent__ctor_m2331793288(L_0, /*hidden argument*/NULL);
		__this->set_m_OnValueChanged_22(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector2_t59524482  L_1 = Vector2_get_zero_m3701231313(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Offset_25(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Selectable_t3890617260_il2cpp_TypeInfo_var);
		Selectable__ctor_m1063272198(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::get_handleRect()
extern "C"  RectTransform_t1885177139 * BoxSlider_get_handleRect_m2534378138 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		RectTransform_t1885177139 * L_0 = __this->get_m_HandleRect_16();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_handleRect(UnityEngine.RectTransform)
extern "C"  void BoxSlider_set_handleRect_m1467039354 (BoxSlider_t2780260910 * __this, RectTransform_t1885177139 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_handleRect_m1467039354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t1885177139 ** L_0 = __this->get_address_of_m_HandleRect_16();
		RectTransform_t1885177139 * L_1 = ___value0;
		bool L_2 = BoxSlider_SetClass_TisRectTransform_t1885177139_m1931764714(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/BoxSlider_SetClass_TisRectTransform_t1885177139_m1931764714_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		BoxSlider_UpdateCachedReferences_m977902596(__this, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m4195678704(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_minValue()
extern "C"  float BoxSlider_get_minValue_m43333998 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_m_MinValue_17();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_minValue(System.Single)
extern "C"  void BoxSlider_set_minValue_m3177896132 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_minValue_m3177896132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float* L_0 = __this->get_address_of_m_MinValue_17();
		float L_1 = ___value0;
		bool L_2 = BoxSlider_SetStruct_TisSingle_t2847614712_m250040320(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/BoxSlider_SetStruct_TisSingle_t2847614712_m250040320_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = __this->get_m_Value_20();
		BoxSlider_Set_m1701966247(__this, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_m_ValueY_21();
		BoxSlider_SetY_m3783125856(__this, L_4, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m4195678704(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_maxValue()
extern "C"  float BoxSlider_get_maxValue_m1548328961 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_m_MaxValue_18();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_maxValue(System.Single)
extern "C"  void BoxSlider_set_maxValue_m646776916 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_maxValue_m646776916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float* L_0 = __this->get_address_of_m_MaxValue_18();
		float L_1 = ___value0;
		bool L_2 = BoxSlider_SetStruct_TisSingle_t2847614712_m250040320(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/BoxSlider_SetStruct_TisSingle_t2847614712_m250040320_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = __this->get_m_Value_20();
		BoxSlider_Set_m1701966247(__this, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_m_ValueY_21();
		BoxSlider_SetY_m3783125856(__this, L_4, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m4195678704(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.BoxSlider::get_wholeNumbers()
extern "C"  bool BoxSlider_get_wholeNumbers_m366103797 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_WholeNumbers_19();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_wholeNumbers(System.Boolean)
extern "C"  void BoxSlider_set_wholeNumbers_m4022631289 (BoxSlider_t2780260910 * __this, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_wholeNumbers_m4022631289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool* L_0 = __this->get_address_of_m_WholeNumbers_19();
		bool L_1 = ___value0;
		bool L_2 = BoxSlider_SetStruct_TisBoolean_t362855854_m292852063(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/BoxSlider_SetStruct_TisBoolean_t362855854_m292852063_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		float L_3 = __this->get_m_Value_20();
		BoxSlider_Set_m1701966247(__this, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_m_ValueY_21();
		BoxSlider_SetY_m3783125856(__this, L_4, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m4195678704(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_value()
extern "C"  float BoxSlider_get_value_m1655722908 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_get_value_m1655722908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = BoxSlider_get_wholeNumbers_m366103797(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = __this->get_m_Value_20();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_2 = bankers_roundf(L_1);
		return L_2;
	}

IL_0017:
	{
		float L_3 = __this->get_m_Value_20();
		return L_3;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_value(System.Single)
extern "C"  void BoxSlider_set_value_m2367170594 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		BoxSlider_Set_m1701966247(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_normalizedValue()
extern "C"  float BoxSlider_get_normalizedValue_m1950536199 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_get_normalizedValue_m1950536199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		float L_1 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m2110007471(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		return (0.0f);
	}

IL_001c:
	{
		float L_3 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		float L_4 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		float L_5 = BoxSlider_get_value_m1655722908(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_6 = Mathf_InverseLerp_m4028041258(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_normalizedValue(System.Single)
extern "C"  void BoxSlider_set_normalizedValue_m1073637371 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_normalizedValue_m1073637371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		float L_1 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		float L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m44200011(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		BoxSlider_set_value_m2367170594(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_valueY()
extern "C"  float BoxSlider_get_valueY_m1180997838 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_get_valueY_m1180997838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = BoxSlider_get_wholeNumbers_m366103797(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = __this->get_m_ValueY_21();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_2 = bankers_roundf(L_1);
		return L_2;
	}

IL_0017:
	{
		float L_3 = __this->get_m_ValueY_21();
		return L_3;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_valueY(System.Single)
extern "C"  void BoxSlider_set_valueY_m3094914561 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		BoxSlider_SetY_m3783125856(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_normalizedValueY()
extern "C"  float BoxSlider_get_normalizedValueY_m266672788 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_get_normalizedValueY_m266672788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		float L_1 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m2110007471(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		return (0.0f);
	}

IL_001c:
	{
		float L_3 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		float L_4 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		float L_5 = BoxSlider_get_valueY_m1180997838(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_6 = Mathf_InverseLerp_m4028041258(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_normalizedValueY(System.Single)
extern "C"  void BoxSlider_set_normalizedValueY_m815891677 (BoxSlider_t2780260910 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_set_normalizedValueY_m815891677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		float L_1 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		float L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m44200011(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		BoxSlider_set_valueY_m3094914561(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::get_onValueChanged()
extern "C"  BoxSliderEvent_t2026169079 * BoxSlider_get_onValueChanged_m180314062 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		BoxSliderEvent_t2026169079 * L_0 = __this->get_m_OnValueChanged_22();
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::set_onValueChanged(UnityEngine.UI.BoxSlider/BoxSliderEvent)
extern "C"  void BoxSlider_set_onValueChanged_m3678258718 (BoxSlider_t2780260910 * __this, BoxSliderEvent_t2026169079 * ___value0, const RuntimeMethod* method)
{
	{
		BoxSliderEvent_t2026169079 * L_0 = ___value0;
		__this->set_m_OnValueChanged_22(L_0);
		return;
	}
}
// System.Single UnityEngine.UI.BoxSlider::get_stepSize()
extern "C"  float BoxSlider_get_stepSize_m240234921 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	float G_B3_0 = 0.0f;
	{
		bool L_0 = BoxSlider_get_wholeNumbers_m366103797(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_0028;
	}

IL_0015:
	{
		float L_1 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		float L_2 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)((float)((float)L_1-(float)L_2))*(float)(0.1f)));
	}

IL_0028:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::Rebuild(UnityEngine.UI.CanvasUpdate)
extern "C"  void BoxSlider_Rebuild_m1037772324 (BoxSlider_t2780260910 * __this, int32_t ___executing0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::LayoutComplete()
extern "C"  void BoxSlider_LayoutComplete_m1821827405 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::GraphicUpdateComplete()
extern "C"  void BoxSlider_GraphicUpdateComplete_m4031967707 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnEnable()
extern "C"  void BoxSlider_OnEnable_m1474905605 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		Selectable_OnEnable_m1471812096(__this, /*hidden argument*/NULL);
		BoxSlider_UpdateCachedReferences_m977902596(__this, /*hidden argument*/NULL);
		float L_0 = __this->get_m_Value_20();
		BoxSlider_Set_m3176380998(__this, L_0, (bool)0, /*hidden argument*/NULL);
		float L_1 = __this->get_m_ValueY_21();
		BoxSlider_SetY_m3865330729(__this, L_1, (bool)0, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m4195678704(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnDisable()
extern "C"  void BoxSlider_OnDisable_m4018138510 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		DrivenRectTransformTracker_t1306825633 * L_0 = __this->get_address_of_m_Tracker_26();
		DrivenRectTransformTracker_Clear_m1227380991(L_0, /*hidden argument*/NULL);
		Selectable_OnDisable_m2837870712(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::UpdateCachedReferences()
extern "C"  void BoxSlider_UpdateCachedReferences_m977902596 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_UpdateCachedReferences_m977902596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t1885177139 * L_0 = __this->get_m_HandleRect_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3672469481(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		RectTransform_t1885177139 * L_2 = __this->get_m_HandleRect_16();
		NullCheck(L_2);
		Transform_t3933397867 * L_3 = Component_get_transform_m2033240428(L_2, /*hidden argument*/NULL);
		__this->set_m_HandleTransform_23(L_3);
		Transform_t3933397867 * L_4 = __this->get_m_HandleTransform_23();
		NullCheck(L_4);
		Transform_t3933397867 * L_5 = Transform_get_parent_m901998732(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_5, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004d;
		}
	}
	{
		Transform_t3933397867 * L_7 = __this->get_m_HandleTransform_23();
		NullCheck(L_7);
		Transform_t3933397867 * L_8 = Transform_get_parent_m901998732(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		RectTransform_t1885177139 * L_9 = Component_GetComponent_TisRectTransform_t1885177139_m2102537755(L_8, /*hidden argument*/Component_GetComponent_TisRectTransform_t1885177139_m2102537755_RuntimeMethod_var);
		__this->set_m_HandleContainerRect_24(L_9);
	}

IL_004d:
	{
		goto IL_0059;
	}

IL_0052:
	{
		__this->set_m_HandleContainerRect_24((RectTransform_t1885177139 *)NULL);
	}

IL_0059:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::Set(System.Single)
extern "C"  void BoxSlider_Set_m1701966247 (BoxSlider_t2780260910 * __this, float ___input0, const RuntimeMethod* method)
{
	{
		float L_0 = ___input0;
		BoxSlider_Set_m3176380998(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::Set(System.Single,System.Boolean)
extern "C"  void BoxSlider_Set_m3176380998 (BoxSlider_t2780260910 * __this, float ___input0, bool ___sendCallback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_Set_m3176380998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___input0;
		float L_1 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		float L_2 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m934937530(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = BoxSlider_get_wholeNumbers_m366103797(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_6 = bankers_roundf(L_5);
		V_0 = L_6;
	}

IL_0025:
	{
		float L_7 = __this->get_m_Value_20();
		float L_8 = V_0;
		if ((!(((float)L_7) == ((float)L_8))))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0032:
	{
		float L_9 = V_0;
		__this->set_m_Value_20(L_9);
		BoxSlider_UpdateVisuals_m4195678704(__this, /*hidden argument*/NULL);
		bool L_10 = ___sendCallback1;
		if (!L_10)
		{
			goto IL_0057;
		}
	}
	{
		BoxSliderEvent_t2026169079 * L_11 = __this->get_m_OnValueChanged_22();
		float L_12 = V_0;
		float L_13 = BoxSlider_get_valueY_m1180997838(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		UnityEvent_2_Invoke_m2732613775(L_11, L_12, L_13, /*hidden argument*/UnityEvent_2_Invoke_m2732613775_RuntimeMethod_var);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::SetY(System.Single)
extern "C"  void BoxSlider_SetY_m3783125856 (BoxSlider_t2780260910 * __this, float ___input0, const RuntimeMethod* method)
{
	{
		float L_0 = ___input0;
		BoxSlider_SetY_m3865330729(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::SetY(System.Single,System.Boolean)
extern "C"  void BoxSlider_SetY_m3865330729 (BoxSlider_t2780260910 * __this, float ___input0, bool ___sendCallback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_SetY_m3865330729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___input0;
		float L_1 = BoxSlider_get_minValue_m43333998(__this, /*hidden argument*/NULL);
		float L_2 = BoxSlider_get_maxValue_m1548328961(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m934937530(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = BoxSlider_get_wholeNumbers_m366103797(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_6 = bankers_roundf(L_5);
		V_0 = L_6;
	}

IL_0025:
	{
		float L_7 = __this->get_m_ValueY_21();
		float L_8 = V_0;
		if ((!(((float)L_7) == ((float)L_8))))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0032:
	{
		float L_9 = V_0;
		__this->set_m_ValueY_21(L_9);
		BoxSlider_UpdateVisuals_m4195678704(__this, /*hidden argument*/NULL);
		bool L_10 = ___sendCallback1;
		if (!L_10)
		{
			goto IL_0057;
		}
	}
	{
		BoxSliderEvent_t2026169079 * L_11 = __this->get_m_OnValueChanged_22();
		float L_12 = BoxSlider_get_value_m1655722908(__this, /*hidden argument*/NULL);
		float L_13 = V_0;
		NullCheck(L_11);
		UnityEvent_2_Invoke_m2732613775(L_11, L_12, L_13, /*hidden argument*/UnityEvent_2_Invoke_m2732613775_RuntimeMethod_var);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnRectTransformDimensionsChange()
extern "C"  void BoxSlider_OnRectTransformDimensionsChange_m1038133235 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		UIBehaviour_OnRectTransformDimensionsChange_m1031470322(__this, /*hidden argument*/NULL);
		BoxSlider_UpdateVisuals_m4195678704(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::UpdateVisuals()
extern "C"  void BoxSlider_UpdateVisuals_m4195678704 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_UpdateVisuals_m4195678704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t59524482  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t59524482  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		DrivenRectTransformTracker_t1306825633 * L_0 = __this->get_address_of_m_Tracker_26();
		DrivenRectTransformTracker_Clear_m1227380991(L_0, /*hidden argument*/NULL);
		RectTransform_t1885177139 * L_1 = __this->get_m_HandleContainerRect_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0089;
		}
	}
	{
		DrivenRectTransformTracker_t1306825633 * L_3 = __this->get_address_of_m_Tracker_26();
		RectTransform_t1885177139 * L_4 = __this->get_m_HandleRect_16();
		DrivenRectTransformTracker_Add_m3357982639(L_3, __this, L_4, ((int32_t)3840), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector2_t59524482  L_5 = Vector2_get_zero_m3701231313(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector2_t59524482  L_6 = Vector2_get_one_m242331669(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = BoxSlider_get_normalizedValue_m1950536199(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = V_2;
		Vector2_set_Item_m4254885928((&V_1), 0, L_8, /*hidden argument*/NULL);
		float L_9 = V_2;
		Vector2_set_Item_m4254885928((&V_0), 0, L_9, /*hidden argument*/NULL);
		float L_10 = BoxSlider_get_normalizedValueY_m266672788(__this, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = V_2;
		Vector2_set_Item_m4254885928((&V_1), 1, L_11, /*hidden argument*/NULL);
		float L_12 = V_2;
		Vector2_set_Item_m4254885928((&V_0), 1, L_12, /*hidden argument*/NULL);
		RectTransform_t1885177139 * L_13 = __this->get_m_HandleRect_16();
		Vector2_t59524482  L_14 = V_0;
		NullCheck(L_13);
		RectTransform_set_anchorMin_m1050503853(L_13, L_14, /*hidden argument*/NULL);
		RectTransform_t1885177139 * L_15 = __this->get_m_HandleRect_16();
		Vector2_t59524482  L_16 = V_1;
		NullCheck(L_15);
		RectTransform_set_anchorMax_m2185008930(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
extern "C"  void BoxSlider_UpdateDrag_m2830268571 (BoxSlider_t2780260910 * __this, PointerEventData_t1563322019 * ___eventData0, Camera_t226495598 * ___cam1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_UpdateDrag_m2830268571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t1885177139 * V_0 = NULL;
	Rect_t1992046353  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t59524482  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t59524482  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t1992046353  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector2_t59524482  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t1992046353  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t59524482  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	Vector2_t59524482  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Rect_t1992046353  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector2_t59524482  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		RectTransform_t1885177139 * L_0 = __this->get_m_HandleContainerRect_24();
		V_0 = L_0;
		RectTransform_t1885177139 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_1, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00dd;
		}
	}
	{
		RectTransform_t1885177139 * L_3 = V_0;
		NullCheck(L_3);
		Rect_t1992046353  L_4 = RectTransform_get_rect_m562447867(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Vector2_t59524482  L_5 = Rect_get_size_m455598348((&V_1), /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Vector2_get_Item_m3603954497((&V_2), 0, /*hidden argument*/NULL);
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_00dd;
		}
	}
	{
		RectTransform_t1885177139 * L_7 = V_0;
		PointerEventData_t1563322019 * L_8 = ___eventData0;
		NullCheck(L_8);
		Vector2_t59524482  L_9 = PointerEventData_get_position_m4019811242(L_8, /*hidden argument*/NULL);
		Camera_t226495598 * L_10 = ___cam1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2874646166_il2cpp_TypeInfo_var);
		bool L_11 = RectTransformUtility_ScreenPointToLocalPointInRectangle_m3576743012(NULL /*static, unused*/, L_7, L_9, L_10, (&V_3), /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0049;
		}
	}
	{
		return;
	}

IL_0049:
	{
		Vector2_t59524482  L_12 = V_3;
		RectTransform_t1885177139 * L_13 = V_0;
		NullCheck(L_13);
		Rect_t1992046353  L_14 = RectTransform_get_rect_m562447867(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		Vector2_t59524482  L_15 = Rect_get_position_m2983583565((&V_4), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector2_t59524482  L_16 = Vector2_op_Subtraction_m49443056(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		Vector2_t59524482  L_17 = V_3;
		Vector2_t59524482  L_18 = __this->get_m_Offset_25();
		Vector2_t59524482  L_19 = Vector2_op_Subtraction_m49443056(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		float L_20 = Vector2_get_Item_m3603954497((&V_6), 0, /*hidden argument*/NULL);
		RectTransform_t1885177139 * L_21 = V_0;
		NullCheck(L_21);
		Rect_t1992046353  L_22 = RectTransform_get_rect_m562447867(L_21, /*hidden argument*/NULL);
		V_7 = L_22;
		Vector2_t59524482  L_23 = Rect_get_size_m455598348((&V_7), /*hidden argument*/NULL);
		V_8 = L_23;
		float L_24 = Vector2_get_Item_m3603954497((&V_8), 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t413863475_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Clamp01_m3153283104(NULL /*static, unused*/, ((float)((float)L_20/(float)L_24)), /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = V_5;
		BoxSlider_set_normalizedValue_m1073637371(__this, L_26, /*hidden argument*/NULL);
		Vector2_t59524482  L_27 = V_3;
		Vector2_t59524482  L_28 = __this->get_m_Offset_25();
		Vector2_t59524482  L_29 = Vector2_op_Subtraction_m49443056(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		V_10 = L_29;
		float L_30 = Vector2_get_Item_m3603954497((&V_10), 1, /*hidden argument*/NULL);
		RectTransform_t1885177139 * L_31 = V_0;
		NullCheck(L_31);
		Rect_t1992046353  L_32 = RectTransform_get_rect_m562447867(L_31, /*hidden argument*/NULL);
		V_11 = L_32;
		Vector2_t59524482  L_33 = Rect_get_size_m455598348((&V_11), /*hidden argument*/NULL);
		V_12 = L_33;
		float L_34 = Vector2_get_Item_m3603954497((&V_12), 1, /*hidden argument*/NULL);
		float L_35 = Mathf_Clamp01_m3153283104(NULL /*static, unused*/, ((float)((float)L_30/(float)L_34)), /*hidden argument*/NULL);
		V_9 = L_35;
		float L_36 = V_9;
		BoxSlider_set_normalizedValueY_m815891677(__this, L_36, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.BoxSlider::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  bool BoxSlider_MayDrag_m1768002497 (BoxSlider_t2780260910 * __this, PointerEventData_t1563322019 * ___eventData0, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean UnityEngine.UI.Selectable::IsInteractable() */, __this);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		PointerEventData_t1563322019 * L_2 = ___eventData0;
		NullCheck(L_2);
		int32_t L_3 = PointerEventData_get_button_m2484257731(L_2, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BoxSlider_OnPointerDown_m730960263 (BoxSlider_t2780260910 * __this, PointerEventData_t1563322019 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSlider_OnPointerDown_m730960263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t59524482  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PointerEventData_t1563322019 * L_0 = ___eventData0;
		bool L_1 = BoxSlider_MayDrag_m1768002497(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		PointerEventData_t1563322019 * L_2 = ___eventData0;
		Selectable_OnPointerDown_m3466118125(__this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t59524482_il2cpp_TypeInfo_var);
		Vector2_t59524482  L_3 = Vector2_get_zero_m3701231313(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Offset_25(L_3);
		RectTransform_t1885177139 * L_4 = __this->get_m_HandleContainerRect_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1008057425_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m1545791448(NULL /*static, unused*/, L_4, (Object_t1008057425 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_008d;
		}
	}
	{
		RectTransform_t1885177139 * L_6 = __this->get_m_HandleRect_16();
		PointerEventData_t1563322019 * L_7 = ___eventData0;
		NullCheck(L_7);
		Vector2_t59524482  L_8 = PointerEventData_get_position_m4019811242(L_7, /*hidden argument*/NULL);
		PointerEventData_t1563322019 * L_9 = ___eventData0;
		NullCheck(L_9);
		Camera_t226495598 * L_10 = PointerEventData_get_enterEventCamera_m4139929799(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2874646166_il2cpp_TypeInfo_var);
		bool L_11 = RectTransformUtility_RectangleContainsScreenPoint_m326099737(NULL /*static, unused*/, L_6, L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008d;
		}
	}
	{
		RectTransform_t1885177139 * L_12 = __this->get_m_HandleRect_16();
		PointerEventData_t1563322019 * L_13 = ___eventData0;
		NullCheck(L_13);
		Vector2_t59524482  L_14 = PointerEventData_get_position_m4019811242(L_13, /*hidden argument*/NULL);
		PointerEventData_t1563322019 * L_15 = ___eventData0;
		NullCheck(L_15);
		Camera_t226495598 * L_16 = PointerEventData_get_pressEventCamera_m2916200056(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2874646166_il2cpp_TypeInfo_var);
		bool L_17 = RectTransformUtility_ScreenPointToLocalPointInRectangle_m3576743012(NULL /*static, unused*/, L_12, L_14, L_16, (&V_0), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0071;
		}
	}
	{
		Vector2_t59524482  L_18 = V_0;
		__this->set_m_Offset_25(L_18);
	}

IL_0071:
	{
		Vector2_t59524482 * L_19 = __this->get_address_of_m_Offset_25();
		Vector2_t59524482 * L_20 = __this->get_address_of_m_Offset_25();
		float L_21 = L_20->get_y_1();
		L_19->set_y_1(((-L_21)));
		goto IL_009a;
	}

IL_008d:
	{
		PointerEventData_t1563322019 * L_22 = ___eventData0;
		PointerEventData_t1563322019 * L_23 = ___eventData0;
		NullCheck(L_23);
		Camera_t226495598 * L_24 = PointerEventData_get_pressEventCamera_m2916200056(L_23, /*hidden argument*/NULL);
		BoxSlider_UpdateDrag_m2830268571(__this, L_22, L_24, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BoxSlider_OnDrag_m2328603067 (BoxSlider_t2780260910 * __this, PointerEventData_t1563322019 * ___eventData0, const RuntimeMethod* method)
{
	{
		PointerEventData_t1563322019 * L_0 = ___eventData0;
		bool L_1 = BoxSlider_MayDrag_m1768002497(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		PointerEventData_t1563322019 * L_2 = ___eventData0;
		PointerEventData_t1563322019 * L_3 = ___eventData0;
		NullCheck(L_3);
		Camera_t226495598 * L_4 = PointerEventData_get_pressEventCamera_m2916200056(L_3, /*hidden argument*/NULL);
		BoxSlider_UpdateDrag_m2830268571(__this, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BoxSlider::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BoxSlider_OnInitializePotentialDrag_m4154894609 (BoxSlider_t2780260910 * __this, PointerEventData_t1563322019 * ___eventData0, const RuntimeMethod* method)
{
	{
		PointerEventData_t1563322019 * L_0 = ___eventData0;
		NullCheck(L_0);
		PointerEventData_set_useDragThreshold_m4047126439(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.UI.BoxSlider::UnityEngine.UI.ICanvasElement.get_transform()
extern "C"  Transform_t3933397867 * BoxSlider_UnityEngine_UI_ICanvasElement_get_transform_m2033855609 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3933397867 * L_0 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.UI.BoxSlider::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern "C"  bool BoxSlider_UnityEngine_UI_ICanvasElement_IsDestroyed_m3618900358 (BoxSlider_t2780260910 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = UIBehaviour_IsDestroyed_m2514623896(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.UI.BoxSlider/BoxSliderEvent::.ctor()
extern "C"  void BoxSliderEvent__ctor_m2331793288 (BoxSliderEvent_t2026169079 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BoxSliderEvent__ctor_m2331793288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_2__ctor_m1813845425(__this, /*hidden argument*/UnityEvent_2__ctor_m1813845425_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAmbient::.ctor()
extern "C"  void UnityARAmbient__ctor_m3057611808 (UnityARAmbient_t3389999931 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAmbient::Start()
extern "C"  void UnityARAmbient_Start_m4140057879 (UnityARAmbient_t3389999931 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAmbient_Start_m4140057879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Light_t2513018095 * L_0 = Component_GetComponent_TisLight_t2513018095_m2996527359(__this, /*hidden argument*/Component_GetComponent_TisLight_t2513018095_m2996527359_RuntimeMethod_var);
		__this->set_l_2(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t3869411053_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t3869411053 * L_1 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m2292413636(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Session_3(L_1);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAmbient::Update()
extern "C"  void UnityARAmbient_Update_m1624235607 (UnityARAmbient_t3389999931 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		UnityARSessionNativeInterface_t3869411053 * L_0 = __this->get_m_Session_3();
		NullCheck(L_0);
		float L_1 = UnityARSessionNativeInterface_GetARAmbientIntensity_m2324758640(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Light_t2513018095 * L_2 = __this->get_l_2();
		float L_3 = V_0;
		NullCheck(L_2);
		Light_set_intensity_m3261273135(L_2, ((float)((float)L_3/(float)(1000.0f))), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
