﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"




extern const Il2CppType RuntimeObject_0_0_0;
extern const Il2CppType Int32_t3425510919_0_0_0;
extern const Il2CppType Char_t539237919_0_0_0;
extern const Il2CppType Int64_t2252457107_0_0_0;
extern const Il2CppType UInt32_t3933237433_0_0_0;
extern const Il2CppType UInt64_t1498391637_0_0_0;
extern const Il2CppType Byte_t3065488403_0_0_0;
extern const Il2CppType SByte_t1519635365_0_0_0;
extern const Il2CppType Int16_t2196066360_0_0_0;
extern const Il2CppType UInt16_t2173916929_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IConvertible_t2722588824_0_0_0;
extern const Il2CppType IComparable_t1036281117_0_0_0;
extern const Il2CppType IEnumerable_t2201844525_0_0_0;
extern const Il2CppType ICloneable_t2975100769_0_0_0;
extern const Il2CppType IComparable_1_t2296033782_0_0_0;
extern const Il2CppType IEquatable_1_t3464885664_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IReflect_t901526961_0_0_0;
extern const Il2CppType _Type_t3150095453_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t490813527_0_0_0;
extern const Il2CppType _MemberInfo_t2295893705_0_0_0;
extern const Il2CppType Double_t1029397067_0_0_0;
extern const Il2CppType Single_t2847614712_0_0_0;
extern const Il2CppType Decimal_t2663171440_0_0_0;
extern const Il2CppType Boolean_t362855854_0_0_0;
extern const Il2CppType Delegate_t69892740_0_0_0;
extern const Il2CppType ISerializable_t1411645196_0_0_0;
extern const Il2CppType ParameterInfo_t3577984089_0_0_0;
extern const Il2CppType _ParameterInfo_t3423239624_0_0_0;
extern const Il2CppType ParameterModifier_t3485655876_0_0_0;
extern const Il2CppType EventInfo_t_0_0_0;
extern const Il2CppType _EventInfo_t1619462957_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType _FieldInfo_t2155012913_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType _MethodInfo_t2553989953_0_0_0;
extern const Il2CppType MethodBase_t1640104494_0_0_0;
extern const Il2CppType _MethodBase_t3674854218_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType _PropertyInfo_t2466204053_0_0_0;
extern const Il2CppType ConstructorInfo_t1214230744_0_0_0;
extern const Il2CppType _ConstructorInfo_t3104092857_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType TableRange_t3005830667_0_0_0;
extern const Il2CppType TailoringInfo_t2103418023_0_0_0;
extern const Il2CppType KeyValuePair_2_t3968584898_0_0_0;
extern const Il2CppType Link_t4043807316_0_0_0;
extern const Il2CppType DictionaryEntry_t3951334252_0_0_0;
extern const Il2CppType KeyValuePair_2_t2032264690_0_0_0;
extern const Il2CppType Contraction_t3464654688_0_0_0;
extern const Il2CppType Level2Map_t782964927_0_0_0;
extern const Il2CppType BigInteger_t1286850636_0_0_0;
extern const Il2CppType KeySizes_t2873621649_0_0_0;
extern const Il2CppType KeyValuePair_2_t665585682_0_0_0;
extern const Il2CppType Slot_t4047215697_0_0_0;
extern const Il2CppType Slot_t3212906767_0_0_0;
extern const Il2CppType StackFrame_t2301538707_0_0_0;
extern const Il2CppType Calendar_t4189858442_0_0_0;
extern const Il2CppType ModuleBuilder_t2846916743_0_0_0;
extern const Il2CppType _ModuleBuilder_t1727831225_0_0_0;
extern const Il2CppType Module_t4287715521_0_0_0;
extern const Il2CppType _Module_t3465402545_0_0_0;
extern const Il2CppType ParameterBuilder_t2375480068_0_0_0;
extern const Il2CppType _ParameterBuilder_t2449263151_0_0_0;
extern const Il2CppType TypeU5BU5D_t1484232934_0_0_0;
extern const Il2CppType RuntimeArray_0_0_0;
extern const Il2CppType ICollection_t1426578131_0_0_0;
extern const Il2CppType IList_t4121428329_0_0_0;
extern const Il2CppType IList_1_t575872299_0_0_0;
extern const Il2CppType ICollection_1_t2387962414_0_0_0;
extern const Il2CppType IEnumerable_1_t445450583_0_0_0;
extern const Il2CppType IList_1_t2458661789_0_0_0;
extern const Il2CppType ICollection_1_t4270751904_0_0_0;
extern const Il2CppType IEnumerable_1_t2328240073_0_0_0;
extern const Il2CppType IList_1_t412262985_0_0_0;
extern const Il2CppType ICollection_1_t2224353100_0_0_0;
extern const Il2CppType IEnumerable_1_t281841269_0_0_0;
extern const Il2CppType IList_1_t2505642904_0_0_0;
extern const Il2CppType ICollection_1_t22765723_0_0_0;
extern const Il2CppType IEnumerable_1_t2375221188_0_0_0;
extern const Il2CppType IList_1_t2047948355_0_0_0;
extern const Il2CppType ICollection_1_t3860038470_0_0_0;
extern const Il2CppType IEnumerable_1_t1917526639_0_0_0;
extern const Il2CppType IList_1_t3853028533_0_0_0;
extern const Il2CppType ICollection_1_t1370151352_0_0_0;
extern const Il2CppType IEnumerable_1_t3722606817_0_0_0;
extern const Il2CppType IList_1_t1679646531_0_0_0;
extern const Il2CppType ICollection_1_t3491736646_0_0_0;
extern const Il2CppType IEnumerable_1_t1549224815_0_0_0;
extern const Il2CppType ILTokenInfo_t4134893432_0_0_0;
extern const Il2CppType LabelData_t1800135739_0_0_0;
extern const Il2CppType LabelFixup_t703658549_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t451279079_0_0_0;
extern const Il2CppType TypeBuilder_t1669012508_0_0_0;
extern const Il2CppType _TypeBuilder_t4212529197_0_0_0;
extern const Il2CppType MethodBuilder_t1092218305_0_0_0;
extern const Il2CppType _MethodBuilder_t3510139682_0_0_0;
extern const Il2CppType ConstructorBuilder_t33025729_0_0_0;
extern const Il2CppType _ConstructorBuilder_t1274505923_0_0_0;
extern const Il2CppType PropertyBuilder_t1720155075_0_0_0;
extern const Il2CppType _PropertyBuilder_t3760533436_0_0_0;
extern const Il2CppType FieldBuilder_t1478132776_0_0_0;
extern const Il2CppType _FieldBuilder_t698479335_0_0_0;
extern const Il2CppType CustomAttributeTypedArgument_t3013856313_0_0_0;
extern const Il2CppType CustomAttributeNamedArgument_t1224813713_0_0_0;
extern const Il2CppType CustomAttributeData_t2607898573_0_0_0;
extern const Il2CppType ResourceInfo_t3677787790_0_0_0;
extern const Il2CppType ResourceCacheItem_t567161473_0_0_0;
extern const Il2CppType IContextProperty_t402073787_0_0_0;
extern const Il2CppType Header_t1099438005_0_0_0;
extern const Il2CppType ITrackingHandler_t127120994_0_0_0;
extern const Il2CppType IContextAttribute_t3616836717_0_0_0;
extern const Il2CppType DateTime_t1819153659_0_0_0;
extern const Il2CppType TimeSpan_t4158060032_0_0_0;
extern const Il2CppType TypeTag_t1792597276_0_0_0;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType StrongName_t3725927940_0_0_0;
extern const Il2CppType IBuiltInEvidence_t3225111825_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t4063359944_0_0_0;
extern const Il2CppType DateTimeOffset_t2489358039_0_0_0;
extern const Il2CppType Guid_t_0_0_0;
extern const Il2CppType Version_t3431102195_0_0_0;
extern const Il2CppType KeyValuePair_2_t905929833_0_0_0;
extern const Il2CppType KeyValuePair_2_t3264576921_0_0_0;
extern const Il2CppType X509Certificate_t1886472155_0_0_0;
extern const Il2CppType IDeserializationCallback_t1045582565_0_0_0;
extern const Il2CppType X509ChainStatus_t3209745328_0_0_0;
extern const Il2CppType Capture_t545002712_0_0_0;
extern const Il2CppType Group_t1246193761_0_0_0;
extern const Il2CppType Mark_t1421951811_0_0_0;
extern const Il2CppType UriScheme_t1685060244_0_0_0;
extern const Il2CppType BigInteger_t1286850637_0_0_0;
extern const Il2CppType ByteU5BU5D_t3548078658_0_0_0;
extern const Il2CppType IList_1_t327655935_0_0_0;
extern const Il2CppType ICollection_1_t2139746050_0_0_0;
extern const Il2CppType IEnumerable_1_t197234219_0_0_0;
extern const Il2CppType ClientCertificateType_t1149805517_0_0_0;
extern const Il2CppType Link_t655338529_0_0_0;
extern const Il2CppType List_1_t2975039247_0_0_0;
extern const Il2CppType DispatcherKey_t3620541735_0_0_0;
extern const Il2CppType Dispatcher_t55733337_0_0_0;
extern const Il2CppType KeyValuePair_2_t2849605124_0_0_0;
extern const Il2CppType Object_t1008057425_0_0_0;
extern const Il2CppType Camera_t226495598_0_0_0;
extern const Il2CppType Behaviour_t2200997390_0_0_0;
extern const Il2CppType Component_t531478471_0_0_0;
extern const Il2CppType Display_t4159198643_0_0_0;
extern const Il2CppType Keyframe_t643552408_0_0_0;
extern const Il2CppType Vector3_t596762001_0_0_0;
extern const Il2CppType Vector4_t1376926224_0_0_0;
extern const Il2CppType Vector2_t59524482_0_0_0;
extern const Il2CppType Color32_t2499566028_0_0_0;
extern const Il2CppType Playable_t1112762172_0_0_0;
extern const Il2CppType PlayableOutput_t1654971666_0_0_0;
extern const Il2CppType Scene_t1614351645_0_0_0;
extern const Il2CppType LoadSceneMode_t2556746618_0_0_0;
extern const Il2CppType SpriteAtlas_t1553410611_0_0_0;
extern const Il2CppType Particle_t1268635397_0_0_0;
extern const Il2CppType ContactPoint_t3149140470_0_0_0;
extern const Il2CppType RaycastHit_t1273336641_0_0_0;
extern const Il2CppType Rigidbody2D_t2029812455_0_0_0;
extern const Il2CppType AudioClipPlayable_t1384420439_0_0_0;
extern const Il2CppType AudioMixerPlayable_t3880288074_0_0_0;
extern const Il2CppType AnimationClipPlayable_t3824769629_0_0_0;
extern const Il2CppType AnimationLayerMixerPlayable_t3575213449_0_0_0;
extern const Il2CppType AnimationMixerPlayable_t4293775112_0_0_0;
extern const Il2CppType AnimationOffsetPlayable_t1345717778_0_0_0;
extern const Il2CppType AnimatorControllerPlayable_t3438332640_0_0_0;
extern const Il2CppType AnimatorClipInfo_t183054942_0_0_0;
extern const Il2CppType AnimatorControllerParameter_t3164458347_0_0_0;
extern const Il2CppType UIVertex_t2672378834_0_0_0;
extern const Il2CppType UICharInfo_t2367319176_0_0_0;
extern const Il2CppType UILineInfo_t248963365_0_0_0;
extern const Il2CppType Font_t2699442136_0_0_0;
extern const Il2CppType GUILayoutOption_t721761240_0_0_0;
extern const Il2CppType GUILayoutEntry_t240640878_0_0_0;
extern const Il2CppType LayoutCache_t1385791520_0_0_0;
extern const Il2CppType KeyValuePair_2_t2171644578_0_0_0;
extern const Il2CppType KeyValuePair_2_t3434924395_0_0_0;
extern const Il2CppType GUIStyle_t2932200036_0_0_0;
extern const Il2CppType KeyValuePair_2_t1538953807_0_0_0;
extern const Il2CppType Exception_t82373287_0_0_0;
extern const Il2CppType UserProfile_t771069284_0_0_0;
extern const Il2CppType IUserProfile_t1988237155_0_0_0;
extern const Il2CppType AchievementDescription_t433697264_0_0_0;
extern const Il2CppType IAchievementDescription_t1962024065_0_0_0;
extern const Il2CppType GcLeaderboard_t374162520_0_0_0;
extern const Il2CppType IAchievementDescriptionU5BU5D_t857867356_0_0_0;
extern const Il2CppType IAchievementU5BU5D_t2204500354_0_0_0;
extern const Il2CppType IAchievement_t1434617299_0_0_0;
extern const Il2CppType GcAchievementData_t1098514478_0_0_0;
extern const Il2CppType Achievement_t1914343199_0_0_0;
extern const Il2CppType IScoreU5BU5D_t754807597_0_0_0;
extern const Il2CppType IScore_t2259084100_0_0_0;
extern const Il2CppType GcScoreData_t322958630_0_0_0;
extern const Il2CppType Score_t1824312173_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t1589946290_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t2877457562_0_0_0;
extern const Il2CppType Attribute_t1842279184_0_0_0;
extern const Il2CppType _Attribute_t3546903852_0_0_0;
extern const Il2CppType ExecuteInEditMode_t3420945569_0_0_0;
extern const Il2CppType RequireComponent_t4227770712_0_0_0;
extern const Il2CppType HitInfo_t2832651489_0_0_0;
extern const Il2CppType PersistentCall_t709146344_0_0_0;
extern const Il2CppType BaseInvokableCall_t3830733407_0_0_0;
extern const Il2CppType WorkRequest_t886435312_0_0_0;
extern const Il2CppType PlayableBinding_t1499311357_0_0_0;
extern const Il2CppType MessageTypeSubscribers_t2879909665_0_0_0;
extern const Il2CppType MessageEventArgs_t3627585352_0_0_0;
extern const Il2CppType WeakReference_t245340424_0_0_0;
extern const Il2CppType KeyValuePair_2_t3430616441_0_0_0;
extern const Il2CppType KeyValuePair_2_t3553445162_0_0_0;
extern const Il2CppType BaseInputModule_t309635854_0_0_0;
extern const Il2CppType RaycastResult_t1715322097_0_0_0;
extern const Il2CppType IDeselectHandler_t3843174961_0_0_0;
extern const Il2CppType IEventSystemHandler_t558645447_0_0_0;
extern const Il2CppType List_1_t3596965026_0_0_0;
extern const Il2CppType List_1_t3160831282_0_0_0;
extern const Il2CppType List_1_t3569798050_0_0_0;
extern const Il2CppType ISelectHandler_t2734039658_0_0_0;
extern const Il2CppType BaseRaycaster_t3590231488_0_0_0;
extern const Il2CppType Entry_t3975632235_0_0_0;
extern const Il2CppType BaseEventData_t341072110_0_0_0;
extern const Il2CppType IPointerEnterHandler_t2566364723_0_0_0;
extern const Il2CppType IPointerExitHandler_t1566539265_0_0_0;
extern const Il2CppType IPointerDownHandler_t1810984543_0_0_0;
extern const Il2CppType IPointerUpHandler_t2617963051_0_0_0;
extern const Il2CppType IPointerClickHandler_t1471500927_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t2875812772_0_0_0;
extern const Il2CppType IBeginDragHandler_t737688467_0_0_0;
extern const Il2CppType IDragHandler_t1361425836_0_0_0;
extern const Il2CppType IEndDragHandler_t492564000_0_0_0;
extern const Il2CppType IDropHandler_t4181017068_0_0_0;
extern const Il2CppType IScrollHandler_t2087319098_0_0_0;
extern const Il2CppType IUpdateSelectedHandler_t1291562379_0_0_0;
extern const Il2CppType IMoveHandler_t4033052693_0_0_0;
extern const Il2CppType ISubmitHandler_t234818760_0_0_0;
extern const Il2CppType ICancelHandler_t516662923_0_0_0;
extern const Il2CppType Transform_t3933397867_0_0_0;
extern const Il2CppType GameObject_t3649338848_0_0_0;
extern const Il2CppType BaseInput_t3666734484_0_0_0;
extern const Il2CppType UIBehaviour_t2441083007_0_0_0;
extern const Il2CppType MonoBehaviour_t1096588306_0_0_0;
extern const Il2CppType PointerEventData_t1563322019_0_0_0;
extern const Il2CppType KeyValuePair_2_t3612454894_0_0_0;
extern const Il2CppType ButtonState_t4214419048_0_0_0;
extern const Il2CppType RaycastHit2D_t2323693239_0_0_0;
extern const Il2CppType Color_t320819310_0_0_0;
extern const Il2CppType ICanvasElement_t803896971_0_0_0;
extern const Il2CppType ColorBlock_t625039033_0_0_0;
extern const Il2CppType OptionData_t445311210_0_0_0;
extern const Il2CppType DropdownItem_t953411360_0_0_0;
extern const Il2CppType FloatTween_t1459678735_0_0_0;
extern const Il2CppType Sprite_t1309550511_0_0_0;
extern const Il2CppType Canvas_t3680465181_0_0_0;
extern const Il2CppType List_1_t2423817464_0_0_0;
extern const Il2CppType HashSet_1_t3994364505_0_0_0;
extern const Il2CppType Text_t3069741234_0_0_0;
extern const Il2CppType KeyValuePair_2_t3253400431_0_0_0;
extern const Il2CppType ColorTween_t1347433457_0_0_0;
extern const Il2CppType Graphic_t1406460313_0_0_0;
extern const Il2CppType IndexedSet_1_t2830149342_0_0_0;
extern const Il2CppType KeyValuePair_2_t2983860219_0_0_0;
extern const Il2CppType KeyValuePair_2_t2618575160_0_0_0;
extern const Il2CppType KeyValuePair_2_t1278652478_0_0_0;
extern const Il2CppType Type_t125599537_0_0_0;
extern const Il2CppType FillMethod_t46703468_0_0_0;
extern const Il2CppType ContentType_t2687439383_0_0_0;
extern const Il2CppType LineType_t4134456689_0_0_0;
extern const Il2CppType InputType_t3397888567_0_0_0;
extern const Il2CppType TouchScreenKeyboardType_t491524606_0_0_0;
extern const Il2CppType CharacterValidation_t2578987709_0_0_0;
extern const Il2CppType Mask_t3495621644_0_0_0;
extern const Il2CppType List_1_t2238973927_0_0_0;
extern const Il2CppType RectMask2D_t2103047313_0_0_0;
extern const Il2CppType List_1_t846399596_0_0_0;
extern const Il2CppType Navigation_t2887492880_0_0_0;
extern const Il2CppType IClippable_t2263516411_0_0_0;
extern const Il2CppType Direction_t1877520305_0_0_0;
extern const Il2CppType Selectable_t3890617260_0_0_0;
extern const Il2CppType Transition_t327687789_0_0_0;
extern const Il2CppType SpriteState_t758977253_0_0_0;
extern const Il2CppType CanvasGroup_t3075965541_0_0_0;
extern const Il2CppType Direction_t1918642359_0_0_0;
extern const Il2CppType MatEntry_t4139921362_0_0_0;
extern const Il2CppType Toggle_t1116311230_0_0_0;
extern const Il2CppType IClipper_t1468079658_0_0_0;
extern const Il2CppType KeyValuePair_2_t3227114307_0_0_0;
extern const Il2CppType AspectMode_t3157395208_0_0_0;
extern const Il2CppType FitMode_t760495087_0_0_0;
extern const Il2CppType RectTransform_t1885177139_0_0_0;
extern const Il2CppType LayoutRebuilder_t1578890533_0_0_0;
extern const Il2CppType ILayoutElement_t2907498299_0_0_0;
extern const Il2CppType List_1_t3635081580_0_0_0;
extern const Il2CppType List_1_t1242918311_0_0_0;
extern const Il2CppType List_1_t3097844061_0_0_0;
extern const Il2CppType List_1_t120278507_0_0_0;
extern const Il2CppType List_1_t2168863202_0_0_0;
extern const Il2CppType List_1_t1415731117_0_0_0;
extern const Il2CppType ARHitTestResult_t1803723679_0_0_0;
extern const Il2CppType ARPlaneAnchorGameObject_t3146329710_0_0_0;
extern const Il2CppType ARHitTestResultType_t270086150_0_0_0;
extern const Il2CppType UnityARSessionRunOption_t2337414342_0_0_0;
extern const Il2CppType UnityARAlignment_t1226993326_0_0_0;
extern const Il2CppType UnityARPlaneDetection_t2335225179_0_0_0;
extern const Il2CppType KeyValuePair_2_t1753083481_0_0_0;
extern const Il2CppType ParticleSystem_t1777616458_0_0_0;
extern const Il2CppType IEnumerable_1_t1371926219_gp_0_0_0_0;
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m4218696652_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3424865288_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1174453676_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1174453676_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m904955079_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m224785917_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m224785917_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3326076588_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2780446043_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2780446043_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2417833379_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1373481026_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1373481026_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m1201249205_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3696811223_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m3347095892_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m3347095892_gp_1_0_0_0;
extern const Il2CppType Array_compare_m286771913_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m1529647689_gp_0_0_0_0;
extern const Il2CppType Array_Resize_m2542074053_gp_0_0_0_0;
extern const Il2CppType Array_TrueForAll_m2493260605_gp_0_0_0_0;
extern const Il2CppType Array_ForEach_m3467030689_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1694469864_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1694469864_gp_1_0_0_0;
extern const Il2CppType Array_FindLastIndex_m4135284164_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m1527152247_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m4002081672_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m17293001_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m936277329_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m178503657_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m557100490_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3954992392_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m1167857953_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3918896244_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m758192846_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m1794394959_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m315707683_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m2029688934_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m3148242585_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m1969981718_gp_0_0_0_0;
extern const Il2CppType Array_FindAll_m1088630835_gp_0_0_0_0;
extern const Il2CppType Array_Exists_m2063596975_gp_0_0_0_0;
extern const Il2CppType Array_AsReadOnly_m2584618994_gp_0_0_0_0;
extern const Il2CppType Array_Find_m2415417361_gp_0_0_0_0;
extern const Il2CppType Array_FindLast_m3619283605_gp_0_0_0_0;
extern const Il2CppType InternalEnumerator_1_t15510975_gp_0_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t5340530_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3976382471_gp_0_0_0_0;
extern const Il2CppType IList_1_t1284029119_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t3388121722_gp_0_0_0_0;
extern const Il2CppType Nullable_1_t739790191_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t2195447696_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t989032782_gp_0_0_0_0;
extern const Il2CppType GenericComparer_1_t3293245800_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t681358595_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t681358595_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t1779626263_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m37576101_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1842228008_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1842228008_gp_1_0_0_0;
extern const Il2CppType Enumerator_t3839420370_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3839420370_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t2283510011_0_0_0;
extern const Il2CppType ValueCollection_t2793800625_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2793800625_gp_1_0_0_0;
extern const Il2CppType Enumerator_t760914474_gp_0_0_0_0;
extern const Il2CppType Enumerator_t760914474_gp_1_0_0_0;
extern const Il2CppType EqualityComparer_1_t1195631860_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t706283517_gp_0_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t3304566894_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t916714459_0_0_0;
extern const Il2CppType IDictionary_2_t1884790490_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t1884790490_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t1574552141_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1574552141_gp_1_0_0_0;
extern const Il2CppType List_1_t295223622_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2074507559_gp_0_0_0_0;
extern const Il2CppType Collection_1_t3142256919_gp_0_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t914928874_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1404835840_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1404835840_gp_1_0_0_0;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1727439354_gp_0_0_0_0;
extern const Il2CppType Queue_1_t1196217950_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2727315752_gp_0_0_0_0;
extern const Il2CppType Stack_1_t696983570_gp_0_0_0_0;
extern const Il2CppType Enumerator_t812808600_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t1291849714_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1535822487_gp_0_0_0_0;
extern const Il2CppType PrimeHelper_t3642281417_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m2221973312_gp_0_0_0_0;
extern const Il2CppType Enumerable_ToList_m1954003453_gp_0_0_0_0;
extern const Il2CppType Enumerable_Where_m3550166720_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0;
extern const Il2CppType List_1_t467069803_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator6_t4041278014_gp_0_0_0_0;
extern const Il2CppType List_1_t3967229796_0_0_0;
extern const Il2CppType Component_GetComponentInChildren_m3128998196_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m1790039706_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m1997911980_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m159208863_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m4223062209_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m1689563606_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m1885645319_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m1546679232_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m1602522827_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m2981955009_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentInChildren_m972597910_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponents_m3123327788_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m1146767846_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInParent_m2891368970_gp_0_0_0_0;
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m4121023089_gp_0_0_0_0;
extern const Il2CppType Mesh_SafeLength_m3772110265_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m4080854334_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m2700408318_gp_0_0_0_0;
extern const Il2CppType Mesh_SetUvsImpl_m1338450254_gp_0_0_0_0;
extern const Il2CppType Object_Instantiate_m3278571309_gp_0_0_0_0;
extern const Il2CppType Object_FindObjectsOfType_m1189854602_gp_0_0_0_0;
extern const Il2CppType Playable_IsPlayableOfType_m2446752783_gp_0_0_0_0;
extern const Il2CppType PlayableOutput_IsPlayableOutputOfType_m3414177984_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t1043888743_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t2762766517_0_0_0;
extern const Il2CppType InvokableCall_2_t4283310926_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t4283310926_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t846580519_0_0_0;
extern const Il2CppType InvokableCall_3_t1407968758_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t1407968758_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t1407968758_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t1541563382_0_0_0;
extern const Il2CppType InvokableCall_4_t3522370476_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3522370476_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3522370476_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3522370476_gp_3_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t2169007495_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t2170633027_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t2498946136_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t2498946136_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3426698314_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t3426698314_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3426698314_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1332048011_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t1332048011_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t1332048011_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1332048011_gp_3_0_0_0;
extern const Il2CppType ExecuteEvents_Execute_m3317274811_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m3880156326_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventList_m3038861451_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_CanHandleEvent_m151454768_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventHandler_m1494251347_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t726558302_gp_0_0_0_0;
extern const Il2CppType Dropdown_GetOrAddComponent_m3300878866_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m3021317927_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t2282855104_gp_0_0_0_0;
extern const Il2CppType ListPool_1_t681572707_gp_0_0_0_0;
extern const Il2CppType List_1_t72557439_0_0_0;
extern const Il2CppType ObjectPool_1_t3954034076_gp_0_0_0_0;
extern const Il2CppType AnimationPlayableOutput_t2577072500_0_0_0;
extern const Il2CppType DefaultExecutionOrder_t3670034221_0_0_0;
extern const Il2CppType AudioPlayableOutput_t3404408964_0_0_0;
extern const Il2CppType PlayerConnection_t3806501584_0_0_0;
extern const Il2CppType ScriptPlayableOutput_t3124905912_0_0_0;
extern const Il2CppType GUILayer_t2573986518_0_0_0;
extern const Il2CppType EventSystem_t487304461_0_0_0;
extern const Il2CppType AxisEventData_t3116767805_0_0_0;
extern const Il2CppType SpriteRenderer_t621304888_0_0_0;
extern const Il2CppType Image_t126372159_0_0_0;
extern const Il2CppType Button_t467150838_0_0_0;
extern const Il2CppType RawImage_t3037409423_0_0_0;
extern const Il2CppType Slider_t301630380_0_0_0;
extern const Il2CppType Scrollbar_t1086108786_0_0_0;
extern const Il2CppType InputField_t3161868630_0_0_0;
extern const Il2CppType ScrollRect_t371228710_0_0_0;
extern const Il2CppType Dropdown_t1637122921_0_0_0;
extern const Il2CppType GraphicRaycaster_t3396295912_0_0_0;
extern const Il2CppType CanvasRenderer_t799251060_0_0_0;
extern const Il2CppType Corner_t1917386932_0_0_0;
extern const Il2CppType Axis_t312615268_0_0_0;
extern const Il2CppType Constraint_t464777171_0_0_0;
extern const Il2CppType SubmitEvent_t1556243238_0_0_0;
extern const Il2CppType OnChangeEvent_t2953828243_0_0_0;
extern const Il2CppType OnValidateInput_t4262405814_0_0_0;
extern const Il2CppType LayoutElement_t1267321563_0_0_0;
extern const Il2CppType RectOffset_t2771649182_0_0_0;
extern const Il2CppType TextAnchor_t1510919083_0_0_0;
extern const Il2CppType AnimationTriggers_t2976076601_0_0_0;
extern const Il2CppType Animator_t5420133_0_0_0;
extern const Il2CppType UnityARVideo_t2470488057_0_0_0;
extern const Il2CppType DontDestroyOnLoad_t2575831040_0_0_0;
extern const Il2CppType MeshFilter_t2678471223_0_0_0;
extern const Il2CppType MeshRenderer_t1885265068_0_0_0;
extern const Il2CppType Animation_t1557311624_0_0_0;
extern const Il2CppType BoxSlider_t2780260910_0_0_0;
extern const Il2CppType Light_t2513018095_0_0_0;




static const RuntimeType* GenInst_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0 = { 1, GenInst_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0 = { 1, GenInst_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_Char_t539237919_0_0_0_Types[] = { (&Char_t539237919_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t539237919_0_0_0 = { 1, GenInst_Char_t539237919_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t2252457107_0_0_0_Types[] = { (&Int64_t2252457107_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t2252457107_0_0_0 = { 1, GenInst_Int64_t2252457107_0_0_0_Types };
static const RuntimeType* GenInst_UInt32_t3933237433_0_0_0_Types[] = { (&UInt32_t3933237433_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt32_t3933237433_0_0_0 = { 1, GenInst_UInt32_t3933237433_0_0_0_Types };
static const RuntimeType* GenInst_UInt64_t1498391637_0_0_0_Types[] = { (&UInt64_t1498391637_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt64_t1498391637_0_0_0 = { 1, GenInst_UInt64_t1498391637_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t3065488403_0_0_0_Types[] = { (&Byte_t3065488403_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t3065488403_0_0_0 = { 1, GenInst_Byte_t3065488403_0_0_0_Types };
static const RuntimeType* GenInst_SByte_t1519635365_0_0_0_Types[] = { (&SByte_t1519635365_0_0_0) };
extern const Il2CppGenericInst GenInst_SByte_t1519635365_0_0_0 = { 1, GenInst_SByte_t1519635365_0_0_0_Types };
static const RuntimeType* GenInst_Int16_t2196066360_0_0_0_Types[] = { (&Int16_t2196066360_0_0_0) };
extern const Il2CppGenericInst GenInst_Int16_t2196066360_0_0_0 = { 1, GenInst_Int16_t2196066360_0_0_0_Types };
static const RuntimeType* GenInst_UInt16_t2173916929_0_0_0_Types[] = { (&UInt16_t2173916929_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt16_t2173916929_0_0_0 = { 1, GenInst_UInt16_t2173916929_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Types[] = { (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const RuntimeType* GenInst_IConvertible_t2722588824_0_0_0_Types[] = { (&IConvertible_t2722588824_0_0_0) };
extern const Il2CppGenericInst GenInst_IConvertible_t2722588824_0_0_0 = { 1, GenInst_IConvertible_t2722588824_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_t1036281117_0_0_0_Types[] = { (&IComparable_t1036281117_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_t1036281117_0_0_0 = { 1, GenInst_IComparable_t1036281117_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_t2201844525_0_0_0_Types[] = { (&IEnumerable_t2201844525_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_t2201844525_0_0_0 = { 1, GenInst_IEnumerable_t2201844525_0_0_0_Types };
static const RuntimeType* GenInst_ICloneable_t2975100769_0_0_0_Types[] = { (&ICloneable_t2975100769_0_0_0) };
extern const Il2CppGenericInst GenInst_ICloneable_t2975100769_0_0_0 = { 1, GenInst_ICloneable_t2975100769_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_1_t2296033782_0_0_0_Types[] = { (&IComparable_1_t2296033782_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_1_t2296033782_0_0_0 = { 1, GenInst_IComparable_1_t2296033782_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t3464885664_0_0_0_Types[] = { (&IEquatable_1_t3464885664_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3464885664_0_0_0 = { 1, GenInst_IEquatable_1_t3464885664_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_IReflect_t901526961_0_0_0_Types[] = { (&IReflect_t901526961_0_0_0) };
extern const Il2CppGenericInst GenInst_IReflect_t901526961_0_0_0 = { 1, GenInst_IReflect_t901526961_0_0_0_Types };
static const RuntimeType* GenInst__Type_t3150095453_0_0_0_Types[] = { (&_Type_t3150095453_0_0_0) };
extern const Il2CppGenericInst GenInst__Type_t3150095453_0_0_0 = { 1, GenInst__Type_t3150095453_0_0_0_Types };
static const RuntimeType* GenInst_MemberInfo_t_0_0_0_Types[] = { (&MemberInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_ICustomAttributeProvider_t490813527_0_0_0_Types[] = { (&ICustomAttributeProvider_t490813527_0_0_0) };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t490813527_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t490813527_0_0_0_Types };
static const RuntimeType* GenInst__MemberInfo_t2295893705_0_0_0_Types[] = { (&_MemberInfo_t2295893705_0_0_0) };
extern const Il2CppGenericInst GenInst__MemberInfo_t2295893705_0_0_0 = { 1, GenInst__MemberInfo_t2295893705_0_0_0_Types };
static const RuntimeType* GenInst_Double_t1029397067_0_0_0_Types[] = { (&Double_t1029397067_0_0_0) };
extern const Il2CppGenericInst GenInst_Double_t1029397067_0_0_0 = { 1, GenInst_Double_t1029397067_0_0_0_Types };
static const RuntimeType* GenInst_Single_t2847614712_0_0_0_Types[] = { (&Single_t2847614712_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t2847614712_0_0_0 = { 1, GenInst_Single_t2847614712_0_0_0_Types };
static const RuntimeType* GenInst_Decimal_t2663171440_0_0_0_Types[] = { (&Decimal_t2663171440_0_0_0) };
extern const Il2CppGenericInst GenInst_Decimal_t2663171440_0_0_0 = { 1, GenInst_Decimal_t2663171440_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t362855854_0_0_0_Types[] = { (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t362855854_0_0_0 = { 1, GenInst_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_Delegate_t69892740_0_0_0_Types[] = { (&Delegate_t69892740_0_0_0) };
extern const Il2CppGenericInst GenInst_Delegate_t69892740_0_0_0 = { 1, GenInst_Delegate_t69892740_0_0_0_Types };
static const RuntimeType* GenInst_ISerializable_t1411645196_0_0_0_Types[] = { (&ISerializable_t1411645196_0_0_0) };
extern const Il2CppGenericInst GenInst_ISerializable_t1411645196_0_0_0 = { 1, GenInst_ISerializable_t1411645196_0_0_0_Types };
static const RuntimeType* GenInst_ParameterInfo_t3577984089_0_0_0_Types[] = { (&ParameterInfo_t3577984089_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterInfo_t3577984089_0_0_0 = { 1, GenInst_ParameterInfo_t3577984089_0_0_0_Types };
static const RuntimeType* GenInst__ParameterInfo_t3423239624_0_0_0_Types[] = { (&_ParameterInfo_t3423239624_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterInfo_t3423239624_0_0_0 = { 1, GenInst__ParameterInfo_t3423239624_0_0_0_Types };
static const RuntimeType* GenInst_ParameterModifier_t3485655876_0_0_0_Types[] = { (&ParameterModifier_t3485655876_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterModifier_t3485655876_0_0_0 = { 1, GenInst_ParameterModifier_t3485655876_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_EventInfo_t_0_0_0_Types[] = { (&EventInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__EventInfo_t1619462957_0_0_0_Types[] = { (&_EventInfo_t1619462957_0_0_0) };
extern const Il2CppGenericInst GenInst__EventInfo_t1619462957_0_0_0 = { 1, GenInst__EventInfo_t1619462957_0_0_0_Types };
static const RuntimeType* GenInst_FieldInfo_t_0_0_0_Types[] = { (&FieldInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__FieldInfo_t2155012913_0_0_0_Types[] = { (&_FieldInfo_t2155012913_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldInfo_t2155012913_0_0_0 = { 1, GenInst__FieldInfo_t2155012913_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Types[] = { (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__MethodInfo_t2553989953_0_0_0_Types[] = { (&_MethodInfo_t2553989953_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodInfo_t2553989953_0_0_0 = { 1, GenInst__MethodInfo_t2553989953_0_0_0_Types };
static const RuntimeType* GenInst_MethodBase_t1640104494_0_0_0_Types[] = { (&MethodBase_t1640104494_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBase_t1640104494_0_0_0 = { 1, GenInst_MethodBase_t1640104494_0_0_0_Types };
static const RuntimeType* GenInst__MethodBase_t3674854218_0_0_0_Types[] = { (&_MethodBase_t3674854218_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBase_t3674854218_0_0_0 = { 1, GenInst__MethodBase_t3674854218_0_0_0_Types };
static const RuntimeType* GenInst_PropertyInfo_t_0_0_0_Types[] = { (&PropertyInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__PropertyInfo_t2466204053_0_0_0_Types[] = { (&_PropertyInfo_t2466204053_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyInfo_t2466204053_0_0_0 = { 1, GenInst__PropertyInfo_t2466204053_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorInfo_t1214230744_0_0_0_Types[] = { (&ConstructorInfo_t1214230744_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1214230744_0_0_0 = { 1, GenInst_ConstructorInfo_t1214230744_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorInfo_t3104092857_0_0_0_Types[] = { (&_ConstructorInfo_t3104092857_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3104092857_0_0_0 = { 1, GenInst__ConstructorInfo_t3104092857_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_TableRange_t3005830667_0_0_0_Types[] = { (&TableRange_t3005830667_0_0_0) };
extern const Il2CppGenericInst GenInst_TableRange_t3005830667_0_0_0 = { 1, GenInst_TableRange_t3005830667_0_0_0_Types };
static const RuntimeType* GenInst_TailoringInfo_t2103418023_0_0_0_Types[] = { (&TailoringInfo_t2103418023_0_0_0) };
extern const Il2CppGenericInst GenInst_TailoringInfo_t2103418023_0_0_0 = { 1, GenInst_TailoringInfo_t2103418023_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3968584898_0_0_0_Types[] = { (&KeyValuePair_2_t3968584898_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3968584898_0_0_0 = { 1, GenInst_KeyValuePair_2_t3968584898_0_0_0_Types };
static const RuntimeType* GenInst_Link_t4043807316_0_0_0_Types[] = { (&Link_t4043807316_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t4043807316_0_0_0 = { 1, GenInst_Link_t4043807316_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_Int32_t3425510919_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t3425510919_0_0_0), (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_Int32_t3425510919_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t3425510919_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3951334252_0_0_0 = { 1, GenInst_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t3968584898_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t3425510919_0_0_0), (&KeyValuePair_2_t3968584898_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t3968584898_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t3968584898_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t3425510919_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t2032264690_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t3425510919_0_0_0), (&KeyValuePair_2_t2032264690_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t2032264690_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t2032264690_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2032264690_0_0_0_Types[] = { (&KeyValuePair_2_t2032264690_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2032264690_0_0_0 = { 1, GenInst_KeyValuePair_2_t2032264690_0_0_0_Types };
static const RuntimeType* GenInst_Contraction_t3464654688_0_0_0_Types[] = { (&Contraction_t3464654688_0_0_0) };
extern const Il2CppGenericInst GenInst_Contraction_t3464654688_0_0_0 = { 1, GenInst_Contraction_t3464654688_0_0_0_Types };
static const RuntimeType* GenInst_Level2Map_t782964927_0_0_0_Types[] = { (&Level2Map_t782964927_0_0_0) };
extern const Il2CppGenericInst GenInst_Level2Map_t782964927_0_0_0 = { 1, GenInst_Level2Map_t782964927_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t1286850636_0_0_0_Types[] = { (&BigInteger_t1286850636_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t1286850636_0_0_0 = { 1, GenInst_BigInteger_t1286850636_0_0_0_Types };
static const RuntimeType* GenInst_KeySizes_t2873621649_0_0_0_Types[] = { (&KeySizes_t2873621649_0_0_0) };
extern const Il2CppGenericInst GenInst_KeySizes_t2873621649_0_0_0 = { 1, GenInst_KeySizes_t2873621649_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t665585682_0_0_0_Types[] = { (&KeyValuePair_2_t665585682_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t665585682_0_0_0 = { 1, GenInst_KeyValuePair_2_t665585682_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t665585682_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t665585682_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t665585682_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t665585682_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t4047215697_0_0_0_Types[] = { (&Slot_t4047215697_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t4047215697_0_0_0 = { 1, GenInst_Slot_t4047215697_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t3212906767_0_0_0_Types[] = { (&Slot_t3212906767_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t3212906767_0_0_0 = { 1, GenInst_Slot_t3212906767_0_0_0_Types };
static const RuntimeType* GenInst_StackFrame_t2301538707_0_0_0_Types[] = { (&StackFrame_t2301538707_0_0_0) };
extern const Il2CppGenericInst GenInst_StackFrame_t2301538707_0_0_0 = { 1, GenInst_StackFrame_t2301538707_0_0_0_Types };
static const RuntimeType* GenInst_Calendar_t4189858442_0_0_0_Types[] = { (&Calendar_t4189858442_0_0_0) };
extern const Il2CppGenericInst GenInst_Calendar_t4189858442_0_0_0 = { 1, GenInst_Calendar_t4189858442_0_0_0_Types };
static const RuntimeType* GenInst_ModuleBuilder_t2846916743_0_0_0_Types[] = { (&ModuleBuilder_t2846916743_0_0_0) };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t2846916743_0_0_0 = { 1, GenInst_ModuleBuilder_t2846916743_0_0_0_Types };
static const RuntimeType* GenInst__ModuleBuilder_t1727831225_0_0_0_Types[] = { (&_ModuleBuilder_t1727831225_0_0_0) };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1727831225_0_0_0 = { 1, GenInst__ModuleBuilder_t1727831225_0_0_0_Types };
static const RuntimeType* GenInst_Module_t4287715521_0_0_0_Types[] = { (&Module_t4287715521_0_0_0) };
extern const Il2CppGenericInst GenInst_Module_t4287715521_0_0_0 = { 1, GenInst_Module_t4287715521_0_0_0_Types };
static const RuntimeType* GenInst__Module_t3465402545_0_0_0_Types[] = { (&_Module_t3465402545_0_0_0) };
extern const Il2CppGenericInst GenInst__Module_t3465402545_0_0_0 = { 1, GenInst__Module_t3465402545_0_0_0_Types };
static const RuntimeType* GenInst_ParameterBuilder_t2375480068_0_0_0_Types[] = { (&ParameterBuilder_t2375480068_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t2375480068_0_0_0 = { 1, GenInst_ParameterBuilder_t2375480068_0_0_0_Types };
static const RuntimeType* GenInst__ParameterBuilder_t2449263151_0_0_0_Types[] = { (&_ParameterBuilder_t2449263151_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2449263151_0_0_0 = { 1, GenInst__ParameterBuilder_t2449263151_0_0_0_Types };
static const RuntimeType* GenInst_TypeU5BU5D_t1484232934_0_0_0_Types[] = { (&TypeU5BU5D_t1484232934_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1484232934_0_0_0 = { 1, GenInst_TypeU5BU5D_t1484232934_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeArray_0_0_0_Types[] = { (&RuntimeArray_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeArray_0_0_0 = { 1, GenInst_RuntimeArray_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_t1426578131_0_0_0_Types[] = { (&ICollection_t1426578131_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_t1426578131_0_0_0 = { 1, GenInst_ICollection_t1426578131_0_0_0_Types };
static const RuntimeType* GenInst_IList_t4121428329_0_0_0_Types[] = { (&IList_t4121428329_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_t4121428329_0_0_0 = { 1, GenInst_IList_t4121428329_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t575872299_0_0_0_Types[] = { (&IList_1_t575872299_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t575872299_0_0_0 = { 1, GenInst_IList_1_t575872299_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2387962414_0_0_0_Types[] = { (&ICollection_1_t2387962414_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2387962414_0_0_0 = { 1, GenInst_ICollection_1_t2387962414_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t445450583_0_0_0_Types[] = { (&IEnumerable_1_t445450583_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t445450583_0_0_0 = { 1, GenInst_IEnumerable_1_t445450583_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2458661789_0_0_0_Types[] = { (&IList_1_t2458661789_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2458661789_0_0_0 = { 1, GenInst_IList_1_t2458661789_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t4270751904_0_0_0_Types[] = { (&ICollection_1_t4270751904_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t4270751904_0_0_0 = { 1, GenInst_ICollection_1_t4270751904_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2328240073_0_0_0_Types[] = { (&IEnumerable_1_t2328240073_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2328240073_0_0_0 = { 1, GenInst_IEnumerable_1_t2328240073_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t412262985_0_0_0_Types[] = { (&IList_1_t412262985_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t412262985_0_0_0 = { 1, GenInst_IList_1_t412262985_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2224353100_0_0_0_Types[] = { (&ICollection_1_t2224353100_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2224353100_0_0_0 = { 1, GenInst_ICollection_1_t2224353100_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t281841269_0_0_0_Types[] = { (&IEnumerable_1_t281841269_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t281841269_0_0_0 = { 1, GenInst_IEnumerable_1_t281841269_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2505642904_0_0_0_Types[] = { (&IList_1_t2505642904_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2505642904_0_0_0 = { 1, GenInst_IList_1_t2505642904_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t22765723_0_0_0_Types[] = { (&ICollection_1_t22765723_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t22765723_0_0_0 = { 1, GenInst_ICollection_1_t22765723_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2375221188_0_0_0_Types[] = { (&IEnumerable_1_t2375221188_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2375221188_0_0_0 = { 1, GenInst_IEnumerable_1_t2375221188_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2047948355_0_0_0_Types[] = { (&IList_1_t2047948355_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2047948355_0_0_0 = { 1, GenInst_IList_1_t2047948355_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3860038470_0_0_0_Types[] = { (&ICollection_1_t3860038470_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3860038470_0_0_0 = { 1, GenInst_ICollection_1_t3860038470_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1917526639_0_0_0_Types[] = { (&IEnumerable_1_t1917526639_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1917526639_0_0_0 = { 1, GenInst_IEnumerable_1_t1917526639_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3853028533_0_0_0_Types[] = { (&IList_1_t3853028533_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3853028533_0_0_0 = { 1, GenInst_IList_1_t3853028533_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1370151352_0_0_0_Types[] = { (&ICollection_1_t1370151352_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1370151352_0_0_0 = { 1, GenInst_ICollection_1_t1370151352_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3722606817_0_0_0_Types[] = { (&IEnumerable_1_t3722606817_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3722606817_0_0_0 = { 1, GenInst_IEnumerable_1_t3722606817_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1679646531_0_0_0_Types[] = { (&IList_1_t1679646531_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1679646531_0_0_0 = { 1, GenInst_IList_1_t1679646531_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3491736646_0_0_0_Types[] = { (&ICollection_1_t3491736646_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3491736646_0_0_0 = { 1, GenInst_ICollection_1_t3491736646_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1549224815_0_0_0_Types[] = { (&IEnumerable_1_t1549224815_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1549224815_0_0_0 = { 1, GenInst_IEnumerable_1_t1549224815_0_0_0_Types };
static const RuntimeType* GenInst_ILTokenInfo_t4134893432_0_0_0_Types[] = { (&ILTokenInfo_t4134893432_0_0_0) };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t4134893432_0_0_0 = { 1, GenInst_ILTokenInfo_t4134893432_0_0_0_Types };
static const RuntimeType* GenInst_LabelData_t1800135739_0_0_0_Types[] = { (&LabelData_t1800135739_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelData_t1800135739_0_0_0 = { 1, GenInst_LabelData_t1800135739_0_0_0_Types };
static const RuntimeType* GenInst_LabelFixup_t703658549_0_0_0_Types[] = { (&LabelFixup_t703658549_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelFixup_t703658549_0_0_0 = { 1, GenInst_LabelFixup_t703658549_0_0_0_Types };
static const RuntimeType* GenInst_GenericTypeParameterBuilder_t451279079_0_0_0_Types[] = { (&GenericTypeParameterBuilder_t451279079_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t451279079_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t451279079_0_0_0_Types };
static const RuntimeType* GenInst_TypeBuilder_t1669012508_0_0_0_Types[] = { (&TypeBuilder_t1669012508_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1669012508_0_0_0 = { 1, GenInst_TypeBuilder_t1669012508_0_0_0_Types };
static const RuntimeType* GenInst__TypeBuilder_t4212529197_0_0_0_Types[] = { (&_TypeBuilder_t4212529197_0_0_0) };
extern const Il2CppGenericInst GenInst__TypeBuilder_t4212529197_0_0_0 = { 1, GenInst__TypeBuilder_t4212529197_0_0_0_Types };
static const RuntimeType* GenInst_MethodBuilder_t1092218305_0_0_0_Types[] = { (&MethodBuilder_t1092218305_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1092218305_0_0_0 = { 1, GenInst_MethodBuilder_t1092218305_0_0_0_Types };
static const RuntimeType* GenInst__MethodBuilder_t3510139682_0_0_0_Types[] = { (&_MethodBuilder_t3510139682_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3510139682_0_0_0 = { 1, GenInst__MethodBuilder_t3510139682_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorBuilder_t33025729_0_0_0_Types[] = { (&ConstructorBuilder_t33025729_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t33025729_0_0_0 = { 1, GenInst_ConstructorBuilder_t33025729_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorBuilder_t1274505923_0_0_0_Types[] = { (&_ConstructorBuilder_t1274505923_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1274505923_0_0_0 = { 1, GenInst__ConstructorBuilder_t1274505923_0_0_0_Types };
static const RuntimeType* GenInst_PropertyBuilder_t1720155075_0_0_0_Types[] = { (&PropertyBuilder_t1720155075_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t1720155075_0_0_0 = { 1, GenInst_PropertyBuilder_t1720155075_0_0_0_Types };
static const RuntimeType* GenInst__PropertyBuilder_t3760533436_0_0_0_Types[] = { (&_PropertyBuilder_t3760533436_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3760533436_0_0_0 = { 1, GenInst__PropertyBuilder_t3760533436_0_0_0_Types };
static const RuntimeType* GenInst_FieldBuilder_t1478132776_0_0_0_Types[] = { (&FieldBuilder_t1478132776_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1478132776_0_0_0 = { 1, GenInst_FieldBuilder_t1478132776_0_0_0_Types };
static const RuntimeType* GenInst__FieldBuilder_t698479335_0_0_0_Types[] = { (&_FieldBuilder_t698479335_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldBuilder_t698479335_0_0_0 = { 1, GenInst__FieldBuilder_t698479335_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t3013856313_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t3013856313_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t3013856313_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t3013856313_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t1224813713_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t1224813713_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1224813713_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t1224813713_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeData_t2607898573_0_0_0_Types[] = { (&CustomAttributeData_t2607898573_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t2607898573_0_0_0 = { 1, GenInst_CustomAttributeData_t2607898573_0_0_0_Types };
static const RuntimeType* GenInst_ResourceInfo_t3677787790_0_0_0_Types[] = { (&ResourceInfo_t3677787790_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3677787790_0_0_0 = { 1, GenInst_ResourceInfo_t3677787790_0_0_0_Types };
static const RuntimeType* GenInst_ResourceCacheItem_t567161473_0_0_0_Types[] = { (&ResourceCacheItem_t567161473_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t567161473_0_0_0 = { 1, GenInst_ResourceCacheItem_t567161473_0_0_0_Types };
static const RuntimeType* GenInst_IContextProperty_t402073787_0_0_0_Types[] = { (&IContextProperty_t402073787_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextProperty_t402073787_0_0_0 = { 1, GenInst_IContextProperty_t402073787_0_0_0_Types };
static const RuntimeType* GenInst_Header_t1099438005_0_0_0_Types[] = { (&Header_t1099438005_0_0_0) };
extern const Il2CppGenericInst GenInst_Header_t1099438005_0_0_0 = { 1, GenInst_Header_t1099438005_0_0_0_Types };
static const RuntimeType* GenInst_ITrackingHandler_t127120994_0_0_0_Types[] = { (&ITrackingHandler_t127120994_0_0_0) };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t127120994_0_0_0 = { 1, GenInst_ITrackingHandler_t127120994_0_0_0_Types };
static const RuntimeType* GenInst_IContextAttribute_t3616836717_0_0_0_Types[] = { (&IContextAttribute_t3616836717_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextAttribute_t3616836717_0_0_0 = { 1, GenInst_IContextAttribute_t3616836717_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t1819153659_0_0_0_Types[] = { (&DateTime_t1819153659_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t1819153659_0_0_0 = { 1, GenInst_DateTime_t1819153659_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t4158060032_0_0_0_Types[] = { (&TimeSpan_t4158060032_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t4158060032_0_0_0 = { 1, GenInst_TimeSpan_t4158060032_0_0_0_Types };
static const RuntimeType* GenInst_TypeTag_t1792597276_0_0_0_Types[] = { (&TypeTag_t1792597276_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeTag_t1792597276_0_0_0 = { 1, GenInst_TypeTag_t1792597276_0_0_0_Types };
static const RuntimeType* GenInst_MonoType_t_0_0_0_Types[] = { (&MonoType_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
static const RuntimeType* GenInst_StrongName_t3725927940_0_0_0_Types[] = { (&StrongName_t3725927940_0_0_0) };
extern const Il2CppGenericInst GenInst_StrongName_t3725927940_0_0_0 = { 1, GenInst_StrongName_t3725927940_0_0_0_Types };
static const RuntimeType* GenInst_IBuiltInEvidence_t3225111825_0_0_0_Types[] = { (&IBuiltInEvidence_t3225111825_0_0_0) };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t3225111825_0_0_0 = { 1, GenInst_IBuiltInEvidence_t3225111825_0_0_0_Types };
static const RuntimeType* GenInst_IIdentityPermissionFactory_t4063359944_0_0_0_Types[] = { (&IIdentityPermissionFactory_t4063359944_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t4063359944_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t4063359944_0_0_0_Types };
static const RuntimeType* GenInst_DateTimeOffset_t2489358039_0_0_0_Types[] = { (&DateTimeOffset_t2489358039_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t2489358039_0_0_0 = { 1, GenInst_DateTimeOffset_t2489358039_0_0_0_Types };
static const RuntimeType* GenInst_Guid_t_0_0_0_Types[] = { (&Guid_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
static const RuntimeType* GenInst_Version_t3431102195_0_0_0_Types[] = { (&Version_t3431102195_0_0_0) };
extern const Il2CppGenericInst GenInst_Version_t3431102195_0_0_0 = { 1, GenInst_Version_t3431102195_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t905929833_0_0_0_Types[] = { (&KeyValuePair_2_t905929833_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t905929833_0_0_0 = { 1, GenInst_KeyValuePair_2_t905929833_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t362855854_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_Boolean_t362855854_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t362855854_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_KeyValuePair_2_t905929833_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t362855854_0_0_0), (&KeyValuePair_2_t905929833_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_KeyValuePair_2_t905929833_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_KeyValuePair_2_t905929833_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t362855854_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_KeyValuePair_2_t3264576921_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t362855854_0_0_0), (&KeyValuePair_2_t3264576921_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_KeyValuePair_2_t3264576921_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_KeyValuePair_2_t3264576921_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3264576921_0_0_0_Types[] = { (&KeyValuePair_2_t3264576921_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3264576921_0_0_0 = { 1, GenInst_KeyValuePair_2_t3264576921_0_0_0_Types };
static const RuntimeType* GenInst_X509Certificate_t1886472155_0_0_0_Types[] = { (&X509Certificate_t1886472155_0_0_0) };
extern const Il2CppGenericInst GenInst_X509Certificate_t1886472155_0_0_0 = { 1, GenInst_X509Certificate_t1886472155_0_0_0_Types };
static const RuntimeType* GenInst_IDeserializationCallback_t1045582565_0_0_0_Types[] = { (&IDeserializationCallback_t1045582565_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1045582565_0_0_0 = { 1, GenInst_IDeserializationCallback_t1045582565_0_0_0_Types };
static const RuntimeType* GenInst_X509ChainStatus_t3209745328_0_0_0_Types[] = { (&X509ChainStatus_t3209745328_0_0_0) };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t3209745328_0_0_0 = { 1, GenInst_X509ChainStatus_t3209745328_0_0_0_Types };
static const RuntimeType* GenInst_Capture_t545002712_0_0_0_Types[] = { (&Capture_t545002712_0_0_0) };
extern const Il2CppGenericInst GenInst_Capture_t545002712_0_0_0 = { 1, GenInst_Capture_t545002712_0_0_0_Types };
static const RuntimeType* GenInst_Group_t1246193761_0_0_0_Types[] = { (&Group_t1246193761_0_0_0) };
extern const Il2CppGenericInst GenInst_Group_t1246193761_0_0_0 = { 1, GenInst_Group_t1246193761_0_0_0_Types };
static const RuntimeType* GenInst_Mark_t1421951811_0_0_0_Types[] = { (&Mark_t1421951811_0_0_0) };
extern const Il2CppGenericInst GenInst_Mark_t1421951811_0_0_0 = { 1, GenInst_Mark_t1421951811_0_0_0_Types };
static const RuntimeType* GenInst_UriScheme_t1685060244_0_0_0_Types[] = { (&UriScheme_t1685060244_0_0_0) };
extern const Il2CppGenericInst GenInst_UriScheme_t1685060244_0_0_0 = { 1, GenInst_UriScheme_t1685060244_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t1286850637_0_0_0_Types[] = { (&BigInteger_t1286850637_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t1286850637_0_0_0 = { 1, GenInst_BigInteger_t1286850637_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t3548078658_0_0_0_Types[] = { (&ByteU5BU5D_t3548078658_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3548078658_0_0_0 = { 1, GenInst_ByteU5BU5D_t3548078658_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t327655935_0_0_0_Types[] = { (&IList_1_t327655935_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t327655935_0_0_0 = { 1, GenInst_IList_1_t327655935_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2139746050_0_0_0_Types[] = { (&ICollection_1_t2139746050_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2139746050_0_0_0 = { 1, GenInst_ICollection_1_t2139746050_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t197234219_0_0_0_Types[] = { (&IEnumerable_1_t197234219_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t197234219_0_0_0 = { 1, GenInst_IEnumerable_1_t197234219_0_0_0_Types };
static const RuntimeType* GenInst_ClientCertificateType_t1149805517_0_0_0_Types[] = { (&ClientCertificateType_t1149805517_0_0_0) };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1149805517_0_0_0 = { 1, GenInst_ClientCertificateType_t1149805517_0_0_0_Types };
static const RuntimeType* GenInst_Link_t655338529_0_0_0_Types[] = { (&Link_t655338529_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t655338529_0_0_0 = { 1, GenInst_Link_t655338529_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2975039247_0_0_0_Types[] = { (&List_1_t2975039247_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2975039247_0_0_0 = { 1, GenInst_List_1_t2975039247_0_0_0_Types };
static const RuntimeType* GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_Types[] = { (&DispatcherKey_t3620541735_0_0_0), (&Dispatcher_t55733337_0_0_0) };
extern const Il2CppGenericInst GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0 = { 2, GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_Types };
static const RuntimeType* GenInst_DispatcherKey_t3620541735_0_0_0_Types[] = { (&DispatcherKey_t3620541735_0_0_0) };
extern const Il2CppGenericInst GenInst_DispatcherKey_t3620541735_0_0_0 = { 1, GenInst_DispatcherKey_t3620541735_0_0_0_Types };
static const RuntimeType* GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&DispatcherKey_t3620541735_0_0_0), (&Dispatcher_t55733337_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_KeyValuePair_2_t2849605124_0_0_0_Types[] = { (&DispatcherKey_t3620541735_0_0_0), (&Dispatcher_t55733337_0_0_0), (&KeyValuePair_2_t2849605124_0_0_0) };
extern const Il2CppGenericInst GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_KeyValuePair_2_t2849605124_0_0_0 = { 3, GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_KeyValuePair_2_t2849605124_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2849605124_0_0_0_Types[] = { (&KeyValuePair_2_t2849605124_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2849605124_0_0_0 = { 1, GenInst_KeyValuePair_2_t2849605124_0_0_0_Types };
static const RuntimeType* GenInst_Object_t1008057425_0_0_0_Types[] = { (&Object_t1008057425_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_t1008057425_0_0_0 = { 1, GenInst_Object_t1008057425_0_0_0_Types };
static const RuntimeType* GenInst_Camera_t226495598_0_0_0_Types[] = { (&Camera_t226495598_0_0_0) };
extern const Il2CppGenericInst GenInst_Camera_t226495598_0_0_0 = { 1, GenInst_Camera_t226495598_0_0_0_Types };
static const RuntimeType* GenInst_Behaviour_t2200997390_0_0_0_Types[] = { (&Behaviour_t2200997390_0_0_0) };
extern const Il2CppGenericInst GenInst_Behaviour_t2200997390_0_0_0 = { 1, GenInst_Behaviour_t2200997390_0_0_0_Types };
static const RuntimeType* GenInst_Component_t531478471_0_0_0_Types[] = { (&Component_t531478471_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_t531478471_0_0_0 = { 1, GenInst_Component_t531478471_0_0_0_Types };
static const RuntimeType* GenInst_Display_t4159198643_0_0_0_Types[] = { (&Display_t4159198643_0_0_0) };
extern const Il2CppGenericInst GenInst_Display_t4159198643_0_0_0 = { 1, GenInst_Display_t4159198643_0_0_0_Types };
static const RuntimeType* GenInst_Keyframe_t643552408_0_0_0_Types[] = { (&Keyframe_t643552408_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyframe_t643552408_0_0_0 = { 1, GenInst_Keyframe_t643552408_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t596762001_0_0_0_Types[] = { (&Vector3_t596762001_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t596762001_0_0_0 = { 1, GenInst_Vector3_t596762001_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t1376926224_0_0_0_Types[] = { (&Vector4_t1376926224_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t1376926224_0_0_0 = { 1, GenInst_Vector4_t1376926224_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t59524482_0_0_0_Types[] = { (&Vector2_t59524482_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t59524482_0_0_0 = { 1, GenInst_Vector2_t59524482_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t2499566028_0_0_0_Types[] = { (&Color32_t2499566028_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t2499566028_0_0_0 = { 1, GenInst_Color32_t2499566028_0_0_0_Types };
static const RuntimeType* GenInst_Playable_t1112762172_0_0_0_Types[] = { (&Playable_t1112762172_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_t1112762172_0_0_0 = { 1, GenInst_Playable_t1112762172_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_t1654971666_0_0_0_Types[] = { (&PlayableOutput_t1654971666_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_t1654971666_0_0_0 = { 1, GenInst_PlayableOutput_t1654971666_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1614351645_0_0_0_LoadSceneMode_t2556746618_0_0_0_Types[] = { (&Scene_t1614351645_0_0_0), (&LoadSceneMode_t2556746618_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1614351645_0_0_0_LoadSceneMode_t2556746618_0_0_0 = { 2, GenInst_Scene_t1614351645_0_0_0_LoadSceneMode_t2556746618_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1614351645_0_0_0_Types[] = { (&Scene_t1614351645_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1614351645_0_0_0 = { 1, GenInst_Scene_t1614351645_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1614351645_0_0_0_Scene_t1614351645_0_0_0_Types[] = { (&Scene_t1614351645_0_0_0), (&Scene_t1614351645_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1614351645_0_0_0_Scene_t1614351645_0_0_0 = { 2, GenInst_Scene_t1614351645_0_0_0_Scene_t1614351645_0_0_0_Types };
static const RuntimeType* GenInst_SpriteAtlas_t1553410611_0_0_0_Types[] = { (&SpriteAtlas_t1553410611_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteAtlas_t1553410611_0_0_0 = { 1, GenInst_SpriteAtlas_t1553410611_0_0_0_Types };
static const RuntimeType* GenInst_Particle_t1268635397_0_0_0_Types[] = { (&Particle_t1268635397_0_0_0) };
extern const Il2CppGenericInst GenInst_Particle_t1268635397_0_0_0 = { 1, GenInst_Particle_t1268635397_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint_t3149140470_0_0_0_Types[] = { (&ContactPoint_t3149140470_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint_t3149140470_0_0_0 = { 1, GenInst_ContactPoint_t3149140470_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit_t1273336641_0_0_0_Types[] = { (&RaycastHit_t1273336641_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit_t1273336641_0_0_0 = { 1, GenInst_RaycastHit_t1273336641_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody2D_t2029812455_0_0_0_Types[] = { (&Rigidbody2D_t2029812455_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t2029812455_0_0_0 = { 1, GenInst_Rigidbody2D_t2029812455_0_0_0_Types };
static const RuntimeType* GenInst_AudioClipPlayable_t1384420439_0_0_0_Types[] = { (&AudioClipPlayable_t1384420439_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClipPlayable_t1384420439_0_0_0 = { 1, GenInst_AudioClipPlayable_t1384420439_0_0_0_Types };
static const RuntimeType* GenInst_AudioMixerPlayable_t3880288074_0_0_0_Types[] = { (&AudioMixerPlayable_t3880288074_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioMixerPlayable_t3880288074_0_0_0 = { 1, GenInst_AudioMixerPlayable_t3880288074_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClipPlayable_t3824769629_0_0_0_Types[] = { (&AnimationClipPlayable_t3824769629_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClipPlayable_t3824769629_0_0_0 = { 1, GenInst_AnimationClipPlayable_t3824769629_0_0_0_Types };
static const RuntimeType* GenInst_AnimationLayerMixerPlayable_t3575213449_0_0_0_Types[] = { (&AnimationLayerMixerPlayable_t3575213449_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationLayerMixerPlayable_t3575213449_0_0_0 = { 1, GenInst_AnimationLayerMixerPlayable_t3575213449_0_0_0_Types };
static const RuntimeType* GenInst_AnimationMixerPlayable_t4293775112_0_0_0_Types[] = { (&AnimationMixerPlayable_t4293775112_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationMixerPlayable_t4293775112_0_0_0 = { 1, GenInst_AnimationMixerPlayable_t4293775112_0_0_0_Types };
static const RuntimeType* GenInst_AnimationOffsetPlayable_t1345717778_0_0_0_Types[] = { (&AnimationOffsetPlayable_t1345717778_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationOffsetPlayable_t1345717778_0_0_0 = { 1, GenInst_AnimationOffsetPlayable_t1345717778_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerPlayable_t3438332640_0_0_0_Types[] = { (&AnimatorControllerPlayable_t3438332640_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerPlayable_t3438332640_0_0_0 = { 1, GenInst_AnimatorControllerPlayable_t3438332640_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorClipInfo_t183054942_0_0_0_Types[] = { (&AnimatorClipInfo_t183054942_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t183054942_0_0_0 = { 1, GenInst_AnimatorClipInfo_t183054942_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerParameter_t3164458347_0_0_0_Types[] = { (&AnimatorControllerParameter_t3164458347_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t3164458347_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t3164458347_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t2672378834_0_0_0_Types[] = { (&UIVertex_t2672378834_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t2672378834_0_0_0 = { 1, GenInst_UIVertex_t2672378834_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t2367319176_0_0_0_Types[] = { (&UICharInfo_t2367319176_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t2367319176_0_0_0 = { 1, GenInst_UICharInfo_t2367319176_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t248963365_0_0_0_Types[] = { (&UILineInfo_t248963365_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t248963365_0_0_0 = { 1, GenInst_UILineInfo_t248963365_0_0_0_Types };
static const RuntimeType* GenInst_Font_t2699442136_0_0_0_Types[] = { (&Font_t2699442136_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t2699442136_0_0_0 = { 1, GenInst_Font_t2699442136_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutOption_t721761240_0_0_0_Types[] = { (&GUILayoutOption_t721761240_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t721761240_0_0_0 = { 1, GenInst_GUILayoutOption_t721761240_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutEntry_t240640878_0_0_0_Types[] = { (&GUILayoutEntry_t240640878_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t240640878_0_0_0 = { 1, GenInst_GUILayoutEntry_t240640878_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&LayoutCache_t1385791520_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0 = { 2, GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2171644578_0_0_0_Types[] = { (&KeyValuePair_2_t2171644578_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2171644578_0_0_0 = { 1, GenInst_KeyValuePair_2_t2171644578_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2171644578_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2171644578_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2171644578_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2171644578_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&LayoutCache_t1385791520_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_KeyValuePair_2_t3434924395_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&LayoutCache_t1385791520_0_0_0), (&KeyValuePair_2_t3434924395_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_KeyValuePair_2_t3434924395_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_KeyValuePair_2_t3434924395_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3434924395_0_0_0_Types[] = { (&KeyValuePair_2_t3434924395_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434924395_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434924395_0_0_0_Types };
static const RuntimeType* GenInst_GUIStyle_t2932200036_0_0_0_Types[] = { (&GUIStyle_t2932200036_0_0_0) };
extern const Il2CppGenericInst GenInst_GUIStyle_t2932200036_0_0_0 = { 1, GenInst_GUIStyle_t2932200036_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t2932200036_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t2932200036_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_KeyValuePair_2_t1538953807_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t2932200036_0_0_0), (&KeyValuePair_2_t1538953807_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_KeyValuePair_2_t1538953807_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_KeyValuePair_2_t1538953807_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1538953807_0_0_0_Types[] = { (&KeyValuePair_2_t1538953807_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1538953807_0_0_0 = { 1, GenInst_KeyValuePair_2_t1538953807_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_GUIStyle_t2932200036_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t2932200036_0_0_0), (&GUIStyle_t2932200036_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_GUIStyle_t2932200036_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_GUIStyle_t2932200036_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_IntPtr_t_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&IntPtr_t_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_IntPtr_t_0_0_0_Boolean_t362855854_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_IntPtr_t_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t82373287_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&Exception_t82373287_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t82373287_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_Exception_t82373287_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_UserProfile_t771069284_0_0_0_Types[] = { (&UserProfile_t771069284_0_0_0) };
extern const Il2CppGenericInst GenInst_UserProfile_t771069284_0_0_0 = { 1, GenInst_UserProfile_t771069284_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfile_t1988237155_0_0_0_Types[] = { (&IUserProfile_t1988237155_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfile_t1988237155_0_0_0 = { 1, GenInst_IUserProfile_t1988237155_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t362855854_0_0_0_String_t_0_0_0_Types[] = { (&Boolean_t362855854_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t362855854_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t362855854_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t362855854_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Boolean_t362855854_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t362855854_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Boolean_t362855854_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AchievementDescription_t433697264_0_0_0_Types[] = { (&AchievementDescription_t433697264_0_0_0) };
extern const Il2CppGenericInst GenInst_AchievementDescription_t433697264_0_0_0 = { 1, GenInst_AchievementDescription_t433697264_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescription_t1962024065_0_0_0_Types[] = { (&IAchievementDescription_t1962024065_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t1962024065_0_0_0 = { 1, GenInst_IAchievementDescription_t1962024065_0_0_0_Types };
static const RuntimeType* GenInst_GcLeaderboard_t374162520_0_0_0_Types[] = { (&GcLeaderboard_t374162520_0_0_0) };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t374162520_0_0_0 = { 1, GenInst_GcLeaderboard_t374162520_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescriptionU5BU5D_t857867356_0_0_0_Types[] = { (&IAchievementDescriptionU5BU5D_t857867356_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t857867356_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t857867356_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementU5BU5D_t2204500354_0_0_0_Types[] = { (&IAchievementU5BU5D_t2204500354_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2204500354_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2204500354_0_0_0_Types };
static const RuntimeType* GenInst_IAchievement_t1434617299_0_0_0_Types[] = { (&IAchievement_t1434617299_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievement_t1434617299_0_0_0 = { 1, GenInst_IAchievement_t1434617299_0_0_0_Types };
static const RuntimeType* GenInst_GcAchievementData_t1098514478_0_0_0_Types[] = { (&GcAchievementData_t1098514478_0_0_0) };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1098514478_0_0_0 = { 1, GenInst_GcAchievementData_t1098514478_0_0_0_Types };
static const RuntimeType* GenInst_Achievement_t1914343199_0_0_0_Types[] = { (&Achievement_t1914343199_0_0_0) };
extern const Il2CppGenericInst GenInst_Achievement_t1914343199_0_0_0 = { 1, GenInst_Achievement_t1914343199_0_0_0_Types };
static const RuntimeType* GenInst_IScoreU5BU5D_t754807597_0_0_0_Types[] = { (&IScoreU5BU5D_t754807597_0_0_0) };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t754807597_0_0_0 = { 1, GenInst_IScoreU5BU5D_t754807597_0_0_0_Types };
static const RuntimeType* GenInst_IScore_t2259084100_0_0_0_Types[] = { (&IScore_t2259084100_0_0_0) };
extern const Il2CppGenericInst GenInst_IScore_t2259084100_0_0_0 = { 1, GenInst_IScore_t2259084100_0_0_0_Types };
static const RuntimeType* GenInst_GcScoreData_t322958630_0_0_0_Types[] = { (&GcScoreData_t322958630_0_0_0) };
extern const Il2CppGenericInst GenInst_GcScoreData_t322958630_0_0_0 = { 1, GenInst_GcScoreData_t322958630_0_0_0_Types };
static const RuntimeType* GenInst_Score_t1824312173_0_0_0_Types[] = { (&Score_t1824312173_0_0_0) };
extern const Il2CppGenericInst GenInst_Score_t1824312173_0_0_0 = { 1, GenInst_Score_t1824312173_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfileU5BU5D_t1589946290_0_0_0_Types[] = { (&IUserProfileU5BU5D_t1589946290_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t1589946290_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t1589946290_0_0_0_Types };
static const RuntimeType* GenInst_DisallowMultipleComponent_t2877457562_0_0_0_Types[] = { (&DisallowMultipleComponent_t2877457562_0_0_0) };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2877457562_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2877457562_0_0_0_Types };
static const RuntimeType* GenInst_Attribute_t1842279184_0_0_0_Types[] = { (&Attribute_t1842279184_0_0_0) };
extern const Il2CppGenericInst GenInst_Attribute_t1842279184_0_0_0 = { 1, GenInst_Attribute_t1842279184_0_0_0_Types };
static const RuntimeType* GenInst__Attribute_t3546903852_0_0_0_Types[] = { (&_Attribute_t3546903852_0_0_0) };
extern const Il2CppGenericInst GenInst__Attribute_t3546903852_0_0_0 = { 1, GenInst__Attribute_t3546903852_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteInEditMode_t3420945569_0_0_0_Types[] = { (&ExecuteInEditMode_t3420945569_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3420945569_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3420945569_0_0_0_Types };
static const RuntimeType* GenInst_RequireComponent_t4227770712_0_0_0_Types[] = { (&RequireComponent_t4227770712_0_0_0) };
extern const Il2CppGenericInst GenInst_RequireComponent_t4227770712_0_0_0 = { 1, GenInst_RequireComponent_t4227770712_0_0_0_Types };
static const RuntimeType* GenInst_HitInfo_t2832651489_0_0_0_Types[] = { (&HitInfo_t2832651489_0_0_0) };
extern const Il2CppGenericInst GenInst_HitInfo_t2832651489_0_0_0 = { 1, GenInst_HitInfo_t2832651489_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_PersistentCall_t709146344_0_0_0_Types[] = { (&PersistentCall_t709146344_0_0_0) };
extern const Il2CppGenericInst GenInst_PersistentCall_t709146344_0_0_0 = { 1, GenInst_PersistentCall_t709146344_0_0_0_Types };
static const RuntimeType* GenInst_BaseInvokableCall_t3830733407_0_0_0_Types[] = { (&BaseInvokableCall_t3830733407_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t3830733407_0_0_0 = { 1, GenInst_BaseInvokableCall_t3830733407_0_0_0_Types };
static const RuntimeType* GenInst_WorkRequest_t886435312_0_0_0_Types[] = { (&WorkRequest_t886435312_0_0_0) };
extern const Il2CppGenericInst GenInst_WorkRequest_t886435312_0_0_0 = { 1, GenInst_WorkRequest_t886435312_0_0_0_Types };
static const RuntimeType* GenInst_PlayableBinding_t1499311357_0_0_0_Types[] = { (&PlayableBinding_t1499311357_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableBinding_t1499311357_0_0_0 = { 1, GenInst_PlayableBinding_t1499311357_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2879909665_0_0_0_Types[] = { (&MessageTypeSubscribers_t2879909665_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2879909665_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t2879909665_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2879909665_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&MessageTypeSubscribers_t2879909665_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2879909665_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t2879909665_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t3627585352_0_0_0_Types[] = { (&MessageEventArgs_t3627585352_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t3627585352_0_0_0 = { 1, GenInst_MessageEventArgs_t3627585352_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t245340424_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3430616441_0_0_0_Types[] = { (&KeyValuePair_2_t3430616441_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3430616441_0_0_0 = { 1, GenInst_KeyValuePair_2_t3430616441_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3430616441_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3430616441_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3430616441_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3430616441_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t245340424_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_KeyValuePair_2_t3553445162_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t245340424_0_0_0), (&KeyValuePair_2_t3553445162_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_KeyValuePair_2_t3553445162_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_KeyValuePair_2_t3553445162_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3553445162_0_0_0_Types[] = { (&KeyValuePair_2_t3553445162_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3553445162_0_0_0 = { 1, GenInst_KeyValuePair_2_t3553445162_0_0_0_Types };
static const RuntimeType* GenInst_BaseInputModule_t309635854_0_0_0_Types[] = { (&BaseInputModule_t309635854_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInputModule_t309635854_0_0_0 = { 1, GenInst_BaseInputModule_t309635854_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t1715322097_0_0_0_Types[] = { (&RaycastResult_t1715322097_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t1715322097_0_0_0 = { 1, GenInst_RaycastResult_t1715322097_0_0_0_Types };
static const RuntimeType* GenInst_IDeselectHandler_t3843174961_0_0_0_Types[] = { (&IDeselectHandler_t3843174961_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3843174961_0_0_0 = { 1, GenInst_IDeselectHandler_t3843174961_0_0_0_Types };
static const RuntimeType* GenInst_IEventSystemHandler_t558645447_0_0_0_Types[] = { (&IEventSystemHandler_t558645447_0_0_0) };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t558645447_0_0_0 = { 1, GenInst_IEventSystemHandler_t558645447_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3596965026_0_0_0_Types[] = { (&List_1_t3596965026_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3596965026_0_0_0 = { 1, GenInst_List_1_t3596965026_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3160831282_0_0_0_Types[] = { (&List_1_t3160831282_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3160831282_0_0_0 = { 1, GenInst_List_1_t3160831282_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3569798050_0_0_0_Types[] = { (&List_1_t3569798050_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3569798050_0_0_0 = { 1, GenInst_List_1_t3569798050_0_0_0_Types };
static const RuntimeType* GenInst_ISelectHandler_t2734039658_0_0_0_Types[] = { (&ISelectHandler_t2734039658_0_0_0) };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2734039658_0_0_0 = { 1, GenInst_ISelectHandler_t2734039658_0_0_0_Types };
static const RuntimeType* GenInst_BaseRaycaster_t3590231488_0_0_0_Types[] = { (&BaseRaycaster_t3590231488_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t3590231488_0_0_0 = { 1, GenInst_BaseRaycaster_t3590231488_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t3975632235_0_0_0_Types[] = { (&Entry_t3975632235_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t3975632235_0_0_0 = { 1, GenInst_Entry_t3975632235_0_0_0_Types };
static const RuntimeType* GenInst_BaseEventData_t341072110_0_0_0_Types[] = { (&BaseEventData_t341072110_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseEventData_t341072110_0_0_0 = { 1, GenInst_BaseEventData_t341072110_0_0_0_Types };
static const RuntimeType* GenInst_IPointerEnterHandler_t2566364723_0_0_0_Types[] = { (&IPointerEnterHandler_t2566364723_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t2566364723_0_0_0 = { 1, GenInst_IPointerEnterHandler_t2566364723_0_0_0_Types };
static const RuntimeType* GenInst_IPointerExitHandler_t1566539265_0_0_0_Types[] = { (&IPointerExitHandler_t1566539265_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t1566539265_0_0_0 = { 1, GenInst_IPointerExitHandler_t1566539265_0_0_0_Types };
static const RuntimeType* GenInst_IPointerDownHandler_t1810984543_0_0_0_Types[] = { (&IPointerDownHandler_t1810984543_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t1810984543_0_0_0 = { 1, GenInst_IPointerDownHandler_t1810984543_0_0_0_Types };
static const RuntimeType* GenInst_IPointerUpHandler_t2617963051_0_0_0_Types[] = { (&IPointerUpHandler_t2617963051_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t2617963051_0_0_0 = { 1, GenInst_IPointerUpHandler_t2617963051_0_0_0_Types };
static const RuntimeType* GenInst_IPointerClickHandler_t1471500927_0_0_0_Types[] = { (&IPointerClickHandler_t1471500927_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t1471500927_0_0_0 = { 1, GenInst_IPointerClickHandler_t1471500927_0_0_0_Types };
static const RuntimeType* GenInst_IInitializePotentialDragHandler_t2875812772_0_0_0_Types[] = { (&IInitializePotentialDragHandler_t2875812772_0_0_0) };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t2875812772_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t2875812772_0_0_0_Types };
static const RuntimeType* GenInst_IBeginDragHandler_t737688467_0_0_0_Types[] = { (&IBeginDragHandler_t737688467_0_0_0) };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t737688467_0_0_0 = { 1, GenInst_IBeginDragHandler_t737688467_0_0_0_Types };
static const RuntimeType* GenInst_IDragHandler_t1361425836_0_0_0_Types[] = { (&IDragHandler_t1361425836_0_0_0) };
extern const Il2CppGenericInst GenInst_IDragHandler_t1361425836_0_0_0 = { 1, GenInst_IDragHandler_t1361425836_0_0_0_Types };
static const RuntimeType* GenInst_IEndDragHandler_t492564000_0_0_0_Types[] = { (&IEndDragHandler_t492564000_0_0_0) };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t492564000_0_0_0 = { 1, GenInst_IEndDragHandler_t492564000_0_0_0_Types };
static const RuntimeType* GenInst_IDropHandler_t4181017068_0_0_0_Types[] = { (&IDropHandler_t4181017068_0_0_0) };
extern const Il2CppGenericInst GenInst_IDropHandler_t4181017068_0_0_0 = { 1, GenInst_IDropHandler_t4181017068_0_0_0_Types };
static const RuntimeType* GenInst_IScrollHandler_t2087319098_0_0_0_Types[] = { (&IScrollHandler_t2087319098_0_0_0) };
extern const Il2CppGenericInst GenInst_IScrollHandler_t2087319098_0_0_0 = { 1, GenInst_IScrollHandler_t2087319098_0_0_0_Types };
static const RuntimeType* GenInst_IUpdateSelectedHandler_t1291562379_0_0_0_Types[] = { (&IUpdateSelectedHandler_t1291562379_0_0_0) };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t1291562379_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t1291562379_0_0_0_Types };
static const RuntimeType* GenInst_IMoveHandler_t4033052693_0_0_0_Types[] = { (&IMoveHandler_t4033052693_0_0_0) };
extern const Il2CppGenericInst GenInst_IMoveHandler_t4033052693_0_0_0 = { 1, GenInst_IMoveHandler_t4033052693_0_0_0_Types };
static const RuntimeType* GenInst_ISubmitHandler_t234818760_0_0_0_Types[] = { (&ISubmitHandler_t234818760_0_0_0) };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t234818760_0_0_0 = { 1, GenInst_ISubmitHandler_t234818760_0_0_0_Types };
static const RuntimeType* GenInst_ICancelHandler_t516662923_0_0_0_Types[] = { (&ICancelHandler_t516662923_0_0_0) };
extern const Il2CppGenericInst GenInst_ICancelHandler_t516662923_0_0_0 = { 1, GenInst_ICancelHandler_t516662923_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t3933397867_0_0_0_Types[] = { (&Transform_t3933397867_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t3933397867_0_0_0 = { 1, GenInst_Transform_t3933397867_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_t3649338848_0_0_0_Types[] = { (&GameObject_t3649338848_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_t3649338848_0_0_0 = { 1, GenInst_GameObject_t3649338848_0_0_0_Types };
static const RuntimeType* GenInst_BaseInput_t3666734484_0_0_0_Types[] = { (&BaseInput_t3666734484_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInput_t3666734484_0_0_0 = { 1, GenInst_BaseInput_t3666734484_0_0_0_Types };
static const RuntimeType* GenInst_UIBehaviour_t2441083007_0_0_0_Types[] = { (&UIBehaviour_t2441083007_0_0_0) };
extern const Il2CppGenericInst GenInst_UIBehaviour_t2441083007_0_0_0 = { 1, GenInst_UIBehaviour_t2441083007_0_0_0_Types };
static const RuntimeType* GenInst_MonoBehaviour_t1096588306_0_0_0_Types[] = { (&MonoBehaviour_t1096588306_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1096588306_0_0_0 = { 1, GenInst_MonoBehaviour_t1096588306_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&PointerEventData_t1563322019_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0 = { 2, GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&PointerEventData_t1563322019_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_KeyValuePair_2_t3612454894_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&PointerEventData_t1563322019_0_0_0), (&KeyValuePair_2_t3612454894_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_KeyValuePair_2_t3612454894_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_KeyValuePair_2_t3612454894_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3612454894_0_0_0_Types[] = { (&KeyValuePair_2_t3612454894_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3612454894_0_0_0 = { 1, GenInst_KeyValuePair_2_t3612454894_0_0_0_Types };
static const RuntimeType* GenInst_PointerEventData_t1563322019_0_0_0_Types[] = { (&PointerEventData_t1563322019_0_0_0) };
extern const Il2CppGenericInst GenInst_PointerEventData_t1563322019_0_0_0 = { 1, GenInst_PointerEventData_t1563322019_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_PointerEventData_t1563322019_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&PointerEventData_t1563322019_0_0_0), (&PointerEventData_t1563322019_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_PointerEventData_t1563322019_0_0_0 = { 3, GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_PointerEventData_t1563322019_0_0_0_Types };
static const RuntimeType* GenInst_ButtonState_t4214419048_0_0_0_Types[] = { (&ButtonState_t4214419048_0_0_0) };
extern const Il2CppGenericInst GenInst_ButtonState_t4214419048_0_0_0 = { 1, GenInst_ButtonState_t4214419048_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit2D_t2323693239_0_0_0_Types[] = { (&RaycastHit2D_t2323693239_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t2323693239_0_0_0 = { 1, GenInst_RaycastHit2D_t2323693239_0_0_0_Types };
static const RuntimeType* GenInst_Color_t320819310_0_0_0_Types[] = { (&Color_t320819310_0_0_0) };
extern const Il2CppGenericInst GenInst_Color_t320819310_0_0_0 = { 1, GenInst_Color_t320819310_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t803896971_0_0_0_Types[] = { (&ICanvasElement_t803896971_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t803896971_0_0_0 = { 1, GenInst_ICanvasElement_t803896971_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_Types[] = { (&ICanvasElement_t803896971_0_0_0), (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0 = { 2, GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&ICanvasElement_t803896971_0_0_0), (&Int32_t3425510919_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_ColorBlock_t625039033_0_0_0_Types[] = { (&ColorBlock_t625039033_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorBlock_t625039033_0_0_0 = { 1, GenInst_ColorBlock_t625039033_0_0_0_Types };
static const RuntimeType* GenInst_OptionData_t445311210_0_0_0_Types[] = { (&OptionData_t445311210_0_0_0) };
extern const Il2CppGenericInst GenInst_OptionData_t445311210_0_0_0 = { 1, GenInst_OptionData_t445311210_0_0_0_Types };
static const RuntimeType* GenInst_DropdownItem_t953411360_0_0_0_Types[] = { (&DropdownItem_t953411360_0_0_0) };
extern const Il2CppGenericInst GenInst_DropdownItem_t953411360_0_0_0 = { 1, GenInst_DropdownItem_t953411360_0_0_0_Types };
static const RuntimeType* GenInst_FloatTween_t1459678735_0_0_0_Types[] = { (&FloatTween_t1459678735_0_0_0) };
extern const Il2CppGenericInst GenInst_FloatTween_t1459678735_0_0_0 = { 1, GenInst_FloatTween_t1459678735_0_0_0_Types };
static const RuntimeType* GenInst_Sprite_t1309550511_0_0_0_Types[] = { (&Sprite_t1309550511_0_0_0) };
extern const Il2CppGenericInst GenInst_Sprite_t1309550511_0_0_0 = { 1, GenInst_Sprite_t1309550511_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t3680465181_0_0_0_Types[] = { (&Canvas_t3680465181_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t3680465181_0_0_0 = { 1, GenInst_Canvas_t3680465181_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2423817464_0_0_0_Types[] = { (&List_1_t2423817464_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2423817464_0_0_0 = { 1, GenInst_List_1_t2423817464_0_0_0_Types };
static const RuntimeType* GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_Types[] = { (&Font_t2699442136_0_0_0), (&HashSet_1_t3994364505_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0 = { 2, GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_Types };
static const RuntimeType* GenInst_Text_t3069741234_0_0_0_Types[] = { (&Text_t3069741234_0_0_0) };
extern const Il2CppGenericInst GenInst_Text_t3069741234_0_0_0 = { 1, GenInst_Text_t3069741234_0_0_0_Types };
static const RuntimeType* GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&Font_t2699442136_0_0_0), (&HashSet_1_t3994364505_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_KeyValuePair_2_t3253400431_0_0_0_Types[] = { (&Font_t2699442136_0_0_0), (&HashSet_1_t3994364505_0_0_0), (&KeyValuePair_2_t3253400431_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_KeyValuePair_2_t3253400431_0_0_0 = { 3, GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_KeyValuePair_2_t3253400431_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3253400431_0_0_0_Types[] = { (&KeyValuePair_2_t3253400431_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3253400431_0_0_0 = { 1, GenInst_KeyValuePair_2_t3253400431_0_0_0_Types };
static const RuntimeType* GenInst_ColorTween_t1347433457_0_0_0_Types[] = { (&ColorTween_t1347433457_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorTween_t1347433457_0_0_0 = { 1, GenInst_ColorTween_t1347433457_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t1406460313_0_0_0_Types[] = { (&Graphic_t1406460313_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t1406460313_0_0_0 = { 1, GenInst_Graphic_t1406460313_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_Types[] = { (&Canvas_t3680465181_0_0_0), (&IndexedSet_1_t2830149342_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0 = { 2, GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_Types[] = { (&Graphic_t1406460313_0_0_0), (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0 = { 2, GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&Graphic_t1406460313_0_0_0), (&Int32_t3425510919_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&Canvas_t3680465181_0_0_0), (&IndexedSet_1_t2830149342_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_KeyValuePair_2_t2983860219_0_0_0_Types[] = { (&Canvas_t3680465181_0_0_0), (&IndexedSet_1_t2830149342_0_0_0), (&KeyValuePair_2_t2983860219_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_KeyValuePair_2_t2983860219_0_0_0 = { 3, GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_KeyValuePair_2_t2983860219_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2983860219_0_0_0_Types[] = { (&KeyValuePair_2_t2983860219_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2983860219_0_0_0 = { 1, GenInst_KeyValuePair_2_t2983860219_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t2618575160_0_0_0_Types[] = { (&Graphic_t1406460313_0_0_0), (&Int32_t3425510919_0_0_0), (&KeyValuePair_2_t2618575160_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t2618575160_0_0_0 = { 3, GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t2618575160_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2618575160_0_0_0_Types[] = { (&KeyValuePair_2_t2618575160_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2618575160_0_0_0 = { 1, GenInst_KeyValuePair_2_t2618575160_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t1278652478_0_0_0_Types[] = { (&ICanvasElement_t803896971_0_0_0), (&Int32_t3425510919_0_0_0), (&KeyValuePair_2_t1278652478_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t1278652478_0_0_0 = { 3, GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t1278652478_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1278652478_0_0_0_Types[] = { (&KeyValuePair_2_t1278652478_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1278652478_0_0_0 = { 1, GenInst_KeyValuePair_2_t1278652478_0_0_0_Types };
static const RuntimeType* GenInst_Type_t125599537_0_0_0_Types[] = { (&Type_t125599537_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t125599537_0_0_0 = { 1, GenInst_Type_t125599537_0_0_0_Types };
static const RuntimeType* GenInst_FillMethod_t46703468_0_0_0_Types[] = { (&FillMethod_t46703468_0_0_0) };
extern const Il2CppGenericInst GenInst_FillMethod_t46703468_0_0_0 = { 1, GenInst_FillMethod_t46703468_0_0_0_Types };
static const RuntimeType* GenInst_ContentType_t2687439383_0_0_0_Types[] = { (&ContentType_t2687439383_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentType_t2687439383_0_0_0 = { 1, GenInst_ContentType_t2687439383_0_0_0_Types };
static const RuntimeType* GenInst_LineType_t4134456689_0_0_0_Types[] = { (&LineType_t4134456689_0_0_0) };
extern const Il2CppGenericInst GenInst_LineType_t4134456689_0_0_0 = { 1, GenInst_LineType_t4134456689_0_0_0_Types };
static const RuntimeType* GenInst_InputType_t3397888567_0_0_0_Types[] = { (&InputType_t3397888567_0_0_0) };
extern const Il2CppGenericInst GenInst_InputType_t3397888567_0_0_0 = { 1, GenInst_InputType_t3397888567_0_0_0_Types };
static const RuntimeType* GenInst_TouchScreenKeyboardType_t491524606_0_0_0_Types[] = { (&TouchScreenKeyboardType_t491524606_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t491524606_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t491524606_0_0_0_Types };
static const RuntimeType* GenInst_CharacterValidation_t2578987709_0_0_0_Types[] = { (&CharacterValidation_t2578987709_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterValidation_t2578987709_0_0_0 = { 1, GenInst_CharacterValidation_t2578987709_0_0_0_Types };
static const RuntimeType* GenInst_Mask_t3495621644_0_0_0_Types[] = { (&Mask_t3495621644_0_0_0) };
extern const Il2CppGenericInst GenInst_Mask_t3495621644_0_0_0 = { 1, GenInst_Mask_t3495621644_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2238973927_0_0_0_Types[] = { (&List_1_t2238973927_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2238973927_0_0_0 = { 1, GenInst_List_1_t2238973927_0_0_0_Types };
static const RuntimeType* GenInst_RectMask2D_t2103047313_0_0_0_Types[] = { (&RectMask2D_t2103047313_0_0_0) };
extern const Il2CppGenericInst GenInst_RectMask2D_t2103047313_0_0_0 = { 1, GenInst_RectMask2D_t2103047313_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t846399596_0_0_0_Types[] = { (&List_1_t846399596_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t846399596_0_0_0 = { 1, GenInst_List_1_t846399596_0_0_0_Types };
static const RuntimeType* GenInst_Navigation_t2887492880_0_0_0_Types[] = { (&Navigation_t2887492880_0_0_0) };
extern const Il2CppGenericInst GenInst_Navigation_t2887492880_0_0_0 = { 1, GenInst_Navigation_t2887492880_0_0_0_Types };
static const RuntimeType* GenInst_IClippable_t2263516411_0_0_0_Types[] = { (&IClippable_t2263516411_0_0_0) };
extern const Il2CppGenericInst GenInst_IClippable_t2263516411_0_0_0 = { 1, GenInst_IClippable_t2263516411_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t1877520305_0_0_0_Types[] = { (&Direction_t1877520305_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t1877520305_0_0_0 = { 1, GenInst_Direction_t1877520305_0_0_0_Types };
static const RuntimeType* GenInst_Selectable_t3890617260_0_0_0_Types[] = { (&Selectable_t3890617260_0_0_0) };
extern const Il2CppGenericInst GenInst_Selectable_t3890617260_0_0_0 = { 1, GenInst_Selectable_t3890617260_0_0_0_Types };
static const RuntimeType* GenInst_Transition_t327687789_0_0_0_Types[] = { (&Transition_t327687789_0_0_0) };
extern const Il2CppGenericInst GenInst_Transition_t327687789_0_0_0 = { 1, GenInst_Transition_t327687789_0_0_0_Types };
static const RuntimeType* GenInst_SpriteState_t758977253_0_0_0_Types[] = { (&SpriteState_t758977253_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteState_t758977253_0_0_0 = { 1, GenInst_SpriteState_t758977253_0_0_0_Types };
static const RuntimeType* GenInst_CanvasGroup_t3075965541_0_0_0_Types[] = { (&CanvasGroup_t3075965541_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3075965541_0_0_0 = { 1, GenInst_CanvasGroup_t3075965541_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t1918642359_0_0_0_Types[] = { (&Direction_t1918642359_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t1918642359_0_0_0 = { 1, GenInst_Direction_t1918642359_0_0_0_Types };
static const RuntimeType* GenInst_MatEntry_t4139921362_0_0_0_Types[] = { (&MatEntry_t4139921362_0_0_0) };
extern const Il2CppGenericInst GenInst_MatEntry_t4139921362_0_0_0 = { 1, GenInst_MatEntry_t4139921362_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t1116311230_0_0_0_Types[] = { (&Toggle_t1116311230_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t1116311230_0_0_0 = { 1, GenInst_Toggle_t1116311230_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t1116311230_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&Toggle_t1116311230_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t1116311230_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_Toggle_t1116311230_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t1468079658_0_0_0_Types[] = { (&IClipper_t1468079658_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t1468079658_0_0_0 = { 1, GenInst_IClipper_t1468079658_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_Types[] = { (&IClipper_t1468079658_0_0_0), (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0 = { 2, GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&IClipper_t1468079658_0_0_0), (&Int32_t3425510919_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t3227114307_0_0_0_Types[] = { (&IClipper_t1468079658_0_0_0), (&Int32_t3425510919_0_0_0), (&KeyValuePair_2_t3227114307_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t3227114307_0_0_0 = { 3, GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t3227114307_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3227114307_0_0_0_Types[] = { (&KeyValuePair_2_t3227114307_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3227114307_0_0_0 = { 1, GenInst_KeyValuePair_2_t3227114307_0_0_0_Types };
static const RuntimeType* GenInst_AspectMode_t3157395208_0_0_0_Types[] = { (&AspectMode_t3157395208_0_0_0) };
extern const Il2CppGenericInst GenInst_AspectMode_t3157395208_0_0_0 = { 1, GenInst_AspectMode_t3157395208_0_0_0_Types };
static const RuntimeType* GenInst_FitMode_t760495087_0_0_0_Types[] = { (&FitMode_t760495087_0_0_0) };
extern const Il2CppGenericInst GenInst_FitMode_t760495087_0_0_0 = { 1, GenInst_FitMode_t760495087_0_0_0_Types };
static const RuntimeType* GenInst_RectTransform_t1885177139_0_0_0_Types[] = { (&RectTransform_t1885177139_0_0_0) };
extern const Il2CppGenericInst GenInst_RectTransform_t1885177139_0_0_0 = { 1, GenInst_RectTransform_t1885177139_0_0_0_Types };
static const RuntimeType* GenInst_LayoutRebuilder_t1578890533_0_0_0_Types[] = { (&LayoutRebuilder_t1578890533_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t1578890533_0_0_0 = { 1, GenInst_LayoutRebuilder_t1578890533_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t2907498299_0_0_0_Single_t2847614712_0_0_0_Types[] = { (&ILayoutElement_t2907498299_0_0_0), (&Single_t2847614712_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t2907498299_0_0_0_Single_t2847614712_0_0_0 = { 2, GenInst_ILayoutElement_t2907498299_0_0_0_Single_t2847614712_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t2847614712_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t2847614712_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t2847614712_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Single_t2847614712_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3635081580_0_0_0_Types[] = { (&List_1_t3635081580_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3635081580_0_0_0 = { 1, GenInst_List_1_t3635081580_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1242918311_0_0_0_Types[] = { (&List_1_t1242918311_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1242918311_0_0_0 = { 1, GenInst_List_1_t1242918311_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3097844061_0_0_0_Types[] = { (&List_1_t3097844061_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3097844061_0_0_0 = { 1, GenInst_List_1_t3097844061_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t120278507_0_0_0_Types[] = { (&List_1_t120278507_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t120278507_0_0_0 = { 1, GenInst_List_1_t120278507_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2168863202_0_0_0_Types[] = { (&List_1_t2168863202_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2168863202_0_0_0 = { 1, GenInst_List_1_t2168863202_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1415731117_0_0_0_Types[] = { (&List_1_t1415731117_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1415731117_0_0_0 = { 1, GenInst_List_1_t1415731117_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResult_t1803723679_0_0_0_Types[] = { (&ARHitTestResult_t1803723679_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResult_t1803723679_0_0_0 = { 1, GenInst_ARHitTestResult_t1803723679_0_0_0_Types };
static const RuntimeType* GenInst_ARPlaneAnchorGameObject_t3146329710_0_0_0_Types[] = { (&ARPlaneAnchorGameObject_t3146329710_0_0_0) };
extern const Il2CppGenericInst GenInst_ARPlaneAnchorGameObject_t3146329710_0_0_0 = { 1, GenInst_ARPlaneAnchorGameObject_t3146329710_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResultType_t270086150_0_0_0_Types[] = { (&ARHitTestResultType_t270086150_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResultType_t270086150_0_0_0 = { 1, GenInst_ARHitTestResultType_t270086150_0_0_0_Types };
static const RuntimeType* GenInst_UnityARSessionRunOption_t2337414342_0_0_0_Types[] = { (&UnityARSessionRunOption_t2337414342_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARSessionRunOption_t2337414342_0_0_0 = { 1, GenInst_UnityARSessionRunOption_t2337414342_0_0_0_Types };
static const RuntimeType* GenInst_UnityARAlignment_t1226993326_0_0_0_Types[] = { (&UnityARAlignment_t1226993326_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARAlignment_t1226993326_0_0_0 = { 1, GenInst_UnityARAlignment_t1226993326_0_0_0_Types };
static const RuntimeType* GenInst_UnityARPlaneDetection_t2335225179_0_0_0_Types[] = { (&UnityARPlaneDetection_t2335225179_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARPlaneDetection_t2335225179_0_0_0 = { 1, GenInst_UnityARPlaneDetection_t2335225179_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t3146329710_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0 = { 2, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t3146329710_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_KeyValuePair_2_t1753083481_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t3146329710_0_0_0), (&KeyValuePair_2_t1753083481_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_KeyValuePair_2_t1753083481_0_0_0 = { 3, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_KeyValuePair_2_t1753083481_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1753083481_0_0_0_Types[] = { (&KeyValuePair_2_t1753083481_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1753083481_0_0_0 = { 1, GenInst_KeyValuePair_2_t1753083481_0_0_0_Types };
static const RuntimeType* GenInst_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0_Types[] = { (&Single_t2847614712_0_0_0), (&Single_t2847614712_0_0_0), (&Single_t2847614712_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0 = { 3, GenInst_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0_Types };
static const RuntimeType* GenInst_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0_Types[] = { (&Single_t2847614712_0_0_0), (&Single_t2847614712_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0 = { 2, GenInst_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0_Types };
static const RuntimeType* GenInst_ParticleSystem_t1777616458_0_0_0_Types[] = { (&ParticleSystem_t1777616458_0_0_0) };
extern const Il2CppGenericInst GenInst_ParticleSystem_t1777616458_0_0_0 = { 1, GenInst_ParticleSystem_t1777616458_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1371926219_gp_0_0_0_0_Types[] = { (&IEnumerable_1_t1371926219_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1371926219_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t1371926219_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m4218696652_gp_0_0_0_0_Types[] = { (&Array_InternalArray__IEnumerable_GetEnumerator_m4218696652_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m4218696652_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m4218696652_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3424865288_gp_0_0_0_0_Array_Sort_m3424865288_gp_0_0_0_0_Types[] = { (&Array_Sort_m3424865288_gp_0_0_0_0), (&Array_Sort_m3424865288_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3424865288_gp_0_0_0_0_Array_Sort_m3424865288_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3424865288_gp_0_0_0_0_Array_Sort_m3424865288_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1174453676_gp_0_0_0_0_Array_Sort_m1174453676_gp_1_0_0_0_Types[] = { (&Array_Sort_m1174453676_gp_0_0_0_0), (&Array_Sort_m1174453676_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1174453676_gp_0_0_0_0_Array_Sort_m1174453676_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1174453676_gp_0_0_0_0_Array_Sort_m1174453676_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m904955079_gp_0_0_0_0_Types[] = { (&Array_Sort_m904955079_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m904955079_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m904955079_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m904955079_gp_0_0_0_0_Array_Sort_m904955079_gp_0_0_0_0_Types[] = { (&Array_Sort_m904955079_gp_0_0_0_0), (&Array_Sort_m904955079_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m904955079_gp_0_0_0_0_Array_Sort_m904955079_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m904955079_gp_0_0_0_0_Array_Sort_m904955079_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m224785917_gp_0_0_0_0_Types[] = { (&Array_Sort_m224785917_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m224785917_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m224785917_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m224785917_gp_0_0_0_0_Array_Sort_m224785917_gp_1_0_0_0_Types[] = { (&Array_Sort_m224785917_gp_0_0_0_0), (&Array_Sort_m224785917_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m224785917_gp_0_0_0_0_Array_Sort_m224785917_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m224785917_gp_0_0_0_0_Array_Sort_m224785917_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3326076588_gp_0_0_0_0_Array_Sort_m3326076588_gp_0_0_0_0_Types[] = { (&Array_Sort_m3326076588_gp_0_0_0_0), (&Array_Sort_m3326076588_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3326076588_gp_0_0_0_0_Array_Sort_m3326076588_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3326076588_gp_0_0_0_0_Array_Sort_m3326076588_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2780446043_gp_0_0_0_0_Array_Sort_m2780446043_gp_1_0_0_0_Types[] = { (&Array_Sort_m2780446043_gp_0_0_0_0), (&Array_Sort_m2780446043_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2780446043_gp_0_0_0_0_Array_Sort_m2780446043_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2780446043_gp_0_0_0_0_Array_Sort_m2780446043_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2417833379_gp_0_0_0_0_Types[] = { (&Array_Sort_m2417833379_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2417833379_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2417833379_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2417833379_gp_0_0_0_0_Array_Sort_m2417833379_gp_0_0_0_0_Types[] = { (&Array_Sort_m2417833379_gp_0_0_0_0), (&Array_Sort_m2417833379_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2417833379_gp_0_0_0_0_Array_Sort_m2417833379_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2417833379_gp_0_0_0_0_Array_Sort_m2417833379_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1373481026_gp_0_0_0_0_Types[] = { (&Array_Sort_m1373481026_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1373481026_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1373481026_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1373481026_gp_1_0_0_0_Types[] = { (&Array_Sort_m1373481026_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1373481026_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1373481026_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1373481026_gp_0_0_0_0_Array_Sort_m1373481026_gp_1_0_0_0_Types[] = { (&Array_Sort_m1373481026_gp_0_0_0_0), (&Array_Sort_m1373481026_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1373481026_gp_0_0_0_0_Array_Sort_m1373481026_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1373481026_gp_0_0_0_0_Array_Sort_m1373481026_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1201249205_gp_0_0_0_0_Types[] = { (&Array_Sort_m1201249205_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1201249205_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1201249205_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3696811223_gp_0_0_0_0_Types[] = { (&Array_Sort_m3696811223_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3696811223_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3696811223_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m3347095892_gp_0_0_0_0_Types[] = { (&Array_qsort_m3347095892_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m3347095892_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3347095892_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m3347095892_gp_0_0_0_0_Array_qsort_m3347095892_gp_1_0_0_0_Types[] = { (&Array_qsort_m3347095892_gp_0_0_0_0), (&Array_qsort_m3347095892_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m3347095892_gp_0_0_0_0_Array_qsort_m3347095892_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m3347095892_gp_0_0_0_0_Array_qsort_m3347095892_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_compare_m286771913_gp_0_0_0_0_Types[] = { (&Array_compare_m286771913_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_compare_m286771913_gp_0_0_0_0 = { 1, GenInst_Array_compare_m286771913_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m1529647689_gp_0_0_0_0_Types[] = { (&Array_qsort_m1529647689_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m1529647689_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m1529647689_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Resize_m2542074053_gp_0_0_0_0_Types[] = { (&Array_Resize_m2542074053_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Resize_m2542074053_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m2542074053_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_TrueForAll_m2493260605_gp_0_0_0_0_Types[] = { (&Array_TrueForAll_m2493260605_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2493260605_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2493260605_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ForEach_m3467030689_gp_0_0_0_0_Types[] = { (&Array_ForEach_m3467030689_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3467030689_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3467030689_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ConvertAll_m1694469864_gp_0_0_0_0_Array_ConvertAll_m1694469864_gp_1_0_0_0_Types[] = { (&Array_ConvertAll_m1694469864_gp_0_0_0_0), (&Array_ConvertAll_m1694469864_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1694469864_gp_0_0_0_0_Array_ConvertAll_m1694469864_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1694469864_gp_0_0_0_0_Array_ConvertAll_m1694469864_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m4135284164_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m4135284164_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m4135284164_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m4135284164_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m1527152247_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m1527152247_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1527152247_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1527152247_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m4002081672_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m4002081672_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m4002081672_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m4002081672_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m17293001_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m17293001_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m17293001_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m17293001_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m936277329_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m936277329_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m936277329_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m936277329_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m178503657_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m178503657_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m178503657_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m178503657_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m557100490_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m557100490_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m557100490_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m557100490_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3954992392_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3954992392_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3954992392_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3954992392_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m1167857953_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m1167857953_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1167857953_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1167857953_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3918896244_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3918896244_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3918896244_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3918896244_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m758192846_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m758192846_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m758192846_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m758192846_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m1794394959_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m1794394959_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1794394959_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1794394959_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m315707683_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m315707683_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m315707683_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m315707683_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m2029688934_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m2029688934_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2029688934_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2029688934_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m3148242585_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m3148242585_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3148242585_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3148242585_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m1969981718_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m1969981718_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1969981718_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1969981718_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindAll_m1088630835_gp_0_0_0_0_Types[] = { (&Array_FindAll_m1088630835_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1088630835_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1088630835_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Exists_m2063596975_gp_0_0_0_0_Types[] = { (&Array_Exists_m2063596975_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Exists_m2063596975_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m2063596975_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_AsReadOnly_m2584618994_gp_0_0_0_0_Types[] = { (&Array_AsReadOnly_m2584618994_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m2584618994_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m2584618994_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Find_m2415417361_gp_0_0_0_0_Types[] = { (&Array_Find_m2415417361_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Find_m2415417361_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2415417361_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLast_m3619283605_gp_0_0_0_0_Types[] = { (&Array_FindLast_m3619283605_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3619283605_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3619283605_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InternalEnumerator_1_t15510975_gp_0_0_0_0_Types[] = { (&InternalEnumerator_1_t15510975_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t15510975_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t15510975_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArrayReadOnlyList_1_t5340530_gp_0_0_0_0_Types[] = { (&ArrayReadOnlyList_1_t5340530_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t5340530_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t5340530_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3976382471_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator0_t3976382471_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3976382471_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3976382471_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1284029119_gp_0_0_0_0_Types[] = { (&IList_1_t1284029119_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1284029119_gp_0_0_0_0 = { 1, GenInst_IList_1_t1284029119_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3388121722_gp_0_0_0_0_Types[] = { (&ICollection_1_t3388121722_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3388121722_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t3388121722_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Nullable_1_t739790191_gp_0_0_0_0_Types[] = { (&Nullable_1_t739790191_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Nullable_1_t739790191_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t739790191_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Comparer_1_t2195447696_gp_0_0_0_0_Types[] = { (&Comparer_1_t2195447696_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Comparer_1_t2195447696_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t2195447696_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t989032782_gp_0_0_0_0_Types[] = { (&DefaultComparer_t989032782_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t989032782_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t989032782_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericComparer_1_t3293245800_gp_0_0_0_0_Types[] = { (&GenericComparer_1_t3293245800_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t3293245800_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t3293245800_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Types[] = { (&Dictionary_2_t681358595_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t681358595_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Types[] = { (&Dictionary_2_t681358595_gp_0_0_0_0), (&Dictionary_2_t681358595_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1779626263_0_0_0_Types[] = { (&KeyValuePair_2_t1779626263_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1779626263_0_0_0 = { 1, GenInst_KeyValuePair_2_t1779626263_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m37576101_gp_0_0_0_0_Types[] = { (&Dictionary_2_t681358595_gp_0_0_0_0), (&Dictionary_2_t681358595_gp_1_0_0_0), (&Dictionary_2_Do_CopyTo_m37576101_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m37576101_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m37576101_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0_Types[] = { (&Dictionary_2_t681358595_gp_0_0_0_0), (&Dictionary_2_t681358595_gp_1_0_0_0), (&Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&Dictionary_2_t681358595_gp_0_0_0_0), (&Dictionary_2_t681358595_gp_1_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 3, GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_ShimEnumerator_t1842228008_gp_0_0_0_0_ShimEnumerator_t1842228008_gp_1_0_0_0_Types[] = { (&ShimEnumerator_t1842228008_gp_0_0_0_0), (&ShimEnumerator_t1842228008_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t1842228008_gp_0_0_0_0_ShimEnumerator_t1842228008_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t1842228008_gp_0_0_0_0_ShimEnumerator_t1842228008_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3839420370_gp_0_0_0_0_Enumerator_t3839420370_gp_1_0_0_0_Types[] = { (&Enumerator_t3839420370_gp_0_0_0_0), (&Enumerator_t3839420370_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3839420370_gp_0_0_0_0_Enumerator_t3839420370_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3839420370_gp_0_0_0_0_Enumerator_t3839420370_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2283510011_0_0_0_Types[] = { (&KeyValuePair_2_t2283510011_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2283510011_0_0_0 = { 1, GenInst_KeyValuePair_2_t2283510011_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2793800625_gp_0_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_Types[] = { (&ValueCollection_t2793800625_gp_0_0_0_0), (&ValueCollection_t2793800625_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2793800625_gp_0_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2793800625_gp_0_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2793800625_gp_1_0_0_0_Types[] = { (&ValueCollection_t2793800625_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2793800625_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2793800625_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t760914474_gp_0_0_0_0_Enumerator_t760914474_gp_1_0_0_0_Types[] = { (&Enumerator_t760914474_gp_0_0_0_0), (&Enumerator_t760914474_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t760914474_gp_0_0_0_0_Enumerator_t760914474_gp_1_0_0_0 = { 2, GenInst_Enumerator_t760914474_gp_0_0_0_0_Enumerator_t760914474_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t760914474_gp_1_0_0_0_Types[] = { (&Enumerator_t760914474_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t760914474_gp_1_0_0_0 = { 1, GenInst_Enumerator_t760914474_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2793800625_gp_0_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_Types[] = { (&ValueCollection_t2793800625_gp_0_0_0_0), (&ValueCollection_t2793800625_gp_1_0_0_0), (&ValueCollection_t2793800625_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2793800625_gp_0_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2793800625_gp_0_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2793800625_gp_1_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_Types[] = { (&ValueCollection_t2793800625_gp_1_0_0_0), (&ValueCollection_t2793800625_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2793800625_gp_1_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2793800625_gp_1_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t3951334252_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types[] = { (&DictionaryEntry_t3951334252_0_0_0), (&DictionaryEntry_t3951334252_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3951334252_0_0_0_DictionaryEntry_t3951334252_0_0_0 = { 2, GenInst_DictionaryEntry_t3951334252_0_0_0_DictionaryEntry_t3951334252_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_KeyValuePair_2_t1779626263_0_0_0_Types[] = { (&Dictionary_2_t681358595_gp_0_0_0_0), (&Dictionary_2_t681358595_gp_1_0_0_0), (&KeyValuePair_2_t1779626263_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_KeyValuePair_2_t1779626263_0_0_0 = { 3, GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_KeyValuePair_2_t1779626263_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1779626263_0_0_0_KeyValuePair_2_t1779626263_0_0_0_Types[] = { (&KeyValuePair_2_t1779626263_0_0_0), (&KeyValuePair_2_t1779626263_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1779626263_0_0_0_KeyValuePair_2_t1779626263_0_0_0 = { 2, GenInst_KeyValuePair_2_t1779626263_0_0_0_KeyValuePair_2_t1779626263_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t681358595_gp_1_0_0_0_Types[] = { (&Dictionary_2_t681358595_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t681358595_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t681358595_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_EqualityComparer_1_t1195631860_gp_0_0_0_0_Types[] = { (&EqualityComparer_1_t1195631860_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t1195631860_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t1195631860_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t706283517_gp_0_0_0_0_Types[] = { (&DefaultComparer_t706283517_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t706283517_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t706283517_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericEqualityComparer_1_t3304566894_gp_0_0_0_0_Types[] = { (&GenericEqualityComparer_1_t3304566894_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t3304566894_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t3304566894_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t916714459_0_0_0_Types[] = { (&KeyValuePair_2_t916714459_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t916714459_0_0_0 = { 1, GenInst_KeyValuePair_2_t916714459_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t1884790490_gp_0_0_0_0_IDictionary_2_t1884790490_gp_1_0_0_0_Types[] = { (&IDictionary_2_t1884790490_gp_0_0_0_0), (&IDictionary_2_t1884790490_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1884790490_gp_0_0_0_0_IDictionary_2_t1884790490_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t1884790490_gp_0_0_0_0_IDictionary_2_t1884790490_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1574552141_gp_0_0_0_0_KeyValuePair_2_t1574552141_gp_1_0_0_0_Types[] = { (&KeyValuePair_2_t1574552141_gp_0_0_0_0), (&KeyValuePair_2_t1574552141_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1574552141_gp_0_0_0_0_KeyValuePair_2_t1574552141_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1574552141_gp_0_0_0_0_KeyValuePair_2_t1574552141_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t295223622_gp_0_0_0_0_Types[] = { (&List_1_t295223622_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t295223622_gp_0_0_0_0 = { 1, GenInst_List_1_t295223622_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2074507559_gp_0_0_0_0_Types[] = { (&Enumerator_t2074507559_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2074507559_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2074507559_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Collection_1_t3142256919_gp_0_0_0_0_Types[] = { (&Collection_1_t3142256919_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Collection_1_t3142256919_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3142256919_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ReadOnlyCollection_1_t914928874_gp_0_0_0_0_Types[] = { (&ReadOnlyCollection_1_t914928874_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t914928874_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t914928874_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_GetterAdapterFrame_m1404835840_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1404835840_gp_1_0_0_0_Types[] = { (&MonoProperty_GetterAdapterFrame_m1404835840_gp_0_0_0_0), (&MonoProperty_GetterAdapterFrame_m1404835840_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m1404835840_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1404835840_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m1404835840_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1404835840_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1727439354_gp_0_0_0_0_Types[] = { (&MonoProperty_StaticGetterAdapterFrame_m1727439354_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1727439354_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1727439354_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Queue_1_t1196217950_gp_0_0_0_0_Types[] = { (&Queue_1_t1196217950_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Queue_1_t1196217950_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1196217950_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2727315752_gp_0_0_0_0_Types[] = { (&Enumerator_t2727315752_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2727315752_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2727315752_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Stack_1_t696983570_gp_0_0_0_0_Types[] = { (&Stack_1_t696983570_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Stack_1_t696983570_gp_0_0_0_0 = { 1, GenInst_Stack_1_t696983570_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t812808600_gp_0_0_0_0_Types[] = { (&Enumerator_t812808600_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t812808600_gp_0_0_0_0 = { 1, GenInst_Enumerator_t812808600_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t1291849714_gp_0_0_0_0_Types[] = { (&HashSet_1_t1291849714_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t1291849714_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t1291849714_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1535822487_gp_0_0_0_0_Types[] = { (&Enumerator_t1535822487_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1535822487_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1535822487_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PrimeHelper_t3642281417_gp_0_0_0_0_Types[] = { (&PrimeHelper_t3642281417_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3642281417_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3642281417_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m2221973312_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m2221973312_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2221973312_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m2221973312_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ToList_m1954003453_gp_0_0_0_0_Types[] = { (&Enumerable_ToList_m1954003453_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m1954003453_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m1954003453_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m3550166720_gp_0_0_0_0_Types[] = { (&Enumerable_Where_m3550166720_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m3550166720_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m3550166720_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m3550166720_gp_0_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&Enumerable_Where_m3550166720_gp_0_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m3550166720_gp_0_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_Enumerable_Where_m3550166720_gp_0_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t467069803_gp_0_0_0_0_Types[] = { (&List_1_t467069803_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t467069803_gp_0_0_0_0 = { 1, GenInst_List_1_t467069803_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator6_t4041278014_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator6_t4041278014_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator6_t4041278014_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator6_t4041278014_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3967229796_0_0_0_Types[] = { (&List_1_t3967229796_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3967229796_0_0_0 = { 1, GenInst_List_1_t3967229796_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentInChildren_m3128998196_gp_0_0_0_0_Types[] = { (&Component_GetComponentInChildren_m3128998196_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3128998196_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3128998196_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m1790039706_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m1790039706_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1790039706_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1790039706_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m1997911980_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m1997911980_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1997911980_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1997911980_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m159208863_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m159208863_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m159208863_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m159208863_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m4223062209_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m4223062209_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m4223062209_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m4223062209_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m1689563606_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m1689563606_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1689563606_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1689563606_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m1885645319_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m1885645319_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1885645319_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1885645319_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m1546679232_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m1546679232_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1546679232_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1546679232_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m1602522827_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m1602522827_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m1602522827_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m1602522827_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m2981955009_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m2981955009_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2981955009_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2981955009_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentInChildren_m972597910_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentInChildren_m972597910_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m972597910_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m972597910_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponents_m3123327788_gp_0_0_0_0_Types[] = { (&GameObject_GetComponents_m3123327788_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m3123327788_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m3123327788_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m1146767846_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m1146767846_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1146767846_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1146767846_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInParent_m2891368970_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInParent_m2891368970_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m2891368970_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m2891368970_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_GetAllocArrayFromChannel_m4121023089_gp_0_0_0_0_Types[] = { (&Mesh_GetAllocArrayFromChannel_m4121023089_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m4121023089_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m4121023089_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SafeLength_m3772110265_gp_0_0_0_0_Types[] = { (&Mesh_SafeLength_m3772110265_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3772110265_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3772110265_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m4080854334_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m4080854334_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4080854334_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4080854334_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m2700408318_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m2700408318_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m2700408318_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m2700408318_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetUvsImpl_m1338450254_gp_0_0_0_0_Types[] = { (&Mesh_SetUvsImpl_m1338450254_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m1338450254_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m1338450254_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_Instantiate_m3278571309_gp_0_0_0_0_Types[] = { (&Object_Instantiate_m3278571309_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m3278571309_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m3278571309_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_FindObjectsOfType_m1189854602_gp_0_0_0_0_Types[] = { (&Object_FindObjectsOfType_m1189854602_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m1189854602_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m1189854602_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Playable_IsPlayableOfType_m2446752783_gp_0_0_0_0_Types[] = { (&Playable_IsPlayableOfType_m2446752783_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_IsPlayableOfType_m2446752783_gp_0_0_0_0 = { 1, GenInst_Playable_IsPlayableOfType_m2446752783_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_IsPlayableOutputOfType_m3414177984_gp_0_0_0_0_Types[] = { (&PlayableOutput_IsPlayableOutputOfType_m3414177984_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_IsPlayableOutputOfType_m3414177984_gp_0_0_0_0 = { 1, GenInst_PlayableOutput_IsPlayableOutputOfType_m3414177984_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_1_t1043888743_gp_0_0_0_0_Types[] = { (&InvokableCall_1_t1043888743_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t1043888743_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t1043888743_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_1_t2762766517_0_0_0_Types[] = { (&UnityAction_1_t2762766517_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2762766517_0_0_0 = { 1, GenInst_UnityAction_1_t2762766517_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t4283310926_gp_0_0_0_0_InvokableCall_2_t4283310926_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t4283310926_gp_0_0_0_0), (&InvokableCall_2_t4283310926_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t4283310926_gp_0_0_0_0_InvokableCall_2_t4283310926_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t4283310926_gp_0_0_0_0_InvokableCall_2_t4283310926_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_2_t846580519_0_0_0_Types[] = { (&UnityAction_2_t846580519_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_2_t846580519_0_0_0 = { 1, GenInst_UnityAction_2_t846580519_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t4283310926_gp_0_0_0_0_Types[] = { (&InvokableCall_2_t4283310926_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t4283310926_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t4283310926_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t4283310926_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t4283310926_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t4283310926_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t4283310926_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1407968758_gp_0_0_0_0_InvokableCall_3_t1407968758_gp_1_0_0_0_InvokableCall_3_t1407968758_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t1407968758_gp_0_0_0_0), (&InvokableCall_3_t1407968758_gp_1_0_0_0), (&InvokableCall_3_t1407968758_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1407968758_gp_0_0_0_0_InvokableCall_3_t1407968758_gp_1_0_0_0_InvokableCall_3_t1407968758_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t1407968758_gp_0_0_0_0_InvokableCall_3_t1407968758_gp_1_0_0_0_InvokableCall_3_t1407968758_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_3_t1541563382_0_0_0_Types[] = { (&UnityAction_3_t1541563382_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_3_t1541563382_0_0_0 = { 1, GenInst_UnityAction_3_t1541563382_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1407968758_gp_0_0_0_0_Types[] = { (&InvokableCall_3_t1407968758_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1407968758_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t1407968758_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1407968758_gp_1_0_0_0_Types[] = { (&InvokableCall_3_t1407968758_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1407968758_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t1407968758_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1407968758_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t1407968758_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1407968758_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t1407968758_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3522370476_gp_0_0_0_0_InvokableCall_4_t3522370476_gp_1_0_0_0_InvokableCall_4_t3522370476_gp_2_0_0_0_InvokableCall_4_t3522370476_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t3522370476_gp_0_0_0_0), (&InvokableCall_4_t3522370476_gp_1_0_0_0), (&InvokableCall_4_t3522370476_gp_2_0_0_0), (&InvokableCall_4_t3522370476_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3522370476_gp_0_0_0_0_InvokableCall_4_t3522370476_gp_1_0_0_0_InvokableCall_4_t3522370476_gp_2_0_0_0_InvokableCall_4_t3522370476_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3522370476_gp_0_0_0_0_InvokableCall_4_t3522370476_gp_1_0_0_0_InvokableCall_4_t3522370476_gp_2_0_0_0_InvokableCall_4_t3522370476_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3522370476_gp_0_0_0_0_Types[] = { (&InvokableCall_4_t3522370476_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3522370476_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3522370476_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3522370476_gp_1_0_0_0_Types[] = { (&InvokableCall_4_t3522370476_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3522370476_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3522370476_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3522370476_gp_2_0_0_0_Types[] = { (&InvokableCall_4_t3522370476_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3522370476_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3522370476_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3522370476_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t3522370476_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3522370476_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3522370476_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_CachedInvokableCall_1_t2169007495_gp_0_0_0_0_Types[] = { (&CachedInvokableCall_1_t2169007495_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t2169007495_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t2169007495_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_1_t2170633027_gp_0_0_0_0_Types[] = { (&UnityEvent_1_t2170633027_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t2170633027_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t2170633027_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_2_t2498946136_gp_0_0_0_0_UnityEvent_2_t2498946136_gp_1_0_0_0_Types[] = { (&UnityEvent_2_t2498946136_gp_0_0_0_0), (&UnityEvent_2_t2498946136_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t2498946136_gp_0_0_0_0_UnityEvent_2_t2498946136_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t2498946136_gp_0_0_0_0_UnityEvent_2_t2498946136_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_3_t3426698314_gp_0_0_0_0_UnityEvent_3_t3426698314_gp_1_0_0_0_UnityEvent_3_t3426698314_gp_2_0_0_0_Types[] = { (&UnityEvent_3_t3426698314_gp_0_0_0_0), (&UnityEvent_3_t3426698314_gp_1_0_0_0), (&UnityEvent_3_t3426698314_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t3426698314_gp_0_0_0_0_UnityEvent_3_t3426698314_gp_1_0_0_0_UnityEvent_3_t3426698314_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t3426698314_gp_0_0_0_0_UnityEvent_3_t3426698314_gp_1_0_0_0_UnityEvent_3_t3426698314_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_4_t1332048011_gp_0_0_0_0_UnityEvent_4_t1332048011_gp_1_0_0_0_UnityEvent_4_t1332048011_gp_2_0_0_0_UnityEvent_4_t1332048011_gp_3_0_0_0_Types[] = { (&UnityEvent_4_t1332048011_gp_0_0_0_0), (&UnityEvent_4_t1332048011_gp_1_0_0_0), (&UnityEvent_4_t1332048011_gp_2_0_0_0), (&UnityEvent_4_t1332048011_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t1332048011_gp_0_0_0_0_UnityEvent_4_t1332048011_gp_1_0_0_0_UnityEvent_4_t1332048011_gp_2_0_0_0_UnityEvent_4_t1332048011_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t1332048011_gp_0_0_0_0_UnityEvent_4_t1332048011_gp_1_0_0_0_UnityEvent_4_t1332048011_gp_2_0_0_0_UnityEvent_4_t1332048011_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_Execute_m3317274811_gp_0_0_0_0_Types[] = { (&ExecuteEvents_Execute_m3317274811_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m3317274811_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m3317274811_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_ExecuteHierarchy_m3880156326_gp_0_0_0_0_Types[] = { (&ExecuteEvents_ExecuteHierarchy_m3880156326_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m3880156326_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m3880156326_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventList_m3038861451_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventList_m3038861451_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m3038861451_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m3038861451_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_CanHandleEvent_m151454768_gp_0_0_0_0_Types[] = { (&ExecuteEvents_CanHandleEvent_m151454768_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m151454768_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m151454768_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventHandler_m1494251347_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventHandler_m1494251347_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m1494251347_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m1494251347_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TweenRunner_1_t726558302_gp_0_0_0_0_Types[] = { (&TweenRunner_1_t726558302_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t726558302_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t726558302_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_GetOrAddComponent_m3300878866_gp_0_0_0_0_Types[] = { (&Dropdown_GetOrAddComponent_m3300878866_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m3300878866_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m3300878866_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SetPropertyUtility_SetStruct_m3021317927_gp_0_0_0_0_Types[] = { (&SetPropertyUtility_SetStruct_m3021317927_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m3021317927_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m3021317927_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t2282855104_gp_0_0_0_0_Types[] = { (&IndexedSet_1_t2282855104_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2282855104_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t2282855104_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t2282855104_gp_0_0_0_0_Int32_t3425510919_0_0_0_Types[] = { (&IndexedSet_1_t2282855104_gp_0_0_0_0), (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2282855104_gp_0_0_0_0_Int32_t3425510919_0_0_0 = { 2, GenInst_IndexedSet_1_t2282855104_gp_0_0_0_0_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_ListPool_1_t681572707_gp_0_0_0_0_Types[] = { (&ListPool_1_t681572707_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ListPool_1_t681572707_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t681572707_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t72557439_0_0_0_Types[] = { (&List_1_t72557439_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t72557439_0_0_0 = { 1, GenInst_List_1_t72557439_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t3954034076_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t3954034076_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3954034076_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t3954034076_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPlayableOutput_t2577072500_0_0_0_Types[] = { (&AnimationPlayableOutput_t2577072500_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPlayableOutput_t2577072500_0_0_0 = { 1, GenInst_AnimationPlayableOutput_t2577072500_0_0_0_Types };
static const RuntimeType* GenInst_DefaultExecutionOrder_t3670034221_0_0_0_Types[] = { (&DefaultExecutionOrder_t3670034221_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t3670034221_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t3670034221_0_0_0_Types };
static const RuntimeType* GenInst_AudioPlayableOutput_t3404408964_0_0_0_Types[] = { (&AudioPlayableOutput_t3404408964_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioPlayableOutput_t3404408964_0_0_0 = { 1, GenInst_AudioPlayableOutput_t3404408964_0_0_0_Types };
static const RuntimeType* GenInst_PlayerConnection_t3806501584_0_0_0_Types[] = { (&PlayerConnection_t3806501584_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3806501584_0_0_0 = { 1, GenInst_PlayerConnection_t3806501584_0_0_0_Types };
static const RuntimeType* GenInst_ScriptPlayableOutput_t3124905912_0_0_0_Types[] = { (&ScriptPlayableOutput_t3124905912_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptPlayableOutput_t3124905912_0_0_0 = { 1, GenInst_ScriptPlayableOutput_t3124905912_0_0_0_Types };
static const RuntimeType* GenInst_GUILayer_t2573986518_0_0_0_Types[] = { (&GUILayer_t2573986518_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayer_t2573986518_0_0_0 = { 1, GenInst_GUILayer_t2573986518_0_0_0_Types };
static const RuntimeType* GenInst_EventSystem_t487304461_0_0_0_Types[] = { (&EventSystem_t487304461_0_0_0) };
extern const Il2CppGenericInst GenInst_EventSystem_t487304461_0_0_0 = { 1, GenInst_EventSystem_t487304461_0_0_0_Types };
static const RuntimeType* GenInst_AxisEventData_t3116767805_0_0_0_Types[] = { (&AxisEventData_t3116767805_0_0_0) };
extern const Il2CppGenericInst GenInst_AxisEventData_t3116767805_0_0_0 = { 1, GenInst_AxisEventData_t3116767805_0_0_0_Types };
static const RuntimeType* GenInst_SpriteRenderer_t621304888_0_0_0_Types[] = { (&SpriteRenderer_t621304888_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t621304888_0_0_0 = { 1, GenInst_SpriteRenderer_t621304888_0_0_0_Types };
static const RuntimeType* GenInst_Image_t126372159_0_0_0_Types[] = { (&Image_t126372159_0_0_0) };
extern const Il2CppGenericInst GenInst_Image_t126372159_0_0_0 = { 1, GenInst_Image_t126372159_0_0_0_Types };
static const RuntimeType* GenInst_Button_t467150838_0_0_0_Types[] = { (&Button_t467150838_0_0_0) };
extern const Il2CppGenericInst GenInst_Button_t467150838_0_0_0 = { 1, GenInst_Button_t467150838_0_0_0_Types };
static const RuntimeType* GenInst_RawImage_t3037409423_0_0_0_Types[] = { (&RawImage_t3037409423_0_0_0) };
extern const Il2CppGenericInst GenInst_RawImage_t3037409423_0_0_0 = { 1, GenInst_RawImage_t3037409423_0_0_0_Types };
static const RuntimeType* GenInst_Slider_t301630380_0_0_0_Types[] = { (&Slider_t301630380_0_0_0) };
extern const Il2CppGenericInst GenInst_Slider_t301630380_0_0_0 = { 1, GenInst_Slider_t301630380_0_0_0_Types };
static const RuntimeType* GenInst_Scrollbar_t1086108786_0_0_0_Types[] = { (&Scrollbar_t1086108786_0_0_0) };
extern const Il2CppGenericInst GenInst_Scrollbar_t1086108786_0_0_0 = { 1, GenInst_Scrollbar_t1086108786_0_0_0_Types };
static const RuntimeType* GenInst_InputField_t3161868630_0_0_0_Types[] = { (&InputField_t3161868630_0_0_0) };
extern const Il2CppGenericInst GenInst_InputField_t3161868630_0_0_0 = { 1, GenInst_InputField_t3161868630_0_0_0_Types };
static const RuntimeType* GenInst_ScrollRect_t371228710_0_0_0_Types[] = { (&ScrollRect_t371228710_0_0_0) };
extern const Il2CppGenericInst GenInst_ScrollRect_t371228710_0_0_0 = { 1, GenInst_ScrollRect_t371228710_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_t1637122921_0_0_0_Types[] = { (&Dropdown_t1637122921_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_t1637122921_0_0_0 = { 1, GenInst_Dropdown_t1637122921_0_0_0_Types };
static const RuntimeType* GenInst_GraphicRaycaster_t3396295912_0_0_0_Types[] = { (&GraphicRaycaster_t3396295912_0_0_0) };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t3396295912_0_0_0 = { 1, GenInst_GraphicRaycaster_t3396295912_0_0_0_Types };
static const RuntimeType* GenInst_CanvasRenderer_t799251060_0_0_0_Types[] = { (&CanvasRenderer_t799251060_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t799251060_0_0_0 = { 1, GenInst_CanvasRenderer_t799251060_0_0_0_Types };
static const RuntimeType* GenInst_Corner_t1917386932_0_0_0_Types[] = { (&Corner_t1917386932_0_0_0) };
extern const Il2CppGenericInst GenInst_Corner_t1917386932_0_0_0 = { 1, GenInst_Corner_t1917386932_0_0_0_Types };
static const RuntimeType* GenInst_Axis_t312615268_0_0_0_Types[] = { (&Axis_t312615268_0_0_0) };
extern const Il2CppGenericInst GenInst_Axis_t312615268_0_0_0 = { 1, GenInst_Axis_t312615268_0_0_0_Types };
static const RuntimeType* GenInst_Constraint_t464777171_0_0_0_Types[] = { (&Constraint_t464777171_0_0_0) };
extern const Il2CppGenericInst GenInst_Constraint_t464777171_0_0_0 = { 1, GenInst_Constraint_t464777171_0_0_0_Types };
static const RuntimeType* GenInst_SubmitEvent_t1556243238_0_0_0_Types[] = { (&SubmitEvent_t1556243238_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmitEvent_t1556243238_0_0_0 = { 1, GenInst_SubmitEvent_t1556243238_0_0_0_Types };
static const RuntimeType* GenInst_OnChangeEvent_t2953828243_0_0_0_Types[] = { (&OnChangeEvent_t2953828243_0_0_0) };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2953828243_0_0_0 = { 1, GenInst_OnChangeEvent_t2953828243_0_0_0_Types };
static const RuntimeType* GenInst_OnValidateInput_t4262405814_0_0_0_Types[] = { (&OnValidateInput_t4262405814_0_0_0) };
extern const Il2CppGenericInst GenInst_OnValidateInput_t4262405814_0_0_0 = { 1, GenInst_OnValidateInput_t4262405814_0_0_0_Types };
static const RuntimeType* GenInst_LayoutElement_t1267321563_0_0_0_Types[] = { (&LayoutElement_t1267321563_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutElement_t1267321563_0_0_0 = { 1, GenInst_LayoutElement_t1267321563_0_0_0_Types };
static const RuntimeType* GenInst_RectOffset_t2771649182_0_0_0_Types[] = { (&RectOffset_t2771649182_0_0_0) };
extern const Il2CppGenericInst GenInst_RectOffset_t2771649182_0_0_0 = { 1, GenInst_RectOffset_t2771649182_0_0_0_Types };
static const RuntimeType* GenInst_TextAnchor_t1510919083_0_0_0_Types[] = { (&TextAnchor_t1510919083_0_0_0) };
extern const Il2CppGenericInst GenInst_TextAnchor_t1510919083_0_0_0 = { 1, GenInst_TextAnchor_t1510919083_0_0_0_Types };
static const RuntimeType* GenInst_AnimationTriggers_t2976076601_0_0_0_Types[] = { (&AnimationTriggers_t2976076601_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t2976076601_0_0_0 = { 1, GenInst_AnimationTriggers_t2976076601_0_0_0_Types };
static const RuntimeType* GenInst_Animator_t5420133_0_0_0_Types[] = { (&Animator_t5420133_0_0_0) };
extern const Il2CppGenericInst GenInst_Animator_t5420133_0_0_0 = { 1, GenInst_Animator_t5420133_0_0_0_Types };
static const RuntimeType* GenInst_UnityARVideo_t2470488057_0_0_0_Types[] = { (&UnityARVideo_t2470488057_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARVideo_t2470488057_0_0_0 = { 1, GenInst_UnityARVideo_t2470488057_0_0_0_Types };
static const RuntimeType* GenInst_DontDestroyOnLoad_t2575831040_0_0_0_Types[] = { (&DontDestroyOnLoad_t2575831040_0_0_0) };
extern const Il2CppGenericInst GenInst_DontDestroyOnLoad_t2575831040_0_0_0 = { 1, GenInst_DontDestroyOnLoad_t2575831040_0_0_0_Types };
static const RuntimeType* GenInst_MeshFilter_t2678471223_0_0_0_Types[] = { (&MeshFilter_t2678471223_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshFilter_t2678471223_0_0_0 = { 1, GenInst_MeshFilter_t2678471223_0_0_0_Types };
static const RuntimeType* GenInst_MeshRenderer_t1885265068_0_0_0_Types[] = { (&MeshRenderer_t1885265068_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshRenderer_t1885265068_0_0_0 = { 1, GenInst_MeshRenderer_t1885265068_0_0_0_Types };
static const RuntimeType* GenInst_Animation_t1557311624_0_0_0_Types[] = { (&Animation_t1557311624_0_0_0) };
extern const Il2CppGenericInst GenInst_Animation_t1557311624_0_0_0 = { 1, GenInst_Animation_t1557311624_0_0_0_Types };
static const RuntimeType* GenInst_BoxSlider_t2780260910_0_0_0_Types[] = { (&BoxSlider_t2780260910_0_0_0) };
extern const Il2CppGenericInst GenInst_BoxSlider_t2780260910_0_0_0 = { 1, GenInst_BoxSlider_t2780260910_0_0_0_Types };
static const RuntimeType* GenInst_Light_t2513018095_0_0_0_Types[] = { (&Light_t2513018095_0_0_0) };
extern const Il2CppGenericInst GenInst_Light_t2513018095_0_0_0 = { 1, GenInst_Light_t2513018095_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3425510919_0_0_0_Int32_t3425510919_0_0_0_Types[] = { (&Int32_t3425510919_0_0_0), (&Int32_t3425510919_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3425510919_0_0_0_Int32_t3425510919_0_0_0 = { 2, GenInst_Int32_t3425510919_0_0_0_Int32_t3425510919_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t1224813713_0_0_0_CustomAttributeNamedArgument_t1224813713_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t1224813713_0_0_0), (&CustomAttributeNamedArgument_t1224813713_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1224813713_0_0_0_CustomAttributeNamedArgument_t1224813713_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t1224813713_0_0_0_CustomAttributeNamedArgument_t1224813713_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t3013856313_0_0_0_CustomAttributeTypedArgument_t3013856313_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t3013856313_0_0_0), (&CustomAttributeTypedArgument_t3013856313_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t3013856313_0_0_0_CustomAttributeTypedArgument_t3013856313_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t3013856313_0_0_0_CustomAttributeTypedArgument_t3013856313_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorClipInfo_t183054942_0_0_0_AnimatorClipInfo_t183054942_0_0_0_Types[] = { (&AnimatorClipInfo_t183054942_0_0_0), (&AnimatorClipInfo_t183054942_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t183054942_0_0_0_AnimatorClipInfo_t183054942_0_0_0 = { 2, GenInst_AnimatorClipInfo_t183054942_0_0_0_AnimatorClipInfo_t183054942_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t2499566028_0_0_0_Color32_t2499566028_0_0_0_Types[] = { (&Color32_t2499566028_0_0_0), (&Color32_t2499566028_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t2499566028_0_0_0_Color32_t2499566028_0_0_0 = { 2, GenInst_Color32_t2499566028_0_0_0_Color32_t2499566028_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t1715322097_0_0_0_RaycastResult_t1715322097_0_0_0_Types[] = { (&RaycastResult_t1715322097_0_0_0), (&RaycastResult_t1715322097_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t1715322097_0_0_0_RaycastResult_t1715322097_0_0_0 = { 2, GenInst_RaycastResult_t1715322097_0_0_0_RaycastResult_t1715322097_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t2367319176_0_0_0_UICharInfo_t2367319176_0_0_0_Types[] = { (&UICharInfo_t2367319176_0_0_0), (&UICharInfo_t2367319176_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t2367319176_0_0_0_UICharInfo_t2367319176_0_0_0 = { 2, GenInst_UICharInfo_t2367319176_0_0_0_UICharInfo_t2367319176_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t248963365_0_0_0_UILineInfo_t248963365_0_0_0_Types[] = { (&UILineInfo_t248963365_0_0_0), (&UILineInfo_t248963365_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t248963365_0_0_0_UILineInfo_t248963365_0_0_0 = { 2, GenInst_UILineInfo_t248963365_0_0_0_UILineInfo_t248963365_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t2672378834_0_0_0_UIVertex_t2672378834_0_0_0_Types[] = { (&UIVertex_t2672378834_0_0_0), (&UIVertex_t2672378834_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t2672378834_0_0_0_UIVertex_t2672378834_0_0_0 = { 2, GenInst_UIVertex_t2672378834_0_0_0_UIVertex_t2672378834_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t59524482_0_0_0_Vector2_t59524482_0_0_0_Types[] = { (&Vector2_t59524482_0_0_0), (&Vector2_t59524482_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t59524482_0_0_0_Vector2_t59524482_0_0_0 = { 2, GenInst_Vector2_t59524482_0_0_0_Vector2_t59524482_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t596762001_0_0_0_Vector3_t596762001_0_0_0_Types[] = { (&Vector3_t596762001_0_0_0), (&Vector3_t596762001_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t596762001_0_0_0_Vector3_t596762001_0_0_0 = { 2, GenInst_Vector3_t596762001_0_0_0_Vector3_t596762001_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t1376926224_0_0_0_Vector4_t1376926224_0_0_0_Types[] = { (&Vector4_t1376926224_0_0_0), (&Vector4_t1376926224_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t1376926224_0_0_0_Vector4_t1376926224_0_0_0 = { 2, GenInst_Vector4_t1376926224_0_0_0_Vector4_t1376926224_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResult_t1803723679_0_0_0_ARHitTestResult_t1803723679_0_0_0_Types[] = { (&ARHitTestResult_t1803723679_0_0_0), (&ARHitTestResult_t1803723679_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResult_t1803723679_0_0_0_ARHitTestResult_t1803723679_0_0_0 = { 2, GenInst_ARHitTestResult_t1803723679_0_0_0_ARHitTestResult_t1803723679_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2171644578_0_0_0_KeyValuePair_2_t2171644578_0_0_0_Types[] = { (&KeyValuePair_2_t2171644578_0_0_0), (&KeyValuePair_2_t2171644578_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2171644578_0_0_0_KeyValuePair_2_t2171644578_0_0_0 = { 2, GenInst_KeyValuePair_2_t2171644578_0_0_0_KeyValuePair_2_t2171644578_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2171644578_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2171644578_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2171644578_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2171644578_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3430616441_0_0_0_KeyValuePair_2_t3430616441_0_0_0_Types[] = { (&KeyValuePair_2_t3430616441_0_0_0), (&KeyValuePair_2_t3430616441_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3430616441_0_0_0_KeyValuePair_2_t3430616441_0_0_0 = { 2, GenInst_KeyValuePair_2_t3430616441_0_0_0_KeyValuePair_2_t3430616441_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3430616441_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3430616441_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3430616441_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3430616441_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t362855854_0_0_0_Boolean_t362855854_0_0_0_Types[] = { (&Boolean_t362855854_0_0_0), (&Boolean_t362855854_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t362855854_0_0_0_Boolean_t362855854_0_0_0 = { 2, GenInst_Boolean_t362855854_0_0_0_Boolean_t362855854_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t905929833_0_0_0_KeyValuePair_2_t905929833_0_0_0_Types[] = { (&KeyValuePair_2_t905929833_0_0_0), (&KeyValuePair_2_t905929833_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t905929833_0_0_0_KeyValuePair_2_t905929833_0_0_0 = { 2, GenInst_KeyValuePair_2_t905929833_0_0_0_KeyValuePair_2_t905929833_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t905929833_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t905929833_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t905929833_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t905929833_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3968584898_0_0_0_KeyValuePair_2_t3968584898_0_0_0_Types[] = { (&KeyValuePair_2_t3968584898_0_0_0), (&KeyValuePair_2_t3968584898_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3968584898_0_0_0_KeyValuePair_2_t3968584898_0_0_0 = { 2, GenInst_KeyValuePair_2_t3968584898_0_0_0_KeyValuePair_2_t3968584898_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3968584898_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3968584898_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3968584898_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3968584898_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t665585682_0_0_0_KeyValuePair_2_t665585682_0_0_0_Types[] = { (&KeyValuePair_2_t665585682_0_0_0), (&KeyValuePair_2_t665585682_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t665585682_0_0_0_KeyValuePair_2_t665585682_0_0_0 = { 2, GenInst_KeyValuePair_2_t665585682_0_0_0_KeyValuePair_2_t665585682_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t665585682_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t665585682_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t665585682_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t665585682_0_0_0_RuntimeObject_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[598] = 
{
	&GenInst_RuntimeObject_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0,
	&GenInst_Char_t539237919_0_0_0,
	&GenInst_Int64_t2252457107_0_0_0,
	&GenInst_UInt32_t3933237433_0_0_0,
	&GenInst_UInt64_t1498391637_0_0_0,
	&GenInst_Byte_t3065488403_0_0_0,
	&GenInst_SByte_t1519635365_0_0_0,
	&GenInst_Int16_t2196066360_0_0_0,
	&GenInst_UInt16_t2173916929_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t2722588824_0_0_0,
	&GenInst_IComparable_t1036281117_0_0_0,
	&GenInst_IEnumerable_t2201844525_0_0_0,
	&GenInst_ICloneable_t2975100769_0_0_0,
	&GenInst_IComparable_1_t2296033782_0_0_0,
	&GenInst_IEquatable_1_t3464885664_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t901526961_0_0_0,
	&GenInst__Type_t3150095453_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t490813527_0_0_0,
	&GenInst__MemberInfo_t2295893705_0_0_0,
	&GenInst_Double_t1029397067_0_0_0,
	&GenInst_Single_t2847614712_0_0_0,
	&GenInst_Decimal_t2663171440_0_0_0,
	&GenInst_Boolean_t362855854_0_0_0,
	&GenInst_Delegate_t69892740_0_0_0,
	&GenInst_ISerializable_t1411645196_0_0_0,
	&GenInst_ParameterInfo_t3577984089_0_0_0,
	&GenInst__ParameterInfo_t3423239624_0_0_0,
	&GenInst_ParameterModifier_t3485655876_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t1619462957_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2155012913_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t2553989953_0_0_0,
	&GenInst_MethodBase_t1640104494_0_0_0,
	&GenInst__MethodBase_t3674854218_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t2466204053_0_0_0,
	&GenInst_ConstructorInfo_t1214230744_0_0_0,
	&GenInst__ConstructorInfo_t3104092857_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t3005830667_0_0_0,
	&GenInst_TailoringInfo_t2103418023_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0,
	&GenInst_KeyValuePair_2_t3968584898_0_0_0,
	&GenInst_Link_t4043807316_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_Int32_t3425510919_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t3968584898_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t2032264690_0_0_0,
	&GenInst_KeyValuePair_2_t2032264690_0_0_0,
	&GenInst_Contraction_t3464654688_0_0_0,
	&GenInst_Level2Map_t782964927_0_0_0,
	&GenInst_BigInteger_t1286850636_0_0_0,
	&GenInst_KeySizes_t2873621649_0_0_0,
	&GenInst_KeyValuePair_2_t665585682_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t665585682_0_0_0,
	&GenInst_Slot_t4047215697_0_0_0,
	&GenInst_Slot_t3212906767_0_0_0,
	&GenInst_StackFrame_t2301538707_0_0_0,
	&GenInst_Calendar_t4189858442_0_0_0,
	&GenInst_ModuleBuilder_t2846916743_0_0_0,
	&GenInst__ModuleBuilder_t1727831225_0_0_0,
	&GenInst_Module_t4287715521_0_0_0,
	&GenInst__Module_t3465402545_0_0_0,
	&GenInst_ParameterBuilder_t2375480068_0_0_0,
	&GenInst__ParameterBuilder_t2449263151_0_0_0,
	&GenInst_TypeU5BU5D_t1484232934_0_0_0,
	&GenInst_RuntimeArray_0_0_0,
	&GenInst_ICollection_t1426578131_0_0_0,
	&GenInst_IList_t4121428329_0_0_0,
	&GenInst_IList_1_t575872299_0_0_0,
	&GenInst_ICollection_1_t2387962414_0_0_0,
	&GenInst_IEnumerable_1_t445450583_0_0_0,
	&GenInst_IList_1_t2458661789_0_0_0,
	&GenInst_ICollection_1_t4270751904_0_0_0,
	&GenInst_IEnumerable_1_t2328240073_0_0_0,
	&GenInst_IList_1_t412262985_0_0_0,
	&GenInst_ICollection_1_t2224353100_0_0_0,
	&GenInst_IEnumerable_1_t281841269_0_0_0,
	&GenInst_IList_1_t2505642904_0_0_0,
	&GenInst_ICollection_1_t22765723_0_0_0,
	&GenInst_IEnumerable_1_t2375221188_0_0_0,
	&GenInst_IList_1_t2047948355_0_0_0,
	&GenInst_ICollection_1_t3860038470_0_0_0,
	&GenInst_IEnumerable_1_t1917526639_0_0_0,
	&GenInst_IList_1_t3853028533_0_0_0,
	&GenInst_ICollection_1_t1370151352_0_0_0,
	&GenInst_IEnumerable_1_t3722606817_0_0_0,
	&GenInst_IList_1_t1679646531_0_0_0,
	&GenInst_ICollection_1_t3491736646_0_0_0,
	&GenInst_IEnumerable_1_t1549224815_0_0_0,
	&GenInst_ILTokenInfo_t4134893432_0_0_0,
	&GenInst_LabelData_t1800135739_0_0_0,
	&GenInst_LabelFixup_t703658549_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t451279079_0_0_0,
	&GenInst_TypeBuilder_t1669012508_0_0_0,
	&GenInst__TypeBuilder_t4212529197_0_0_0,
	&GenInst_MethodBuilder_t1092218305_0_0_0,
	&GenInst__MethodBuilder_t3510139682_0_0_0,
	&GenInst_ConstructorBuilder_t33025729_0_0_0,
	&GenInst__ConstructorBuilder_t1274505923_0_0_0,
	&GenInst_PropertyBuilder_t1720155075_0_0_0,
	&GenInst__PropertyBuilder_t3760533436_0_0_0,
	&GenInst_FieldBuilder_t1478132776_0_0_0,
	&GenInst__FieldBuilder_t698479335_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t3013856313_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1224813713_0_0_0,
	&GenInst_CustomAttributeData_t2607898573_0_0_0,
	&GenInst_ResourceInfo_t3677787790_0_0_0,
	&GenInst_ResourceCacheItem_t567161473_0_0_0,
	&GenInst_IContextProperty_t402073787_0_0_0,
	&GenInst_Header_t1099438005_0_0_0,
	&GenInst_ITrackingHandler_t127120994_0_0_0,
	&GenInst_IContextAttribute_t3616836717_0_0_0,
	&GenInst_DateTime_t1819153659_0_0_0,
	&GenInst_TimeSpan_t4158060032_0_0_0,
	&GenInst_TypeTag_t1792597276_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t3725927940_0_0_0,
	&GenInst_IBuiltInEvidence_t3225111825_0_0_0,
	&GenInst_IIdentityPermissionFactory_t4063359944_0_0_0,
	&GenInst_DateTimeOffset_t2489358039_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t3431102195_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_KeyValuePair_2_t905929833_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t362855854_0_0_0_KeyValuePair_2_t905929833_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t362855854_0_0_0_KeyValuePair_2_t3264576921_0_0_0,
	&GenInst_KeyValuePair_2_t3264576921_0_0_0,
	&GenInst_X509Certificate_t1886472155_0_0_0,
	&GenInst_IDeserializationCallback_t1045582565_0_0_0,
	&GenInst_X509ChainStatus_t3209745328_0_0_0,
	&GenInst_Capture_t545002712_0_0_0,
	&GenInst_Group_t1246193761_0_0_0,
	&GenInst_Mark_t1421951811_0_0_0,
	&GenInst_UriScheme_t1685060244_0_0_0,
	&GenInst_BigInteger_t1286850637_0_0_0,
	&GenInst_ByteU5BU5D_t3548078658_0_0_0,
	&GenInst_IList_1_t327655935_0_0_0,
	&GenInst_ICollection_1_t2139746050_0_0_0,
	&GenInst_IEnumerable_1_t197234219_0_0_0,
	&GenInst_ClientCertificateType_t1149805517_0_0_0,
	&GenInst_Link_t655338529_0_0_0,
	&GenInst_List_1_t2975039247_0_0_0,
	&GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0,
	&GenInst_DispatcherKey_t3620541735_0_0_0,
	&GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_DispatcherKey_t3620541735_0_0_0_Dispatcher_t55733337_0_0_0_KeyValuePair_2_t2849605124_0_0_0,
	&GenInst_KeyValuePair_2_t2849605124_0_0_0,
	&GenInst_Object_t1008057425_0_0_0,
	&GenInst_Camera_t226495598_0_0_0,
	&GenInst_Behaviour_t2200997390_0_0_0,
	&GenInst_Component_t531478471_0_0_0,
	&GenInst_Display_t4159198643_0_0_0,
	&GenInst_Keyframe_t643552408_0_0_0,
	&GenInst_Vector3_t596762001_0_0_0,
	&GenInst_Vector4_t1376926224_0_0_0,
	&GenInst_Vector2_t59524482_0_0_0,
	&GenInst_Color32_t2499566028_0_0_0,
	&GenInst_Playable_t1112762172_0_0_0,
	&GenInst_PlayableOutput_t1654971666_0_0_0,
	&GenInst_Scene_t1614351645_0_0_0_LoadSceneMode_t2556746618_0_0_0,
	&GenInst_Scene_t1614351645_0_0_0,
	&GenInst_Scene_t1614351645_0_0_0_Scene_t1614351645_0_0_0,
	&GenInst_SpriteAtlas_t1553410611_0_0_0,
	&GenInst_Particle_t1268635397_0_0_0,
	&GenInst_ContactPoint_t3149140470_0_0_0,
	&GenInst_RaycastHit_t1273336641_0_0_0,
	&GenInst_Rigidbody2D_t2029812455_0_0_0,
	&GenInst_AudioClipPlayable_t1384420439_0_0_0,
	&GenInst_AudioMixerPlayable_t3880288074_0_0_0,
	&GenInst_AnimationClipPlayable_t3824769629_0_0_0,
	&GenInst_AnimationLayerMixerPlayable_t3575213449_0_0_0,
	&GenInst_AnimationMixerPlayable_t4293775112_0_0_0,
	&GenInst_AnimationOffsetPlayable_t1345717778_0_0_0,
	&GenInst_AnimatorControllerPlayable_t3438332640_0_0_0,
	&GenInst_AnimatorClipInfo_t183054942_0_0_0,
	&GenInst_AnimatorControllerParameter_t3164458347_0_0_0,
	&GenInst_UIVertex_t2672378834_0_0_0,
	&GenInst_UICharInfo_t2367319176_0_0_0,
	&GenInst_UILineInfo_t248963365_0_0_0,
	&GenInst_Font_t2699442136_0_0_0,
	&GenInst_GUILayoutOption_t721761240_0_0_0,
	&GenInst_GUILayoutEntry_t240640878_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2171644578_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2171644578_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_LayoutCache_t1385791520_0_0_0_KeyValuePair_2_t3434924395_0_0_0,
	&GenInst_KeyValuePair_2_t3434924395_0_0_0,
	&GenInst_GUIStyle_t2932200036_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_KeyValuePair_2_t1538953807_0_0_0,
	&GenInst_KeyValuePair_2_t1538953807_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2932200036_0_0_0_GUIStyle_t2932200036_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_IntPtr_t_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_Exception_t82373287_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_UserProfile_t771069284_0_0_0,
	&GenInst_IUserProfile_t1988237155_0_0_0,
	&GenInst_Boolean_t362855854_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t362855854_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AchievementDescription_t433697264_0_0_0,
	&GenInst_IAchievementDescription_t1962024065_0_0_0,
	&GenInst_GcLeaderboard_t374162520_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t857867356_0_0_0,
	&GenInst_IAchievementU5BU5D_t2204500354_0_0_0,
	&GenInst_IAchievement_t1434617299_0_0_0,
	&GenInst_GcAchievementData_t1098514478_0_0_0,
	&GenInst_Achievement_t1914343199_0_0_0,
	&GenInst_IScoreU5BU5D_t754807597_0_0_0,
	&GenInst_IScore_t2259084100_0_0_0,
	&GenInst_GcScoreData_t322958630_0_0_0,
	&GenInst_Score_t1824312173_0_0_0,
	&GenInst_IUserProfileU5BU5D_t1589946290_0_0_0,
	&GenInst_DisallowMultipleComponent_t2877457562_0_0_0,
	&GenInst_Attribute_t1842279184_0_0_0,
	&GenInst__Attribute_t3546903852_0_0_0,
	&GenInst_ExecuteInEditMode_t3420945569_0_0_0,
	&GenInst_RequireComponent_t4227770712_0_0_0,
	&GenInst_HitInfo_t2832651489_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_PersistentCall_t709146344_0_0_0,
	&GenInst_BaseInvokableCall_t3830733407_0_0_0,
	&GenInst_WorkRequest_t886435312_0_0_0,
	&GenInst_PlayableBinding_t1499311357_0_0_0,
	&GenInst_MessageTypeSubscribers_t2879909665_0_0_0,
	&GenInst_MessageTypeSubscribers_t2879909665_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_MessageEventArgs_t3627585352_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3430616441_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3430616441_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t245340424_0_0_0_KeyValuePair_2_t3553445162_0_0_0,
	&GenInst_KeyValuePair_2_t3553445162_0_0_0,
	&GenInst_BaseInputModule_t309635854_0_0_0,
	&GenInst_RaycastResult_t1715322097_0_0_0,
	&GenInst_IDeselectHandler_t3843174961_0_0_0,
	&GenInst_IEventSystemHandler_t558645447_0_0_0,
	&GenInst_List_1_t3596965026_0_0_0,
	&GenInst_List_1_t3160831282_0_0_0,
	&GenInst_List_1_t3569798050_0_0_0,
	&GenInst_ISelectHandler_t2734039658_0_0_0,
	&GenInst_BaseRaycaster_t3590231488_0_0_0,
	&GenInst_Entry_t3975632235_0_0_0,
	&GenInst_BaseEventData_t341072110_0_0_0,
	&GenInst_IPointerEnterHandler_t2566364723_0_0_0,
	&GenInst_IPointerExitHandler_t1566539265_0_0_0,
	&GenInst_IPointerDownHandler_t1810984543_0_0_0,
	&GenInst_IPointerUpHandler_t2617963051_0_0_0,
	&GenInst_IPointerClickHandler_t1471500927_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t2875812772_0_0_0,
	&GenInst_IBeginDragHandler_t737688467_0_0_0,
	&GenInst_IDragHandler_t1361425836_0_0_0,
	&GenInst_IEndDragHandler_t492564000_0_0_0,
	&GenInst_IDropHandler_t4181017068_0_0_0,
	&GenInst_IScrollHandler_t2087319098_0_0_0,
	&GenInst_IUpdateSelectedHandler_t1291562379_0_0_0,
	&GenInst_IMoveHandler_t4033052693_0_0_0,
	&GenInst_ISubmitHandler_t234818760_0_0_0,
	&GenInst_ICancelHandler_t516662923_0_0_0,
	&GenInst_Transform_t3933397867_0_0_0,
	&GenInst_GameObject_t3649338848_0_0_0,
	&GenInst_BaseInput_t3666734484_0_0_0,
	&GenInst_UIBehaviour_t2441083007_0_0_0,
	&GenInst_MonoBehaviour_t1096588306_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_KeyValuePair_2_t3612454894_0_0_0,
	&GenInst_KeyValuePair_2_t3612454894_0_0_0,
	&GenInst_PointerEventData_t1563322019_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_PointerEventData_t1563322019_0_0_0_PointerEventData_t1563322019_0_0_0,
	&GenInst_ButtonState_t4214419048_0_0_0,
	&GenInst_RaycastHit2D_t2323693239_0_0_0,
	&GenInst_Color_t320819310_0_0_0,
	&GenInst_ICanvasElement_t803896971_0_0_0,
	&GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0,
	&GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_ColorBlock_t625039033_0_0_0,
	&GenInst_OptionData_t445311210_0_0_0,
	&GenInst_DropdownItem_t953411360_0_0_0,
	&GenInst_FloatTween_t1459678735_0_0_0,
	&GenInst_Sprite_t1309550511_0_0_0,
	&GenInst_Canvas_t3680465181_0_0_0,
	&GenInst_List_1_t2423817464_0_0_0,
	&GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0,
	&GenInst_Text_t3069741234_0_0_0,
	&GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_Font_t2699442136_0_0_0_HashSet_1_t3994364505_0_0_0_KeyValuePair_2_t3253400431_0_0_0,
	&GenInst_KeyValuePair_2_t3253400431_0_0_0,
	&GenInst_ColorTween_t1347433457_0_0_0,
	&GenInst_Graphic_t1406460313_0_0_0,
	&GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0,
	&GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0,
	&GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_Canvas_t3680465181_0_0_0_IndexedSet_1_t2830149342_0_0_0_KeyValuePair_2_t2983860219_0_0_0,
	&GenInst_KeyValuePair_2_t2983860219_0_0_0,
	&GenInst_Graphic_t1406460313_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t2618575160_0_0_0,
	&GenInst_KeyValuePair_2_t2618575160_0_0_0,
	&GenInst_ICanvasElement_t803896971_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t1278652478_0_0_0,
	&GenInst_KeyValuePair_2_t1278652478_0_0_0,
	&GenInst_Type_t125599537_0_0_0,
	&GenInst_FillMethod_t46703468_0_0_0,
	&GenInst_ContentType_t2687439383_0_0_0,
	&GenInst_LineType_t4134456689_0_0_0,
	&GenInst_InputType_t3397888567_0_0_0,
	&GenInst_TouchScreenKeyboardType_t491524606_0_0_0,
	&GenInst_CharacterValidation_t2578987709_0_0_0,
	&GenInst_Mask_t3495621644_0_0_0,
	&GenInst_List_1_t2238973927_0_0_0,
	&GenInst_RectMask2D_t2103047313_0_0_0,
	&GenInst_List_1_t846399596_0_0_0,
	&GenInst_Navigation_t2887492880_0_0_0,
	&GenInst_IClippable_t2263516411_0_0_0,
	&GenInst_Direction_t1877520305_0_0_0,
	&GenInst_Selectable_t3890617260_0_0_0,
	&GenInst_Transition_t327687789_0_0_0,
	&GenInst_SpriteState_t758977253_0_0_0,
	&GenInst_CanvasGroup_t3075965541_0_0_0,
	&GenInst_Direction_t1918642359_0_0_0,
	&GenInst_MatEntry_t4139921362_0_0_0,
	&GenInst_Toggle_t1116311230_0_0_0,
	&GenInst_Toggle_t1116311230_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_IClipper_t1468079658_0_0_0,
	&GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0,
	&GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_IClipper_t1468079658_0_0_0_Int32_t3425510919_0_0_0_KeyValuePair_2_t3227114307_0_0_0,
	&GenInst_KeyValuePair_2_t3227114307_0_0_0,
	&GenInst_AspectMode_t3157395208_0_0_0,
	&GenInst_FitMode_t760495087_0_0_0,
	&GenInst_RectTransform_t1885177139_0_0_0,
	&GenInst_LayoutRebuilder_t1578890533_0_0_0,
	&GenInst_ILayoutElement_t2907498299_0_0_0_Single_t2847614712_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t2847614712_0_0_0,
	&GenInst_List_1_t3635081580_0_0_0,
	&GenInst_List_1_t1242918311_0_0_0,
	&GenInst_List_1_t3097844061_0_0_0,
	&GenInst_List_1_t120278507_0_0_0,
	&GenInst_List_1_t2168863202_0_0_0,
	&GenInst_List_1_t1415731117_0_0_0,
	&GenInst_ARHitTestResult_t1803723679_0_0_0,
	&GenInst_ARPlaneAnchorGameObject_t3146329710_0_0_0,
	&GenInst_ARHitTestResultType_t270086150_0_0_0,
	&GenInst_UnityARSessionRunOption_t2337414342_0_0_0,
	&GenInst_UnityARAlignment_t1226993326_0_0_0,
	&GenInst_UnityARPlaneDetection_t2335225179_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3146329710_0_0_0_KeyValuePair_2_t1753083481_0_0_0,
	&GenInst_KeyValuePair_2_t1753083481_0_0_0,
	&GenInst_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0,
	&GenInst_Single_t2847614712_0_0_0_Single_t2847614712_0_0_0,
	&GenInst_ParticleSystem_t1777616458_0_0_0,
	&GenInst_IEnumerable_1_t1371926219_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m4218696652_gp_0_0_0_0,
	&GenInst_Array_Sort_m3424865288_gp_0_0_0_0_Array_Sort_m3424865288_gp_0_0_0_0,
	&GenInst_Array_Sort_m1174453676_gp_0_0_0_0_Array_Sort_m1174453676_gp_1_0_0_0,
	&GenInst_Array_Sort_m904955079_gp_0_0_0_0,
	&GenInst_Array_Sort_m904955079_gp_0_0_0_0_Array_Sort_m904955079_gp_0_0_0_0,
	&GenInst_Array_Sort_m224785917_gp_0_0_0_0,
	&GenInst_Array_Sort_m224785917_gp_0_0_0_0_Array_Sort_m224785917_gp_1_0_0_0,
	&GenInst_Array_Sort_m3326076588_gp_0_0_0_0_Array_Sort_m3326076588_gp_0_0_0_0,
	&GenInst_Array_Sort_m2780446043_gp_0_0_0_0_Array_Sort_m2780446043_gp_1_0_0_0,
	&GenInst_Array_Sort_m2417833379_gp_0_0_0_0,
	&GenInst_Array_Sort_m2417833379_gp_0_0_0_0_Array_Sort_m2417833379_gp_0_0_0_0,
	&GenInst_Array_Sort_m1373481026_gp_0_0_0_0,
	&GenInst_Array_Sort_m1373481026_gp_1_0_0_0,
	&GenInst_Array_Sort_m1373481026_gp_0_0_0_0_Array_Sort_m1373481026_gp_1_0_0_0,
	&GenInst_Array_Sort_m1201249205_gp_0_0_0_0,
	&GenInst_Array_Sort_m3696811223_gp_0_0_0_0,
	&GenInst_Array_qsort_m3347095892_gp_0_0_0_0,
	&GenInst_Array_qsort_m3347095892_gp_0_0_0_0_Array_qsort_m3347095892_gp_1_0_0_0,
	&GenInst_Array_compare_m286771913_gp_0_0_0_0,
	&GenInst_Array_qsort_m1529647689_gp_0_0_0_0,
	&GenInst_Array_Resize_m2542074053_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2493260605_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3467030689_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1694469864_gp_0_0_0_0_Array_ConvertAll_m1694469864_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m4135284164_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1527152247_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m4002081672_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m17293001_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m936277329_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m178503657_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m557100490_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3954992392_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1167857953_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3918896244_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m758192846_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1794394959_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m315707683_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2029688934_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3148242585_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1969981718_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1088630835_gp_0_0_0_0,
	&GenInst_Array_Exists_m2063596975_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m2584618994_gp_0_0_0_0,
	&GenInst_Array_Find_m2415417361_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3619283605_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t15510975_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t5340530_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3976382471_gp_0_0_0_0,
	&GenInst_IList_1_t1284029119_gp_0_0_0_0,
	&GenInst_ICollection_1_t3388121722_gp_0_0_0_0,
	&GenInst_Nullable_1_t739790191_gp_0_0_0_0,
	&GenInst_Comparer_1_t2195447696_gp_0_0_0_0,
	&GenInst_DefaultComparer_t989032782_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t3293245800_gp_0_0_0_0,
	&GenInst_Dictionary_2_t681358595_gp_0_0_0_0,
	&GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1779626263_0_0_0,
	&GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m37576101_gp_0_0_0_0,
	&GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m693317911_gp_0_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_ShimEnumerator_t1842228008_gp_0_0_0_0_ShimEnumerator_t1842228008_gp_1_0_0_0,
	&GenInst_Enumerator_t3839420370_gp_0_0_0_0_Enumerator_t3839420370_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2283510011_0_0_0,
	&GenInst_ValueCollection_t2793800625_gp_0_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0,
	&GenInst_ValueCollection_t2793800625_gp_1_0_0_0,
	&GenInst_Enumerator_t760914474_gp_0_0_0_0_Enumerator_t760914474_gp_1_0_0_0,
	&GenInst_Enumerator_t760914474_gp_1_0_0_0,
	&GenInst_ValueCollection_t2793800625_gp_0_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0,
	&GenInst_ValueCollection_t2793800625_gp_1_0_0_0_ValueCollection_t2793800625_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3951334252_0_0_0_DictionaryEntry_t3951334252_0_0_0,
	&GenInst_Dictionary_2_t681358595_gp_0_0_0_0_Dictionary_2_t681358595_gp_1_0_0_0_KeyValuePair_2_t1779626263_0_0_0,
	&GenInst_KeyValuePair_2_t1779626263_0_0_0_KeyValuePair_2_t1779626263_0_0_0,
	&GenInst_Dictionary_2_t681358595_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t1195631860_gp_0_0_0_0,
	&GenInst_DefaultComparer_t706283517_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t3304566894_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t916714459_0_0_0,
	&GenInst_IDictionary_2_t1884790490_gp_0_0_0_0_IDictionary_2_t1884790490_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1574552141_gp_0_0_0_0_KeyValuePair_2_t1574552141_gp_1_0_0_0,
	&GenInst_List_1_t295223622_gp_0_0_0_0,
	&GenInst_Enumerator_t2074507559_gp_0_0_0_0,
	&GenInst_Collection_1_t3142256919_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t914928874_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m1404835840_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1404835840_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1727439354_gp_0_0_0_0,
	&GenInst_Queue_1_t1196217950_gp_0_0_0_0,
	&GenInst_Enumerator_t2727315752_gp_0_0_0_0,
	&GenInst_Stack_1_t696983570_gp_0_0_0_0,
	&GenInst_Enumerator_t812808600_gp_0_0_0_0,
	&GenInst_HashSet_1_t1291849714_gp_0_0_0_0,
	&GenInst_Enumerator_t1535822487_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3642281417_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2221973312_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m1954003453_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m3550166720_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m3550166720_gp_0_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2391756827_gp_0_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t486096562_gp_0_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_List_1_t467069803_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator6_t4041278014_gp_0_0_0_0,
	&GenInst_List_1_t3967229796_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3128998196_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1790039706_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1997911980_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m159208863_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m4223062209_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1689563606_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1885645319_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1546679232_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m1602522827_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2981955009_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m972597910_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m3123327788_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1146767846_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m2891368970_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m4121023089_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3772110265_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4080854334_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m2700408318_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m1338450254_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m3278571309_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m1189854602_gp_0_0_0_0,
	&GenInst_Playable_IsPlayableOfType_m2446752783_gp_0_0_0_0,
	&GenInst_PlayableOutput_IsPlayableOutputOfType_m3414177984_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t1043888743_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2762766517_0_0_0,
	&GenInst_InvokableCall_2_t4283310926_gp_0_0_0_0_InvokableCall_2_t4283310926_gp_1_0_0_0,
	&GenInst_UnityAction_2_t846580519_0_0_0,
	&GenInst_InvokableCall_2_t4283310926_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t4283310926_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t1407968758_gp_0_0_0_0_InvokableCall_3_t1407968758_gp_1_0_0_0_InvokableCall_3_t1407968758_gp_2_0_0_0,
	&GenInst_UnityAction_3_t1541563382_0_0_0,
	&GenInst_InvokableCall_3_t1407968758_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t1407968758_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t1407968758_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3522370476_gp_0_0_0_0_InvokableCall_4_t3522370476_gp_1_0_0_0_InvokableCall_4_t3522370476_gp_2_0_0_0_InvokableCall_4_t3522370476_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3522370476_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3522370476_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3522370476_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3522370476_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t2169007495_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t2170633027_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t2498946136_gp_0_0_0_0_UnityEvent_2_t2498946136_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t3426698314_gp_0_0_0_0_UnityEvent_3_t3426698314_gp_1_0_0_0_UnityEvent_3_t3426698314_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t1332048011_gp_0_0_0_0_UnityEvent_4_t1332048011_gp_1_0_0_0_UnityEvent_4_t1332048011_gp_2_0_0_0_UnityEvent_4_t1332048011_gp_3_0_0_0,
	&GenInst_ExecuteEvents_Execute_m3317274811_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m3880156326_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m3038861451_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m151454768_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m1494251347_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t726558302_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m3300878866_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m3021317927_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2282855104_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2282855104_gp_0_0_0_0_Int32_t3425510919_0_0_0,
	&GenInst_ListPool_1_t681572707_gp_0_0_0_0,
	&GenInst_List_1_t72557439_0_0_0,
	&GenInst_ObjectPool_1_t3954034076_gp_0_0_0_0,
	&GenInst_AnimationPlayableOutput_t2577072500_0_0_0,
	&GenInst_DefaultExecutionOrder_t3670034221_0_0_0,
	&GenInst_AudioPlayableOutput_t3404408964_0_0_0,
	&GenInst_PlayerConnection_t3806501584_0_0_0,
	&GenInst_ScriptPlayableOutput_t3124905912_0_0_0,
	&GenInst_GUILayer_t2573986518_0_0_0,
	&GenInst_EventSystem_t487304461_0_0_0,
	&GenInst_AxisEventData_t3116767805_0_0_0,
	&GenInst_SpriteRenderer_t621304888_0_0_0,
	&GenInst_Image_t126372159_0_0_0,
	&GenInst_Button_t467150838_0_0_0,
	&GenInst_RawImage_t3037409423_0_0_0,
	&GenInst_Slider_t301630380_0_0_0,
	&GenInst_Scrollbar_t1086108786_0_0_0,
	&GenInst_InputField_t3161868630_0_0_0,
	&GenInst_ScrollRect_t371228710_0_0_0,
	&GenInst_Dropdown_t1637122921_0_0_0,
	&GenInst_GraphicRaycaster_t3396295912_0_0_0,
	&GenInst_CanvasRenderer_t799251060_0_0_0,
	&GenInst_Corner_t1917386932_0_0_0,
	&GenInst_Axis_t312615268_0_0_0,
	&GenInst_Constraint_t464777171_0_0_0,
	&GenInst_SubmitEvent_t1556243238_0_0_0,
	&GenInst_OnChangeEvent_t2953828243_0_0_0,
	&GenInst_OnValidateInput_t4262405814_0_0_0,
	&GenInst_LayoutElement_t1267321563_0_0_0,
	&GenInst_RectOffset_t2771649182_0_0_0,
	&GenInst_TextAnchor_t1510919083_0_0_0,
	&GenInst_AnimationTriggers_t2976076601_0_0_0,
	&GenInst_Animator_t5420133_0_0_0,
	&GenInst_UnityARVideo_t2470488057_0_0_0,
	&GenInst_DontDestroyOnLoad_t2575831040_0_0_0,
	&GenInst_MeshFilter_t2678471223_0_0_0,
	&GenInst_MeshRenderer_t1885265068_0_0_0,
	&GenInst_Animation_t1557311624_0_0_0,
	&GenInst_BoxSlider_t2780260910_0_0_0,
	&GenInst_Light_t2513018095_0_0_0,
	&GenInst_Int32_t3425510919_0_0_0_Int32_t3425510919_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1224813713_0_0_0_CustomAttributeNamedArgument_t1224813713_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t3013856313_0_0_0_CustomAttributeTypedArgument_t3013856313_0_0_0,
	&GenInst_AnimatorClipInfo_t183054942_0_0_0_AnimatorClipInfo_t183054942_0_0_0,
	&GenInst_Color32_t2499566028_0_0_0_Color32_t2499566028_0_0_0,
	&GenInst_RaycastResult_t1715322097_0_0_0_RaycastResult_t1715322097_0_0_0,
	&GenInst_UICharInfo_t2367319176_0_0_0_UICharInfo_t2367319176_0_0_0,
	&GenInst_UILineInfo_t248963365_0_0_0_UILineInfo_t248963365_0_0_0,
	&GenInst_UIVertex_t2672378834_0_0_0_UIVertex_t2672378834_0_0_0,
	&GenInst_Vector2_t59524482_0_0_0_Vector2_t59524482_0_0_0,
	&GenInst_Vector3_t596762001_0_0_0_Vector3_t596762001_0_0_0,
	&GenInst_Vector4_t1376926224_0_0_0_Vector4_t1376926224_0_0_0,
	&GenInst_ARHitTestResult_t1803723679_0_0_0_ARHitTestResult_t1803723679_0_0_0,
	&GenInst_KeyValuePair_2_t2171644578_0_0_0_KeyValuePair_2_t2171644578_0_0_0,
	&GenInst_KeyValuePair_2_t2171644578_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3430616441_0_0_0_KeyValuePair_2_t3430616441_0_0_0,
	&GenInst_KeyValuePair_2_t3430616441_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Boolean_t362855854_0_0_0_Boolean_t362855854_0_0_0,
	&GenInst_KeyValuePair_2_t905929833_0_0_0_KeyValuePair_2_t905929833_0_0_0,
	&GenInst_KeyValuePair_2_t905929833_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3968584898_0_0_0_KeyValuePair_2_t3968584898_0_0_0,
	&GenInst_KeyValuePair_2_t3968584898_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t665585682_0_0_0_KeyValuePair_2_t665585682_0_0_0,
	&GenInst_KeyValuePair_2_t665585682_0_0_0_RuntimeObject_0_0_0,
};
