﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t124164409;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3977637013;
// System.String
struct String_t;
// UnityEngine.MeshCollider
struct MeshCollider_t3960673270;
// UnityEngine.MeshFilter
struct MeshFilter_t2678471223;
// UnityEngine.GameObject
struct GameObject_t3649338848;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct Dictionary_2_t3033730875;
// System.Object[]
struct ObjectU5BU5D_t3384890222;
// System.Char[]
struct CharU5BU5D_t674980486;
// System.Void
struct Void_t2725935594;
// UnityEngine.Sprite
struct Sprite_t1309550511;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t421390435;
// UnityEngine.UI.Selectable
struct Selectable_t3890617260;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t4153953548;
// System.IAsyncResult
struct IAsyncResult_t1614106113;
// System.AsyncCallback
struct AsyncCallback_t606388952;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct ARFrameUpdate_t3333962149;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct ARAnchorAdded_t1580091102;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct ARAnchorUpdated_t1492128022;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct ARAnchorRemoved_t3663543047;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct ARSessionFailed_t3599980200;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct internal_ARFrameUpdate_t2726389622;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct internal_ARAnchorAdded_t3791587037;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct internal_ARAnchorUpdated_t2928809250;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct internal_ARAnchorRemoved_t409959511;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1777616458;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t419044168;
// ColorPicker
struct ColorPicker_t2582382693;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t1488940705;
// UnityEngine.UI.Image
struct Image_t126372159;
// UnityEngine.Camera
struct Camera_t226495598;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t3869411053;
// UnityEngine.Material
struct Material_t4055262778;
// ColorChangedEvent
struct ColorChangedEvent_t347040188;
// HSVChangedEvent
struct HSVChangedEvent_t3546572410;
// UnityEngine.Transform
struct Transform_t3933397867;
// UnityEngine.UI.Slider
struct Slider_t301630380;
// UnityEngine.UI.RawImage
struct RawImage_t3037409423;
// UnityEngine.UI.InputField
struct InputField_t3161868630;
// UnityEngine.UI.BoxSlider
struct BoxSlider_t2780260910;
// UnityEngine.UI.Text
struct Text_t3069741234;
// UnityEngine.Renderer
struct Renderer_t674913088;
// UnityEngine.XR.iOS.UnityARAnchorManager
struct UnityARAnchorManager_t3624459219;
// UnityEngine.Animation
struct Animation_t1557311624;
// UnityEngine.Light
struct Light_t2513018095;
// UnityScript.Lang.Array
struct Array_t273358690;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2709947369;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2392691131;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1027034615;
// UnityEngine.Texture2D
struct Texture2D_t2870930912;
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t3872385379;
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_t2020264667;
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t4098669274;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t3635081580;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t520968741;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t2633969543;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2976076601;
// UnityEngine.UI.Graphic
struct Graphic_t1406460313;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1819317824;
// UnityEngine.RectTransform
struct RectTransform_t1885177139;
// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t2026169079;

struct Vector3_t596762001 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3896811617_H
#define U3CMODULEU3E_T3896811617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3896811617 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3896811617_H
#ifndef U3CMODULEU3E_T3896811618_H
#define U3CMODULEU3E_T3896811618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3896811618 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3896811618_H
#ifndef UNITYEVENTBASE_T3260807274_H
#define UNITYEVENTBASE_T3260807274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3260807274  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t124164409 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3977637013 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3260807274, ___m_Calls_0)); }
	inline InvokableCallList_t124164409 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t124164409 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t124164409 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3260807274, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3977637013 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3977637013 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3977637013 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3260807274, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3260807274, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3260807274_H
#ifndef UNITYARUTILITY_T2651983316_H
#define UNITYARUTILITY_T2651983316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUtility
struct  UnityARUtility_t2651983316  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider UnityEngine.XR.iOS.UnityARUtility::meshCollider
	MeshCollider_t3960673270 * ___meshCollider_0;
	// UnityEngine.MeshFilter UnityEngine.XR.iOS.UnityARUtility::meshFilter
	MeshFilter_t2678471223 * ___meshFilter_1;

public:
	inline static int32_t get_offset_of_meshCollider_0() { return static_cast<int32_t>(offsetof(UnityARUtility_t2651983316, ___meshCollider_0)); }
	inline MeshCollider_t3960673270 * get_meshCollider_0() const { return ___meshCollider_0; }
	inline MeshCollider_t3960673270 ** get_address_of_meshCollider_0() { return &___meshCollider_0; }
	inline void set_meshCollider_0(MeshCollider_t3960673270 * value)
	{
		___meshCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshCollider_0), value);
	}

	inline static int32_t get_offset_of_meshFilter_1() { return static_cast<int32_t>(offsetof(UnityARUtility_t2651983316, ___meshFilter_1)); }
	inline MeshFilter_t2678471223 * get_meshFilter_1() const { return ___meshFilter_1; }
	inline MeshFilter_t2678471223 ** get_address_of_meshFilter_1() { return &___meshFilter_1; }
	inline void set_meshFilter_1(MeshFilter_t2678471223 * value)
	{
		___meshFilter_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_1), value);
	}
};

struct UnityARUtility_t2651983316_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::planePrefab
	GameObject_t3649338848 * ___planePrefab_2;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARUtility_t2651983316_StaticFields, ___planePrefab_2)); }
	inline GameObject_t3649338848 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t3649338848 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t3649338848 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUTILITY_T2651983316_H
#ifndef UNITYARANCHORMANAGER_T3624459219_H
#define UNITYARANCHORMANAGER_T3624459219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorManager
struct  UnityARAnchorManager_t3624459219  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::planeAnchorMap
	Dictionary_2_t3033730875 * ___planeAnchorMap_0;

public:
	inline static int32_t get_offset_of_planeAnchorMap_0() { return static_cast<int32_t>(offsetof(UnityARAnchorManager_t3624459219, ___planeAnchorMap_0)); }
	inline Dictionary_2_t3033730875 * get_planeAnchorMap_0() const { return ___planeAnchorMap_0; }
	inline Dictionary_2_t3033730875 ** get_address_of_planeAnchorMap_0() { return &___planeAnchorMap_0; }
	inline void set_planeAnchorMap_0(Dictionary_2_t3033730875 * value)
	{
		___planeAnchorMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___planeAnchorMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORMANAGER_T3624459219_H
#ifndef VALUETYPE_T3433162460_H
#define VALUETYPE_T3433162460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3433162460  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_com
{
};
#endif // VALUETYPE_T3433162460_H
#ifndef HSVUTIL_T1113819789_H
#define HSVUTIL_T1113819789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVUtil
struct  HSVUtil_t1113819789  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T1113819789_H
#ifndef UNITYARMATRIXOPS_T180517933_H
#define UNITYARMATRIXOPS_T180517933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrixOps
struct  UnityARMatrixOps_t180517933  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIXOPS_T180517933_H
#ifndef COLOR_T320819310_H
#define COLOR_T320819310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t320819310 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t320819310, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t320819310, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t320819310, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t320819310, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T320819310_H
#ifndef UNITYEVENT_2_T1533210863_H
#define UNITYEVENT_2_T1533210863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct  UnityEvent_2_t1533210863  : public UnityEventBase_t3260807274
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3384890222* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1533210863, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3384890222* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3384890222** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3384890222* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1533210863_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T1306825633_H
#define DRIVENRECTTRANSFORMTRACKER_T1306825633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t1306825633 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T1306825633_H
#ifndef QUATERNION_T3165733013_H
#define QUATERNION_T3165733013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t3165733013 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t3165733013_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t3165733013  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t3165733013_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t3165733013  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t3165733013 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t3165733013  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T3165733013_H
#ifndef VECTOR2_T59524482_H
#define VECTOR2_T59524482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t59524482 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t59524482, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t59524482, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t59524482_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t59524482  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t59524482  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t59524482  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t59524482  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t59524482  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t59524482  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t59524482  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t59524482  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___zeroVector_2)); }
	inline Vector2_t59524482  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t59524482 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t59524482  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___oneVector_3)); }
	inline Vector2_t59524482  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t59524482 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t59524482  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___upVector_4)); }
	inline Vector2_t59524482  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t59524482 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t59524482  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___downVector_5)); }
	inline Vector2_t59524482  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t59524482 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t59524482  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___leftVector_6)); }
	inline Vector2_t59524482  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t59524482 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t59524482  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___rightVector_7)); }
	inline Vector2_t59524482  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t59524482 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t59524482  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t59524482  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t59524482 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t59524482  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t59524482_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t59524482  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t59524482 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t59524482  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T59524482_H
#ifndef ENUM_T473240710_H
#define ENUM_T473240710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t473240710  : public ValueType_t3433162460
{
public:

public:
};

struct Enum_t473240710_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t674980486* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t473240710_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t674980486* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t674980486** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t674980486* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t473240710_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t473240710_marshaled_com
{
};
#endif // ENUM_T473240710_H
#ifndef UNITYEVENT_1_T3808368702_H
#define UNITYEVENT_1_T3808368702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3808368702  : public UnityEventBase_t3260807274
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3384890222* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3808368702, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3384890222* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3384890222** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3384890222* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3808368702_H
#ifndef VOID_T2725935594_H
#define VOID_T2725935594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2725935594 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2725935594_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T596762001_H
#define VECTOR3_T596762001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t596762001 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t596762001_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t596762001  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t596762001  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t596762001  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t596762001  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t596762001  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t596762001  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t596762001  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t596762001  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t596762001  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t596762001  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___zeroVector_4)); }
	inline Vector3_t596762001  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t596762001 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t596762001  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___oneVector_5)); }
	inline Vector3_t596762001  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t596762001 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t596762001  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___upVector_6)); }
	inline Vector3_t596762001  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t596762001 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t596762001  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___downVector_7)); }
	inline Vector3_t596762001  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t596762001 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t596762001  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___leftVector_8)); }
	inline Vector3_t596762001  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t596762001 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t596762001  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___rightVector_9)); }
	inline Vector3_t596762001  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t596762001 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t596762001  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___forwardVector_10)); }
	inline Vector3_t596762001  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t596762001 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t596762001  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___backVector_11)); }
	inline Vector3_t596762001  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t596762001 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t596762001  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t596762001  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t596762001 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t596762001  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t596762001  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t596762001 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t596762001  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T596762001_H
#ifndef UNITYEVENT_3_T876514367_H
#define UNITYEVENT_3_T876514367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct  UnityEvent_3_t876514367  : public UnityEventBase_t3260807274
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3384890222* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t876514367, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3384890222* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3384890222** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3384890222* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T876514367_H
#ifndef VECTOR4_T1376926224_H
#define VECTOR4_T1376926224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t1376926224 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t1376926224, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t1376926224_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t1376926224  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t1376926224  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t1376926224  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t1376926224  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___zeroVector_5)); }
	inline Vector4_t1376926224  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t1376926224 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t1376926224  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___oneVector_6)); }
	inline Vector4_t1376926224  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t1376926224 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t1376926224  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t1376926224  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t1376926224 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t1376926224  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t1376926224_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t1376926224  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t1376926224 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t1376926224  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T1376926224_H
#ifndef MATRIX4X4_T1288378485_H
#define MATRIX4X4_T1288378485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1288378485 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1288378485_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1288378485  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1288378485  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1288378485  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1288378485 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1288378485  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1288378485_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1288378485  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1288378485 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1288378485  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1288378485_H
#ifndef SPRITESTATE_T758977253_H
#define SPRITESTATE_T758977253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t758977253 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t1309550511 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t1309550511 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t1309550511 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t758977253, ___m_HighlightedSprite_0)); }
	inline Sprite_t1309550511 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t1309550511 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t1309550511 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t758977253, ___m_PressedSprite_1)); }
	inline Sprite_t1309550511 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t1309550511 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t1309550511 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t758977253, ___m_DisabledSprite_2)); }
	inline Sprite_t1309550511 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t1309550511 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t1309550511 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t758977253_marshaled_pinvoke
{
	Sprite_t1309550511 * ___m_HighlightedSprite_0;
	Sprite_t1309550511 * ___m_PressedSprite_1;
	Sprite_t1309550511 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t758977253_marshaled_com
{
	Sprite_t1309550511 * ___m_HighlightedSprite_0;
	Sprite_t1309550511 * ___m_PressedSprite_1;
	Sprite_t1309550511 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T758977253_H
#ifndef HSVCOLOR_T3806242730_H
#define HSVCOLOR_T3806242730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HsvColor
struct  HsvColor_t3806242730 
{
public:
	// System.Double HsvColor::H
	double ___H_0;
	// System.Double HsvColor::S
	double ___S_1;
	// System.Double HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t3806242730, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t3806242730, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t3806242730, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T3806242730_H
#ifndef ARSIZE_T2759123995_H
#define ARSIZE_T2759123995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARSize
struct  ARSize_t2759123995 
{
public:
	// System.Double UnityEngine.XR.iOS.ARSize::width
	double ___width_0;
	// System.Double UnityEngine.XR.iOS.ARSize::height
	double ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ARSize_t2759123995, ___width_0)); }
	inline double get_width_0() const { return ___width_0; }
	inline double* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(double value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ARSize_t2759123995, ___height_1)); }
	inline double get_height_1() const { return ___height_1; }
	inline double* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(double value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSIZE_T2759123995_H
#ifndef ARPOINT_T1649490841_H
#define ARPOINT_T1649490841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPoint
struct  ARPoint_t1649490841 
{
public:
	// System.Double UnityEngine.XR.iOS.ARPoint::x
	double ___x_0;
	// System.Double UnityEngine.XR.iOS.ARPoint::y
	double ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ARPoint_t1649490841, ___x_0)); }
	inline double get_x_0() const { return ___x_0; }
	inline double* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(double value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ARPoint_t1649490841, ___y_1)); }
	inline double get_y_1() const { return ___y_1; }
	inline double* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(double value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPOINT_T1649490841_H
#ifndef U24ARRAYTYPEU3D24_T4048413920_H
#define U24ARRAYTYPEU3D24_T4048413920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t4048413920 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t4048413920__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T4048413920_H
#ifndef COLORVALUES_T1727074643_H
#define COLORVALUES_T1727074643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorValues
struct  ColorValues_t1727074643 
{
public:
	// System.Int32 ColorValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorValues_t1727074643, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORVALUES_T1727074643_H
#ifndef COLORCHANGEDEVENT_T347040188_H
#define COLORCHANGEDEVENT_T347040188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChangedEvent
struct  ColorChangedEvent_t347040188  : public UnityEvent_1_t3808368702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGEDEVENT_T347040188_H
#ifndef HSVCHANGEDEVENT_T3546572410_H
#define HSVCHANGEDEVENT_T3546572410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVChangedEvent
struct  HSVChangedEvent_t3546572410  : public UnityEvent_3_t876514367
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCHANGEDEVENT_T3546572410_H
#ifndef AXIS_T2687941421_H
#define AXIS_T2687941421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Axis
struct  Axis_t2687941421 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t2687941421, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T2687941421_H
#ifndef MODE_T3803593892_H
#define MODE_T3803593892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t3803593892 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3803593892, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3803593892_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1316871563_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1316871563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1316871563  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1316871563_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-8E7629AD5AF686202B8CB7C014505C432FFE31E6
	U24ArrayTypeU3D24_t4048413920  ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1316871563_StaticFields, ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0)); }
	inline U24ArrayTypeU3D24_t4048413920  get_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() const { return ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline U24ArrayTypeU3D24_t4048413920 * get_address_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return &___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline void set_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(U24ArrayTypeU3D24_t4048413920  value)
	{
		___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1316871563_H
#ifndef OBJECT_T1008057425_H
#define OBJECT_T1008057425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1008057425  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1008057425, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1008057425_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1008057425_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1008057425_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1008057425_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1008057425_H
#ifndef SELECTIONSTATE_T2653550480_H
#define SELECTIONSTATE_T2653550480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2653550480 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2653550480, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2653550480_H
#ifndef ARPLANEANCHORALIGNMENT_T185787390_H
#define ARPLANEANCHORALIGNMENT_T185787390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t185787390 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t185787390, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T185787390_H
#ifndef ARRECT_T3161395965_H
#define ARRECT_T3161395965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARRect
struct  ARRect_t3161395965 
{
public:
	// UnityEngine.XR.iOS.ARPoint UnityEngine.XR.iOS.ARRect::origin
	ARPoint_t1649490841  ___origin_0;
	// UnityEngine.XR.iOS.ARSize UnityEngine.XR.iOS.ARRect::size
	ARSize_t2759123995  ___size_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(ARRect_t3161395965, ___origin_0)); }
	inline ARPoint_t1649490841  get_origin_0() const { return ___origin_0; }
	inline ARPoint_t1649490841 * get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(ARPoint_t1649490841  value)
	{
		___origin_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ARRect_t3161395965, ___size_1)); }
	inline ARSize_t2759123995  get_size_1() const { return ___size_1; }
	inline ARSize_t2759123995 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(ARSize_t2759123995  value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRECT_T3161395965_H
#ifndef ARTEXTUREHANDLES_T1375402930_H
#define ARTEXTUREHANDLES_T1375402930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTextureHandles
struct  ARTextureHandles_t1375402930 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureY
	IntPtr_t ___textureY_0;
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureCbCr
	IntPtr_t ___textureCbCr_1;

public:
	inline static int32_t get_offset_of_textureY_0() { return static_cast<int32_t>(offsetof(ARTextureHandles_t1375402930, ___textureY_0)); }
	inline IntPtr_t get_textureY_0() const { return ___textureY_0; }
	inline IntPtr_t* get_address_of_textureY_0() { return &___textureY_0; }
	inline void set_textureY_0(IntPtr_t value)
	{
		___textureY_0 = value;
	}

	inline static int32_t get_offset_of_textureCbCr_1() { return static_cast<int32_t>(offsetof(ARTextureHandles_t1375402930, ___textureCbCr_1)); }
	inline IntPtr_t get_textureCbCr_1() const { return ___textureCbCr_1; }
	inline IntPtr_t* get_address_of_textureCbCr_1() { return &___textureCbCr_1; }
	inline void set_textureCbCr_1(IntPtr_t value)
	{
		___textureCbCr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTEXTUREHANDLES_T1375402930_H
#ifndef ARTRACKINGQUALITY_T1141893651_H
#define ARTRACKINGQUALITY_T1141893651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingQuality
struct  ARTrackingQuality_t1141893651 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARTrackingQuality::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingQuality_t1141893651, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGQUALITY_T1141893651_H
#ifndef ARTRACKINGSTATE_T3679923289_H
#define ARTRACKINGSTATE_T3679923289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t3679923289 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t3679923289, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T3679923289_H
#ifndef ARTRACKINGSTATEREASON_T2365446872_H
#define ARTRACKINGSTATEREASON_T2365446872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t2365446872 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t2365446872, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T2365446872_H
#ifndef UNITYARMATRIX4X4_T1132406930_H
#define UNITYARMATRIX4X4_T1132406930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_t1132406930 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_t1376926224  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_t1376926224  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_t1376926224  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_t1376926224  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column0_0)); }
	inline Vector4_t1376926224  get_column0_0() const { return ___column0_0; }
	inline Vector4_t1376926224 * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_t1376926224  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column1_1)); }
	inline Vector4_t1376926224  get_column1_1() const { return ___column1_1; }
	inline Vector4_t1376926224 * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_t1376926224  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column2_2)); }
	inline Vector4_t1376926224  get_column2_2() const { return ___column2_2; }
	inline Vector4_t1376926224 * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_t1376926224  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t1132406930, ___column3_3)); }
	inline Vector4_t1376926224  get_column3_3() const { return ___column3_3; }
	inline Vector4_t1376926224 * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_t1376926224  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_T1132406930_H
#ifndef ARHITTESTRESULTTYPE_T270086150_H
#define ARHITTESTRESULTTYPE_T270086150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResultType
struct  ARHitTestResultType_t270086150 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARHitTestResultType::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARHitTestResultType_t270086150, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARHITTESTRESULTTYPE_T270086150_H
#ifndef DIRECTION_T1434896290_H
#define DIRECTION_T1434896290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Direction
struct  Direction_t1434896290 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1434896290, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1434896290_H
#ifndef UNITYARALIGNMENT_T1226993326_H
#define UNITYARALIGNMENT_T1226993326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAlignment
struct  UnityARAlignment_t1226993326 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARAlignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARAlignment_t1226993326, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARALIGNMENT_T1226993326_H
#ifndef UNITYARPLANEDETECTION_T2335225179_H
#define UNITYARPLANEDETECTION_T2335225179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARPlaneDetection
struct  UnityARPlaneDetection_t2335225179 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARPlaneDetection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARPlaneDetection_t2335225179, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARPLANEDETECTION_T2335225179_H
#ifndef TRANSITION_T327687789_H
#define TRANSITION_T327687789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t327687789 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t327687789, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T327687789_H
#ifndef UNITYARSESSIONRUNOPTION_T2337414342_H
#define UNITYARSESSIONRUNOPTION_T2337414342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionRunOption
struct  UnityARSessionRunOption_t2337414342 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARSessionRunOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARSessionRunOption_t2337414342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONRUNOPTION_T2337414342_H
#ifndef DELEGATE_T69892740_H
#define DELEGATE_T69892740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t69892740  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t421390435 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t69892740, ___data_8)); }
	inline DelegateData_t421390435 * get_data_8() const { return ___data_8; }
	inline DelegateData_t421390435 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t421390435 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T69892740_H
#ifndef DIRECTION_T1918642359_H
#define DIRECTION_T1918642359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t1918642359 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1918642359, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1918642359_H
#ifndef BOXSLIDEREVENT_T2026169079_H
#define BOXSLIDEREVENT_T2026169079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct  BoxSliderEvent_t2026169079  : public UnityEvent_2_t1533210863
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDEREVENT_T2026169079_H
#ifndef COLORBLOCK_T625039033_H
#define COLORBLOCK_T625039033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t625039033 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t320819310  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t320819310  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t320819310  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t320819310  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_NormalColor_0)); }
	inline Color_t320819310  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t320819310 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t320819310  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_HighlightedColor_1)); }
	inline Color_t320819310  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t320819310 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t320819310  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_PressedColor_2)); }
	inline Color_t320819310  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t320819310 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t320819310  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_DisabledColor_3)); }
	inline Color_t320819310  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t320819310 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t320819310  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t625039033, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T625039033_H
#ifndef COMPONENT_T531478471_H
#define COMPONENT_T531478471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t531478471  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T531478471_H
#ifndef NAVIGATION_T2887492880_H
#define NAVIGATION_T2887492880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t2887492880 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3890617260 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3890617260 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3890617260 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3890617260 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_SelectOnUp_1)); }
	inline Selectable_t3890617260 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3890617260 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3890617260 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_SelectOnDown_2)); }
	inline Selectable_t3890617260 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3890617260 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3890617260 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_SelectOnLeft_3)); }
	inline Selectable_t3890617260 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3890617260 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3890617260 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t2887492880, ___m_SelectOnRight_4)); }
	inline Selectable_t3890617260 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3890617260 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3890617260 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t2887492880_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3890617260 * ___m_SelectOnUp_1;
	Selectable_t3890617260 * ___m_SelectOnDown_2;
	Selectable_t3890617260 * ___m_SelectOnLeft_3;
	Selectable_t3890617260 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t2887492880_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3890617260 * ___m_SelectOnUp_1;
	Selectable_t3890617260 * ___m_SelectOnDown_2;
	Selectable_t3890617260 * ___m_SelectOnLeft_3;
	Selectable_t3890617260 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T2887492880_H
#ifndef MULTICASTDELEGATE_T1138444986_H
#define MULTICASTDELEGATE_T1138444986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t1138444986  : public Delegate_t69892740
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t1138444986 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t1138444986 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1138444986, ___prev_9)); }
	inline MulticastDelegate_t1138444986 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t1138444986 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t1138444986 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t1138444986, ___kpm_next_10)); }
	inline MulticastDelegate_t1138444986 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t1138444986 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t1138444986 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T1138444986_H
#ifndef ARPLANEANCHOR_T1843157284_H
#define ARPLANEANCHOR_T1843157284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchor
struct  ARPlaneAnchor_t1843157284 
{
public:
	// System.String UnityEngine.XR.iOS.ARPlaneAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARPlaneAnchor::transform
	Matrix4x4_t1288378485  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.ARPlaneAnchor::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::center
	Vector3_t596762001  ___center_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::extent
	Vector3_t596762001  ___extent_4;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___transform_1)); }
	inline Matrix4x4_t1288378485  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t1288378485 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t1288378485  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___center_3)); }
	inline Vector3_t596762001  get_center_3() const { return ___center_3; }
	inline Vector3_t596762001 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector3_t596762001  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1843157284, ___extent_4)); }
	inline Vector3_t596762001  get_extent_4() const { return ___extent_4; }
	inline Vector3_t596762001 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector3_t596762001  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1843157284_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t1288378485  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t596762001  ___center_3;
	Vector3_t596762001  ___extent_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1843157284_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t1288378485  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t596762001  ___center_3;
	Vector3_t596762001  ___extent_4;
};
#endif // ARPLANEANCHOR_T1843157284_H
#ifndef INTERNAL_UNITYARCAMERA_T1506237410_H
#define INTERNAL_UNITYARCAMERA_T1506237410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.internal_UnityARCamera
struct  internal_UnityARCamera_t1506237410 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::worldTransform
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.internal_UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.internal_UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// System.UInt32 UnityEngine.XR.iOS.internal_UnityARCamera::getPointCloudData
	uint32_t ___getPointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t1132406930  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t1132406930  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t1132406930  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t1132406930  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_4() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t1506237410, ___getPointCloudData_4)); }
	inline uint32_t get_getPointCloudData_4() const { return ___getPointCloudData_4; }
	inline uint32_t* get_address_of_getPointCloudData_4() { return &___getPointCloudData_4; }
	inline void set_getPointCloudData_4(uint32_t value)
	{
		___getPointCloudData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_UNITYARCAMERA_T1506237410_H
#ifndef UNITYARCAMERA_T2376600594_H
#define UNITYARCAMERA_T2376600594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARCamera
struct  UnityARCamera_t2376600594 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::worldTransform
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.Vector3[] UnityEngine.XR.iOS.UnityARCamera::pointCloudData
	Vector3U5BU5D_t4153953548* ___pointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t1132406930  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t1132406930  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t1132406930  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t1132406930  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_pointCloudData_4() { return static_cast<int32_t>(offsetof(UnityARCamera_t2376600594, ___pointCloudData_4)); }
	inline Vector3U5BU5D_t4153953548* get_pointCloudData_4() const { return ___pointCloudData_4; }
	inline Vector3U5BU5D_t4153953548** get_address_of_pointCloudData_4() { return &___pointCloudData_4; }
	inline void set_pointCloudData_4(Vector3U5BU5D_t4153953548* value)
	{
		___pointCloudData_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t2376600594_marshaled_pinvoke
{
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t596762001 * ___pointCloudData_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t2376600594_marshaled_com
{
	UnityARMatrix4x4_t1132406930  ___worldTransform_0;
	UnityARMatrix4x4_t1132406930  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t596762001 * ___pointCloudData_4;
};
#endif // UNITYARCAMERA_T2376600594_H
#ifndef UNITYARANCHORDATA_T1511263172_H
#define UNITYARANCHORDATA_T1511263172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorData
struct  UnityARAnchorData_t1511263172 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARAnchorData::ptrIdentifier
	IntPtr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARAnchorData::transform
	UnityARMatrix4x4_t1132406930  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.UnityARAnchorData::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::center
	Vector4_t1376926224  ___center_3;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::extent
	Vector4_t1376926224  ___extent_4;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___ptrIdentifier_0)); }
	inline IntPtr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline IntPtr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(IntPtr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___transform_1)); }
	inline UnityARMatrix4x4_t1132406930  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t1132406930 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t1132406930  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___center_3)); }
	inline Vector4_t1376926224  get_center_3() const { return ___center_3; }
	inline Vector4_t1376926224 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector4_t1376926224  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1511263172, ___extent_4)); }
	inline Vector4_t1376926224  get_extent_4() const { return ___extent_4; }
	inline Vector4_t1376926224 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector4_t1376926224  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORDATA_T1511263172_H
#ifndef UNITYARHITTESTRESULT_T1002809393_H
#define UNITYARHITTESTRESULT_T1002809393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestResult
struct  UnityARHitTestResult_t1002809393 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.UnityARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.UnityARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARHitTestResult::localTransform
	Matrix4x4_t1288378485  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARHitTestResult::worldTransform
	Matrix4x4_t1288378485  ___worldTransform_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityARHitTestResult::anchor
	IntPtr_t ___anchor_4;
	// System.Boolean UnityEngine.XR.iOS.UnityARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___localTransform_2)); }
	inline Matrix4x4_t1288378485  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t1288378485 * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t1288378485  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___worldTransform_3)); }
	inline Matrix4x4_t1288378485  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t1288378485 * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t1288378485  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchor_4() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___anchor_4)); }
	inline IntPtr_t get_anchor_4() const { return ___anchor_4; }
	inline IntPtr_t* get_address_of_anchor_4() { return &___anchor_4; }
	inline void set_anchor_4(IntPtr_t value)
	{
		___anchor_4 = value;
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t1002809393, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t1002809393_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1288378485  ___localTransform_2;
	Matrix4x4_t1288378485  ___worldTransform_3;
	intptr_t ___anchor_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t1002809393_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t1288378485  ___localTransform_2;
	Matrix4x4_t1288378485  ___worldTransform_3;
	intptr_t ___anchor_4;
	int32_t ___isValid_5;
};
#endif // UNITYARHITTESTRESULT_T1002809393_H
#ifndef ARKITSESSIONCONFIGURATION_T3142208763_H
#define ARKITSESSIONCONFIGURATION_T3142208763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitSessionConfiguration
struct  ARKitSessionConfiguration_t3142208763 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitSessionConfiguration::alignment
	int32_t ___alignment_0;
	// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_2;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t3142208763, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_1() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t3142208763, ___getPointCloudData_1)); }
	inline bool get_getPointCloudData_1() const { return ___getPointCloudData_1; }
	inline bool* get_address_of_getPointCloudData_1() { return &___getPointCloudData_1; }
	inline void set_getPointCloudData_1(bool value)
	{
		___getPointCloudData_1 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_2() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t3142208763, ___enableLightEstimation_2)); }
	inline bool get_enableLightEstimation_2() const { return ___enableLightEstimation_2; }
	inline bool* get_address_of_enableLightEstimation_2() { return &___enableLightEstimation_2; }
	inline void set_enableLightEstimation_2(bool value)
	{
		___enableLightEstimation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t3142208763_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___getPointCloudData_1;
	int32_t ___enableLightEstimation_2;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t3142208763_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___getPointCloudData_1;
	int32_t ___enableLightEstimation_2;
};
#endif // ARKITSESSIONCONFIGURATION_T3142208763_H
#ifndef ARKITWORLDTACKINGSESSIONCONFIGURATION_T2186350340_H
#define ARKITWORLDTACKINGSESSIONCONFIGURATION_T2186350340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct  ARKitWorldTackingSessionConfiguration_t2186350340 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t2186350340, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t2186350340, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t2186350340, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t2186350340, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
#endif // ARKITWORLDTACKINGSESSIONCONFIGURATION_T2186350340_H
#ifndef ARANCHORADDED_T1580091102_H
#define ARANCHORADDED_T1580091102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct  ARAnchorAdded_t1580091102  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORADDED_T1580091102_H
#ifndef INTERNAL_ARANCHORADDED_T3791587037_H
#define INTERNAL_ARANCHORADDED_T3791587037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct  internal_ARAnchorAdded_t3791587037  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORADDED_T3791587037_H
#ifndef INTERNAL_ARFRAMEUPDATE_T2726389622_H
#define INTERNAL_ARFRAMEUPDATE_T2726389622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct  internal_ARFrameUpdate_t2726389622  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFRAMEUPDATE_T2726389622_H
#ifndef ARSESSIONFAILED_T3599980200_H
#define ARSESSIONFAILED_T3599980200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct  ARSessionFailed_t3599980200  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONFAILED_T3599980200_H
#ifndef ARANCHORREMOVED_T3663543047_H
#define ARANCHORREMOVED_T3663543047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct  ARAnchorRemoved_t3663543047  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORREMOVED_T3663543047_H
#ifndef ARANCHORUPDATED_T1492128022_H
#define ARANCHORUPDATED_T1492128022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct  ARAnchorUpdated_t1492128022  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORUPDATED_T1492128022_H
#ifndef BEHAVIOUR_T2200997390_H
#define BEHAVIOUR_T2200997390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2200997390  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2200997390_H
#ifndef ARFRAMEUPDATE_T3333962149_H
#define ARFRAMEUPDATE_T3333962149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct  ARFrameUpdate_t3333962149  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAMEUPDATE_T3333962149_H
#ifndef INTERNAL_ARANCHORUPDATED_T2928809250_H
#define INTERNAL_ARANCHORUPDATED_T2928809250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct  internal_ARAnchorUpdated_t2928809250  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORUPDATED_T2928809250_H
#ifndef UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#define UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct  UnityARSessionNativeInterface_t3869411053  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::m_NativeARSession
	IntPtr_t ___m_NativeARSession_5;

public:
	inline static int32_t get_offset_of_m_NativeARSession_5() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053, ___m_NativeARSession_5)); }
	inline IntPtr_t get_m_NativeARSession_5() const { return ___m_NativeARSession_5; }
	inline IntPtr_t* get_address_of_m_NativeARSession_5() { return &___m_NativeARSession_5; }
	inline void set_m_NativeARSession_5(IntPtr_t value)
	{
		___m_NativeARSession_5 = value;
	}
};

struct UnityARSessionNativeInterface_t3869411053_StaticFields
{
public:
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFrameUpdatedEvent
	ARFrameUpdate_t3333962149 * ___ARFrameUpdatedEvent_0;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorAddedEvent
	ARAnchorAdded_t1580091102 * ___ARAnchorAddedEvent_1;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorUpdatedEvent
	ARAnchorUpdated_t1492128022 * ___ARAnchorUpdatedEvent_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorRemovedEvent
	ARAnchorRemoved_t3663543047 * ___ARAnchorRemovedEvent_3;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionFailedEvent
	ARSessionFailed_t3599980200 * ___ARSessionFailedEvent_4;
	// UnityEngine.XR.iOS.UnityARCamera UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_Camera
	UnityARCamera_t2376600594  ___s_Camera_6;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_UnityARSessionNativeInterface
	UnityARSessionNativeInterface_t3869411053 * ___s_UnityARSessionNativeInterface_7;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache0
	internal_ARFrameUpdate_t2726389622 * ___U3CU3Ef__mgU24cache0_8;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache1
	internal_ARAnchorAdded_t3791587037 * ___U3CU3Ef__mgU24cache1_9;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache2
	internal_ARAnchorUpdated_t2928809250 * ___U3CU3Ef__mgU24cache2_10;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache3
	internal_ARAnchorRemoved_t409959511 * ___U3CU3Ef__mgU24cache3_11;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache4
	ARSessionFailed_t3599980200 * ___U3CU3Ef__mgU24cache4_12;

public:
	inline static int32_t get_offset_of_ARFrameUpdatedEvent_0() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARFrameUpdatedEvent_0)); }
	inline ARFrameUpdate_t3333962149 * get_ARFrameUpdatedEvent_0() const { return ___ARFrameUpdatedEvent_0; }
	inline ARFrameUpdate_t3333962149 ** get_address_of_ARFrameUpdatedEvent_0() { return &___ARFrameUpdatedEvent_0; }
	inline void set_ARFrameUpdatedEvent_0(ARFrameUpdate_t3333962149 * value)
	{
		___ARFrameUpdatedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ARFrameUpdatedEvent_0), value);
	}

	inline static int32_t get_offset_of_ARAnchorAddedEvent_1() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorAddedEvent_1)); }
	inline ARAnchorAdded_t1580091102 * get_ARAnchorAddedEvent_1() const { return ___ARAnchorAddedEvent_1; }
	inline ARAnchorAdded_t1580091102 ** get_address_of_ARAnchorAddedEvent_1() { return &___ARAnchorAddedEvent_1; }
	inline void set_ARAnchorAddedEvent_1(ARAnchorAdded_t1580091102 * value)
	{
		___ARAnchorAddedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorAddedEvent_1), value);
	}

	inline static int32_t get_offset_of_ARAnchorUpdatedEvent_2() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorUpdatedEvent_2)); }
	inline ARAnchorUpdated_t1492128022 * get_ARAnchorUpdatedEvent_2() const { return ___ARAnchorUpdatedEvent_2; }
	inline ARAnchorUpdated_t1492128022 ** get_address_of_ARAnchorUpdatedEvent_2() { return &___ARAnchorUpdatedEvent_2; }
	inline void set_ARAnchorUpdatedEvent_2(ARAnchorUpdated_t1492128022 * value)
	{
		___ARAnchorUpdatedEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorUpdatedEvent_2), value);
	}

	inline static int32_t get_offset_of_ARAnchorRemovedEvent_3() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARAnchorRemovedEvent_3)); }
	inline ARAnchorRemoved_t3663543047 * get_ARAnchorRemovedEvent_3() const { return ___ARAnchorRemovedEvent_3; }
	inline ARAnchorRemoved_t3663543047 ** get_address_of_ARAnchorRemovedEvent_3() { return &___ARAnchorRemovedEvent_3; }
	inline void set_ARAnchorRemovedEvent_3(ARAnchorRemoved_t3663543047 * value)
	{
		___ARAnchorRemovedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorRemovedEvent_3), value);
	}

	inline static int32_t get_offset_of_ARSessionFailedEvent_4() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___ARSessionFailedEvent_4)); }
	inline ARSessionFailed_t3599980200 * get_ARSessionFailedEvent_4() const { return ___ARSessionFailedEvent_4; }
	inline ARSessionFailed_t3599980200 ** get_address_of_ARSessionFailedEvent_4() { return &___ARSessionFailedEvent_4; }
	inline void set_ARSessionFailedEvent_4(ARSessionFailed_t3599980200 * value)
	{
		___ARSessionFailedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionFailedEvent_4), value);
	}

	inline static int32_t get_offset_of_s_Camera_6() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___s_Camera_6)); }
	inline UnityARCamera_t2376600594  get_s_Camera_6() const { return ___s_Camera_6; }
	inline UnityARCamera_t2376600594 * get_address_of_s_Camera_6() { return &___s_Camera_6; }
	inline void set_s_Camera_6(UnityARCamera_t2376600594  value)
	{
		___s_Camera_6 = value;
	}

	inline static int32_t get_offset_of_s_UnityARSessionNativeInterface_7() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___s_UnityARSessionNativeInterface_7)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_s_UnityARSessionNativeInterface_7() const { return ___s_UnityARSessionNativeInterface_7; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_s_UnityARSessionNativeInterface_7() { return &___s_UnityARSessionNativeInterface_7; }
	inline void set_s_UnityARSessionNativeInterface_7(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___s_UnityARSessionNativeInterface_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnityARSessionNativeInterface_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline internal_ARFrameUpdate_t2726389622 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline internal_ARFrameUpdate_t2726389622 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(internal_ARFrameUpdate_t2726389622 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_9() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache1_9)); }
	inline internal_ARAnchorAdded_t3791587037 * get_U3CU3Ef__mgU24cache1_9() const { return ___U3CU3Ef__mgU24cache1_9; }
	inline internal_ARAnchorAdded_t3791587037 ** get_address_of_U3CU3Ef__mgU24cache1_9() { return &___U3CU3Ef__mgU24cache1_9; }
	inline void set_U3CU3Ef__mgU24cache1_9(internal_ARAnchorAdded_t3791587037 * value)
	{
		___U3CU3Ef__mgU24cache1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_10() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache2_10)); }
	inline internal_ARAnchorUpdated_t2928809250 * get_U3CU3Ef__mgU24cache2_10() const { return ___U3CU3Ef__mgU24cache2_10; }
	inline internal_ARAnchorUpdated_t2928809250 ** get_address_of_U3CU3Ef__mgU24cache2_10() { return &___U3CU3Ef__mgU24cache2_10; }
	inline void set_U3CU3Ef__mgU24cache2_10(internal_ARAnchorUpdated_t2928809250 * value)
	{
		___U3CU3Ef__mgU24cache2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_11() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache3_11)); }
	inline internal_ARAnchorRemoved_t409959511 * get_U3CU3Ef__mgU24cache3_11() const { return ___U3CU3Ef__mgU24cache3_11; }
	inline internal_ARAnchorRemoved_t409959511 ** get_address_of_U3CU3Ef__mgU24cache3_11() { return &___U3CU3Ef__mgU24cache3_11; }
	inline void set_U3CU3Ef__mgU24cache3_11(internal_ARAnchorRemoved_t409959511 * value)
	{
		___U3CU3Ef__mgU24cache3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_12() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t3869411053_StaticFields, ___U3CU3Ef__mgU24cache4_12)); }
	inline ARSessionFailed_t3599980200 * get_U3CU3Ef__mgU24cache4_12() const { return ___U3CU3Ef__mgU24cache4_12; }
	inline ARSessionFailed_t3599980200 ** get_address_of_U3CU3Ef__mgU24cache4_12() { return &___U3CU3Ef__mgU24cache4_12; }
	inline void set_U3CU3Ef__mgU24cache4_12(ARSessionFailed_t3599980200 * value)
	{
		___U3CU3Ef__mgU24cache4_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONNATIVEINTERFACE_T3869411053_H
#ifndef INTERNAL_ARANCHORREMOVED_T409959511_H
#define INTERNAL_ARANCHORREMOVED_T409959511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct  internal_ARAnchorRemoved_t409959511  : public MulticastDelegate_t1138444986
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORREMOVED_T409959511_H
#ifndef MONOBEHAVIOUR_T1096588306_H
#define MONOBEHAVIOUR_T1096588306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1096588306  : public Behaviour_t2200997390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1096588306_H
#ifndef POINTCLOUDPARTICLEEXAMPLE_T3205217707_H
#define POINTCLOUDPARTICLEEXAMPLE_T3205217707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudParticleExample
struct  PointCloudParticleExample_t3205217707  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.ParticleSystem PointCloudParticleExample::pointCloudParticlePrefab
	ParticleSystem_t1777616458 * ___pointCloudParticlePrefab_2;
	// System.Int32 PointCloudParticleExample::maxPointsToShow
	int32_t ___maxPointsToShow_3;
	// System.Single PointCloudParticleExample::particleSize
	float ___particleSize_4;
	// UnityEngine.Vector3[] PointCloudParticleExample::m_PointCloudData
	Vector3U5BU5D_t4153953548* ___m_PointCloudData_5;
	// System.Boolean PointCloudParticleExample::frameUpdated
	bool ___frameUpdated_6;
	// UnityEngine.ParticleSystem PointCloudParticleExample::currentPS
	ParticleSystem_t1777616458 * ___currentPS_7;
	// UnityEngine.ParticleSystem/Particle[] PointCloudParticleExample::particles
	ParticleU5BU5D_t419044168* ___particles_8;

public:
	inline static int32_t get_offset_of_pointCloudParticlePrefab_2() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___pointCloudParticlePrefab_2)); }
	inline ParticleSystem_t1777616458 * get_pointCloudParticlePrefab_2() const { return ___pointCloudParticlePrefab_2; }
	inline ParticleSystem_t1777616458 ** get_address_of_pointCloudParticlePrefab_2() { return &___pointCloudParticlePrefab_2; }
	inline void set_pointCloudParticlePrefab_2(ParticleSystem_t1777616458 * value)
	{
		___pointCloudParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_maxPointsToShow_3() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___maxPointsToShow_3)); }
	inline int32_t get_maxPointsToShow_3() const { return ___maxPointsToShow_3; }
	inline int32_t* get_address_of_maxPointsToShow_3() { return &___maxPointsToShow_3; }
	inline void set_maxPointsToShow_3(int32_t value)
	{
		___maxPointsToShow_3 = value;
	}

	inline static int32_t get_offset_of_particleSize_4() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___particleSize_4)); }
	inline float get_particleSize_4() const { return ___particleSize_4; }
	inline float* get_address_of_particleSize_4() { return &___particleSize_4; }
	inline void set_particleSize_4(float value)
	{
		___particleSize_4 = value;
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t4153953548* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t4153953548** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t4153953548* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}

	inline static int32_t get_offset_of_frameUpdated_6() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___frameUpdated_6)); }
	inline bool get_frameUpdated_6() const { return ___frameUpdated_6; }
	inline bool* get_address_of_frameUpdated_6() { return &___frameUpdated_6; }
	inline void set_frameUpdated_6(bool value)
	{
		___frameUpdated_6 = value;
	}

	inline static int32_t get_offset_of_currentPS_7() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___currentPS_7)); }
	inline ParticleSystem_t1777616458 * get_currentPS_7() const { return ___currentPS_7; }
	inline ParticleSystem_t1777616458 ** get_address_of_currentPS_7() { return &___currentPS_7; }
	inline void set_currentPS_7(ParticleSystem_t1777616458 * value)
	{
		___currentPS_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_7), value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3205217707, ___particles_8)); }
	inline ParticleU5BU5D_t419044168* get_particles_8() const { return ___particles_8; }
	inline ParticleU5BU5D_t419044168** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ParticleU5BU5D_t419044168* value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier((&___particles_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDPARTICLEEXAMPLE_T3205217707_H
#ifndef COLORPRESETS_T321881905_H
#define COLORPRESETS_T321881905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPresets
struct  ColorPresets_t321881905  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorPresets::picker
	ColorPicker_t2582382693 * ___picker_2;
	// UnityEngine.GameObject[] ColorPresets::presets
	GameObjectU5BU5D_t1488940705* ___presets_3;
	// UnityEngine.UI.Image ColorPresets::createPresetImage
	Image_t126372159 * ___createPresetImage_4;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorPresets_t321881905, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_presets_3() { return static_cast<int32_t>(offsetof(ColorPresets_t321881905, ___presets_3)); }
	inline GameObjectU5BU5D_t1488940705* get_presets_3() const { return ___presets_3; }
	inline GameObjectU5BU5D_t1488940705** get_address_of_presets_3() { return &___presets_3; }
	inline void set_presets_3(GameObjectU5BU5D_t1488940705* value)
	{
		___presets_3 = value;
		Il2CppCodeGenWriteBarrier((&___presets_3), value);
	}

	inline static int32_t get_offset_of_createPresetImage_4() { return static_cast<int32_t>(offsetof(ColorPresets_t321881905, ___createPresetImage_4)); }
	inline Image_t126372159 * get_createPresetImage_4() const { return ___createPresetImage_4; }
	inline Image_t126372159 ** get_address_of_createPresetImage_4() { return &___createPresetImage_4; }
	inline void set_createPresetImage_4(Image_t126372159 * value)
	{
		___createPresetImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___createPresetImage_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPRESETS_T321881905_H
#ifndef UIBEHAVIOUR_T2441083007_H
#define UIBEHAVIOUR_T2441083007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t2441083007  : public MonoBehaviour_t1096588306
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T2441083007_H
#ifndef UNITYARCAMERAMANAGER_T1803962782_H
#define UNITYARCAMERAMANAGER_T1803962782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraManager
struct  UnityARCameraManager_t1803962782  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Camera UnityARCameraManager::m_camera
	Camera_t226495598 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARCameraManager::m_session
	UnityARSessionNativeInterface_t3869411053 * ___m_session_3;
	// UnityEngine.Material UnityARCameraManager::savedClearMaterial
	Material_t4055262778 * ___savedClearMaterial_4;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t1803962782, ___m_camera_2)); }
	inline Camera_t226495598 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t226495598 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t226495598 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t1803962782, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t1803962782, ___savedClearMaterial_4)); }
	inline Material_t4055262778 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t4055262778 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t4055262778 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERAMANAGER_T1803962782_H
#ifndef COLORPICKER_T2582382693_H
#define COLORPICKER_T2582382693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPicker
struct  ColorPicker_t2582382693  : public MonoBehaviour_t1096588306
{
public:
	// System.Single ColorPicker::_hue
	float ____hue_2;
	// System.Single ColorPicker::_saturation
	float ____saturation_3;
	// System.Single ColorPicker::_brightness
	float ____brightness_4;
	// System.Single ColorPicker::_red
	float ____red_5;
	// System.Single ColorPicker::_green
	float ____green_6;
	// System.Single ColorPicker::_blue
	float ____blue_7;
	// System.Single ColorPicker::_alpha
	float ____alpha_8;
	// ColorChangedEvent ColorPicker::onValueChanged
	ColorChangedEvent_t347040188 * ___onValueChanged_9;
	// HSVChangedEvent ColorPicker::onHSVChanged
	HSVChangedEvent_t3546572410 * ___onHSVChanged_10;

public:
	inline static int32_t get_offset_of__hue_2() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____hue_2)); }
	inline float get__hue_2() const { return ____hue_2; }
	inline float* get_address_of__hue_2() { return &____hue_2; }
	inline void set__hue_2(float value)
	{
		____hue_2 = value;
	}

	inline static int32_t get_offset_of__saturation_3() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____saturation_3)); }
	inline float get__saturation_3() const { return ____saturation_3; }
	inline float* get_address_of__saturation_3() { return &____saturation_3; }
	inline void set__saturation_3(float value)
	{
		____saturation_3 = value;
	}

	inline static int32_t get_offset_of__brightness_4() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____brightness_4)); }
	inline float get__brightness_4() const { return ____brightness_4; }
	inline float* get_address_of__brightness_4() { return &____brightness_4; }
	inline void set__brightness_4(float value)
	{
		____brightness_4 = value;
	}

	inline static int32_t get_offset_of__red_5() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____red_5)); }
	inline float get__red_5() const { return ____red_5; }
	inline float* get_address_of__red_5() { return &____red_5; }
	inline void set__red_5(float value)
	{
		____red_5 = value;
	}

	inline static int32_t get_offset_of__green_6() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____green_6)); }
	inline float get__green_6() const { return ____green_6; }
	inline float* get_address_of__green_6() { return &____green_6; }
	inline void set__green_6(float value)
	{
		____green_6 = value;
	}

	inline static int32_t get_offset_of__blue_7() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____blue_7)); }
	inline float get__blue_7() const { return ____blue_7; }
	inline float* get_address_of__blue_7() { return &____blue_7; }
	inline void set__blue_7(float value)
	{
		____blue_7 = value;
	}

	inline static int32_t get_offset_of__alpha_8() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ____alpha_8)); }
	inline float get__alpha_8() const { return ____alpha_8; }
	inline float* get_address_of__alpha_8() { return &____alpha_8; }
	inline void set__alpha_8(float value)
	{
		____alpha_8 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_9() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ___onValueChanged_9)); }
	inline ColorChangedEvent_t347040188 * get_onValueChanged_9() const { return ___onValueChanged_9; }
	inline ColorChangedEvent_t347040188 ** get_address_of_onValueChanged_9() { return &___onValueChanged_9; }
	inline void set_onValueChanged_9(ColorChangedEvent_t347040188 * value)
	{
		___onValueChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_9), value);
	}

	inline static int32_t get_offset_of_onHSVChanged_10() { return static_cast<int32_t>(offsetof(ColorPicker_t2582382693, ___onHSVChanged_10)); }
	inline HSVChangedEvent_t3546572410 * get_onHSVChanged_10() const { return ___onHSVChanged_10; }
	inline HSVChangedEvent_t3546572410 ** get_address_of_onHSVChanged_10() { return &___onHSVChanged_10; }
	inline void set_onHSVChanged_10(HSVChangedEvent_t3546572410 * value)
	{
		___onHSVChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___onHSVChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKER_T2582382693_H
#ifndef UNITYARCAMERANEARFAR_T4152080291_H
#define UNITYARCAMERANEARFAR_T4152080291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraNearFar
struct  UnityARCameraNearFar_t4152080291  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Camera UnityARCameraNearFar::attachedCamera
	Camera_t226495598 * ___attachedCamera_2;
	// System.Single UnityARCameraNearFar::currentNearZ
	float ___currentNearZ_3;
	// System.Single UnityARCameraNearFar::currentFarZ
	float ___currentFarZ_4;

public:
	inline static int32_t get_offset_of_attachedCamera_2() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t4152080291, ___attachedCamera_2)); }
	inline Camera_t226495598 * get_attachedCamera_2() const { return ___attachedCamera_2; }
	inline Camera_t226495598 ** get_address_of_attachedCamera_2() { return &___attachedCamera_2; }
	inline void set_attachedCamera_2(Camera_t226495598 * value)
	{
		___attachedCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachedCamera_2), value);
	}

	inline static int32_t get_offset_of_currentNearZ_3() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t4152080291, ___currentNearZ_3)); }
	inline float get_currentNearZ_3() const { return ___currentNearZ_3; }
	inline float* get_address_of_currentNearZ_3() { return &___currentNearZ_3; }
	inline void set_currentNearZ_3(float value)
	{
		___currentNearZ_3 = value;
	}

	inline static int32_t get_offset_of_currentFarZ_4() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t4152080291, ___currentFarZ_4)); }
	inline float get_currentFarZ_4() const { return ___currentFarZ_4; }
	inline float* get_address_of_currentFarZ_4() { return &___currentFarZ_4; }
	inline void set_currentFarZ_4(float value)
	{
		___currentFarZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERANEARFAR_T4152080291_H
#ifndef UNITYARHITTESTEXAMPLE_T2411672648_H
#define UNITYARHITTESTEXAMPLE_T2411672648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestExample
struct  UnityARHitTestExample_t2411672648  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.UnityARHitTestExample::m_HitTransform
	Transform_t3933397867 * ___m_HitTransform_2;

public:
	inline static int32_t get_offset_of_m_HitTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t2411672648, ___m_HitTransform_2)); }
	inline Transform_t3933397867 * get_m_HitTransform_2() const { return ___m_HitTransform_2; }
	inline Transform_t3933397867 ** get_address_of_m_HitTransform_2() { return &___m_HitTransform_2; }
	inline void set_m_HitTransform_2(Transform_t3933397867 * value)
	{
		___m_HitTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARHITTESTEXAMPLE_T2411672648_H
#ifndef COLORSLIDER_T4061812907_H
#define COLORSLIDER_T4061812907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSlider
struct  ColorSlider_t4061812907  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorSlider::hsvpicker
	ColorPicker_t2582382693 * ___hsvpicker_2;
	// ColorValues ColorSlider::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider ColorSlider::slider
	Slider_t301630380 * ___slider_4;
	// System.Boolean ColorSlider::listen
	bool ___listen_5;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(ColorSlider_t4061812907, ___hsvpicker_2)); }
	inline ColorPicker_t2582382693 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t2582382693 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSlider_t4061812907, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(ColorSlider_t4061812907, ___slider_4)); }
	inline Slider_t301630380 * get_slider_4() const { return ___slider_4; }
	inline Slider_t301630380 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t301630380 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_listen_5() { return static_cast<int32_t>(offsetof(ColorSlider_t4061812907, ___listen_5)); }
	inline bool get_listen_5() const { return ___listen_5; }
	inline bool* get_address_of_listen_5() { return &___listen_5; }
	inline void set_listen_5(bool value)
	{
		___listen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDER_T4061812907_H
#ifndef COLORSLIDERIMAGE_T664667956_H
#define COLORSLIDERIMAGE_T664667956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSliderImage
struct  ColorSliderImage_t664667956  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorSliderImage::picker
	ColorPicker_t2582382693 * ___picker_2;
	// ColorValues ColorSliderImage::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider/Direction ColorSliderImage::direction
	int32_t ___direction_4;
	// UnityEngine.UI.RawImage ColorSliderImage::image
	RawImage_t3037409423 * ___image_5;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorSliderImage_t664667956, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSliderImage_t664667956, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(ColorSliderImage_t664667956, ___direction_4)); }
	inline int32_t get_direction_4() const { return ___direction_4; }
	inline int32_t* get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(int32_t value)
	{
		___direction_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ColorSliderImage_t664667956, ___image_5)); }
	inline RawImage_t3037409423 * get_image_5() const { return ___image_5; }
	inline RawImage_t3037409423 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t3037409423 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDERIMAGE_T664667956_H
#ifndef HEXCOLORFIELD_T1392034802_H
#define HEXCOLORFIELD_T1392034802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HexColorField
struct  HexColorField_t1392034802  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker HexColorField::hsvpicker
	ColorPicker_t2582382693 * ___hsvpicker_2;
	// System.Boolean HexColorField::displayAlpha
	bool ___displayAlpha_3;
	// UnityEngine.UI.InputField HexColorField::hexInputField
	InputField_t3161868630 * ___hexInputField_4;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(HexColorField_t1392034802, ___hsvpicker_2)); }
	inline ColorPicker_t2582382693 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t2582382693 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_displayAlpha_3() { return static_cast<int32_t>(offsetof(HexColorField_t1392034802, ___displayAlpha_3)); }
	inline bool get_displayAlpha_3() const { return ___displayAlpha_3; }
	inline bool* get_address_of_displayAlpha_3() { return &___displayAlpha_3; }
	inline void set_displayAlpha_3(bool value)
	{
		___displayAlpha_3 = value;
	}

	inline static int32_t get_offset_of_hexInputField_4() { return static_cast<int32_t>(offsetof(HexColorField_t1392034802, ___hexInputField_4)); }
	inline InputField_t3161868630 * get_hexInputField_4() const { return ___hexInputField_4; }
	inline InputField_t3161868630 ** get_address_of_hexInputField_4() { return &___hexInputField_4; }
	inline void set_hexInputField_4(InputField_t3161868630 * value)
	{
		___hexInputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___hexInputField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXCOLORFIELD_T1392034802_H
#ifndef SVBOXSLIDER_T2769455468_H
#define SVBOXSLIDER_T2769455468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVBoxSlider
struct  SVBoxSlider_t2769455468  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker SVBoxSlider::picker
	ColorPicker_t2582382693 * ___picker_2;
	// UnityEngine.UI.BoxSlider SVBoxSlider::slider
	BoxSlider_t2780260910 * ___slider_3;
	// UnityEngine.UI.RawImage SVBoxSlider::image
	RawImage_t3037409423 * ___image_4;
	// System.Single SVBoxSlider::lastH
	float ___lastH_5;
	// System.Boolean SVBoxSlider::listen
	bool ___listen_6;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___slider_3)); }
	inline BoxSlider_t2780260910 * get_slider_3() const { return ___slider_3; }
	inline BoxSlider_t2780260910 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(BoxSlider_t2780260910 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier((&___slider_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___image_4)); }
	inline RawImage_t3037409423 * get_image_4() const { return ___image_4; }
	inline RawImage_t3037409423 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(RawImage_t3037409423 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_lastH_5() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___lastH_5)); }
	inline float get_lastH_5() const { return ___lastH_5; }
	inline float* get_address_of_lastH_5() { return &___lastH_5; }
	inline void set_lastH_5(float value)
	{
		___lastH_5 = value;
	}

	inline static int32_t get_offset_of_listen_6() { return static_cast<int32_t>(offsetof(SVBoxSlider_t2769455468, ___listen_6)); }
	inline bool get_listen_6() const { return ___listen_6; }
	inline bool* get_address_of_listen_6() { return &___listen_6; }
	inline void set_listen_6(bool value)
	{
		___listen_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVBOXSLIDER_T2769455468_H
#ifndef COLORLABEL_T538373442_H
#define COLORLABEL_T538373442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorLabel
struct  ColorLabel_t538373442  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorLabel::picker
	ColorPicker_t2582382693 * ___picker_2;
	// ColorValues ColorLabel::type
	int32_t ___type_3;
	// System.String ColorLabel::prefix
	String_t* ___prefix_4;
	// System.Single ColorLabel::minValue
	float ___minValue_5;
	// System.Single ColorLabel::maxValue
	float ___maxValue_6;
	// System.Int32 ColorLabel::precision
	int32_t ___precision_7;
	// UnityEngine.UI.Text ColorLabel::label
	Text_t3069741234 * ___label_8;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_prefix_4() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___prefix_4)); }
	inline String_t* get_prefix_4() const { return ___prefix_4; }
	inline String_t** get_address_of_prefix_4() { return &___prefix_4; }
	inline void set_prefix_4(String_t* value)
	{
		___prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_4), value);
	}

	inline static int32_t get_offset_of_minValue_5() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___minValue_5)); }
	inline float get_minValue_5() const { return ___minValue_5; }
	inline float* get_address_of_minValue_5() { return &___minValue_5; }
	inline void set_minValue_5(float value)
	{
		___minValue_5 = value;
	}

	inline static int32_t get_offset_of_maxValue_6() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___maxValue_6)); }
	inline float get_maxValue_6() const { return ___maxValue_6; }
	inline float* get_address_of_maxValue_6() { return &___maxValue_6; }
	inline void set_maxValue_6(float value)
	{
		___maxValue_6 = value;
	}

	inline static int32_t get_offset_of_precision_7() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___precision_7)); }
	inline int32_t get_precision_7() const { return ___precision_7; }
	inline int32_t* get_address_of_precision_7() { return &___precision_7; }
	inline void set_precision_7(int32_t value)
	{
		___precision_7 = value;
	}

	inline static int32_t get_offset_of_label_8() { return static_cast<int32_t>(offsetof(ColorLabel_t538373442, ___label_8)); }
	inline Text_t3069741234 * get_label_8() const { return ___label_8; }
	inline Text_t3069741234 ** get_address_of_label_8() { return &___label_8; }
	inline void set_label_8(Text_t3069741234 * value)
	{
		___label_8 = value;
		Il2CppCodeGenWriteBarrier((&___label_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLABEL_T538373442_H
#ifndef COLORIMAGE_T2737809041_H
#define COLORIMAGE_T2737809041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorImage
struct  ColorImage_t2737809041  : public MonoBehaviour_t1096588306
{
public:
	// ColorPicker ColorImage::picker
	ColorPicker_t2582382693 * ___picker_2;
	// UnityEngine.UI.Image ColorImage::image
	Image_t126372159 * ___image_3;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorImage_t2737809041, ___picker_2)); }
	inline ColorPicker_t2582382693 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t2582382693 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(ColorImage_t2737809041, ___image_3)); }
	inline Image_t126372159 * get_image_3() const { return ___image_3; }
	inline Image_t126372159 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t126372159 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMAGE_T2737809041_H
#ifndef COLORPICKERTESTER_T3436045112_H
#define COLORPICKERTESTER_T3436045112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPickerTester
struct  ColorPickerTester_t3436045112  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Renderer ColorPickerTester::renderer
	Renderer_t674913088 * ___renderer_2;
	// ColorPicker ColorPickerTester::picker
	ColorPicker_t2582382693 * ___picker_3;

public:
	inline static int32_t get_offset_of_renderer_2() { return static_cast<int32_t>(offsetof(ColorPickerTester_t3436045112, ___renderer_2)); }
	inline Renderer_t674913088 * get_renderer_2() const { return ___renderer_2; }
	inline Renderer_t674913088 ** get_address_of_renderer_2() { return &___renderer_2; }
	inline void set_renderer_2(Renderer_t674913088 * value)
	{
		___renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_2), value);
	}

	inline static int32_t get_offset_of_picker_3() { return static_cast<int32_t>(offsetof(ColorPickerTester_t3436045112, ___picker_3)); }
	inline ColorPicker_t2582382693 * get_picker_3() const { return ___picker_3; }
	inline ColorPicker_t2582382693 ** get_address_of_picker_3() { return &___picker_3; }
	inline void set_picker_3(ColorPicker_t2582382693 * value)
	{
		___picker_3 = value;
		Il2CppCodeGenWriteBarrier((&___picker_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERTESTER_T3436045112_H
#ifndef MODESWITCHER_T833699841_H
#define MODESWITCHER_T833699841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModeSwitcher
struct  ModeSwitcher_t833699841  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.GameObject ModeSwitcher::ballMake
	GameObject_t3649338848 * ___ballMake_2;
	// UnityEngine.GameObject ModeSwitcher::ballMove
	GameObject_t3649338848 * ___ballMove_3;
	// System.Int32 ModeSwitcher::appMode
	int32_t ___appMode_4;

public:
	inline static int32_t get_offset_of_ballMake_2() { return static_cast<int32_t>(offsetof(ModeSwitcher_t833699841, ___ballMake_2)); }
	inline GameObject_t3649338848 * get_ballMake_2() const { return ___ballMake_2; }
	inline GameObject_t3649338848 ** get_address_of_ballMake_2() { return &___ballMake_2; }
	inline void set_ballMake_2(GameObject_t3649338848 * value)
	{
		___ballMake_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballMake_2), value);
	}

	inline static int32_t get_offset_of_ballMove_3() { return static_cast<int32_t>(offsetof(ModeSwitcher_t833699841, ___ballMove_3)); }
	inline GameObject_t3649338848 * get_ballMove_3() const { return ___ballMove_3; }
	inline GameObject_t3649338848 ** get_address_of_ballMove_3() { return &___ballMove_3; }
	inline void set_ballMove_3(GameObject_t3649338848 * value)
	{
		___ballMove_3 = value;
		Il2CppCodeGenWriteBarrier((&___ballMove_3), value);
	}

	inline static int32_t get_offset_of_appMode_4() { return static_cast<int32_t>(offsetof(ModeSwitcher_t833699841, ___appMode_4)); }
	inline int32_t get_appMode_4() const { return ___appMode_4; }
	inline int32_t* get_address_of_appMode_4() { return &___appMode_4; }
	inline void set_appMode_4(int32_t value)
	{
		___appMode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODESWITCHER_T833699841_H
#ifndef BALLZ_T2183783589_H
#define BALLZ_T2183783589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ballz
struct  Ballz_t2183783589  : public MonoBehaviour_t1096588306
{
public:
	// System.Single Ballz::yDistanceThreshold
	float ___yDistanceThreshold_2;
	// System.Single Ballz::startingY
	float ___startingY_3;

public:
	inline static int32_t get_offset_of_yDistanceThreshold_2() { return static_cast<int32_t>(offsetof(Ballz_t2183783589, ___yDistanceThreshold_2)); }
	inline float get_yDistanceThreshold_2() const { return ___yDistanceThreshold_2; }
	inline float* get_address_of_yDistanceThreshold_2() { return &___yDistanceThreshold_2; }
	inline void set_yDistanceThreshold_2(float value)
	{
		___yDistanceThreshold_2 = value;
	}

	inline static int32_t get_offset_of_startingY_3() { return static_cast<int32_t>(offsetof(Ballz_t2183783589, ___startingY_3)); }
	inline float get_startingY_3() const { return ___startingY_3; }
	inline float* get_address_of_startingY_3() { return &___startingY_3; }
	inline void set_startingY_3(float value)
	{
		___startingY_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLZ_T2183783589_H
#ifndef UNITYARGENERATEPLANE_T3109641619_H
#define UNITYARGENERATEPLANE_T3109641619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARGeneratePlane
struct  UnityARGeneratePlane_t3109641619  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARGeneratePlane::planePrefab
	GameObject_t3649338848 * ___planePrefab_2;
	// UnityEngine.XR.iOS.UnityARAnchorManager UnityEngine.XR.iOS.UnityARGeneratePlane::unityARAnchorManager
	UnityARAnchorManager_t3624459219 * ___unityARAnchorManager_3;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3109641619, ___planePrefab_2)); }
	inline GameObject_t3649338848 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t3649338848 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t3649338848 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}

	inline static int32_t get_offset_of_unityARAnchorManager_3() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3109641619, ___unityARAnchorManager_3)); }
	inline UnityARAnchorManager_t3624459219 * get_unityARAnchorManager_3() const { return ___unityARAnchorManager_3; }
	inline UnityARAnchorManager_t3624459219 ** get_address_of_unityARAnchorManager_3() { return &___unityARAnchorManager_3; }
	inline void set_unityARAnchorManager_3(UnityARAnchorManager_t3624459219 * value)
	{
		___unityARAnchorManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___unityARAnchorManager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARGENERATEPLANE_T3109641619_H
#ifndef KNIGHTCONTROL_T1902388948_H
#define KNIGHTCONTROL_T1902388948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KnightControl
struct  KnightControl_t1902388948  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Animation KnightControl::animation
	Animation_t1557311624 * ___animation_2;
	// System.Boolean KnightControl::detectedMonster
	bool ___detectedMonster_3;
	// System.Boolean KnightControl::moveForward
	bool ___moveForward_4;
	// System.Boolean KnightControl::moveReverse
	bool ___moveReverse_5;
	// System.Boolean KnightControl::KnightDeath
	bool ___KnightDeath_6;

public:
	inline static int32_t get_offset_of_animation_2() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___animation_2)); }
	inline Animation_t1557311624 * get_animation_2() const { return ___animation_2; }
	inline Animation_t1557311624 ** get_address_of_animation_2() { return &___animation_2; }
	inline void set_animation_2(Animation_t1557311624 * value)
	{
		___animation_2 = value;
		Il2CppCodeGenWriteBarrier((&___animation_2), value);
	}

	inline static int32_t get_offset_of_detectedMonster_3() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___detectedMonster_3)); }
	inline bool get_detectedMonster_3() const { return ___detectedMonster_3; }
	inline bool* get_address_of_detectedMonster_3() { return &___detectedMonster_3; }
	inline void set_detectedMonster_3(bool value)
	{
		___detectedMonster_3 = value;
	}

	inline static int32_t get_offset_of_moveForward_4() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___moveForward_4)); }
	inline bool get_moveForward_4() const { return ___moveForward_4; }
	inline bool* get_address_of_moveForward_4() { return &___moveForward_4; }
	inline void set_moveForward_4(bool value)
	{
		___moveForward_4 = value;
	}

	inline static int32_t get_offset_of_moveReverse_5() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___moveReverse_5)); }
	inline bool get_moveReverse_5() const { return ___moveReverse_5; }
	inline bool* get_address_of_moveReverse_5() { return &___moveReverse_5; }
	inline void set_moveReverse_5(bool value)
	{
		___moveReverse_5 = value;
	}

	inline static int32_t get_offset_of_KnightDeath_6() { return static_cast<int32_t>(offsetof(KnightControl_t1902388948, ___KnightDeath_6)); }
	inline bool get_KnightDeath_6() const { return ___KnightDeath_6; }
	inline bool* get_address_of_KnightDeath_6() { return &___KnightDeath_6; }
	inline void set_KnightDeath_6(bool value)
	{
		___KnightDeath_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNIGHTCONTROL_T1902388948_H
#ifndef SLIMECONTROL_T1232093680_H
#define SLIMECONTROL_T1232093680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SlimeControl
struct  SlimeControl_t1232093680  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Animation SlimeControl::animation
	Animation_t1557311624 * ___animation_2;
	// System.Boolean SlimeControl::shouldMove
	bool ___shouldMove_3;
	// System.Boolean SlimeControl::SlimeDeath
	bool ___SlimeDeath_4;

public:
	inline static int32_t get_offset_of_animation_2() { return static_cast<int32_t>(offsetof(SlimeControl_t1232093680, ___animation_2)); }
	inline Animation_t1557311624 * get_animation_2() const { return ___animation_2; }
	inline Animation_t1557311624 ** get_address_of_animation_2() { return &___animation_2; }
	inline void set_animation_2(Animation_t1557311624 * value)
	{
		___animation_2 = value;
		Il2CppCodeGenWriteBarrier((&___animation_2), value);
	}

	inline static int32_t get_offset_of_shouldMove_3() { return static_cast<int32_t>(offsetof(SlimeControl_t1232093680, ___shouldMove_3)); }
	inline bool get_shouldMove_3() const { return ___shouldMove_3; }
	inline bool* get_address_of_shouldMove_3() { return &___shouldMove_3; }
	inline void set_shouldMove_3(bool value)
	{
		___shouldMove_3 = value;
	}

	inline static int32_t get_offset_of_SlimeDeath_4() { return static_cast<int32_t>(offsetof(SlimeControl_t1232093680, ___SlimeDeath_4)); }
	inline bool get_SlimeDeath_4() const { return ___SlimeDeath_4; }
	inline bool* get_address_of_SlimeDeath_4() { return &___SlimeDeath_4; }
	inline void set_SlimeDeath_4(bool value)
	{
		___SlimeDeath_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIMECONTROL_T1232093680_H
#ifndef UNITYARAMBIENT_T3389999931_H
#define UNITYARAMBIENT_T3389999931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAmbient
struct  UnityARAmbient_t3389999931  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Light UnityEngine.XR.iOS.UnityARAmbient::l
	Light_t2513018095 * ___l_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARAmbient::m_Session
	UnityARSessionNativeInterface_t3869411053 * ___m_Session_3;

public:
	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(UnityARAmbient_t3389999931, ___l_2)); }
	inline Light_t2513018095 * get_l_2() const { return ___l_2; }
	inline Light_t2513018095 ** get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(Light_t2513018095 * value)
	{
		___l_2 = value;
		Il2CppCodeGenWriteBarrier((&___l_2), value);
	}

	inline static int32_t get_offset_of_m_Session_3() { return static_cast<int32_t>(offsetof(UnityARAmbient_t3389999931, ___m_Session_3)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_m_Session_3() const { return ___m_Session_3; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_m_Session_3() { return &___m_Session_3; }
	inline void set_m_Session_3(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___m_Session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARAMBIENT_T3389999931_H
#ifndef BALLMOVER_T4283490589_H
#define BALLMOVER_T4283490589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMover
struct  BallMover_t4283490589  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.GameObject BallMover::collBallPrefab
	GameObject_t3649338848 * ___collBallPrefab_2;
	// UnityEngine.GameObject BallMover::collBallGO
	GameObject_t3649338848 * ___collBallGO_3;

public:
	inline static int32_t get_offset_of_collBallPrefab_2() { return static_cast<int32_t>(offsetof(BallMover_t4283490589, ___collBallPrefab_2)); }
	inline GameObject_t3649338848 * get_collBallPrefab_2() const { return ___collBallPrefab_2; }
	inline GameObject_t3649338848 ** get_address_of_collBallPrefab_2() { return &___collBallPrefab_2; }
	inline void set_collBallPrefab_2(GameObject_t3649338848 * value)
	{
		___collBallPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___collBallPrefab_2), value);
	}

	inline static int32_t get_offset_of_collBallGO_3() { return static_cast<int32_t>(offsetof(BallMover_t4283490589, ___collBallGO_3)); }
	inline GameObject_t3649338848 * get_collBallGO_3() const { return ___collBallGO_3; }
	inline GameObject_t3649338848 ** get_address_of_collBallGO_3() { return &___collBallGO_3; }
	inline void set_collBallGO_3(GameObject_t3649338848 * value)
	{
		___collBallGO_3 = value;
		Il2CppCodeGenWriteBarrier((&___collBallGO_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMOVER_T4283490589_H
#ifndef CHARAVIEW_T926653685_H
#define CHARAVIEW_T926653685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharaView
struct  CharaView_t926653685  : public MonoBehaviour_t1096588306
{
public:
	// System.Single CharaView::roteSpeed
	float ___roteSpeed_2;
	// System.Single CharaView::animationSpeed
	float ___animationSpeed_3;
	// System.UInt32 CharaView::animationCount
	uint32_t ___animationCount_4;
	// UnityScript.Lang.Array CharaView::animationList
	Array_t273358690 * ___animationList_5;

public:
	inline static int32_t get_offset_of_roteSpeed_2() { return static_cast<int32_t>(offsetof(CharaView_t926653685, ___roteSpeed_2)); }
	inline float get_roteSpeed_2() const { return ___roteSpeed_2; }
	inline float* get_address_of_roteSpeed_2() { return &___roteSpeed_2; }
	inline void set_roteSpeed_2(float value)
	{
		___roteSpeed_2 = value;
	}

	inline static int32_t get_offset_of_animationSpeed_3() { return static_cast<int32_t>(offsetof(CharaView_t926653685, ___animationSpeed_3)); }
	inline float get_animationSpeed_3() const { return ___animationSpeed_3; }
	inline float* get_address_of_animationSpeed_3() { return &___animationSpeed_3; }
	inline void set_animationSpeed_3(float value)
	{
		___animationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_animationCount_4() { return static_cast<int32_t>(offsetof(CharaView_t926653685, ___animationCount_4)); }
	inline uint32_t get_animationCount_4() const { return ___animationCount_4; }
	inline uint32_t* get_address_of_animationCount_4() { return &___animationCount_4; }
	inline void set_animationCount_4(uint32_t value)
	{
		___animationCount_4 = value;
	}

	inline static int32_t get_offset_of_animationList_5() { return static_cast<int32_t>(offsetof(CharaView_t926653685, ___animationList_5)); }
	inline Array_t273358690 * get_animationList_5() const { return ___animationList_5; }
	inline Array_t273358690 ** get_address_of_animationList_5() { return &___animationList_5; }
	inline void set_animationList_5(Array_t273358690 * value)
	{
		___animationList_5 = value;
		Il2CppCodeGenWriteBarrier((&___animationList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARAVIEW_T926653685_H
#ifndef BALLMAKER_T1786680372_H
#define BALLMAKER_T1786680372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMaker
struct  BallMaker_t1786680372  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.GameObject BallMaker::ballPrefab
	GameObject_t3649338848 * ___ballPrefab_2;
	// System.Single BallMaker::createHeight
	float ___createHeight_3;
	// UnityEngine.MaterialPropertyBlock BallMaker::props
	MaterialPropertyBlock_t2709947369 * ___props_4;

public:
	inline static int32_t get_offset_of_ballPrefab_2() { return static_cast<int32_t>(offsetof(BallMaker_t1786680372, ___ballPrefab_2)); }
	inline GameObject_t3649338848 * get_ballPrefab_2() const { return ___ballPrefab_2; }
	inline GameObject_t3649338848 ** get_address_of_ballPrefab_2() { return &___ballPrefab_2; }
	inline void set_ballPrefab_2(GameObject_t3649338848 * value)
	{
		___ballPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballPrefab_2), value);
	}

	inline static int32_t get_offset_of_createHeight_3() { return static_cast<int32_t>(offsetof(BallMaker_t1786680372, ___createHeight_3)); }
	inline float get_createHeight_3() const { return ___createHeight_3; }
	inline float* get_address_of_createHeight_3() { return &___createHeight_3; }
	inline void set_createHeight_3(float value)
	{
		___createHeight_3 = value;
	}

	inline static int32_t get_offset_of_props_4() { return static_cast<int32_t>(offsetof(BallMaker_t1786680372, ___props_4)); }
	inline MaterialPropertyBlock_t2709947369 * get_props_4() const { return ___props_4; }
	inline MaterialPropertyBlock_t2709947369 ** get_address_of_props_4() { return &___props_4; }
	inline void set_props_4(MaterialPropertyBlock_t2709947369 * value)
	{
		___props_4 = value;
		Il2CppCodeGenWriteBarrier((&___props_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMAKER_T1786680372_H
#ifndef SCREENSHOT_T707795496_H
#define SCREENSHOT_T707795496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenShot
struct  ScreenShot_t707795496  : public MonoBehaviour_t1096588306
{
public:
	// System.String ScreenShot::format
	String_t* ___format_2;

public:
	inline static int32_t get_offset_of_format_2() { return static_cast<int32_t>(offsetof(ScreenShot_t707795496, ___format_2)); }
	inline String_t* get_format_2() const { return ___format_2; }
	inline String_t** get_address_of_format_2() { return &___format_2; }
	inline void set_format_2(String_t* value)
	{
		___format_2 = value;
		Il2CppCodeGenWriteBarrier((&___format_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOT_T707795496_H
#ifndef UNITYPOINTCLOUDEXAMPLE_T595762290_H
#define UNITYPOINTCLOUDEXAMPLE_T595762290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityPointCloudExample
struct  UnityPointCloudExample_t595762290  : public MonoBehaviour_t1096588306
{
public:
	// System.UInt32 UnityPointCloudExample::numPointsToShow
	uint32_t ___numPointsToShow_2;
	// UnityEngine.GameObject UnityPointCloudExample::PointCloudPrefab
	GameObject_t3649338848 * ___PointCloudPrefab_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityPointCloudExample::pointCloudObjects
	List_1_t2392691131 * ___pointCloudObjects_4;
	// UnityEngine.Vector3[] UnityPointCloudExample::m_PointCloudData
	Vector3U5BU5D_t4153953548* ___m_PointCloudData_5;

public:
	inline static int32_t get_offset_of_numPointsToShow_2() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t595762290, ___numPointsToShow_2)); }
	inline uint32_t get_numPointsToShow_2() const { return ___numPointsToShow_2; }
	inline uint32_t* get_address_of_numPointsToShow_2() { return &___numPointsToShow_2; }
	inline void set_numPointsToShow_2(uint32_t value)
	{
		___numPointsToShow_2 = value;
	}

	inline static int32_t get_offset_of_PointCloudPrefab_3() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t595762290, ___PointCloudPrefab_3)); }
	inline GameObject_t3649338848 * get_PointCloudPrefab_3() const { return ___PointCloudPrefab_3; }
	inline GameObject_t3649338848 ** get_address_of_PointCloudPrefab_3() { return &___PointCloudPrefab_3; }
	inline void set_PointCloudPrefab_3(GameObject_t3649338848 * value)
	{
		___PointCloudPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudPrefab_3), value);
	}

	inline static int32_t get_offset_of_pointCloudObjects_4() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t595762290, ___pointCloudObjects_4)); }
	inline List_1_t2392691131 * get_pointCloudObjects_4() const { return ___pointCloudObjects_4; }
	inline List_1_t2392691131 ** get_address_of_pointCloudObjects_4() { return &___pointCloudObjects_4; }
	inline void set_pointCloudObjects_4(List_1_t2392691131 * value)
	{
		___pointCloudObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudObjects_4), value);
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t595762290, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t4153953548* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t4153953548** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t4153953548* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPOINTCLOUDEXAMPLE_T595762290_H
#ifndef UNITYARVIDEO_T2470488057_H
#define UNITYARVIDEO_T2470488057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideo
struct  UnityARVideo_t2470488057  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Material UnityEngine.XR.iOS.UnityARVideo::m_ClearMaterial
	Material_t4055262778 * ___m_ClearMaterial_2;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.XR.iOS.UnityARVideo::m_VideoCommandBuffer
	CommandBuffer_t1027034615 * ___m_VideoCommandBuffer_3;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureY
	Texture2D_t2870930912 * ____videoTextureY_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureCbCr
	Texture2D_t2870930912 * ____videoTextureCbCr_5;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARVideo::m_Session
	UnityARSessionNativeInterface_t3869411053 * ___m_Session_6;
	// System.Boolean UnityEngine.XR.iOS.UnityARVideo::bCommandBufferInitialized
	bool ___bCommandBufferInitialized_7;

public:
	inline static int32_t get_offset_of_m_ClearMaterial_2() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ___m_ClearMaterial_2)); }
	inline Material_t4055262778 * get_m_ClearMaterial_2() const { return ___m_ClearMaterial_2; }
	inline Material_t4055262778 ** get_address_of_m_ClearMaterial_2() { return &___m_ClearMaterial_2; }
	inline void set_m_ClearMaterial_2(Material_t4055262778 * value)
	{
		___m_ClearMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClearMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_VideoCommandBuffer_3() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ___m_VideoCommandBuffer_3)); }
	inline CommandBuffer_t1027034615 * get_m_VideoCommandBuffer_3() const { return ___m_VideoCommandBuffer_3; }
	inline CommandBuffer_t1027034615 ** get_address_of_m_VideoCommandBuffer_3() { return &___m_VideoCommandBuffer_3; }
	inline void set_m_VideoCommandBuffer_3(CommandBuffer_t1027034615 * value)
	{
		___m_VideoCommandBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VideoCommandBuffer_3), value);
	}

	inline static int32_t get_offset_of__videoTextureY_4() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ____videoTextureY_4)); }
	inline Texture2D_t2870930912 * get__videoTextureY_4() const { return ____videoTextureY_4; }
	inline Texture2D_t2870930912 ** get_address_of__videoTextureY_4() { return &____videoTextureY_4; }
	inline void set__videoTextureY_4(Texture2D_t2870930912 * value)
	{
		____videoTextureY_4 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureY_4), value);
	}

	inline static int32_t get_offset_of__videoTextureCbCr_5() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ____videoTextureCbCr_5)); }
	inline Texture2D_t2870930912 * get__videoTextureCbCr_5() const { return ____videoTextureCbCr_5; }
	inline Texture2D_t2870930912 ** get_address_of__videoTextureCbCr_5() { return &____videoTextureCbCr_5; }
	inline void set__videoTextureCbCr_5(Texture2D_t2870930912 * value)
	{
		____videoTextureCbCr_5 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureCbCr_5), value);
	}

	inline static int32_t get_offset_of_m_Session_6() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ___m_Session_6)); }
	inline UnityARSessionNativeInterface_t3869411053 * get_m_Session_6() const { return ___m_Session_6; }
	inline UnityARSessionNativeInterface_t3869411053 ** get_address_of_m_Session_6() { return &___m_Session_6; }
	inline void set_m_Session_6(UnityARSessionNativeInterface_t3869411053 * value)
	{
		___m_Session_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_6), value);
	}

	inline static int32_t get_offset_of_bCommandBufferInitialized_7() { return static_cast<int32_t>(offsetof(UnityARVideo_t2470488057, ___bCommandBufferInitialized_7)); }
	inline bool get_bCommandBufferInitialized_7() const { return ___bCommandBufferInitialized_7; }
	inline bool* get_address_of_bCommandBufferInitialized_7() { return &___bCommandBufferInitialized_7; }
	inline void set_bCommandBufferInitialized_7(bool value)
	{
		___bCommandBufferInitialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEO_T2470488057_H
#ifndef UNITYARKITCONTROL_T4114739031_H
#define UNITYARKITCONTROL_T4114739031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARKitControl
struct  UnityARKitControl_t4114739031  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.XR.iOS.UnityARSessionRunOption[] UnityEngine.XR.iOS.UnityARKitControl::runOptions
	UnityARSessionRunOptionU5BU5D_t3872385379* ___runOptions_2;
	// UnityEngine.XR.iOS.UnityARAlignment[] UnityEngine.XR.iOS.UnityARKitControl::alignmentOptions
	UnityARAlignmentU5BU5D_t2020264667* ___alignmentOptions_3;
	// UnityEngine.XR.iOS.UnityARPlaneDetection[] UnityEngine.XR.iOS.UnityARKitControl::planeOptions
	UnityARPlaneDetectionU5BU5D_t4098669274* ___planeOptions_4;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentOptionIndex
	int32_t ___currentOptionIndex_5;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentAlignmentIndex
	int32_t ___currentAlignmentIndex_6;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentPlaneIndex
	int32_t ___currentPlaneIndex_7;

public:
	inline static int32_t get_offset_of_runOptions_2() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___runOptions_2)); }
	inline UnityARSessionRunOptionU5BU5D_t3872385379* get_runOptions_2() const { return ___runOptions_2; }
	inline UnityARSessionRunOptionU5BU5D_t3872385379** get_address_of_runOptions_2() { return &___runOptions_2; }
	inline void set_runOptions_2(UnityARSessionRunOptionU5BU5D_t3872385379* value)
	{
		___runOptions_2 = value;
		Il2CppCodeGenWriteBarrier((&___runOptions_2), value);
	}

	inline static int32_t get_offset_of_alignmentOptions_3() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___alignmentOptions_3)); }
	inline UnityARAlignmentU5BU5D_t2020264667* get_alignmentOptions_3() const { return ___alignmentOptions_3; }
	inline UnityARAlignmentU5BU5D_t2020264667** get_address_of_alignmentOptions_3() { return &___alignmentOptions_3; }
	inline void set_alignmentOptions_3(UnityARAlignmentU5BU5D_t2020264667* value)
	{
		___alignmentOptions_3 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentOptions_3), value);
	}

	inline static int32_t get_offset_of_planeOptions_4() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___planeOptions_4)); }
	inline UnityARPlaneDetectionU5BU5D_t4098669274* get_planeOptions_4() const { return ___planeOptions_4; }
	inline UnityARPlaneDetectionU5BU5D_t4098669274** get_address_of_planeOptions_4() { return &___planeOptions_4; }
	inline void set_planeOptions_4(UnityARPlaneDetectionU5BU5D_t4098669274* value)
	{
		___planeOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeOptions_4), value);
	}

	inline static int32_t get_offset_of_currentOptionIndex_5() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___currentOptionIndex_5)); }
	inline int32_t get_currentOptionIndex_5() const { return ___currentOptionIndex_5; }
	inline int32_t* get_address_of_currentOptionIndex_5() { return &___currentOptionIndex_5; }
	inline void set_currentOptionIndex_5(int32_t value)
	{
		___currentOptionIndex_5 = value;
	}

	inline static int32_t get_offset_of_currentAlignmentIndex_6() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___currentAlignmentIndex_6)); }
	inline int32_t get_currentAlignmentIndex_6() const { return ___currentAlignmentIndex_6; }
	inline int32_t* get_address_of_currentAlignmentIndex_6() { return &___currentAlignmentIndex_6; }
	inline void set_currentAlignmentIndex_6(int32_t value)
	{
		___currentAlignmentIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentPlaneIndex_7() { return static_cast<int32_t>(offsetof(UnityARKitControl_t4114739031, ___currentPlaneIndex_7)); }
	inline int32_t get_currentPlaneIndex_7() const { return ___currentPlaneIndex_7; }
	inline int32_t* get_address_of_currentPlaneIndex_7() { return &___currentPlaneIndex_7; }
	inline void set_currentPlaneIndex_7(int32_t value)
	{
		___currentPlaneIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITCONTROL_T4114739031_H
#ifndef PARTICLEPAINTER_T294017160_H
#define PARTICLEPAINTER_T294017160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlePainter
struct  ParticlePainter_t294017160  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.ParticleSystem ParticlePainter::painterParticlePrefab
	ParticleSystem_t1777616458 * ___painterParticlePrefab_2;
	// System.Single ParticlePainter::minDistanceThreshold
	float ___minDistanceThreshold_3;
	// System.Single ParticlePainter::maxDistanceThreshold
	float ___maxDistanceThreshold_4;
	// System.Boolean ParticlePainter::frameUpdated
	bool ___frameUpdated_5;
	// System.Single ParticlePainter::particleSize
	float ___particleSize_6;
	// System.Single ParticlePainter::penDistance
	float ___penDistance_7;
	// ColorPicker ParticlePainter::colorPicker
	ColorPicker_t2582382693 * ___colorPicker_8;
	// UnityEngine.ParticleSystem ParticlePainter::currentPS
	ParticleSystem_t1777616458 * ___currentPS_9;
	// UnityEngine.ParticleSystem/Particle[] ParticlePainter::particles
	ParticleU5BU5D_t419044168* ___particles_10;
	// UnityEngine.Vector3 ParticlePainter::previousPosition
	Vector3_t596762001  ___previousPosition_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ParticlePainter::currentPaintVertices
	List_1_t3635081580 * ___currentPaintVertices_12;
	// UnityEngine.Color ParticlePainter::currentColor
	Color_t320819310  ___currentColor_13;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> ParticlePainter::paintSystems
	List_1_t520968741 * ___paintSystems_14;
	// System.Int32 ParticlePainter::paintMode
	int32_t ___paintMode_15;

public:
	inline static int32_t get_offset_of_painterParticlePrefab_2() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___painterParticlePrefab_2)); }
	inline ParticleSystem_t1777616458 * get_painterParticlePrefab_2() const { return ___painterParticlePrefab_2; }
	inline ParticleSystem_t1777616458 ** get_address_of_painterParticlePrefab_2() { return &___painterParticlePrefab_2; }
	inline void set_painterParticlePrefab_2(ParticleSystem_t1777616458 * value)
	{
		___painterParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___painterParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_minDistanceThreshold_3() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___minDistanceThreshold_3)); }
	inline float get_minDistanceThreshold_3() const { return ___minDistanceThreshold_3; }
	inline float* get_address_of_minDistanceThreshold_3() { return &___minDistanceThreshold_3; }
	inline void set_minDistanceThreshold_3(float value)
	{
		___minDistanceThreshold_3 = value;
	}

	inline static int32_t get_offset_of_maxDistanceThreshold_4() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___maxDistanceThreshold_4)); }
	inline float get_maxDistanceThreshold_4() const { return ___maxDistanceThreshold_4; }
	inline float* get_address_of_maxDistanceThreshold_4() { return &___maxDistanceThreshold_4; }
	inline void set_maxDistanceThreshold_4(float value)
	{
		___maxDistanceThreshold_4 = value;
	}

	inline static int32_t get_offset_of_frameUpdated_5() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___frameUpdated_5)); }
	inline bool get_frameUpdated_5() const { return ___frameUpdated_5; }
	inline bool* get_address_of_frameUpdated_5() { return &___frameUpdated_5; }
	inline void set_frameUpdated_5(bool value)
	{
		___frameUpdated_5 = value;
	}

	inline static int32_t get_offset_of_particleSize_6() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___particleSize_6)); }
	inline float get_particleSize_6() const { return ___particleSize_6; }
	inline float* get_address_of_particleSize_6() { return &___particleSize_6; }
	inline void set_particleSize_6(float value)
	{
		___particleSize_6 = value;
	}

	inline static int32_t get_offset_of_penDistance_7() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___penDistance_7)); }
	inline float get_penDistance_7() const { return ___penDistance_7; }
	inline float* get_address_of_penDistance_7() { return &___penDistance_7; }
	inline void set_penDistance_7(float value)
	{
		___penDistance_7 = value;
	}

	inline static int32_t get_offset_of_colorPicker_8() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___colorPicker_8)); }
	inline ColorPicker_t2582382693 * get_colorPicker_8() const { return ___colorPicker_8; }
	inline ColorPicker_t2582382693 ** get_address_of_colorPicker_8() { return &___colorPicker_8; }
	inline void set_colorPicker_8(ColorPicker_t2582382693 * value)
	{
		___colorPicker_8 = value;
		Il2CppCodeGenWriteBarrier((&___colorPicker_8), value);
	}

	inline static int32_t get_offset_of_currentPS_9() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___currentPS_9)); }
	inline ParticleSystem_t1777616458 * get_currentPS_9() const { return ___currentPS_9; }
	inline ParticleSystem_t1777616458 ** get_address_of_currentPS_9() { return &___currentPS_9; }
	inline void set_currentPS_9(ParticleSystem_t1777616458 * value)
	{
		___currentPS_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_9), value);
	}

	inline static int32_t get_offset_of_particles_10() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___particles_10)); }
	inline ParticleU5BU5D_t419044168* get_particles_10() const { return ___particles_10; }
	inline ParticleU5BU5D_t419044168** get_address_of_particles_10() { return &___particles_10; }
	inline void set_particles_10(ParticleU5BU5D_t419044168* value)
	{
		___particles_10 = value;
		Il2CppCodeGenWriteBarrier((&___particles_10), value);
	}

	inline static int32_t get_offset_of_previousPosition_11() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___previousPosition_11)); }
	inline Vector3_t596762001  get_previousPosition_11() const { return ___previousPosition_11; }
	inline Vector3_t596762001 * get_address_of_previousPosition_11() { return &___previousPosition_11; }
	inline void set_previousPosition_11(Vector3_t596762001  value)
	{
		___previousPosition_11 = value;
	}

	inline static int32_t get_offset_of_currentPaintVertices_12() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___currentPaintVertices_12)); }
	inline List_1_t3635081580 * get_currentPaintVertices_12() const { return ___currentPaintVertices_12; }
	inline List_1_t3635081580 ** get_address_of_currentPaintVertices_12() { return &___currentPaintVertices_12; }
	inline void set_currentPaintVertices_12(List_1_t3635081580 * value)
	{
		___currentPaintVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentPaintVertices_12), value);
	}

	inline static int32_t get_offset_of_currentColor_13() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___currentColor_13)); }
	inline Color_t320819310  get_currentColor_13() const { return ___currentColor_13; }
	inline Color_t320819310 * get_address_of_currentColor_13() { return &___currentColor_13; }
	inline void set_currentColor_13(Color_t320819310  value)
	{
		___currentColor_13 = value;
	}

	inline static int32_t get_offset_of_paintSystems_14() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___paintSystems_14)); }
	inline List_1_t520968741 * get_paintSystems_14() const { return ___paintSystems_14; }
	inline List_1_t520968741 ** get_address_of_paintSystems_14() { return &___paintSystems_14; }
	inline void set_paintSystems_14(List_1_t520968741 * value)
	{
		___paintSystems_14 = value;
		Il2CppCodeGenWriteBarrier((&___paintSystems_14), value);
	}

	inline static int32_t get_offset_of_paintMode_15() { return static_cast<int32_t>(offsetof(ParticlePainter_t294017160, ___paintMode_15)); }
	inline int32_t get_paintMode_15() const { return ___paintMode_15; }
	inline int32_t* get_address_of_paintMode_15() { return &___paintMode_15; }
	inline void set_paintMode_15(int32_t value)
	{
		___paintMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPAINTER_T294017160_H
#ifndef TILTWINDOW_T1431928114_H
#define TILTWINDOW_T1431928114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltWindow
struct  TiltWindow_t1431928114  : public MonoBehaviour_t1096588306
{
public:
	// UnityEngine.Vector2 TiltWindow::range
	Vector2_t59524482  ___range_2;
	// UnityEngine.Transform TiltWindow::mTrans
	Transform_t3933397867 * ___mTrans_3;
	// UnityEngine.Quaternion TiltWindow::mStart
	Quaternion_t3165733013  ___mStart_4;
	// UnityEngine.Vector2 TiltWindow::mRot
	Vector2_t59524482  ___mRot_5;

public:
	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(TiltWindow_t1431928114, ___range_2)); }
	inline Vector2_t59524482  get_range_2() const { return ___range_2; }
	inline Vector2_t59524482 * get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(Vector2_t59524482  value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(TiltWindow_t1431928114, ___mTrans_3)); }
	inline Transform_t3933397867 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3933397867 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3933397867 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mStart_4() { return static_cast<int32_t>(offsetof(TiltWindow_t1431928114, ___mStart_4)); }
	inline Quaternion_t3165733013  get_mStart_4() const { return ___mStart_4; }
	inline Quaternion_t3165733013 * get_address_of_mStart_4() { return &___mStart_4; }
	inline void set_mStart_4(Quaternion_t3165733013  value)
	{
		___mStart_4 = value;
	}

	inline static int32_t get_offset_of_mRot_5() { return static_cast<int32_t>(offsetof(TiltWindow_t1431928114, ___mRot_5)); }
	inline Vector2_t59524482  get_mRot_5() const { return ___mRot_5; }
	inline Vector2_t59524482 * get_address_of_mRot_5() { return &___mRot_5; }
	inline void set_mRot_5(Vector2_t59524482  value)
	{
		___mRot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T1431928114_H
#ifndef SELECTABLE_T3890617260_H
#define SELECTABLE_T3890617260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3890617260  : public UIBehaviour_t2441083007
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t2887492880  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t625039033  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t758977253  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2976076601 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1406460313 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1819317824 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_Navigation_3)); }
	inline Navigation_t2887492880  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t2887492880 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t2887492880  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_Colors_5)); }
	inline ColorBlock_t625039033  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t625039033 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t625039033  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_SpriteState_6)); }
	inline SpriteState_t758977253  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t758977253 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t758977253  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2976076601 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2976076601 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2976076601 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_TargetGraphic_9)); }
	inline Graphic_t1406460313 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1406460313 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1406460313 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3890617260, ___m_CanvasGroupCache_15)); }
	inline List_1_t1819317824 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1819317824 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1819317824 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3890617260_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t2633969543 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3890617260_StaticFields, ___s_List_2)); }
	inline List_1_t2633969543 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t2633969543 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t2633969543 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3890617260_H
#ifndef BOXSLIDER_T2780260910_H
#define BOXSLIDER_T2780260910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider
struct  BoxSlider_t2780260910  : public Selectable_t3890617260
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleRect
	RectTransform_t1885177139 * ___m_HandleRect_16;
	// System.Single UnityEngine.UI.BoxSlider::m_MinValue
	float ___m_MinValue_17;
	// System.Single UnityEngine.UI.BoxSlider::m_MaxValue
	float ___m_MaxValue_18;
	// System.Boolean UnityEngine.UI.BoxSlider::m_WholeNumbers
	bool ___m_WholeNumbers_19;
	// System.Single UnityEngine.UI.BoxSlider::m_Value
	float ___m_Value_20;
	// System.Single UnityEngine.UI.BoxSlider::m_ValueY
	float ___m_ValueY_21;
	// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::m_OnValueChanged
	BoxSliderEvent_t2026169079 * ___m_OnValueChanged_22;
	// UnityEngine.Transform UnityEngine.UI.BoxSlider::m_HandleTransform
	Transform_t3933397867 * ___m_HandleTransform_23;
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleContainerRect
	RectTransform_t1885177139 * ___m_HandleContainerRect_24;
	// UnityEngine.Vector2 UnityEngine.UI.BoxSlider::m_Offset
	Vector2_t59524482  ___m_Offset_25;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.BoxSlider::m_Tracker
	DrivenRectTransformTracker_t1306825633  ___m_Tracker_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_HandleRect_16)); }
	inline RectTransform_t1885177139 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t1885177139 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t1885177139 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_MinValue_17() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_MinValue_17)); }
	inline float get_m_MinValue_17() const { return ___m_MinValue_17; }
	inline float* get_address_of_m_MinValue_17() { return &___m_MinValue_17; }
	inline void set_m_MinValue_17(float value)
	{
		___m_MinValue_17 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_18() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_MaxValue_18)); }
	inline float get_m_MaxValue_18() const { return ___m_MaxValue_18; }
	inline float* get_address_of_m_MaxValue_18() { return &___m_MaxValue_18; }
	inline void set_m_MaxValue_18(float value)
	{
		___m_MaxValue_18 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_19() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_WholeNumbers_19)); }
	inline bool get_m_WholeNumbers_19() const { return ___m_WholeNumbers_19; }
	inline bool* get_address_of_m_WholeNumbers_19() { return &___m_WholeNumbers_19; }
	inline void set_m_WholeNumbers_19(bool value)
	{
		___m_WholeNumbers_19 = value;
	}

	inline static int32_t get_offset_of_m_Value_20() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_Value_20)); }
	inline float get_m_Value_20() const { return ___m_Value_20; }
	inline float* get_address_of_m_Value_20() { return &___m_Value_20; }
	inline void set_m_Value_20(float value)
	{
		___m_Value_20 = value;
	}

	inline static int32_t get_offset_of_m_ValueY_21() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_ValueY_21)); }
	inline float get_m_ValueY_21() const { return ___m_ValueY_21; }
	inline float* get_address_of_m_ValueY_21() { return &___m_ValueY_21; }
	inline void set_m_ValueY_21(float value)
	{
		___m_ValueY_21 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_22() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_OnValueChanged_22)); }
	inline BoxSliderEvent_t2026169079 * get_m_OnValueChanged_22() const { return ___m_OnValueChanged_22; }
	inline BoxSliderEvent_t2026169079 ** get_address_of_m_OnValueChanged_22() { return &___m_OnValueChanged_22; }
	inline void set_m_OnValueChanged_22(BoxSliderEvent_t2026169079 * value)
	{
		___m_OnValueChanged_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_22), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_23() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_HandleTransform_23)); }
	inline Transform_t3933397867 * get_m_HandleTransform_23() const { return ___m_HandleTransform_23; }
	inline Transform_t3933397867 ** get_address_of_m_HandleTransform_23() { return &___m_HandleTransform_23; }
	inline void set_m_HandleTransform_23(Transform_t3933397867 * value)
	{
		___m_HandleTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_23), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_24() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_HandleContainerRect_24)); }
	inline RectTransform_t1885177139 * get_m_HandleContainerRect_24() const { return ___m_HandleContainerRect_24; }
	inline RectTransform_t1885177139 ** get_address_of_m_HandleContainerRect_24() { return &___m_HandleContainerRect_24; }
	inline void set_m_HandleContainerRect_24(RectTransform_t1885177139 * value)
	{
		___m_HandleContainerRect_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_24), value);
	}

	inline static int32_t get_offset_of_m_Offset_25() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_Offset_25)); }
	inline Vector2_t59524482  get_m_Offset_25() const { return ___m_Offset_25; }
	inline Vector2_t59524482 * get_address_of_m_Offset_25() { return &___m_Offset_25; }
	inline void set_m_Offset_25(Vector2_t59524482  value)
	{
		___m_Offset_25 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_26() { return static_cast<int32_t>(offsetof(BoxSlider_t2780260910, ___m_Tracker_26)); }
	inline DrivenRectTransformTracker_t1306825633  get_m_Tracker_26() const { return ___m_Tracker_26; }
	inline DrivenRectTransformTracker_t1306825633 * get_address_of_m_Tracker_26() { return &___m_Tracker_26; }
	inline void set_m_Tracker_26(DrivenRectTransformTracker_t1306825633  value)
	{
		___m_Tracker_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDER_T2780260910_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (ARPlaneAnchor_t1843157284)+ sizeof (RuntimeObject), sizeof(ARPlaneAnchor_t1843157284_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	ARPlaneAnchor_t1843157284::get_offset_of_identifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPlaneAnchor_t1843157284::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPlaneAnchor_t1843157284::get_offset_of_alignment_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPlaneAnchor_t1843157284::get_offset_of_center_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPlaneAnchor_t1843157284::get_offset_of_extent_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (ARPlaneAnchorAlignment_t185787390)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1801[2] = 
{
	ARPlaneAnchorAlignment_t185787390::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (ARPoint_t1649490841)+ sizeof (RuntimeObject), sizeof(ARPoint_t1649490841 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1802[2] = 
{
	ARPoint_t1649490841::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPoint_t1649490841::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (ARRect_t3161395965)+ sizeof (RuntimeObject), sizeof(ARRect_t3161395965 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[2] = 
{
	ARRect_t3161395965::get_offset_of_origin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARRect_t3161395965::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ARSize_t2759123995)+ sizeof (RuntimeObject), sizeof(ARSize_t2759123995 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1804[2] = 
{
	ARSize_t2759123995::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARSize_t2759123995::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (ARTextureHandles_t1375402930)+ sizeof (RuntimeObject), sizeof(ARTextureHandles_t1375402930 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[2] = 
{
	ARTextureHandles_t1375402930::get_offset_of_textureY_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARTextureHandles_t1375402930::get_offset_of_textureCbCr_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (ARTrackingQuality_t1141893651)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1806[5] = 
{
	ARTrackingQuality_t1141893651::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (ARTrackingState_t3679923289)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1807[4] = 
{
	ARTrackingState_t3679923289::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (ARTrackingStateReason_t2365446872)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1808[4] = 
{
	ARTrackingStateReason_t2365446872::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (UnityARMatrix4x4_t1132406930)+ sizeof (RuntimeObject), sizeof(UnityARMatrix4x4_t1132406930 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[4] = 
{
	UnityARMatrix4x4_t1132406930::get_offset_of_column0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARMatrix4x4_t1132406930::get_offset_of_column1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARMatrix4x4_t1132406930::get_offset_of_column2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARMatrix4x4_t1132406930::get_offset_of_column3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (internal_UnityARCamera_t1506237410)+ sizeof (RuntimeObject), sizeof(internal_UnityARCamera_t1506237410 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[5] = 
{
	internal_UnityARCamera_t1506237410::get_offset_of_worldTransform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t1506237410::get_offset_of_projectionMatrix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t1506237410::get_offset_of_trackingState_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t1506237410::get_offset_of_trackingReason_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t1506237410::get_offset_of_getPointCloudData_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (UnityARCamera_t2376600594)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[5] = 
{
	UnityARCamera_t2376600594::get_offset_of_worldTransform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t2376600594::get_offset_of_projectionMatrix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t2376600594::get_offset_of_trackingState_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t2376600594::get_offset_of_trackingReason_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t2376600594::get_offset_of_pointCloudData_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (UnityARAnchorData_t1511263172)+ sizeof (RuntimeObject), sizeof(UnityARAnchorData_t1511263172 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1812[5] = 
{
	UnityARAnchorData_t1511263172::get_offset_of_ptrIdentifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARAnchorData_t1511263172::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARAnchorData_t1511263172::get_offset_of_alignment_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARAnchorData_t1511263172::get_offset_of_center_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARAnchorData_t1511263172::get_offset_of_extent_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (UnityARHitTestResult_t1002809393)+ sizeof (RuntimeObject), sizeof(UnityARHitTestResult_t1002809393_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1813[6] = 
{
	UnityARHitTestResult_t1002809393::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t1002809393::get_offset_of_distance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t1002809393::get_offset_of_localTransform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t1002809393::get_offset_of_worldTransform_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t1002809393::get_offset_of_anchor_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t1002809393::get_offset_of_isValid_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (UnityARAlignment_t1226993326)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1814[4] = 
{
	UnityARAlignment_t1226993326::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (UnityARPlaneDetection_t2335225179)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[3] = 
{
	UnityARPlaneDetection_t2335225179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (ARKitSessionConfiguration_t3142208763)+ sizeof (RuntimeObject), sizeof(ARKitSessionConfiguration_t3142208763_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[3] = 
{
	ARKitSessionConfiguration_t3142208763::get_offset_of_alignment_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitSessionConfiguration_t3142208763::get_offset_of_getPointCloudData_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitSessionConfiguration_t3142208763::get_offset_of_enableLightEstimation_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (ARKitWorldTackingSessionConfiguration_t2186350340)+ sizeof (RuntimeObject), sizeof(ARKitWorldTackingSessionConfiguration_t2186350340_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1817[4] = 
{
	ARKitWorldTackingSessionConfiguration_t2186350340::get_offset_of_alignment_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitWorldTackingSessionConfiguration_t2186350340::get_offset_of_planeDetection_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitWorldTackingSessionConfiguration_t2186350340::get_offset_of_getPointCloudData_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitWorldTackingSessionConfiguration_t2186350340::get_offset_of_enableLightEstimation_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (UnityARSessionRunOption_t2337414342)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1818[3] = 
{
	UnityARSessionRunOption_t2337414342::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (UnityARSessionNativeInterface_t3869411053), -1, sizeof(UnityARSessionNativeInterface_t3869411053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1819[13] = 
{
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_ARFrameUpdatedEvent_0(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_ARAnchorAddedEvent_1(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_ARAnchorUpdatedEvent_2(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_ARAnchorRemovedEvent_3(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_ARSessionFailedEvent_4(),
	UnityARSessionNativeInterface_t3869411053::get_offset_of_m_NativeARSession_5(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_s_Camera_6(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_s_UnityARSessionNativeInterface_7(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_8(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_9(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_10(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_11(),
	UnityARSessionNativeInterface_t3869411053_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (ARFrameUpdate_t3333962149), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (ARAnchorAdded_t1580091102), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (ARAnchorUpdated_t1492128022), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (ARAnchorRemoved_t3663543047), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (ARSessionFailed_t3599980200), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (internal_ARFrameUpdate_t2726389622), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (internal_ARAnchorAdded_t3791587037), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (internal_ARAnchorUpdated_t2928809250), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (internal_ARAnchorRemoved_t409959511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (PointCloudParticleExample_t3205217707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[7] = 
{
	PointCloudParticleExample_t3205217707::get_offset_of_pointCloudParticlePrefab_2(),
	PointCloudParticleExample_t3205217707::get_offset_of_maxPointsToShow_3(),
	PointCloudParticleExample_t3205217707::get_offset_of_particleSize_4(),
	PointCloudParticleExample_t3205217707::get_offset_of_m_PointCloudData_5(),
	PointCloudParticleExample_t3205217707::get_offset_of_frameUpdated_6(),
	PointCloudParticleExample_t3205217707::get_offset_of_currentPS_7(),
	PointCloudParticleExample_t3205217707::get_offset_of_particles_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (UnityARCameraManager_t1803962782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[3] = 
{
	UnityARCameraManager_t1803962782::get_offset_of_m_camera_2(),
	UnityARCameraManager_t1803962782::get_offset_of_m_session_3(),
	UnityARCameraManager_t1803962782::get_offset_of_savedClearMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (UnityARCameraNearFar_t4152080291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[3] = 
{
	UnityARCameraNearFar_t4152080291::get_offset_of_attachedCamera_2(),
	UnityARCameraNearFar_t4152080291::get_offset_of_currentNearZ_3(),
	UnityARCameraNearFar_t4152080291::get_offset_of_currentFarZ_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (UnityARGeneratePlane_t3109641619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[2] = 
{
	UnityARGeneratePlane_t3109641619::get_offset_of_planePrefab_2(),
	UnityARGeneratePlane_t3109641619::get_offset_of_unityARAnchorManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (UnityARHitTestExample_t2411672648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[1] = 
{
	UnityARHitTestExample_t2411672648::get_offset_of_m_HitTransform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (UnityARKitControl_t4114739031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[6] = 
{
	UnityARKitControl_t4114739031::get_offset_of_runOptions_2(),
	UnityARKitControl_t4114739031::get_offset_of_alignmentOptions_3(),
	UnityARKitControl_t4114739031::get_offset_of_planeOptions_4(),
	UnityARKitControl_t4114739031::get_offset_of_currentOptionIndex_5(),
	UnityARKitControl_t4114739031::get_offset_of_currentAlignmentIndex_6(),
	UnityARKitControl_t4114739031::get_offset_of_currentPlaneIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (UnityARVideo_t2470488057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[6] = 
{
	UnityARVideo_t2470488057::get_offset_of_m_ClearMaterial_2(),
	UnityARVideo_t2470488057::get_offset_of_m_VideoCommandBuffer_3(),
	UnityARVideo_t2470488057::get_offset_of__videoTextureY_4(),
	UnityARVideo_t2470488057::get_offset_of__videoTextureCbCr_5(),
	UnityARVideo_t2470488057::get_offset_of_m_Session_6(),
	UnityARVideo_t2470488057::get_offset_of_bCommandBufferInitialized_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (UnityPointCloudExample_t595762290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1836[4] = 
{
	UnityPointCloudExample_t595762290::get_offset_of_numPointsToShow_2(),
	UnityPointCloudExample_t595762290::get_offset_of_PointCloudPrefab_3(),
	UnityPointCloudExample_t595762290::get_offset_of_pointCloudObjects_4(),
	UnityPointCloudExample_t595762290::get_offset_of_m_PointCloudData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (UnityARAnchorManager_t3624459219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[1] = 
{
	UnityARAnchorManager_t3624459219::get_offset_of_planeAnchorMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (UnityARMatrixOps_t180517933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (UnityARUtility_t2651983316), -1, sizeof(UnityARUtility_t2651983316_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1839[3] = 
{
	UnityARUtility_t2651983316::get_offset_of_meshCollider_0(),
	UnityARUtility_t2651983316::get_offset_of_meshFilter_1(),
	UnityARUtility_t2651983316_StaticFields::get_offset_of_planePrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (U3CPrivateImplementationDetailsU3E_t1316871563), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1316871563_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1840[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1316871563_StaticFields::get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (U24ArrayTypeU3D24_t4048413920)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t4048413920 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (U3CModuleU3E_t3896811617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (ScreenShot_t707795496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[1] = 
{
	ScreenShot_t707795496::get_offset_of_format_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (BallMaker_t1786680372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[3] = 
{
	BallMaker_t1786680372::get_offset_of_ballPrefab_2(),
	BallMaker_t1786680372::get_offset_of_createHeight_3(),
	BallMaker_t1786680372::get_offset_of_props_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (BallMover_t4283490589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[2] = 
{
	BallMover_t4283490589::get_offset_of_collBallPrefab_2(),
	BallMover_t4283490589::get_offset_of_collBallGO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (Ballz_t2183783589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[2] = 
{
	Ballz_t2183783589::get_offset_of_yDistanceThreshold_2(),
	Ballz_t2183783589::get_offset_of_startingY_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (ModeSwitcher_t833699841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[3] = 
{
	ModeSwitcher_t833699841::get_offset_of_ballMake_2(),
	ModeSwitcher_t833699841::get_offset_of_ballMove_3(),
	ModeSwitcher_t833699841::get_offset_of_appMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (ColorValues_t1727074643)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1848[8] = 
{
	ColorValues_t1727074643::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (ColorChangedEvent_t347040188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (HSVChangedEvent_t3546572410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (ColorPickerTester_t3436045112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[2] = 
{
	ColorPickerTester_t3436045112::get_offset_of_renderer_2(),
	ColorPickerTester_t3436045112::get_offset_of_picker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (TiltWindow_t1431928114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[4] = 
{
	TiltWindow_t1431928114::get_offset_of_range_2(),
	TiltWindow_t1431928114::get_offset_of_mTrans_3(),
	TiltWindow_t1431928114::get_offset_of_mStart_4(),
	TiltWindow_t1431928114::get_offset_of_mRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (ColorImage_t2737809041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[2] = 
{
	ColorImage_t2737809041::get_offset_of_picker_2(),
	ColorImage_t2737809041::get_offset_of_image_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (ColorLabel_t538373442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[7] = 
{
	ColorLabel_t538373442::get_offset_of_picker_2(),
	ColorLabel_t538373442::get_offset_of_type_3(),
	ColorLabel_t538373442::get_offset_of_prefix_4(),
	ColorLabel_t538373442::get_offset_of_minValue_5(),
	ColorLabel_t538373442::get_offset_of_maxValue_6(),
	ColorLabel_t538373442::get_offset_of_precision_7(),
	ColorLabel_t538373442::get_offset_of_label_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (ColorPicker_t2582382693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[9] = 
{
	ColorPicker_t2582382693::get_offset_of__hue_2(),
	ColorPicker_t2582382693::get_offset_of__saturation_3(),
	ColorPicker_t2582382693::get_offset_of__brightness_4(),
	ColorPicker_t2582382693::get_offset_of__red_5(),
	ColorPicker_t2582382693::get_offset_of__green_6(),
	ColorPicker_t2582382693::get_offset_of__blue_7(),
	ColorPicker_t2582382693::get_offset_of__alpha_8(),
	ColorPicker_t2582382693::get_offset_of_onValueChanged_9(),
	ColorPicker_t2582382693::get_offset_of_onHSVChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (ColorPresets_t321881905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[3] = 
{
	ColorPresets_t321881905::get_offset_of_picker_2(),
	ColorPresets_t321881905::get_offset_of_presets_3(),
	ColorPresets_t321881905::get_offset_of_createPresetImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (ColorSlider_t4061812907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[4] = 
{
	ColorSlider_t4061812907::get_offset_of_hsvpicker_2(),
	ColorSlider_t4061812907::get_offset_of_type_3(),
	ColorSlider_t4061812907::get_offset_of_slider_4(),
	ColorSlider_t4061812907::get_offset_of_listen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (ColorSliderImage_t664667956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[4] = 
{
	ColorSliderImage_t664667956::get_offset_of_picker_2(),
	ColorSliderImage_t664667956::get_offset_of_type_3(),
	ColorSliderImage_t664667956::get_offset_of_direction_4(),
	ColorSliderImage_t664667956::get_offset_of_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (HexColorField_t1392034802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[4] = 
{
	HexColorField_t1392034802::get_offset_of_hsvpicker_2(),
	HexColorField_t1392034802::get_offset_of_displayAlpha_3(),
	HexColorField_t1392034802::get_offset_of_hexInputField_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (SVBoxSlider_t2769455468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[5] = 
{
	SVBoxSlider_t2769455468::get_offset_of_picker_2(),
	SVBoxSlider_t2769455468::get_offset_of_slider_3(),
	SVBoxSlider_t2769455468::get_offset_of_image_4(),
	SVBoxSlider_t2769455468::get_offset_of_lastH_5(),
	SVBoxSlider_t2769455468::get_offset_of_listen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (BoxSlider_t2780260910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[11] = 
{
	BoxSlider_t2780260910::get_offset_of_m_HandleRect_16(),
	BoxSlider_t2780260910::get_offset_of_m_MinValue_17(),
	BoxSlider_t2780260910::get_offset_of_m_MaxValue_18(),
	BoxSlider_t2780260910::get_offset_of_m_WholeNumbers_19(),
	BoxSlider_t2780260910::get_offset_of_m_Value_20(),
	BoxSlider_t2780260910::get_offset_of_m_ValueY_21(),
	BoxSlider_t2780260910::get_offset_of_m_OnValueChanged_22(),
	BoxSlider_t2780260910::get_offset_of_m_HandleTransform_23(),
	BoxSlider_t2780260910::get_offset_of_m_HandleContainerRect_24(),
	BoxSlider_t2780260910::get_offset_of_m_Offset_25(),
	BoxSlider_t2780260910::get_offset_of_m_Tracker_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (Direction_t1434896290)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1862[5] = 
{
	Direction_t1434896290::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (BoxSliderEvent_t2026169079), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (Axis_t2687941421)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1864[3] = 
{
	Axis_t2687941421::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (HSVUtil_t1113819789), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (HsvColor_t3806242730)+ sizeof (RuntimeObject), sizeof(HsvColor_t3806242730 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1866[3] = 
{
	HsvColor_t3806242730::get_offset_of_H_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t3806242730::get_offset_of_S_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t3806242730::get_offset_of_V_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (KnightControl_t1902388948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[5] = 
{
	KnightControl_t1902388948::get_offset_of_animation_2(),
	KnightControl_t1902388948::get_offset_of_detectedMonster_3(),
	KnightControl_t1902388948::get_offset_of_moveForward_4(),
	KnightControl_t1902388948::get_offset_of_moveReverse_5(),
	KnightControl_t1902388948::get_offset_of_KnightDeath_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (ParticlePainter_t294017160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[14] = 
{
	ParticlePainter_t294017160::get_offset_of_painterParticlePrefab_2(),
	ParticlePainter_t294017160::get_offset_of_minDistanceThreshold_3(),
	ParticlePainter_t294017160::get_offset_of_maxDistanceThreshold_4(),
	ParticlePainter_t294017160::get_offset_of_frameUpdated_5(),
	ParticlePainter_t294017160::get_offset_of_particleSize_6(),
	ParticlePainter_t294017160::get_offset_of_penDistance_7(),
	ParticlePainter_t294017160::get_offset_of_colorPicker_8(),
	ParticlePainter_t294017160::get_offset_of_currentPS_9(),
	ParticlePainter_t294017160::get_offset_of_particles_10(),
	ParticlePainter_t294017160::get_offset_of_previousPosition_11(),
	ParticlePainter_t294017160::get_offset_of_currentPaintVertices_12(),
	ParticlePainter_t294017160::get_offset_of_currentColor_13(),
	ParticlePainter_t294017160::get_offset_of_paintSystems_14(),
	ParticlePainter_t294017160::get_offset_of_paintMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (SlimeControl_t1232093680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[3] = 
{
	SlimeControl_t1232093680::get_offset_of_animation_2(),
	SlimeControl_t1232093680::get_offset_of_shouldMove_3(),
	SlimeControl_t1232093680::get_offset_of_SlimeDeath_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (UnityARAmbient_t3389999931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[2] = 
{
	UnityARAmbient_t3389999931::get_offset_of_l_2(),
	UnityARAmbient_t3389999931::get_offset_of_m_Session_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (U3CModuleU3E_t3896811618), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (CharaView_t926653685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[4] = 
{
	CharaView_t926653685::get_offset_of_roteSpeed_2(),
	CharaView_t926653685::get_offset_of_animationSpeed_3(),
	CharaView_t926653685::get_offset_of_animationCount_4(),
	CharaView_t926653685::get_offset_of_animationList_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
