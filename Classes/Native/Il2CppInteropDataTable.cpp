﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t3832562246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t3832562246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t3832562246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t3832562246_0_0_0;
extern "C" void Escape_t461740526_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t461740526_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t461740526_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t461740526_0_0_0;
extern "C" void PreviousInfo_t3459356125_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t3459356125_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t3459356125_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t3459356125_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t2097540903();
extern const RuntimeType AppDomainInitializer_t2097540903_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t11045870();
extern const RuntimeType Swapper_t11045870_0_0_0;
extern "C" void DictionaryEntry_t3951334252_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3951334252_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3951334252_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t3951334252_0_0_0;
extern "C" void Slot_t4047215697_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t4047215697_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t4047215697_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t4047215697_0_0_0;
extern "C" void Slot_t3212906767_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3212906767_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3212906767_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t3212906767_0_0_0;
extern "C" void Enum_t473240710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t473240710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t473240710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t473240710_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t2801676951();
extern const RuntimeType ReadDelegate_t2801676951_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1081122740();
extern const RuntimeType WriteDelegate_t1081122740_0_0_0;
extern "C" void MonoIOStat_t1651089741_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t1651089741_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t1651089741_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t1651089741_0_0_0;
extern "C" void MonoEnumInfo_t2947462192_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t2947462192_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t2947462192_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t2947462192_0_0_0;
extern "C" void CustomAttributeNamedArgument_t1224813713_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t1224813713_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t1224813713_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t1224813713_0_0_0;
extern "C" void CustomAttributeTypedArgument_t3013856313_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t3013856313_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t3013856313_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t3013856313_0_0_0;
extern "C" void ILTokenInfo_t4134893432_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t4134893432_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t4134893432_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t4134893432_0_0_0;
extern "C" void MonoEventInfo_t552897433_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t552897433_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t552897433_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t552897433_0_0_0;
extern "C" void MonoMethodInfo_t3574083146_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t3574083146_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t3574083146_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t3574083146_0_0_0;
extern "C" void MonoPropertyInfo_t236022391_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t236022391_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t236022391_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t236022391_0_0_0;
extern "C" void ParameterModifier_t3485655876_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t3485655876_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t3485655876_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t3485655876_0_0_0;
extern "C" void ResourceCacheItem_t567161473_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t567161473_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t567161473_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t567161473_0_0_0;
extern "C" void ResourceInfo_t3677787790_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t3677787790_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t3677787790_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t3677787790_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t1580892121();
extern const RuntimeType CrossContextDelegate_t1580892121_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t2854585113();
extern const RuntimeType CallbackHandler_t2854585113_0_0_0;
extern "C" void SerializationEntry_t4132831264_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t4132831264_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t4132831264_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t4132831264_0_0_0;
extern "C" void StreamingContext_t134436063_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t134436063_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t134436063_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t134436063_0_0_0;
extern "C" void DSAParameters_t585022427_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t585022427_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t585022427_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t585022427_0_0_0;
extern "C" void RSAParameters_t1689816471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1689816471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1689816471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t1689816471_0_0_0;
extern "C" void SecurityFrame_t1764124542_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1764124542_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1764124542_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t1764124542_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t2241185063();
extern const RuntimeType ThreadStart_t2241185063_0_0_0;
extern "C" void ValueType_t3433162460_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3433162460_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3433162460_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t3433162460_0_0_0;
extern "C" void X509ChainStatus_t3209745328_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t3209745328_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t3209745328_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t3209745328_0_0_0;
extern "C" void IntStack_t405153906_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t405153906_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t405153906_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t405153906_0_0_0;
extern "C" void Interval_t3283126145_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t3283126145_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t3283126145_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t3283126145_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t2569868644();
extern const RuntimeType CostDelegate_t2569868644_0_0_0;
extern "C" void UriScheme_t1685060244_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t1685060244_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t1685060244_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t1685060244_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t2680046182();
extern const RuntimeType Action_t2680046182_0_0_0;
extern "C" void DelegatePInvokeWrapper_DispatcherFactory_t2726905719();
extern const RuntimeType DispatcherFactory_t2726905719_0_0_0;
extern "C" void AnimationCurve_t4189941858_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t4189941858_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t4189941858_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t4189941858_0_0_0;
extern "C" void AnimationEvent_t2070876715_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t2070876715_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t2070876715_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t2070876715_0_0_0;
extern "C" void AnimatorTransitionInfo_t3385393505_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t3385393505_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t3385393505_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t3385393505_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t1726514994();
extern const RuntimeType LogCallback_t1726514994_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t1512677506();
extern const RuntimeType LowMemoryCallback_t1512677506_0_0_0;
extern "C" void AssetBundleRequest_t3541706811_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t3541706811_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t3541706811_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t3541706811_0_0_0;
extern "C" void AsyncOperation_t2290923989_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t2290923989_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t2290923989_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t2290923989_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3585007765();
extern const RuntimeType PCMReaderCallback_t3585007765_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t3376339675();
extern const RuntimeType PCMSetPositionCallback_t3376339675_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2491934040();
extern const RuntimeType AudioConfigurationChangeHandler_t2491934040_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t4000486942();
extern const RuntimeType WillRenderCanvases_t4000486942_0_0_0;
extern "C" void Collision_t2454413492_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t2454413492_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t2454413492_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t2454413492_0_0_0;
extern "C" void ControllerColliderHit_t842731780_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t842731780_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t842731780_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t842731780_0_0_0;
extern "C" void Coroutine_t3555871518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3555871518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3555871518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t3555871518_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t1539218346();
extern const RuntimeType CSSMeasureFunc_t1539218346_0_0_0;
extern "C" void CullingGroup_t44589201_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t44589201_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t44589201_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t44589201_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t1111834607();
extern const RuntimeType StateChanged_t1111834607_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t1864361006();
extern const RuntimeType DisplaysUpdatedDelegate_t1864361006_0_0_0;
extern "C" void Event_t3603750770_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t3603750770_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t3603750770_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t3603750770_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t2037703605();
extern const RuntimeType UnityAction_t2037703605_0_0_0;
extern "C" void FailedToLoadScriptObject_t813148810_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t813148810_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t813148810_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t813148810_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1352591366();
extern const RuntimeType FontTextureRebuildCallback_t1352591366_0_0_0;
extern "C" void Gradient_t3716333628_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3716333628_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3716333628_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t3716333628_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t2031498674();
extern const RuntimeType WindowFunction_t2031498674_0_0_0;
extern "C" void GUIContent_t468091463_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t468091463_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t468091463_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t468091463_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t2520811586();
extern const RuntimeType SkinChangedDelegate_t2520811586_0_0_0;
extern "C" void GUIStyle_t2932200036_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t2932200036_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t2932200036_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t2932200036_0_0_0;
extern "C" void GUIStyleState_t2470603889_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t2470603889_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t2470603889_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t2470603889_0_0_0;
extern "C" void HumanBone_t1430191328_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t1430191328_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t1430191328_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t1430191328_0_0_0;
extern "C" void Object_t1008057425_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t1008057425_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t1008057425_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t1008057425_0_0_0;
extern "C" void PlayableBinding_t1499311357_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t1499311357_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t1499311357_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t1499311357_0_0_0;
extern "C" void RaycastHit_t1273336641_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t1273336641_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t1273336641_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t1273336641_0_0_0;
extern "C" void RaycastHit2D_t2323693239_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t2323693239_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t2323693239_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t2323693239_0_0_0;
extern "C" void RectOffset_t2771649182_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t2771649182_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t2771649182_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t2771649182_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1627915661();
extern const RuntimeType UpdatedEventHandler_t1627915661_0_0_0;
extern "C" void ResourceRequest_t1970232538_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t1970232538_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t1970232538_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t1970232538_0_0_0;
extern "C" void ScriptableObject_t332921144_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t332921144_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t332921144_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t332921144_0_0_0;
extern "C" void HitInfo_t2832651489_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t2832651489_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t2832651489_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t2832651489_0_0_0;
extern "C" void SkeletonBone_t2095843144_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t2095843144_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t2095843144_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t2095843144_0_0_0;
extern "C" void GcAchievementData_t1098514478_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t1098514478_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t1098514478_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t1098514478_0_0_0;
extern "C" void GcAchievementDescriptionData_t83581021_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t83581021_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t83581021_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t83581021_0_0_0;
extern "C" void GcLeaderboard_t374162520_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t374162520_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t374162520_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t374162520_0_0_0;
extern "C" void GcScoreData_t322958630_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t322958630_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t322958630_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t322958630_0_0_0;
extern "C" void GcUserProfileData_t1570437431_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t1570437431_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t1570437431_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t1570437431_0_0_0;
extern "C" void TextGenerationSettings_t2160595927_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t2160595927_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t2160595927_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t2160595927_0_0_0;
extern "C" void TextGenerator_t3308673789_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t3308673789_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t3308673789_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t3308673789_0_0_0;
extern "C" void TrackedReference_t3940723406_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t3940723406_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t3940723406_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t3940723406_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3398639038();
extern const RuntimeType RequestAtlasCallback_t3398639038_0_0_0;
extern "C" void WorkRequest_t886435312_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t886435312_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t886435312_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t886435312_0_0_0;
extern "C" void WaitForSeconds_t3647418858_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t3647418858_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t3647418858_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t3647418858_0_0_0;
extern "C" void YieldInstruction_t3975915868_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t3975915868_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t3975915868_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t3975915868_0_0_0;
extern "C" void RaycastResult_t1715322097_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t1715322097_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t1715322097_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t1715322097_0_0_0;
extern "C" void ColorTween_t1347433457_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t1347433457_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t1347433457_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t1347433457_0_0_0;
extern "C" void FloatTween_t1459678735_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1459678735_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1459678735_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t1459678735_0_0_0;
extern "C" void Resources_t3522918643_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t3522918643_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t3522918643_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t3522918643_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t4262405814();
extern const RuntimeType OnValidateInput_t4262405814_0_0_0;
extern "C" void Navigation_t2887492880_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t2887492880_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t2887492880_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t2887492880_0_0_0;
extern "C" void SpriteState_t758977253_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t758977253_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t758977253_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t758977253_0_0_0;
extern "C" void ARAnchor_t4044008708_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARAnchor_t4044008708_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARAnchor_t4044008708_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARAnchor_t4044008708_0_0_0;
extern "C" void ARHitTestResult_t1803723679_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARHitTestResult_t1803723679_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARHitTestResult_t1803723679_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARHitTestResult_t1803723679_0_0_0;
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitSessionConfiguration_t3142208763_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitSessionConfiguration_t3142208763_0_0_0;
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitWorldTackingSessionConfiguration_t2186350340_0_0_0;
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARPlaneAnchor_t1843157284_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARPlaneAnchor_t1843157284_0_0_0;
extern "C" void UnityARCamera_t2376600594_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARCamera_t2376600594_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARCamera_t2376600594_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARCamera_t2376600594_0_0_0;
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARHitTestResult_t1002809393_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARHitTestResult_t1002809393_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorAdded_t1580091102();
extern const RuntimeType ARAnchorAdded_t1580091102_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorRemoved_t3663543047();
extern const RuntimeType ARAnchorRemoved_t3663543047_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorUpdated_t1492128022();
extern const RuntimeType ARAnchorUpdated_t1492128022_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARSessionFailed_t3599980200();
extern const RuntimeType ARSessionFailed_t3599980200_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorAdded_t3791587037();
extern const RuntimeType internal_ARAnchorAdded_t3791587037_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorRemoved_t409959511();
extern const RuntimeType internal_ARAnchorRemoved_t409959511_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorUpdated_t2928809250();
extern const RuntimeType internal_ARAnchorUpdated_t2928809250_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFrameUpdate_t2726389622();
extern const RuntimeType internal_ARFrameUpdate_t2726389622_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[112] = 
{
	{ NULL, Context_t3832562246_marshal_pinvoke, Context_t3832562246_marshal_pinvoke_back, Context_t3832562246_marshal_pinvoke_cleanup, NULL, NULL, &Context_t3832562246_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t461740526_marshal_pinvoke, Escape_t461740526_marshal_pinvoke_back, Escape_t461740526_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t461740526_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t3459356125_marshal_pinvoke, PreviousInfo_t3459356125_marshal_pinvoke_back, PreviousInfo_t3459356125_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t3459356125_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t2097540903, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t2097540903_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t11045870, NULL, NULL, NULL, NULL, NULL, &Swapper_t11045870_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3951334252_marshal_pinvoke, DictionaryEntry_t3951334252_marshal_pinvoke_back, DictionaryEntry_t3951334252_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3951334252_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t4047215697_marshal_pinvoke, Slot_t4047215697_marshal_pinvoke_back, Slot_t4047215697_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t4047215697_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t3212906767_marshal_pinvoke, Slot_t3212906767_marshal_pinvoke_back, Slot_t3212906767_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3212906767_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t473240710_marshal_pinvoke, Enum_t473240710_marshal_pinvoke_back, Enum_t473240710_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t473240710_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t2801676951, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t2801676951_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t1081122740, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t1081122740_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t1651089741_marshal_pinvoke, MonoIOStat_t1651089741_marshal_pinvoke_back, MonoIOStat_t1651089741_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t1651089741_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t2947462192_marshal_pinvoke, MonoEnumInfo_t2947462192_marshal_pinvoke_back, MonoEnumInfo_t2947462192_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t2947462192_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t1224813713_marshal_pinvoke, CustomAttributeNamedArgument_t1224813713_marshal_pinvoke_back, CustomAttributeNamedArgument_t1224813713_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t1224813713_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t3013856313_marshal_pinvoke, CustomAttributeTypedArgument_t3013856313_marshal_pinvoke_back, CustomAttributeTypedArgument_t3013856313_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t3013856313_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t4134893432_marshal_pinvoke, ILTokenInfo_t4134893432_marshal_pinvoke_back, ILTokenInfo_t4134893432_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t4134893432_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoEventInfo_t552897433_marshal_pinvoke, MonoEventInfo_t552897433_marshal_pinvoke_back, MonoEventInfo_t552897433_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t552897433_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t3574083146_marshal_pinvoke, MonoMethodInfo_t3574083146_marshal_pinvoke_back, MonoMethodInfo_t3574083146_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t3574083146_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t236022391_marshal_pinvoke, MonoPropertyInfo_t236022391_marshal_pinvoke_back, MonoPropertyInfo_t236022391_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t236022391_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t3485655876_marshal_pinvoke, ParameterModifier_t3485655876_marshal_pinvoke_back, ParameterModifier_t3485655876_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t3485655876_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t567161473_marshal_pinvoke, ResourceCacheItem_t567161473_marshal_pinvoke_back, ResourceCacheItem_t567161473_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t567161473_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t3677787790_marshal_pinvoke, ResourceInfo_t3677787790_marshal_pinvoke_back, ResourceInfo_t3677787790_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t3677787790_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t1580892121, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t1580892121_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t2854585113, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t2854585113_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t4132831264_marshal_pinvoke, SerializationEntry_t4132831264_marshal_pinvoke_back, SerializationEntry_t4132831264_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t4132831264_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t134436063_marshal_pinvoke, StreamingContext_t134436063_marshal_pinvoke_back, StreamingContext_t134436063_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t134436063_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t585022427_marshal_pinvoke, DSAParameters_t585022427_marshal_pinvoke_back, DSAParameters_t585022427_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t585022427_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1689816471_marshal_pinvoke, RSAParameters_t1689816471_marshal_pinvoke_back, RSAParameters_t1689816471_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1689816471_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t1764124542_marshal_pinvoke, SecurityFrame_t1764124542_marshal_pinvoke_back, SecurityFrame_t1764124542_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1764124542_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t2241185063, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t2241185063_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3433162460_marshal_pinvoke, ValueType_t3433162460_marshal_pinvoke_back, ValueType_t3433162460_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3433162460_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t3209745328_marshal_pinvoke, X509ChainStatus_t3209745328_marshal_pinvoke_back, X509ChainStatus_t3209745328_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t3209745328_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t405153906_marshal_pinvoke, IntStack_t405153906_marshal_pinvoke_back, IntStack_t405153906_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t405153906_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t3283126145_marshal_pinvoke, Interval_t3283126145_marshal_pinvoke_back, Interval_t3283126145_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t3283126145_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t2569868644, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t2569868644_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t1685060244_marshal_pinvoke, UriScheme_t1685060244_marshal_pinvoke_back, UriScheme_t1685060244_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t1685060244_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t2680046182, NULL, NULL, NULL, NULL, NULL, &Action_t2680046182_0_0_0 } /* System.Action */,
	{ DelegatePInvokeWrapper_DispatcherFactory_t2726905719, NULL, NULL, NULL, NULL, NULL, &DispatcherFactory_t2726905719_0_0_0 } /* Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory */,
	{ NULL, AnimationCurve_t4189941858_marshal_pinvoke, AnimationCurve_t4189941858_marshal_pinvoke_back, AnimationCurve_t4189941858_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t4189941858_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t2070876715_marshal_pinvoke, AnimationEvent_t2070876715_marshal_pinvoke_back, AnimationEvent_t2070876715_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t2070876715_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t3385393505_marshal_pinvoke, AnimatorTransitionInfo_t3385393505_marshal_pinvoke_back, AnimatorTransitionInfo_t3385393505_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t3385393505_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t1726514994, NULL, NULL, NULL, NULL, NULL, &LogCallback_t1726514994_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t1512677506, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t1512677506_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t3541706811_marshal_pinvoke, AssetBundleRequest_t3541706811_marshal_pinvoke_back, AssetBundleRequest_t3541706811_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t3541706811_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t2290923989_marshal_pinvoke, AsyncOperation_t2290923989_marshal_pinvoke_back, AsyncOperation_t2290923989_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t2290923989_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t3585007765, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t3585007765_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t3376339675, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t3376339675_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2491934040, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2491934040_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t4000486942, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t4000486942_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, Collision_t2454413492_marshal_pinvoke, Collision_t2454413492_marshal_pinvoke_back, Collision_t2454413492_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t2454413492_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t842731780_marshal_pinvoke, ControllerColliderHit_t842731780_marshal_pinvoke_back, ControllerColliderHit_t842731780_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t842731780_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, Coroutine_t3555871518_marshal_pinvoke, Coroutine_t3555871518_marshal_pinvoke_back, Coroutine_t3555871518_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3555871518_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t1539218346, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t1539218346_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t44589201_marshal_pinvoke, CullingGroup_t44589201_marshal_pinvoke_back, CullingGroup_t44589201_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t44589201_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t1111834607, NULL, NULL, NULL, NULL, NULL, &StateChanged_t1111834607_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t1864361006, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t1864361006_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t3603750770_marshal_pinvoke, Event_t3603750770_marshal_pinvoke_back, Event_t3603750770_marshal_pinvoke_cleanup, NULL, NULL, &Event_t3603750770_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t2037703605, NULL, NULL, NULL, NULL, NULL, &UnityAction_t2037703605_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t813148810_marshal_pinvoke, FailedToLoadScriptObject_t813148810_marshal_pinvoke_back, FailedToLoadScriptObject_t813148810_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t813148810_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t1352591366, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t1352591366_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t3716333628_marshal_pinvoke, Gradient_t3716333628_marshal_pinvoke_back, Gradient_t3716333628_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3716333628_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t2031498674, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t2031498674_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t468091463_marshal_pinvoke, GUIContent_t468091463_marshal_pinvoke_back, GUIContent_t468091463_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t468091463_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t2520811586, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t2520811586_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t2932200036_marshal_pinvoke, GUIStyle_t2932200036_marshal_pinvoke_back, GUIStyle_t2932200036_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t2932200036_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t2470603889_marshal_pinvoke, GUIStyleState_t2470603889_marshal_pinvoke_back, GUIStyleState_t2470603889_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t2470603889_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HumanBone_t1430191328_marshal_pinvoke, HumanBone_t1430191328_marshal_pinvoke_back, HumanBone_t1430191328_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t1430191328_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, Object_t1008057425_marshal_pinvoke, Object_t1008057425_marshal_pinvoke_back, Object_t1008057425_marshal_pinvoke_cleanup, NULL, NULL, &Object_t1008057425_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t1499311357_marshal_pinvoke, PlayableBinding_t1499311357_marshal_pinvoke_back, PlayableBinding_t1499311357_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t1499311357_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RaycastHit_t1273336641_marshal_pinvoke, RaycastHit_t1273336641_marshal_pinvoke_back, RaycastHit_t1273336641_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t1273336641_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t2323693239_marshal_pinvoke, RaycastHit2D_t2323693239_marshal_pinvoke_back, RaycastHit2D_t2323693239_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t2323693239_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t2771649182_marshal_pinvoke, RectOffset_t2771649182_marshal_pinvoke_back, RectOffset_t2771649182_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t2771649182_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1627915661, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1627915661_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t1970232538_marshal_pinvoke, ResourceRequest_t1970232538_marshal_pinvoke_back, ResourceRequest_t1970232538_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t1970232538_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t332921144_marshal_pinvoke, ScriptableObject_t332921144_marshal_pinvoke_back, ScriptableObject_t332921144_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t332921144_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t2832651489_marshal_pinvoke, HitInfo_t2832651489_marshal_pinvoke_back, HitInfo_t2832651489_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t2832651489_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t2095843144_marshal_pinvoke, SkeletonBone_t2095843144_marshal_pinvoke_back, SkeletonBone_t2095843144_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t2095843144_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t1098514478_marshal_pinvoke, GcAchievementData_t1098514478_marshal_pinvoke_back, GcAchievementData_t1098514478_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t1098514478_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t83581021_marshal_pinvoke, GcAchievementDescriptionData_t83581021_marshal_pinvoke_back, GcAchievementDescriptionData_t83581021_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t83581021_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t374162520_marshal_pinvoke, GcLeaderboard_t374162520_marshal_pinvoke_back, GcLeaderboard_t374162520_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t374162520_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t322958630_marshal_pinvoke, GcScoreData_t322958630_marshal_pinvoke_back, GcScoreData_t322958630_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t322958630_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t1570437431_marshal_pinvoke, GcUserProfileData_t1570437431_marshal_pinvoke_back, GcUserProfileData_t1570437431_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t1570437431_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, TextGenerationSettings_t2160595927_marshal_pinvoke, TextGenerationSettings_t2160595927_marshal_pinvoke_back, TextGenerationSettings_t2160595927_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t2160595927_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t3308673789_marshal_pinvoke, TextGenerator_t3308673789_marshal_pinvoke_back, TextGenerator_t3308673789_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t3308673789_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, TrackedReference_t3940723406_marshal_pinvoke, TrackedReference_t3940723406_marshal_pinvoke_back, TrackedReference_t3940723406_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t3940723406_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3398639038, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3398639038_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t886435312_marshal_pinvoke, WorkRequest_t886435312_marshal_pinvoke_back, WorkRequest_t886435312_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t886435312_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t3647418858_marshal_pinvoke, WaitForSeconds_t3647418858_marshal_pinvoke_back, WaitForSeconds_t3647418858_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t3647418858_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t3975915868_marshal_pinvoke, YieldInstruction_t3975915868_marshal_pinvoke_back, YieldInstruction_t3975915868_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t3975915868_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ NULL, RaycastResult_t1715322097_marshal_pinvoke, RaycastResult_t1715322097_marshal_pinvoke_back, RaycastResult_t1715322097_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t1715322097_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t1347433457_marshal_pinvoke, ColorTween_t1347433457_marshal_pinvoke_back, ColorTween_t1347433457_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t1347433457_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1459678735_marshal_pinvoke, FloatTween_t1459678735_marshal_pinvoke_back, FloatTween_t1459678735_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1459678735_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t3522918643_marshal_pinvoke, Resources_t3522918643_marshal_pinvoke_back, Resources_t3522918643_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t3522918643_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t4262405814, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t4262405814_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t2887492880_marshal_pinvoke, Navigation_t2887492880_marshal_pinvoke_back, Navigation_t2887492880_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t2887492880_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t758977253_marshal_pinvoke, SpriteState_t758977253_marshal_pinvoke_back, SpriteState_t758977253_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t758977253_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, ARAnchor_t4044008708_marshal_pinvoke, ARAnchor_t4044008708_marshal_pinvoke_back, ARAnchor_t4044008708_marshal_pinvoke_cleanup, NULL, NULL, &ARAnchor_t4044008708_0_0_0 } /* UnityEngine.XR.iOS.ARAnchor */,
	{ NULL, ARHitTestResult_t1803723679_marshal_pinvoke, ARHitTestResult_t1803723679_marshal_pinvoke_back, ARHitTestResult_t1803723679_marshal_pinvoke_cleanup, NULL, NULL, &ARHitTestResult_t1803723679_0_0_0 } /* UnityEngine.XR.iOS.ARHitTestResult */,
	{ NULL, ARKitSessionConfiguration_t3142208763_marshal_pinvoke, ARKitSessionConfiguration_t3142208763_marshal_pinvoke_back, ARKitSessionConfiguration_t3142208763_marshal_pinvoke_cleanup, NULL, NULL, &ARKitSessionConfiguration_t3142208763_0_0_0 } /* UnityEngine.XR.iOS.ARKitSessionConfiguration */,
	{ NULL, ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke, ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_back, ARKitWorldTackingSessionConfiguration_t2186350340_marshal_pinvoke_cleanup, NULL, NULL, &ARKitWorldTackingSessionConfiguration_t2186350340_0_0_0 } /* UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration */,
	{ NULL, ARPlaneAnchor_t1843157284_marshal_pinvoke, ARPlaneAnchor_t1843157284_marshal_pinvoke_back, ARPlaneAnchor_t1843157284_marshal_pinvoke_cleanup, NULL, NULL, &ARPlaneAnchor_t1843157284_0_0_0 } /* UnityEngine.XR.iOS.ARPlaneAnchor */,
	{ NULL, UnityARCamera_t2376600594_marshal_pinvoke, UnityARCamera_t2376600594_marshal_pinvoke_back, UnityARCamera_t2376600594_marshal_pinvoke_cleanup, NULL, NULL, &UnityARCamera_t2376600594_0_0_0 } /* UnityEngine.XR.iOS.UnityARCamera */,
	{ NULL, UnityARHitTestResult_t1002809393_marshal_pinvoke, UnityARHitTestResult_t1002809393_marshal_pinvoke_back, UnityARHitTestResult_t1002809393_marshal_pinvoke_cleanup, NULL, NULL, &UnityARHitTestResult_t1002809393_0_0_0 } /* UnityEngine.XR.iOS.UnityARHitTestResult */,
	{ DelegatePInvokeWrapper_ARAnchorAdded_t1580091102, NULL, NULL, NULL, NULL, NULL, &ARAnchorAdded_t1580091102_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded */,
	{ DelegatePInvokeWrapper_ARAnchorRemoved_t3663543047, NULL, NULL, NULL, NULL, NULL, &ARAnchorRemoved_t3663543047_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_ARAnchorUpdated_t1492128022, NULL, NULL, NULL, NULL, NULL, &ARAnchorUpdated_t1492128022_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_ARSessionFailed_t3599980200, NULL, NULL, NULL, NULL, NULL, &ARSessionFailed_t3599980200_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed */,
	{ DelegatePInvokeWrapper_internal_ARAnchorAdded_t3791587037, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorAdded_t3791587037_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARAnchorRemoved_t409959511, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorRemoved_t409959511_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARAnchorUpdated_t2928809250, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorUpdated_t2928809250_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARFrameUpdate_t2726389622, NULL, NULL, NULL, NULL, NULL, &internal_ARFrameUpdate_t2726389622_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate */,
	NULL,
};
