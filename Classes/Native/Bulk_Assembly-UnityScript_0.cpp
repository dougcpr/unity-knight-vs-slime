﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// CharaView
struct CharaView_t926653685;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1096588306;
// UnityEngine.Component
struct Component_t531478471;
// UnityEngine.Animation
struct Animation_t1557311624;
// System.String
struct String_t;
// UnityEngine.AnimationClip
struct AnimationClip_t2131674754;
// UnityEngine.Object
struct Object_t1008057425;
// UnityEngine.GameObject
struct GameObject_t3649338848;
// UnityScript.Lang.Array
struct Array_t273358690;
// UnityEngine.Transform
struct Transform_t3933397867;
// System.Collections.IEnumerator
struct IEnumerator_t2441520391;
// System.Type
struct Type_t;
// UnityEngine.AnimationState
struct AnimationState_t2216918921;
// System.Collections.ArrayList
struct ArrayList_t2776062965;
// System.Char[]
struct CharU5BU5D_t674980486;
// System.Void
struct Void_t2725935594;
// System.Type[]
struct TypeU5BU5D_t1484232934;
// System.Reflection.MemberFilter
struct MemberFilter_t139494810;

extern RuntimeClass* Int32_t3425510919_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeServices_t4253430258_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisAnimation_t1557311624_m834402472_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAnimation_t1557311624_m1891609684_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2435691891;
extern const uint32_t CharaView_Start_m778585814_MetadataUsageId;
extern const RuntimeType* String_t_0_0_0_var;
extern RuntimeClass* Array_t273358690_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityRuntimeServices_t3217273655_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t2441520391_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* GUI_t3786480996_il2cpp_TypeInfo_var;
extern const uint32_t CharaView_OnGUI_m3334526304_MetadataUsageId;
extern const RuntimeType* AnimationState_t2216918921_0_0_0_var;
extern RuntimeClass* AnimationState_t2216918921_il2cpp_TypeInfo_var;
extern const uint32_t CharaView_GetAnimationList_m1479231431_MetadataUsageId;



#ifndef U3CMODULEU3E_T3896811618_H
#define U3CMODULEU3E_T3896811618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3896811618 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3896811618_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef COLLECTIONBASE_T2085369418_H
#define COLLECTIONBASE_T2085369418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t2085369418  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t2776062965 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t2085369418, ___list_0)); }
	inline ArrayList_t2776062965 * get_list_0() const { return ___list_0; }
	inline ArrayList_t2776062965 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t2776062965 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T2085369418_H
#ifndef VALUETYPE_T3433162460_H
#define VALUETYPE_T3433162460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3433162460  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3433162460_marshaled_com
{
};
#endif // VALUETYPE_T3433162460_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t674980486* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t674980486* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t674980486** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t674980486* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T362855854_H
#define BOOLEAN_T362855854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t362855854 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t362855854, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t362855854_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t362855854_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t362855854_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T362855854_H
#ifndef RECT_T1992046353_H
#define RECT_T1992046353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t1992046353 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t1992046353, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T1992046353_H
#ifndef VECTOR3_T596762001_H
#define VECTOR3_T596762001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t596762001 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t596762001, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t596762001_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t596762001  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t596762001  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t596762001  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t596762001  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t596762001  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t596762001  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t596762001  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t596762001  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t596762001  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t596762001  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___zeroVector_4)); }
	inline Vector3_t596762001  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t596762001 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t596762001  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___oneVector_5)); }
	inline Vector3_t596762001  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t596762001 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t596762001  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___upVector_6)); }
	inline Vector3_t596762001  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t596762001 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t596762001  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___downVector_7)); }
	inline Vector3_t596762001  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t596762001 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t596762001  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___leftVector_8)); }
	inline Vector3_t596762001  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t596762001 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t596762001  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___rightVector_9)); }
	inline Vector3_t596762001  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t596762001 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t596762001  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___forwardVector_10)); }
	inline Vector3_t596762001  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t596762001 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t596762001  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___backVector_11)); }
	inline Vector3_t596762001  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t596762001 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t596762001  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t596762001  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t596762001 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t596762001  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t596762001_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t596762001  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t596762001 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t596762001  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T596762001_H
#ifndef UINT32_T3933237433_H
#define UINT32_T3933237433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t3933237433 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t3933237433, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T3933237433_H
#ifndef ARRAY_T273358690_H
#define ARRAY_T273358690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityScript.Lang.Array
struct  Array_t273358690  : public CollectionBase_t2085369418
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAY_T273358690_H
#ifndef ENUM_T473240710_H
#define ENUM_T473240710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t473240710  : public ValueType_t3433162460
{
public:

public:
};

struct Enum_t473240710_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t674980486* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t473240710_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t674980486* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t674980486** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t674980486* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t473240710_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t473240710_marshaled_com
{
};
#endif // ENUM_T473240710_H
#ifndef INT32_T3425510919_H
#define INT32_T3425510919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t3425510919 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t3425510919, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T3425510919_H
#ifndef VOID_T2725935594_H
#define VOID_T2725935594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2725935594 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2725935594_H
#ifndef SINGLE_T2847614712_H
#define SINGLE_T2847614712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2847614712 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2847614712, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2847614712_H
#ifndef BINDINGFLAGS_T3095937196_H
#define BINDINGFLAGS_T3095937196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t3095937196 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t3095937196, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T3095937196_H
#ifndef TRACKEDREFERENCE_T3940723406_H
#define TRACKEDREFERENCE_T3940723406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TrackedReference
struct  TrackedReference_t3940723406  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TrackedReference_t3940723406, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_t3940723406_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_t3940723406_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // TRACKEDREFERENCE_T3940723406_H
#ifndef RUNTIMETYPEHANDLE_T3928229644_H
#define RUNTIMETYPEHANDLE_T3928229644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3928229644 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3928229644, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3928229644_H
#ifndef OBJECT_T1008057425_H
#define OBJECT_T1008057425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1008057425  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1008057425, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1008057425_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1008057425_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1008057425_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1008057425_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1008057425_H
#ifndef MOTION_T2597954375_H
#define MOTION_T2597954375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Motion
struct  Motion_t2597954375  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTION_T2597954375_H
#ifndef ANIMATIONSTATE_T2216918921_H
#define ANIMATIONSTATE_T2216918921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationState
struct  AnimationState_t2216918921  : public TrackedReference_t3940723406
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T2216918921_H
#ifndef COMPONENT_T531478471_H
#define COMPONENT_T531478471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t531478471  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T531478471_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3928229644  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3928229644  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3928229644 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3928229644  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1484232934* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t139494810 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t139494810 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t139494810 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1484232934* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1484232934** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1484232934* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t139494810 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t139494810 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t139494810 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t139494810 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t139494810 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t139494810 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t139494810 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t139494810 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t139494810 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef GAMEOBJECT_T3649338848_H
#define GAMEOBJECT_T3649338848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t3649338848  : public Object_t1008057425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T3649338848_H
#ifndef BEHAVIOUR_T2200997390_H
#define BEHAVIOUR_T2200997390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2200997390  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2200997390_H
#ifndef TRANSFORM_T3933397867_H
#define TRANSFORM_T3933397867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3933397867  : public Component_t531478471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3933397867_H
#ifndef ANIMATIONCLIP_T2131674754_H
#define ANIMATIONCLIP_T2131674754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationClip
struct  AnimationClip_t2131674754  : public Motion_t2597954375
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIP_T2131674754_H
#ifndef MONOBEHAVIOUR_T1096588306_H
#define MONOBEHAVIOUR_T1096588306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1096588306  : public Behaviour_t2200997390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1096588306_H
#ifndef ANIMATION_T1557311624_H
#define ANIMATION_T1557311624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation
struct  Animation_t1557311624  : public Behaviour_t2200997390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T1557311624_H
#ifndef CHARAVIEW_T926653685_H
#define CHARAVIEW_T926653685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharaView
struct  CharaView_t926653685  : public MonoBehaviour_t1096588306
{
public:
	// System.Single CharaView::roteSpeed
	float ___roteSpeed_2;
	// System.Single CharaView::animationSpeed
	float ___animationSpeed_3;
	// System.UInt32 CharaView::animationCount
	uint32_t ___animationCount_4;
	// UnityScript.Lang.Array CharaView::animationList
	Array_t273358690 * ___animationList_5;

public:
	inline static int32_t get_offset_of_roteSpeed_2() { return static_cast<int32_t>(offsetof(CharaView_t926653685, ___roteSpeed_2)); }
	inline float get_roteSpeed_2() const { return ___roteSpeed_2; }
	inline float* get_address_of_roteSpeed_2() { return &___roteSpeed_2; }
	inline void set_roteSpeed_2(float value)
	{
		___roteSpeed_2 = value;
	}

	inline static int32_t get_offset_of_animationSpeed_3() { return static_cast<int32_t>(offsetof(CharaView_t926653685, ___animationSpeed_3)); }
	inline float get_animationSpeed_3() const { return ___animationSpeed_3; }
	inline float* get_address_of_animationSpeed_3() { return &___animationSpeed_3; }
	inline void set_animationSpeed_3(float value)
	{
		___animationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_animationCount_4() { return static_cast<int32_t>(offsetof(CharaView_t926653685, ___animationCount_4)); }
	inline uint32_t get_animationCount_4() const { return ___animationCount_4; }
	inline uint32_t* get_address_of_animationCount_4() { return &___animationCount_4; }
	inline void set_animationCount_4(uint32_t value)
	{
		___animationCount_4 = value;
	}

	inline static int32_t get_offset_of_animationList_5() { return static_cast<int32_t>(offsetof(CharaView_t926653685, ___animationList_5)); }
	inline Array_t273358690 * get_animationList_5() const { return ___animationList_5; }
	inline Array_t273358690 ** get_address_of_animationList_5() { return &___animationList_5; }
	inline void set_animationList_5(Array_t273358690 * value)
	{
		___animationList_5 = value;
		Il2CppCodeGenWriteBarrier((&___animationList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARAVIEW_T926653685_H


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m3649492292_gshared (Component_t531478471 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m3090413657_gshared (GameObject_t3649338848 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m4062598822 (MonoBehaviour_t1096588306 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t1557311624_m834402472(__this, method) ((  Animation_t1557311624 * (*) (Component_t531478471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3649492292_gshared)(__this, method)
// System.Int32 UnityEngine.Animation::GetClipCount()
extern "C"  int32_t Animation_GetClipCount_m2530880824 (Animation_t1557311624 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Boo.Lang.Runtime.RuntimeServices::op_Addition(System.String,System.Object)
extern "C"  String_t* RuntimeServices_op_Addition_m1883231782 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m2318390180 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Animation::get_clip()
extern "C"  AnimationClip_t2131674754 * Animation_get_clip_m996818244 (Animation_t1557311624 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3367685655 (Object_t1008057425 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t3649338848 * Component_get_gameObject_m3065601689 (Component_t531478471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
#define GameObject_GetComponent_TisAnimation_t1557311624_m1891609684(__this, method) ((  Animation_t1557311624 * (*) (GameObject_t3649338848 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m3090413657_gshared)(__this, method)
// UnityScript.Lang.Array CharaView::GetAnimationList()
extern "C"  Array_t273358690 * CharaView_GetAnimationList_m1479231431 (CharaView_t926653685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3933397867 * Component_get_transform_m2033240428 (Component_t531478471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t596762001  Transform_get_eulerAngles_m709937209 (Transform_t3933397867 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m952845496 (Transform_t3933397867 * __this, Vector3_t596762001  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.Array::.ctor()
extern "C"  void Array__ctor_m4055908730 (Array_t273358690 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityScript.Lang.UnityRuntimeServices::GetEnumerator(System.Object)
extern "C"  RuntimeObject* UnityRuntimeServices_GetEnumerator_m3761824340 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1145571015 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3928229644  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object Boo.Lang.Runtime.RuntimeServices::Coerce(System.Object,System.Type)
extern "C"  RuntimeObject * RuntimeServices_Coerce_m2787646698 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Type_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1383055445 (Rect_t1992046353 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object UnityScript.Lang.Array::get_Item(System.Int32)
extern "C"  RuntimeObject * Array_get_Item_m35308191 (Array_t273358690 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern "C"  bool GUI_Button_m1880561377 (RuntimeObject * __this /* static, unused */, Rect_t1992046353  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single)
extern "C"  void Animation_CrossFade_m309085159 (Animation_t1557311624 * __this, String_t* p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationState::get_name()
extern "C"  String_t* AnimationState_get_name_m2233452754 (AnimationState_t2216918921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.Array::Add(System.Object)
extern "C"  void Array_Add_m2056036819 (Array_t273358690 * __this, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityScript.Lang.UnityRuntimeServices::Update(System.Collections.IEnumerator,System.Object)
extern "C"  void UnityRuntimeServices_Update_m2170783750 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharaView::.ctor()
extern "C"  void CharaView__ctor_m1346264881 (CharaView_t926653685 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m4062598822(__this, /*hidden argument*/NULL);
		__this->set_animationSpeed_3((1.0f));
		return;
	}
}
// System.Void CharaView::Start()
extern "C"  void CharaView_Start_m778585814 (CharaView_t926653685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharaView_Start_m778585814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t1557311624 * L_0 = Component_GetComponent_TisAnimation_t1557311624_m834402472(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t1557311624_m834402472_RuntimeMethod_var);
		NullCheck(L_0);
		int32_t L_1 = Animation_GetClipCount_m2530880824(L_0, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t3425510919_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		String_t* L_4 = RuntimeServices_op_Addition_m1883231782(NULL /*static, unused*/, _stringLiteral2435691891, L_3, /*hidden argument*/NULL);
		MonoBehaviour_print_m2318390180(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Animation_t1557311624 * L_5 = Component_GetComponent_TisAnimation_t1557311624_m834402472(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t1557311624_m834402472_RuntimeMethod_var);
		NullCheck(L_5);
		AnimationClip_t2131674754 * L_6 = Animation_get_clip_m996818244(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m3367685655(L_6, /*hidden argument*/NULL);
		MonoBehaviour_print_m2318390180(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Animation_t1557311624 * L_8 = Component_GetComponent_TisAnimation_t1557311624_m834402472(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t1557311624_m834402472_RuntimeMethod_var);
		NullCheck(L_8);
		int32_t L_9 = Animation_GetClipCount_m2530880824(L_8, /*hidden argument*/NULL);
		__this->set_animationCount_4((((int32_t)((uint32_t)L_9))));
		GameObject_t3649338848 * L_10 = Component_get_gameObject_m3065601689(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Animation_t1557311624 * L_11 = GameObject_GetComponent_TisAnimation_t1557311624_m1891609684(L_10, /*hidden argument*/GameObject_GetComponent_TisAnimation_t1557311624_m1891609684_RuntimeMethod_var);
		MonoBehaviour_print_m2318390180(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Array_t273358690 * L_12 = CharaView_GetAnimationList_m1479231431(__this, /*hidden argument*/NULL);
		__this->set_animationList_5(L_12);
		return;
	}
}
// System.Void CharaView::Update()
extern "C"  void CharaView_Update_m469188604 (CharaView_t926653685 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	Vector3_t596762001  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t596762001  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t596762001  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Transform_t3933397867 * L_0 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t596762001  L_1 = Transform_get_eulerAngles_m709937209(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		float L_2 = (&V_2)->get_y_2();
		float L_3 = __this->get_roteSpeed_2();
		float L_4 = ((float)((float)L_2+(float)L_3));
		V_0 = L_4;
		Transform_t3933397867 * L_5 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t596762001  L_6 = Transform_get_eulerAngles_m709937209(L_5, /*hidden argument*/NULL);
		Vector3_t596762001  L_7 = L_6;
		V_1 = L_7;
		float L_8 = V_0;
		float L_9 = L_8;
		V_3 = L_9;
		(&V_1)->set_y_2(L_9);
		float L_10 = V_3;
		Transform_t3933397867 * L_11 = Component_get_transform_m2033240428(__this, /*hidden argument*/NULL);
		Vector3_t596762001  L_12 = V_1;
		Vector3_t596762001  L_13 = L_12;
		V_4 = L_13;
		NullCheck(L_11);
		Transform_set_eulerAngles_m952845496(L_11, L_13, /*hidden argument*/NULL);
		Vector3_t596762001  L_14 = V_4;
		return;
	}
}
// System.Void CharaView::OnGUI()
extern "C"  void CharaView_OnGUI_m3334526304 (CharaView_t926653685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharaView_OnGUI_m3334526304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Array_t273358690 * V_5 = NULL;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	Rect_t1992046353  V_8;
	memset(&V_8, 0, sizeof(V_8));
	RuntimeObject* V_9 = NULL;
	RuntimeObject * G_B3_0 = NULL;
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B6_0 = NULL;
	Animation_t1557311624 * G_B6_1 = NULL;
	RuntimeObject * G_B5_0 = NULL;
	Animation_t1557311624 * G_B5_1 = NULL;
	{
		V_0 = ((int32_t)10);
		V_1 = ((int32_t)25);
		V_2 = ((int32_t)100);
		V_3 = ((int32_t)40);
		V_4 = ((int32_t)10);
		Array_t273358690 * L_0 = (Array_t273358690 *)il2cpp_codegen_object_new(Array_t273358690_il2cpp_TypeInfo_var);
		Array__ctor_m4055908730(L_0, /*hidden argument*/NULL);
		V_5 = L_0;
		V_6 = 0;
		Array_t273358690 * L_1 = __this->get_animationList_5();
		IL2CPP_RUNTIME_CLASS_INIT(UnityRuntimeServices_t3217273655_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = UnityRuntimeServices_GetEnumerator_m3761824340(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_9 = L_2;
		goto IL_00cf;
	}

IL_002c:
	{
		RuntimeObject* L_3 = V_9;
		NullCheck(L_3);
		RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_3);
		RuntimeObject * L_5 = L_4;
		G_B2_0 = L_5;
		if (((String_t*)IsInstSealed((RuntimeObject*)L_5, String_t_il2cpp_TypeInfo_var)))
		{
			G_B3_0 = L_5;
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1145571015(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		RuntimeObject * L_7 = RuntimeServices_Coerce_m2787646698(NULL /*static, unused*/, G_B2_0, L_6, /*hidden argument*/NULL);
		G_B3_0 = L_7;
	}

IL_004d:
	{
		V_7 = ((String_t*)CastclassSealed((RuntimeObject*)G_B3_0, String_t_il2cpp_TypeInfo_var));
		int32_t L_8 = V_0;
		int32_t L_9 = V_6;
		int32_t L_10 = V_1;
		int32_t L_11 = V_6;
		int32_t L_12 = V_2;
		int32_t L_13 = V_3;
		Rect_t1992046353  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1383055445((&L_14), (((float)((float)((int32_t)15)))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_8+(int32_t)((int32_t)((int32_t)((int32_t)20)*(int32_t)L_9))))+(int32_t)((int32_t)((int32_t)L_10*(int32_t)L_11))))))), (((float)((float)L_12))), (((float)((float)L_13))), /*hidden argument*/NULL);
		V_8 = L_14;
		Rect_t1992046353  L_15 = V_8;
		Array_t273358690 * L_16 = __this->get_animationList_5();
		int32_t L_17 = V_6;
		NullCheck(L_16);
		RuntimeObject * L_18 = Array_get_Item_m35308191(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3786480996_il2cpp_TypeInfo_var);
		bool L_20 = GUI_Button_m1880561377(NULL /*static, unused*/, L_15, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00c9;
		}
	}
	{
		Animation_t1557311624 * L_21 = Component_GetComponent_TisAnimation_t1557311624_m834402472(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t1557311624_m834402472_RuntimeMethod_var);
		Array_t273358690 * L_22 = __this->get_animationList_5();
		int32_t L_23 = V_6;
		NullCheck(L_22);
		RuntimeObject * L_24 = Array_get_Item_m35308191(L_22, L_23, /*hidden argument*/NULL);
		RuntimeObject * L_25 = L_24;
		G_B5_0 = L_25;
		G_B5_1 = L_21;
		if (((String_t*)IsInstSealed((RuntimeObject*)L_25, String_t_il2cpp_TypeInfo_var)))
		{
			G_B6_0 = L_25;
			G_B6_1 = L_21;
			goto IL_00ba;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1145571015(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		RuntimeObject * L_27 = RuntimeServices_Coerce_m2787646698(NULL /*static, unused*/, G_B5_0, L_26, /*hidden argument*/NULL);
		G_B6_0 = L_27;
		G_B6_1 = G_B5_1;
	}

IL_00ba:
	{
		NullCheck(G_B6_1);
		Animation_CrossFade_m309085159(G_B6_1, ((String_t*)CastclassSealed((RuntimeObject*)G_B6_0, String_t_il2cpp_TypeInfo_var)), (0.01f), /*hidden argument*/NULL);
	}

IL_00c9:
	{
		int32_t L_28 = V_6;
		V_6 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00cf:
	{
		RuntimeObject* L_29 = V_9;
		NullCheck(L_29);
		bool L_30 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_29);
		if (L_30)
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// UnityScript.Lang.Array CharaView::GetAnimationList()
extern "C"  Array_t273358690 * CharaView_GetAnimationList_m1479231431 (CharaView_t926653685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharaView_GetAnimationList_m1479231431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Array_t273358690 * V_0 = NULL;
	AnimationState_t2216918921 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject * G_B3_0 = NULL;
	RuntimeObject * G_B2_0 = NULL;
	{
		Array_t273358690 * L_0 = (Array_t273358690 *)il2cpp_codegen_object_new(Array_t273358690_il2cpp_TypeInfo_var);
		Array__ctor_m4055908730(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t3649338848 * L_1 = Component_get_gameObject_m3065601689(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Animation_t1557311624 * L_2 = GameObject_GetComponent_TisAnimation_t1557311624_m1891609684(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimation_t1557311624_m1891609684_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(UnityRuntimeServices_t3217273655_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = UnityRuntimeServices_GetEnumerator_m3761824340(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		goto IL_0055;
	}

IL_001c:
	{
		RuntimeObject* L_4 = V_2;
		NullCheck(L_4);
		RuntimeObject * L_5 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_4);
		RuntimeObject * L_6 = L_5;
		G_B2_0 = L_6;
		if (((AnimationState_t2216918921 *)IsInstSealed((RuntimeObject*)L_6, AnimationState_t2216918921_il2cpp_TypeInfo_var)))
		{
			G_B3_0 = L_6;
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1145571015(NULL /*static, unused*/, LoadTypeToken(AnimationState_t2216918921_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t4253430258_il2cpp_TypeInfo_var);
		RuntimeObject * L_8 = RuntimeServices_Coerce_m2787646698(NULL /*static, unused*/, G_B2_0, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_003c:
	{
		V_1 = ((AnimationState_t2216918921 *)CastclassSealed((RuntimeObject*)G_B3_0, AnimationState_t2216918921_il2cpp_TypeInfo_var));
		Array_t273358690 * L_9 = V_0;
		AnimationState_t2216918921 * L_10 = V_1;
		NullCheck(L_10);
		String_t* L_11 = AnimationState_get_name_m2233452754(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Array_Add_m2056036819(L_9, L_11, /*hidden argument*/NULL);
		RuntimeObject* L_12 = V_2;
		AnimationState_t2216918921 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityRuntimeServices_t3217273655_il2cpp_TypeInfo_var);
		UnityRuntimeServices_Update_m2170783750(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0055:
	{
		RuntimeObject* L_14 = V_2;
		NullCheck(L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2441520391_il2cpp_TypeInfo_var, L_14);
		if (L_15)
		{
			goto IL_001c;
		}
	}
	{
		Array_t273358690 * L_16 = V_0;
		return L_16;
	}
}
// System.Void CharaView::Main()
extern "C"  void CharaView_Main_m1161562734 (CharaView_t926653685 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
